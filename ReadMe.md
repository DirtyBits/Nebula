# Nebula - Modded Nebulous

### Classes
* `software.simplicial.a.ao` - base class for all interactable objects in nebulous
* `software.simplicial.a.bf` - base class for all players
* `software.simplicial.a.bs` - the game's engine. most of the game's physics and calculations are done here. split into two files because of its size.
* `software.simplicial.a.e` - this class handles all the skins
* `software.simplicial.a.i` - any instance of this class is a bot. this is the bot class
* `software.simplicial.nebulous.f.al` - this class does all the in-game http requests
* `software.simplicial.nebulous.f.b` - ads.
* `software.simplicial.nebulous.views.j` - this class houses the game's camera
* `software.simplicial.nebulous.views.GameView$c` - this is where all the opengl drawing is done.
* `software.simplicial.nebulous.f.ag` - the class that handles the shared preferences(really useful)

### Mods
* `software.simplicial.a.bf Line: 2472` - Insta Recombine
* `software.simplicial.a.bs Line: 25806` - 2.25 speed
* `software.simplicial.a.bs Line: 29748` - Anti-pop
* `software.simplicial.a.bs Line: 30255` - 100 mass per blue hole
* `software.simplicial.nebulous.views.GameView Line: 6710` - 1.5x fov mod
* `software.simplicial.nebulous.application.ar Line: 2352` - core function for the invite button
* `software.simplicial.nebulous.f.al$26 Line: 140` - download skin

### Other useful stuff
* `software.simplicial.nebulous.views.GameView.dj` - fov of the camera
* `software.simplicial.a.bk.s` - probably the camera's x coordinate
* `software.simplicial.a.bk.t` - probably the camera's y coordinate
* `software.simplicial.a.ao.n` - object radius
* `software.simplicial.a.ao.l` - object x pos
* `software.simplicial.a.ao.m` - object y pos
* `software.simplicial.a.bk.ae` - array containing all the player objects in the game
* `software.simplicial.a.bs Line: 30255` - 100 mass per blue hole
