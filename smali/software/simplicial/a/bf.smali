.class public Lsoftware/simplicial/a/bf;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/a/ao;",
        "Ljava/lang/Comparable",
        "<",
        "Lsoftware/simplicial/a/bf;",
        ">;"
    }
.end annotation


# instance fields
.field public A:I

.field public B:J

.field public C:I

.field public D:Ljava/lang/String;

.field public E:[B

.field public F:[B

.field public G:Z

.field public H:Ljava/lang/String;

.field public I:[B

.field public J:Lsoftware/simplicial/a/q;

.field public K:Z

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bh;",
            ">;"
        }
    .end annotation
.end field

.field public volatile M:F

.field public N:F

.field public volatile O:F

.field public P:F

.field public Q:I

.field public R:B

.field public S:I

.field public T:F

.field public U:Lsoftware/simplicial/a/e;

.field public V:Lsoftware/simplicial/a/e;

.field public W:Lsoftware/simplicial/a/af;

.field public X:Lsoftware/simplicial/a/af;

.field public Y:Lsoftware/simplicial/a/bd;

.field public Z:B

.field private a:F

.field public aA:Z

.field public aB:Lsoftware/simplicial/a/ay;

.field public aC:Lsoftware/simplicial/a/az;

.field public aD:F

.field public aE:J

.field public aF:Z

.field public aG:Lsoftware/simplicial/a/ag;

.field public aH:Lsoftware/simplicial/a/f/bv;

.field public aI:I

.field public aJ:B

.field public aK:J

.field public aL:B

.field public aM:[B

.field public aN:I

.field public aO:I

.field public aP:I

.field public aQ:I

.field public aR:I

.field public aS:I

.field public aT:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/bf;",
            "Lsoftware/simplicial/a/ad;",
            ">;"
        }
    .end annotation
.end field

.field public aU:F

.field public aV:I

.field public aW:J

.field public aX:Z

.field public aY:Z

.field public aZ:I

.field public aa:Lsoftware/simplicial/a/as;

.field public ab:Lsoftware/simplicial/a/as;

.field public ac:Ljava/lang/String;

.field public ad:I

.field public ae:I

.field public af:I

.field public ag:I

.field public ah:I

.field public ai:I

.field public aj:Z

.field public ak:Lsoftware/simplicial/a/bx;

.field public al:F

.field public am:B

.field public an:I

.field public ao:Lsoftware/simplicial/a/s;

.field public ap:J

.field public aq:J

.field public ar:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation
.end field

.field public as:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation
.end field

.field public at:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation
.end field

.field public au:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public av:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation
.end field

.field public aw:Z

.field public ax:I

.field public ay:Lsoftware/simplicial/a/aa;

.field public az:Z

.field private b:Lsoftware/simplicial/a/bh;

.field public bA:I

.field public bB:I

.field public bC:Lsoftware/simplicial/a/f/bl;

.field public bD:I

.field public bE:Z

.field public bF:I

.field public ba:I

.field public bb:I

.field public bc:I

.field public bd:I

.field public be:I

.field public bf:I

.field public bg:I

.field public bh:I

.field public bi:I

.field public bj:I

.field public bk:I

.field public bl:I

.field public bm:I

.field public bn:I

.field public bo:I

.field public bp:Z

.field public bq:Z

.field public br:I

.field public bs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation
.end field

.field public bt:Z

.field public bu:Z

.field public bv:I

.field public bw:Z

.field public bx:Z

.field public by:Z

.field public bz:I

.field public z:[Lsoftware/simplicial/a/bh;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 167
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 46
    iput-object v3, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/a/bf;->B:J

    .line 51
    new-array v0, v2, [B

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->E:[B

    .line 52
    new-array v0, v2, [B

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->F:[B

    .line 57
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->K:Z

    .line 72
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bf;->Z:B

    .line 120
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    .line 147
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 148
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bu:Z

    .line 149
    iput v2, p0, Lsoftware/simplicial/a/bf;->bv:I

    .line 150
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bw:Z

    .line 151
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bx:Z

    .line 152
    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->by:Z

    .line 153
    iput v2, p0, Lsoftware/simplicial/a/bf;->bz:I

    .line 154
    iput v2, p0, Lsoftware/simplicial/a/bf;->bA:I

    .line 158
    iput v2, p0, Lsoftware/simplicial/a/bf;->bB:I

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bE:Z

    .line 163
    ## 32 blobs
    const/16 v0, 0x20

    iput v0, p0, Lsoftware/simplicial/a/bf;->bF:I

    .line 164
    iput-object v3, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    .line 168
    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->b()V

    .line 169
    return-void
.end method

.method private a(FF)F
    .locals 8

    .prologue
    .line 617
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 619
    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->d()I

    move-result v0

    int-to-float v3, v0

    .line 620
    const/high16 v4, 0x42c80000    # 100.0f

    .line 621
    const/high16 v1, 0x3f800000    # 1.0f

    .line 622
    const v5, 0x453b8000    # 3000.0f

    .line 623
    const/high16 v0, 0x3fe00000    # 1.75f

    .line 624
    sub-float v6, v0, v1

    sub-float v7, v5, v4

    div-float/2addr v6, v7

    .line 625
    mul-float v7, v6, v4

    sub-float v7, v1, v7

    .line 628
    cmpl-float v5, v3, v5

    if-ltz v5, :cond_0

    .line 635
    :goto_0
    const/high16 v1, 0x42aa0000    # 85.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    .line 636
    return v0

    .line 630
    :cond_0
    cmpl-float v0, v3, v4

    if-ltz v0, :cond_1

    .line 631
    mul-float v0, v6, v3

    add-float/2addr v0, v7

    goto :goto_0

    :cond_1
    move v0, v1

    .line 633
    goto :goto_0
.end method

.method private a(IZ)V
    .locals 8

    .prologue
    const-wide/32 v6, 0x4c4b40

    .line 841
    if-gtz p1, :cond_1

    .line 871
    :cond_0
    return-void

    .line 843
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_0

    .line 846
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v0, v0, Lsoftware/simplicial/a/az;->b:J

    .line 848
    if-eqz p2, :cond_2

    .line 849
    iget v2, p0, Lsoftware/simplicial/a/bf;->an:I

    mul-int/2addr p1, v2

    .line 851
    :cond_2
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v2, v2, Lsoftware/simplicial/a/ay;->g:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-lez v2, :cond_3

    .line 852
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v2, v2, Lsoftware/simplicial/a/ay;->g:J

    sub-long v2, v6, v2

    long-to-int p1, v2

    .line 854
    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v4, v2, Lsoftware/simplicial/a/ay;->g:J

    int-to-long v6, p1

    add-long/2addr v4, v6

    iput-wide v4, v2, Lsoftware/simplicial/a/ay;->g:J

    .line 855
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v2, Lsoftware/simplicial/a/az;->b:J

    int-to-long v6, p1

    add-long/2addr v4, v6

    iput-wide v4, v2, Lsoftware/simplicial/a/az;->b:J

    .line 856
    iget v2, p0, Lsoftware/simplicial/a/bf;->bo:I

    add-int/2addr v2, p1

    iput v2, p0, Lsoftware/simplicial/a/bf;->bo:I

    .line 858
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v2, Lsoftware/simplicial/a/az;->b:J

    iget-wide v4, p0, Lsoftware/simplicial/a/bf;->aK:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 860
    invoke-static {v0, v1}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v0

    .line 861
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v1, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v2, v3}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v1

    .line 862
    add-int/lit8 v2, v1, 0x1

    invoke-static {v2}, Lsoftware/simplicial/a/aw;->b(I)J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->aK:J

    .line 864
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bu:Z

    .line 865
    add-int/lit8 v0, v0, 0x1

    :goto_0
    if-gt v0, v1, :cond_0

    .line 867
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v2, Lsoftware/simplicial/a/ay;->u:I

    invoke-static {v0}, Lsoftware/simplicial/a/aw;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ay;->u:I

    .line 868
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v2, Lsoftware/simplicial/a/az;->p:I

    invoke-static {v0}, Lsoftware/simplicial/a/aw;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/az;->p:I

    .line 865
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(F)F
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v3, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 559
    .line 564
    iget-object v5, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v6, v5

    const/4 v0, 0x0

    move v4, v0

    move v1, v3

    move v2, p1

    move v0, p1

    :goto_0
    if-ge v4, v6, :cond_4

    aget-object v7, v5, v4

    .line 566
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v8

    if-eqz v8, :cond_0

    move v12, v3

    move v3, v0

    move v0, v12

    .line 564
    :goto_1
    add-int/lit8 v4, v4, 0x1

    move v12, v0

    move v0, v3

    move v3, v12

    goto :goto_0

    .line 569
    :cond_0
    iget v8, v7, Lsoftware/simplicial/a/bh;->s:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    sub-float/2addr v8, v9

    cmpg-float v8, v8, v0

    if-gez v8, :cond_1

    .line 570
    iget v0, v7, Lsoftware/simplicial/a/bh;->s:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    sub-float/2addr v0, v8

    .line 571
    :cond_1
    iget v8, v7, Lsoftware/simplicial/a/bh;->s:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    add-float/2addr v8, v9

    cmpl-float v8, v8, v1

    if-lez v8, :cond_2

    .line 572
    iget v1, v7, Lsoftware/simplicial/a/bh;->s:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    add-float/2addr v1, v8

    .line 573
    :cond_2
    iget v8, v7, Lsoftware/simplicial/a/bh;->t:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    sub-float/2addr v8, v9

    cmpg-float v8, v8, v2

    if-gez v8, :cond_3

    .line 574
    iget v2, v7, Lsoftware/simplicial/a/bh;->t:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    sub-float/2addr v2, v8

    .line 575
    :cond_3
    iget v8, v7, Lsoftware/simplicial/a/bh;->t:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    add-float/2addr v8, v9

    cmpl-float v8, v8, v3

    if-lez v8, :cond_8

    .line 576
    iget v3, v7, Lsoftware/simplicial/a/bh;->t:F

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    add-float/2addr v3, v7

    move v12, v3

    move v3, v0

    move v0, v12

    goto :goto_1

    .line 580
    :cond_4
    iget v4, p0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v4, :cond_6

    .line 582
    add-float v4, v1, v0

    div-float/2addr v4, v10

    iput v4, p0, Lsoftware/simplicial/a/bf;->s:F

    .line 583
    add-float v4, v3, v2

    div-float/2addr v4, v10

    iput v4, p0, Lsoftware/simplicial/a/bf;->t:F

    .line 585
    sub-float v0, v1, v0

    .line 586
    sub-float v1, v3, v2

    .line 587
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/bf;->a(FF)F

    move-result p1

    .line 604
    :goto_2
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bE:Z

    if-nez v0, :cond_5

    .line 606
    iget v0, p0, Lsoftware/simplicial/a/bf;->a:F

    iget v1, p0, Lsoftware/simplicial/a/bf;->a:F

    sub-float v1, p1, v1

    div-float/2addr v1, v10

    add-float p1, v0, v1

    .line 607
    iget v0, p0, Lsoftware/simplicial/a/bf;->a:F

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3d4ccccd    # 0.05f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 608
    iput-boolean v11, p0, Lsoftware/simplicial/a/bf;->bE:Z

    .line 611
    :cond_5
    iput p1, p0, Lsoftware/simplicial/a/bf;->a:F

    .line 612
    return p1

    .line 591
    :cond_6
    iget v0, p0, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_7

    iget v0, p0, Lsoftware/simplicial/a/bf;->bB:I

    if-nez v0, :cond_7

    .line 593
    iput-boolean v11, p0, Lsoftware/simplicial/a/bf;->bE:Z

    .line 594
    div-float v0, p1, v10

    iput v0, p0, Lsoftware/simplicial/a/bf;->s:F

    .line 595
    div-float v0, p1, v10

    iput v0, p0, Lsoftware/simplicial/a/bf;->t:F

    goto :goto_2

    .line 600
    :cond_7
    iget p1, p0, Lsoftware/simplicial/a/bf;->a:F

    goto :goto_2

    :cond_8
    move v12, v3

    move v3, v0

    move v0, v12

    goto/16 :goto_1
.end method

.method public a(FZ)F
    .locals 7

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 657
    iget v0, p0, Lsoftware/simplicial/a/bf;->S:I

    if-nez v0, :cond_1

    .line 659
    iget v0, p0, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/a/bf;->bB:I

    if-nez v0, :cond_0

    .line 661
    div-float v0, p1, v2

    iput v0, p0, Lsoftware/simplicial/a/bf;->l:F

    .line 662
    div-float v0, p1, v2

    iput v0, p0, Lsoftware/simplicial/a/bf;->m:F

    .line 663
    iget v0, p0, Lsoftware/simplicial/a/bf;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bf;->s:F

    .line 664
    iget v0, p0, Lsoftware/simplicial/a/bf;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bf;->t:F

    .line 712
    :goto_0
    return p1

    .line 669
    :cond_0
    iget p1, p0, Lsoftware/simplicial/a/bf;->a:F

    goto :goto_0

    .line 677
    :cond_1
    const/4 v0, 0x0

    move v2, v1

    move v3, p1

    :goto_1
    iget v4, p0, Lsoftware/simplicial/a/bf;->bF:I

    if-ge v0, v4, :cond_b

    .line 679
    iget-object v4, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v4, v4, v0

    .line 680
    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 677
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 683
    :cond_3
    if-eqz p2, :cond_7

    .line 685
    iget v5, v4, Lsoftware/simplicial/a/h;->s:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v3

    if-gez v5, :cond_4

    .line 686
    iget v3, v4, Lsoftware/simplicial/a/h;->s:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v5

    sub-float/2addr v3, v5

    .line 687
    :cond_4
    iget v5, v4, Lsoftware/simplicial/a/h;->s:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    add-float/2addr v5, v6

    cmpl-float v5, v5, v2

    if-lez v5, :cond_5

    .line 688
    iget v2, v4, Lsoftware/simplicial/a/h;->s:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v5

    add-float/2addr v2, v5

    .line 689
    :cond_5
    iget v5, v4, Lsoftware/simplicial/a/h;->t:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    cmpg-float v5, v5, p1

    if-gez v5, :cond_6

    .line 690
    iget v5, v4, Lsoftware/simplicial/a/h;->t:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float p1, v5, v6

    .line 691
    :cond_6
    iget v5, v4, Lsoftware/simplicial/a/h;->t:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    add-float/2addr v5, v6

    cmpl-float v5, v5, v1

    if-lez v5, :cond_2

    .line 692
    iget v1, v4, Lsoftware/simplicial/a/h;->t:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v4

    add-float/2addr v1, v4

    goto :goto_2

    .line 696
    :cond_7
    iget v5, v4, Lsoftware/simplicial/a/h;->l:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v3

    if-gez v5, :cond_8

    .line 697
    iget v3, v4, Lsoftware/simplicial/a/h;->l:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v5

    sub-float/2addr v3, v5

    .line 698
    :cond_8
    iget v5, v4, Lsoftware/simplicial/a/h;->l:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    add-float/2addr v5, v6

    cmpl-float v5, v5, v2

    if-lez v5, :cond_9

    .line 699
    iget v2, v4, Lsoftware/simplicial/a/h;->l:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v5

    add-float/2addr v2, v5

    .line 700
    :cond_9
    iget v5, v4, Lsoftware/simplicial/a/h;->m:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    cmpg-float v5, v5, p1

    if-gez v5, :cond_a

    .line 701
    iget v5, v4, Lsoftware/simplicial/a/h;->m:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    sub-float p1, v5, v6

    .line 702
    :cond_a
    iget v5, v4, Lsoftware/simplicial/a/h;->m:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v6

    add-float/2addr v5, v6

    cmpl-float v5, v5, v1

    if-lez v5, :cond_2

    .line 703
    iget v1, v4, Lsoftware/simplicial/a/h;->m:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/h;->q_()F

    move-result v4

    add-float/2addr v1, v4

    goto/16 :goto_2

    .line 707
    :cond_b
    sub-float v0, v2, v3

    .line 708
    sub-float/2addr v1, p1

    .line 709
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/bf;->a(FF)F

    move-result p1

    .line 711
    iput p1, p0, Lsoftware/simplicial/a/bf;->a:F

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bf;)I
    .locals 4

    .prologue
    .line 455
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v0, :cond_1

    .line 457
    iget-byte v0, p0, Lsoftware/simplicial/a/bf;->R:B

    iget-byte v1, p1, Lsoftware/simplicial/a/bf;->R:B

    if-eq v0, v1, :cond_0

    .line 458
    iget-byte v0, p1, Lsoftware/simplicial/a/bf;->R:B

    iget-byte v1, p0, Lsoftware/simplicial/a/bf;->R:B

    sub-int/2addr v0, v1

    .line 469
    :goto_0
    return v0

    .line 459
    :cond_0
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->d()I

    move-result v0

    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->d()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 461
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v0, :cond_2

    .line 462
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 463
    :cond_2
    iget-boolean v0, p1, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v0, :cond_3

    .line 464
    const v0, 0x7fffffff

    goto :goto_0

    .line 465
    :cond_3
    iget-wide v0, p0, Lsoftware/simplicial/a/bf;->aE:J

    iget-wide v2, p1, Lsoftware/simplicial/a/bf;->aE:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 466
    const/4 v0, 0x1

    goto :goto_0

    .line 467
    :cond_4
    iget-wide v0, p0, Lsoftware/simplicial/a/bf;->aE:J

    iget-wide v2, p1, Lsoftware/simplicial/a/bf;->aE:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    .line 468
    const/4 v0, -0x1

    goto :goto_0

    .line 469
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 724
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(IJLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZZZ)V
    .locals 8

    .prologue
    .line 1648
    iput p1, p0, Lsoftware/simplicial/a/bf;->A:I

    .line 1649
    iput-wide p2, p0, Lsoftware/simplicial/a/bf;->B:J

    .line 1650
    if-eqz p9, :cond_0

    .line 1652
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    .line 1653
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->I:[B

    .line 1654
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    .line 1657
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    .line 1658
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    .line 1659
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1660
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bq:Z

    .line 1662
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->an:I

    .line 1663
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/a/bf;->ap:J

    .line 1664
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bp:Z

    .line 1666
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 1667
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/a/bf;->aq:J

    .line 1669
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1670
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1671
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1672
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1673
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1674
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->G:Z

    .line 1675
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->aw:Z

    .line 1677
    if-eqz p10, :cond_1

    .line 1679
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    invoke-virtual {v0}, Lsoftware/simplicial/a/az;->a()V

    .line 1680
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    move v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move/from16 v6, p8

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    .line 1681
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lsoftware/simplicial/a/bf;->aK:J

    .line 1683
    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->v()V

    .line 1686
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    .line 1687
    return-void
.end method

.method public a(ILsoftware/simplicial/a/am;)V
    .locals 6

    .prologue
    .line 1288
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_1

    .line 1305
    :cond_0
    :goto_0
    return-void

    .line 1291
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_0

    .line 1294
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/2addr v1, p1

    iput v1, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1295
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->F:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->F:I

    .line 1297
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1298
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->F:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->F:I

    .line 1301
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_2

    .line 1302
    div-int/lit8 p1, p1, 0x2

    .line 1304
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lsoftware/simplicial/a/bf;->a(IZ)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 1248
    iget v0, p0, Lsoftware/simplicial/a/bf;->bc:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->bc:I

    .line 1250
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1258
    :goto_0
    return-void

    .line 1253
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->w:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->w:I

    .line 1255
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->r:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->r:I

    .line 1257
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->W:J

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/am;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 763
    iget v0, p0, Lsoftware/simplicial/a/bf;->ax:I

    if-lez v0, :cond_0

    .line 765
    iget v0, p0, Lsoftware/simplicial/a/bf;->ax:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->ax:I

    .line 766
    iget v0, p0, Lsoftware/simplicial/a/bf;->ax:I

    if-gtz v0, :cond_0

    .line 768
    iput v2, p0, Lsoftware/simplicial/a/bf;->ax:I

    .line 769
    iget-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v0, v0, -0x2

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    .line 772
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 773
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    .line 775
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/a/ad;

    .line 776
    invoke-virtual {v1}, Lsoftware/simplicial/a/ad;->a()V

    .line 777
    iget v1, v1, Lsoftware/simplicial/a/ad;->c:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_1

    .line 778
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 780
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    .line 781
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 782
    :cond_3
    iget v0, p0, Lsoftware/simplicial/a/bf;->aU:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    .line 784
    iget v0, p0, Lsoftware/simplicial/a/bf;->aU:F

    const v1, 0x3b23d70a    # 0.0025f

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bf;->aU:F

    .line 785
    iget v0, p0, Lsoftware/simplicial/a/bf;->aU:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_4

    .line 786
    iput v5, p0, Lsoftware/simplicial/a/bf;->aU:F

    :cond_4
    move v0, v2

    .line 788
    :goto_2
    iget v1, p0, Lsoftware/simplicial/a/bf;->S:I

    if-ge v0, v1, :cond_5

    .line 790
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v1, v1, v0

    .line 791
    invoke-virtual {v1}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 796
    :cond_5
    return-void

    .line 793
    :cond_6
    iget-byte v1, v1, Lsoftware/simplicial/a/bh;->H:B

    const/4 v2, -0x1

    if-gt v1, v2, :cond_7

    sget-object v1, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-eq p1, v1, :cond_7

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p1, v1, :cond_8

    .line 794
    :cond_7
    const/4 v1, 0x0

    iput v1, p0, Lsoftware/simplicial/a/bf;->T:F

    .line 788
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public a(Lsoftware/simplicial/a/am;I)V
    .locals 2

    .prologue
    .line 1558
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1571
    :cond_0
    :goto_0
    return-void

    .line 1561
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    add-int/2addr v0, p2

    iput v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    .line 1563
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    sget-object v1, Lsoftware/simplicial/a/d;->ap:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ap:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1564
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ap:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1566
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_3

    iget v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    sget-object v1, Lsoftware/simplicial/a/d;->X:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->X:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1567
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->X:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1569
    :cond_3
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    sget-object v1, Lsoftware/simplicial/a/d;->ad:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ad:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1570
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ad:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;JLsoftware/simplicial/a/i/a;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1596
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1643
    :cond_0
    :goto_0
    return-void

    .line 1599
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-nez v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p2, v0, :cond_2

    .line 1601
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->l:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v0, v0, Lsoftware/simplicial/a/bx;->e:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v1, v1, Lsoftware/simplicial/a/aa;->s:I

    if-lt v0, v1, :cond_2

    .line 1602
    iput-boolean v4, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1605
    :cond_2
    if-eqz p5, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bq:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1606
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bq:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1608
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->B:J

    sub-long v0, p3, v0

    sget-object v2, Lsoftware/simplicial/a/d;->I:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->I:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1609
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->I:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1611
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->V:J

    sub-long v0, p3, v0

    sget-object v2, Lsoftware/simplicial/a/d;->G:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_5

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->G:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1612
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->G:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1614
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->B:J

    sub-long v0, p3, v0

    sget-object v2, Lsoftware/simplicial/a/d;->J:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->F:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->J:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1616
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->J:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1618
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->B:J

    sub-long v0, p3, v0

    sget-object v2, Lsoftware/simplicial/a/d;->K:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->A:I

    const/16 v1, 0x1388

    if-lt v0, v1, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->K:Lsoftware/simplicial/a/d;

    .line 1619
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1620
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->K:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1622
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->W:J

    sub-long v0, p3, v0

    sget-object v2, Lsoftware/simplicial/a/d;->H:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->H:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1623
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->H:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1625
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 1627
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-object v2, v2, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1629
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1630
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-object v2, v2, Lsoftware/simplicial/a/ay;->S:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1632
    iget v0, v0, Lsoftware/simplicial/a/d;->by:I

    invoke-direct {p0, v0, v5}, Lsoftware/simplicial/a/bf;->a(IZ)V

    goto :goto_1

    .line 1636
    :cond_a
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_0

    .line 1638
    iput-boolean v4, p0, Lsoftware/simplicial/a/bf;->az:Z

    .line 1639
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-boolean v4, v0, Lsoftware/simplicial/a/ay;->R:Z

    .line 1641
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v0, v0, Lsoftware/simplicial/a/aa;->t:I

    invoke-direct {p0, v0, v5}, Lsoftware/simplicial/a/bf;->a(IZ)V

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 353
    iget-boolean v1, p0, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v1, :cond_1

    if-eqz p3, :cond_1

    .line 355
    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq p1, v1, :cond_0

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-eq p1, v1, :cond_0

    .line 356
    iput v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    if-eqz p3, :cond_2

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq p1, v1, :cond_6

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-eq p1, v1, :cond_6

    .line 362
    :cond_2
    iput v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    .line 363
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 365
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 363
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 368
    :cond_3
    if-eqz p3, :cond_5

    iget-object v4, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v4, v4, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v4, :cond_5

    .line 370
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    .line 371
    iget-object v5, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v5, v5, Lsoftware/simplicial/a/ay;->z:I

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_4

    .line 372
    iget-object v5, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    float-to-int v6, v4

    iput v6, v5, Lsoftware/simplicial/a/ay;->z:I

    .line 373
    :cond_4
    iget-object v5, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v5, v5, Lsoftware/simplicial/a/az;->u:I

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_5

    .line 374
    iget-object v5, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    float-to-int v4, v4

    iput v4, v5, Lsoftware/simplicial/a/az;->u:I

    .line 377
    :cond_5
    ## Insta Recombine (only works for your player)
    sget-object v4, Lsoftware/simplicial/a/bs;->engineInstance:Lsoftware/simplicial/a/bs;
    
    iget-object v4, v4, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;
    
    invoke-static {v4}, Ljava/util/Arrays;->asList(Ljava/lang/Object;)Ljava/lang/List;
    
    move-result-object v4
    
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    
    move-result-object v4
    
    :loop_players
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z
    
    move-result v5
    
    if-eqz v5, :break
    
    invoke-virtual {v4}, Ljava/util/Iterator;->next()Ljava/util/Iterator;
    
    move-result-object v5
    
    instance-of v6, v5, Lsoftware/simplicial/a/i;
    
    if-nez v6, :default
    
    const/4 v6, 0x0
    
    goto :set
    
    :default
    check-cast v5, Lsoftware/simplicial/a/bf;
    
    iget v5, p0, Lsoftware/simplicial/a/bf;->Q:I

    int-to-float v5, v5

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->d()F

    move-result v6

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->c()F

    move-result v3

    invoke-static {v6, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    add-float/2addr v3, v5

    float-to-int v3, v3
    
    :set
    iput v3, p0, Lsoftware/simplicial/a/bf;->Q:I
    
    goto :loop_players

    :break
    goto :goto_2

    .line 381
    :cond_6
    if-eqz p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_0

    .line 383
    iget v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v1, Lsoftware/simplicial/a/ay;->A:I

    if-le v0, v1, :cond_7

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, p0, Lsoftware/simplicial/a/bf;->Q:I

    iput v1, v0, Lsoftware/simplicial/a/ay;->A:I

    .line 385
    :cond_7
    iget v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v1, Lsoftware/simplicial/a/az;->v:I

    if-le v0, v1, :cond_8

    .line 386
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, p0, Lsoftware/simplicial/a/bf;->Q:I

    iput v1, v0, Lsoftware/simplicial/a/az;->v:I

    .line 388
    :cond_8
    sget-object v0, Lsoftware/simplicial/a/d;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 389
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->v:I

    iget v3, v0, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_9

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 390
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 392
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->A:I

    sget-object v1, Lsoftware/simplicial/a/d;->E:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->w:I

    if-nez v0, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->E:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 393
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->E:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 395
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->A:I

    sget-object v1, Lsoftware/simplicial/a/d;->F:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_c

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->D:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->E:I

    if-nez v0, :cond_c

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->F:Lsoftware/simplicial/a/d;

    .line 396
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 397
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->F:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_c
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-nez v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p2, v0, :cond_0

    .line 401
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->j:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->A:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v1, v1, Lsoftware/simplicial/a/aa;->s:I

    if-lt v0, v1, :cond_d

    .line 402
    iput-boolean v7, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 404
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->k:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->A:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v1, v1, Lsoftware/simplicial/a/aa;->s:I

    if-lt v0, v1, :cond_0

    .line 405
    iput-boolean v7, p0, Lsoftware/simplicial/a/bf;->bt:Z

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V
    .locals 6

    .prologue
    .line 1322
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v1, v1, Lsoftware/simplicial/a/az;->M:Z

    if-nez v1, :cond_1

    .line 1509
    :cond_0
    :goto_0
    return-void

    .line 1325
    :cond_1
    if-nez p2, :cond_7

    .line 1327
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lsoftware/simplicial/a/ay;->M:Z

    .line 1328
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v1, Lsoftware/simplicial/a/az;->z:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/az;->z:I

    .line 1329
    if-eqz p8, :cond_2

    iget-boolean v1, p8, Lsoftware/simplicial/a/b/a;->c:Z

    if-eqz v1, :cond_3

    :cond_2
    if-eqz p11, :cond_4

    move-object/from16 v0, p11

    iget-boolean v1, v0, Lsoftware/simplicial/a/h/c;->d:Z

    if-nez v1, :cond_4

    .line 1331
    :cond_3
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lsoftware/simplicial/a/ay;->N:Z

    .line 1332
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v1, Lsoftware/simplicial/a/az;->A:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/az;->A:I

    .line 1334
    :cond_4
    if-eqz p7, :cond_5

    .line 1336
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lsoftware/simplicial/a/ay;->O:Z

    .line 1337
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v1, Lsoftware/simplicial/a/az;->B:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/az;->B:I

    .line 1339
    :cond_5
    if-eqz p9, :cond_6

    .line 1341
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lsoftware/simplicial/a/ay;->P:Z

    .line 1342
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v1, Lsoftware/simplicial/a/az;->C:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/az;->C:I

    .line 1345
    :cond_6
    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-ne p1, v1, :cond_7

    .line 1347
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v1, Lsoftware/simplicial/a/ay;->u:I

    iput v1, p0, Lsoftware/simplicial/a/bf;->bA:I

    .line 1348
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v2, v1, Lsoftware/simplicial/a/ay;->u:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, v1, Lsoftware/simplicial/a/ay;->u:I

    .line 1352
    :cond_7
    if-gez p3, :cond_24

    .line 1353
    const/4 v1, 0x0

    .line 1354
    :goto_1
    const/16 v2, 0x258

    if-le v1, v2, :cond_8

    .line 1355
    const/16 v1, 0x258

    .line 1357
    :cond_8
    const/4 v2, 0x0

    .line 1358
    sget-object v3, Lsoftware/simplicial/a/bf$1;->e:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1380
    :cond_9
    :goto_2
    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, p10

    int-to-float v4, v0

    const/high16 v5, 0x41a80000    # 21.0f

    div-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1381
    if-eqz p2, :cond_a

    .line 1382
    div-int/lit8 v2, v2, 0x2

    .line 1383
    :cond_a
    sget-object v3, Lsoftware/simplicial/a/aq;->b:Lsoftware/simplicial/a/aq;

    if-ne p6, v3, :cond_b

    .line 1384
    div-int/lit8 v2, v2, 0x2

    .line 1386
    :cond_b
    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1388
    iget-boolean v2, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v2, :cond_15

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v2, v2, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v2, :cond_15

    iget-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-nez v2, :cond_15

    .line 1390
    sget-object v2, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p6, v2, :cond_14

    .line 1392
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->b:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    sget-object v2, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_c

    .line 1393
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1394
    :cond_c
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->e:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    sget-object v2, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_d

    .line 1395
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1396
    :cond_d
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->f:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    sget-object v2, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_e

    .line 1397
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1398
    :cond_e
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->g:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_f

    .line 1399
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1400
    :cond_f
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->i:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_10

    .line 1401
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1402
    :cond_10
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->c:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    sget-object v2, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_11

    .line 1403
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1404
    :cond_11
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->h:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    sget-object v2, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_12

    .line 1405
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1406
    :cond_12
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->d:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_13

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->c:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_13

    .line 1407
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1408
    :cond_13
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->m:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_14

    .line 1409
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1411
    :cond_14
    if-eqz p8, :cond_15

    iget-boolean v2, p8, Lsoftware/simplicial/a/b/a;->c:Z

    if-nez v2, :cond_15

    .line 1412
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v3, Lsoftware/simplicial/a/aa;->p:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1413
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 1416
    :cond_15
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->z:I

    sget-object v3, Lsoftware/simplicial/a/d;->N:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_16

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->N:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 1417
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->N:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1419
    :cond_16
    if-eqz p7, :cond_1a

    if-nez p2, :cond_1a

    .line 1421
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->B:I

    sget-object v3, Lsoftware/simplicial/a/d;->bf:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_17

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bf:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 1422
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bf:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1424
    :cond_17
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->B:I

    sget-object v3, Lsoftware/simplicial/a/d;->bg:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_18

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bg:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 1425
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bg:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1427
    :cond_18
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->B:I

    sget-object v3, Lsoftware/simplicial/a/d;->bh:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_19

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bh:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    .line 1428
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bh:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1430
    :cond_19
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->B:I

    sget-object v3, Lsoftware/simplicial/a/d;->bi:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_1a

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bi:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 1431
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bi:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1434
    :cond_1a
    if-eqz p9, :cond_1b

    if-nez p2, :cond_1b

    .line 1436
    iget v2, p9, Lsoftware/simplicial/a/i/a;->j:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1b

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->br:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1437
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->br:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1440
    :cond_1b
    if-eqz p8, :cond_20

    if-nez p2, :cond_20

    .line 1442
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->A:I

    sget-object v3, Lsoftware/simplicial/a/d;->bk:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_1c

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bk:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1443
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bk:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1445
    :cond_1c
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->A:I

    sget-object v3, Lsoftware/simplicial/a/d;->bl:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_1d

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bl:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1446
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bl:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1448
    :cond_1d
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->A:I

    sget-object v3, Lsoftware/simplicial/a/d;->bm:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_1e

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bm:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 1449
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bm:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1451
    :cond_1e
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->A:I

    sget-object v3, Lsoftware/simplicial/a/d;->bn:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_1f

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bn:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 1452
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bn:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1454
    :cond_1f
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->A:I

    sget-object v3, Lsoftware/simplicial/a/d;->bo:Lsoftware/simplicial/a/d;

    iget v3, v3, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_20

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bo:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    .line 1455
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->bo:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1458
    :cond_20
    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_21

    if-nez p2, :cond_21

    .line 1460
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->c:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_23

    sget-object v2, Lsoftware/simplicial/a/d;->am:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    move/from16 v0, p12

    if-gt v0, v2, :cond_23

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->am:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 1461
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->am:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1466
    :cond_21
    :goto_3
    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-ne p1, v2, :cond_22

    if-nez p2, :cond_22

    .line 1467
    sget-object v2, Lsoftware/simplicial/a/d;->Z:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    if-gt v1, v2, :cond_22

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->Z:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 1468
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->Z:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1470
    :cond_22
    if-ltz p5, :cond_0

    .line 1472
    sget-object v1, Lsoftware/simplicial/a/bf$1;->e:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :pswitch_0
    goto/16 :goto_0

    .line 1475
    :pswitch_1
    sget-object v1, Lsoftware/simplicial/a/d;->S:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->S:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1476
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->S:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1365
    :pswitch_2
    mul-int/lit16 v2, v1, 0x4e20

    div-int/lit16 v2, v2, 0x12c

    goto/16 :goto_2

    .line 1375
    :pswitch_3
    mul-int/lit16 v2, v1, 0x2710

    div-int/lit16 v2, v2, 0x12c

    .line 1376
    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne p1, v3, :cond_9

    iget-object v3, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->c:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 1377
    mul-int/lit8 v2, v2, 0x2

    goto/16 :goto_2

    .line 1462
    :cond_23
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->c:I

    if-nez v2, :cond_21

    sget-object v2, Lsoftware/simplicial/a/d;->al:Lsoftware/simplicial/a/d;

    iget v2, v2, Lsoftware/simplicial/a/d;->bx:I

    if-gt v1, v2, :cond_21

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->al:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 1463
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v3, Lsoftware/simplicial/a/d;->al:Lsoftware/simplicial/a/d;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1479
    :pswitch_4
    sget-object v1, Lsoftware/simplicial/a/d;->U:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->U:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1480
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->U:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1483
    :pswitch_5
    sget-object v1, Lsoftware/simplicial/a/d;->ai:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    move/from16 v0, p13

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ai:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1484
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ai:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1487
    :pswitch_6
    sget-object v1, Lsoftware/simplicial/a/d;->W:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->W:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1488
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->W:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1491
    :pswitch_7
    sget-object v1, Lsoftware/simplicial/a/d;->ac:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ac:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1492
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ac:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1495
    :pswitch_8
    sget-object v1, Lsoftware/simplicial/a/d;->aa:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-gt p5, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->aa:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1496
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->aa:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1499
    :pswitch_9
    sget-object v1, Lsoftware/simplicial/a/d;->ao:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ao:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1500
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->ao:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1503
    :pswitch_a
    sget-object v1, Lsoftware/simplicial/a/d;->af:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    add-int/2addr v1, p5

    if-lt p4, v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->af:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1504
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->af:Lsoftware/simplicial/a/d;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_24
    move v1, p3

    goto/16 :goto_1

    .line 1358
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 1472
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bf;)V
    .locals 8

    .prologue
    const/16 v0, 0x26

    .line 504
    .line 505
    sget-object v1, Lsoftware/simplicial/a/bf$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 520
    :goto_0
    :pswitch_0
    const/4 v2, 0x0

    .line 521
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v4, v3

    const/4 v1, 0x0

    move v7, v1

    move v1, v2

    move v2, v7

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 523
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 521
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 508
    :pswitch_1
    const/16 v0, 0x1b

    .line 509
    goto :goto_0

    .line 511
    :pswitch_2
    const/16 v0, 0x21

    .line 512
    goto :goto_0

    .line 517
    :pswitch_3
    const/16 v0, 0x2a

    goto :goto_0

    .line 526
    :cond_0
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->d()F

    move-result v6

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->c()F

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    add-float/2addr v1, v5

    goto :goto_2

    .line 528
    :cond_1
    float-to-double v2, v1

    invoke-static {v2, v3}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v2

    const-wide v4, 0x400e666660000000L    # 3.799999952316284

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    div-double/2addr v2, v4

    int-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/bf;->T:F

    .line 529
    if-ne p0, p2, :cond_2

    .line 530
    iget v0, p0, Lsoftware/simplicial/a/bf;->T:F

    const v1, 0x3e4ccccd    # 0.2f

    mul-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bf;->T:F

    .line 531
    :cond_2
    return-void

    .line 505
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/am;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 800
    iget v0, p0, Lsoftware/simplicial/a/bf;->aZ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->aZ:I

    .line 802
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_1

    .line 829
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->h:I

    .line 809
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 811
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->c:I

    .line 812
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    .line 814
    const/4 v0, 0x5

    .line 815
    sget-object v1, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne p2, v1, :cond_2

    .line 816
    const/4 v0, 0x2

    .line 818
    :cond_2
    invoke-direct {p0, v0, v6}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 820
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_3

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->n:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 821
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->h:I

    iput v0, p0, Lsoftware/simplicial/a/bf;->br:I

    .line 823
    :cond_3
    sget-object v0, Lsoftware/simplicial/a/d;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 824
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->c:I

    iget v3, v0, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_4

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 825
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 827
    :cond_5
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-nez v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->n:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->h:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v1, v1, Lsoftware/simplicial/a/aa;->s:I

    if-lt v0, v1, :cond_0

    .line 828
    iput-boolean v6, p0, Lsoftware/simplicial/a/bf;->bt:Z

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;JLsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/bj;)V
    .locals 7

    .prologue
    const/16 v2, 0x7d0

    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 1173
    iget v0, p0, Lsoftware/simplicial/a/bf;->bd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->bd:I

    .line 1175
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1244
    :cond_0
    :goto_0
    return-void

    .line 1178
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ad;

    .line 1179
    if-nez v0, :cond_2

    .line 1181
    new-instance v0, Lsoftware/simplicial/a/ad;

    invoke-direct {v0, v6}, Lsoftware/simplicial/a/ad;-><init>(I)V

    .line 1182
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1185
    :cond_2
    iget v1, v0, Lsoftware/simplicial/a/ad;->c:F

    .line 1186
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    div-float v1, v4, v1

    float-to-int v1, v1

    .line 1187
    instance-of v4, p1, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_3

    sget-object v4, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne p7, v4, :cond_3

    .line 1189
    sget-object v4, Lsoftware/simplicial/a/bf$1;->c:[I

    invoke-virtual {p6}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1202
    :cond_3
    :goto_1
    int-to-float v1, v1

    iget v4, p0, Lsoftware/simplicial/a/bf;->aU:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 1203
    iget v4, p0, Lsoftware/simplicial/a/bf;->aV:I

    if-lez v4, :cond_4

    .line 1206
    iget v1, p0, Lsoftware/simplicial/a/bf;->aV:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lsoftware/simplicial/a/bf;->aV:I

    move v1, v3

    .line 1209
    :cond_4
    if-le v1, v2, :cond_5

    move v1, v2

    .line 1211
    :cond_5
    invoke-direct {p0, v1, v6}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1212
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/ad;->a(F)V

    .line 1214
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->q:I

    .line 1215
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    long-to-float v1, v4

    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v2

    add-float/2addr v1, v2

    float-to-long v4, v1

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1217
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->v:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->v:I

    .line 1218
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->x:I

    int-to-float v1, v1

    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1220
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->T:J

    sub-long v0, p3, v0

    const-wide/16 v4, 0x1388

    cmp-long v0, v0, v4

    if-ltz v0, :cond_6

    .line 1221
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput v3, v0, Lsoftware/simplicial/a/ay;->U:I

    .line 1223
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->U:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->U:I

    .line 1224
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p3, v0, Lsoftware/simplicial/a/ay;->T:J

    .line 1226
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v1, Lsoftware/simplicial/a/e;->l:Lsoftware/simplicial/a/e;

    if-eq v0, v1, :cond_7

    iget-object v0, p1, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    sget-object v1, Lsoftware/simplicial/a/e;->r:Lsoftware/simplicial/a/e;

    if-ne v0, v1, :cond_8

    .line 1228
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->X:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->X:I

    .line 1230
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->X:I

    sget-object v1, Lsoftware/simplicial/a/d;->M:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->M:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1231
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->M:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1233
    :cond_8
    sget-object v0, Lsoftware/simplicial/a/d;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 1234
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v2, v2, Lsoftware/simplicial/a/ay;->U:I

    iget v3, v0, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_9

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1235
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1192
    :pswitch_0
    div-int/lit8 v1, v1, 0x2

    goto/16 :goto_1

    .line 1195
    :pswitch_1
    div-int/lit8 v1, v1, 0x4

    goto/16 :goto_1

    .line 1198
    :pswitch_2
    div-int/lit8 v1, v1, 0x6

    goto/16 :goto_1

    .line 1236
    :cond_a
    sget-object v0, Lsoftware/simplicial/a/d;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 1237
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v2, v2, Lsoftware/simplicial/a/ay;->v:I

    iget v3, v0, Lsoftware/simplicial/a/d;->bx:I

    if-lt v2, v3, :cond_b

    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1238
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1240
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_d

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p5, v0, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->o:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1241
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->v:I

    iput v0, p0, Lsoftware/simplicial/a/bf;->br:I

    .line 1242
    :cond_d
    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->az:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v0, v0, Lsoftware/simplicial/a/ay;->R:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    if-nez v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne p5, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    sget-object v1, Lsoftware/simplicial/a/aa;->o:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->v:I

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget v1, v1, Lsoftware/simplicial/a/aa;->s:I

    if-lt v0, v1, :cond_0

    .line 1243
    iput-boolean v6, p0, Lsoftware/simplicial/a/bf;->bt:Z

    goto/16 :goto_0

    .line 1189
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/bq;)V
    .locals 4

    .prologue
    .line 1309
    iget v0, p0, Lsoftware/simplicial/a/bf;->be:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->be:I

    .line 1311
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1317
    :goto_0
    return-void

    .line 1314
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->x:I

    int-to-float v1, v1

    invoke-virtual {p1}, Lsoftware/simplicial/a/bq;->d()F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1316
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    long-to-float v1, v2

    invoke-virtual {p1}, Lsoftware/simplicial/a/bq;->d()F

    move-result v2

    add-float/2addr v1, v2

    float-to-long v2, v1

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->s:J

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bv;Lsoftware/simplicial/a/am;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3

    const/4 v1, 0x1

    .line 875
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_1

    .line 1169
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 878
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_2

    .line 879
    iget v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    .line 881
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/ai;->Z:Lsoftware/simplicial/a/bp;

    sget-object v2, Lsoftware/simplicial/a/bp;->d:Lsoftware/simplicial/a/bp;

    if-ne v0, v2, :cond_6

    const/4 v0, 0x2

    .line 883
    :goto_1
    sget-object v2, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    if-ne p1, v2, :cond_7

    .line 884
    iget v2, p0, Lsoftware/simplicial/a/bf;->bb:I

    add-int/2addr v2, v0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bb:I

    .line 890
    :goto_2
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v2, v2, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v2, :cond_0

    .line 893
    const/16 v2, 0xf

    .line 894
    sget-object v3, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne p2, v3, :cond_3

    .line 895
    const/4 v2, 0x7

    .line 897
    :cond_3
    sget-object v3, Lsoftware/simplicial/a/bf$1;->b:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 900
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->i:I

    .line 901
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 903
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->d:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->d:I

    .line 904
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 906
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 908
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->d:I

    sget-object v1, Lsoftware/simplicial/a/d;->ar:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ar:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 909
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ar:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 911
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->d:I

    sget-object v1, Lsoftware/simplicial/a/d;->as:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->as:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 912
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->as:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 914
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->d:I

    sget-object v1, Lsoftware/simplicial/a/d;->at:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->at:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 915
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->at:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 881
    goto/16 :goto_1

    .line 885
    :cond_7
    sget-object v2, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    if-ne p1, v2, :cond_8

    .line 886
    iget v2, p0, Lsoftware/simplicial/a/bf;->aZ:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/bf;->aZ:I

    goto/16 :goto_2

    .line 888
    :cond_8
    iget v2, p0, Lsoftware/simplicial/a/bf;->ba:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/bf;->ba:I

    goto/16 :goto_2

    .line 918
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->j:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->j:I

    .line 919
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 921
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->e:I

    .line 922
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 924
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 926
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->e:I

    sget-object v1, Lsoftware/simplicial/a/d;->au:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->au:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 927
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->au:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 929
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->e:I

    sget-object v1, Lsoftware/simplicial/a/d;->av:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->av:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 930
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->av:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 932
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->e:I

    sget-object v1, Lsoftware/simplicial/a/d;->aw:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aw:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 933
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aw:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 936
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->k:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->k:I

    .line 937
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 939
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->f:I

    .line 940
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 942
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 944
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->f:I

    sget-object v1, Lsoftware/simplicial/a/d;->ax:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ax:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 945
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ax:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 947
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->f:I

    sget-object v1, Lsoftware/simplicial/a/d;->ay:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_c

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ay:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 948
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ay:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 950
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->f:I

    sget-object v1, Lsoftware/simplicial/a/d;->az:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->az:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 951
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->az:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 954
    :pswitch_4
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v4, v3, Lsoftware/simplicial/a/ay;->u:I

    add-int/2addr v4, v0

    iput v4, v3, Lsoftware/simplicial/a/ay;->u:I

    .line 955
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v4, v3, Lsoftware/simplicial/a/ay;->x:I

    mul-int/lit8 v5, v0, 0x3

    add-int/2addr v4, v5

    iput v4, v3, Lsoftware/simplicial/a/ay;->x:I

    .line 957
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v4, v3, Lsoftware/simplicial/a/az;->p:I

    add-int/2addr v4, v0

    iput v4, v3, Lsoftware/simplicial/a/az;->p:I

    .line 958
    iget-object v3, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v3, Lsoftware/simplicial/a/az;->s:J

    mul-int/lit8 v6, v0, 0x3

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, v3, Lsoftware/simplicial/a/az;->s:J

    .line 960
    mul-int/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 962
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 963
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_e

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 966
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 968
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 969
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 974
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->l:I

    .line 975
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 977
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->g:I

    .line 978
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 980
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 982
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->g:I

    sget-object v1, Lsoftware/simplicial/a/d;->aD:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_f

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aD:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 983
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aD:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 985
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->g:I

    sget-object v1, Lsoftware/simplicial/a/d;->aE:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_10

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aE:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 986
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aE:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 988
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->g:I

    sget-object v1, Lsoftware/simplicial/a/d;->aF:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aF:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 989
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aF:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 993
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->m:I

    .line 994
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 996
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->h:I

    .line 997
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 999
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1001
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->h:I

    sget-object v1, Lsoftware/simplicial/a/d;->aG:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_11

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aG:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1002
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aG:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1004
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->h:I

    sget-object v1, Lsoftware/simplicial/a/d;->aH:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_12

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aH:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 1005
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aH:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1007
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->h:I

    sget-object v1, Lsoftware/simplicial/a/d;->aI:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aI:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1008
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aI:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1011
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->n:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->n:I

    .line 1012
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1014
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->i:I

    .line 1015
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1017
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1019
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->i:I

    sget-object v1, Lsoftware/simplicial/a/d;->aJ:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_13

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aJ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1020
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aJ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1022
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->i:I

    sget-object v1, Lsoftware/simplicial/a/d;->aK:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_14

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aK:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1023
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aK:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1025
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->i:I

    sget-object v1, Lsoftware/simplicial/a/d;->aL:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aL:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1026
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aL:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1029
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->o:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->o:I

    .line 1030
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1032
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->j:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->j:I

    .line 1033
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1035
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1037
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->j:I

    sget-object v1, Lsoftware/simplicial/a/d;->aM:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_15

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aM:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 1038
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aM:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1040
    :cond_15
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->j:I

    sget-object v1, Lsoftware/simplicial/a/d;->aN:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aN:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 1041
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aN:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1043
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->j:I

    sget-object v1, Lsoftware/simplicial/a/d;->aO:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aO:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aO:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1047
    :pswitch_9
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->p:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->p:I

    .line 1048
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1050
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->k:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->k:I

    .line 1051
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1053
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1055
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->k:I

    sget-object v1, Lsoftware/simplicial/a/d;->aP:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_17

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aP:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1056
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aP:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1058
    :cond_17
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->k:I

    sget-object v1, Lsoftware/simplicial/a/d;->aQ:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_18

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aQ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 1059
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aQ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1061
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->k:I

    sget-object v1, Lsoftware/simplicial/a/d;->aR:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aR:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1062
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aR:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1065
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->u:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->u:I

    .line 1066
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->p:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->p:I

    .line 1068
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1a

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1069
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1071
    :cond_1a
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1b

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1072
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1074
    :cond_1b
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1075
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1078
    :pswitch_a
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->q:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->q:I

    .line 1079
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1081
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->l:I

    .line 1082
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1084
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1086
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->l:I

    sget-object v1, Lsoftware/simplicial/a/d;->aS:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1c

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aS:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1087
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aS:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1089
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->l:I

    sget-object v1, Lsoftware/simplicial/a/d;->aT:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1d

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aT:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 1090
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aT:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1092
    :cond_1d
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->l:I

    sget-object v1, Lsoftware/simplicial/a/d;->aU:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1e

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aU:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1093
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aU:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1095
    :cond_1e
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_1f

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 1096
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aA:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1098
    :cond_1f
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_20

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 1099
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aB:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1101
    :cond_20
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->p:I

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1102
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aC:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1105
    :pswitch_b
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->r:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->r:I

    .line 1106
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1108
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->m:I

    .line 1109
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1111
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1113
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->m:I

    sget-object v1, Lsoftware/simplicial/a/d;->aV:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_21

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aV:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_21

    .line 1114
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aV:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1116
    :cond_21
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->m:I

    sget-object v1, Lsoftware/simplicial/a/d;->aW:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_22

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aW:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 1117
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aW:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1119
    :cond_22
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->m:I

    sget-object v1, Lsoftware/simplicial/a/d;->aX:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aX:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1120
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aX:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1123
    :pswitch_c
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->s:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->s:I

    .line 1124
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1126
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->n:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->n:I

    .line 1127
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1129
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1131
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->n:I

    sget-object v1, Lsoftware/simplicial/a/d;->aY:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_23

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aY:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 1132
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aY:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1134
    :cond_23
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->n:I

    sget-object v1, Lsoftware/simplicial/a/d;->aZ:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_24

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aZ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 1135
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aZ:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1137
    :cond_24
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->n:I

    sget-object v1, Lsoftware/simplicial/a/d;->ba:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ba:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1138
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ba:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1142
    :pswitch_d
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->t:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->t:I

    .line 1143
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1145
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->o:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->o:I

    .line 1146
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1148
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1150
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->o:I

    sget-object v1, Lsoftware/simplicial/a/d;->bb:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_25

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bb:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 1151
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bb:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1153
    :cond_25
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->o:I

    sget-object v1, Lsoftware/simplicial/a/d;->bc:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_26

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bc:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 1154
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bc:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1156
    :cond_26
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->o:I

    sget-object v1, Lsoftware/simplicial/a/d;->bd:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bd:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1157
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->bd:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1161
    :pswitch_e
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/ay;->h:I

    .line 1162
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v3, v0, Lsoftware/simplicial/a/az;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/az;->c:I

    .line 1163
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v3, v0, Lsoftware/simplicial/a/ay;->x:I

    add-int/lit8 v3, v3, 0x3

    iput v3, v0, Lsoftware/simplicial/a/ay;->x:I

    .line 1164
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    add-long/2addr v4, v6

    iput-wide v4, v0, Lsoftware/simplicial/a/az;->s:J

    .line 1166
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    goto/16 :goto_0

    .line 897
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V
    .locals 12

    .prologue
    .line 197
    const/4 v2, 0x0

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-super {p0, v0, v1, v2}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 199
    move/from16 v0, p15

    new-array v2, v0, [Lsoftware/simplicial/a/bh;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    .line 200
    const/4 v4, 0x0

    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v2, v2

    if-ge v4, v2, :cond_0

    .line 202
    iget-object v11, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    new-instance v2, Lsoftware/simplicial/a/bh;

    iget v3, p1, Lsoftware/simplicial/a/f/bl;->a:I

    const/high16 v7, 0x40a00000    # 5.0f

    const/high16 v8, 0x40a00000    # 5.0f

    sget v9, Lsoftware/simplicial/a/bh;->e:F

    move/from16 v5, p4

    move/from16 v6, p5

    move v10, p3

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bh;-><init>(IIFFFFFI)V

    aput-object v2, v11, v4

    .line 203
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->e()V

    .line 200
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 205
    :cond_0
    iput-object p1, p0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    .line 206
    iput p2, p0, Lsoftware/simplicial/a/bf;->A:I

    .line 207
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lsoftware/simplicial/a/bf;->B:J

    .line 208
    iput p3, p0, Lsoftware/simplicial/a/bf;->C:I

    .line 209
    const-string v2, "NULL"

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 210
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->E:[B

    .line 211
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->F:[B

    .line 212
    move-object/from16 v0, p8

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    .line 213
    move-object/from16 v0, p9

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->I:[B

    .line 214
    move-object/from16 v0, p11

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    .line 215
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 216
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    .line 217
    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    .line 218
    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    .line 219
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->ad:I

    .line 220
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->ae:I

    .line 221
    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    .line 222
    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    .line 223
    sget-object v2, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    .line 224
    const-string v2, ""

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 225
    move/from16 v0, p14

    iput v0, p0, Lsoftware/simplicial/a/bf;->ag:I

    .line 226
    move/from16 v0, p15

    iput v0, p0, Lsoftware/simplicial/a/bf;->bF:I

    .line 227
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->K:Z

    .line 228
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 229
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->M:F

    .line 230
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->N:F

    .line 231
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->O:F

    .line 232
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->P:F

    .line 233
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->Q:I

    .line 235
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->S:I

    .line 236
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->T:F

    .line 238
    const/4 v2, 0x0

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    .line 239
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->al:F

    .line 240
    const/4 v2, 0x0

    iput-byte v2, p0, Lsoftware/simplicial/a/bf;->am:B

    .line 241
    const/4 v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/bf;->an:I

    .line 242
    sget-object v2, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 244
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->ap:J

    .line 245
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->aq:J

    .line 246
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 247
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 248
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 249
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 250
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 251
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->G:Z

    .line 252
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->aw:Z

    .line 253
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->ax:I

    .line 254
    sget-object v2, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    .line 255
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->az:Z

    .line 256
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->aA:Z

    .line 257
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    const-string v6, "NULL"

    const/4 v3, 0x0

    new-array v7, v3, [B

    move v3, p2

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v8, p10

    invoke-virtual/range {v2 .. v8}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    .line 258
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    invoke-virtual {v2}, Lsoftware/simplicial/a/az;->a()V

    .line 259
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lsoftware/simplicial/a/bf;->aD:F

    .line 260
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->aE:J

    .line 261
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->aF:Z

    .line 262
    const/4 v2, 0x0

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 263
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aH:Lsoftware/simplicial/a/f/bv;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bv;->a()V

    .line 264
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->aI:I

    .line 265
    const/4 v2, -0x1

    iput-byte v2, p0, Lsoftware/simplicial/a/bf;->aJ:B

    .line 266
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->aK:J

    .line 267
    const/16 v2, 0x7f

    iput-byte v2, p0, Lsoftware/simplicial/a/bf;->aL:B

    .line 268
    const/4 v2, 0x0

    iput-object v2, p0, Lsoftware/simplicial/a/bf;->aM:[B

    .line 269
    const/4 v2, 0x2

    iput v2, p0, Lsoftware/simplicial/a/bf;->aN:I

    .line 271
    const/4 v2, -0x1

    iput v2, p0, Lsoftware/simplicial/a/bf;->aO:I

    .line 272
    const/4 v2, -0x2

    iput v2, p0, Lsoftware/simplicial/a/bf;->aP:I

    .line 273
    const/16 v2, -0xa

    iput v2, p0, Lsoftware/simplicial/a/bf;->aQ:I

    .line 274
    const/16 v2, -0xa

    iput v2, p0, Lsoftware/simplicial/a/bf;->aR:I

    .line 275
    const/16 v2, -0x28

    iput v2, p0, Lsoftware/simplicial/a/bf;->aS:I

    .line 276
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->aT:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 277
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->aU:F

    .line 278
    const/4 v2, 0x2

    iput v2, p0, Lsoftware/simplicial/a/bf;->aV:I

    .line 279
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lsoftware/simplicial/a/bf;->aW:J

    .line 281
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->aX:Z

    .line 282
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->aY:Z

    .line 283
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->aZ:I

    .line 284
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->ba:I

    .line 285
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bb:I

    .line 286
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bc:I

    .line 287
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bd:I

    .line 288
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->be:I

    .line 289
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bf:I

    .line 290
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bl:I

    .line 291
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bm:I

    .line 292
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bn:I

    .line 293
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bg:I

    .line 294
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bi:I

    .line 295
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bh:I

    .line 296
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bj:I

    .line 297
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bk:I

    .line 298
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bo:I

    .line 299
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bz:I

    .line 300
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bp:Z

    .line 301
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bq:Z

    .line 302
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->br:I

    .line 303
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 304
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 305
    const/4 v2, 0x0

    iput-boolean v2, p0, Lsoftware/simplicial/a/bf;->bu:Z

    .line 310
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/bf;->bB:I

    .line 311
    sget v2, Lsoftware/simplicial/a/ai;->Q:F

    iput v2, p0, Lsoftware/simplicial/a/bf;->a:F

    .line 312
    const/4 v2, -0x1

    iput v2, p0, Lsoftware/simplicial/a/bf;->bD:I

    .line 313
    return-void
.end method

.method public a(Lsoftware/simplicial/a/g;JI)V
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1284
    :cond_0
    :goto_0
    return-void

    .line 1265
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/bf$1;->d:[I

    iget-object v1, p1, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    invoke-virtual {v1}, Lsoftware/simplicial/a/g$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1281
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p2, v0, Lsoftware/simplicial/a/ay;->V:J

    .line 1282
    if-eqz p4, :cond_0

    .line 1283
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput p4, v0, Lsoftware/simplicial/a/ay;->Y:I

    goto :goto_0

    .line 1268
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->G:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->G:I

    .line 1269
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->H:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->H:I

    goto :goto_1

    .line 1272
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->H:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->H:I

    .line 1273
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->G:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->G:I

    goto :goto_1

    .line 1276
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->I:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->I:I

    .line 1277
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->I:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->I:I

    goto :goto_1

    .line 1265
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 755
    if-eqz p1, :cond_0

    .line 756
    iget-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    .line 759
    :goto_0
    return-void

    .line 758
    :cond_0
    iget-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v0, v0, -0x3

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    goto :goto_0
.end method

.method public b()V
    .locals 14

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 173
    new-instance v0, Lsoftware/simplicial/a/ay;

    const/4 v1, -0x1

    sget-object v2, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const-string v4, ""

    new-array v5, v6, [B

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/ay;-><init>(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    .line 174
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0}, Lsoftware/simplicial/a/az;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    .line 176
    new-instance v0, Lsoftware/simplicial/a/f/bv;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bv;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->aH:Lsoftware/simplicial/a/f/bv;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    .line 179
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    .line 181
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    .line 184
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    .line 185
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    .line 186
    iput-boolean v6, p0, Lsoftware/simplicial/a/bf;->G:Z

    .line 187
    iput-boolean v6, p0, Lsoftware/simplicial/a/bf;->aw:Z

    .line 189
    iget v0, p0, Lsoftware/simplicial/a/bf;->bF:I

    new-array v0, v0, [Lsoftware/simplicial/a/bh;

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move v7, v6

    .line 190
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v0, v0

    if-ge v7, v0, :cond_0

    .line 191
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    new-instance v5, Lsoftware/simplicial/a/bh;

    const/high16 v12, 0x40a00000    # 5.0f

    iget v13, p0, Lsoftware/simplicial/a/bf;->C:I

    move v9, v8

    move v10, v8

    move v11, v8

    invoke-direct/range {v5 .. v13}, Lsoftware/simplicial/a/bh;-><init>(IIFFFFFI)V

    aput-object v5, v0, v7

    .line 190
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 192
    :cond_0
    return-void
.end method

.method public b(F)V
    .locals 4

    .prologue
    .line 1523
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1531
    :goto_0
    return-void

    .line 1526
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->y:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    float-to-int v1, v1

    iput v1, v0, Lsoftware/simplicial/a/ay;->y:I

    .line 1527
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->E:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->E:I

    .line 1529
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v2, v0, Lsoftware/simplicial/a/az;->t:J

    long-to-float v1, v2

    add-float/2addr v1, p1

    float-to-long v2, v1

    iput-wide v2, v0, Lsoftware/simplicial/a/az;->t:J

    .line 1530
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->D:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->D:I

    goto :goto_0
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 433
    iget v0, p0, Lsoftware/simplicial/a/bf;->S:I

    .line 434
    if-nez v0, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v1, v1, p1

    .line 438
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-object v3, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    aput-object v3, v2, p1

    .line 439
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    add-int/lit8 v0, v0, -0x1

    aput-object v1, v2, v0

    .line 440
    iget v0, p0, Lsoftware/simplicial/a/bf;->S:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bf;->S:I

    .line 441
    const/4 v0, 0x0

    iput v0, v1, Lsoftware/simplicial/a/bh;->Q:F

    .line 442
    const/4 v0, 0x1

    iput-boolean v0, v1, Lsoftware/simplicial/a/bh;->M:Z

    .line 443
    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lsoftware/simplicial/a/bf;->m()V

    goto :goto_0
.end method

.method public b(ILsoftware/simplicial/a/am;)V
    .locals 2

    .prologue
    .line 1696
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_1

    .line 1712
    :cond_0
    :goto_0
    return-void

    .line 1699
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_0

    .line 1702
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne p2, v0, :cond_2

    .line 1703
    div-int/lit8 p1, p1, 0x2

    .line 1705
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lsoftware/simplicial/a/bf;->a(IZ)V

    .line 1707
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->J:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->J:I

    .line 1708
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->J:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->J:I

    .line 1710
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->J:I

    sget-object v1, Lsoftware/simplicial/a/d;->Q:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->Q:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1711
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->Q:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(J)V
    .locals 3

    .prologue
    .line 1535
    iput-wide p1, p0, Lsoftware/simplicial/a/bf;->aE:J

    .line 1537
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1542
    :goto_0
    return-void

    .line 1540
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->C:J

    .line 1541
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-object v1, p0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iget-byte v1, v1, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v1, v0, Lsoftware/simplicial/a/ay;->L:B

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/am;)V
    .locals 2

    .prologue
    .line 1575
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1592
    :cond_0
    :goto_0
    return-void

    .line 1578
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_2

    .line 1580
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ag:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1582
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->ag:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1585
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne p1, v0, :cond_0

    .line 1587
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aj:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1589
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->aj:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 317
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->aX:Z

    .line 318
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->aY:Z

    .line 319
    iput v0, p0, Lsoftware/simplicial/a/bf;->aZ:I

    .line 320
    iput v0, p0, Lsoftware/simplicial/a/bf;->ba:I

    .line 321
    iput v0, p0, Lsoftware/simplicial/a/bf;->bb:I

    .line 322
    iput v0, p0, Lsoftware/simplicial/a/bf;->bc:I

    .line 323
    iput v0, p0, Lsoftware/simplicial/a/bf;->bd:I

    .line 324
    iput v0, p0, Lsoftware/simplicial/a/bf;->be:I

    .line 325
    iput v0, p0, Lsoftware/simplicial/a/bf;->bf:I

    .line 326
    iput v0, p0, Lsoftware/simplicial/a/bf;->bl:I

    .line 327
    iput v0, p0, Lsoftware/simplicial/a/bf;->bm:I

    .line 328
    iput v0, p0, Lsoftware/simplicial/a/bf;->bn:I

    .line 329
    iput v0, p0, Lsoftware/simplicial/a/bf;->bg:I

    .line 330
    iput v0, p0, Lsoftware/simplicial/a/bf;->bi:I

    .line 331
    iput v0, p0, Lsoftware/simplicial/a/bf;->bh:I

    .line 332
    iput v0, p0, Lsoftware/simplicial/a/bf;->bj:I

    .line 333
    iput v0, p0, Lsoftware/simplicial/a/bf;->bk:I

    .line 334
    iput v0, p0, Lsoftware/simplicial/a/bf;->bo:I

    .line 337
    iput v0, p0, Lsoftware/simplicial/a/bf;->br:I

    .line 338
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bt:Z

    .line 339
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bu:Z

    .line 340
    iput v0, p0, Lsoftware/simplicial/a/bf;->bv:I

    .line 341
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bx:Z

    .line 342
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->by:Z

    .line 343
    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->bw:Z

    .line 344
    iput v0, p0, Lsoftware/simplicial/a/bf;->bz:I

    .line 345
    iput v0, p0, Lsoftware/simplicial/a/bf;->bA:I

    .line 346
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    .line 348
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 349
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 743
    iput p1, p0, Lsoftware/simplicial/a/bf;->ah:I

    .line 744
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/bf;->ai:I

    .line 745
    return-void
.end method

.method public c(J)V
    .locals 1

    .prologue
    .line 1546
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1554
    :goto_0
    return-void

    .line 1549
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->B:J

    .line 1550
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->W:J

    .line 1551
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->V:J

    .line 1552
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->T:J

    .line 1553
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide p1, v0, Lsoftware/simplicial/a/ay;->C:J

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lsoftware/simplicial/a/bf;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/bf;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lsoftware/simplicial/a/bf;->Q:I

    return v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 749
    iput p1, p0, Lsoftware/simplicial/a/bf;->ax:I

    .line 750
    iget-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    or-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bf;->am:B

    .line 751
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 717
    invoke-super {p0}, Lsoftware/simplicial/a/ao;->e()V

    .line 718
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bf;->aj:Z

    .line 719
    return-void
.end method

.method public k()I
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v0, v0, Lsoftware/simplicial/a/f/bl;->a:I

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v0, v0, Lsoftware/simplicial/a/f/bl;->b:I

    return v0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    .line 450
    return-void
.end method

.method public n()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 474
    iget v1, p0, Lsoftware/simplicial/a/bf;->S:I

    iget v2, p0, Lsoftware/simplicial/a/bf;->bF:I

    if-ne v1, v2, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 477
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 479
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 477
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 482
    :cond_3
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_2

    .line 483
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public o()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 490
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 492
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 490
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 495
    :cond_1
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    const/high16 v5, 0x41a00000    # 20.0f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_0

    .line 496
    const/4 v0, 0x1

    .line 499
    :cond_2
    return v0
.end method

.method public p()F
    .locals 7

    .prologue
    .line 535
    const/4 v1, 0x0

    .line 536
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    const/4 v0, 0x0

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 538
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 536
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 541
    :cond_1
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    cmpl-float v5, v5, v0

    if-lez v5, :cond_0

    .line 542
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v0

    goto :goto_1

    .line 544
    :cond_2
    return v0
.end method

.method public q()Lsoftware/simplicial/a/bh;
    .locals 6

    .prologue
    .line 641
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    .line 652
    :goto_0
    return-object v0

    .line 644
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 646
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 644
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 649
    :cond_2
    iget-object v4, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 650
    :cond_3
    iput-object v3, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    goto :goto_2

    .line 652
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->b:Lsoftware/simplicial/a/bh;

    goto :goto_0
.end method

.method public r()F
    .locals 7

    .prologue
    .line 729
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 730
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    const/4 v0, 0x0

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 732
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 730
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 735
    :cond_1
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    cmpg-float v5, v5, v0

    if-gez v5, :cond_0

    .line 736
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v0

    goto :goto_1

    .line 738
    :cond_2
    return v0
.end method

.method public s()V
    .locals 2

    .prologue
    .line 833
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 837
    :goto_0
    return-void

    .line 836
    :cond_0
    const/16 v0, 0x64

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/bf;->a(IZ)V

    goto :goto_0
.end method

.method public t()V
    .locals 2

    .prologue
    .line 1513
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 1519
    :goto_0
    return-void

    .line 1516
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->D:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->D:I

    .line 1518
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->E:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->E:I

    goto :goto_0
.end method

.method public u()V
    .locals 2

    .prologue
    .line 1716
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_1

    .line 1727
    :cond_0
    :goto_0
    return-void

    .line 1719
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v0, Lsoftware/simplicial/a/ay;->K:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/ay;->K:I

    .line 1720
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v1, v0, Lsoftware/simplicial/a/az;->K:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/az;->K:I

    .line 1722
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v0, v0, Lsoftware/simplicial/a/ay;->K:I

    sget-object v1, Lsoftware/simplicial/a/d;->O:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->O:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1723
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->O:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1725
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v0, v0, Lsoftware/simplicial/a/az;->K:I

    sget-object v1, Lsoftware/simplicial/a/d;->P:Lsoftware/simplicial/a/d;

    iget v1, v1, Lsoftware/simplicial/a/d;->bx:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->P:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1726
    iget-object v0, p0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/d;->P:Lsoftware/simplicial/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public v()V
    .locals 1

    .prologue
    .line 1731
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bf;->aI:I

    .line 1732
    return-void
.end method

.method public w()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1736
    iget-object v2, p0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1737
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1739
    :goto_1
    return v0

    .line 1736
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1739
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
