.class public final enum Lsoftware/simplicial/a/i$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/i$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/i$a;

.field public static final enum b:Lsoftware/simplicial/a/i$a;

.field public static final enum c:Lsoftware/simplicial/a/i$a;

.field public static final enum d:Lsoftware/simplicial/a/i$a;

.field private static final synthetic e:[Lsoftware/simplicial/a/i$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-instance v0, Lsoftware/simplicial/a/i$a;

    const-string v1, "MEANDERING"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    new-instance v0, Lsoftware/simplicial/a/i$a;

    const-string v1, "CHASING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    new-instance v0, Lsoftware/simplicial/a/i$a;

    const-string v1, "FLEEING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i$a;->c:Lsoftware/simplicial/a/i$a;

    new-instance v0, Lsoftware/simplicial/a/i$a;

    const-string v1, "DEFENSIVE_SHOOTING"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/i$a;->d:Lsoftware/simplicial/a/i$a;

    .line 99
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/i$a;

    sget-object v1, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/i$a;->c:Lsoftware/simplicial/a/i$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/i$a;->d:Lsoftware/simplicial/a/i$a;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/i$a;->e:[Lsoftware/simplicial/a/i$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/i$a;
    .locals 1

    .prologue
    .line 99
    const-class v0, Lsoftware/simplicial/a/i$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/i$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/i$a;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lsoftware/simplicial/a/i$a;->e:[Lsoftware/simplicial/a/i$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/i$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/i$a;

    return-object v0
.end method
