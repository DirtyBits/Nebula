.class public Lsoftware/simplicial/a/i/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:Z

.field public final d:Lsoftware/simplicial/a/i/d;

.field public final e:Lsoftware/simplicial/a/i/d;

.field public final f:Lsoftware/simplicial/a/i/d;

.field public final g:Lsoftware/simplicial/a/i/d;

.field public final h:I

.field public final i:I

.field public final j:I

.field public k:Lsoftware/simplicial/a/i/d;

.field public l:I

.field public m:I


# virtual methods
.method public a(I)[Lsoftware/simplicial/a/i/d;
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    iget-boolean v0, p0, Lsoftware/simplicial/a/i/a;->c:Z

    if-nez v0, :cond_0

    .line 109
    new-array v0, v2, [Lsoftware/simplicial/a/i/d;

    .line 114
    :goto_0
    return-object v0

    .line 110
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-ne p1, v0, :cond_2

    .line 111
    :cond_1
    new-array v0, v1, [Lsoftware/simplicial/a/i/d;

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v3

    goto :goto_0

    .line 112
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->f:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->g:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-ne p1, v0, :cond_4

    .line 113
    :cond_3
    new-array v0, v1, [Lsoftware/simplicial/a/i/d;

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->f:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->g:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v3

    goto :goto_0

    .line 114
    :cond_4
    new-array v0, v2, [Lsoftware/simplicial/a/i/d;

    goto :goto_0
.end method

.method public b(I)[Lsoftware/simplicial/a/i/d;
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 119
    iget-boolean v0, p0, Lsoftware/simplicial/a/i/a;->c:Z

    if-nez v0, :cond_0

    .line 120
    new-array v0, v2, [Lsoftware/simplicial/a/i/d;

    .line 125
    :goto_0
    return-object v0

    .line 121
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_1

    .line 122
    new-array v0, v1, [Lsoftware/simplicial/a/i/d;

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v3

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->f:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->g:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    if-eq p1, v0, :cond_2

    .line 124
    new-array v0, v1, [Lsoftware/simplicial/a/i/d;

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->f:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v2

    iget-object v1, p0, Lsoftware/simplicial/a/i/a;->g:Lsoftware/simplicial/a/i/d;

    aput-object v1, v0, v3

    goto :goto_0

    .line 125
    :cond_2
    new-array v0, v2, [Lsoftware/simplicial/a/i/d;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 103
    const-string v1, "GID %d A %d B %d W %d N %s TO %d"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v3, p0, Lsoftware/simplicial/a/i/a;->l:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget-object v3, p0, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget-object v3, p0, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->k:Lsoftware/simplicial/a/i/d;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x4

    iget-object v3, p0, Lsoftware/simplicial/a/i/a;->a:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x5

    iget v3, p0, Lsoftware/simplicial/a/i/a;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/i/a;->k:Lsoftware/simplicial/a/i/d;

    iget v0, v0, Lsoftware/simplicial/a/i/d;->a:I

    goto :goto_0
.end method
