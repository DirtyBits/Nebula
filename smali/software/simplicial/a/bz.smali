.class public Lsoftware/simplicial/a/bz;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:F

.field public final d:F

.field public final e:F

.field public final f:F

.field public g:Lsoftware/simplicial/a/bx;

.field public h:F


# direct methods
.method public constructor <init>(IILsoftware/simplicial/a/ap;F)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 24
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bz;->h:F

    .line 25
    iput p1, p0, Lsoftware/simplicial/a/bz;->b:I

    .line 26
    iput p2, p0, Lsoftware/simplicial/a/bz;->a:I

    .line 28
    invoke-static {p3}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    int-to-float v0, v0

    div-float v0, p4, v0

    .line 30
    int-to-float v1, p1

    mul-float/2addr v1, v0

    iput v1, p0, Lsoftware/simplicial/a/bz;->c:F

    .line 31
    add-int/lit8 v1, p1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iput v1, p0, Lsoftware/simplicial/a/bz;->d:F

    .line 32
    int-to-float v1, p2

    mul-float/2addr v1, v0

    iput v1, p0, Lsoftware/simplicial/a/bz;->f:F

    .line 33
    add-int/lit8 v1, p2, 0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bz;->e:F

    .line 35
    iget v0, p0, Lsoftware/simplicial/a/bz;->c:F

    iget v1, p0, Lsoftware/simplicial/a/bz;->d:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/a/bz;->l:F

    .line 36
    iget v0, p0, Lsoftware/simplicial/a/bz;->f:F

    iget v1, p0, Lsoftware/simplicial/a/bz;->e:F

    add-float/2addr v0, v1

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/a/bz;->m:F

    .line 37
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 41
    sget-object v0, Lsoftware/simplicial/a/bz$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 44
    :pswitch_0
    const/16 v0, 0x62

    goto :goto_0

    .line 46
    :pswitch_1
    const/16 v0, 0x46

    goto :goto_0

    .line 48
    :pswitch_2
    const/16 v0, 0x77

    goto :goto_0

    .line 50
    :pswitch_3
    const/16 v0, 0x23

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lsoftware/simplicial/a/ap;)I
    .locals 2

    .prologue
    .line 64
    invoke-static {p0}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/ap;)I

    move-result v0

    invoke-static {p0}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/ap;)I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lsoftware/simplicial/a/bz;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bz;->s:F

    .line 77
    iget v0, p0, Lsoftware/simplicial/a/bz;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bz;->t:F

    .line 78
    iget v0, p0, Lsoftware/simplicial/a/bz;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bz;->w:F

    .line 79
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lsoftware/simplicial/a/bz;->b:I

    add-int/lit8 v0, v0, -0x80

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 58
    iget v0, p0, Lsoftware/simplicial/a/bz;->a:I

    add-int/lit8 v0, v0, -0x80

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 59
    iget-object v0, p0, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 60
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    iget v0, v0, Lsoftware/simplicial/a/bx;->c:I

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bz;->h:F

    .line 71
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bz;->h:F

    .line 84
    return-void
.end method
