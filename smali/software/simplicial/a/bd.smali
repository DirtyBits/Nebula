.class public Lsoftware/simplicial/a/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lsoftware/simplicial/a/bd;

.field public static final b:[Lsoftware/simplicial/a/bd;


# instance fields
.field public final c:B

.field public d:I

.field public e:S


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 10
    new-instance v0, Lsoftware/simplicial/a/bd;

    invoke-direct {v0, v2, v2, v1}, Lsoftware/simplicial/a/bd;-><init>(BIS)V

    sput-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    .line 12
    const/16 v0, 0xf

    new-array v0, v0, [Lsoftware/simplicial/a/bd;

    sput-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    move v0, v1

    .line 16
    :goto_0
    sget-object v2, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 17
    sget-object v2, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    new-instance v3, Lsoftware/simplicial/a/bd;

    int-to-byte v4, v0

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5, v1}, Lsoftware/simplicial/a/bd;-><init>(BIS)V

    aput-object v3, v2, v0

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 18
    :cond_0
    return-void
.end method

.method public constructor <init>(BIS)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-byte p1, p0, Lsoftware/simplicial/a/bd;->c:B

    .line 27
    iput p2, p0, Lsoftware/simplicial/a/bd;->d:I

    .line 28
    iput-short p3, p0, Lsoftware/simplicial/a/bd;->e:S

    .line 29
    return-void
.end method

.method public static a(I)Lsoftware/simplicial/a/bd;
    .locals 1

    .prologue
    .line 44
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 45
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    .line 46
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static a(IJ)Lsoftware/simplicial/a/bd;
    .locals 5

    .prologue
    .line 51
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 52
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    .line 53
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lsoftware/simplicial/a/bd;

    int-to-byte v1, p0

    sget-object v2, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    aget-object v2, v2, p0

    iget v2, v2, Lsoftware/simplicial/a/bd;->d:I

    invoke-static {p1, p2}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v3

    int-to-short v3, v3

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/bd;-><init>(BIS)V

    goto :goto_0
.end method

.method public static a(IS)Lsoftware/simplicial/a/bd;
    .locals 3

    .prologue
    .line 58
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 59
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    .line 60
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lsoftware/simplicial/a/bd;

    int-to-byte v1, p0

    sget-object v2, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    aget-object v2, v2, p0

    iget v2, v2, Lsoftware/simplicial/a/bd;->d:I

    invoke-direct {v0, v1, v2, p1}, Lsoftware/simplicial/a/bd;-><init>(BIS)V

    goto :goto_0
.end method

.method public static a(BLjava/util/Map;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 34
    sget-object v1, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v1, v1, Lsoftware/simplicial/a/bd;->c:B

    if-ne p0, v1, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(BLjava/util/Map;)Lsoftware/simplicial/a/bd;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;)",
            "Lsoftware/simplicial/a/bd;"
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bd;

    .line 74
    if-eqz v0, :cond_0

    sget-object v1, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v1, v1, Lsoftware/simplicial/a/bd;->c:B

    if-ne p0, v1, :cond_1

    .line 75
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    .line 78
    :goto_0
    return-object v0

    .line 77
    :cond_1
    sget-object v1, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    aget-object v1, v1, p0

    iget v1, v1, Lsoftware/simplicial/a/bd;->d:I

    iput v1, v0, Lsoftware/simplicial/a/bd;->d:I

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 84
    if-eqz p1, :cond_0

    instance-of v1, p1, Lsoftware/simplicial/a/bd;

    if-nez v1, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-byte v1, p0, Lsoftware/simplicial/a/bd;->c:B

    check-cast p1, Lsoftware/simplicial/a/bd;

    iget-byte v2, p1, Lsoftware/simplicial/a/bd;->c:B

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    if-ne p0, v0, :cond_0

    .line 67
    const-string v0, "misc_none"

    .line 68
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pet_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lsoftware/simplicial/a/bd;->c:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
