.class public Lsoftware/simplicial/a/z;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/bx;

.field public b:F

.field public c:I

.field public d:F


# direct methods
.method public constructor <init>(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    .line 15
    iput v1, p0, Lsoftware/simplicial/a/z;->b:F

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/z;->c:I

    .line 17
    iput v1, p0, Lsoftware/simplicial/a/z;->d:F

    .line 21
    invoke-virtual/range {p0 .. p5}, Lsoftware/simplicial/a/z;->a(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    .line 22
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lsoftware/simplicial/a/z;->l:F

    iput v0, p0, Lsoftware/simplicial/a/z;->s:F

    .line 28
    iget v0, p0, Lsoftware/simplicial/a/z;->m:F

    iput v0, p0, Lsoftware/simplicial/a/z;->t:F

    .line 29
    iget v0, p0, Lsoftware/simplicial/a/z;->n:F

    iput v0, p0, Lsoftware/simplicial/a/z;->w:F

    .line 30
    return-void
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 84
    iget v0, p0, Lsoftware/simplicial/a/z;->n:F

    const/high16 v1, 0x41700000    # 15.0f

    sub-float/2addr v0, v1

    const v1, 0x3daaaaab

    mul-float/2addr v1, p1

    div-float/2addr v0, v1

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lsoftware/simplicial/a/z;->c:I

    .line 85
    iget v0, p0, Lsoftware/simplicial/a/z;->c:I

    if-ge v0, v2, :cond_0

    .line 86
    iput v2, p0, Lsoftware/simplicial/a/z;->c:I

    .line 87
    :cond_0
    return-void
.end method

.method public a(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 34
    iput-object p5, p0, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    .line 35
    iput v2, p0, Lsoftware/simplicial/a/z;->b:F

    .line 37
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_4

    .line 39
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-virtual {p1}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x41400000    # 12.0f

    div-float/2addr v2, v3

    mul-float/2addr v2, p2

    add-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/a/z;->n:F

    .line 41
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/z;->a(F)V

    .line 46
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/z;->n:F

    invoke-virtual {p1}, Ljava/util/Random;->nextFloat()F

    move-result v2

    iget v3, p0, Lsoftware/simplicial/a/z;->n:F

    mul-float/2addr v3, v4

    sub-float v3, p2, v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/a/z;->l:F

    .line 47
    iget v0, p0, Lsoftware/simplicial/a/z;->n:F

    invoke-virtual {p1}, Ljava/util/Random;->nextFloat()F

    move-result v2

    iget v3, p0, Lsoftware/simplicial/a/z;->n:F

    mul-float/2addr v3, v4

    sub-float v3, p2, v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/a/z;->m:F

    move v0, v1

    .line 50
    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_6

    .line 52
    aget-object v2, p3, v0

    .line 53
    if-eq p0, v2, :cond_1

    if-nez v2, :cond_2

    .line 50
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_2
    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    const/4 v0, 0x1

    .line 62
    :goto_1
    if-nez v0, :cond_0

    .line 79
    :cond_3
    :goto_2
    iget v0, p0, Lsoftware/simplicial/a/z;->l:F

    iget v1, p0, Lsoftware/simplicial/a/z;->m:F

    iget v2, p0, Lsoftware/simplicial/a/z;->n:F

    invoke-super {p0, v0, v1, v2}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 80
    return-void

    .line 64
    :cond_4
    sget-object v0, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_5

    .line 66
    iget v0, p5, Lsoftware/simplicial/a/bx;->l:F

    iput v0, p0, Lsoftware/simplicial/a/z;->l:F

    .line 67
    iget v0, p5, Lsoftware/simplicial/a/bx;->m:F

    iput v0, p0, Lsoftware/simplicial/a/z;->m:F

    .line 68
    iput v2, p0, Lsoftware/simplicial/a/z;->n:F

    .line 69
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lsoftware/simplicial/a/z;->d:F

    goto :goto_2

    .line 71
    :cond_5
    sget-object v0, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne p4, v0, :cond_3

    .line 73
    div-float v0, p2, v4

    iput v0, p0, Lsoftware/simplicial/a/z;->l:F

    .line 74
    div-float v0, p2, v4

    iput v0, p0, Lsoftware/simplicial/a/z;->m:F

    .line 75
    iput v2, p0, Lsoftware/simplicial/a/z;->n:F

    .line 76
    iput v2, p0, Lsoftware/simplicial/a/z;->d:F

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_1
.end method
