.class public Lsoftware/simplicial/a/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:Lsoftware/simplicial/a/b/d;

.field public final c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/h;",
            ">;"
        }
    .end annotation
.end field

.field public e:I


# direct methods
.method public static a(Lsoftware/simplicial/a/b/d;)I
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lsoftware/simplicial/a/b/a$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 73
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 69
    :pswitch_0
    const/16 v0, 0x8

    goto :goto_0

    .line 71
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(ILjava/util/Collection;)Lsoftware/simplicial/a/b/h;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/b/h;",
            ">;)",
            "Lsoftware/simplicial/a/b/h;"
        }
    .end annotation

    .prologue
    .line 106
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/h;

    .line 107
    iget v2, v0, Lsoftware/simplicial/a/b/h;->a:I

    if-ne v2, p0, :cond_0

    .line 110
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lsoftware/simplicial/a/b/d;)Lsoftware/simplicial/a/ap;
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lsoftware/simplicial/a/b/a$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    sget-object v0, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    :goto_0
    return-object v0

    .line 82
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    goto :goto_0

    .line 84
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/a/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/h;

    .line 117
    iget-object v2, v0, Lsoftware/simplicial/a/b/h;->c:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    if-ne v2, p1, :cond_0

    .line 119
    sget-object v1, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    iput-object v1, v0, Lsoftware/simplicial/a/b/h;->b:Lsoftware/simplicial/a/b/b;

    .line 123
    :cond_1
    return-void
.end method
