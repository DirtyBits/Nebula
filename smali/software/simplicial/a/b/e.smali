.class public final enum Lsoftware/simplicial/a/b/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/b/e;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/b/e;

.field public static final enum b:Lsoftware/simplicial/a/b/e;

.field public static final enum c:Lsoftware/simplicial/a/b/e;

.field public static final enum d:Lsoftware/simplicial/a/b/e;

.field public static final enum e:Lsoftware/simplicial/a/b/e;

.field public static final enum f:Lsoftware/simplicial/a/b/e;

.field public static final g:[Lsoftware/simplicial/a/b/e;

.field private static final synthetic h:[Lsoftware/simplicial/a/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->a:Lsoftware/simplicial/a/b/e;

    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "OK"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->b:Lsoftware/simplicial/a/b/e;

    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "INVALID_ACCOUNT_ID"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->c:Lsoftware/simplicial/a/b/e;

    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "ALREADY_IN_QUEUE"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->d:Lsoftware/simplicial/a/b/e;

    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "ALREADY_IN_ARENA"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->e:Lsoftware/simplicial/a/b/e;

    new-instance v0, Lsoftware/simplicial/a/b/e;

    const-string v1, "VALIDATING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/b/e;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/b/e;->f:Lsoftware/simplicial/a/b/e;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lsoftware/simplicial/a/b/e;

    sget-object v1, Lsoftware/simplicial/a/b/e;->a:Lsoftware/simplicial/a/b/e;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/b/e;->b:Lsoftware/simplicial/a/b/e;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/b/e;->c:Lsoftware/simplicial/a/b/e;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/b/e;->d:Lsoftware/simplicial/a/b/e;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/b/e;->e:Lsoftware/simplicial/a/b/e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/b/e;->f:Lsoftware/simplicial/a/b/e;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/b/e;->h:[Lsoftware/simplicial/a/b/e;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/b/e;->values()[Lsoftware/simplicial/a/b/e;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/b/e;->g:[Lsoftware/simplicial/a/b/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/b/e;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/b/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/e;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/b/e;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/b/e;->h:[Lsoftware/simplicial/a/b/e;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/b/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/b/e;

    return-object v0
.end method
