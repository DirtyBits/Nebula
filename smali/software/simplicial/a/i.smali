.class public Lsoftware/simplicial/a/i;
.super Lsoftware/simplicial/a/bf;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/i$a;
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field private bG:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Lsoftware/simplicial/a/i$a;

.field public k:Lsoftware/simplicial/a/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/a/bf;-><init>()V

    .line 16
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/i;->c:I

    .line 17
    iput v1, p0, Lsoftware/simplicial/a/i;->d:I

    .line 18
    iput v1, p0, Lsoftware/simplicial/a/i;->e:I

    .line 19
    iput v1, p0, Lsoftware/simplicial/a/i;->f:I

    .line 20
    iput v1, p0, Lsoftware/simplicial/a/i;->g:I

    .line 21
    iput v1, p0, Lsoftware/simplicial/a/i;->h:I

    .line 22
    iput v1, p0, Lsoftware/simplicial/a/i;->i:I

    .line 23
    sget-object v0, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    iput-object v0, p0, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    .line 30
    return-void
.end method


# virtual methods
.method public a(IIFFLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;ZII)V
    .locals 18

    .prologue
    .line 34
    new-instance v3, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v3}, Lsoftware/simplicial/a/f/bl;-><init>()V

    .line 35
    move/from16 v0, p1

    iput v0, v3, Lsoftware/simplicial/a/f/bl;->a:I

    .line 36
    move/from16 v0, p1

    iput v0, v3, Lsoftware/simplicial/a/f/bl;->b:I

    .line 37
    const/16 v2, 0x7fff

    iput-short v2, v3, Lsoftware/simplicial/a/f/bl;->d:S

    .line 38
    const/4 v4, -0x2

    const-string v10, ""

    const/4 v2, 0x0

    new-array v11, v2, [B

    sget-object v13, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const-wide/16 v14, 0x0

    move-object/from16 v2, p0

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v12, p8

    move/from16 v16, p9

    move/from16 v17, p10

    invoke-super/range {v2 .. v17}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 40
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->c:I

    .line 41
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->f:I

    .line 42
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->h:I

    .line 43
    sget-object v2, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    .line 45
    sget-object v2, Lsoftware/simplicial/a/i$1;->a:[I

    invoke-virtual/range {p5 .. p5}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 49
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->bG:I

    .line 50
    const/16 v2, 0x50

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->e:I

    .line 51
    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->a:I

    .line 52
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->b:I

    .line 67
    :goto_0
    return-void

    .line 55
    :pswitch_0
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->bG:I

    .line 56
    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->e:I

    .line 57
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->a:I

    .line 58
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->b:I

    goto :goto_0

    .line 61
    :pswitch_1
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->bG:I

    .line 62
    const/16 v2, 0x78

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->e:I

    .line 63
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->a:I

    .line 64
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/i;->b:I

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bf;)V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bf;)V

    .line 91
    iget v0, p0, Lsoftware/simplicial/a/i;->a:I

    iput v0, p0, Lsoftware/simplicial/a/i;->h:I

    .line 92
    return-void
.end method

.method public a(Lsoftware/simplicial/a/i$a;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    if-ne v0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    sget-object v1, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    if-ne v0, v1, :cond_2

    .line 76
    const/16 v0, 0x1e

    iput v0, p0, Lsoftware/simplicial/a/i;->g:I

    .line 79
    :cond_2
    iget v0, p0, Lsoftware/simplicial/a/i;->f:I

    if-lez v0, :cond_3

    sget-object v0, Lsoftware/simplicial/a/i$a;->d:Lsoftware/simplicial/a/i$a;

    if-ne p1, v0, :cond_0

    .line 81
    :cond_3
    iput-object p1, p0, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    .line 82
    iget v0, p0, Lsoftware/simplicial/a/i;->bG:I

    iput v0, p0, Lsoftware/simplicial/a/i;->f:I

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/i;->d:I

    goto :goto_0
.end method

.method public r_()V
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lsoftware/simplicial/a/i;->b:I

    iput v0, p0, Lsoftware/simplicial/a/i;->i:I

    .line 97
    return-void
.end method
