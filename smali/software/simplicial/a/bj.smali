.class public final enum Lsoftware/simplicial/a/bj;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/bj;

.field public static final enum b:Lsoftware/simplicial/a/bj;

.field public static final enum c:Lsoftware/simplicial/a/bj;

.field private static final synthetic d:[Lsoftware/simplicial/a/bj;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/bj;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    new-instance v0, Lsoftware/simplicial/a/bj;

    const-string v1, "NOT_PLAYING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bj;->b:Lsoftware/simplicial/a/bj;

    new-instance v0, Lsoftware/simplicial/a/bj;

    const-string v1, "MULTI"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/a/bj;

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/bj;->b:Lsoftware/simplicial/a/bj;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/a/bj;->d:[Lsoftware/simplicial/a/bj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bj;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/bj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bj;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bj;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/bj;->d:[Lsoftware/simplicial/a/bj;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bj;

    return-object v0
.end method
