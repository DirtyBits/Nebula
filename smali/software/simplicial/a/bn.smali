.class public final enum Lsoftware/simplicial/a/bn;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/bn;

.field public static final enum b:Lsoftware/simplicial/a/bn;

.field public static final enum c:Lsoftware/simplicial/a/bn;

.field public static final enum d:Lsoftware/simplicial/a/bn;

.field public static final enum e:Lsoftware/simplicial/a/bn;

.field public static final enum f:Lsoftware/simplicial/a/bn;

.field public static final enum g:Lsoftware/simplicial/a/bn;

.field public static final enum h:Lsoftware/simplicial/a/bn;

.field public static final enum i:Lsoftware/simplicial/a/bn;

.field public static final j:[Lsoftware/simplicial/a/bn;

.field private static final synthetic k:[Lsoftware/simplicial/a/bn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "US_EAST"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "US_WEST"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->b:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "EU"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "EAST_ASIA"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->d:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "SOUTH_ASIA"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->e:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "SOUTH_AMERICA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "AUSTRALIA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->g:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "DEBUG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->h:Lsoftware/simplicial/a/bn;

    new-instance v0, Lsoftware/simplicial/a/bn;

    const-string v1, "DEBUG_GLOBAL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bn;->i:Lsoftware/simplicial/a/bn;

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [Lsoftware/simplicial/a/bn;

    sget-object v1, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bn;->b:Lsoftware/simplicial/a/bn;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/bn;->d:Lsoftware/simplicial/a/bn;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/bn;->e:Lsoftware/simplicial/a/bn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/bn;->g:Lsoftware/simplicial/a/bn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/bn;->h:Lsoftware/simplicial/a/bn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/bn;->i:Lsoftware/simplicial/a/bn;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/bn;->k:[Lsoftware/simplicial/a/bn;

    .line 11
    invoke-static {}, Lsoftware/simplicial/a/bn;->values()[Lsoftware/simplicial/a/bn;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/bn;->j:[Lsoftware/simplicial/a/bn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lsoftware/simplicial/a/bn;
    .locals 1

    .prologue
    .line 17
    :try_start_0
    invoke-static {p0}, Lsoftware/simplicial/a/bn;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bn;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 28
    :goto_0
    return-object v0

    .line 19
    :catch_0
    move-exception v0

    .line 23
    const-string v0, "BRAZIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    sget-object v0, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 25
    :cond_0
    const-string v0, "EUROPE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    sget-object v0, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 28
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/bn;->h:Lsoftware/simplicial/a/bn;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bn;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/bn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bn;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bn;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/bn;->k:[Lsoftware/simplicial/a/bn;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bn;

    return-object v0
.end method
