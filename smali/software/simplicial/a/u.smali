.class public Lsoftware/simplicial/a/u;
.super Lsoftware/simplicial/a/ai;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public A:J

.field public B:I

.field public C:J

.field public D:Z

.field public E:Lsoftware/simplicial/a/s;

.field public F:Ljava/util/concurrent/atomic/AtomicInteger;

.field public G:F

.field public H:Z

.field public I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bq;",
            ">;"
        }
    .end annotation
.end field

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ae;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bt;",
            ">;"
        }
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bh;",
            ">;"
        }
    .end annotation
.end field

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/a/x;",
            ">;"
        }
    .end annotation
.end field

.field public N:Z

.field public O:J

.field public P:F

.field private bc:Lsoftware/simplicial/a/bf;

.field private bd:Lsoftware/simplicial/a/f/bg;

.field private be:J

.field private bf:J

.field private bg:S

.field private bh:S

.field private bi:Z

.field private bj:Z

.field private bk:B

.field private bl:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;"
        }
    .end annotation
.end field

.field private bm:I

.field public final c:[I

.field public final d:[I

.field public e:Z

.field public f:I

.field public g:F

.field public h:Z

.field public i:I

.field public volatile j:I

.field public k:J

.field public l:J

.field public m:Lsoftware/simplicial/a/aa;

.field public n:I

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Lsoftware/simplicial/a/c/g;

.field public w:I

.field public x:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public y:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lsoftware/simplicial/a/u;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lsoftware/simplicial/a/u;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bg;Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BILsoftware/simplicial/a/bj;Z)V
    .locals 24

    .prologue
    .line 125
    const/16 v5, 0x3c

    const-wide/16 v10, 0x0

    const/16 v14, 0x7fff

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x7fff

    sget-object v22, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    const/16 v23, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v15, p10

    move/from16 v16, p11

    move/from16 v20, p9

    invoke-direct/range {v3 .. v23}, Lsoftware/simplicial/a/ai;-><init>(Lsoftware/simplicial/a/al;ILsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;JLjava/lang/String;[BSLsoftware/simplicial/a/bj;Z[ZZIISLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/i/a;)V

    .line 90
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->A:J

    .line 91
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->B:I

    .line 92
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->C:J

    .line 93
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->D:Z

    .line 104
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->bl:Ljava/util/Set;

    .line 105
    sget-object v2, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->E:Lsoftware/simplicial/a/s;

    .line 106
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->bm:I

    .line 107
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 112
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->I:Ljava/util/List;

    .line 113
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->J:Ljava/util/List;

    .line 114
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->K:Ljava/util/List;

    .line 115
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->L:Ljava/util/List;

    .line 116
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->M:Ljava/util/List;

    .line 118
    const-wide v2, 0x7fffffffffffffffL

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->O:J

    .line 120
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->P:F

    .line 129
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->at:Ljava/util/Collection;

    .line 130
    const/4 v2, 0x5

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->c:[I

    .line 131
    const/4 v2, 0x5

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->d:[I

    .line 132
    new-instance v2, Lsoftware/simplicial/a/bf;

    invoke-direct {v2}, Lsoftware/simplicial/a/bf;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    new-instance v3, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v3}, Lsoftware/simplicial/a/f/bl;-><init>()V

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sget-object v8, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    sget-object v9, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const-string v10, ""

    const/4 v11, 0x0

    new-array v11, v11, [B

    const/4 v12, 0x0

    sget-object v13, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    .line 134
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/u;->p()I

    move-result v17

    .line 133
    invoke-virtual/range {v2 .. v17}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 135
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->e:Z

    .line 136
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->f:I

    .line 137
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->g:F

    .line 138
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->h:Z

    .line 139
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->i:I

    .line 140
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->j:I

    .line 141
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->k:J

    .line 142
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->l:J

    .line 143
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->D:Z

    .line 144
    sget-object v2, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 145
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->n:I

    .line 146
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->o:Z

    .line 147
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->p:Z

    .line 148
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->q:Z

    .line 149
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->t:Z

    .line 150
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->u:Z

    .line 151
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->r:Z

    .line 152
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->s:Z

    .line 153
    sget-object v2, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->v:Lsoftware/simplicial/a/c/g;

    .line 154
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/u;->w:I

    .line 156
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/u;->bd:Lsoftware/simplicial/a/f/bg;

    .line 157
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->be:J

    .line 158
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/u;->bf:J

    .line 159
    const/16 v2, 0x7fff

    move-object/from16 v0, p0

    iput-short v2, v0, Lsoftware/simplicial/a/u;->bh:S

    .line 160
    const/16 v2, 0x7fff

    move-object/from16 v0, p0

    iput-short v2, v0, Lsoftware/simplicial/a/u;->bg:S

    .line 161
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->bi:Z

    .line 162
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->bj:Z

    .line 163
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-byte v2, v0, Lsoftware/simplicial/a/u;->bk:B

    .line 164
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 165
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 167
    sget-object v2, Lsoftware/simplicial/a/f/al;->a:Lsoftware/simplicial/a/f/bv;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bv;->a()V

    .line 169
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 170
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v3, v3, v2

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, -0x40800000    # -1.0f

    int-to-short v6, v2

    invoke-virtual {v3, v4, v5, v6}, Lsoftware/simplicial/a/ae;->a(FFS)V

    .line 169
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    :cond_0
    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    array-length v2, v2

    if-ge v3, v2, :cond_1

    .line 172
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    aget-object v2, v2, v3

    sget v4, Lsoftware/simplicial/a/u;->T:F

    neg-float v4, v4

    sget v5, Lsoftware/simplicial/a/u;->T:F

    neg-float v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lsoftware/simplicial/a/g$a;->a:Lsoftware/simplicial/a/g$a;

    const v9, 0x417a6666    # 15.65f

    invoke-virtual/range {v2 .. v9}, Lsoftware/simplicial/a/g;->a(IFFFFLsoftware/simplicial/a/g$a;F)V

    .line 171
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 173
    :cond_1
    const/4 v2, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/u;->c:[I

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 174
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/u;->c:[I

    const/4 v4, -0x1

    aput v4, v3, v2

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 175
    :cond_2
    const/4 v2, 0x0

    move v8, v2

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v2, v2

    if-ge v8, v2, :cond_4

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v2, v2, v8

    invoke-static/range {p6 .. p6}, Lsoftware/simplicial/a/u;->e(Lsoftware/simplicial/a/ap;)I

    move-result v3

    if-ge v8, v3, :cond_3

    sget-object v3, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    :goto_4
    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, -0x40800000    # -1.0f

    int-to-byte v6, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/u;->ad:Ljava/util/Random;

    invoke-virtual/range {v2 .. v7}, Lsoftware/simplicial/a/bt;->a(Lsoftware/simplicial/a/bv;FFBLjava/util/Random;)V

    .line 175
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_3

    .line 176
    :cond_3
    sget-object v3, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    goto :goto_4

    .line 178
    :cond_4
    sget-object v2, Lsoftware/simplicial/a/u;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 179
    return-void
.end method

.method private a(Lsoftware/simplicial/a/s;)I
    .locals 1

    .prologue
    .line 1275
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/u;->b(Lsoftware/simplicial/a/s;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private a(FF)Lsoftware/simplicial/a/bh;
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 374
    const/4 v2, 0x0

    .line 375
    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 376
    iget-object v7, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v8, v7

    move v4, v5

    :goto_0
    if-ge v4, v8, :cond_3

    aget-object v1, v7, v4

    .line 378
    iget-object v9, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v10, v9

    move v6, v5

    :goto_1
    if-ge v6, v10, :cond_2

    aget-object v3, v9, v6

    .line 380
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    .line 378
    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-object v2, v1

    goto :goto_1

    .line 383
    :cond_0
    iget-short v1, v3, Lsoftware/simplicial/a/bh;->z:S

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_4

    .line 385
    if-nez v2, :cond_1

    move-object v1, v3

    .line 386
    goto :goto_2

    .line 389
    :cond_1
    iget v1, v3, Lsoftware/simplicial/a/bh;->l:F

    sub-float v1, p1, v1

    .line 390
    iget v11, v3, Lsoftware/simplicial/a/bh;->m:F

    sub-float v11, p2, v11

    .line 391
    mul-float/2addr v1, v1

    mul-float/2addr v11, v11

    add-float/2addr v1, v11

    .line 392
    cmpg-float v11, v1, v0

    if-gez v11, :cond_4

    move v0, v1

    move-object v1, v3

    .line 395
    goto :goto_2

    .line 376
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 401
    :cond_3
    return-object v2

    :cond_4
    move-object v1, v2

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/a/y;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 904
    iget v0, p0, Lsoftware/simplicial/a/u;->aY:I

    const/16 v1, 0x10

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/a/y;->a(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 907
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/u$1;->a:[I

    iget-object v1, p1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v1}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 910
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_2

    .line 911
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    const/high16 v1, 0x40400000    # 3.0f

    iput v1, v0, Lsoftware/simplicial/a/bf;->al:F

    .line 912
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_0

    .line 913
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    const/high16 v1, 0x3fc00000    # 1.5f

    iput v1, v0, Lsoftware/simplicial/a/bf;->al:F

    goto :goto_0

    .line 916
    :pswitch_1
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->D:Z

    if-eqz v0, :cond_0

    .line 918
    check-cast p1, Lsoftware/simplicial/a/a/aj;

    .line 919
    iget v0, p0, Lsoftware/simplicial/a/u;->z:I

    iget v1, p1, Lsoftware/simplicial/a/a/aj;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/u;->z:I

    .line 920
    iget-wide v0, p0, Lsoftware/simplicial/a/u;->A:J

    iget v2, p1, Lsoftware/simplicial/a/a/aj;->a:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/a/u;->A:J

    goto :goto_0

    .line 924
    :pswitch_2
    check-cast p1, Lsoftware/simplicial/a/a/ak;

    .line 925
    iget-wide v0, p1, Lsoftware/simplicial/a/a/ak;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 927
    iget-wide v0, p1, Lsoftware/simplicial/a/a/ak;->a:J

    iput-wide v0, p0, Lsoftware/simplicial/a/u;->A:J

    .line 928
    iget-byte v0, p1, Lsoftware/simplicial/a/a/ak;->b:B

    iput v0, p0, Lsoftware/simplicial/a/u;->B:I

    .line 929
    iget v0, p1, Lsoftware/simplicial/a/a/ak;->c:I

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/u;->C:J

    .line 930
    iput-boolean v4, p0, Lsoftware/simplicial/a/u;->D:Z

    goto :goto_0

    .line 934
    :pswitch_3
    check-cast p1, Lsoftware/simplicial/a/a/n;

    .line 936
    sget-object v0, Lsoftware/simplicial/a/aa;->q:Ljava/util/Map;

    iget-byte v1, p1, Lsoftware/simplicial/a/a/n;->a:B

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 937
    iget-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    if-nez v0, :cond_3

    .line 938
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 939
    :cond_3
    iget-boolean v0, p1, Lsoftware/simplicial/a/a/n;->b:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->o:Z

    .line 940
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/u;->n:I

    goto :goto_0

    .line 943
    :pswitch_4
    check-cast p1, Lsoftware/simplicial/a/a/l;

    .line 944
    sget-object v0, Lsoftware/simplicial/a/aa;->q:Ljava/util/Map;

    iget-byte v1, p1, Lsoftware/simplicial/a/a/l;->a:B

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 945
    iget-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    if-nez v0, :cond_4

    .line 946
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    .line 947
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    iget v0, v0, Lsoftware/simplicial/a/aa;->s:I

    iput v0, p0, Lsoftware/simplicial/a/u;->n:I

    .line 948
    iput-boolean v4, p0, Lsoftware/simplicial/a/u;->o:Z

    goto/16 :goto_0

    .line 951
    :pswitch_5
    check-cast p1, Lsoftware/simplicial/a/a/m;

    .line 952
    iget-short v0, p1, Lsoftware/simplicial/a/a/m;->a:S

    iput v0, p0, Lsoftware/simplicial/a/u;->n:I

    goto/16 :goto_0

    .line 955
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/a/u;->M:Ljava/util/List;

    check-cast p1, Lsoftware/simplicial/a/a/x;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 907
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/a/y;FFI)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    .line 963
    iget v0, p0, Lsoftware/simplicial/a/u;->aY:I

    const/16 v1, 0x10

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/a/y;->a(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 966
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/u$1;->a:[I

    iget-object v1, p1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v1}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1079
    :pswitch_0
    iget v0, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float v3, v0, v2

    .line 1080
    iget v0, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float v4, v0, v2

    .line 1081
    iget v0, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float p2, v0, v2

    .line 1082
    iget v0, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float p3, v0, v2

    move v6, p3

    move v5, p2

    .line 1087
    :goto_1
    sget-object v0, Lsoftware/simplicial/a/u$1;->a:[I

    iget-object v1, p1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v1}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 1100
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-virtual {v1}, Lsoftware/simplicial/a/bf;->d()I

    move-result v1

    int-to-float v7, v1

    iget v8, p0, Lsoftware/simplicial/a/u;->aE:F

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v8}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;Lsoftware/simplicial/a/a/y;FFFFFF)V

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 969
    check-cast v0, Lsoftware/simplicial/a/a/c;

    .line 970
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/c;->a:B

    aget-object v1, v1, v2

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/c;->b:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bh;->l:F

    .line 971
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/c;->a:B

    aget-object v1, v1, v2

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/c;->b:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bh;->m:F

    move v6, p3

    move v5, p2

    .line 974
    goto :goto_1

    :pswitch_2
    move-object v0, p1

    .line 976
    check-cast v0, Lsoftware/simplicial/a/a/e;

    .line 977
    iget-byte v1, v0, Lsoftware/simplicial/a/a/e;->a:B

    if-ne v1, p4, :cond_0

    .line 979
    iget-short v1, v0, Lsoftware/simplicial/a/a/e;->c:S

    const/16 v2, 0x400

    if-ne v1, v2, :cond_2

    .line 980
    const/high16 v1, 0x40400000    # 3.0f

    iput v1, p0, Lsoftware/simplicial/a/u;->P:F

    .line 981
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/e;->a:B

    aget-object v1, v1, v2

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/e;->b:B

    aget-object v0, v1, v0

    .line 982
    iget v3, v0, Lsoftware/simplicial/a/bh;->l:F

    .line 983
    iget v4, v0, Lsoftware/simplicial/a/bh;->m:F

    move v6, p3

    move v5, p2

    .line 986
    goto :goto_1

    :pswitch_3
    move-object v0, p1

    .line 988
    check-cast v0, Lsoftware/simplicial/a/a/ag;

    .line 989
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/ag;->a:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bf;->l:F

    .line 990
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/ag;->a:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bf;->m:F

    move v6, p3

    move v5, p2

    .line 993
    goto :goto_1

    :pswitch_4
    move-object v0, p1

    .line 995
    check-cast v0, Lsoftware/simplicial/a/a/ae;

    .line 996
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/ae;->a:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bf;->l:F

    .line 997
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/ae;->a:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bf;->m:F

    move v6, p3

    move v5, p2

    .line 1000
    goto/16 :goto_1

    :pswitch_5
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1004
    goto/16 :goto_1

    :pswitch_6
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1008
    goto/16 :goto_1

    :pswitch_7
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1012
    goto/16 :goto_1

    :pswitch_8
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1016
    goto/16 :goto_1

    :pswitch_9
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1020
    goto/16 :goto_1

    :pswitch_a
    move-object v0, p1

    .line 1022
    check-cast v0, Lsoftware/simplicial/a/a/w;

    .line 1023
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/w;->a:B

    aget-object v1, v1, v2

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/w;->b:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bh;->l:F

    .line 1024
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/w;->a:B

    aget-object v1, v1, v2

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/w;->b:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bh;->m:F

    move v6, p3

    move v5, p2

    .line 1027
    goto/16 :goto_1

    :pswitch_b
    move-object v0, p1

    .line 1029
    check-cast v0, Lsoftware/simplicial/a/a/af;

    .line 1030
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/af;->a:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bf;->l:F

    .line 1031
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/af;->a:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bf;->m:F

    move v6, p3

    move v5, p2

    .line 1034
    goto/16 :goto_1

    :pswitch_c
    move-object v0, p1

    .line 1036
    check-cast v0, Lsoftware/simplicial/a/a/ac;

    .line 1037
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, v0, Lsoftware/simplicial/a/a/ac;->a:B

    aget-object v1, v1, v2

    iget v3, v1, Lsoftware/simplicial/a/bf;->l:F

    .line 1038
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/ac;->a:B

    aget-object v0, v1, v0

    iget v4, v0, Lsoftware/simplicial/a/bf;->m:F

    move v6, p3

    move v5, p2

    .line 1041
    goto/16 :goto_1

    :pswitch_d
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1047
    goto/16 :goto_1

    :pswitch_e
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1053
    goto/16 :goto_1

    :pswitch_f
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1059
    goto/16 :goto_1

    :pswitch_10
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1065
    goto/16 :goto_1

    :pswitch_11
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1071
    goto/16 :goto_1

    :pswitch_12
    move v6, p3

    move v5, p2

    move v4, p3

    move v3, p2

    .line 1077
    goto/16 :goto_1

    :sswitch_0
    move-object v0, p1

    .line 1090
    check-cast v0, Lsoftware/simplicial/a/a/a;

    .line 1091
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1092
    sget-object v2, Lsoftware/simplicial/a/d;->bv:Ljava/util/Map;

    iget-short v0, v0, Lsoftware/simplicial/a/a/a;->a:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1093
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-interface {v0, p0, v1, v2, v7}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;Ljava/util/Collection;Lsoftware/simplicial/a/bf;Z)V

    goto/16 :goto_2

    :sswitch_1
    move-object v0, p1

    .line 1096
    check-cast v0, Lsoftware/simplicial/a/a/l;

    .line 1097
    iget-object v1, p0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    sget-object v2, Lsoftware/simplicial/a/aa;->q:Ljava/util/Map;

    iget-byte v0, v0, Lsoftware/simplicial/a/a/l;->a:B

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/aa;

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-interface {v1, v0, v2, v7}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/aa;Lsoftware/simplicial/a/bf;Z)V

    goto/16 :goto_2

    .line 966
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch

    .line 1087
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Lsoftware/simplicial/a/f/ai;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 1363
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1365
    iget v4, v3, Lsoftware/simplicial/a/bf;->A:I

    iget v5, p1, Lsoftware/simplicial/a/f/ai;->d:I

    if-ne v4, v5, :cond_0

    iget v4, p1, Lsoftware/simplicial/a/f/ai;->d:I

    if-eq v4, v6, :cond_0

    iget v4, p1, Lsoftware/simplicial/a/f/ai;->d:I

    if-le v4, v6, :cond_0

    .line 1367
    iget-object v4, p1, Lsoftware/simplicial/a/f/ai;->a:Ljava/lang/String;

    iput-object v4, v3, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 1368
    iget-object v4, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    iput-object v4, v3, Lsoftware/simplicial/a/bf;->E:[B

    .line 1363
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1371
    :cond_1
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/ak;)V
    .locals 7

    .prologue
    .line 1375
    iget-object v0, p1, Lsoftware/simplicial/a/f/ak;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bi;

    .line 1377
    iget v2, v0, Lsoftware/simplicial/a/bi;->a:I

    if-ltz v2, :cond_0

    iget v2, v0, Lsoftware/simplicial/a/bi;->a:I

    iget-object v3, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1380
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v3, v0, Lsoftware/simplicial/a/bi;->a:I

    aget-object v2, v2, v3

    .line 1382
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->k:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 1383
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->l:[B

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->E:[B

    .line 1384
    iget-short v3, v0, Lsoftware/simplicial/a/bi;->b:S

    if-ltz v3, :cond_1

    iget-short v3, v0, Lsoftware/simplicial/a/bi;->b:S

    sget-object v4, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 1385
    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    iget-short v4, v0, Lsoftware/simplicial/a/bi;->b:S

    aget-object v3, v3, v4

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 1388
    :goto_1
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->c:Lsoftware/simplicial/a/af;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    .line 1389
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->d:Lsoftware/simplicial/a/as;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    .line 1390
    iget-byte v3, v0, Lsoftware/simplicial/a/bi;->e:B

    iget-short v4, v0, Lsoftware/simplicial/a/bi;->f:S

    invoke-static {v3, v4}, Lsoftware/simplicial/a/bd;->a(IS)Lsoftware/simplicial/a/bd;

    move-result-object v3

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    .line 1391
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->g:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 1392
    iget v3, v0, Lsoftware/simplicial/a/bi;->h:I

    iput v3, v2, Lsoftware/simplicial/a/bf;->ad:I

    .line 1393
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->r:Lsoftware/simplicial/a/s;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 1394
    iget v3, v0, Lsoftware/simplicial/a/bi;->i:I

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/bf;->c(I)V

    .line 1395
    iget v3, v0, Lsoftware/simplicial/a/bi;->m:I

    iput v3, v2, Lsoftware/simplicial/a/bf;->A:I

    .line 1396
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->o:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    .line 1397
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->p:[B

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->I:[B

    .line 1398
    iget-object v3, v0, Lsoftware/simplicial/a/bi;->q:Lsoftware/simplicial/a/q;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    .line 1399
    iget-short v3, v0, Lsoftware/simplicial/a/bi;->n:S

    iput v3, v2, Lsoftware/simplicial/a/bf;->bD:I

    .line 1401
    iget-byte v0, v0, Lsoftware/simplicial/a/bi;->j:B

    .line 1402
    if-ltz v0, :cond_2

    iget v3, p0, Lsoftware/simplicial/a/u;->aj:I

    if-ge v0, v3, :cond_2

    .line 1403
    iget-object v3, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v0, v3, v0

    iput-object v0, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_0

    .line 1387
    :cond_1
    sget-object v3, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    goto :goto_1

    .line 1405
    :cond_2
    const/4 v0, 0x0

    iput-object v0, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto/16 :goto_0

    .line 1408
    :cond_3
    iget-object v0, p1, Lsoftware/simplicial/a/f/ak;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/br;

    .line 1410
    iget v2, v0, Lsoftware/simplicial/a/br;->a:I

    if-ltz v2, :cond_4

    iget v2, v0, Lsoftware/simplicial/a/br;->a:I

    iget-object v3, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 1413
    iget-object v2, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    iget v3, v0, Lsoftware/simplicial/a/br;->a:I

    aget-object v2, v2, v3

    .line 1414
    iget v3, v0, Lsoftware/simplicial/a/br;->b:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 1415
    iget v3, v0, Lsoftware/simplicial/a/br;->c:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    .line 1416
    iget v3, v0, Lsoftware/simplicial/a/br;->b:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->s:F

    .line 1417
    iget v3, v0, Lsoftware/simplicial/a/br;->c:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->t:F

    .line 1418
    iget v0, v0, Lsoftware/simplicial/a/br;->d:F

    invoke-virtual {v2, v0}, Lsoftware/simplicial/a/bq;->c(F)V

    .line 1419
    const/4 v0, -0x1

    iput v0, v2, Lsoftware/simplicial/a/bq;->C:I

    .line 1420
    sget-object v0, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    iput-object v0, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 1421
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v0, v2, Lsoftware/simplicial/a/bq;->K:Lsoftware/simplicial/a/af;

    .line 1422
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v0, v2, Lsoftware/simplicial/a/bq;->L:Lsoftware/simplicial/a/as;

    .line 1423
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v0, v2, Lsoftware/simplicial/a/bq;->N:Lsoftware/simplicial/a/s;

    .line 1424
    const v0, -0x7f7f80

    iput v0, v2, Lsoftware/simplicial/a/bq;->I:I

    .line 1425
    const v0, -0x5f5f60

    iput v0, v2, Lsoftware/simplicial/a/bq;->J:I

    goto :goto_2

    .line 1428
    :cond_5
    iget-object v0, p1, Lsoftware/simplicial/a/f/ak;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/ad;

    .line 1430
    iget v2, v0, Lsoftware/simplicial/a/f/ad;->a:I

    if-ltz v2, :cond_6

    iget v2, v0, Lsoftware/simplicial/a/f/ad;->a:I

    iget-object v3, p0, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 1433
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    iget v3, v0, Lsoftware/simplicial/a/f/ad;->a:I

    aget-object v2, v2, v3

    .line 1434
    iget v3, v0, Lsoftware/simplicial/a/f/ad;->b:F

    iput v3, v2, Lsoftware/simplicial/a/ae;->s:F

    .line 1435
    iget v0, v0, Lsoftware/simplicial/a/f/ad;->c:F

    iput v0, v2, Lsoftware/simplicial/a/ae;->t:F

    goto :goto_3

    .line 1438
    :cond_7
    iget-object v0, p1, Lsoftware/simplicial/a/f/ak;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lsoftware/simplicial/a/bu;

    .line 1440
    iget v0, v4, Lsoftware/simplicial/a/bu;->a:I

    if-ltz v0, :cond_8

    iget v0, v4, Lsoftware/simplicial/a/bu;->a:I

    iget-object v1, p0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 1443
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    iget v1, v4, Lsoftware/simplicial/a/bu;->a:I

    aget-object v0, v0, v1

    .line 1445
    iget v1, v4, Lsoftware/simplicial/a/bu;->b:I

    if-ltz v1, :cond_9

    iget v1, v4, Lsoftware/simplicial/a/bu;->b:I

    sget-object v2, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 1446
    sget-object v1, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    iget v2, v4, Lsoftware/simplicial/a/bu;->b:I

    aget-object v1, v1, v2

    .line 1450
    :goto_5
    iget v2, v4, Lsoftware/simplicial/a/bu;->c:F

    iget v3, v4, Lsoftware/simplicial/a/bu;->d:F

    iget v4, v4, Lsoftware/simplicial/a/bu;->a:I

    int-to-byte v4, v4

    iget-object v5, p0, Lsoftware/simplicial/a/u;->ad:Ljava/util/Random;

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/a/bt;->a(Lsoftware/simplicial/a/bv;FFBLjava/util/Random;)V

    goto :goto_4

    .line 1448
    :cond_9
    sget-object v1, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    goto :goto_5

    .line 1453
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->N:Z

    .line 1454
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bc;)V
    .locals 7

    .prologue
    .line 1357
    invoke-virtual {p0}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v5

    .line 1358
    iget-object v0, v5, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v5, Lsoftware/simplicial/a/bf;->A:I

    iget-object v2, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    iget-object v3, p0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    iget-object v4, v5, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v5, v5, Lsoftware/simplicial/a/bf;->E:[B

    iget-boolean v6, p0, Lsoftware/simplicial/a/u;->aR:Z

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    .line 1359
    return-void
.end method

.method private b(Lsoftware/simplicial/a/s;)I
    .locals 1

    .prologue
    .line 1280
    sget-object v0, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    if-ne p1, v0, :cond_0

    .line 1281
    const/4 v0, 0x2

    .line 1284
    :goto_0
    return v0

    .line 1282
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    if-ne p1, v0, :cond_1

    .line 1283
    const/4 v0, 0x1

    goto :goto_0

    .line 1284
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 406
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    iget-short v1, p0, Lsoftware/simplicial/a/u;->bh:S

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1

    iget-short v0, p0, Lsoftware/simplicial/a/u;->bg:S

    if-ltz v0, :cond_1

    :cond_0
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    const/16 v1, 0x7fff

    if-ne v0, v1, :cond_2

    iget-short v0, p0, Lsoftware/simplicial/a/u;->bg:S

    const/4 v1, -0x5

    if-lt v0, v1, :cond_2

    iget-short v0, p0, Lsoftware/simplicial/a/u;->bg:S

    const/4 v1, -0x1

    if-gt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1211
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v0, :cond_4

    .line 1213
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1215
    iget-object v0, p0, Lsoftware/simplicial/a/u;->E:Lsoftware/simplicial/a/s;

    .line 1216
    sget-object v1, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    if-ne v0, v1, :cond_5

    .line 1218
    const/4 v1, -0x1

    iput v1, p0, Lsoftware/simplicial/a/u;->bm:I

    .line 1228
    :cond_0
    :goto_0
    iget-wide v2, p0, Lsoftware/simplicial/a/u;->be:J

    sub-long v2, v10, v2

    const-wide/16 v4, 0x2a

    cmp-long v1, v2, v4

    if-ltz v1, :cond_4

    .line 1230
    iget v1, p0, Lsoftware/simplicial/a/u;->bm:I

    if-ltz v1, :cond_1

    .line 1232
    iget v1, p0, Lsoftware/simplicial/a/u;->bm:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lsoftware/simplicial/a/u;->bm:I

    .line 1234
    iget v1, p0, Lsoftware/simplicial/a/u;->bm:I

    if-gtz v1, :cond_1

    .line 1236
    iget-object v1, p0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1237
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/u;->b(Lsoftware/simplicial/a/s;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/u;->bm:I

    .line 1241
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/u;->x:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v5

    .line 1242
    iget-object v0, p0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v6

    .line 1243
    iget-object v0, p0, Lsoftware/simplicial/a/u;->F:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v8}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndSet(I)I

    move-result v7

    .line 1245
    iget-object v9, p0, Lsoftware/simplicial/a/u;->bd:Lsoftware/simplicial/a/f/bg;

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v1, p0, Lsoftware/simplicial/a/u;->bd:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v1}, Lsoftware/simplicial/a/f/bg;->c()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v2, Lsoftware/simplicial/a/bf;->N:F

    iget-object v3, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v3, v3, Lsoftware/simplicial/a/bf;->P:F

    iget-byte v4, p0, Lsoftware/simplicial/a/u;->bk:B

    add-int/lit8 v12, v4, 0x1

    int-to-byte v12, v12

    iput-byte v12, p0, Lsoftware/simplicial/a/u;->bk:B

    invoke-static/range {v0 .. v7}, Lsoftware/simplicial/a/f/ab;->a(Lsoftware/simplicial/a/f/bn;IFFBZZI)[B

    move-result-object v0

    invoke-interface {v9, v0}, Lsoftware/simplicial/a/f/bg;->a([B)V

    .line 1248
    if-eqz v5, :cond_2

    .line 1249
    iput-wide v10, p0, Lsoftware/simplicial/a/u;->l:J

    .line 1250
    :cond_2
    if-eqz v6, :cond_3

    .line 1251
    iput-wide v10, p0, Lsoftware/simplicial/a/u;->k:J

    .line 1253
    :cond_3
    iput-wide v10, p0, Lsoftware/simplicial/a/u;->be:J

    .line 1257
    :cond_4
    iput v8, p0, Lsoftware/simplicial/a/u;->z:I

    .line 1258
    iget-object v0, p0, Lsoftware/simplicial/a/u;->at:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 1259
    iget-object v0, p0, Lsoftware/simplicial/a/u;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1260
    iget-object v0, p0, Lsoftware/simplicial/a/u;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1261
    iget-object v0, p0, Lsoftware/simplicial/a/u;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1262
    iget-object v0, p0, Lsoftware/simplicial/a/u;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1263
    iget-object v0, p0, Lsoftware/simplicial/a/u;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1264
    iput-boolean v8, p0, Lsoftware/simplicial/a/u;->N:Z

    .line 1266
    iget-object v1, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v1

    move v0, v8

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 1268
    invoke-virtual {v3}, Lsoftware/simplicial/a/bf;->m()V

    .line 1266
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1220
    :cond_5
    iget v1, p0, Lsoftware/simplicial/a/u;->bm:I

    if-gez v1, :cond_0

    .line 1222
    iget-object v1, p0, Lsoftware/simplicial/a/u;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1223
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/s;)I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/u;->bm:I

    goto/16 :goto_0

    .line 1271
    :cond_6
    return-void
.end method

.method private s()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1483
    move v0, v1

    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1485
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v0

    .line 1487
    iget v3, v2, Lsoftware/simplicial/a/bf;->S:I

    if-nez v3, :cond_1

    .line 1483
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1490
    :cond_1
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 1492
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1490
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1495
    :cond_2
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->a()V

    goto :goto_2

    :cond_3
    move v0, v1

    .line 1499
    :goto_3
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 1501
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    aget-object v2, v2, v0

    .line 1503
    iget v3, v2, Lsoftware/simplicial/a/g;->y:I

    if-lez v3, :cond_4

    .line 1505
    iget v3, v2, Lsoftware/simplicial/a/g;->e:F

    const v4, 0x3b5a740e

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/g;->e:F

    .line 1506
    invoke-virtual {v2}, Lsoftware/simplicial/a/g;->a()V

    .line 1499
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v1

    .line 1510
    :goto_4
    iget-object v2, p0, Lsoftware/simplicial/a/u;->av:[Lsoftware/simplicial/a/bl;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 1512
    iget-object v2, p0, Lsoftware/simplicial/a/u;->av:[Lsoftware/simplicial/a/bl;

    aget-object v2, v2, v0

    .line 1514
    iget v3, v2, Lsoftware/simplicial/a/bl;->y:I

    if-lez v3, :cond_6

    .line 1516
    iget v3, p0, Lsoftware/simplicial/a/u;->ab:F

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/bl;->a(F)V

    .line 1517
    invoke-virtual {v2}, Lsoftware/simplicial/a/bl;->a()V

    .line 1510
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    move v0, v1

    .line 1521
    :goto_5
    iget-object v2, p0, Lsoftware/simplicial/a/u;->aq:[Lsoftware/simplicial/a/ca;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 1523
    iget-object v2, p0, Lsoftware/simplicial/a/u;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v2, v2, v0

    .line 1525
    invoke-virtual {v2}, Lsoftware/simplicial/a/ca;->a()V

    .line 1521
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    move v0, v1

    .line 1528
    :goto_6
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 1530
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v2, v2, v0

    .line 1532
    iget v3, p0, Lsoftware/simplicial/a/u;->ab:F

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/bt;->a(F)V

    .line 1528
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    move v0, v1

    .line 1535
    :goto_7
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 1537
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v2, v2, v0

    .line 1539
    invoke-virtual {v2}, Lsoftware/simplicial/a/ag;->a()V

    .line 1535
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_a
    move v0, v1

    .line 1542
    :goto_8
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ax:[Lsoftware/simplicial/a/z;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 1544
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ax:[Lsoftware/simplicial/a/z;

    aget-object v2, v2, v0

    .line 1546
    invoke-virtual {v2}, Lsoftware/simplicial/a/z;->a()V

    .line 1542
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_b
    move v0, v1

    .line 1549
    :goto_9
    iget v2, p0, Lsoftware/simplicial/a/u;->aj:I

    if-ge v0, v2, :cond_c

    .line 1551
    iget-object v2, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v2, v0

    .line 1553
    invoke-virtual {v2}, Lsoftware/simplicial/a/bx;->a()V

    .line 1549
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1556
    :cond_c
    :goto_a
    iget-object v0, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    array-length v0, v0

    if-ge v1, v0, :cond_e

    .line 1558
    iget-object v0, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    aget-object v0, v0, v1

    .line 1560
    iget-object v2, v0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v3, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    if-ne v2, v3, :cond_d

    .line 1561
    invoke-virtual {v0}, Lsoftware/simplicial/a/bq;->a()V

    .line 1556
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1563
    :cond_e
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 13

    .prologue
    const/high16 v12, -0x40800000    # -1.0f

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 186
    sget-object v0, Lsoftware/simplicial/a/u;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 187
    invoke-direct {p0}, Lsoftware/simplicial/a/u;->q()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->H:Z

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v2, Lsoftware/simplicial/a/bf;->M:F

    iput v2, v0, Lsoftware/simplicial/a/bf;->N:F

    .line 190
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v2, Lsoftware/simplicial/a/bf;->O:F

    iput v2, v0, Lsoftware/simplicial/a/bf;->P:F

    .line 192
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->o()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->bi:Z

    .line 193
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->n()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->bj:Z

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bl:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bz;

    .line 196
    invoke-virtual {v0}, Lsoftware/simplicial/a/bz;->c()V

    goto :goto_0

    .line 197
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bl:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move v0, v1

    .line 199
    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/u;->az:I

    if-ge v0, v2, :cond_1

    .line 201
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    aget-object v2, v2, v0

    .line 203
    invoke-virtual {v2}, Lsoftware/simplicial/a/g;->j()V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 206
    :goto_2
    iget v2, p0, Lsoftware/simplicial/a/u;->au:I

    if-ge v0, v2, :cond_6

    .line 208
    iget-object v2, p0, Lsoftware/simplicial/a/u;->av:[Lsoftware/simplicial/a/bl;

    aget-object v2, v2, v0

    .line 210
    invoke-virtual {v2}, Lsoftware/simplicial/a/bl;->j()V

    .line 212
    iget-object v3, v2, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v4, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-ne v3, v4, :cond_4

    .line 214
    iget-object v3, v2, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    sget-object v4, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    if-ne v3, v4, :cond_5

    .line 216
    iget-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget-short v3, v3, Lsoftware/simplicial/a/bh;->z:S

    and-int/lit16 v3, v3, 0x200

    if-eqz v3, :cond_2

    iget-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 217
    :cond_2
    const/4 v3, 0x0

    iput-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 218
    :cond_3
    iget-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    if-nez v3, :cond_4

    .line 219
    iget v3, v2, Lsoftware/simplicial/a/bl;->l:F

    iget v4, v2, Lsoftware/simplicial/a/bl;->m:F

    invoke-direct {p0, v3, v4}, Lsoftware/simplicial/a/u;->a(FF)Lsoftware/simplicial/a/bh;

    move-result-object v3

    iput-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 206
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 223
    :cond_5
    const/4 v3, 0x0

    iput-object v3, v2, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    goto :goto_3

    :cond_6
    move v0, v1

    .line 229
    :goto_4
    iget v2, p0, Lsoftware/simplicial/a/u;->aY:I

    if-ge v0, v2, :cond_e

    .line 231
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v2, v0

    .line 233
    iget v2, v3, Lsoftware/simplicial/a/bf;->S:I

    if-nez v2, :cond_7

    .line 235
    iget v2, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float/2addr v2, v11

    iput v2, v3, Lsoftware/simplicial/a/bf;->l:F

    .line 236
    iget v2, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float/2addr v2, v11

    iput v2, v3, Lsoftware/simplicial/a/bf;->m:F

    .line 229
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 240
    :cond_7
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    cmpl-float v2, v2, v9

    if-lez v2, :cond_8

    .line 242
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    iget v4, p0, Lsoftware/simplicial/a/u;->ab:F

    sub-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/bf;->al:F

    .line 243
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    cmpg-float v2, v2, v9

    if-gez v2, :cond_8

    .line 244
    iput v9, v3, Lsoftware/simplicial/a/bf;->al:F

    .line 248
    :cond_8
    iget-boolean v2, p0, Lsoftware/simplicial/a/u;->H:Z

    if-eqz v2, :cond_9

    move v2, v1

    .line 249
    :goto_6
    iget v4, v3, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v4, :cond_9

    .line 250
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v4, v4, v2

    invoke-virtual {v4, v1}, Lsoftware/simplicial/a/bh;->a(I)V

    .line 249
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 252
    :cond_9
    iget-byte v2, v3, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_a

    .line 254
    invoke-virtual {p0}, Lsoftware/simplicial/a/u;->o()I

    move-result v2

    iput v2, v3, Lsoftware/simplicial/a/bf;->ah:I

    .line 255
    iget v2, v3, Lsoftware/simplicial/a/bf;->ah:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v2

    iput v2, v3, Lsoftware/simplicial/a/bf;->ai:I

    .line 258
    :cond_a
    iput v9, v3, Lsoftware/simplicial/a/bf;->l:F

    .line 259
    iput v9, v3, Lsoftware/simplicial/a/bf;->m:F

    .line 260
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v5, v4

    move v2, v1

    :goto_7
    if-ge v2, v5, :cond_d

    aget-object v6, v4, v2

    .line 262
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 260
    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 265
    :cond_b
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->j()V

    .line 266
    iget v7, v6, Lsoftware/simplicial/a/bh;->y:I

    if-nez v7, :cond_c

    .line 268
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->e()V

    .line 269
    iget v6, v3, Lsoftware/simplicial/a/bf;->S:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v3, Lsoftware/simplicial/a/bf;->S:I

    goto :goto_8

    .line 273
    :cond_c
    iget v7, v3, Lsoftware/simplicial/a/bf;->l:F

    iget v8, v6, Lsoftware/simplicial/a/bh;->l:F

    add-float/2addr v7, v8

    iput v7, v3, Lsoftware/simplicial/a/bf;->l:F

    .line 274
    iget v7, v3, Lsoftware/simplicial/a/bf;->m:F

    iget v6, v6, Lsoftware/simplicial/a/bh;->m:F

    add-float/2addr v6, v7

    iput v6, v3, Lsoftware/simplicial/a/bf;->m:F

    goto :goto_8

    .line 277
    :cond_d
    iget v2, v3, Lsoftware/simplicial/a/bf;->l:F

    iget v4, v3, Lsoftware/simplicial/a/bf;->S:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/bf;->l:F

    .line 278
    iget v2, v3, Lsoftware/simplicial/a/bf;->m:F

    iget v4, v3, Lsoftware/simplicial/a/bf;->S:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/bf;->m:F

    goto/16 :goto_5

    .line 281
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    sget-object v2, Lsoftware/simplicial/a/aa;->j:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_f

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v2, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne v0, v2, :cond_f

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->d()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/u;->n:I

    .line 284
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    sget-object v2, Lsoftware/simplicial/a/aa;->k:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v2, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne v0, v2, :cond_10

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->d()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/u;->n:I

    .line 287
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-nez v0, :cond_18

    .line 289
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->bB:I

    if-lez v0, :cond_11

    .line 290
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v0, Lsoftware/simplicial/a/bf;->bB:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lsoftware/simplicial/a/bf;->bB:I

    .line 291
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->bB:I

    if-gtz v0, :cond_15

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput v1, v0, Lsoftware/simplicial/a/bf;->bB:I

    .line 295
    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v0, :cond_12

    iget-wide v2, p0, Lsoftware/simplicial/a/u;->aO:J

    iget-wide v4, p0, Lsoftware/simplicial/a/u;->bf:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v0, v2, v4

    if-lez v0, :cond_12

    .line 296
    iput v10, p0, Lsoftware/simplicial/a/u;->g:F

    .line 298
    :cond_12
    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    cmpl-float v0, v0, v9

    if-nez v0, :cond_13

    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    if-lez v0, :cond_13

    .line 299
    iput-boolean v1, p0, Lsoftware/simplicial/a/u;->e:Z

    .line 301
    :cond_13
    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    cmpl-float v0, v0, v9

    if-lez v0, :cond_15

    .line 303
    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    .line 304
    iget v2, p0, Lsoftware/simplicial/a/u;->g:F

    iget v3, p0, Lsoftware/simplicial/a/u;->ab:F

    sub-float/2addr v2, v3

    iput v2, p0, Lsoftware/simplicial/a/u;->g:F

    .line 305
    const v2, 0x3eaaaaab

    cmpl-float v0, v0, v2

    if-lez v0, :cond_14

    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    const v2, 0x3eaaaaab

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_14

    .line 306
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    invoke-interface {v0, p0}, Lsoftware/simplicial/a/al;->b(Lsoftware/simplicial/a/ai;)V

    .line 307
    :cond_14
    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    cmpg-float v0, v0, v9

    if-gez v0, :cond_15

    .line 308
    iput v9, p0, Lsoftware/simplicial/a/u;->g:F

    .line 319
    :cond_15
    :goto_9
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->bB:I

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->h:Z

    if-eqz v0, :cond_19

    .line 321
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v0, Lsoftware/simplicial/a/bf;->aD:F

    iget v3, p0, Lsoftware/simplicial/a/u;->ab:F

    add-float/2addr v2, v3

    iput v2, v0, Lsoftware/simplicial/a/bf;->aD:F

    .line 322
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->aD:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_17

    .line 323
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput v10, v0, Lsoftware/simplicial/a/bf;->aD:F

    .line 330
    :cond_17
    :goto_a
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->H:Z

    if-eqz v0, :cond_1c

    .line 332
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput v1, v0, Lsoftware/simplicial/a/ay;->A:I

    .line 333
    iput v1, p0, Lsoftware/simplicial/a/u;->n:I

    .line 334
    iput v9, p0, Lsoftware/simplicial/a/u;->P:F

    move v0, v1

    .line 336
    :goto_b
    iget-object v2, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    array-length v2, v2

    if-ge v0, v2, :cond_1a

    .line 338
    iget-object v2, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v2, v0

    .line 340
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 336
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 314
    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->e:Z

    .line 315
    iput v12, p0, Lsoftware/simplicial/a/u;->g:F

    .line 316
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput v9, v0, Lsoftware/simplicial/a/bf;->aD:F

    goto :goto_9

    .line 327
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput v9, v0, Lsoftware/simplicial/a/bf;->aD:F

    goto :goto_a

    :cond_1a
    move v0, v1

    .line 342
    :goto_c
    iget v2, p0, Lsoftware/simplicial/a/u;->ar:I

    if-ge v0, v2, :cond_1c

    move v2, v1

    .line 344
    :goto_d
    iget v3, p0, Lsoftware/simplicial/a/u;->ar:I

    if-ge v2, v3, :cond_1b

    .line 346
    iget-object v3, p0, Lsoftware/simplicial/a/u;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v3, v3, v2

    aget-object v3, v3, v0

    .line 348
    invoke-virtual {v3}, Lsoftware/simplicial/a/bz;->b()V

    .line 344
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 342
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 353
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_1d

    .line 355
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ax:[Lsoftware/simplicial/a/z;

    aget-object v0, v0, v1

    iget v0, v0, Lsoftware/simplicial/a/z;->n:F

    iput v0, p0, Lsoftware/simplicial/a/u;->aF:F

    .line 356
    iget v0, p0, Lsoftware/simplicial/a/u;->G:F

    iget v1, p0, Lsoftware/simplicial/a/u;->aF:F

    iget v2, p0, Lsoftware/simplicial/a/u;->G:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v11

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/u;->G:F

    .line 359
    :cond_1d
    iget v0, p0, Lsoftware/simplicial/a/u;->P:F

    cmpl-float v0, v0, v9

    if-lez v0, :cond_1e

    .line 361
    iget v0, p0, Lsoftware/simplicial/a/u;->P:F

    iget v1, p0, Lsoftware/simplicial/a/u;->ab:F

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/u;->P:F

    .line 362
    iget v0, p0, Lsoftware/simplicial/a/u;->P:F

    cmpg-float v0, v0, v9

    if-gez v0, :cond_1e

    .line 363
    iput v9, p0, Lsoftware/simplicial/a/u;->P:F

    .line 366
    :cond_1e
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    iput-short v0, p0, Lsoftware/simplicial/a/u;->bg:S

    .line 367
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    if-lez v0, :cond_1f

    .line 368
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    iput-short v0, p0, Lsoftware/simplicial/a/u;->bh:S

    .line 370
    :cond_1f
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/ac;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 425
    const/4 v3, 0x0

    move v0, v1

    .line 426
    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/u;->aY:I

    if-ge v0, v2, :cond_2

    .line 428
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v0

    .line 431
    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->k()I

    move-result v4

    iget v5, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    if-ne v4, v5, :cond_1

    move-object v0, v2

    .line 437
    :goto_1
    if-eqz v0, :cond_0

    .line 439
    iput v1, v0, Lsoftware/simplicial/a/bf;->S:I

    .line 440
    const/4 v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/bf;->A:I

    .line 443
    :cond_0
    return-void

    .line 426
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/f/al;)V
    .locals 13

    .prologue
    const/high16 v12, -0x800000    # Float.NEGATIVE_INFINITY

    const/16 v11, 0x24

    const/16 v10, 0x12

    const/4 v9, -0x1

    const/4 v1, 0x0

    .line 536
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/al;->ab:Z

    if-eqz v0, :cond_1

    .line 899
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    iget v0, p1, Lsoftware/simplicial/a/f/al;->ac:F

    iput v0, p0, Lsoftware/simplicial/a/u;->aE:F

    .line 547
    const/4 v0, 0x0

    .line 548
    iget v2, p0, Lsoftware/simplicial/a/u;->j:I

    if-ltz v2, :cond_4

    iget v2, p0, Lsoftware/simplicial/a/u;->j:I

    iget v3, p0, Lsoftware/simplicial/a/u;->aY:I

    if-ge v2, v3, :cond_4

    .line 549
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v2, p0, Lsoftware/simplicial/a/u;->j:I

    aget-object v0, v0, v2

    .line 553
    :cond_2
    :goto_1
    if-nez v0, :cond_3

    .line 554
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    .line 556
    :cond_3
    iget v2, v0, Lsoftware/simplicial/a/bf;->l:F

    .line 557
    iget v3, v0, Lsoftware/simplicial/a/bf;->m:F

    .line 558
    iget v4, v0, Lsoftware/simplicial/a/bf;->C:I

    .line 561
    iget-byte v0, p1, Lsoftware/simplicial/a/f/al;->af:B

    if-lez v0, :cond_5

    move v0, v1

    .line 563
    :goto_2
    iget-byte v5, p1, Lsoftware/simplicial/a/f/al;->af:B

    if-ge v0, v5, :cond_5

    .line 564
    iget-object v5, p1, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    aget-object v5, v5, v0

    invoke-direct {p0, v5, v2, v3, v4}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/a/y;FFI)V

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 550
    :cond_4
    iget v2, p0, Lsoftware/simplicial/a/u;->j:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_2

    .line 551
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v0}, Lsoftware/simplicial/a/ai;->a([Lsoftware/simplicial/a/bf;)Lsoftware/simplicial/a/bf;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 569
    :goto_3
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->af:B

    if-ge v0, v2, :cond_6

    .line 570
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/a/y;)V

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move v0, v1

    .line 573
    :goto_4
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->am:B

    if-ge v0, v2, :cond_e

    .line 575
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->k:[B

    aget-byte v2, v2, v0

    .line 576
    if-ltz v2, :cond_7

    iget-object v3, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-lt v2, v3, :cond_8

    .line 573
    :cond_7
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 578
    :cond_8
    iget-object v3, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v3, v2

    .line 580
    iget v3, v2, Lsoftware/simplicial/a/bq;->s:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->u:F

    .line 581
    iget v3, v2, Lsoftware/simplicial/a/bq;->t:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->v:F

    .line 582
    iget v3, v2, Lsoftware/simplicial/a/bq;->w:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->x:F

    .line 584
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->l:[B

    aget-byte v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->C:I

    .line 585
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->m:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 586
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->n:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    .line 587
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->o:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->j:F

    .line 588
    iget v3, v2, Lsoftware/simplicial/a/bq;->C:I

    if-ltz v3, :cond_c

    iget v3, v2, Lsoftware/simplicial/a/bq;->C:I

    iget v4, p0, Lsoftware/simplicial/a/u;->aY:I

    if-ge v3, v4, :cond_c

    .line 590
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v4, v2, Lsoftware/simplicial/a/bq;->C:I

    aget-object v3, v3, v4

    .line 591
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    iput-object v4, v2, Lsoftware/simplicial/a/bq;->K:Lsoftware/simplicial/a/af;

    .line 592
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    iput-object v4, v2, Lsoftware/simplicial/a/bq;->N:Lsoftware/simplicial/a/s;

    .line 593
    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v4, v5, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v4, v5, :cond_b

    :cond_9
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v4, :cond_b

    .line 597
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->g:I

    iput v4, v2, Lsoftware/simplicial/a/bq;->I:I

    .line 598
    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->h:I

    iput v3, v2, Lsoftware/simplicial/a/bq;->J:I

    .line 613
    :goto_6
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->p:[F

    aget v3, v3, v0

    .line 614
    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_a

    .line 615
    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/bq;->c(F)V

    .line 616
    :cond_a
    cmpl-float v3, v3, v12

    if-nez v3, :cond_d

    .line 617
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 621
    :goto_7
    iget v3, v2, Lsoftware/simplicial/a/bq;->l:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->s:F

    .line 622
    iget v3, v2, Lsoftware/simplicial/a/bq;->m:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->t:F

    .line 623
    iget v3, v2, Lsoftware/simplicial/a/bq;->n:F

    iput v3, v2, Lsoftware/simplicial/a/bq;->w:F

    .line 624
    iget-object v3, p0, Lsoftware/simplicial/a/u;->I:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 602
    :cond_b
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v4, v2, Lsoftware/simplicial/a/bq;->C:I

    aget-object v3, v3, v4

    iget v3, v3, Lsoftware/simplicial/a/bf;->ah:I

    iput v3, v2, Lsoftware/simplicial/a/bq;->I:I

    .line 603
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v4, v2, Lsoftware/simplicial/a/bq;->C:I

    aget-object v3, v3, v4

    iget v3, v3, Lsoftware/simplicial/a/bf;->ai:I

    iput v3, v2, Lsoftware/simplicial/a/bq;->J:I

    goto :goto_6

    .line 608
    :cond_c
    sget-object v3, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->K:Lsoftware/simplicial/a/af;

    .line 609
    sget-object v3, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->N:Lsoftware/simplicial/a/s;

    .line 610
    const v3, -0x7f7f80

    iput v3, v2, Lsoftware/simplicial/a/bq;->I:I

    .line 611
    const v3, -0x5f5f60

    iput v3, v2, Lsoftware/simplicial/a/bq;->J:I

    goto :goto_6

    .line 619
    :cond_d
    sget-object v3, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    goto :goto_7

    :cond_e
    move v0, v1

    .line 627
    :goto_8
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->al:B

    if-ge v0, v2, :cond_12

    .line 629
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->q:[B

    aget-byte v2, v2, v0

    .line 630
    if-ltz v2, :cond_f

    iget-object v3, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-lt v2, v3, :cond_10

    .line 627
    :cond_f
    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 632
    :cond_10
    iget-object v3, p0, Lsoftware/simplicial/a/u;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v3, v2

    .line 634
    iget-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    if-eq v3, v4, :cond_11

    .line 636
    iget-object v3, p0, Lsoftware/simplicial/a/u;->I:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->r:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->s:F

    .line 638
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->s:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->t:F

    .line 641
    :cond_11
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->r:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 642
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->s:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    .line 643
    sget-object v3, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    goto :goto_9

    :cond_12
    move v0, v1

    .line 646
    :goto_a
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->ag:B

    if-ge v0, v2, :cond_15

    .line 648
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->t:[S

    aget-short v2, v2, v0

    .line 649
    if-ltz v2, :cond_13

    iget v3, p0, Lsoftware/simplicial/a/u;->ay:I

    if-lt v2, v3, :cond_14

    .line 646
    :cond_13
    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 651
    :cond_14
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v2, v3, v2

    .line 653
    iget v3, v2, Lsoftware/simplicial/a/ae;->s:F

    iput v3, v2, Lsoftware/simplicial/a/ae;->u:F

    .line 654
    iget v3, v2, Lsoftware/simplicial/a/ae;->t:F

    iput v3, v2, Lsoftware/simplicial/a/ae;->v:F

    .line 655
    iget v3, v2, Lsoftware/simplicial/a/ae;->w:F

    iput v3, v2, Lsoftware/simplicial/a/ae;->x:F

    .line 657
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->u:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ae;->s:F

    .line 658
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->v:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ae;->t:F

    .line 660
    iget-object v3, p0, Lsoftware/simplicial/a/u;->J:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_15
    move v0, v1

    .line 663
    :goto_c
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->ai:B

    if-ge v0, v2, :cond_1a

    .line 665
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->w:[B

    aget-byte v2, v2, v0

    .line 666
    if-ltz v2, :cond_16

    iget v3, p0, Lsoftware/simplicial/a/u;->aA:I

    if-lt v2, v3, :cond_17

    .line 663
    :cond_16
    :goto_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 668
    :cond_17
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v3, v3, v2

    .line 670
    iget v2, v3, Lsoftware/simplicial/a/bt;->s:F

    iput v2, v3, Lsoftware/simplicial/a/bt;->u:F

    .line 671
    iget v2, v3, Lsoftware/simplicial/a/bt;->t:F

    iput v2, v3, Lsoftware/simplicial/a/bt;->v:F

    .line 672
    iget v2, v3, Lsoftware/simplicial/a/bt;->w:F

    iput v2, v3, Lsoftware/simplicial/a/bt;->x:F

    .line 673
    iget-object v2, v3, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    iput-object v2, v3, Lsoftware/simplicial/a/bt;->d:Lsoftware/simplicial/a/bv;

    .line 674
    iget v2, v3, Lsoftware/simplicial/a/bt;->c:I

    iput v2, v3, Lsoftware/simplicial/a/bt;->e:I

    .line 677
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->x:[B

    aget-byte v2, v2, v0

    if-ltz v2, :cond_18

    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->x:[B

    aget-byte v2, v2, v0

    sget-object v4, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    array-length v4, v4

    if-lt v2, v4, :cond_19

    .line 678
    :cond_18
    sget-object v2, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    .line 682
    :goto_e
    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->y:[F

    aget v4, v4, v0

    iput v4, v3, Lsoftware/simplicial/a/bt;->s:F

    .line 683
    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->z:[F

    aget v4, v4, v0

    iput v4, v3, Lsoftware/simplicial/a/bt;->t:F

    .line 684
    iput-object v2, v3, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    .line 686
    iget-object v2, p0, Lsoftware/simplicial/a/u;->K:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 680
    :cond_19
    sget-object v2, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->x:[B

    aget-byte v4, v4, v0

    aget-object v2, v2, v4

    goto :goto_e

    :cond_1a
    move v0, v1

    .line 689
    :goto_f
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->ad:B

    if-ge v0, v2, :cond_1e

    .line 691
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->b:[B

    aget-byte v3, v2, v0

    .line 693
    if-ne v3, v9, :cond_1d

    .line 695
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    array-length v2, v2

    if-nez v2, :cond_1c

    .line 689
    :cond_1b
    :goto_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 698
    :cond_1c
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v2, v2, v1

    .line 699
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ak:Lsoftware/simplicial/a/bx;

    iput-object v3, v2, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    .line 709
    :goto_11
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->c:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ag;->l:F

    .line 710
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->d:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ag;->m:F

    goto :goto_10

    .line 703
    :cond_1d
    if-ltz v3, :cond_1b

    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    array-length v2, v2

    if-ge v3, v2, :cond_1b

    iget v2, p0, Lsoftware/simplicial/a/u;->aj:I

    if-ge v3, v2, :cond_1b

    .line 705
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v2, v2, v3

    .line 706
    iget-object v4, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v3, v4, v3

    iput-object v3, v2, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    goto :goto_11

    :cond_1e
    move v0, v1

    .line 713
    :goto_12
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->ae:B

    if-ge v0, v2, :cond_22

    .line 715
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->e:[B

    aget-byte v2, v2, v0

    .line 716
    if-lt v2, v9, :cond_1f

    iget v3, p0, Lsoftware/simplicial/a/u;->aj:I

    if-lt v2, v3, :cond_20

    .line 713
    :cond_1f
    :goto_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 719
    :cond_20
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ax:[Lsoftware/simplicial/a/z;

    array-length v3, v3

    if-ge v0, v3, :cond_1f

    .line 723
    iget-object v3, p0, Lsoftware/simplicial/a/u;->ax:[Lsoftware/simplicial/a/z;

    aget-object v3, v3, v0

    .line 724
    if-le v2, v9, :cond_21

    iget-object v4, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v4, v2

    :goto_14
    iput-object v2, v3, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    .line 726
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->f:[F

    aget v2, v2, v0

    iput v2, v3, Lsoftware/simplicial/a/z;->l:F

    .line 727
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->g:[F

    aget v2, v2, v0

    iput v2, v3, Lsoftware/simplicial/a/z;->m:F

    .line 728
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->h:[F

    aget v2, v2, v0

    iput v2, v3, Lsoftware/simplicial/a/z;->n:F

    .line 729
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->i:[F

    aget v2, v2, v0

    iput v2, v3, Lsoftware/simplicial/a/z;->b:F

    .line 730
    iget v2, p0, Lsoftware/simplicial/a/u;->aE:F

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/z;->a(F)V

    goto :goto_13

    .line 724
    :cond_21
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_14

    :cond_22
    move v0, v1

    .line 733
    :goto_15
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->aj:B

    if-ge v0, v2, :cond_27

    .line 735
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->A:[B

    aget-byte v2, v2, v0

    .line 736
    if-ltz v2, :cond_23

    iget-object v3, p0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    array-length v3, v3

    if-lt v2, v3, :cond_24

    .line 733
    :cond_23
    :goto_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 738
    :cond_24
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ai:[Lsoftware/simplicial/a/g;

    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->A:[B

    aget-byte v3, v3, v0

    aget-object v2, v2, v3

    .line 740
    sget-object v3, Lsoftware/simplicial/a/g$a;->d:[Lsoftware/simplicial/a/g$a;

    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->B:[B

    aget-byte v4, v4, v0

    aget-object v3, v3, v4

    iput-object v3, v2, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    .line 741
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->E:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/g;->n:F

    .line 742
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->C:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/g;->l:F

    .line 743
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->D:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/g;->m:F

    .line 745
    iget v3, v2, Lsoftware/simplicial/a/g;->n:F

    const v4, 0x40163d71    # 2.3475f

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_25

    .line 747
    iget v3, v2, Lsoftware/simplicial/a/g;->l:F

    iput v3, v2, Lsoftware/simplicial/a/g;->s:F

    .line 748
    iget v3, v2, Lsoftware/simplicial/a/g;->m:F

    iput v3, v2, Lsoftware/simplicial/a/g;->t:F

    .line 749
    iget v3, v2, Lsoftware/simplicial/a/g;->n:F

    iput v3, v2, Lsoftware/simplicial/a/g;->w:F

    .line 752
    :cond_25
    iget-object v3, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v3, v3, Lsoftware/simplicial/a/bf;->S:I

    if-lez v3, :cond_26

    .line 753
    invoke-virtual {v2, v10}, Lsoftware/simplicial/a/g;->a(I)V

    goto :goto_16

    .line 755
    :cond_26
    invoke-virtual {v2, v11}, Lsoftware/simplicial/a/g;->a(I)V

    goto :goto_16

    :cond_27
    move v0, v1

    .line 758
    :goto_17
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->an:B

    if-ge v0, v2, :cond_2b

    .line 760
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->O:[B

    aget-byte v2, v2, v0

    .line 762
    if-ltz v2, :cond_28

    iget-object v3, p0, Lsoftware/simplicial/a/u;->av:[Lsoftware/simplicial/a/bl;

    array-length v3, v3

    if-lt v2, v3, :cond_29

    .line 758
    :cond_28
    :goto_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 764
    :cond_29
    iget-object v3, p0, Lsoftware/simplicial/a/u;->av:[Lsoftware/simplicial/a/bl;

    aget-object v2, v3, v2

    .line 766
    sget-object v3, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->P:[B

    aget-byte v4, v4, v0

    aget-object v3, v3, v4

    iput-object v3, v2, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    .line 767
    sget-object v3, Lsoftware/simplicial/a/bl$a;->d:[Lsoftware/simplicial/a/bl$a;

    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->Q:[B

    aget-byte v4, v4, v0

    aget-object v3, v3, v4

    iput-object v3, v2, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 768
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->R:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bl;->l:F

    .line 769
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->S:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/bl;->m:F

    .line 770
    iget-object v3, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v3, v3, Lsoftware/simplicial/a/bf;->S:I

    if-lez v3, :cond_2a

    .line 771
    invoke-virtual {v2, v10}, Lsoftware/simplicial/a/bl;->a(I)V

    goto :goto_18

    .line 773
    :cond_2a
    invoke-virtual {v2, v11}, Lsoftware/simplicial/a/bl;->a(I)V

    goto :goto_18

    :cond_2b
    move v0, v1

    .line 776
    :goto_19
    iget-byte v2, p1, Lsoftware/simplicial/a/f/al;->ao:B

    if-ge v0, v2, :cond_2f

    .line 778
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->T:[B

    aget-byte v2, v2, v0

    .line 779
    if-ltz v2, :cond_2c

    iget-object v3, p0, Lsoftware/simplicial/a/u;->aq:[Lsoftware/simplicial/a/ca;

    array-length v3, v3

    if-lt v2, v3, :cond_2d

    .line 776
    :cond_2c
    :goto_1a
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 781
    :cond_2d
    iget-object v3, p0, Lsoftware/simplicial/a/u;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v2, v3, v2

    .line 784
    iget v3, v2, Lsoftware/simplicial/a/ca;->b:F

    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->U:[F

    aget v4, v4, v0

    sub-float/2addr v3, v4

    .line 785
    iget v4, v2, Lsoftware/simplicial/a/ca;->c:F

    iget-object v5, p1, Lsoftware/simplicial/a/f/al;->V:[F

    aget v5, v5, v0

    sub-float/2addr v4, v5

    .line 786
    iget v5, v2, Lsoftware/simplicial/a/ca;->d:F

    iget-object v6, p1, Lsoftware/simplicial/a/f/al;->W:[F

    aget v6, v6, v0

    sub-float/2addr v5, v6

    .line 787
    iget v6, v2, Lsoftware/simplicial/a/ca;->e:F

    iget-object v7, p1, Lsoftware/simplicial/a/f/al;->X:[F

    aget v7, v7, v0

    sub-float/2addr v6, v7

    .line 788
    mul-float/2addr v3, v3

    mul-float/2addr v4, v4

    add-float/2addr v3, v4

    mul-float v4, v5, v5

    add-float/2addr v3, v4

    mul-float v4, v6, v6

    add-float/2addr v3, v4

    .line 789
    iget v4, v2, Lsoftware/simplicial/a/ca;->b:F

    iget-object v5, p1, Lsoftware/simplicial/a/f/al;->W:[F

    aget v5, v5, v0

    sub-float/2addr v4, v5

    .line 790
    iget v5, v2, Lsoftware/simplicial/a/ca;->c:F

    iget-object v6, p1, Lsoftware/simplicial/a/f/al;->X:[F

    aget v6, v6, v0

    sub-float/2addr v5, v6

    .line 791
    iget v6, v2, Lsoftware/simplicial/a/ca;->d:F

    iget-object v7, p1, Lsoftware/simplicial/a/f/al;->U:[F

    aget v7, v7, v0

    sub-float/2addr v6, v7

    .line 792
    iget v7, v2, Lsoftware/simplicial/a/ca;->e:F

    iget-object v8, p1, Lsoftware/simplicial/a/f/al;->V:[F

    aget v8, v8, v0

    sub-float/2addr v7, v8

    .line 793
    mul-float/2addr v4, v4

    mul-float/2addr v5, v5

    add-float/2addr v4, v5

    mul-float v5, v6, v6

    add-float/2addr v4, v5

    mul-float v5, v7, v7

    add-float/2addr v4, v5

    .line 795
    cmpg-float v3, v3, v4

    if-gez v3, :cond_2e

    .line 797
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->U:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->b:F

    .line 798
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->V:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->c:F

    .line 799
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->W:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->d:F

    .line 800
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->X:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->e:F

    goto :goto_1a

    .line 804
    :cond_2e
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->W:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->b:F

    .line 805
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->X:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->c:F

    .line 806
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->U:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->d:F

    .line 807
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->V:[F

    aget v3, v3, v0

    iput v3, v2, Lsoftware/simplicial/a/ca;->e:F

    goto/16 :goto_1a

    :cond_2f
    move v0, v1

    .line 811
    :goto_1b
    iget-short v2, p1, Lsoftware/simplicial/a/f/al;->ah:S

    if-ge v0, v2, :cond_33

    .line 813
    iget-object v2, p1, Lsoftware/simplicial/a/f/al;->Y:[B

    aget-byte v2, v2, v0

    .line 814
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->Z:[B

    aget-byte v3, v3, v0

    .line 815
    iget-object v4, p1, Lsoftware/simplicial/a/f/al;->aa:[B

    aget-byte v4, v4, v0

    .line 816
    if-ltz v2, :cond_30

    iget v5, p0, Lsoftware/simplicial/a/u;->ar:I

    if-ge v2, v5, :cond_30

    if-ltz v3, :cond_30

    iget v5, p0, Lsoftware/simplicial/a/u;->ar:I

    if-ge v3, v5, :cond_30

    if-lt v4, v9, :cond_30

    iget v5, p0, Lsoftware/simplicial/a/u;->aj:I

    if-lt v4, v5, :cond_31

    .line 811
    :cond_30
    :goto_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    .line 818
    :cond_31
    iget-object v5, p0, Lsoftware/simplicial/a/u;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v5, v2

    aget-object v3, v2, v3

    .line 820
    if-ltz v4, :cond_32

    iget-object v2, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v2, v4

    :goto_1d
    iput-object v2, v3, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    .line 821
    iget-object v2, p0, Lsoftware/simplicial/a/u;->at:Ljava/util/Collection;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 820
    :cond_32
    const/4 v2, 0x0

    goto :goto_1d

    .line 824
    :cond_33
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v5, v0, Lsoftware/simplicial/a/bf;->S:I

    move v2, v1

    move v0, v1

    .line 826
    :goto_1e
    iget-byte v3, p1, Lsoftware/simplicial/a/f/al;->ak:B

    if-ge v2, v3, :cond_40

    .line 828
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->F:[B

    aget-byte v3, v3, v2

    .line 829
    if-ltz v3, :cond_44

    iget-object v4, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v4, v4

    if-lt v3, v4, :cond_34

    move v3, v0

    .line 826
    :goto_1f
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1e

    .line 831
    :cond_34
    iget-object v4, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v6, v4, v3

    .line 832
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->H:[B

    aget-byte v3, v3, v2

    iput-byte v3, v6, Lsoftware/simplicial/a/bf;->am:B

    .line 833
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->G:[B

    aget-byte v3, v3, v2

    int-to-float v3, v3

    iput v3, v6, Lsoftware/simplicial/a/bf;->T:F

    .line 834
    iget-byte v3, v6, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_35

    .line 835
    invoke-virtual {p0, v6}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/bf;)V

    .line 837
    :cond_35
    iget-object v3, p1, Lsoftware/simplicial/a/f/al;->I:[B

    aget-byte v7, v3, v2

    move v4, v1

    move v3, v0

    .line 838
    :goto_20
    if-ge v4, v7, :cond_3d

    .line 840
    iget-object v0, p1, Lsoftware/simplicial/a/f/al;->J:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    .line 841
    if-ltz v0, :cond_43

    iget-object v8, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v8, v8

    if-lt v0, v8, :cond_36

    move v0, v3

    .line 838
    :goto_21
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_20

    .line 843
    :cond_36
    iget-object v8, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v8, v8, v0

    .line 845
    iget-object v0, p1, Lsoftware/simplicial/a/f/al;->K:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    iput-short v0, v8, Lsoftware/simplicial/a/bh;->z:S

    .line 846
    iget-object v0, p1, Lsoftware/simplicial/a/f/al;->L:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v8, Lsoftware/simplicial/a/bh;->l:F

    .line 847
    iget-object v0, p1, Lsoftware/simplicial/a/f/al;->M:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v8, Lsoftware/simplicial/a/bh;->m:F

    .line 848
    iget-object v0, p1, Lsoftware/simplicial/a/f/al;->N:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 849
    const/4 v9, 0x0

    cmpl-float v9, v0, v9

    if-lez v9, :cond_37

    .line 850
    invoke-virtual {v8, v0}, Lsoftware/simplicial/a/bh;->c(F)V

    .line 851
    :cond_37
    iget-object v9, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    if-ne v6, v9, :cond_3a

    .line 852
    const/16 v9, 0xb4

    invoke-virtual {v8, v9}, Lsoftware/simplicial/a/bh;->a(I)V

    .line 858
    :goto_22
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v9

    if-eqz v9, :cond_38

    .line 860
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->g()V

    .line 861
    iget-object v9, p0, Lsoftware/simplicial/a/u;->L:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 862
    iget v9, v6, Lsoftware/simplicial/a/bf;->S:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v6, Lsoftware/simplicial/a/bf;->S:I

    .line 865
    :cond_38
    cmpl-float v0, v0, v12

    if-nez v0, :cond_3c

    .line 867
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->e()V

    .line 868
    iget v0, v6, Lsoftware/simplicial/a/bf;->S:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v6, Lsoftware/simplicial/a/bf;->S:I

    .line 875
    :cond_39
    :goto_23
    add-int/lit8 v0, v3, 0x1

    goto :goto_21

    .line 853
    :cond_3a
    iget-object v9, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v9, v9, Lsoftware/simplicial/a/bf;->S:I

    if-lez v9, :cond_3b

    .line 854
    invoke-virtual {v8, v10}, Lsoftware/simplicial/a/bh;->a(I)V

    goto :goto_22

    .line 856
    :cond_3b
    invoke-virtual {v8, v11}, Lsoftware/simplicial/a/bh;->a(I)V

    goto :goto_22

    .line 870
    :cond_3c
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v9, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-ne v0, v9, :cond_39

    iget-object v0, p0, Lsoftware/simplicial/a/u;->aa:Lsoftware/simplicial/a/bj;

    sget-object v9, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v9, :cond_39

    iget-byte v0, v6, Lsoftware/simplicial/a/bf;->am:B

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_39

    .line 872
    iget-object v0, p0, Lsoftware/simplicial/a/u;->at:Ljava/util/Collection;

    iget-object v9, p0, Lsoftware/simplicial/a/u;->bl:Ljava/util/Set;

    invoke-virtual {p0, v6, v8, v9}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    goto :goto_23

    .line 878
    :cond_3d
    if-nez v7, :cond_3f

    .line 880
    iget-object v4, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v7, v4

    move v0, v1

    :goto_24
    if-ge v0, v7, :cond_3e

    aget-object v8, v4, v0

    .line 881
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->e()V

    .line 880
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 882
    :cond_3e
    iput v1, v6, Lsoftware/simplicial/a/bf;->S:I

    .line 884
    :cond_3f
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    iget-object v4, p0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    invoke-virtual {v6, v0, v4, v1}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Z)V

    goto/16 :goto_1f

    .line 887
    :cond_40
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-eq v0, v5, :cond_41

    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v0, :cond_41

    .line 888
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput-boolean v1, v0, Lsoftware/simplicial/a/bf;->bE:Z

    .line 890
    :cond_41
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-nez v0, :cond_42

    if-eqz v5, :cond_42

    iget v0, p0, Lsoftware/simplicial/a/u;->g:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_42

    .line 892
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/u;->g:F

    .line 893
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iput v1, v0, Lsoftware/simplicial/a/bf;->bB:I

    .line 894
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->h:Z

    .line 896
    :cond_42
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v0, :cond_0

    .line 897
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    const/16 v1, 0xd2

    iput v1, v0, Lsoftware/simplicial/a/bf;->bB:I

    goto/16 :goto_0

    :cond_43
    move v0, v3

    goto/16 :goto_21

    :cond_44
    move v3, v0

    goto/16 :goto_1f
.end method

.method public a(Lsoftware/simplicial/a/f/bb;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 449
    iget-object v0, p1, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v0, v2, :cond_0

    iget-byte v0, p1, Lsoftware/simplicial/a/f/bb;->c:B

    if-ltz v0, :cond_0

    iget-byte v0, p1, Lsoftware/simplicial/a/f/bb;->c:B

    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget-byte v2, p1, Lsoftware/simplicial/a/f/bb;->c:B

    aget-object v0, v0, v2

    .line 453
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->d:Ljava/lang/String;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 454
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->e:[B

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->E:[B

    .line 455
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->p:Ljava/lang/String;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    .line 456
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->q:[B

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->I:[B

    .line 457
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->r:Lsoftware/simplicial/a/q;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    .line 458
    iget-short v2, p1, Lsoftware/simplicial/a/f/bb;->f:S

    if-ltz v2, :cond_4

    iget-short v2, p1, Lsoftware/simplicial/a/f/bb;->f:S

    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v3, v3

    if-ge v2, v3, :cond_4

    .line 459
    sget-object v2, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    iget-short v3, p1, Lsoftware/simplicial/a/f/bb;->f:S

    aget-object v2, v2, v3

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 462
    :goto_1
    iget-byte v2, p1, Lsoftware/simplicial/a/f/bb;->g:B

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v2

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    .line 463
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->x:Lsoftware/simplicial/a/as;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    .line 464
    iget-byte v2, p1, Lsoftware/simplicial/a/f/bb;->i:B

    iget-short v3, p1, Lsoftware/simplicial/a/f/bb;->j:S

    invoke-static {v2, v3}, Lsoftware/simplicial/a/bd;->a(IS)Lsoftware/simplicial/a/bd;

    move-result-object v2

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    .line 465
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->k:Ljava/lang/String;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 466
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->h:I

    iput v2, v0, Lsoftware/simplicial/a/bf;->ad:I

    .line 467
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->n:I

    iput v2, v0, Lsoftware/simplicial/a/bf;->A:I

    .line 468
    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v4, p1, Lsoftware/simplicial/a/f/bb;->o:J

    iput-wide v4, v2, Lsoftware/simplicial/a/az;->b:J

    .line 469
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->s:I

    iput v2, v0, Lsoftware/simplicial/a/bf;->an:I

    .line 470
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->t:I

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/bf;->ap:J

    .line 471
    iget-object v2, p1, Lsoftware/simplicial/a/f/bb;->v:Lsoftware/simplicial/a/s;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 472
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->w:I

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v2

    iput-wide v2, v0, Lsoftware/simplicial/a/bf;->aq:J

    .line 473
    iget-wide v2, p1, Lsoftware/simplicial/a/f/bb;->o:J

    cmp-long v2, v2, v8

    if-ltz v2, :cond_5

    .line 474
    iget-wide v2, p1, Lsoftware/simplicial/a/f/bb;->o:J

    invoke-static {v2, v3}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/a/bf;->bD:I

    .line 477
    :goto_2
    iget-byte v2, p1, Lsoftware/simplicial/a/f/bb;->m:B

    .line 478
    if-ltz v2, :cond_6

    iget v3, p0, Lsoftware/simplicial/a/u;->aj:I

    if-ge v2, v3, :cond_6

    .line 479
    iget-object v3, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v3, v2

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    .line 482
    :goto_3
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->l:I

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/bf;->c(I)V

    .line 484
    iget v2, p1, Lsoftware/simplicial/a/f/bb;->ar:I

    iget-object v3, p0, Lsoftware/simplicial/a/u;->bd:Lsoftware/simplicial/a/f/bg;

    invoke-interface {v3}, Lsoftware/simplicial/a/f/bg;->d()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 486
    iget-boolean v2, p1, Lsoftware/simplicial/a/f/bb;->u:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lsoftware/simplicial/a/u;->j:I

    if-eq v2, v6, :cond_3

    .line 487
    :cond_2
    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput v1, v2, Lsoftware/simplicial/a/ay;->A:I

    .line 489
    :cond_3
    iput v6, p0, Lsoftware/simplicial/a/u;->j:I

    .line 490
    iput v1, p0, Lsoftware/simplicial/a/u;->n:I

    .line 492
    iput-boolean v1, p0, Lsoftware/simplicial/a/u;->h:Z

    .line 493
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lsoftware/simplicial/a/u;->g:F

    .line 494
    iput-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    .line 495
    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    const/4 v3, 0x0

    iput v3, v2, Lsoftware/simplicial/a/bf;->aD:F

    .line 496
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/u;->bf:J

    .line 497
    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    const/16 v3, 0xd2

    iput v3, v2, Lsoftware/simplicial/a/bf;->bB:I

    .line 498
    iput-boolean v7, p0, Lsoftware/simplicial/a/u;->e:Z

    .line 499
    iget-object v2, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v2

    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_7

    aget-object v4, v2, v0

    .line 500
    invoke-virtual {v4, v1}, Lsoftware/simplicial/a/bh;->a(I)V

    .line 499
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 461
    :cond_4
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    goto/16 :goto_1

    .line 476
    :cond_5
    iput v6, v0, Lsoftware/simplicial/a/bf;->bD:I

    goto :goto_2

    .line 481
    :cond_6
    const/4 v2, 0x0

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_3

    .line 503
    :cond_7
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bb;->o:J

    cmp-long v0, v0, v8

    if-ltz v0, :cond_0

    .line 505
    iget-wide v0, p1, Lsoftware/simplicial/a/f/bb;->o:J

    iput-wide v0, p0, Lsoftware/simplicial/a/u;->A:J

    .line 506
    iget v0, p1, Lsoftware/simplicial/a/f/bb;->s:I

    iput v0, p0, Lsoftware/simplicial/a/u;->B:I

    .line 507
    iget v0, p1, Lsoftware/simplicial/a/f/bb;->t:I

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/u;->C:J

    .line 508
    iput-boolean v7, p0, Lsoftware/simplicial/a/u;->D:Z

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/f/bi;)V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aC:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 415
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/co;)V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1105
    .line 1110
    iget v0, p1, Lsoftware/simplicial/a/f/co;->k:I

    iput v0, p0, Lsoftware/simplicial/a/u;->aZ:I

    move v0, v1

    .line 1112
    :goto_0
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    .line 1114
    iget-object v2, p0, Lsoftware/simplicial/a/u;->c:[I

    iget-object v4, p1, Lsoftware/simplicial/a/f/co;->a:[B

    aget-byte v4, v4, v0

    aput v4, v2, v0

    .line 1115
    iget-object v2, p0, Lsoftware/simplicial/a/u;->d:[I

    iget-object v4, p1, Lsoftware/simplicial/a/f/co;->b:[I

    aget v4, v4, v0

    aput v4, v2, v0

    .line 1117
    iget-object v2, p0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    sget-object v4, Lsoftware/simplicial/a/aa;->l:Lsoftware/simplicial/a/aa;

    invoke-virtual {v2, v4}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v4, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget-object v2, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/u;->c:[I

    aget v2, v2, v0

    iget-object v4, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget-object v4, v4, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    if-ne v2, v4, :cond_0

    .line 1119
    iget-object v2, p0, Lsoftware/simplicial/a/u;->d:[I

    aget v2, v2, v0

    iput v2, p0, Lsoftware/simplicial/a/u;->n:I

    .line 1112
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1122
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1123
    iget-object v2, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    iget v2, v2, Lsoftware/simplicial/a/bf;->C:I

    if-eq v0, v2, :cond_2

    .line 1124
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v0

    iput-boolean v1, v2, Lsoftware/simplicial/a/bf;->aF:Z

    .line 1122
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1126
    :goto_2
    iget-byte v2, p1, Lsoftware/simplicial/a/f/co;->e:B

    if-ge v0, v2, :cond_7

    .line 1128
    iget-object v2, p1, Lsoftware/simplicial/a/f/co;->f:[B

    aget-byte v4, v2, v0

    .line 1129
    if-ltz v4, :cond_4

    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v2, v2

    if-lt v4, v2, :cond_5

    .line 1126
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1132
    :cond_5
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v4

    iput-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    .line 1145
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v4

    iget-object v5, p1, Lsoftware/simplicial/a/f/co;->h:[S

    aget-short v5, v5, v0

    iput v5, v2, Lsoftware/simplicial/a/bf;->bD:I

    move v2, v1

    .line 1146
    :goto_4
    iget v5, p0, Lsoftware/simplicial/a/u;->aj:I

    if-ge v2, v5, :cond_4

    .line 1148
    iget-object v5, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v2

    .line 1150
    iget v6, v5, Lsoftware/simplicial/a/bx;->c:I

    iget-object v7, p1, Lsoftware/simplicial/a/f/co;->g:[B

    aget-byte v7, v7, v0

    if-ne v6, v7, :cond_6

    .line 1152
    iget-object v2, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v4

    iput-object v5, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_3

    .line 1146
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1157
    :cond_7
    iget-short v2, p0, Lsoftware/simplicial/a/u;->aH:S

    .line 1158
    iget-short v0, p1, Lsoftware/simplicial/a/f/co;->c:S

    iput-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    .line 1159
    iget-short v4, p0, Lsoftware/simplicial/a/u;->aH:S

    .line 1160
    iget-object v0, p0, Lsoftware/simplicial/a/u;->c:[I

    aget v0, v0, v1

    iput v0, p0, Lsoftware/simplicial/a/u;->i:I

    .line 1161
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v5, :cond_8

    .line 1163
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    const/16 v5, -0x14

    if-gt v0, v5, :cond_a

    .line 1164
    iput v1, p0, Lsoftware/simplicial/a/u;->i:I

    .line 1168
    :cond_8
    :goto_5
    iget-object v0, p1, Lsoftware/simplicial/a/f/co;->i:Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    .line 1169
    iget-object v0, p1, Lsoftware/simplicial/a/f/co;->j:[B

    iput-object v0, p0, Lsoftware/simplicial/a/u;->bb:[B

    .line 1170
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->l:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->p:Z

    .line 1171
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->m:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->q:Z

    .line 1172
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->u:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->t:Z

    .line 1173
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->v:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->u:Z

    .line 1174
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->n:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->r:Z

    .line 1175
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/co;->s:Z

    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->s:Z

    .line 1176
    iget-object v0, p1, Lsoftware/simplicial/a/f/co;->w:Lsoftware/simplicial/a/c/g;

    iput-object v0, p0, Lsoftware/simplicial/a/u;->v:Lsoftware/simplicial/a/c/g;

    .line 1177
    iget v0, p1, Lsoftware/simplicial/a/f/co;->t:I

    iput v0, p0, Lsoftware/simplicial/a/u;->w:I

    .line 1179
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->u:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->aR:Z

    if-nez v0, :cond_9

    .line 1181
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v0, v1

    iget-object v5, p1, Lsoftware/simplicial/a/f/co;->x:[B

    aget-byte v5, v5, v1

    iput-byte v5, v0, Lsoftware/simplicial/a/bf;->R:B

    .line 1182
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v0, v3

    iget-object v5, p1, Lsoftware/simplicial/a/f/co;->x:[B

    aget-byte v5, v5, v3

    iput-byte v5, v0, Lsoftware/simplicial/a/bf;->R:B

    .line 1185
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v0, v5, :cond_b

    move v0, v1

    .line 1186
    :goto_6
    iget v5, p0, Lsoftware/simplicial/a/u;->aj:I

    iget-object v6, p1, Lsoftware/simplicial/a/f/co;->y:[B

    array-length v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-ge v0, v5, :cond_b

    .line 1187
    iget-object v5, p0, Lsoftware/simplicial/a/u;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v0

    iget-object v6, p1, Lsoftware/simplicial/a/f/co;->y:[B

    aget-byte v6, v6, v0

    iput v6, v5, Lsoftware/simplicial/a/bx;->d:I

    .line 1186
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1165
    :cond_a
    iget-short v0, p0, Lsoftware/simplicial/a/u;->aH:S

    const/16 v5, -0xa

    if-gt v0, v5, :cond_8

    .line 1166
    iput v3, p0, Lsoftware/simplicial/a/u;->i:I

    goto :goto_5

    .line 1190
    :cond_b
    if-lt v4, v3, :cond_d

    const/4 v0, 0x5

    if-gt v4, v0, :cond_d

    if-eq v4, v2, :cond_d

    .line 1192
    new-instance v0, Lsoftware/simplicial/a/a/ah;

    if-nez v4, :cond_c

    move v1, v3

    :cond_c
    invoke-direct {v0, v1}, Lsoftware/simplicial/a/a/ah;-><init>(Z)V

    iget v1, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float/2addr v1, v9

    iget v3, p0, Lsoftware/simplicial/a/u;->aE:F

    div-float/2addr v3, v9

    invoke-direct {p0, v0, v1, v3, v10}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/a/y;FFI)V

    .line 1194
    :cond_d
    const/4 v0, -0x5

    if-lt v4, v0, :cond_e

    if-gtz v4, :cond_e

    if-lez v2, :cond_e

    .line 1196
    new-instance v0, Lsoftware/simplicial/a/a/z;

    invoke-direct {v0}, Lsoftware/simplicial/a/a/z;-><init>()V

    invoke-direct {p0, v0, v8, v8, v10}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/a/y;FFI)V

    .line 1198
    :cond_e
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/u;)V
    .locals 3

    .prologue
    .line 518
    iget-object v0, p0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    iget v1, p1, Lsoftware/simplicial/a/f/u;->ar:I

    aget-object v0, v0, v1

    .line 519
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->a:Ljava/lang/String;

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 520
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->b:[B

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->E:[B

    .line 521
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 522
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    .line 523
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->k:Lsoftware/simplicial/a/as;

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    .line 524
    iget-byte v1, p1, Lsoftware/simplicial/a/f/u;->g:B

    iget-short v2, p1, Lsoftware/simplicial/a/f/u;->h:S

    invoke-static {v1, v2}, Lsoftware/simplicial/a/bd;->a(IS)Lsoftware/simplicial/a/bd;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    .line 525
    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->i:Ljava/lang/String;

    iput-object v1, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 526
    iget v1, p1, Lsoftware/simplicial/a/f/u;->j:I

    iput v1, v0, Lsoftware/simplicial/a/bf;->ah:I

    .line 527
    iget v1, v0, Lsoftware/simplicial/a/bf;->ah:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/a/bf;->ai:I

    .line 528
    iget v1, p1, Lsoftware/simplicial/a/f/u;->e:I

    iput v1, v0, Lsoftware/simplicial/a/bf;->ad:I

    .line 529
    iget v1, p1, Lsoftware/simplicial/a/f/u;->f:I

    iput v1, v0, Lsoftware/simplicial/a/bf;->A:I

    .line 531
    return-void
.end method

.method public a(ZZ)Z
    .locals 1

    .prologue
    .line 1203
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/a/ai;->a(ZZ)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 7

    .prologue
    .line 1291
    :cond_0
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/a/u;->aC:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/a/f/bi;

    .line 1292
    if-nez v1, :cond_1

    .line 1311
    :goto_1
    iget-object v1, p0, Lsoftware/simplicial/a/u;->aB:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/f/be;

    .line 1312
    if-nez v2, :cond_2

    .line 1353
    return-void

    .line 1297
    :cond_1
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    iget v3, v1, Lsoftware/simplicial/a/f/bi;->a:I

    aget-object v2, v2, v3

    invoke-static {v2}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bh;)Lsoftware/simplicial/a/f/be;

    move-result-object v2

    .line 1298
    invoke-virtual {v2, v1}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1301
    iget-object v3, p0, Lsoftware/simplicial/a/u;->aB:Ljava/util/Queue;

    invoke-interface {v3, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1303
    :catch_0
    move-exception v2

    .line 1305
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CG: Exception parsing wrapper "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v1, v1, Lsoftware/simplicial/a/f/bi;->a:I

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 1317
    :cond_2
    :try_start_1
    sget-object v1, Lsoftware/simplicial/a/u$1;->b:[I

    iget-object v3, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 1344
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CG: Uhandled message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1348
    :catch_1
    move-exception v1

    .line 1350
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Failed to handle message %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v2, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1320
    :pswitch_0
    :try_start_2
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/al;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/al;)V

    goto/16 :goto_1

    .line 1323
    :pswitch_1
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/co;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/co;)V

    goto/16 :goto_1

    .line 1326
    :pswitch_2
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/bb;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/bb;)V

    goto/16 :goto_1

    .line 1329
    :pswitch_3
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ai;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/ai;)V

    goto/16 :goto_1

    .line 1332
    :pswitch_4
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/u;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/u;)V

    goto/16 :goto_1

    .line 1335
    :pswitch_5
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ac;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/ac;)V

    goto/16 :goto_1

    .line 1338
    :pswitch_6
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ak;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/ak;)V

    goto/16 :goto_1

    .line 1341
    :pswitch_7
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/bc;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/u;->a(Lsoftware/simplicial/a/f/bc;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 1317
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1459
    invoke-super {p0}, Lsoftware/simplicial/a/ai;->c()Z

    .line 1460
    iget-boolean v1, p0, Lsoftware/simplicial/a/u;->aM:Z

    if-eqz v1, :cond_0

    .line 1477
    :goto_0
    return v0

    .line 1464
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/a/u;->r()V

    .line 1465
    invoke-virtual {p0}, Lsoftware/simplicial/a/u;->b()V

    .line 1466
    invoke-virtual {p0}, Lsoftware/simplicial/a/u;->a()V

    .line 1467
    invoke-direct {p0}, Lsoftware/simplicial/a/u;->s()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1470
    :catch_0
    move-exception v0

    .line 1472
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 1473
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 1474
    invoke-virtual {v0, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 1475
    iget-object v1, p0, Lsoftware/simplicial/a/u;->aJ:Lsoftware/simplicial/a/al;

    invoke-interface {v1, p0, v0}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;Ljava/lang/Exception;)V

    .line 1476
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "CG CRASHED"

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1477
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Lsoftware/simplicial/a/bf;
    .locals 1

    .prologue
    .line 1569
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    return-object v0
.end method

.method public e()Lsoftware/simplicial/a/bf;
    .locals 1

    .prologue
    .line 1575
    iget-object v0, p0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 1580
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->bi:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 1585
    iget-boolean v0, p0, Lsoftware/simplicial/a/u;->bj:Z

    return v0
.end method

.method public h()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1592
    iput-boolean v0, p0, Lsoftware/simplicial/a/u;->D:Z

    .line 1593
    iput-wide v2, p0, Lsoftware/simplicial/a/u;->A:J

    .line 1594
    iput v0, p0, Lsoftware/simplicial/a/u;->B:I

    .line 1595
    iput-wide v2, p0, Lsoftware/simplicial/a/u;->C:J

    .line 1597
    return-void
.end method

.method public i()V
    .locals 18

    .prologue
    .line 1603
    new-instance v2, Lsoftware/simplicial/a/bf;

    invoke-direct {v2}, Lsoftware/simplicial/a/bf;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    .line 1604
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/u;->bc:Lsoftware/simplicial/a/bf;

    new-instance v3, Lsoftware/simplicial/a/f/bl;

    invoke-direct {v3}, Lsoftware/simplicial/a/f/bl;-><init>()V

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    sget-object v8, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    sget-object v9, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    const-string v10, ""

    const/4 v11, 0x0

    new-array v11, v11, [B

    const/4 v12, 0x0

    sget-object v13, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    .line 1605
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/u;->p()I

    move-result v17

    .line 1604
    invoke-virtual/range {v2 .. v17}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 1606
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/u;->e:Z

    .line 1608
    return-void
.end method
