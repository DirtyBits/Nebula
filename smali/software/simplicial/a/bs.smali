.class public Lsoftware/simplicial/a/bs;
.super Lsoftware/simplicial/a/ai;
.source "SourceFile"


# instance fields
.field private A:J

.field private B:I

.field private final C:Z

.field private final a:Lsoftware/simplicial/a/f/bf;

.field private final b:[Lsoftware/simplicial/a/bf;

.field private final c:[Lsoftware/simplicial/a/i;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/f/bl;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lsoftware/simplicial/a/bw;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bt;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lsoftware/simplicial/a/c/a;

.field private final i:Lsoftware/simplicial/a/h/c;

.field private final j:Lsoftware/simplicial/a/b/a;

.field private final k:Lsoftware/simplicial/a/i/a;

.field private final l:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lsoftware/simplicial/a/bh;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lsoftware/simplicial/a/f/al;

.field private n:Lsoftware/simplicial/a/f/ak;

.field private o:I

.field private p:I

.field private q:I

.field private r:S

.field private s:I

.field private t:S

.field private u:Lsoftware/simplicial/a/c/j;

.field private v:Z

.field private w:I

.field private x:[I

.field private y:[I

.field private z:[I


# direct methods
.method public constructor <init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bf;ILjava/lang/String;[BLsoftware/simplicial/a/am;IJLsoftware/simplicial/a/ac;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/bj;Lsoftware/simplicial/a/ap;SLsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/h/c;Lsoftware/simplicial/a/i/a;Z[ZZZ)V
    .locals 24

    .prologue
    .line 159
    const/16 v5, 0x14

    if-nez p16, :cond_0

    if-nez p17, :cond_0

    if-nez p18, :cond_0

    if-eqz p19, :cond_1

    :cond_0
    const/16 v14, -0x20

    :goto_0
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v6, p6

    move/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p14

    move-wide/from16 v10, p8

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v15, p13

    move/from16 v16, p20

    move-object/from16 v17, p21

    move/from16 v18, p22

    move/from16 v19, p7

    move/from16 v20, p3

    move/from16 v21, p15

    move-object/from16 v22, p10

    move-object/from16 v23, p19

    invoke-direct/range {v3 .. v23}, Lsoftware/simplicial/a/ai;-><init>(Lsoftware/simplicial/a/al;ILsoftware/simplicial/a/am;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;JLjava/lang/String;[BSLsoftware/simplicial/a/bj;Z[ZZIISLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/i/a;)V

    .line 125
    new-instance v2, Lsoftware/simplicial/a/bs$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lsoftware/simplicial/a/bs$1;-><init>(Lsoftware/simplicial/a/bs;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->l:Ljava/util/Comparator;

    .line 137
    new-instance v2, Lsoftware/simplicial/a/f/al;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/al;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->m:Lsoftware/simplicial/a/f/al;

    .line 138
    new-instance v2, Lsoftware/simplicial/a/f/ak;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/ak;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->n:Lsoftware/simplicial/a/f/ak;

    .line 151
    const-wide/high16 v2, -0x8000000000000000L

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/bs;->A:J

    .line 152
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->B:I

    .line 163
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    array-length v3, v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->b:[Lsoftware/simplicial/a/bf;

    .line 167
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aP:I

    new-array v2, v2, [Lsoftware/simplicial/a/i;

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    .line 168
    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    array-length v2, v2

    if-ge v4, v2, :cond_2

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    new-instance v3, Lsoftware/simplicial/a/i;

    invoke-direct {v3}, Lsoftware/simplicial/a/i;-><init>()V

    aput-object v3, v2, v4

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    aget-object v2, v2, v4

    sget-object v3, Lsoftware/simplicial/a/bs;->V:[Ljava/lang/Integer;

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->o()I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->p()I

    move-result v12

    move-object/from16 v7, p10

    move-object/from16 v8, p6

    move-object/from16 v9, p12

    move/from16 v10, p20

    invoke-virtual/range {v2 .. v12}, Lsoftware/simplicial/a/i;->a(IIFFLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;ZII)V

    .line 168
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 160
    :cond_1
    move-object/from16 v0, p6

    move/from16 v1, p15

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/am;S)S

    move-result v14

    goto/16 :goto_0

    .line 174
    :cond_2
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    .line 175
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->at:Ljava/util/Collection;

    .line 179
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    .line 180
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    .line 181
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    .line 182
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    .line 183
    move/from16 v0, p23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lsoftware/simplicial/a/bs;->C:Z

    .line 184
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->o:I

    .line 185
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/a/bs;->aO:J

    .line 186
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->p:I

    .line 187
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->q:I

    .line 188
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-short v2, v0, Lsoftware/simplicial/a/bs;->r:S

    .line 189
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->s:I

    .line 190
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-short v2, v0, Lsoftware/simplicial/a/bs;->t:S

    .line 191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    .line 192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    .line 193
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/a/bs;->w:I

    .line 195
    const/16 v2, 0x14

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->x:[I

    .line 196
    const/16 v2, 0x14

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->y:[I

    .line 197
    const/16 v2, 0x14

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/a/bs;->z:[I

    .line 199
    const/4 v2, 0x1

    const/4 v3, 0x0

    const v4, 0x417a6666    # 15.65f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lsoftware/simplicial/a/bs;->a(ZZF)V

    .line 200
    return-void
.end method

.method private A()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 4046
    move v0, v1

    move v2, v1

    .line 4047
    :goto_0
    iget v3, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v0, v3, :cond_6

    .line 4049
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v8, v3, v0

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    .line 4055
    :goto_1
    iget v9, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v9, :cond_3

    .line 4057
    iget-object v9, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v9, v9, v3

    .line 4059
    iget-boolean v10, v9, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v10, :cond_0

    iget-object v10, v9, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eq v10, v8, :cond_1

    .line 4055
    :cond_0
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 4062
    :cond_1
    add-int/lit8 v5, v5, 0x1

    .line 4063
    iget-boolean v10, v9, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v10, :cond_2

    .line 4064
    add-int/lit8 v6, v6, 0x1

    .line 4065
    :cond_2
    iget-boolean v9, v9, Lsoftware/simplicial/a/bf;->aA:Z

    if-eqz v9, :cond_0

    .line 4066
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 4069
    :cond_3
    if-ge v6, v7, :cond_4

    if-eqz v5, :cond_4

    if-nez v4, :cond_5

    .line 4070
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 4047
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4072
    :cond_6
    if-gt v2, v7, :cond_7

    move v1, v7

    :cond_7
    return v1
.end method

.method private B()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4077
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v0, v2, :cond_0

    .line 4079
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v2, v0

    .line 4081
    iget v2, v2, Lsoftware/simplicial/a/bx;->e:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    .line 4083
    const/4 v1, 0x1

    .line 4086
    :cond_0
    return v1

    .line 4077
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private C()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4091
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v0, v2, :cond_0

    .line 4093
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v2, v2, v0

    .line 4095
    iget v2, v2, Lsoftware/simplicial/a/z;->n:F

    iget v3, p0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 4096
    const/4 v1, 0x1

    .line 4098
    :cond_0
    return v1

    .line 4091
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private D()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4103
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v0, v2, :cond_0

    .line 4105
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v2, v0

    .line 4107
    iget v2, v2, Lsoftware/simplicial/a/bx;->e:I

    int-to-float v2, v2

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    invoke-static {v3}, Lsoftware/simplicial/a/bz;->b(Lsoftware/simplicial/a/ap;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 4109
    const/4 v1, 0x1

    .line 4112
    :cond_0
    return v1

    .line 4103
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private E()V
    .locals 24

    .prologue
    .line 4118
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-eqz v2, :cond_0

    .line 4120
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    if-ne v2, v3, :cond_4

    .line 4121
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4129
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_22

    .line 4131
    :cond_1
    const/4 v3, 0x0

    .line 4132
    const/16 v22, 0x0

    .line 4133
    const/4 v7, -0x1

    .line 4134
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-byte v2, v2, Lsoftware/simplicial/a/bf;->R:B

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    if-eq v2, v4, :cond_2

    .line 4136
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    iput v4, v2, Lsoftware/simplicial/a/bf;->Q:I

    .line 4137
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    iput v4, v2, Lsoftware/simplicial/a/bf;->Q:I

    .line 4139
    :cond_2
    const/4 v2, 0x0

    move v4, v2

    move-object v2, v3

    :goto_1
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v4, v3, :cond_8

    .line 4141
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v3, v4

    .line 4143
    iget-boolean v5, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v5, :cond_6

    move-object v3, v2

    move-object/from16 v2, v22

    .line 4139
    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v22, v2

    move-object v2, v3

    goto :goto_1

    .line 4122
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    if-ne v2, v3, :cond_5

    .line 4123
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_0

    .line 4125
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_0

    .line 4146
    :cond_6
    if-eqz v2, :cond_3

    invoke-virtual {v3}, Lsoftware/simplicial/a/bf;->d()I

    move-result v5

    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->d()I

    move-result v6

    if-gt v5, v6, :cond_3

    .line 4151
    if-eqz v22, :cond_7

    invoke-virtual {v3}, Lsoftware/simplicial/a/bf;->d()I

    move-result v5

    invoke-virtual/range {v22 .. v22}, Lsoftware/simplicial/a/bf;->d()I

    move-result v6

    if-le v5, v6, :cond_44

    :cond_7
    move-object/from16 v23, v3

    move-object v3, v2

    move-object/from16 v2, v23

    .line 4152
    goto :goto_2

    .line 4154
    :cond_8
    if-eqz v22, :cond_9

    .line 4155
    move-object/from16 v0, v22

    iget v7, v0, Lsoftware/simplicial/a/bf;->Q:I

    .line 4157
    :cond_9
    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v3, :cond_b

    .line 4159
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v3, :cond_a

    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_a

    .line 4160
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    .line 4162
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x14

    iget v6, v2, Lsoftware/simplicial/a/bf;->Q:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aG:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v0, p0

    iget v14, v0, Lsoftware/simplicial/a/bs;->B:I

    const/4 v15, 0x0

    invoke-virtual/range {v2 .. v15}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    .line 4166
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v3, v4, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_e

    .line 4168
    :cond_c
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v4, :cond_e

    .line 4170
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v4, v3

    .line 4172
    iget-boolean v5, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v5, :cond_d

    if-eq v4, v2, :cond_d

    .line 4173
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v4, v5}, Lsoftware/simplicial/a/bf;->b(Lsoftware/simplicial/a/am;)V

    .line 4168
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 4177
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-nez v3, :cond_f

    .line 4179
    if-nez v2, :cond_14

    .line 4181
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v9, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v10, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v11, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v12, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4228
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v3, :cond_10

    .line 4230
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 4231
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 4232
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lsoftware/simplicial/a/bs;->a(Ljava/util/List;Z)V

    .line 4235
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v3, :cond_11

    .line 4236
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v2, v1, v3}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bf;Z)V

    .line 4238
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v3, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v3, :cond_1c

    if-eqz v2, :cond_1c

    .line 4241
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v4, v2, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 4243
    sget-object v4, Lsoftware/simplicial/a/h/d;->a:Lsoftware/simplicial/a/h/d;

    .line 4244
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_12
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsoftware/simplicial/a/f/bl;

    .line 4246
    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v8

    .line 4247
    if-eqz v8, :cond_12

    .line 4249
    iget-boolean v3, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_13

    .line 4250
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v6, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    .line 4251
    :cond_13
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v3, v6

    div-int/lit8 v11, v3, 0x14

    iget v12, v2, Lsoftware/simplicial/a/bf;->Q:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aG:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->B:I

    move/from16 v20, v0

    const/16 v21, 0x0

    move v13, v7

    invoke-virtual/range {v8 .. v21}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_5

    .line 4183
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    iget-object v4, v2, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 4185
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v9, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v10, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v11, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v12, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4186
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_15
    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsoftware/simplicial/a/f/bl;

    .line 4188
    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v8

    .line 4189
    if-eqz v8, :cond_15

    .line 4191
    iget-boolean v3, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_16

    .line 4192
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    .line 4193
    :cond_16
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v3, v5

    div-int/lit8 v11, v3, 0x14

    iget v12, v2, Lsoftware/simplicial/a/bf;->Q:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aG:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->B:I

    move/from16 v20, v0

    const/16 v21, 0x0

    move v13, v7

    invoke-virtual/range {v8 .. v21}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_6

    .line 4198
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    iget-object v4, v2, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 4200
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v9, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v10, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v11, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v12, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4201
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_18
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsoftware/simplicial/a/f/bl;

    .line 4203
    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v8

    .line 4204
    if-eqz v8, :cond_18

    .line 4206
    iget-boolean v3, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_19

    .line 4207
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v5, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    .line 4208
    :cond_19
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v3, v5

    div-int/lit8 v11, v3, 0x14

    iget v12, v2, Lsoftware/simplicial/a/bf;->Q:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aG:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->B:I

    move/from16 v20, v0

    const/16 v21, 0x0

    move v13, v7

    invoke-virtual/range {v8 .. v21}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_7

    .line 4217
    :cond_1a
    :try_start_0
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Failed to find winner client ID in clan war\'s legions! player acct id = %d, clan = %s. legA = %s, legB = %s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v8, v2, Lsoftware/simplicial/a/bf;->A:I

    .line 4218
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    iget-object v8, v2, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    aput-object v8, v5, v6

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v8, v8, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v8, v8, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v8, v5, v6

    const/4 v6, 0x3

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v8, v8, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v8, v8, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v8, v5, v6

    .line 4217
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4224
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v9, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v10, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v11, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v12, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v14}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_4

    .line 4220
    :catch_0
    move-exception v3

    .line 4222
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "FAILED EXCEPTION"

    invoke-static {v3, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_8

    :cond_1b
    move-object v2, v4

    .line 4273
    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/h/d;)V

    .line 4397
    :cond_1c
    :goto_a
    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-ge v2, v3, :cond_3d

    .line 4399
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v3, v3, v2

    .line 4401
    sget-object v4, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v4, v3, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 4397
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 4256
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v4, v2, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 4258
    sget-object v4, Lsoftware/simplicial/a/h/d;->b:Lsoftware/simplicial/a/h/d;

    .line 4259
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1e
    :goto_c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_20

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsoftware/simplicial/a/f/bl;

    .line 4261
    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v8

    .line 4262
    if-eqz v8, :cond_1e

    .line 4264
    iget-boolean v3, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_1f

    .line 4265
    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3, v6, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    .line 4266
    :cond_1f
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v3, v6

    div-int/lit8 v11, v3, 0x14

    iget v12, v2, Lsoftware/simplicial/a/bf;->Q:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aG:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->B:I

    move/from16 v20, v0

    const/16 v21, 0x0

    move v13, v7

    invoke-virtual/range {v8 .. v21}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_c

    :cond_20
    move-object v2, v4

    .line 4269
    goto/16 :goto_9

    .line 4272
    :cond_21
    sget-object v2, Lsoftware/simplicial/a/h/d;->c:Lsoftware/simplicial/a/h/d;

    goto/16 :goto_9

    .line 4276
    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_1c

    .line 4279
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_24

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-eqz v2, :cond_24

    .line 4281
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    if-ne v2, v3, :cond_26

    .line 4282
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, -0x1

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 4292
    :cond_24
    :goto_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_25

    .line 4294
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Lsoftware/simplicial/a/bx;->f:I

    if-nez v2, :cond_28

    .line 4295
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 4300
    :cond_25
    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aj:I

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lsoftware/simplicial/a/bx;

    .line 4301
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 4302
    const/4 v15, 0x0

    .line 4304
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_2a

    .line 4306
    const/4 v3, 0x0

    aget-object v3, v2, v3

    iget v3, v3, Lsoftware/simplicial/a/bx;->d:I

    const/4 v4, 0x1

    aget-object v4, v2, v4

    iget v4, v4, Lsoftware/simplicial/a/bx;->d:I

    if-eq v3, v4, :cond_29

    .line 4308
    const/4 v3, 0x0

    :goto_f
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v3, v4, :cond_29

    .line 4310
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v4, v4, v3

    .line 4312
    iget v5, v4, Lsoftware/simplicial/a/bx;->d:I

    iput v5, v4, Lsoftware/simplicial/a/bx;->e:I

    .line 4308
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    .line 4283
    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    if-ne v2, v3, :cond_27

    .line 4284
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, -0x1

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    goto :goto_d

    .line 4287
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, -0x1

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 4288
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, -0x1

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    goto/16 :goto_d

    .line 4297
    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iput v3, v2, Lsoftware/simplicial/a/bx;->e:I

    goto :goto_e

    .line 4315
    :cond_29
    const/4 v3, 0x0

    aget-object v3, v2, v3

    iget v3, v3, Lsoftware/simplicial/a/bx;->d:I

    const/4 v4, 0x1

    aget-object v4, v2, v4

    iget v4, v4, Lsoftware/simplicial/a/bx;->d:I

    sub-int v15, v3, v4

    .line 4318
    :cond_2a
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 4319
    const/4 v3, 0x0

    :goto_10
    array-length v4, v2

    if-ge v3, v4, :cond_2c

    .line 4320
    aget-object v4, v2, v3

    iget v4, v4, Lsoftware/simplicial/a/bx;->e:I

    const/4 v5, 0x0

    aget-object v5, v2, v5

    iget v5, v5, Lsoftware/simplicial/a/bx;->e:I

    if-ne v4, v5, :cond_2b

    .line 4321
    aget-object v4, v2, v3

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4319
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    .line 4323
    :cond_2c
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_30

    .line 4325
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_11
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v0, v16

    if-ge v0, v2, :cond_2f

    .line 4327
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v16

    .line 4329
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v3, :cond_2d

    iget-object v3, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2e

    .line 4325
    :cond_2d
    :goto_12
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_11

    .line 4332
    :cond_2e
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x14

    const/4 v6, 0x0

    const/4 v7, -0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aG:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v0, p0

    iget v14, v0, Lsoftware/simplicial/a/bs;->B:I

    invoke-virtual/range {v2 .. v15}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_12

    .line 4336
    :cond_2f
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-nez v2, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_1c

    .line 4337
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_a

    .line 4341
    :cond_30
    const/16 v16, 0x0

    .line 4342
    const/4 v3, 0x0

    .line 4343
    const/4 v7, -0x1

    .line 4344
    const/4 v2, 0x0

    move v4, v2

    move-object v2, v3

    :goto_13
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v4, v3, :cond_35

    .line 4346
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v3, v3, v4

    .line 4348
    if-eqz v16, :cond_31

    iget v5, v3, Lsoftware/simplicial/a/bx;->e:I

    move-object/from16 v0, v16

    iget v6, v0, Lsoftware/simplicial/a/bx;->e:I

    if-le v5, v6, :cond_33

    :cond_31
    move-object/from16 v2, v16

    move-object/from16 v16, v3

    .line 4344
    :cond_32
    :goto_14
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_13

    .line 4353
    :cond_33
    if-eqz v2, :cond_34

    iget v5, v3, Lsoftware/simplicial/a/bx;->e:I

    iget v6, v2, Lsoftware/simplicial/a/bx;->e:I

    if-le v5, v6, :cond_32

    :cond_34
    move-object v2, v3

    .line 4354
    goto :goto_14

    .line 4357
    :cond_35
    if-eqz v16, :cond_1c

    .line 4359
    if-eqz v2, :cond_36

    .line 4360
    iget v7, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 4362
    :cond_36
    const/4 v2, 0x0

    move/from16 v17, v2

    :goto_15
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v0, v17

    if-ge v0, v2, :cond_3a

    .line 4364
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v17

    .line 4366
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_38

    .line 4362
    :cond_37
    :goto_16
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    goto :goto_15

    .line 4369
    :cond_38
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v16

    if-ne v3, v0, :cond_39

    .line 4370
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x14

    move-object/from16 v0, v16

    iget v6, v0, Lsoftware/simplicial/a/bx;->e:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aG:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v0, p0

    iget v14, v0, Lsoftware/simplicial/a/bs;->B:I

    invoke-virtual/range {v2 .. v15}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;ZIIILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/i/a;ILsoftware/simplicial/a/h/c;II)V

    goto :goto_16

    .line 4372
    :cond_39
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_37

    .line 4373
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/bf;->b(Lsoftware/simplicial/a/am;)V

    goto :goto_16

    .line 4376
    :cond_3a
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-nez v2, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_1c

    .line 4378
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object/from16 v0, v16

    if-ne v0, v2, :cond_3b

    .line 4380
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_a

    .line 4382
    :cond_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    move-object/from16 v0, v16

    if-ne v0, v2, :cond_3c

    .line 4384
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->b:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    goto/16 :goto_a

    .line 4388
    :cond_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4389
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to determine clan war winner for team game!"

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 4405
    :cond_3d
    const/4 v2, 0x0

    :goto_17
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v3, :cond_3f

    .line 4407
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v3, v2

    .line 4410
    instance-of v4, v3, Lsoftware/simplicial/a/i;

    if-nez v4, :cond_3e

    iget-boolean v4, v3, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v4, :cond_3e

    .line 4411
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lsoftware/simplicial/a/bs;->aO:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    invoke-virtual/range {v3 .. v8}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;JLsoftware/simplicial/a/i/a;)V

    .line 4405
    :cond_3e
    add-int/lit8 v2, v2, 0x1

    goto :goto_17

    .line 4414
    :cond_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_40

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_40

    .line 4415
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bf;Z)V

    .line 4416
    :cond_40
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_41

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_41

    .line 4417
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v2, v2, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v5, v2, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v6, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 4418
    :cond_41
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v2, :cond_42

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_42

    .line 4419
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lsoftware/simplicial/a/bs;->a(Ljava/util/List;Z)V

    .line 4420
    :cond_42
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v2, :cond_43

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v2, :cond_43

    .line 4421
    sget-object v2, Lsoftware/simplicial/a/h/d;->c:Lsoftware/simplicial/a/h/d;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/h/d;)V

    .line 4422
    :cond_43
    return-void

    :cond_44
    move-object v3, v2

    move-object/from16 v2, v22

    goto/16 :goto_2
.end method

.method private F()Lsoftware/simplicial/a/bq;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5180
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v0, v0, v1

    move-object v2, v0

    move v0, v1

    .line 5181
    :goto_0
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 5183
    iget v3, p0, Lsoftware/simplicial/a/bs;->w:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lsoftware/simplicial/a/bs;->w:I

    .line 5184
    iget v3, p0, Lsoftware/simplicial/a/bs;->w:I

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v4, v4

    if-lt v3, v4, :cond_0

    .line 5185
    iput v1, p0, Lsoftware/simplicial/a/bs;->w:I

    .line 5186
    :cond_0
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    iget v4, p0, Lsoftware/simplicial/a/bs;->w:I

    aget-object v3, v3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-ne v3, v4, :cond_2

    .line 5187
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    iget v1, p0, Lsoftware/simplicial/a/bs;->w:I

    aget-object v2, v0, v1

    .line 5192
    :cond_1
    return-object v2

    .line 5188
    :cond_2
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    iget v4, p0, Lsoftware/simplicial/a/bs;->w:I

    aget-object v3, v3, v4

    iget v3, v3, Lsoftware/simplicial/a/bq;->k:F

    iget v4, v2, Lsoftware/simplicial/a/bq;->k:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    .line 5189
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    iget v3, p0, Lsoftware/simplicial/a/bs;->w:I

    aget-object v2, v2, v3

    .line 5181
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private G()Lsoftware/simplicial/a/bx;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 5976
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_2

    .line 5978
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v1, v1, v3

    iget v1, v1, Lsoftware/simplicial/a/bx;->f:I

    if-nez v1, :cond_1

    .line 5979
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v0, v0, v3

    .line 5993
    :cond_0
    :goto_0
    return-object v0

    .line 5981
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v0, v1, v0

    goto :goto_0

    .line 5984
    :cond_2
    const/4 v1, 0x0

    move v2, v0

    move-object v0, v1

    .line 5985
    :goto_1
    iget v1, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v2, v1, :cond_0

    .line 5987
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v1, v1, v2

    .line 5988
    if-eqz v0, :cond_3

    iget v3, v1, Lsoftware/simplicial/a/bx;->f:I

    iget v4, v0, Lsoftware/simplicial/a/bx;->f:I

    if-ge v3, v4, :cond_4

    :cond_3
    move-object v0, v1

    .line 5985
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 5397
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_2

    .line 5399
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 5401
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v2, :cond_1

    .line 5397
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5404
    :cond_1
    iget-object v2, v1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 5413
    :goto_1
    return-object v0

    .line 5408
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bw;

    .line 5410
    iget-object v2, v0, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 5413
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lsoftware/simplicial/a/bx;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/bx;",
            ")",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6823
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 6825
    if-nez p1, :cond_0

    move-object v0, v1

    .line 6842
    :goto_0
    return-object v0

    .line 6828
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_3

    .line 6830
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v0

    .line 6832
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_2

    .line 6828
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6835
    :cond_2
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v3, :cond_1

    .line 6838
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v3, p1, :cond_1

    .line 6839
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 6842
    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/i;F)Lsoftware/simplicial/a/ag;
    .locals 8

    .prologue
    .line 4603
    const/4 v2, 0x0

    .line 4604
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 4605
    invoke-virtual {p1}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v5

    .line 4607
    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v4, v1, :cond_2

    .line 4609
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v3, v1, v4

    .line 4612
    iget-object v1, v3, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-nez v1, :cond_0

    iget-object v1, v3, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget-object v6, p1, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    if-ne v1, v6, :cond_1

    iget-object v1, v3, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget-object v6, p1, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    if-ne v1, v6, :cond_0

    iget v1, v3, Lsoftware/simplicial/a/ag;->l:F

    iget-object v6, v3, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget v6, v6, Lsoftware/simplicial/a/bx;->l:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_1

    iget v1, v3, Lsoftware/simplicial/a/ag;->m:F

    iget-object v6, v3, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget v6, v6, Lsoftware/simplicial/a/bx;->m:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v3, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-eqz v1, :cond_3

    .line 4613
    invoke-virtual {p1}, Lsoftware/simplicial/a/i;->p()F

    move-result v1

    iget-object v6, v3, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bf;->p()F

    move-result v6

    const v7, 0x3f866666    # 1.05f

    mul-float/2addr v6, v7

    const/high16 v7, 0x3f400000    # 0.75f

    add-float/2addr v6, v7

    cmpl-float v1, v1, v6

    if-lez v1, :cond_3

    iget-object v1, v3, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget-object v6, p1, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    if-eq v1, v6, :cond_3

    .line 4616
    :cond_1
    neg-float v1, p2

    invoke-virtual {v3, v5, v1}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4618
    invoke-virtual {v3}, Lsoftware/simplicial/a/ag;->h()F

    move-result v1

    iget v6, v5, Lsoftware/simplicial/a/h;->n:F

    sub-float/2addr v1, v6

    iget v6, v3, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v1, v6

    .line 4619
    cmpg-float v6, v1, v0

    if-gez v6, :cond_3

    move v0, v1

    move-object v1, v3

    .line 4607
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_0

    .line 4627
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method private a(Lsoftware/simplicial/a/bh;F)Lsoftware/simplicial/a/ao;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4728
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_4

    .line 4730
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v2, v0

    .line 4732
    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->k()I

    move-result v2

    iget v3, p1, Lsoftware/simplicial/a/bh;->j:I

    if-eq v2, v3, :cond_0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v2, :cond_1

    .line 4728
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4735
    :goto_1
    iget v3, v4, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v3, :cond_0

    .line 4737
    iget-object v3, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v2

    .line 4738
    if-eq p1, v3, :cond_2

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 4735
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4741
    :cond_3
    invoke-virtual {p1, v3, p2}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v3

    .line 4748
    :goto_2
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/g;F)Lsoftware/simplicial/a/ao;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 4792
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_4

    .line 4794
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v2, v0

    .line 4796
    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v2, :cond_1

    .line 4792
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4799
    :goto_1
    iget v3, v4, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v3, :cond_0

    .line 4801
    iget-object v3, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v2

    .line 4803
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v6, v7

    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    .line 4799
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4807
    :cond_3
    invoke-virtual {p1, v3, p2}, Lsoftware/simplicial/a/g;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v3

    .line 4814
    :goto_2
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/v;)Lsoftware/simplicial/a/bf;
    .locals 18

    .prologue
    .line 5754
    const/4 v2, -0x1

    .line 5756
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 5758
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v4, v4, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v4, v4, Lsoftware/simplicial/a/i/d;->a:I

    if-ne v3, v4, :cond_2

    .line 5759
    const/4 v2, 0x0

    .line 5763
    :cond_0
    :goto_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 5765
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to add player %d to game %d. No empty slots!"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v6, v6, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5766
    const/4 v2, 0x0

    .line 5885
    :cond_1
    :goto_1
    return-object v2

    .line 5760
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v4, v4, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v4, v4, Lsoftware/simplicial/a/i/d;->a:I

    if-ne v3, v4, :cond_0

    .line 5761
    const/4 v2, 0x1

    goto :goto_0

    .line 5769
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v3, v2

    iget-boolean v3, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v3, :cond_4

    .line 5770
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v3, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->e(Lsoftware/simplicial/a/bf;)V

    .line 5773
    :cond_4
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aG:I

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ne v3, v4, :cond_6

    .line 5775
    const-string v2, "Failed to add client to game. Game was full!\n"

    .line 5776
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/v;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5777
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5778
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-static {v3, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5780
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v3, :cond_5

    .line 5781
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    sget-object v4, Lsoftware/simplicial/a/f/ba;->d:Lsoftware/simplicial/a/f/ba;

    const-wide/16 v6, 0x0

    invoke-static {v3, v4, v6, v7}, Lsoftware/simplicial/a/f/af;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/ba;J)[B

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v2, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5783
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 5786
    :cond_6
    const/4 v3, -0x1

    if-ne v2, v3, :cond_15

    .line 5788
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v4, :cond_15

    .line 5790
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v4, v3

    iget-boolean v4, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v4, :cond_7

    move v5, v3

    .line 5797
    :goto_3
    const/4 v2, -0x1

    if-ne v5, v2, :cond_8

    .line 5799
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to add player %d to game %d. No empty slots!"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v6, v6, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5800
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 5788
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 5803
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v5

    .line 5805
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 5806
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-byte v3, v3, Lsoftware/simplicial/a/bf;->R:B

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v6, 0x1

    aget-object v4, v4, v6

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-byte v3, v3

    iput-byte v3, v2, Lsoftware/simplicial/a/bf;->R:B

    .line 5808
    :cond_9
    const/16 v16, -0x1

    .line 5809
    move-object/from16 v0, p1

    iget-object v10, v0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    .line 5810
    move-object/from16 v0, p1

    iget-object v11, v0, Lsoftware/simplicial/a/v;->g:[B

    .line 5811
    move-object/from16 v0, p1

    iget-object v13, v0, Lsoftware/simplicial/a/v;->h:Lsoftware/simplicial/a/q;

    .line 5812
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v3, :cond_f

    .line 5814
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 5816
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    invoke-virtual {v3}, Lsoftware/simplicial/a/h/a;->a()I

    move-result v16

    .line 5817
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5818
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v10, v3, Lsoftware/simplicial/a/h/a;->b:Ljava/lang/String;

    .line 5819
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v3

    new-array v11, v3, [B

    .line 5820
    sget-object v13, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    .line 5821
    const/16 v3, -0x4d

    invoke-static {v11, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 5862
    :cond_a
    :goto_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/v;->b:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object/from16 v0, p1

    iget-wide v14, v0, Lsoftware/simplicial/a/v;->d:J

    .line 5863
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->p()I

    move-result v17

    .line 5862
    invoke-virtual/range {v2 .. v17}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/f/bl;IIFFLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZLsoftware/simplicial/a/q;JII)V

    .line 5864
    const/4 v3, 0x1

    iput-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    .line 5865
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->l:Lsoftware/simplicial/a/e;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    .line 5866
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->m:Lsoftware/simplicial/a/af;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    .line 5867
    move-object/from16 v0, p1

    iget-byte v3, v0, Lsoftware/simplicial/a/v;->p:B

    iput-byte v3, v2, Lsoftware/simplicial/a/bf;->Z:B

    .line 5868
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->o:I

    iput v3, v2, Lsoftware/simplicial/a/bf;->ae:I

    .line 5869
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->k:[B

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->F:[B

    .line 5870
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->q:I

    iput v3, v2, Lsoftware/simplicial/a/bf;->af:I

    .line 5871
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->n:Lsoftware/simplicial/a/as;

    iput-object v3, v2, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    .line 5872
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->j:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->k:[B

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/v;->r:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 5875
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget v4, v2, Lsoftware/simplicial/a/bf;->C:I

    iget-object v5, v2, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    iget-object v6, v2, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    iget v7, v2, Lsoftware/simplicial/a/bf;->ad:I

    iget-object v8, v2, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v9, v2, Lsoftware/simplicial/a/bf;->E:[B

    iget v10, v2, Lsoftware/simplicial/a/bf;->A:I

    iget-object v11, v2, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iget-byte v11, v11, Lsoftware/simplicial/a/bd;->c:B

    iget-object v12, v2, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iget-short v12, v12, Lsoftware/simplicial/a/bd;->e:S

    iget v13, v2, Lsoftware/simplicial/a/bf;->ah:I

    iget-object v14, v2, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    iget-object v15, v2, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    invoke-static/range {v3 .. v15}, Lsoftware/simplicial/a/f/u;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILjava/lang/String;[BIBSILjava/lang/String;Lsoftware/simplicial/a/as;)[B

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v16

    invoke-interface {v0, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 5878
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v3, v4, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v3, v4, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v3, v4, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_d

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v3, :cond_d

    move-object/from16 v0, p0

    iget-short v3, v0, Lsoftware/simplicial/a/bs;->aH:S

    move-object/from16 v0, p0

    iget-short v4, v0, Lsoftware/simplicial/a/bs;->aX:S

    add-int/lit8 v4, v4, -0x3c

    if-ge v3, v4, :cond_d

    .line 5880
    :cond_c
    const/4 v3, 0x1

    iput-boolean v3, v2, Lsoftware/simplicial/a/bf;->aA:Z

    .line 5881
    :cond_d
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aG:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/a/bs;->aG:I

    .line 5882
    instance-of v3, v2, Lsoftware/simplicial/a/i;

    if-eqz v3, :cond_1

    .line 5883
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->q:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/a/bs;->q:I

    goto/16 :goto_1

    .line 5823
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 5825
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    invoke-virtual {v3}, Lsoftware/simplicial/a/h/a;->a()I

    move-result v16

    .line 5826
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5827
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v10, v3, Lsoftware/simplicial/a/h/a;->b:Ljava/lang/String;

    .line 5828
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v3

    new-array v11, v3, [B

    .line 5829
    sget-object v13, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    .line 5830
    const/16 v3, -0x4d

    invoke-static {v11, v3}, Ljava/util/Arrays;->fill([BB)V

    goto/16 :goto_4

    .line 5833
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v3, :cond_11

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    if-eqz v3, :cond_11

    .line 5835
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 5837
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5838
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    invoke-virtual {v3}, Lsoftware/simplicial/a/c/j;->a()I

    move-result v16

    goto/16 :goto_4

    .line 5840
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 5842
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5843
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    invoke-virtual {v3}, Lsoftware/simplicial/a/c/j;->a()I

    move-result v16

    goto/16 :goto_4

    .line 5846
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v3, v3, Lsoftware/simplicial/a/i/a;->c:Z

    if-eqz v3, :cond_14

    .line 5849
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v4, v4, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v4, v4, Lsoftware/simplicial/a/i/d;->a:I

    if-eq v3, v4, :cond_12

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v4, v4, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v4, v4, Lsoftware/simplicial/a/i/d;->a:I

    if-ne v3, v4, :cond_13

    .line 5850
    :cond_12
    const/4 v3, 0x0

    .line 5854
    :goto_5
    new-instance v4, Lsoftware/simplicial/a/bx;

    const-string v6, ""

    const/4 v7, 0x0

    sget-object v8, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    invoke-direct {v4, v6, v3, v7, v8}, Lsoftware/simplicial/a/bx;-><init>(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    .line 5855
    iget v0, v4, Lsoftware/simplicial/a/bx;->g:I

    move/from16 v16, v0

    goto/16 :goto_4

    .line 5852
    :cond_13
    const/4 v3, 0x1

    goto :goto_5

    .line 5859
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->o()I

    move-result v16

    goto/16 :goto_4

    :cond_15
    move v5, v2

    goto/16 :goto_3
.end method

.method private a(Lsoftware/simplicial/a/bf;F[Ljava/lang/Object;)Lsoftware/simplicial/a/bh;
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 4657
    const/4 v2, 0x0

    .line 4658
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 4660
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v7

    move v6, v5

    .line 4662
    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v6, v1, :cond_4

    .line 4664
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v8, v1, v6

    .line 4666
    iget-boolean v1, v8, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v1, :cond_1

    if-eq v8, p1, :cond_1

    iget-boolean v1, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v1, v3, :cond_2

    :cond_0
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget-object v3, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v1, v3, :cond_2

    .line 4662
    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    :cond_2
    move v4, v5

    .line 4672
    :goto_1
    iget v1, v8, Lsoftware/simplicial/a/bf;->S:I

    if-ge v4, v1, :cond_1

    .line 4674
    iget-object v1, v8, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v1, v4

    .line 4676
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    .line 4672
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_1

    .line 4679
    :cond_3
    neg-float v1, p2

    invoke-virtual {v3, v7, v1}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4681
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->h()F

    move-result v1

    iget v9, v7, Lsoftware/simplicial/a/h;->n:F

    sub-float/2addr v1, v9

    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    sub-float/2addr v1, v9

    .line 4682
    cmpg-float v9, v1, v0

    if-gez v9, :cond_5

    .line 4686
    aput-object v8, p3, v5

    move v0, v1

    move-object v1, v3

    goto :goto_2

    .line 4691
    :cond_4
    return-object v2

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/bf;FZ)Lsoftware/simplicial/a/g;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 4696
    const/4 v2, 0x0

    .line 4698
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    move v6, v5

    .line 4700
    :goto_0
    iget v1, p1, Lsoftware/simplicial/a/bf;->S:I

    if-ge v6, v1, :cond_2

    .line 4702
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v7, v1, v6

    move v4, v5

    .line 4704
    :goto_1
    iget v1, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v4, v1, :cond_1

    .line 4706
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v3, v1, v4

    .line 4708
    if-eqz p3, :cond_0

    iget-object v1, v3, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v8, Lsoftware/simplicial/a/g$a;->a:Lsoftware/simplicial/a/g$a;

    if-eq v1, v8, :cond_3

    iget-object v1, v3, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v8, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v1, v8, :cond_0

    iget v1, p1, Lsoftware/simplicial/a/bf;->S:I

    const/4 v8, 0x1

    if-ne v1, v8, :cond_0

    move-object v1, v2

    .line 4704
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_1

    .line 4711
    :cond_0
    neg-float v1, p2

    invoke-virtual {v3, v7, v1}, Lsoftware/simplicial/a/g;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4713
    invoke-virtual {v3}, Lsoftware/simplicial/a/g;->h()F

    move-result v1

    iget v8, v7, Lsoftware/simplicial/a/h;->n:F

    sub-float/2addr v1, v8

    iget v8, v3, Lsoftware/simplicial/a/g;->n:F

    sub-float/2addr v1, v8

    .line 4714
    cmpg-float v8, v1, v0

    if-gez v8, :cond_3

    move v0, v1

    move-object v1, v3

    .line 4717
    goto :goto_2

    .line 4700
    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 4723
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4753
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v0, v2, :cond_0

    move-object v0, v1

    .line 4764
    :goto_0
    return-object v0

    .line 4756
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v0, v2, :cond_2

    .line 4758
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v2, v2, v0

    .line 4760
    invoke-virtual {v2, p1}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 4761
    goto :goto_0

    .line 4756
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 4764
    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/i;FZ)Lsoftware/simplicial/a/z;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 4632
    const/4 v2, 0x0

    .line 4633
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 4634
    invoke-virtual {p1}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v6

    move v4, v5

    .line 4636
    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v4, v1, :cond_2

    .line 4638
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v3, v1, v4

    .line 4639
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v1, v7, :cond_1

    iget-object v1, p1, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    iget-object v7, v3, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    if-ne v1, v7, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eq v1, p3, :cond_1

    move-object v1, v2

    .line 4636
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v1

    goto :goto_0

    :cond_0
    move v1, v5

    .line 4639
    goto :goto_1

    .line 4642
    :cond_1
    neg-float v1, p2

    invoke-virtual {v3, v6, v1}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4644
    invoke-virtual {v3}, Lsoftware/simplicial/a/z;->h()F

    move-result v1

    iget v7, v6, Lsoftware/simplicial/a/h;->n:F

    sub-float/2addr v1, v7

    iget v7, v3, Lsoftware/simplicial/a/z;->n:F

    sub-float/2addr v1, v7

    .line 4645
    cmpg-float v7, v1, v0

    if-gez v7, :cond_3

    move v0, v1

    move-object v1, v3

    .line 4648
    goto :goto_2

    .line 4652
    :cond_2
    return-object v2

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method private a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 6759
    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-eqz v0, :cond_0

    .line 6781
    :goto_0
    return-void

    .line 6762
    :cond_0
    const/4 v0, 0x0

    .line 6763
    sget-object v1, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    if-ne p2, v1, :cond_1

    move-object v0, p1

    .line 6765
    :cond_1
    sget-object v1, Lsoftware/simplicial/a/c/f;->a:Lsoftware/simplicial/a/c/f;

    if-ne p4, v1, :cond_2

    move-object v0, p3

    .line 6768
    :cond_2
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v1, v2, :cond_5

    .line 6770
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v1

    .line 6772
    iget-boolean v3, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_4

    .line 6768
    :cond_3
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6775
    :cond_4
    if-eqz v0, :cond_3

    iget-object v3, v2, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v2, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-short v3, v3, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v4, 0x139

    if-lt v3, v4, :cond_3

    .line 6776
    iput-boolean v7, v2, Lsoftware/simplicial/a/bf;->bw:Z

    goto :goto_2

    .line 6779
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lsoftware/simplicial/a/al;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 6780
    iput-boolean v7, p0, Lsoftware/simplicial/a/bs;->v:Z

    goto :goto_0
.end method

.method private a(Ljava/util/List;Z)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bf;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 6786
    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-eqz v0, :cond_0

    .line 6819
    :goto_0
    return-void

    .line 6789
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v0, v0, Lsoftware/simplicial/a/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 6790
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v0, v0, Lsoftware/simplicial/a/b/a;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 6791
    new-instance v4, Ljava/util/HashSet;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v0, v0, Lsoftware/simplicial/a/b/a;->d:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 6795
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v8

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    .line 6797
    iget v6, v0, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v6, v4}, Lsoftware/simplicial/a/b/a;->a(ILjava/util/Collection;)Lsoftware/simplicial/a/b/h;

    move-result-object v6

    .line 6798
    if-nez v6, :cond_1

    .line 6800
    iget v6, v0, Lsoftware/simplicial/a/bf;->A:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    .line 6801
    sget-object v6, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v7, "Failed to find associated gladiator for CID %d AID %d!"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Lsoftware/simplicial/a/bf;->k()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    iget v0, v0, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v8

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    move v0, v1

    :goto_2
    move v1, v0

    .line 6812
    goto :goto_1

    .line 6805
    :cond_1
    iget v7, v6, Lsoftware/simplicial/a/b/h;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6806
    iget-boolean v7, v0, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v7, :cond_2

    .line 6807
    iget-object v7, v6, Lsoftware/simplicial/a/b/h;->c:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6808
    :cond_2
    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 6809
    iput v1, v0, Lsoftware/simplicial/a/bf;->bv:I

    .line 6810
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    goto :goto_2

    .line 6814
    :cond_3
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/h;

    .line 6815
    iget v0, v0, Lsoftware/simplicial/a/b/h;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 6817
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v1, v1, Lsoftware/simplicial/a/b/a;->a:I

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v5, v4, Lsoftware/simplicial/a/b/a;->b:Lsoftware/simplicial/a/b/d;

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-boolean v6, v4, Lsoftware/simplicial/a/b/a;->c:Z

    iget-boolean v7, p0, Lsoftware/simplicial/a/bs;->aR:Z

    move v4, p2

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/al;->a(ILjava/util/List;Ljava/util/List;ZLsoftware/simplicial/a/b/d;ZZ)V

    .line 6818
    iput-boolean v8, p0, Lsoftware/simplicial/a/bs;->v:Z

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private a(Lsoftware/simplicial/a/ae;)V
    .locals 4

    .prologue
    .line 4578
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    const v1, 0x3f2aaaab

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 4580
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 4581
    iget v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v0, v1

    .line 4589
    :goto_0
    const/4 v2, 0x0

    .line 4592
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 4593
    sget v3, Lsoftware/simplicial/a/ae;->a:F

    invoke-virtual {p0, v3, v1, v0}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v3

    iput v3, p1, Lsoftware/simplicial/a/ae;->l:F

    .line 4594
    sget v3, Lsoftware/simplicial/a/ae;->a:F

    invoke-virtual {p0, v3, v1, v0}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v3

    iput v3, p1, Lsoftware/simplicial/a/ae;->m:F

    .line 4596
    const/16 v3, 0xa

    if-ge v2, v3, :cond_1

    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/ao;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;

    move-result-object v3

    if-nez v3, :cond_0

    .line 4598
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4599
    return-void

    .line 4585
    :cond_2
    const/4 v1, 0x0

    .line 4586
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bf;FZF)V
    .locals 18

    .prologue
    .line 4901
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->n()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4970
    :goto_0
    return-void

    .line 4904
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->t()V

    .line 4905
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bf;)V

    .line 4906
    if-nez p3, :cond_1

    .line 4907
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->aX:Z

    .line 4909
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->S:I

    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    move-object v14, v2

    check-cast v14, [Lsoftware/simplicial/a/bh;

    .line 4910
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->l:Ljava/util/Comparator;

    invoke-static {v14, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 4911
    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/bf;->S:I

    .line 4912
    const/4 v3, 0x0

    .line 4913
    const/4 v2, 0x0

    move v15, v2

    move v11, v3

    move v12, v4

    :goto_1
    array-length v2, v14

    if-ge v15, v2, :cond_2

    .line 4915
    aget-object v4, v14, v15

    .line 4917
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    add-int/2addr v2, v11

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->bF:I

    if-lt v2, v3, :cond_3

    .line 4969
    :cond_2
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    add-int/2addr v2, v11

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->S:I

    goto :goto_0

    .line 4920
    :cond_3
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->c()F

    move-result v2

    const/high16 v3, 0x41a00000    # 20.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_b

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v2

    if-nez v2, :cond_b

    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->A:B

    if-gez v2, :cond_b

    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->E:B

    if-gez v2, :cond_b

    .line 4922
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->d()I

    move-result v2

    int-to-float v2, v2

    .line 4926
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v2, v12

    .line 4927
    add-int/lit8 v16, v12, 0x1

    .line 4928
    add-int/lit8 v3, v11, 0x1

    .line 4930
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->d()F

    move-result v2

    const/high16 v6, 0x40000000    # 2.0f

    div-float v12, v2, v6

    .line 4934
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v6

    iget v7, v5, Lsoftware/simplicial/a/bh;->c:I

    iget v8, v4, Lsoftware/simplicial/a/bh;->l:F

    iget v9, v4, Lsoftware/simplicial/a/bh;->m:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p1

    iget v13, v0, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual/range {v5 .. v13}, Lsoftware/simplicial/a/bh;->a(IIFFFFFI)V

    .line 4935
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const/high16 v7, 0x42a00000    # 80.0f

    div-float/2addr v6, v7

    add-float/2addr v2, v6

    iput v2, v5, Lsoftware/simplicial/a/bh;->Q:F

    .line 4936
    if-eqz p3, :cond_6

    .line 4937
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    double-to-float v0, v6

    move/from16 p2, v0

    .line 4938
    :cond_6
    move/from16 v0, p2

    iput v0, v5, Lsoftware/simplicial/a/bh;->P:F

    .line 4939
    const/4 v2, 0x0

    :goto_2
    iget-object v6, v4, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    array-length v6, v6

    if-ge v2, v6, :cond_7

    .line 4940
    iget-object v6, v5, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    iget-object v7, v4, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    aget-object v7, v7, v2

    aput-object v7, v6, v2

    .line 4939
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4941
    :cond_7
    iget v2, v4, Lsoftware/simplicial/a/bh;->q:I

    iput v2, v5, Lsoftware/simplicial/a/bh;->q:I

    .line 4944
    invoke-virtual {v4, v12}, Lsoftware/simplicial/a/bh;->c(F)V

    .line 4946
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->F:B

    iput-byte v2, v5, Lsoftware/simplicial/a/bh;->F:B

    .line 4947
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->G:B

    iput-byte v2, v5, Lsoftware/simplicial/a/bh;->G:B

    .line 4948
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->H:B

    iput-byte v2, v5, Lsoftware/simplicial/a/bh;->H:B

    .line 4949
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v2, :cond_8

    .line 4951
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 4952
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->J:B

    iput-byte v2, v5, Lsoftware/simplicial/a/bh;->J:B

    .line 4953
    const/4 v2, -0x1

    iput-byte v2, v4, Lsoftware/simplicial/a/bh;->J:B

    .line 4955
    :cond_8
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    if-ltz v2, :cond_9

    .line 4957
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    iput-byte v2, v5, Lsoftware/simplicial/a/bh;->D:B

    .line 4958
    const/4 v2, -0x1

    iput-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    .line 4961
    :cond_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 4962
    if-eqz v2, :cond_a

    iget-object v5, v2, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-ne v5, v4, :cond_a

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    iget v5, v2, Lsoftware/simplicial/a/ag;->n:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_a

    .line 4964
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 4965
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->bg:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->bg:I

    :cond_a
    move/from16 v4, v16

    .line 4913
    :goto_3
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    move v11, v3

    move v12, v4

    goto/16 :goto_1

    :cond_b
    move v3, v11

    move v4, v12

    goto :goto_3
.end method

.method private a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bf;Z)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 6697
    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-eqz v0, :cond_0

    .line 6729
    :goto_0
    return-void

    .line 6700
    :cond_0
    if-eqz p1, :cond_2

    iget v2, p1, Lsoftware/simplicial/a/bf;->A:I

    .line 6701
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget v1, p0, Lsoftware/simplicial/a/bs;->aZ:I

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v4, v3, Lsoftware/simplicial/a/i/a;->b:Z

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v5, v3, Lsoftware/simplicial/a/i/a;->c:Z

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lsoftware/simplicial/a/al;->a(IIZZZ)V

    .line 6703
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    if-eqz v0, :cond_5

    .line 6705
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/i/a;->a(I)[Lsoftware/simplicial/a/i/d;

    move-result-object v1

    .line 6706
    array-length v3, v1

    move v0, v6

    :goto_2
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 6708
    iget v4, v4, Lsoftware/simplicial/a/i/d;->a:I

    invoke-direct {p0, v4}, Lsoftware/simplicial/a/bs;->d(I)Lsoftware/simplicial/a/bf;

    move-result-object v4

    .line 6709
    if-eqz v4, :cond_1

    .line 6710
    iput-boolean v7, v4, Lsoftware/simplicial/a/bf;->bx:Z

    .line 6706
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6700
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 6712
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/i/a;->b(I)[Lsoftware/simplicial/a/i/d;

    move-result-object v1

    .line 6713
    array-length v2, v1

    move v0, v6

    :goto_3
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 6715
    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    invoke-direct {p0, v3}, Lsoftware/simplicial/a/bs;->d(I)Lsoftware/simplicial/a/bf;

    move-result-object v3

    .line 6716
    if-eqz v3, :cond_4

    .line 6717
    iput-boolean v7, v3, Lsoftware/simplicial/a/bf;->by:Z

    .line 6713
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6722
    :cond_5
    if-eqz p1, :cond_6

    .line 6723
    iput-boolean v7, p1, Lsoftware/simplicial/a/bf;->bx:Z

    .line 6724
    :cond_6
    if-eqz p2, :cond_7

    .line 6725
    iput-boolean v7, p2, Lsoftware/simplicial/a/bf;->by:Z

    .line 6728
    :cond_7
    iput-boolean v7, p0, Lsoftware/simplicial/a/bs;->v:Z

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V
    .locals 18

    .prologue
    .line 4974
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->bF:I

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->S:I

    sub-int/2addr v2, v3

    .line 4976
    if-nez v2, :cond_0

    .line 5005
    :goto_0
    return-void

    .line 4979
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/bf;)V

    .line 4981
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->c()F

    move-result v3

    .line 4982
    invoke-virtual/range {p2 .. p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v11

    .line 4983
    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    int-to-float v4, v2

    div-float v9, v3, v4

    .line 4984
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v11, v3

    int-to-float v2, v2

    div-float v12, v3, v2

    .line 4985
    :goto_1
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->bF:I

    if-ge v2, v3, :cond_2

    .line 4987
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->S:I

    aget-object v2, v2, v3

    .line 4989
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextFloat()F

    move-result v3

    float-to-double v4, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v13, v4

    .line 4990
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v4, v3

    float-to-double v6, v13

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->n:F

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/bf;->S:I

    aget-object v8, v8, v10

    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    sub-float/2addr v3, v8

    float-to-double v14, v3

    mul-double/2addr v6, v14

    add-double/2addr v4, v6

    double-to-float v5, v4

    .line 4991
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v6, v3

    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->n:F

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v0, p1

    iget v8, v0, Lsoftware/simplicial/a/bf;->S:I

    aget-object v4, v4, v8

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-double v0, v3

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v6, v14

    double-to-float v6, v6

    .line 4993
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v3

    iget v4, v2, Lsoftware/simplicial/a/bh;->c:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual/range {v2 .. v10}, Lsoftware/simplicial/a/bh;->a(IIFFFFFI)V

    .line 4994
    invoke-virtual {v2, v12}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 4995
    const/high16 v3, 0x3ec00000    # 0.375f

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v0, p1

    iget v5, v0, Lsoftware/simplicial/a/bf;->S:I

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    const/high16 v5, 0x42a00000    # 80.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bh;->Q:F

    .line 4997
    iput v13, v2, Lsoftware/simplicial/a/bh;->P:F

    .line 4998
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p2

    iget-object v4, v0, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 4999
    iget-object v4, v2, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    move-object/from16 v0, p2

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    aget-object v5, v5, v3

    aput-object v5, v4, v3

    .line 4998
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 5000
    :cond_1
    move-object/from16 v0, p2

    iget v3, v0, Lsoftware/simplicial/a/bh;->q:I

    iput v3, v2, Lsoftware/simplicial/a/bh;->q:I

    .line 5001
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/bf;->S:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->S:I

    goto/16 :goto_1

    .line 5003
    :cond_2
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v11, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 5004
    move-object/from16 v0, p2

    iget-object v2, v0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v3, Lsoftware/simplicial/a/a/c;

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, p2

    iget v5, v0, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v3, v4, v5}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V
    .locals 12

    .prologue
    .line 5009
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5176
    :cond_0
    :goto_0
    return-void

    .line 5012
    :cond_1
    const/4 v3, 0x0

    .line 5013
    if-eqz p5, :cond_4

    const/4 v0, 0x5

    iget v1, p1, Lsoftware/simplicial/a/bf;->S:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 5014
    :goto_1
    const/4 v1, 0x5

    if-le v0, v1, :cond_17

    .line 5015
    const/4 v0, 0x5

    move v2, v0

    .line 5017
    :goto_2
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    iget-object v0, v0, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-eq v0, p2, :cond_2

    if-eqz p4, :cond_8

    .line 5019
    :cond_2
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    iget-object p2, v0, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    .line 5021
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 5022
    iget-object v0, v1, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    iget v4, v0, Lsoftware/simplicial/a/bh;->c:I

    .line 5023
    const/4 v0, 0x1

    invoke-virtual {v1, p1, v0}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 5025
    instance-of v0, v1, Lsoftware/simplicial/a/bl;

    if-eqz v0, :cond_11

    move-object v0, v1

    .line 5027
    check-cast v0, Lsoftware/simplicial/a/bl;

    .line 5028
    if-eqz p4, :cond_10

    .line 5030
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->u()V

    .line 5031
    iget-object v5, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->e:Lsoftware/simplicial/a/bl$b;

    if-eq v5, v6, :cond_3

    iget-object v5, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->f:Lsoftware/simplicial/a/bl$b;

    if-eq v5, v6, :cond_3

    iget-object v5, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->g:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_5

    .line 5033
    :cond_3
    sget-object v1, Lsoftware/simplicial/a/bs$2;->d:[I

    iget-object v2, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 5048
    :goto_3
    sget-object v1, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    iput-object v1, v0, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 5049
    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/bl;->E:Z

    goto :goto_0

    .line 5013
    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    .line 5036
    :pswitch_0
    const/16 v1, 0x64

    iput-byte v1, p2, Lsoftware/simplicial/a/bh;->F:B

    .line 5037
    iget-object v1, p2, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_3

    .line 5040
    :pswitch_1
    const/16 v1, 0x50

    iput-byte v1, p2, Lsoftware/simplicial/a/bh;->G:B

    .line 5041
    iget-object v1, p2, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_3

    .line 5044
    :pswitch_2
    const/16 v1, 0x14

    iput-byte v1, p2, Lsoftware/simplicial/a/bh;->H:B

    .line 5045
    iget-object v1, p2, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_3

    .line 5054
    :cond_5
    iget-object v5, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_6

    .line 5056
    const/16 v5, 0x1e

    iput-byte v5, p2, Lsoftware/simplicial/a/bh;->J:B

    .line 5057
    iget-object v5, p2, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v6, 0x200

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 5058
    iput-object p2, v0, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 5061
    :cond_6
    sget-object v5, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    iput-object v5, v0, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 5062
    iget v5, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v5, v0, Lsoftware/simplicial/a/bl;->D:I

    .line 5063
    iget-object v5, p2, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v6, Lsoftware/simplicial/a/a/ae;

    iget v7, p1, Lsoftware/simplicial/a/bf;->C:I

    iget-object v8, v0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    invoke-virtual {v8}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v8

    invoke-direct {v6, v7, v4, v8}, Lsoftware/simplicial/a/a/ae;-><init>(III)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5070
    :goto_4
    iput-object p1, v0, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    .line 5071
    const/4 v4, 0x0

    iput-boolean v4, v0, Lsoftware/simplicial/a/bl;->C:Z

    .line 5073
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    .line 5074
    iget v5, p2, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v6, v5

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v4

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v0, Lsoftware/simplicial/a/bl;->l:F

    .line 5075
    iget v5, p2, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v6, v5

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v4, v4

    mul-double/2addr v4, v8

    add-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, v0, Lsoftware/simplicial/a/bl;->m:F

    .line 5076
    const/high16 v4, 0x43340000    # 180.0f

    .line 5077
    float-to-double v6, v4

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v5, v6

    iget v6, p2, Lsoftware/simplicial/a/bh;->a:F

    const/high16 v7, 0x3f400000    # 0.75f

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iput v5, v0, Lsoftware/simplicial/a/bl;->c:F

    .line 5078
    float-to-double v4, v4

    float-to-double v6, p3

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    iget v5, p2, Lsoftware/simplicial/a/bh;->b:F

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iput v4, v0, Lsoftware/simplicial/a/bl;->d:F

    .line 5079
    if-nez p4, :cond_7

    .line 5081
    iget v4, v1, Lsoftware/simplicial/a/ag;->c:F

    const/high16 v5, 0x3f400000    # 0.75f

    mul-float/2addr v4, v5

    iput v4, v1, Lsoftware/simplicial/a/ag;->c:F

    .line 5082
    iget v4, v1, Lsoftware/simplicial/a/ag;->d:F

    const/high16 v5, 0x3f400000    # 0.75f

    mul-float/2addr v4, v5

    iput v4, v1, Lsoftware/simplicial/a/ag;->d:F

    .line 5084
    :cond_7
    iget v1, v0, Lsoftware/simplicial/a/bl;->l:F

    iget v4, v0, Lsoftware/simplicial/a/bl;->c:F

    mul-float v4, v4, p7

    sub-float/2addr v1, v4

    iput v1, v0, Lsoftware/simplicial/a/bl;->l:F

    .line 5085
    iget v1, v0, Lsoftware/simplicial/a/bl;->m:F

    iget v4, v0, Lsoftware/simplicial/a/bl;->d:F

    mul-float v4, v4, p7

    sub-float/2addr v1, v4

    iput v1, v0, Lsoftware/simplicial/a/bl;->m:F

    .line 5102
    :goto_5
    if-nez p4, :cond_0

    .line 5108
    :cond_8
    :goto_6
    if-ge v3, v2, :cond_0

    .line 5110
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->c()F

    move-result v0

    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 5112
    float-to-double v4, v0

    invoke-static {v4, v5}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v4

    double-to-float v1, v4

    add-float v1, v1, p8

    .line 5113
    sub-float/2addr v0, v1

    const/high16 v4, 0x40a00000    # 5.0f

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_0

    .line 5116
    if-nez p5, :cond_12

    .line 5117
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->d()F

    move-result v0

    sub-float/2addr v0, v1

    invoke-virtual {p2, v0}, Lsoftware/simplicial/a/bh;->d(F)V

    .line 5122
    :goto_7
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v0, v0, Lsoftware/simplicial/a/b/a;->b:Lsoftware/simplicial/a/b/d;

    sget-object v4, Lsoftware/simplicial/a/b/d;->c:Lsoftware/simplicial/a/b/d;

    if-eq v0, v4, :cond_a

    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v0, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_13

    .line 5124
    :cond_a
    const v0, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    move v1, v0

    .line 5128
    :goto_8
    if-nez p5, :cond_b

    if-eqz p6, :cond_14

    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v4, v0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v6

    double-to-float v0, v4

    .line 5130
    :goto_9
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->F()Lsoftware/simplicial/a/bq;

    move-result-object v4

    .line 5132
    const/4 v5, 0x0

    iput v5, v4, Lsoftware/simplicial/a/bq;->k:F

    .line 5133
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bq;->A:I

    .line 5134
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->l()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bq;->B:I

    .line 5135
    iget v5, p1, Lsoftware/simplicial/a/bf;->C:I

    iput v5, v4, Lsoftware/simplicial/a/bq;->C:I

    .line 5136
    sget-object v5, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    iput-object v5, v4, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 5137
    invoke-virtual {v4, v1}, Lsoftware/simplicial/a/bq;->c(F)V

    .line 5139
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    .line 5141
    const/4 v6, 0x0

    iput-boolean v6, v4, Lsoftware/simplicial/a/bq;->D:Z

    .line 5142
    const/4 v6, 0x0

    iput-boolean v6, v4, Lsoftware/simplicial/a/bq;->F:Z

    .line 5143
    const/4 v6, 0x1

    iput-boolean v6, v4, Lsoftware/simplicial/a/bq;->G:Z

    .line 5144
    const v6, 0x40c90fdb

    rem-float v6, v0, v6

    iput v6, v4, Lsoftware/simplicial/a/bq;->j:F

    .line 5145
    iget v6, p2, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v6, v6

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v5

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v6, v6

    iput v6, v4, Lsoftware/simplicial/a/bq;->h:F

    .line 5146
    iget v6, p2, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v6, v6

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v10, v5

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bq;->i:F

    .line 5147
    iget v5, v4, Lsoftware/simplicial/a/bq;->h:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_c

    .line 5148
    const/4 v5, 0x0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    add-float/2addr v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bq;->h:F

    .line 5149
    :cond_c
    iget v5, v4, Lsoftware/simplicial/a/bq;->h:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_d

    .line 5150
    iget v5, p0, Lsoftware/simplicial/a/bs;->aE:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bq;->h:F

    .line 5151
    :cond_d
    iget v5, v4, Lsoftware/simplicial/a/bq;->i:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    const/4 v6, 0x0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_e

    .line 5152
    const/4 v5, 0x0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    add-float/2addr v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bq;->i:F

    .line 5153
    :cond_e
    iget v5, v4, Lsoftware/simplicial/a/bq;->i:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_f

    .line 5154
    iget v5, p0, Lsoftware/simplicial/a/bs;->aE:F

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    sub-float/2addr v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bq;->i:F

    .line 5155
    :cond_f
    iget v5, v4, Lsoftware/simplicial/a/bq;->h:F

    iput v5, v4, Lsoftware/simplicial/a/bq;->l:F

    .line 5156
    iget v5, v4, Lsoftware/simplicial/a/bq;->i:F

    iput v5, v4, Lsoftware/simplicial/a/bq;->m:F

    .line 5157
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    const-wide v8, 0x406f400000000000L    # 250.0

    invoke-static {v8, v9}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    const-wide/high16 v8, 0x4035000000000000L    # 21.0

    mul-double/2addr v6, v8

    const-wide v8, 0x405f400000000000L    # 125.0

    add-double/2addr v6, v8

    double-to-float v5, v6

    .line 5158
    iput v5, v4, Lsoftware/simplicial/a/bq;->H:F

    .line 5159
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    float-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-float v6, v6

    iget v7, p2, Lsoftware/simplicial/a/bh;->a:F

    const/high16 v8, 0x3f400000    # 0.75f

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iput v6, v4, Lsoftware/simplicial/a/bq;->a:F

    .line 5160
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    float-to-double v8, v5

    mul-double/2addr v6, v8

    double-to-float v0, v6

    iget v5, p2, Lsoftware/simplicial/a/bh;->b:F

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v5, v6

    add-float/2addr v0, v5

    iput v0, v4, Lsoftware/simplicial/a/bq;->b:F

    .line 5161
    iget v0, v4, Lsoftware/simplicial/a/bq;->l:F

    iget v5, v4, Lsoftware/simplicial/a/bq;->a:F

    mul-float v5, v5, p7

    sub-float/2addr v0, v5

    iput v0, v4, Lsoftware/simplicial/a/bq;->l:F

    .line 5162
    iget v0, v4, Lsoftware/simplicial/a/bq;->m:F

    iget v5, v4, Lsoftware/simplicial/a/bq;->b:F

    mul-float v5, v5, p7

    sub-float/2addr v0, v5

    iput v0, v4, Lsoftware/simplicial/a/bq;->m:F

    .line 5164
    const/4 v0, 0x0

    :goto_a
    iget-object v5, p2, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    array-length v5, v5

    if-ge v0, v5, :cond_15

    .line 5165
    iget-object v5, v4, Lsoftware/simplicial/a/bq;->p:[Lsoftware/simplicial/a/cb;

    iget-object v6, p2, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    aget-object v6, v6, v0

    aput-object v6, v5, v0

    .line 5164
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 5068
    :cond_10
    iget v4, p1, Lsoftware/simplicial/a/bf;->bg:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p1, Lsoftware/simplicial/a/bf;->bg:I

    goto/16 :goto_4

    .line 5089
    :cond_11
    iget v0, p1, Lsoftware/simplicial/a/bf;->bg:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lsoftware/simplicial/a/bf;->bg:I

    .line 5091
    invoke-virtual {p2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v0

    .line 5092
    iget v5, p2, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v6, v5

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    float-to-double v10, v0

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v5, v6

    iput v5, v1, Lsoftware/simplicial/a/ag;->l:F

    .line 5093
    iget v5, p2, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v6, v5

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    float-to-double v10, v0

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v0, v6

    iput v0, v1, Lsoftware/simplicial/a/ag;->m:F

    .line 5094
    const-wide v6, 0x406cc00000000000L    # 230.0

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v0, v6

    iget v5, p2, Lsoftware/simplicial/a/bh;->a:F

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v5, v6

    add-float/2addr v0, v5

    iput v0, v1, Lsoftware/simplicial/a/ag;->c:F

    .line 5095
    const-wide v6, 0x406cc00000000000L    # 230.0

    float-to-double v8, p3

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v0, v6

    iget v5, p2, Lsoftware/simplicial/a/bh;->b:F

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v5, v6

    add-float/2addr v0, v5

    iput v0, v1, Lsoftware/simplicial/a/ag;->d:F

    .line 5096
    iget v0, v1, Lsoftware/simplicial/a/ag;->l:F

    iget v5, v1, Lsoftware/simplicial/a/ag;->c:F

    mul-float v5, v5, p7

    sub-float/2addr v0, v5

    iput v0, v1, Lsoftware/simplicial/a/ag;->l:F

    .line 5097
    iget v0, v1, Lsoftware/simplicial/a/ag;->m:F

    iget v5, v1, Lsoftware/simplicial/a/ag;->d:F

    mul-float v5, v5, p7

    sub-float/2addr v0, v5

    iput v0, v1, Lsoftware/simplicial/a/ag;->m:F

    .line 5099
    iget-object v0, p2, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v1, Lsoftware/simplicial/a/a/w;

    iget v5, p1, Lsoftware/simplicial/a/bf;->C:I

    invoke-direct {v1, v5, v4}, Lsoftware/simplicial/a/a/w;-><init>(II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 5120
    :cond_12
    neg-float v0, v1

    invoke-virtual {p2, v0}, Lsoftware/simplicial/a/bh;->a(F)V

    goto/16 :goto_7

    .line 5126
    :cond_13
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v1, v0

    move v1, v0

    goto/16 :goto_8

    :cond_14
    move v0, p3

    .line 5128
    goto/16 :goto_9

    .line 5166
    :cond_15
    iget v0, p2, Lsoftware/simplicial/a/bh;->q:I

    iput v0, v4, Lsoftware/simplicial/a/bq;->q:I

    .line 5168
    if-eqz p4, :cond_16

    .line 5170
    iget-object v0, p2, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v4, Lsoftware/simplicial/a/a/w;

    iget v5, p1, Lsoftware/simplicial/a/bf;->C:I

    iget v6, p2, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v4, v5, v6}, Lsoftware/simplicial/a/a/w;-><init>(II)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5171
    invoke-virtual {p1, v1}, Lsoftware/simplicial/a/bf;->b(F)V

    .line 5174
    :cond_16
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 5175
    goto/16 :goto_6

    :cond_17
    move v2, v0

    goto/16 :goto_2

    .line 5033
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V
    .locals 1

    .prologue
    .line 5998
    iput-object p2, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    .line 5999
    if-eqz p2, :cond_0

    .line 6000
    iget v0, p2, Lsoftware/simplicial/a/bx;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lsoftware/simplicial/a/bx;->f:I

    .line 6001
    :cond_0
    return-void
.end method

.method private a(Lsoftware/simplicial/a/bf;ZZZ)V
    .locals 30

    .prologue
    .line 6084
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v2, v2, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v14, v2, Lsoftware/simplicial/a/az;->b:J

    .line 6085
    :goto_0
    if-eqz p4, :cond_2

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v2, :cond_2

    .line 6087
    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v3, :cond_2

    .line 6089
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v2

    .line 6090
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 6087
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6084
    :cond_0
    const-wide/16 v14, -0x1

    goto :goto_0

    .line 6092
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;)V

    goto :goto_2

    .line 6095
    :cond_2
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->g(Lsoftware/simplicial/a/bf;)V

    .line 6096
    const/4 v2, -0x1

    move-object/from16 v0, p1

    iput-byte v2, v0, Lsoftware/simplicial/a/bf;->aJ:B

    .line 6098
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v2, :cond_3

    .line 6228
    :goto_3
    return-void

    .line 6102
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/bf;->A:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/bf;->E:[B

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lsoftware/simplicial/a/bs;->aR:Z

    invoke-virtual/range {v2 .. v8}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    .line 6103
    move-object/from16 v0, p0

    iget-wide v2, v0, Lsoftware/simplicial/a/bs;->aO:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/a/bf;->c(J)V

    .line 6104
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->h(Lsoftware/simplicial/a/bf;)Z

    .line 6105
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->i(Lsoftware/simplicial/a/bf;)Z

    .line 6106
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->k(Lsoftware/simplicial/a/bf;)Z

    .line 6107
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->m(Lsoftware/simplicial/a/bf;)Z

    .line 6108
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->n(Lsoftware/simplicial/a/bf;)Z

    .line 6109
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->l(Lsoftware/simplicial/a/bf;)Z

    .line 6110
    invoke-direct/range {p0 .. p1}, Lsoftware/simplicial/a/bs;->j(Lsoftware/simplicial/a/bf;)Z

    .line 6111
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x78

    :goto_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/bf;->d(I)V

    .line 6112
    const/high16 v2, 0x40c00000    # 6.0f

    .line 6113
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_7

    .line 6114
    :cond_4
    const v2, 0x41a33333    # 20.4f

    .line 6117
    :cond_5
    :goto_5
    const/4 v3, 0x0

    .line 6118
    sget-object v4, Lsoftware/simplicial/a/bs$2;->b:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    invoke-virtual {v5}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 6130
    :goto_6
    move-object/from16 v0, p1

    instance-of v4, v0, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_17

    .line 6131
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextFloat()F

    move-result v4

    const/high16 v5, 0x40a00000    # 5.0f

    mul-float/2addr v4, v5

    mul-float/2addr v3, v4

    add-float v9, v2, v3

    .line 6132
    :goto_7
    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v3

    if-ge v2, v3, :cond_8

    .line 6134
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v2

    .line 6136
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->e()V

    .line 6132
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 6111
    :cond_6
    const/16 v2, 0x3c

    goto :goto_4

    .line 6115
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_5

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->c:I

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x14

    const/16 v4, 0xa

    if-ge v3, v4, :cond_5

    .line 6116
    const v2, 0x41a33333    # 20.4f

    goto :goto_5

    .line 6121
    :pswitch_0
    const/high16 v3, 0x3f800000    # 1.0f

    .line 6122
    goto :goto_6

    .line 6124
    :pswitch_1
    const/high16 v3, 0x41200000    # 10.0f

    .line 6125
    goto :goto_6

    .line 6127
    :pswitch_2
    const/high16 v3, 0x420c0000    # 35.0f

    goto :goto_6

    .line 6139
    :cond_8
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 6140
    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v3

    iget v4, v2, Lsoftware/simplicial/a/bh;->c:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual/range {v2 .. v10}, Lsoftware/simplicial/a/bh;->a(IIFFFFFI)V

    .line 6141
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iput v3, v0, Lsoftware/simplicial/a/bf;->aD:F

    .line 6156
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_10

    .line 6158
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aF:F

    .line 6159
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aF:F

    .line 6160
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v5, v6

    .line 6161
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v5, v7

    .line 6196
    :goto_9
    const/4 v7, 0x0

    .line 6199
    :cond_9
    add-int/lit8 v7, v7, 0x1

    .line 6200
    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    add-float/2addr v8, v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v9}, Ljava/util/Random;->nextFloat()F

    move-result v9

    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    sub-float v10, v4, v10

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iput v8, v2, Lsoftware/simplicial/a/bh;->l:F

    .line 6201
    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    add-float/2addr v8, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v9}, Ljava/util/Random;->nextFloat()F

    move-result v9

    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    sub-float v10, v3, v10

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iput v8, v2, Lsoftware/simplicial/a/bh;->m:F

    .line 6203
    const/16 v8, 0x64

    if-ge v7, v8, :cond_a

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    const/high16 v9, -0x3ee00000    # -10.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;F)Lsoftware/simplicial/a/ao;

    move-result-object v8

    if-nez v8, :cond_9

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;

    move-result-object v8

    if-nez v8, :cond_9

    .line 6205
    :cond_a
    move-object/from16 v0, p0

    iget-wide v2, v0, Lsoftware/simplicial/a/bs;->aO:J

    move-object/from16 v0, p1

    iget-wide v4, v0, Lsoftware/simplicial/a/bf;->ap:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_b

    move-object/from16 v0, p1

    iget-wide v2, v0, Lsoftware/simplicial/a/bf;->ap:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 6206
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->an:I

    .line 6207
    :cond_b
    move-object/from16 v0, p0

    iget-wide v2, v0, Lsoftware/simplicial/a/bs;->aO:J

    move-object/from16 v0, p1

    iget-wide v4, v0, Lsoftware/simplicial/a/bf;->aq:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_c

    move-object/from16 v0, p1

    iget-wide v2, v0, Lsoftware/simplicial/a/bf;->aq:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 6208
    sget-object v2, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    move-object/from16 v0, p1

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 6210
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->S:I

    .line 6211
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->K:Z

    .line 6212
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->aA:Z

    .line 6213
    if-nez p2, :cond_d

    if-eqz p3, :cond_f

    .line 6215
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    invoke-virtual/range {p1 .. p1}, Lsoftware/simplicial/a/bf;->l()I

    move-result v3

    sget-object v4, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    move-object/from16 v0, p1

    iget v5, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/bf;->E:[B

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    invoke-virtual {v8}, Lsoftware/simplicial/a/e;->a()I

    move-result v8

    int-to-short v8, v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    iget-byte v9, v9, Lsoftware/simplicial/a/af;->c:B

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/bf;->ad:I

    move-object/from16 v0, p1

    iget v11, v0, Lsoftware/simplicial/a/bf;->ah:I

    move-object/from16 v0, p1

    iget-object v12, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p1

    iget v13, v0, Lsoftware/simplicial/a/bf;->A:I

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    move/from16 v16, v0

    if-eqz v16, :cond_e

    move-object/from16 v0, p1

    iget-object v14, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v14, v14, Lsoftware/simplicial/a/az;->b:J

    :cond_e
    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->I:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Lsoftware/simplicial/a/bf;->an:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lsoftware/simplicial/a/bf;->ap:J

    move-wide/from16 v20, v0

    .line 6218
    invoke-static/range {v20 .. v21}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v20

    const/16 v21, 0x1

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lsoftware/simplicial/a/bf;->aq:J

    move-wide/from16 v24, v0

    .line 6219
    invoke-static/range {v24 .. v25}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v23

    const-wide/16 v24, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    move-object/from16 v28, v0

    .line 6215
    invoke-static/range {v2 .. v28}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;ILjava/lang/String;[BSBIILsoftware/simplicial/a/bx;IJLjava/lang/String;[BLsoftware/simplicial/a/q;IIZLsoftware/simplicial/a/s;IJLsoftware/simplicial/a/bd;Ljava/lang/String;Lsoftware/simplicial/a/as;)[B

    move-result-object v2

    .line 6221
    if-eqz p2, :cond_16

    .line 6222
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v3, v2, v4}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 6227
    :cond_f
    :goto_a
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, p1

    iput v2, v0, Lsoftware/simplicial/a/bf;->aR:I

    goto/16 :goto_3

    .line 6163
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_13

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v3, :cond_13

    .line 6165
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    .line 6166
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->l:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    cmpg-float v3, v3, v4

    if-gez v3, :cond_11

    .line 6167
    const/4 v3, 0x0

    .line 6171
    :goto_b
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    .line 6172
    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v6, v6, Lsoftware/simplicial/a/bx;->m:F

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    cmpg-float v6, v6, v7

    if-gez v6, :cond_12

    .line 6173
    const/4 v6, 0x0

    move/from16 v29, v4

    move v4, v5

    move v5, v6

    move v6, v3

    move/from16 v3, v29

    goto/16 :goto_9

    .line 6169
    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    goto :goto_b

    .line 6175
    :cond_12
    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move/from16 v29, v4

    move v4, v5

    move v5, v6

    move v6, v3

    move/from16 v3, v29

    goto/16 :goto_9

    .line 6177
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_15

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v3, :cond_15

    .line 6179
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    .line 6180
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->l:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    .line 6181
    const/4 v3, 0x0

    .line 6185
    :goto_c
    const/4 v6, 0x0

    .line 6186
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    move/from16 v29, v4

    move v4, v5

    move v5, v6

    move v6, v3

    move/from16 v3, v29

    goto/16 :goto_9

    .line 6183
    :cond_14
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    goto :goto_c

    .line 6190
    :cond_15
    const/4 v6, 0x0

    .line 6191
    const/4 v5, 0x0

    .line 6192
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    .line 6193
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aE:F

    goto/16 :goto_9

    .line 6223
    :cond_16
    if-eqz p3, :cond_f

    move-object/from16 v0, p1

    instance-of v3, v0, Lsoftware/simplicial/a/i;

    if-nez v3, :cond_f

    .line 6224
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v2, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto/16 :goto_a

    :cond_17
    move v9, v2

    goto/16 :goto_7

    .line 6118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/bh;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 6314
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->F()Lsoftware/simplicial/a/bq;

    move-result-object v0

    .line 6315
    if-nez v0, :cond_0

    .line 6317
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v1, "Unexpected null (serverBlob) in Eject()"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 6340
    :goto_0
    return-void

    .line 6321
    :cond_0
    iput v3, v0, Lsoftware/simplicial/a/bq;->k:F

    .line 6322
    iput v4, v0, Lsoftware/simplicial/a/bq;->A:I

    .line 6323
    iput v4, v0, Lsoftware/simplicial/a/bq;->B:I

    .line 6324
    const/4 v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/bq;->C:I

    .line 6325
    sget-object v1, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    iput-object v1, v0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 6326
    invoke-virtual {p1}, Lsoftware/simplicial/a/bh;->c()F

    move-result v1

    invoke-virtual {p1}, Lsoftware/simplicial/a/bh;->d()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/bq;->c(F)V

    .line 6328
    iput-boolean v4, v0, Lsoftware/simplicial/a/bq;->D:Z

    .line 6329
    iput-boolean v5, v0, Lsoftware/simplicial/a/bq;->F:Z

    .line 6330
    iput-boolean v5, v0, Lsoftware/simplicial/a/bq;->G:Z

    .line 6331
    iget v1, p1, Lsoftware/simplicial/a/bh;->l:F

    iput v1, v0, Lsoftware/simplicial/a/bq;->h:F

    .line 6332
    iget v1, p1, Lsoftware/simplicial/a/bh;->m:F

    iput v1, v0, Lsoftware/simplicial/a/bq;->i:F

    .line 6334
    iget v1, v0, Lsoftware/simplicial/a/bq;->h:F

    iput v1, v0, Lsoftware/simplicial/a/bq;->l:F

    .line 6335
    iget v1, v0, Lsoftware/simplicial/a/bq;->i:F

    iput v1, v0, Lsoftware/simplicial/a/bq;->m:F

    .line 6336
    iput v3, v0, Lsoftware/simplicial/a/bq;->a:F

    .line 6337
    iput v3, v0, Lsoftware/simplicial/a/bq;->b:F

    .line 6339
    iput v3, v0, Lsoftware/simplicial/a/bq;->j:F

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V
    .locals 3

    .prologue
    .line 2582
    if-nez p1, :cond_1

    .line 2590
    :cond_0
    return-void

    .line 2584
    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v0, v1, :cond_0

    .line 2586
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v1, v1, v0

    .line 2587
    iget-object v2, v1, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    if-ne v2, p1, :cond_2

    .line 2588
    iput-object p2, v1, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 2584
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bl;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6897
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aS:[Z

    invoke-virtual {p1, v2, v0, v3}, Lsoftware/simplicial/a/bl;->a(Ljava/util/Random;Z[Z)V

    .line 6901
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_3

    .line 6903
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v2, p0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v0, v2

    .line 6904
    iget v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v0, v2

    .line 6915
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 6916
    sget v3, Lsoftware/simplicial/a/bl;->a:F

    invoke-virtual {p0, v3, v2, v0}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v3

    iput v3, p1, Lsoftware/simplicial/a/bl;->l:F

    .line 6917
    sget v3, Lsoftware/simplicial/a/bl;->a:F

    invoke-virtual {p0, v3, v2, v0}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v3

    iput v3, p1, Lsoftware/simplicial/a/bl;->m:F

    .line 6919
    const/16 v3, 0xa

    if-ge v1, v3, :cond_1

    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/ao;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;

    move-result-object v3

    if-nez v3, :cond_0

    .line 6921
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, p0, Lsoftware/simplicial/a/bs;->p:I

    .line 6922
    return-void

    :cond_2
    move v0, v1

    .line 6897
    goto :goto_0

    .line 6908
    :cond_3
    const/4 v2, 0x0

    .line 6909
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v0, v2

    goto :goto_1
.end method

.method private a(Lsoftware/simplicial/a/ca;)V
    .locals 8

    .prologue
    const v0, 0x429c8000    # 78.25f

    const v7, 0x3e00adfd

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 6886
    .line 6887
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/bs;->a(F)F

    move-result v1

    .line 6888
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/bs;->a(F)F

    move-result v2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    .line 6889
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    const v3, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v0, v3

    mul-float v3, v0, v6

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    .line 6890
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    mul-float/2addr v0, v5

    mul-float/2addr v0, v6

    sub-float v4, v0, v5

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    .line 6891
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    mul-float/2addr v0, v5

    mul-float/2addr v0, v6

    sub-float v5, v0, v5

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    .line 6892
    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    mul-float/2addr v0, v7

    mul-float/2addr v0, v6

    sub-float v6, v0, v7

    move-object v0, p1

    .line 6886
    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ca;->a(FFFFFF)V

    .line 6893
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/ai;)V
    .locals 14

    .prologue
    .line 6926
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1

    .line 7040
    :cond_0
    :goto_0
    return-void

    .line 6929
    :cond_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 6932
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    .line 6933
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    .line 6935
    :cond_2
    const-string v0, "NULL"

    .line 6936
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 6941
    iget v0, p1, Lsoftware/simplicial/a/f/ai;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v11

    .line 6943
    if-eqz v11, :cond_a

    .line 6945
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-wide v2, v11, Lsoftware/simplicial/a/bf;->aW:J

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 6947
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iput-wide v0, v11, Lsoftware/simplicial/a/bf;->aW:J

    .line 6949
    iget v0, v11, Lsoftware/simplicial/a/bf;->A:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_3

    .line 6951
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v11}, Lsoftware/simplicial/a/bf;->l()I

    move-result v2

    sget-object v3, Lsoftware/simplicial/a/f/ba;->u:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v2, v3}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_0

    .line 6955
    :cond_3
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-wide v2, v11, Lsoftware/simplicial/a/bf;->B:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 6957
    iget-object v8, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    const/4 v1, 0x0

    const-string v2, "SERVER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "YOU HAVE BEEN MUTED BY A MODERATOR FOR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v11, Lsoftware/simplicial/a/bf;->B:J

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->aO:J

    sub-long/2addr v4, v6

    .line 6958
    invoke-static {v4, v5}, Lsoftware/simplicial/a/ba;->b(J)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " HOURS."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const/4 v5, -0x1

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->A:J

    const-wide/16 v12, 0x1

    add-long/2addr v12, v6

    iput-wide v12, p0, Lsoftware/simplicial/a/bs;->A:J

    .line 6957
    invoke-static/range {v0 .. v7}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;Lsoftware/simplicial/a/q;IJ)[B

    move-result-object v0

    iget-object v1, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v8, v0, v1}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 6960
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v3, Lsoftware/simplicial/a/f/ba;->w:Lsoftware/simplicial/a/f/ba;

    iget-wide v4, v11, Lsoftware/simplicial/a/bf;->B:J

    invoke-static {v1, v2, v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;J)[B

    move-result-object v1

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto/16 :goto_0

    .line 6964
    :cond_4
    iget-object v0, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 6965
    iget-object v1, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 6966
    iget-boolean v2, v11, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v2, :cond_5

    iget-object v2, p1, Lsoftware/simplicial/a/f/ai;->a:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    iget-object v4, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    invoke-direct {p0, v11, v2, v3, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6968
    :cond_5
    iget-object v2, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v11, Lsoftware/simplicial/a/bf;->F:[B

    iget-object v2, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 6969
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v7, v7, Lsoftware/simplicial/a/bd;->c:B

    const/4 v8, 0x0

    iget-object v9, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-interface/range {v0 .. v10}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/f/bl;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 6972
    :cond_7
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    iput-object v0, v11, Lsoftware/simplicial/a/bf;->F:[B

    .line 6974
    iget-object v2, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 6975
    iget-object v0, v11, Lsoftware/simplicial/a/bf;->E:[B

    iget-object v1, v11, Lsoftware/simplicial/a/bf;->E:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 6976
    iget-object v0, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v1, v0, Lsoftware/simplicial/a/f/bl;->b:I

    .line 6977
    iget v4, v11, Lsoftware/simplicial/a/bf;->A:I

    .line 6978
    const/4 v5, 0x0

    .line 7021
    :goto_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 7024
    iput-object v0, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    .line 7026
    sget-boolean v0, Lsoftware/simplicial/a/ba;->b:Z

    if-eqz v0, :cond_8

    .line 7027
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 7029
    :cond_8
    new-instance v9, Ljava/util/ArrayList;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 7031
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_9

    if-nez v11, :cond_9

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-ltz v0, :cond_9

    .line 7034
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v6, 0x0

    aget-object v0, v0, v6

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v9, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 7035
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    const/4 v6, 0x1

    aget-object v0, v0, v6

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v9, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 7037
    :cond_9
    iget-object v10, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->A:J

    const-wide/16 v12, 0x1

    add-long/2addr v12, v6

    iput-wide v12, p0, Lsoftware/simplicial/a/bs;->A:J

    iget-object v8, p1, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v8}, Lsoftware/simplicial/a/f/ai;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;[BIZJLjava/lang/String;)[B

    move-result-object v0

    invoke-interface {v10, v0, v9}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    goto/16 :goto_0

    .line 6982
    :cond_a
    iget v0, p1, Lsoftware/simplicial/a/f/ai;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->e(I)Lsoftware/simplicial/a/bw;

    move-result-object v12

    .line 6983
    if-eqz v12, :cond_0

    .line 6986
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-wide v2, v12, Lsoftware/simplicial/a/bw;->j:J

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 6988
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iput-wide v0, v12, Lsoftware/simplicial/a/bw;->j:J

    .line 6990
    iget v0, v12, Lsoftware/simplicial/a/bw;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_b

    .line 6992
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v2, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v3, Lsoftware/simplicial/a/f/ba;->u:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v2, v3}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v2, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto/16 :goto_0

    .line 6996
    :cond_b
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-wide v2, v12, Lsoftware/simplicial/a/bw;->l:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_c

    .line 6998
    iget-object v8, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    const/4 v1, 0x0

    const-string v2, "SERVER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "YOU HAVE BEEN MUTED BY A MODERATOR FOR "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v12, Lsoftware/simplicial/a/bw;->l:J

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->aO:J

    sub-long/2addr v4, v6

    .line 6999
    invoke-static {v4, v5}, Lsoftware/simplicial/a/ba;->b(J)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " HOURS."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const/4 v5, -0x1

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->A:J

    const-wide/16 v10, 0x1

    add-long/2addr v10, v6

    iput-wide v10, p0, Lsoftware/simplicial/a/bs;->A:J

    .line 6998
    invoke-static/range {v0 .. v7}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;Lsoftware/simplicial/a/q;IJ)[B

    move-result-object v0

    iget-object v1, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v8, v0, v1}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 7001
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v2, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v3, Lsoftware/simplicial/a/f/ba;->w:Lsoftware/simplicial/a/f/ba;

    iget-wide v4, v12, Lsoftware/simplicial/a/bw;->l:J

    invoke-static {v1, v2, v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;J)[B

    move-result-object v1

    iget-object v2, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto/16 :goto_0

    .line 7005
    :cond_c
    iget-object v0, v12, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    .line 7006
    iget-object v1, p1, Lsoftware/simplicial/a/f/ai;->a:Ljava/lang/String;

    iget-object v2, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    invoke-direct {p0, v12, v1, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bw;Ljava/lang/String;[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7008
    iget-object v1, v12, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v12, Lsoftware/simplicial/a/bw;->d:[B

    iget-object v1, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_e

    .line 7009
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v2, v12, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v7, v7, Lsoftware/simplicial/a/bd;->c:B

    const/4 v8, 0x0

    const-string v9, ""

    const/4 v10, 0x0

    invoke-interface/range {v0 .. v10}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/f/bl;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 7012
    :cond_e
    iget-object v0, p1, Lsoftware/simplicial/a/f/ai;->b:[B

    iput-object v0, v12, Lsoftware/simplicial/a/bw;->d:[B

    .line 7014
    iget-object v2, v12, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    .line 7015
    iget-object v3, v12, Lsoftware/simplicial/a/bw;->c:[B

    .line 7016
    iget-object v0, v12, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v1, v0, Lsoftware/simplicial/a/f/bl;->b:I

    .line 7017
    iget v4, v12, Lsoftware/simplicial/a/bw;->k:I

    .line 7018
    const/4 v5, 0x1

    goto/16 :goto_1
.end method

.method private a(Lsoftware/simplicial/a/f/bd;)V
    .locals 2

    .prologue
    .line 7044
    iget v0, p1, Lsoftware/simplicial/a/f/bd;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->b(I)Lsoftware/simplicial/a/f/bl;

    move-result-object v0

    .line 7045
    if-eqz v0, :cond_0

    .line 7047
    iget-object v1, p1, Lsoftware/simplicial/a/f/bd;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v1, v1, Lsoftware/simplicial/a/f/bj;->d:Ljava/net/InetSocketAddress;

    iput-object v1, v0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 7048
    iget-object v1, p1, Lsoftware/simplicial/a/f/bd;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v1, v1, Lsoftware/simplicial/a/f/bj;->e:Ljava/net/DatagramSocket;

    iput-object v1, v0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 7054
    iget v0, p1, Lsoftware/simplicial/a/f/bd;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 7055
    if-eqz v0, :cond_0

    iget v1, v0, Lsoftware/simplicial/a/bf;->aN:I

    if-lez v1, :cond_0

    .line 7057
    iget-object v1, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/f/bl;)V

    .line 7058
    iget v1, v0, Lsoftware/simplicial/a/bf;->aN:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/bf;->aN:I

    .line 7061
    :cond_0
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bk;)V
    .locals 4

    .prologue
    .line 6345
    iget v0, p1, Lsoftware/simplicial/a/f/bk;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 6346
    if-eqz v0, :cond_0

    .line 6347
    iget-wide v2, p1, Lsoftware/simplicial/a/f/bk;->a:J

    iput-wide v2, v0, Lsoftware/simplicial/a/bf;->B:J

    .line 6349
    :cond_0
    iget v0, p1, Lsoftware/simplicial/a/f/bk;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->e(I)Lsoftware/simplicial/a/bw;

    move-result-object v0

    .line 6350
    if-eqz v0, :cond_1

    .line 6351
    iget-wide v2, p1, Lsoftware/simplicial/a/f/bk;->a:J

    iput-wide v2, v0, Lsoftware/simplicial/a/bw;->l:J

    .line 6352
    :cond_1
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bl;)V
    .locals 12

    .prologue
    .line 5561
    iget-short v0, p1, Lsoftware/simplicial/a/f/bl;->d:S

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-short v0, p1, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v1, 0x181

    if-lt v0, v1, :cond_1

    :cond_0
    iget-short v0, p1, Lsoftware/simplicial/a/f/bl;->d:S

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_2

    iget-short v0, p1, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v1, 0x172

    if-ge v0, v1, :cond_2

    .line 5564
    :cond_1
    iget-object v8, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    const/4 v1, 0x0

    const-string v2, "SERVER"

    const-string v3, "New Update Available!."

    sget-object v4, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    const/4 v5, -0x1

    iget-wide v6, p0, Lsoftware/simplicial/a/bs;->A:J

    const-wide/16 v10, 0x1

    add-long/2addr v10, v6

    iput-wide v10, p0, Lsoftware/simplicial/a/bs;->A:J

    invoke-static/range {v0 .. v7}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;Lsoftware/simplicial/a/q;IJ)[B

    move-result-object v0

    invoke-interface {v8, v0, p1}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5567
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget v2, p1, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v3, Lsoftware/simplicial/a/f/ba;->r:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v2, v3}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5569
    :cond_2
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/v;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 5512
    iget v0, p1, Lsoftware/simplicial/a/f/v;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v1

    .line 5513
    if-eqz v1, :cond_2

    iget-object v0, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v0, v0, Lsoftware/simplicial/a/f/bl;->b:I

    iget v2, p1, Lsoftware/simplicial/a/f/v;->a:I

    if-ne v0, v2, :cond_2

    iget-object v0, v1, Lsoftware/simplicial/a/bf;->aM:[B

    if-eqz v0, :cond_2

    .line 5515
    iget-object v0, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p1, Lsoftware/simplicial/a/f/v;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v2, v2, Lsoftware/simplicial/a/f/bj;->d:Ljava/net/InetSocketAddress;

    iput-object v2, v0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 5516
    iget-object v0, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p1, Lsoftware/simplicial/a/f/v;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v2, v2, Lsoftware/simplicial/a/f/bj;->e:Ljava/net/DatagramSocket;

    iput-object v2, v0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 5518
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v2, v1, Lsoftware/simplicial/a/bf;->aM:[B

    iget-object v3, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5520
    iget v0, v1, Lsoftware/simplicial/a/bf;->aN:I

    if-lez v0, :cond_0

    .line 5522
    iget-object v0, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/f/bl;)V

    .line 5523
    iget v0, v1, Lsoftware/simplicial/a/bf;->aN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Lsoftware/simplicial/a/bf;->aN:I

    .line 5526
    :cond_0
    iget-object v0, v1, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v0, :cond_1

    .line 5528
    iput-boolean v5, v1, Lsoftware/simplicial/a/bf;->bp:Z

    .line 5529
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    sget-object v2, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne v0, v2, :cond_1

    .line 5530
    iput-boolean v5, v1, Lsoftware/simplicial/a/bf;->bq:Z

    .line 5533
    :cond_1
    iget-object v0, v1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/bl;)V

    .line 5536
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    iget v2, p1, Lsoftware/simplicial/a/f/v;->ar:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bw;

    .line 5537
    if-eqz v0, :cond_4

    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->b:I

    iget v3, p1, Lsoftware/simplicial/a/f/v;->a:I

    if-ne v2, v3, :cond_4

    iget-object v2, v0, Lsoftware/simplicial/a/bw;->h:[B

    if-eqz v2, :cond_4

    .line 5539
    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v3, p1, Lsoftware/simplicial/a/f/v;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v3, v3, Lsoftware/simplicial/a/f/bj;->d:Ljava/net/InetSocketAddress;

    iput-object v3, v2, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 5540
    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v3, p1, Lsoftware/simplicial/a/f/v;->c:Lsoftware/simplicial/a/f/bj;

    iget-object v3, v3, Lsoftware/simplicial/a/f/bj;->e:Ljava/net/DatagramSocket;

    iput-object v3, v2, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 5542
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v3, v0, Lsoftware/simplicial/a/bw;->h:[B

    iget-object v4, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v2, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5544
    iget-boolean v2, v0, Lsoftware/simplicial/a/bw;->i:Z

    if-nez v2, :cond_3

    .line 5546
    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-direct {p0, v2}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/f/bl;)V

    .line 5547
    iput-boolean v5, v0, Lsoftware/simplicial/a/bw;->i:Z

    .line 5550
    :cond_3
    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-direct {p0, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/bl;)V

    .line 5553
    :cond_4
    if-nez v1, :cond_5

    if-nez v0, :cond_5

    .line 5555
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to find client "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lsoftware/simplicial/a/f/v;->ar:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for OnConnectRequest2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5557
    :cond_5
    return-void
.end method

.method private a(Lsoftware/simplicial/a/h/d;)V
    .locals 8

    .prologue
    .line 6750
    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-eqz v0, :cond_0

    .line 6755
    :goto_0
    return-void

    .line 6753
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v1, v1, Lsoftware/simplicial/a/h/c;->a:I

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v2, v2, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget v2, v2, Lsoftware/simplicial/a/h/a;->a:I

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget v3, v3, Lsoftware/simplicial/a/h/a;->a:I

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-boolean v5, v4, Lsoftware/simplicial/a/h/c;->d:Z

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v6, v4, Lsoftware/simplicial/a/h/c;->e:Lsoftware/simplicial/a/h/f;

    iget-boolean v7, p0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object v4, p1

    invoke-interface/range {v0 .. v7}, Lsoftware/simplicial/a/al;->a(IIILsoftware/simplicial/a/h/d;ZLsoftware/simplicial/a/h/f;Z)V

    .line 6754
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    goto :goto_0
.end method

.method private a(ZZF)V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 4428
    if-eqz p2, :cond_0

    .line 4430
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aX:S

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 4431
    iput v9, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 4432
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, p0, Lsoftware/simplicial/a/bs;->s:I

    .line 4434
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->g()V

    .line 4436
    iput v9, p0, Lsoftware/simplicial/a/bs;->B:I

    move v8, v9

    .line 4439
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v0, v0

    if-ge v8, v0, :cond_1

    .line 4441
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v0, v0, v8

    .line 4443
    iget v1, v0, Lsoftware/simplicial/a/bq;->c:I

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/a/bq;->a(IFFFFFF)V

    .line 4439
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    move v0, v9

    .line 4446
    :goto_1
    iget v1, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v0, v1, :cond_2

    .line 4448
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v1, v1, v0

    .line 4450
    iput v9, v1, Lsoftware/simplicial/a/bx;->e:I

    .line 4451
    iput v9, v1, Lsoftware/simplicial/a/bx;->d:I

    .line 4446
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v9

    .line 4455
    :goto_2
    iget v1, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v0, v1, :cond_3

    .line 4457
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v1, v1, v0

    .line 4459
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 4455
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v9

    .line 4462
    :goto_3
    iget v1, p0, Lsoftware/simplicial/a/bs;->ar:I

    if-ge v0, v1, :cond_5

    move v1, v9

    .line 4464
    :goto_4
    iget v2, p0, Lsoftware/simplicial/a/bs;->ar:I

    if-ge v1, v2, :cond_4

    .line 4466
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v2, v2, v1

    aget-object v2, v2, v0

    .line 4468
    invoke-virtual {v2}, Lsoftware/simplicial/a/bz;->b()V

    .line 4464
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4462
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4472
    :cond_5
    if-eqz p1, :cond_a

    move v6, v9

    .line 4474
    :goto_5
    iget v0, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v6, v0, :cond_7

    .line 4476
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v0, v0, v6

    .line 4478
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v5, v7, :cond_6

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v6

    :goto_6
    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/a/z;->a(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    .line 4474
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 4478
    :cond_6
    const/4 v5, 0x0

    goto :goto_6

    :cond_7
    move v0, v9

    .line 4481
    :goto_7
    iget v1, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v0, v1, :cond_8

    .line 4483
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v1, v1, v0

    .line 4485
    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bl;)V

    .line 4481
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_8
    move v0, v9

    .line 4489
    :goto_8
    iget v1, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v0, v1, :cond_9

    .line 4491
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v1, v1, v0

    .line 4493
    invoke-direct {p0, v1, p3}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/g;F)V

    .line 4489
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    move v0, v9

    .line 4497
    :goto_9
    iget v1, p0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v0, v1, :cond_a

    .line 4499
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v1, v1, v0

    .line 4501
    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ca;)V

    .line 4497
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 4505
    :cond_a
    invoke-virtual {p0, v9, v9}, Lsoftware/simplicial/a/bs;->a(IZ)V

    .line 4507
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_e

    .line 4509
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v9

    .line 4511
    :goto_a
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_c

    .line 4513
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 4515
    iget-boolean v3, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_b

    .line 4511
    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 4518
    :cond_b
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 4521
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-static {v2, v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    .line 4523
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    int-to-double v0, v0

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v1, v9

    .line 4524
    :goto_c
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 4526
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    .line 4527
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->f(Lsoftware/simplicial/a/bf;)V

    .line 4528
    if-ge v1, v3, :cond_d

    .line 4529
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v4, v4, v10

    invoke-direct {p0, v0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    .line 4524
    :goto_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 4531
    :cond_d
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v4, v4, v9

    invoke-direct {p0, v0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    goto :goto_d

    :cond_e
    move v0, v9

    .line 4535
    :goto_e
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_16

    .line 4537
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v1, v0

    .line 4539
    iget-boolean v1, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v1, :cond_f

    .line 4535
    :goto_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 4542
    :cond_f
    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->v()V

    .line 4543
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_10

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v1, v3, :cond_11

    .line 4544
    :cond_10
    iput v9, v2, Lsoftware/simplicial/a/bf;->Q:I

    .line 4550
    :cond_11
    iget-boolean v1, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v1, :cond_13

    instance-of v1, v2, Lsoftware/simplicial/a/i;

    if-nez v1, :cond_13

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_13

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_13

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-ne v1, v3, :cond_12

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v1, :cond_13

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v1, :cond_13

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v1, :cond_13

    :cond_12
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_15

    .line 4553
    :cond_13
    iget-boolean v1, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v1, :cond_14

    move v1, v10

    :goto_10
    invoke-direct {p0, v2, v1, v10, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    goto :goto_f

    :cond_14
    move v1, v9

    goto :goto_10

    .line 4555
    :cond_15
    iput-boolean v9, v2, Lsoftware/simplicial/a/bf;->aA:Z

    goto :goto_f

    .line 4558
    :cond_16
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 4560
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->b:[Lsoftware/simplicial/a/bf;

    array-length v2, v1

    move v0, v9

    :goto_11
    if-ge v0, v2, :cond_17

    aget-object v3, v1, v0

    .line 4561
    iput-byte v11, v3, Lsoftware/simplicial/a/bf;->R:B

    .line 4560
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 4562
    :cond_17
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    array-length v2, v1

    move v0, v9

    :goto_12
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    .line 4563
    iput-byte v11, v3, Lsoftware/simplicial/a/i;->R:B

    .line 4562
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_18
    move v0, v9

    .line 4566
    :goto_13
    const/16 v1, 0x14

    if-ge v0, v1, :cond_19

    .line 4568
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->x:[I

    aput v9, v1, v0

    .line 4569
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->y:[I

    aput v9, v1, v0

    .line 4570
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->z:[I

    aput v9, v1, v0

    .line 4566
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 4572
    :cond_19
    return-void
.end method

.method private a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 5312
    new-array v1, v2, [B

    iput-object v1, p1, Lsoftware/simplicial/a/bf;->E:[B

    .line 5314
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v3, :cond_0

    .line 5315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p1, Lsoftware/simplicial/a/bf;->C:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 5317
    :cond_0
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    invoke-virtual {v1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5319
    if-eqz p3, :cond_2

    iget-boolean v1, p1, Lsoftware/simplicial/a/bf;->G:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_2

    .line 5320
    :cond_1
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    array-length v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p1, Lsoftware/simplicial/a/bf;->E:[B

    .line 5354
    :cond_2
    :goto_0
    return v0

    .line 5325
    :cond_3
    instance-of v1, p1, Lsoftware/simplicial/a/i;

    if-nez v1, :cond_4

    invoke-static {p2}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 5327
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v4, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v3, v4}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    move v0, v2

    .line 5329
    goto :goto_0

    .line 5332
    :cond_4
    invoke-static {p4}, Lsoftware/simplicial/a/ba;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 5334
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v4, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v3, v4}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    move v0, v2

    .line 5336
    goto :goto_0

    .line 5339
    :cond_5
    invoke-direct {p0, p2}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 5340
    if-eqz v1, :cond_8

    if-eq v1, p1, :cond_8

    .line 5342
    instance-of v0, v1, Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_6

    move-object v0, v1

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5343
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v4, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v5, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    invoke-static {v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v3

    iget-object v4, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5344
    :cond_6
    instance-of v0, v1, Lsoftware/simplicial/a/bw;

    if-eqz v0, :cond_7

    check-cast v1, Lsoftware/simplicial/a/bw;

    iget-object v0, v1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5345
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v4, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v3, v4}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v3, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    :cond_7
    move v0, v2

    .line 5346
    goto/16 :goto_0

    .line 5349
    :cond_8
    iput-object p2, p1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 5350
    if-eqz p3, :cond_a

    iget-boolean v1, p1, Lsoftware/simplicial/a/bf;->G:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_a

    .line 5351
    :cond_9
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    array-length v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p1, Lsoftware/simplicial/a/bf;->E:[B

    .line 5352
    :cond_a
    iput-object p4, p1, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bw;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 6617
    iget-boolean v1, p1, Lsoftware/simplicial/a/bw;->e:Z

    if-nez v1, :cond_0

    .line 6619
    new-array v1, v0, [B

    iput-object v1, p1, Lsoftware/simplicial/a/bw;->c:[B

    .line 6624
    :goto_0
    return v0

    .line 6623
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/bw;->d:[B

    iput-object v0, p1, Lsoftware/simplicial/a/bw;->c:[B

    .line 6624
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bw;Ljava/lang/String;[B)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 5359
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v3, :cond_0

    .line 5360
    const-string p2, "Spectator"

    .line 5362
    :cond_0
    iget-object v1, p1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5364
    if-eqz p3, :cond_1

    iget-boolean v1, p1, Lsoftware/simplicial/a/bw;->e:Z

    if-eqz v1, :cond_1

    .line 5365
    iget-object v1, p1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    array-length v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p1, Lsoftware/simplicial/a/bw;->c:[B

    .line 5392
    :cond_1
    :goto_0
    return v0

    .line 5370
    :cond_2
    invoke-static {p2}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5372
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v4, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v3, v4}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v3, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    move v0, v2

    .line 5374
    goto :goto_0

    .line 5377
    :cond_3
    invoke-direct {p0, p2}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 5378
    if-eqz v1, :cond_6

    .line 5380
    instance-of v0, v1, Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5381
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v4, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v5, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    invoke-static {v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v3

    iget-object v4, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5382
    :cond_4
    instance-of v0, v1, Lsoftware/simplicial/a/bw;

    if-eqz v0, :cond_5

    check-cast v1, Lsoftware/simplicial/a/bw;

    iget-object v0, v1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 5383
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v4, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    invoke-static {v1, v3, v4}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v1

    iget-object v3, p1, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1, v3}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    :cond_5
    move v0, v2

    .line 5384
    goto :goto_0

    .line 5387
    :cond_6
    iput-object p2, p1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    .line 5389
    if-eqz p3, :cond_1

    iget-boolean v1, p1, Lsoftware/simplicial/a/bw;->e:Z

    if-eqz v1, :cond_1

    .line 5390
    iget-object v1, p1, Lsoftware/simplicial/a/bw;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    array-length v2, p3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p1, Lsoftware/simplicial/a/bw;->c:[B

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/i;)Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5905
    iget v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    iget v3, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ne v0, v3, :cond_0

    .line 5907
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to add bot %d to game %d. Game was full!"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lsoftware/simplicial/a/i;->k()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget v5, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5929
    :goto_0
    return v1

    .line 5911
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v0, v3, :cond_3

    :cond_1
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    iget-short v3, p0, Lsoftware/simplicial/a/bs;->aX:S

    add-int/lit8 v3, v3, -0x3c

    if-ge v0, v3, :cond_3

    .line 5912
    :cond_2
    iput-boolean v2, p1, Lsoftware/simplicial/a/i;->aA:Z

    :cond_3
    move v0, v1

    .line 5914
    :goto_1
    iget v3, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v3, :cond_5

    .line 5916
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v3, v0

    iget-boolean v3, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_4

    .line 5918
    iput-boolean v2, p1, Lsoftware/simplicial/a/i;->aF:Z

    .line 5919
    iput v0, p1, Lsoftware/simplicial/a/i;->C:I

    .line 5920
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aput-object p1, v1, v0

    .line 5921
    iget v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    .line 5922
    iget v0, p0, Lsoftware/simplicial/a/bs;->q:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bs;->q:I

    move v1, v2

    .line 5923
    goto :goto_0

    .line 5914
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5927
    :cond_5
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to add bot %d to game %d. No empty slots!"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lsoftware/simplicial/a/i;->k()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget v5, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/ao;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4769
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_3

    .line 4771
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v2, v0

    .line 4773
    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_0

    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v2, :cond_1

    .line 4769
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4776
    :goto_1
    iget v3, v4, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v3, :cond_0

    .line 4778
    iget-object v3, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v2

    .line 4780
    invoke-virtual {p1, v3}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v3

    .line 4787
    :goto_2
    return-object v0

    .line 4776
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4787
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private b(Lsoftware/simplicial/a/v;)Lsoftware/simplicial/a/bw;
    .locals 6

    .prologue
    .line 5890
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x1ff

    if-ge v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x3ff

    if-lt v0, v1, :cond_2

    .line 5892
    :cond_1
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v1, "Failed to add spectator %d to game %d. Game was full!"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5893
    const/4 v0, 0x0

    .line 5900
    :goto_0
    return-object v0

    .line 5896
    :cond_2
    new-instance v0, Lsoftware/simplicial/a/bw;

    iget-object v1, p1, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, p1, Lsoftware/simplicial/a/v;->b:I

    iget-object v3, p1, Lsoftware/simplicial/a/v;->k:[B

    iget-wide v4, p1, Lsoftware/simplicial/a/v;->d:J

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/a/bw;-><init>(Lsoftware/simplicial/a/f/bl;I[BJ)V

    .line 5897
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v1, :cond_3

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_4

    .line 5898
    :cond_3
    const/4 v1, -0x2

    iput v1, v0, Lsoftware/simplicial/a/bw;->f:I

    .line 5899
    :cond_4
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    iget-object v2, p1, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private b(I)Lsoftware/simplicial/a/f/bl;
    .locals 3

    .prologue
    .line 5466
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/bl;

    .line 5467
    iget v2, v0, Lsoftware/simplicial/a/f/bl;->a:I

    if-ne v2, p1, :cond_0

    .line 5470
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(F)V
    .locals 21

    .prologue
    .line 834
    const/4 v2, 0x0

    move v11, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    array-length v2, v2

    if-ge v11, v2, :cond_2a

    .line 836
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    aget-object v3, v2, v11

    .line 837
    iget-boolean v2, v3, Lsoftware/simplicial/a/i;->K:Z

    if-nez v2, :cond_1

    .line 834
    :cond_0
    :goto_1
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_0

    .line 840
    :cond_1
    iget v2, v3, Lsoftware/simplicial/a/i;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->d:I

    .line 842
    iget v2, v3, Lsoftware/simplicial/a/i;->f:I

    if-lez v2, :cond_2

    .line 843
    iget v2, v3, Lsoftware/simplicial/a/i;->f:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->f:I

    .line 845
    :cond_2
    iget v2, v3, Lsoftware/simplicial/a/i;->h:I

    if-lez v2, :cond_3

    .line 846
    iget v2, v3, Lsoftware/simplicial/a/i;->h:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->h:I

    .line 848
    :cond_3
    iget v2, v3, Lsoftware/simplicial/a/i;->i:I

    if-lez v2, :cond_4

    .line 849
    iget v2, v3, Lsoftware/simplicial/a/i;->i:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->i:I

    .line 851
    :cond_4
    iget v2, v3, Lsoftware/simplicial/a/i;->g:I

    if-lez v2, :cond_5

    .line 852
    iget v2, v3, Lsoftware/simplicial/a/i;->g:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->g:I

    .line 854
    :cond_5
    const/4 v2, 0x0

    iput v2, v3, Lsoftware/simplicial/a/i;->l:F

    .line 855
    const/4 v2, 0x0

    iput v2, v3, Lsoftware/simplicial/a/i;->m:F

    .line 856
    const/4 v4, 0x0

    .line 857
    const/4 v2, 0x0

    :goto_2
    iget v5, v3, Lsoftware/simplicial/a/i;->S:I

    if-ge v2, v5, :cond_7

    .line 859
    iget-object v5, v3, Lsoftware/simplicial/a/i;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v5, v2

    .line 860
    iget v6, v3, Lsoftware/simplicial/a/i;->l:F

    iget v7, v5, Lsoftware/simplicial/a/bh;->l:F

    add-float/2addr v6, v7

    iput v6, v3, Lsoftware/simplicial/a/i;->l:F

    .line 861
    iget v6, v3, Lsoftware/simplicial/a/i;->m:F

    iget v7, v5, Lsoftware/simplicial/a/bh;->m:F

    add-float/2addr v6, v7

    iput v6, v3, Lsoftware/simplicial/a/i;->m:F

    .line 862
    iget-byte v6, v5, Lsoftware/simplicial/a/bh;->K:B

    if-le v6, v4, :cond_6

    .line 863
    iget-byte v4, v5, Lsoftware/simplicial/a/bh;->K:B

    .line 857
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 865
    :cond_7
    iget v2, v3, Lsoftware/simplicial/a/i;->l:F

    iget v5, v3, Lsoftware/simplicial/a/i;->S:I

    int-to-float v5, v5

    div-float/2addr v2, v5

    iput v2, v3, Lsoftware/simplicial/a/i;->l:F

    .line 866
    iget v2, v3, Lsoftware/simplicial/a/i;->m:F

    iget v5, v3, Lsoftware/simplicial/a/i;->S:I

    int-to-float v5, v5

    div-float/2addr v2, v5

    iput v2, v3, Lsoftware/simplicial/a/i;->m:F

    .line 868
    if-lez v4, :cond_8

    .line 870
    const/4 v2, 0x0

    .line 871
    sget-object v5, Lsoftware/simplicial/a/bs$2;->b:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    invoke-virtual {v6}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 883
    :goto_3
    if-gt v4, v2, :cond_0

    .line 887
    :cond_8
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v12

    .line 888
    invoke-virtual {v12}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    .line 890
    const/high16 v2, 0x42180000    # 38.0f

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZ)Lsoftware/simplicial/a/g;

    move-result-object v6

    .line 891
    const v2, 0x40163d71    # 2.3475f

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZ)Lsoftware/simplicial/a/g;

    move-result-object v5

    .line 892
    const/high16 v2, 0x43460000    # 198.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/i;F)Lsoftware/simplicial/a/ag;

    move-result-object v7

    .line 893
    const/high16 v2, 0x43460000    # 198.0f

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/i;FZ)Lsoftware/simplicial/a/z;

    move-result-object v8

    .line 894
    const/high16 v2, -0x40000000    # -2.0f

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/i;FZ)Lsoftware/simplicial/a/z;

    move-result-object v4

    .line 895
    const/4 v2, 0x1

    new-array v9, v2, [Lsoftware/simplicial/a/bf;

    .line 896
    const/high16 v2, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;F[Ljava/lang/Object;)Lsoftware/simplicial/a/bh;

    move-result-object v2

    .line 897
    const/4 v13, 0x0

    aget-object v13, v9, v13

    .line 899
    const/4 v9, 0x0

    .line 901
    if-eqz v2, :cond_c

    const v14, 0x3f866666    # 1.05f

    mul-float/2addr v14, v10

    const/high16 v15, 0x3f400000    # 0.75f

    add-float/2addr v14, v15

    invoke-virtual {v2}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v15

    cmpg-float v14, v14, v15

    if-gez v14, :cond_c

    .line 903
    const v4, 0x3f4851eb    # 0.78249997f

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZ)Lsoftware/simplicial/a/g;

    move-result-object v4

    .line 904
    if-eqz v4, :cond_b

    iget v5, v4, Lsoftware/simplicial/a/g;->n:F

    cmpl-float v5, v10, v5

    if-lez v5, :cond_b

    .line 906
    sget-object v5, Lsoftware/simplicial/a/i$a;->d:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    .line 907
    iput-object v4, v3, Lsoftware/simplicial/a/i;->k:Lsoftware/simplicial/a/g;

    .line 955
    :goto_4
    iget-object v4, v3, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    sget-object v5, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    if-ne v4, v5, :cond_9

    iget v4, v3, Lsoftware/simplicial/a/i;->d:I

    iget v5, v3, Lsoftware/simplicial/a/i;->e:I

    if-lt v4, v5, :cond_9

    .line 956
    sget-object v4, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    .line 958
    :cond_9
    sget-object v4, Lsoftware/simplicial/a/bs$2;->c:[I

    iget-object v5, v3, Lsoftware/simplicial/a/i;->j:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v5}, Lsoftware/simplicial/a/i$a;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1150
    :cond_a
    :goto_5
    iget-byte v2, v12, Lsoftware/simplicial/a/bh;->E:B

    if-ltz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    sget-object v4, Lsoftware/simplicial/a/ac;->c:Lsoftware/simplicial/a/ac;

    if-ne v2, v4, :cond_0

    .line 1151
    iget v2, v3, Lsoftware/simplicial/a/i;->M:F

    neg-float v2, v2

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_1

    .line 874
    :pswitch_0
    const/16 v2, 0x14

    .line 875
    goto/16 :goto_3

    .line 877
    :pswitch_1
    const/16 v2, 0x28

    .line 878
    goto/16 :goto_3

    .line 880
    :pswitch_2
    const/16 v2, 0x32

    goto/16 :goto_3

    .line 910
    :cond_b
    sget-object v4, Lsoftware/simplicial/a/i$a;->c:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    goto :goto_4

    .line 913
    :cond_c
    if-eqz v4, :cond_d

    .line 915
    sget-object v2, Lsoftware/simplicial/a/i$a;->c:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    move-object v2, v4

    .line 916
    goto :goto_4

    .line 918
    :cond_d
    if-eqz v5, :cond_f

    iget v4, v5, Lsoftware/simplicial/a/g;->n:F

    cmpl-float v4, v10, v4

    if-lez v4, :cond_f

    .line 920
    const v2, 0x3f4851eb    # 0.78249997f

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZ)Lsoftware/simplicial/a/g;

    move-result-object v2

    .line 921
    if-eqz v2, :cond_e

    .line 923
    sget-object v4, Lsoftware/simplicial/a/i$a;->d:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    .line 924
    iput-object v2, v3, Lsoftware/simplicial/a/i;->k:Lsoftware/simplicial/a/g;

    :goto_6
    move-object v2, v5

    .line 929
    goto :goto_4

    .line 927
    :cond_e
    sget-object v2, Lsoftware/simplicial/a/i$a;->c:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    goto :goto_6

    .line 930
    :cond_f
    if-eqz v6, :cond_10

    iget v4, v6, Lsoftware/simplicial/a/g;->n:F

    cmpg-float v4, v10, v4

    if-gez v4, :cond_10

    iget v4, v3, Lsoftware/simplicial/a/i;->g:I

    if-gtz v4, :cond_10

    .line 932
    sget-object v2, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    move-object v2, v6

    .line 933
    goto/16 :goto_4

    .line 935
    :cond_10
    if-eqz v13, :cond_11

    invoke-virtual {v13}, Lsoftware/simplicial/a/bf;->p()F

    move-result v4

    const v5, 0x3f866666    # 1.05f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f400000    # 0.75f

    add-float/2addr v4, v5

    cmpl-float v4, v10, v4

    if-lez v4, :cond_11

    iget v4, v3, Lsoftware/simplicial/a/i;->g:I

    if-gtz v4, :cond_11

    .line 937
    sget-object v4, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    goto/16 :goto_4

    .line 940
    :cond_11
    if-eqz v7, :cond_12

    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    if-nez v2, :cond_12

    iget v2, v7, Lsoftware/simplicial/a/ag;->n:F

    cmpl-float v2, v10, v2

    if-lez v2, :cond_12

    iget v2, v3, Lsoftware/simplicial/a/i;->g:I

    if-gtz v2, :cond_12

    .line 942
    sget-object v2, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    move-object v2, v7

    .line 943
    goto/16 :goto_4

    .line 945
    :cond_12
    if-eqz v8, :cond_13

    iget v2, v3, Lsoftware/simplicial/a/i;->g:I

    if-gtz v2, :cond_13

    iget v2, v8, Lsoftware/simplicial/a/z;->n:F

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v2, v4

    cmpl-float v2, v10, v2

    if-ltz v2, :cond_13

    .line 947
    sget-object v2, Lsoftware/simplicial/a/i$a;->b:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    move-object v2, v8

    .line 948
    goto/16 :goto_4

    .line 952
    :cond_13
    sget-object v2, Lsoftware/simplicial/a/i$a;->a:Lsoftware/simplicial/a/i$a;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/i;->a(Lsoftware/simplicial/a/i$a;)V

    move-object v2, v9

    goto/16 :goto_4

    .line 961
    :pswitch_3
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v3, Lsoftware/simplicial/a/i;->O:F

    .line 962
    if-eqz v2, :cond_a

    .line 964
    const/4 v4, 0x0

    .line 965
    if-eqz v13, :cond_14

    .line 966
    invoke-virtual {v13}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    .line 968
    :cond_14
    iget v5, v2, Lsoftware/simplicial/a/ao;->m:F

    iget v6, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v5, v6

    float-to-double v6, v5

    iget v2, v2, Lsoftware/simplicial/a/ao;->l:F

    iget v5, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v2, v5

    float-to-double v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v2, v6

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    .line 969
    if-eqz v4, :cond_a

    if-eqz v13, :cond_a

    .line 971
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    sget-object v5, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    if-eq v2, v5, :cond_16

    iget v2, v3, Lsoftware/simplicial/a/i;->h:I

    if-nez v2, :cond_16

    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->n()Z

    move-result v2

    if-eqz v2, :cond_16

    iget v2, v3, Lsoftware/simplicial/a/i;->S:I

    const/4 v5, 0x1

    if-eq v2, v5, :cond_15

    instance-of v2, v13, Lsoftware/simplicial/a/i;

    if-nez v2, :cond_16

    .line 973
    :cond_15
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v2

    const v5, 0x3f866666    # 1.05f

    mul-float/2addr v2, v5

    const/high16 v5, 0x3f400000    # 0.75f

    add-float/2addr v2, v5

    .line 974
    mul-float/2addr v2, v2

    .line 975
    invoke-virtual {v12}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    invoke-virtual {v12}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    mul-float/2addr v5, v6

    .line 976
    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v2

    cmpl-float v6, v5, v6

    if-lez v6, :cond_16

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v2, v6

    cmpg-float v2, v5, v2

    if-gez v2, :cond_16

    .line 978
    iget v2, v4, Lsoftware/simplicial/a/bh;->m:F

    iget v5, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v2, v5

    float-to-double v6, v2

    iget v2, v4, Lsoftware/simplicial/a/bh;->l:F

    iget v4, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v2, v4

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    .line 979
    iget v2, v3, Lsoftware/simplicial/a/i;->M:F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v3, v2, v4, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZF)V

    .line 983
    :cond_16
    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    instance-of v2, v2, Lsoftware/simplicial/a/bl;

    if-eqz v2, :cond_a

    .line 985
    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    check-cast v2, Lsoftware/simplicial/a/bl;

    iget-object v2, v2, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    .line 986
    sget-object v4, Lsoftware/simplicial/a/bl$b;->h:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_a

    sget-object v4, Lsoftware/simplicial/a/bl$b;->i:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_a

    .line 987
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    iget v5, v3, Lsoftware/simplicial/a/i;->M:F

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move/from16 v9, p1

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    goto/16 :goto_5

    .line 993
    :pswitch_4
    const/high16 v2, 0x3f000000    # 0.5f

    iput v2, v3, Lsoftware/simplicial/a/i;->O:F

    .line 994
    iget v2, v3, Lsoftware/simplicial/a/i;->i:I

    if-nez v2, :cond_a

    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->o()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v3, Lsoftware/simplicial/a/i;->k:Lsoftware/simplicial/a/g;

    if-eqz v2, :cond_a

    .line 996
    iget-object v2, v3, Lsoftware/simplicial/a/i;->k:Lsoftware/simplicial/a/g;

    iget v2, v2, Lsoftware/simplicial/a/g;->m:F

    iget v4, v12, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v2, v4

    float-to-double v4, v2

    iget-object v2, v3, Lsoftware/simplicial/a/i;->k:Lsoftware/simplicial/a/g;

    iget v2, v2, Lsoftware/simplicial/a/g;->l:F

    iget v6, v12, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v2, v6

    float-to-double v6, v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v2, v4

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    .line 997
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    iget v5, v3, Lsoftware/simplicial/a/i;->M:F

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move/from16 v9, p1

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 998
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->r_()V

    goto/16 :goto_5

    .line 1002
    :pswitch_5
    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    if-eqz v2, :cond_17

    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    instance-of v2, v2, Lsoftware/simplicial/a/bl;

    if-eqz v2, :cond_19

    .line 1004
    :cond_17
    const v2, 0x3f666666    # 0.9f

    iput v2, v3, Lsoftware/simplicial/a/i;->O:F

    .line 1005
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    cmpg-float v2, v2, p1

    if-gez v2, :cond_18

    .line 1007
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v2, v2, 0x1

    iput v2, v3, Lsoftware/simplicial/a/i;->c:I

    .line 1009
    :cond_18
    iget v2, v3, Lsoftware/simplicial/a/i;->M:F

    iget v4, v3, Lsoftware/simplicial/a/i;->c:I

    int-to-float v4, v4

    mul-float v4, v4, p1

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_5

    .line 1013
    :cond_19
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v3, Lsoftware/simplicial/a/i;->O:F

    .line 1014
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v2, v4, :cond_1a

    .line 1016
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    iget-object v4, v3, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    rsub-int/lit8 v4, v4, 0x1

    aget-object v2, v2, v4

    .line 1017
    iget v4, v2, Lsoftware/simplicial/a/bx;->m:F

    iget v5, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    iget v6, v2, Lsoftware/simplicial/a/bx;->l:F

    iget v7, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1018
    iget v4, v3, Lsoftware/simplicial/a/i;->i:I

    if-nez v4, :cond_a

    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->o()Z

    move-result v4

    if-eqz v4, :cond_a

    iget v4, v3, Lsoftware/simplicial/a/i;->l:F

    iget v5, v3, Lsoftware/simplicial/a/i;->m:F

    iget v6, v2, Lsoftware/simplicial/a/bx;->l:F

    iget v2, v2, Lsoftware/simplicial/a/bx;->m:F

    invoke-static {v4, v5, v6, v2}, Lsoftware/simplicial/a/bs;->b(FFFF)F

    move-result v2

    const v4, 0x45a4b615

    cmpg-float v2, v2, v4

    if-gez v2, :cond_a

    .line 1020
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    iget v5, v3, Lsoftware/simplicial/a/i;->M:F

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move/from16 v9, p1

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 1021
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->r_()V

    goto/16 :goto_5

    .line 1025
    :cond_1a
    iget-object v2, v3, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->m:F

    iget v4, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v2, v4

    float-to-double v4, v2

    iget-object v2, v3, Lsoftware/simplicial/a/i;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->l:F

    iget v6, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v2, v6

    float-to-double v6, v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v2, v4

    iput v2, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_5

    .line 1029
    :pswitch_6
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v3, Lsoftware/simplicial/a/i;->O:F

    .line 1030
    if-eqz v2, :cond_a

    .line 1032
    iget v4, v2, Lsoftware/simplicial/a/ao;->m:F

    iget v5, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    iget v6, v2, Lsoftware/simplicial/a/ao;->l:F

    iget v7, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1034
    iget v4, v12, Lsoftware/simplicial/a/bh;->n:F

    const/high16 v5, 0x40800000    # 4.0f

    mul-float v9, v4, v5

    .line 1036
    const/4 v4, 0x0

    .line 1037
    const/4 v6, 0x0

    .line 1038
    const/high16 v5, -0x800000    # Float.NEGATIVE_INFINITY

    .line 1039
    iget v7, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v7, v9

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_2c

    .line 1041
    const v4, 0x3f933333    # 1.15f

    const v7, 0x3fc90fdb

    iget v8, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float v8, v9, v8

    mul-float/2addr v7, v8

    div-float/2addr v7, v9

    mul-float/2addr v4, v7

    .line 1042
    iget v7, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v7

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v4

    move-wide/from16 v18, v0

    add-double v16, v16, v18

    cmpg-double v7, v14, v16

    if-gez v7, :cond_1b

    iget v7, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v7

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v4

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    cmpl-double v7, v14, v16

    if-lez v7, :cond_1b

    .line 1044
    iget v7, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v7

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    cmpg-double v7, v14, v16

    if-gez v7, :cond_26

    .line 1045
    const-wide v14, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v4

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v7, v14

    iput v7, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1049
    :cond_1b
    :goto_7
    const/4 v7, 0x1

    .line 1050
    cmpl-float v8, v4, v5

    if-lez v8, :cond_2b

    .line 1052
    const/4 v5, 0x0

    move v6, v7

    .line 1056
    :goto_8
    iget v7, v3, Lsoftware/simplicial/a/i;->l:F

    add-float/2addr v7, v9

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1e

    .line 1058
    const v7, 0x3f933333    # 1.15f

    const v8, 0x3fc90fdb

    move-object/from16 v0, p0

    iget v10, v0, Lsoftware/simplicial/a/bs;->aE:F

    iget v13, v3, Lsoftware/simplicial/a/i;->l:F

    sub-float/2addr v10, v13

    sub-float v10, v9, v10

    mul-float/2addr v8, v10

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    .line 1059
    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1060
    float-to-double v14, v8

    const-wide v16, 0x400921fb54442d18L    # Math.PI

    cmpl-double v10, v14, v16

    if-lez v10, :cond_1c

    .line 1061
    float-to-double v14, v8

    const-wide v16, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v14, v14, v16

    double-to-float v8, v14

    .line 1062
    :cond_1c
    const/4 v10, 0x0

    add-float/2addr v10, v7

    cmpg-float v10, v8, v10

    if-gez v10, :cond_1d

    const/4 v10, 0x0

    sub-float/2addr v10, v7

    cmpl-float v10, v8, v10

    if-lez v10, :cond_1d

    .line 1064
    const/4 v10, 0x0

    cmpg-float v8, v8, v10

    if-gez v8, :cond_27

    .line 1065
    const/4 v8, 0x0

    sub-float/2addr v8, v7

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1069
    :cond_1d
    :goto_9
    add-int/lit8 v6, v6, 0x1

    .line 1070
    cmpl-float v8, v7, v4

    if-lez v8, :cond_1e

    .line 1072
    const/4 v5, 0x1

    move v4, v7

    .line 1076
    :cond_1e
    iget v7, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v7, v9

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-gez v7, :cond_20

    .line 1078
    const v7, 0x3f933333    # 1.15f

    const v8, 0x3fc90fdb

    iget v10, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float v10, v9, v10

    mul-float/2addr v8, v10

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    .line 1079
    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v8

    const-wide v16, 0x4012d97c7f3321d2L    # 4.71238898038469

    float-to-double v0, v7

    move-wide/from16 v18, v0

    add-double v16, v16, v18

    cmpg-double v8, v14, v16

    if-gez v8, :cond_1f

    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v8

    const-wide v16, 0x4012d97c7f3321d2L    # 4.71238898038469

    float-to-double v0, v7

    move-wide/from16 v18, v0

    sub-double v16, v16, v18

    cmpl-double v8, v14, v16

    if-lez v8, :cond_1f

    .line 1081
    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v14, v8

    const-wide v16, 0x4012d97c7f3321d2L    # 4.71238898038469

    cmpg-double v8, v14, v16

    if-gez v8, :cond_28

    .line 1082
    const-wide v14, 0x4012d97c7f3321d2L    # 4.71238898038469

    float-to-double v0, v7

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    double-to-float v8, v14

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1086
    :cond_1f
    :goto_a
    add-int/lit8 v6, v6, 0x1

    .line 1087
    cmpl-float v8, v7, v4

    if-lez v8, :cond_20

    .line 1089
    const/4 v5, 0x2

    move v4, v7

    .line 1093
    :cond_20
    iget v7, v3, Lsoftware/simplicial/a/i;->m:F

    add-float/2addr v7, v9

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_22

    .line 1095
    const v7, 0x3f933333    # 1.15f

    const v8, 0x3fc90fdb

    move-object/from16 v0, p0

    iget v10, v0, Lsoftware/simplicial/a/bs;->aE:F

    iget v13, v3, Lsoftware/simplicial/a/i;->m:F

    sub-float/2addr v10, v13

    sub-float v10, v9, v10

    mul-float/2addr v8, v10

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    .line 1096
    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v8, v8

    const-wide v14, 0x3ff921fb54442d18L    # 1.5707963267948966

    float-to-double v0, v7

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    cmpg-double v8, v8, v14

    if-gez v8, :cond_21

    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v8, v8

    const-wide v14, 0x3ff921fb54442d18L    # 1.5707963267948966

    float-to-double v0, v7

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    cmpl-double v8, v8, v14

    if-lez v8, :cond_21

    .line 1098
    iget v8, v3, Lsoftware/simplicial/a/i;->M:F

    float-to-double v8, v8

    const-wide v14, 0x3ff921fb54442d18L    # 1.5707963267948966

    cmpg-double v8, v8, v14

    if-gez v8, :cond_29

    .line 1099
    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    float-to-double v14, v7

    sub-double/2addr v8, v14

    double-to-float v8, v8

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1103
    :cond_21
    :goto_b
    add-int/lit8 v6, v6, 0x1

    .line 1104
    cmpl-float v4, v7, v4

    if-lez v4, :cond_22

    .line 1106
    const/4 v5, 0x3

    .line 1110
    :cond_22
    iget v4, v2, Lsoftware/simplicial/a/ao;->n:F

    invoke-virtual {v12}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    add-float/2addr v4, v7

    .line 1111
    const/4 v7, 0x2

    if-lt v6, v7, :cond_23

    .line 1113
    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 1114
    packed-switch v5, :pswitch_data_2

    .line 1130
    :cond_23
    :goto_c
    instance-of v5, v2, Lsoftware/simplicial/a/g;

    if-nez v5, :cond_a

    .line 1132
    invoke-virtual {v2}, Lsoftware/simplicial/a/ao;->h()F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_24

    .line 1134
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    sget-object v4, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    if-eq v2, v4, :cond_24

    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->n()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1135
    iget v2, v3, Lsoftware/simplicial/a/i;->M:F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v3, v2, v4, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZF)V

    .line 1137
    :cond_24
    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    instance-of v2, v2, Lsoftware/simplicial/a/bl;

    if-eqz v2, :cond_a

    .line 1139
    iget-object v2, v3, Lsoftware/simplicial/a/i;->aG:Lsoftware/simplicial/a/ag;

    check-cast v2, Lsoftware/simplicial/a/bl;

    iget-object v2, v2, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    .line 1140
    iget v5, v3, Lsoftware/simplicial/a/i;->M:F

    .line 1141
    sget-object v4, Lsoftware/simplicial/a/bl$b;->h:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_25

    sget-object v4, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_25

    .line 1142
    float-to-double v4, v5

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v4, v6

    double-to-float v5, v4

    .line 1143
    :cond_25
    invoke-virtual {v3}, Lsoftware/simplicial/a/i;->q()Lsoftware/simplicial/a/bh;

    move-result-object v4

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    move/from16 v9, p1

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    goto/16 :goto_5

    .line 1047
    :cond_26
    const-wide v14, 0x400921fb54442d18L    # Math.PI

    float-to-double v0, v4

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    double-to-float v7, v14

    iput v7, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_7

    .line 1067
    :cond_27
    const/4 v8, 0x0

    add-float/2addr v8, v7

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_9

    .line 1084
    :cond_28
    const-wide v14, 0x4012d97c7f3321d2L    # 4.71238898038469

    float-to-double v0, v7

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    double-to-float v8, v14

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_a

    .line 1101
    :cond_29
    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    float-to-double v14, v7

    add-double/2addr v8, v14

    double-to-float v8, v8

    iput v8, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_b

    .line 1117
    :pswitch_7
    const/4 v5, 0x0

    iput v5, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_c

    .line 1120
    :pswitch_8
    const v5, 0x40490fdb    # (float)Math.PI

    iput v5, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_c

    .line 1123
    :pswitch_9
    const v5, 0x3fc90fdb

    iput v5, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_c

    .line 1126
    :pswitch_a
    const v5, 0x4096cbe4

    iput v5, v3, Lsoftware/simplicial/a/i;->M:F

    goto/16 :goto_c

    .line 1153
    :cond_2a
    return-void

    :cond_2b
    move v4, v5

    move v5, v6

    move v6, v7

    goto/16 :goto_8

    :cond_2c
    move/from16 v20, v5

    move v5, v6

    move v6, v4

    move/from16 v4, v20

    goto/16 :goto_8

    .line 871
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 958
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 1114
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private b(Lsoftware/simplicial/a/bf;)V
    .locals 24

    .prologue
    .line 3429
    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v0, v19

    if-ge v0, v2, :cond_3d

    .line 3431
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v20, v2, v19

    .line 3433
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, v20

    instance-of v2, v0, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_1

    .line 3429
    :cond_0
    add-int/lit8 v2, v19, 0x1

    move/from16 v19, v2

    goto :goto_0

    .line 3437
    :cond_1
    move-object/from16 v0, v20

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->aJ:B

    const/4 v3, -0x2

    if-ne v2, v3, :cond_4

    if-eqz p1, :cond_4

    move-object/from16 v3, p1

    .line 3441
    :goto_1
    const v2, 0x3ff33333    # 1.9f

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lsoftware/simplicial/a/bf;->a(FZ)F

    move-result v4

    mul-float/2addr v2, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    .line 3443
    iget v4, v3, Lsoftware/simplicial/a/bf;->l:F

    sub-float v6, v4, v2

    .line 3444
    iget v4, v3, Lsoftware/simplicial/a/bf;->l:F

    add-float v5, v4, v2

    .line 3445
    iget v4, v3, Lsoftware/simplicial/a/bf;->m:F

    sub-float/2addr v4, v2

    .line 3446
    iget v7, v3, Lsoftware/simplicial/a/bf;->m:F

    add-float/2addr v2, v7

    .line 3449
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v7, v8, :cond_3f

    :cond_2
    move-object/from16 v0, p0

    iget-short v7, v0, Lsoftware/simplicial/a/bs;->aH:S

    if-gtz v7, :cond_3f

    .line 3454
    const/high16 v6, -0x800000    # Float.NEGATIVE_INFINITY

    .line 3455
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 3456
    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    .line 3457
    const/high16 v2, 0x7f800000    # Float.POSITIVE_INFINITY

    move v12, v5

    move v13, v6

    move v6, v4

    move v4, v2

    .line 3460
    :goto_2
    new-instance v8, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3461
    new-instance v7, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3462
    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v5, v5

    if-ge v2, v5, :cond_6

    .line 3464
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v5, v5, v2

    .line 3466
    iget-boolean v9, v5, Lsoftware/simplicial/a/bq;->G:Z

    if-eqz v9, :cond_5

    .line 3468
    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3462
    :cond_3
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 3439
    :cond_4
    move-object/from16 v0, v20

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->aJ:B

    if-ltz v2, :cond_40

    move-object/from16 v0, v20

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->aJ:B

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v3, :cond_40

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v20

    iget-byte v3, v0, Lsoftware/simplicial/a/bf;->aJ:B

    aget-object v2, v2, v3

    iget-boolean v2, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v2, :cond_40

    .line 3440
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v20

    iget-byte v3, v0, Lsoftware/simplicial/a/bf;->aJ:B

    aget-object v2, v2, v3

    move-object v3, v2

    goto/16 :goto_1

    .line 3472
    :cond_5
    iget-boolean v9, v5, Lsoftware/simplicial/a/bq;->F:Z

    if-nez v9, :cond_3

    iget-object v9, v5, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v10, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-eq v9, v10, :cond_3

    .line 3475
    iget v9, v5, Lsoftware/simplicial/a/bq;->l:F

    iget v10, v5, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v9, v10

    cmpg-float v9, v9, v13

    if-ltz v9, :cond_3

    .line 3477
    iget v9, v5, Lsoftware/simplicial/a/bq;->l:F

    iget v10, v5, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v9, v10

    cmpl-float v9, v9, v12

    if-gtz v9, :cond_3

    .line 3479
    iget v9, v5, Lsoftware/simplicial/a/bq;->m:F

    iget v10, v5, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v9, v10

    cmpg-float v9, v9, v6

    if-ltz v9, :cond_3

    .line 3481
    iget v9, v5, Lsoftware/simplicial/a/bq;->m:F

    iget v10, v5, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v9, v10

    cmpl-float v9, v9, v4

    if-gtz v9, :cond_3

    .line 3484
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 3488
    :cond_6
    new-instance v9, Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3491
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-object v2, v2, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v5, :cond_a

    .line 3493
    :cond_7
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bp:Z

    if-eqz v2, :cond_8

    .line 3495
    new-instance v2, Lsoftware/simplicial/a/a/ak;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v5, v5, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v5, :cond_13

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v10, v5, Lsoftware/simplicial/a/az;->b:J

    :goto_5
    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bf;->an:I

    int-to-byte v5, v5

    move-object/from16 v0, v20

    iget-wide v14, v0, Lsoftware/simplicial/a/bf;->ap:J

    .line 3496
    invoke-static {v14, v15}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v14

    invoke-direct {v2, v10, v11, v5, v14}, Lsoftware/simplicial/a/a/ak;-><init>(JBI)V

    .line 3495
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3497
    const/4 v2, 0x0

    move-object/from16 v0, v20

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->bp:Z

    .line 3499
    :cond_8
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bq:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    sget-object v5, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-eq v2, v5, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-boolean v2, v2, Lsoftware/simplicial/a/b/a;->c:Z

    if-nez v2, :cond_a

    .line 3501
    :cond_9
    new-instance v2, Lsoftware/simplicial/a/a/n;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget-byte v5, v5, Lsoftware/simplicial/a/aa;->r:B

    move-object/from16 v0, v20

    iget-boolean v10, v0, Lsoftware/simplicial/a/bf;->az:Z

    invoke-direct {v2, v5, v10}, Lsoftware/simplicial/a/a/n;-><init>(IZ)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3502
    const/4 v2, 0x0

    move-object/from16 v0, v20

    iput-boolean v2, v0, Lsoftware/simplicial/a/bf;->bq:Z

    .line 3505
    :cond_a
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v2, v2, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v2, :cond_15

    .line 3507
    move-object/from16 v0, v20

    iget v2, v0, Lsoftware/simplicial/a/bf;->bo:I

    if-lez v2, :cond_b

    .line 3508
    new-instance v2, Lsoftware/simplicial/a/a/aj;

    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bf;->bo:I

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/aj;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3509
    :cond_b
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bu:Z

    if-eqz v2, :cond_c

    .line 3510
    new-instance v2, Lsoftware/simplicial/a/a/aa;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v10, v5, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v10, v11}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/aa;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3511
    :cond_c
    move-object/from16 v0, v20

    iget v2, v0, Lsoftware/simplicial/a/bf;->bv:I

    if-lez v2, :cond_d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/a/bs;->aT:Z

    if-nez v2, :cond_d

    .line 3512
    new-instance v2, Lsoftware/simplicial/a/a/b;

    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bf;->bv:I

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/b;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3513
    :cond_d
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bw:Z

    if-eqz v2, :cond_e

    .line 3514
    new-instance v2, Lsoftware/simplicial/a/a/k;

    const/16 v5, 0xa

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/k;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3515
    :cond_e
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bx:Z

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v2, v2, Lsoftware/simplicial/a/i/a;->h:I

    if-lez v2, :cond_f

    .line 3516
    new-instance v2, Lsoftware/simplicial/a/a/ab;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v5, v5, Lsoftware/simplicial/a/i/a;->h:I

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/ab;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3517
    :cond_f
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->by:Z

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v2, v2, Lsoftware/simplicial/a/i/a;->i:I

    if-lez v2, :cond_10

    .line 3518
    new-instance v2, Lsoftware/simplicial/a/a/ab;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v5, v5, Lsoftware/simplicial/a/i/a;->i:I

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/ab;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3519
    :cond_10
    move-object/from16 v0, v20

    iget v2, v0, Lsoftware/simplicial/a/bf;->bA:I

    if-lez v2, :cond_11

    .line 3520
    new-instance v2, Lsoftware/simplicial/a/a/ab;

    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bf;->bA:I

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/ab;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3521
    :cond_11
    move-object/from16 v0, v20

    iget v2, v0, Lsoftware/simplicial/a/bf;->br:I

    if-lez v2, :cond_12

    .line 3522
    new-instance v2, Lsoftware/simplicial/a/a/m;

    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bf;->br:I

    int-to-short v5, v5

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/m;-><init>(S)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3523
    :cond_12
    move-object/from16 v0, v20

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/d;

    .line 3524
    new-instance v10, Lsoftware/simplicial/a/a/a;

    iget-short v2, v2, Lsoftware/simplicial/a/d;->bw:S

    invoke-direct {v10, v2}, Lsoftware/simplicial/a/a/a;-><init>(S)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 3495
    :cond_13
    const-wide/16 v10, -0x1

    goto/16 :goto_5

    .line 3525
    :cond_14
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->bt:Z

    if-eqz v2, :cond_15

    .line 3526
    new-instance v2, Lsoftware/simplicial/a/a/l;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    iget-byte v5, v5, Lsoftware/simplicial/a/aa;->r:B

    invoke-direct {v2, v5}, Lsoftware/simplicial/a/a/l;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3530
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v5, :cond_17

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v2, v2, 0x3

    const/4 v5, 0x2

    if-ge v2, v5, :cond_17

    iget-boolean v2, v3, Lsoftware/simplicial/a/bf;->aj:Z

    if-nez v2, :cond_17

    iget v2, v3, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v5

    if-ltz v2, :cond_17

    iget v2, v3, Lsoftware/simplicial/a/bf;->S:I

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-short v2, v0, Lsoftware/simplicial/a/bs;->aH:S

    if-gtz v2, :cond_17

    .line 3533
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->m:Lsoftware/simplicial/a/f/al;

    move-object/from16 v0, v20

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->aH:Lsoftware/simplicial/a/f/bv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move/from16 v17, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lsoftware/simplicial/a/f/bl;->d:S

    move/from16 v18, v0

    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;Ljava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/bf;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;FLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Collection;IS)Ljava/util/List;

    move-result-object v2

    .line 3535
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 3536
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v4, v2, v5}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_7

    .line 3540
    :cond_17
    const/4 v10, 0x0

    .line 3541
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->am:I

    if-lez v2, :cond_18

    .line 3544
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->am:I

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3545
    const/4 v2, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v2, v5, :cond_18

    .line 3547
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v5, v5, v2

    .line 3549
    invoke-interface {v10, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3545
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 3553
    :cond_18
    const/4 v15, 0x0

    .line 3554
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-lez v2, :cond_19

    .line 3557
    new-instance v15, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->ap:I

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3559
    const/4 v2, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v2, v5, :cond_19

    .line 3561
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v5, v5, v2

    .line 3563
    invoke-interface {v15, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3559
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 3567
    :cond_19
    const/4 v11, 0x0

    .line 3568
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-lez v2, :cond_1a

    .line 3571
    new-instance v11, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aw:I

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3572
    const/4 v2, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v2, v5, :cond_1b

    .line 3574
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v5, v5, v2

    .line 3576
    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3572
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 3579
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v2, v5, :cond_1b

    .line 3581
    new-instance v11, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3582
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-interface {v11, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3586
    :cond_1b
    const/4 v14, 0x0

    .line 3587
    const/4 v5, 0x0

    .line 3588
    iget v2, v3, Lsoftware/simplicial/a/bf;->aZ:I

    if-lez v2, :cond_1c

    .line 3589
    new-instance v2, Lsoftware/simplicial/a/a/s;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/s;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3590
    :cond_1c
    iget v2, v3, Lsoftware/simplicial/a/bf;->ba:I

    if-lez v2, :cond_1d

    .line 3591
    new-instance v2, Lsoftware/simplicial/a/a/v;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->aD:Lsoftware/simplicial/a/bv;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v16

    iget v0, v3, Lsoftware/simplicial/a/bf;->ba:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v2, v0, v1}, Lsoftware/simplicial/a/a/v;-><init>(II)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3592
    :cond_1d
    iget v2, v3, Lsoftware/simplicial/a/bf;->bb:I

    if-lez v2, :cond_1e

    .line 3593
    new-instance v2, Lsoftware/simplicial/a/a/v;

    sget-object v16, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v16

    iget v0, v3, Lsoftware/simplicial/a/bf;->bb:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v2, v0, v1}, Lsoftware/simplicial/a/a/v;-><init>(II)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3594
    :cond_1e
    iget v2, v3, Lsoftware/simplicial/a/bf;->bd:I

    if-lez v2, :cond_1f

    .line 3595
    new-instance v2, Lsoftware/simplicial/a/a/r;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/r;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3596
    :cond_1f
    iget v2, v3, Lsoftware/simplicial/a/bf;->be:I

    if-lez v2, :cond_20

    .line 3597
    new-instance v2, Lsoftware/simplicial/a/a/u;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/u;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3598
    :cond_20
    iget v2, v3, Lsoftware/simplicial/a/bf;->bf:I

    if-lez v2, :cond_21

    .line 3599
    new-instance v2, Lsoftware/simplicial/a/a/t;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/t;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3600
    :cond_21
    iget v2, v3, Lsoftware/simplicial/a/bf;->bz:I

    if-lez v2, :cond_22

    .line 3601
    new-instance v2, Lsoftware/simplicial/a/a/ag;

    iget v0, v3, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v2, v0}, Lsoftware/simplicial/a/a/ag;-><init>(I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3602
    :cond_22
    iget v2, v3, Lsoftware/simplicial/a/bf;->bc:I

    if-lez v2, :cond_23

    .line 3603
    new-instance v2, Lsoftware/simplicial/a/a/d;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/d;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3604
    :cond_23
    iget v2, v3, Lsoftware/simplicial/a/bf;->bl:I

    if-lez v2, :cond_24

    .line 3605
    new-instance v2, Lsoftware/simplicial/a/a/o;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/o;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3606
    :cond_24
    iget v2, v3, Lsoftware/simplicial/a/bf;->bm:I

    if-lez v2, :cond_25

    .line 3607
    new-instance v2, Lsoftware/simplicial/a/a/p;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/p;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3608
    :cond_25
    iget v2, v3, Lsoftware/simplicial/a/bf;->bn:I

    if-lez v2, :cond_26

    .line 3609
    new-instance v2, Lsoftware/simplicial/a/a/q;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/q;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3610
    :cond_26
    iget v2, v3, Lsoftware/simplicial/a/bf;->bg:I

    if-lez v2, :cond_27

    .line 3611
    new-instance v2, Lsoftware/simplicial/a/a/f;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/f;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3612
    :cond_27
    iget v2, v3, Lsoftware/simplicial/a/bf;->bh:I

    if-lez v2, :cond_28

    .line 3613
    new-instance v2, Lsoftware/simplicial/a/a/g;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/g;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3614
    :cond_28
    iget v2, v3, Lsoftware/simplicial/a/bf;->bi:I

    if-lez v2, :cond_29

    .line 3615
    new-instance v2, Lsoftware/simplicial/a/a/h;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/h;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3616
    :cond_29
    iget v2, v3, Lsoftware/simplicial/a/bf;->bk:I

    if-lez v2, :cond_2a

    .line 3617
    new-instance v2, Lsoftware/simplicial/a/a/i;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/i;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3618
    :cond_2a
    iget v2, v3, Lsoftware/simplicial/a/bf;->bj:I

    if-lez v2, :cond_2b

    .line 3619
    new-instance v2, Lsoftware/simplicial/a/a/j;

    invoke-direct {v2}, Lsoftware/simplicial/a/a/j;-><init>()V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3621
    :cond_2b
    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v2, v0, :cond_2c

    .line 3623
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v16, v0

    aget-object v16, v16, v2

    .line 3625
    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 3621
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 3628
    :cond_2c
    const/4 v2, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v2, v0, :cond_36

    .line 3630
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v16, v0

    aget-object v17, v16, v2

    .line 3632
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lsoftware/simplicial/a/bf;->aF:Z

    move/from16 v16, v0

    if-nez v16, :cond_2d

    .line 3628
    :goto_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 3635
    :cond_2d
    const/16 v16, 0x0

    move/from16 v23, v16

    move/from16 v16, v14

    move v14, v5

    move/from16 v5, v23

    :goto_e
    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->bF:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v5, v0, :cond_34

    .line 3637
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v18, v0

    aget-object v18, v18, v5

    .line 3638
    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v5, v0, :cond_2f

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lsoftware/simplicial/a/bh;->M:Z

    move/from16 v21, v0

    if-nez v21, :cond_2f

    .line 3635
    :cond_2e
    :goto_f
    add-int/lit8 v5, v5, 0x1

    goto :goto_e

    .line 3641
    :cond_2f
    move-object/from16 v0, v17

    if-ne v0, v3, :cond_31

    .line 3643
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3644
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lsoftware/simplicial/a/bh;->a(Ljava/util/List;)V

    .line 3645
    iget-boolean v0, v3, Lsoftware/simplicial/a/bf;->aX:Z

    move/from16 v18, v0

    if-eqz v18, :cond_30

    if-nez v16, :cond_30

    .line 3647
    new-instance v16, Lsoftware/simplicial/a/a/af;

    iget v0, v3, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v18, v0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/a/af;-><init>(I)V

    move-object/from16 v0, v16

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3648
    const/16 v16, 0x1

    .line 3650
    :cond_30
    iget-boolean v0, v3, Lsoftware/simplicial/a/bf;->aY:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2e

    if-nez v14, :cond_2e

    .line 3652
    new-instance v14, Lsoftware/simplicial/a/a/ac;

    iget v0, v3, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-direct {v14, v0}, Lsoftware/simplicial/a/a/ac;-><init>(I)V

    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3653
    const/4 v14, 0x1

    goto :goto_f

    .line 3659
    :cond_31
    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    add-float v21, v21, v22

    cmpg-float v21, v21, v13

    if-ltz v21, :cond_32

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    sub-float v21, v21, v22

    cmpl-float v21, v21, v12

    if-gtz v21, :cond_32

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    add-float v21, v21, v22

    cmpg-float v21, v21, v6

    if-ltz v21, :cond_32

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v21, v0

    .line 3660
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    sub-float v21, v21, v22

    cmpl-float v21, v21, v4

    if-lez v21, :cond_33

    .line 3662
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_2e

    .line 3663
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_f

    .line 3667
    :cond_33
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3668
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lsoftware/simplicial/a/bh;->a(Ljava/util/List;)V

    goto/16 :goto_f

    .line 3671
    :cond_34
    move-object/from16 v0, v17

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_35

    move-object/from16 v0, v17

    iget-boolean v5, v0, Lsoftware/simplicial/a/bf;->aX:Z

    if-eqz v5, :cond_35

    if-nez v16, :cond_35

    .line 3673
    new-instance v5, Lsoftware/simplicial/a/a/af;

    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v5, v0}, Lsoftware/simplicial/a/a/af;-><init>(I)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3674
    const/16 v16, 0x1

    .line 3676
    :cond_35
    move-object/from16 v0, v17

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3e

    move-object/from16 v0, v17

    iget-boolean v5, v0, Lsoftware/simplicial/a/bf;->aY:Z

    if-eqz v5, :cond_3e

    if-nez v14, :cond_3e

    .line 3678
    new-instance v5, Lsoftware/simplicial/a/a/ac;

    move-object/from16 v0, v17

    iget v14, v0, Lsoftware/simplicial/a/bf;->C:I

    invoke-direct {v5, v14}, Lsoftware/simplicial/a/a/ac;-><init>(I)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3679
    const/4 v14, 0x1

    move v5, v14

    move/from16 v14, v16

    goto/16 :goto_d

    .line 3683
    :cond_36
    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->az:I

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3684
    const/4 v2, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v3, :cond_39

    .line 3686
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v3, v3, v2

    .line 3688
    iget-boolean v14, v3, Lsoftware/simplicial/a/g;->f:Z

    if-eqz v14, :cond_38

    .line 3690
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3684
    :cond_37
    :goto_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    .line 3694
    :cond_38
    iget v14, v3, Lsoftware/simplicial/a/g;->l:F

    iget v0, v3, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    add-float v14, v14, v16

    cmpg-float v14, v14, v13

    if-ltz v14, :cond_37

    .line 3696
    iget v14, v3, Lsoftware/simplicial/a/g;->l:F

    iget v0, v3, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    cmpl-float v14, v14, v12

    if-gtz v14, :cond_37

    .line 3698
    iget v14, v3, Lsoftware/simplicial/a/g;->m:F

    iget v0, v3, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    add-float v14, v14, v16

    cmpg-float v14, v14, v6

    if-ltz v14, :cond_37

    .line 3700
    iget v14, v3, Lsoftware/simplicial/a/g;->m:F

    iget v0, v3, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    cmpl-float v14, v14, v4

    if-gtz v14, :cond_37

    .line 3703
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 3706
    :cond_39
    new-instance v14, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->au:I

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3707
    const/4 v2, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v2, v3, :cond_3c

    .line 3709
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v3, v3, v2

    .line 3711
    iget-boolean v0, v3, Lsoftware/simplicial/a/bl;->E:Z

    move/from16 v16, v0

    if-eqz v16, :cond_3b

    .line 3713
    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3707
    :cond_3a
    :goto_13
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 3717
    :cond_3b
    iget v0, v3, Lsoftware/simplicial/a/bl;->l:F

    move/from16 v16, v0

    iget v0, v3, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    add-float v16, v16, v17

    cmpg-float v16, v16, v13

    if-ltz v16, :cond_3a

    .line 3719
    iget v0, v3, Lsoftware/simplicial/a/bl;->l:F

    move/from16 v16, v0

    iget v0, v3, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    cmpl-float v16, v16, v12

    if-gtz v16, :cond_3a

    .line 3721
    iget v0, v3, Lsoftware/simplicial/a/bl;->m:F

    move/from16 v16, v0

    iget v0, v3, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    add-float v16, v16, v17

    cmpg-float v16, v16, v6

    if-ltz v16, :cond_3a

    .line 3723
    iget v0, v3, Lsoftware/simplicial/a/bl;->m:F

    move/from16 v16, v0

    iget v0, v3, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    cmpl-float v16, v16, v4

    if-gtz v16, :cond_3a

    .line 3726
    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 3729
    :cond_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->m:Lsoftware/simplicial/a/f/al;

    move-object/from16 v0, v20

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->aH:Lsoftware/simplicial/a/f/bv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->at:Ljava/util/Collection;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move/from16 v17, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lsoftware/simplicial/a/f/bl;->d:S

    move/from16 v18, v0

    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;Ljava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/bf;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;FLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Collection;IS)Ljava/util/List;

    move-result-object v2

    .line 3732
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 3733
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v4, v2, v5}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_14

    .line 3735
    :cond_3d
    return-void

    :cond_3e
    move v5, v14

    move/from16 v14, v16

    goto/16 :goto_d

    :cond_3f
    move v12, v5

    move v13, v6

    move v6, v4

    move v4, v2

    goto/16 :goto_2

    :cond_40
    move-object/from16 v3, v20

    goto/16 :goto_1
.end method

.method private b(Lsoftware/simplicial/a/f/bl;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 5573
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 5574
    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_1

    .line 5576
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v0

    .line 5578
    iget-boolean v4, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v4, :cond_0

    .line 5579
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5574
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5582
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 5583
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 5585
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v0, v0, v1

    .line 5587
    iget-boolean v2, v0, Lsoftware/simplicial/a/bq;->F:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v5, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-eq v2, v5, :cond_2

    .line 5588
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5583
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5591
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->n:Lsoftware/simplicial/a/f/ak;

    iget v1, p0, Lsoftware/simplicial/a/bs;->aZ:I

    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->ah:[Lsoftware/simplicial/a/ae;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->ag:[Lsoftware/simplicial/a/bt;

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/a/f/ak;->a(IFLjava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/ae;[Lsoftware/simplicial/a/bt;Lsoftware/simplicial/a/f/bl;)Ljava/util/List;

    move-result-object v0

    .line 5592
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 5593
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    invoke-interface {v2, v0, p1}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_2

    .line 5594
    :cond_4
    return-void
.end method

.method private b(Lsoftware/simplicial/a/g;F)V
    .locals 13

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const v12, 0x417a6666    # 15.65f

    .line 6849
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_3

    .line 6851
    iget v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    .line 6852
    sget v1, Lsoftware/simplicial/a/ai;->S:F

    div-float/2addr v1, v2

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 6853
    sget v0, Lsoftware/simplicial/a/ai;->S:F

    div-float/2addr v0, v2

    .line 6854
    :cond_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    sub-float/2addr v1, v0

    div-float/2addr v1, v2

    .line 6855
    add-float/2addr v0, v1

    move v8, v0

    move v9, v1

    .line 6863
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 6864
    const v5, 0x40555555

    .line 6866
    iget v1, p1, Lsoftware/simplicial/a/g;->b:I

    .line 6867
    invoke-virtual {p0, v12, v9, v8}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v2

    .line 6868
    invoke-virtual {p0, v12, v9, v8}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v3

    float-to-double v6, v5

    float-to-double v10, v0

    .line 6869
    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v6, v10

    double-to-float v4, v6

    float-to-double v6, v5

    float-to-double v10, v0

    .line 6870
    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v6, v10

    double-to-float v5, v6

    iget-object v6, p1, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    move-object v0, p1

    move v7, p2

    .line 6866
    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/a/g;->a(IFFFFLsoftware/simplicial/a/g$a;F)V

    .line 6873
    const/4 v0, 0x1

    iput-boolean v0, p1, Lsoftware/simplicial/a/g;->f:Z

    .line 6875
    const/4 v0, 0x0

    .line 6876
    :goto_1
    const/16 v1, 0xfa

    if-ge v0, v1, :cond_1

    const v1, -0x3e85999a    # -15.65f

    invoke-direct {p0, p1, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/g;F)Lsoftware/simplicial/a/ao;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_4

    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 6878
    :cond_2
    invoke-virtual {p0, v12, v9, v8}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v1

    iput v1, p1, Lsoftware/simplicial/a/g;->l:F

    .line 6879
    invoke-virtual {p0, v12, v9, v8}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v1

    iput v1, p1, Lsoftware/simplicial/a/g;->m:F

    .line 6880
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6859
    :cond_3
    const/4 v1, 0x0

    .line 6860
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v0, v1

    move v8, v0

    move v9, v1

    goto :goto_0

    .line 6882
    :cond_4
    return-void
.end method

.method private c(I)Lsoftware/simplicial/a/bf;
    .locals 3

    .prologue
    .line 6647
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_2

    .line 6649
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 6651
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v2, :cond_1

    .line 6647
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6654
    :cond_1
    invoke-virtual {v1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v2

    if-ne v2, p1, :cond_0

    move-object v0, v1

    .line 6659
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Lsoftware/simplicial/a/bf;)V
    .locals 24

    .prologue
    .line 3739
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2e

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v19, v2

    check-cast v19, Lsoftware/simplicial/a/bw;

    .line 3741
    sget-object v2, Lsoftware/simplicial/a/bs;->U:Lsoftware/simplicial/a/bf;

    .line 3742
    move-object/from16 v0, v19

    iget v3, v0, Lsoftware/simplicial/a/bw;->f:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_4

    if-eqz p1, :cond_4

    move-object/from16 v2, p1

    .line 3746
    :cond_1
    :goto_0
    const v3, 0x3ff33333    # 1.9f

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aE:F

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lsoftware/simplicial/a/bf;->a(FZ)F

    move-result v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 3748
    iget v4, v2, Lsoftware/simplicial/a/bf;->l:F

    sub-float v6, v4, v3

    .line 3749
    iget v4, v2, Lsoftware/simplicial/a/bf;->l:F

    add-float v5, v4, v3

    .line 3750
    iget v4, v2, Lsoftware/simplicial/a/bf;->m:F

    sub-float/2addr v4, v3

    .line 3751
    iget v7, v2, Lsoftware/simplicial/a/bf;->m:F

    add-float/2addr v3, v7

    .line 3753
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v7, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v8, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v7, v8, :cond_30

    :cond_2
    move-object/from16 v0, p0

    iget-short v7, v0, Lsoftware/simplicial/a/bs;->aH:S

    if-gtz v7, :cond_30

    .line 3758
    const/high16 v6, -0x800000    # Float.NEGATIVE_INFINITY

    .line 3759
    const/high16 v5, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 3760
    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    .line 3761
    const/high16 v3, 0x7f800000    # Float.POSITIVE_INFINITY

    move v12, v6

    move v6, v5

    .line 3764
    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    const/4 v5, 0x5

    invoke-direct {v8, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3765
    new-instance v7, Ljava/util/ArrayList;

    const/4 v5, 0x5

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3766
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v9, v9

    if-ge v5, v9, :cond_6

    .line 3768
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v9, v9, v5

    .line 3770
    iget-boolean v10, v9, Lsoftware/simplicial/a/bq;->G:Z

    if-eqz v10, :cond_5

    .line 3772
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3766
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 3744
    :cond_4
    move-object/from16 v0, v19

    iget v3, v0, Lsoftware/simplicial/a/bw;->f:I

    if-ltz v3, :cond_1

    move-object/from16 v0, v19

    iget v3, v0, Lsoftware/simplicial/a/bw;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v19

    iget v4, v0, Lsoftware/simplicial/a/bw;->f:I

    aget-object v3, v3, v4

    iget-boolean v3, v3, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v3, :cond_1

    .line 3745
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v19

    iget v3, v0, Lsoftware/simplicial/a/bw;->f:I

    aget-object v2, v2, v3

    goto/16 :goto_0

    .line 3776
    :cond_5
    iget-boolean v10, v9, Lsoftware/simplicial/a/bq;->F:Z

    if-nez v10, :cond_3

    iget-object v10, v9, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v11, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-eq v10, v11, :cond_3

    .line 3779
    iget v10, v9, Lsoftware/simplicial/a/bq;->l:F

    iget v11, v9, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v10, v11

    cmpg-float v10, v10, v12

    if-ltz v10, :cond_3

    .line 3781
    iget v10, v9, Lsoftware/simplicial/a/bq;->l:F

    iget v11, v9, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v10, v11

    cmpl-float v10, v10, v6

    if-gtz v10, :cond_3

    .line 3783
    iget v10, v9, Lsoftware/simplicial/a/bq;->m:F

    iget v11, v9, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v10, v11

    cmpg-float v10, v10, v4

    if-ltz v10, :cond_3

    .line 3785
    iget v10, v9, Lsoftware/simplicial/a/bq;->m:F

    iget v11, v9, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v10, v11

    cmpl-float v10, v10, v3

    if-gtz v10, :cond_3

    .line 3788
    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 3792
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v9, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v5, v9, :cond_8

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v5, v5, 0x3

    const/4 v9, 0x2

    if-ge v5, v9, :cond_8

    iget-boolean v5, v2, Lsoftware/simplicial/a/bf;->aj:Z

    if-nez v5, :cond_8

    iget v5, v2, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v9

    if-ltz v5, :cond_8

    iget v5, v2, Lsoftware/simplicial/a/bf;->S:I

    if-eqz v5, :cond_7

    move-object/from16 v0, p0

    iget-short v5, v0, Lsoftware/simplicial/a/bs;->aH:S

    if-gtz v5, :cond_8

    .line 3794
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->m:Lsoftware/simplicial/a/f/al;

    move-object/from16 v0, v19

    iget-object v3, v0, Lsoftware/simplicial/a/bw;->g:Lsoftware/simplicial/a/f/bv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move/from16 v17, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lsoftware/simplicial/a/f/bl;->d:S

    move/from16 v18, v0

    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;Ljava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/bf;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;FLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Collection;IS)Ljava/util/List;

    move-result-object v2

    .line 3796
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 3797
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, v19

    iget-object v5, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v4, v2, v5}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_4

    .line 3802
    :cond_8
    const/4 v10, 0x0

    .line 3803
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->am:I

    if-lez v5, :cond_9

    .line 3806
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->am:I

    invoke-direct {v10, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3807
    const/4 v5, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v5, v9, :cond_9

    .line 3809
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v9, v9, v5

    .line 3811
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3807
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 3815
    :cond_9
    const/4 v15, 0x0

    .line 3816
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-lez v5, :cond_a

    .line 3819
    new-instance v15, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ap:I

    invoke-direct {v15, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3821
    const/4 v5, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v5, v9, :cond_a

    .line 3823
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v9, v9, v5

    .line 3825
    invoke-interface {v15, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3821
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 3829
    :cond_a
    const/4 v11, 0x0

    .line 3830
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-lez v5, :cond_b

    .line 3833
    new-instance v11, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aw:I

    invoke-direct {v11, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3834
    const/4 v5, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v5, v9, :cond_c

    .line 3836
    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v9, v9, v5

    .line 3838
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3834
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 3841
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v9, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v5, v9, :cond_c

    .line 3843
    new-instance v11, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v11, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3844
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    const/4 v9, 0x0

    aget-object v5, v5, v9

    invoke-interface {v11, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3848
    :cond_c
    new-instance v9, Ljava/util/ArrayList;

    const/4 v5, 0x4

    invoke-direct {v9, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 3851
    const/4 v14, 0x0

    .line 3852
    const/4 v13, 0x0

    .line 3853
    iget v5, v2, Lsoftware/simplicial/a/bf;->aZ:I

    if-lez v5, :cond_d

    .line 3854
    new-instance v5, Lsoftware/simplicial/a/a/s;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/s;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3855
    :cond_d
    iget v5, v2, Lsoftware/simplicial/a/bf;->ba:I

    if-lez v5, :cond_e

    .line 3856
    new-instance v5, Lsoftware/simplicial/a/a/v;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->aD:Lsoftware/simplicial/a/bv;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v16

    iget v0, v2, Lsoftware/simplicial/a/bf;->ba:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v5, v0, v1}, Lsoftware/simplicial/a/a/v;-><init>(II)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3857
    :cond_e
    iget v5, v2, Lsoftware/simplicial/a/bf;->bb:I

    if-lez v5, :cond_f

    .line 3858
    new-instance v5, Lsoftware/simplicial/a/a/v;

    sget-object v16, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v16

    iget v0, v2, Lsoftware/simplicial/a/bf;->bb:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v5, v0, v1}, Lsoftware/simplicial/a/a/v;-><init>(II)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3859
    :cond_f
    iget v5, v2, Lsoftware/simplicial/a/bf;->bd:I

    if-lez v5, :cond_10

    .line 3860
    new-instance v5, Lsoftware/simplicial/a/a/r;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/r;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3861
    :cond_10
    iget v5, v2, Lsoftware/simplicial/a/bf;->be:I

    if-lez v5, :cond_11

    .line 3862
    new-instance v5, Lsoftware/simplicial/a/a/u;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/u;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3863
    :cond_11
    iget v5, v2, Lsoftware/simplicial/a/bf;->bf:I

    if-lez v5, :cond_12

    .line 3864
    new-instance v5, Lsoftware/simplicial/a/a/t;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/t;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3865
    :cond_12
    iget v5, v2, Lsoftware/simplicial/a/bf;->bz:I

    if-lez v5, :cond_13

    .line 3866
    new-instance v5, Lsoftware/simplicial/a/a/ag;

    iget v0, v2, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v5, v0}, Lsoftware/simplicial/a/a/ag;-><init>(I)V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3867
    :cond_13
    iget v5, v2, Lsoftware/simplicial/a/bf;->bc:I

    if-lez v5, :cond_14

    .line 3868
    new-instance v5, Lsoftware/simplicial/a/a/d;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/d;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3869
    :cond_14
    iget v5, v2, Lsoftware/simplicial/a/bf;->bl:I

    if-lez v5, :cond_15

    .line 3870
    new-instance v5, Lsoftware/simplicial/a/a/o;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/o;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3871
    :cond_15
    iget v5, v2, Lsoftware/simplicial/a/bf;->bm:I

    if-lez v5, :cond_16

    .line 3872
    new-instance v5, Lsoftware/simplicial/a/a/p;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/p;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3873
    :cond_16
    iget v5, v2, Lsoftware/simplicial/a/bf;->bn:I

    if-lez v5, :cond_17

    .line 3874
    new-instance v5, Lsoftware/simplicial/a/a/q;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/q;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3875
    :cond_17
    iget v5, v2, Lsoftware/simplicial/a/bf;->bg:I

    if-lez v5, :cond_18

    .line 3876
    new-instance v5, Lsoftware/simplicial/a/a/f;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/f;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3877
    :cond_18
    iget v5, v2, Lsoftware/simplicial/a/bf;->bh:I

    if-lez v5, :cond_19

    .line 3878
    new-instance v5, Lsoftware/simplicial/a/a/g;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/g;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3879
    :cond_19
    iget v5, v2, Lsoftware/simplicial/a/bf;->bi:I

    if-lez v5, :cond_1a

    .line 3880
    new-instance v5, Lsoftware/simplicial/a/a/h;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/h;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3881
    :cond_1a
    iget v5, v2, Lsoftware/simplicial/a/bf;->bk:I

    if-lez v5, :cond_1b

    .line 3882
    new-instance v5, Lsoftware/simplicial/a/a/i;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/i;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3883
    :cond_1b
    iget v5, v2, Lsoftware/simplicial/a/bf;->bj:I

    if-lez v5, :cond_1c

    .line 3884
    new-instance v5, Lsoftware/simplicial/a/a/j;

    invoke-direct {v5}, Lsoftware/simplicial/a/a/j;-><init>()V

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3886
    :cond_1c
    const/4 v5, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v5, v0, :cond_1d

    .line 3888
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v16, v0

    aget-object v16, v16, v5

    .line 3890
    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 3886
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 3893
    :cond_1d
    const/4 v5, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v5, v0, :cond_27

    .line 3895
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v16, v0

    aget-object v17, v16, v5

    .line 3897
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lsoftware/simplicial/a/bf;->aF:Z

    move/from16 v16, v0

    if-nez v16, :cond_1e

    .line 3893
    :goto_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 3900
    :cond_1e
    const/16 v16, 0x0

    move/from16 v23, v16

    move/from16 v16, v14

    move v14, v13

    move/from16 v13, v23

    :goto_b
    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->bF:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_25

    .line 3902
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    move-object/from16 v18, v0

    aget-object v18, v18, v13

    .line 3903
    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v13, v0, :cond_20

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lsoftware/simplicial/a/bh;->M:Z

    move/from16 v21, v0

    if-nez v21, :cond_20

    .line 3900
    :cond_1f
    :goto_c
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    .line 3906
    :cond_20
    move-object/from16 v0, v17

    if-ne v0, v2, :cond_22

    .line 3908
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3909
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lsoftware/simplicial/a/bh;->a(Ljava/util/List;)V

    .line 3910
    iget-boolean v0, v2, Lsoftware/simplicial/a/bf;->aX:Z

    move/from16 v18, v0

    if-eqz v18, :cond_21

    if-nez v16, :cond_21

    .line 3912
    new-instance v16, Lsoftware/simplicial/a/a/af;

    iget v0, v2, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v18, v0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/a/af;-><init>(I)V

    move-object/from16 v0, v16

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3913
    const/16 v16, 0x1

    .line 3915
    :cond_21
    iget-boolean v0, v2, Lsoftware/simplicial/a/bf;->aY:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1f

    if-nez v14, :cond_1f

    .line 3917
    new-instance v14, Lsoftware/simplicial/a/a/ac;

    iget v0, v2, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-direct {v14, v0}, Lsoftware/simplicial/a/a/ac;-><init>(I)V

    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3918
    const/4 v14, 0x1

    goto :goto_c

    .line 3923
    :cond_22
    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    add-float v21, v21, v22

    cmpg-float v21, v21, v12

    if-ltz v21, :cond_23

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    sub-float v21, v21, v22

    cmpl-float v21, v21, v6

    if-gtz v21, :cond_23

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v21, v0

    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    add-float v21, v21, v22

    cmpg-float v21, v21, v4

    if-ltz v21, :cond_23

    move-object/from16 v0, v18

    iget v0, v0, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v21, v0

    .line 3924
    invoke-virtual/range {v18 .. v18}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v22

    sub-float v21, v21, v22

    cmpl-float v21, v21, v3

    if-lez v21, :cond_24

    .line 3926
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_1f

    .line 3927
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 3931
    :cond_24
    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3932
    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lsoftware/simplicial/a/bh;->a(Ljava/util/List;)V

    goto/16 :goto_c

    .line 3935
    :cond_25
    move-object/from16 v0, v17

    iget-object v13, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_26

    move-object/from16 v0, v17

    iget-boolean v13, v0, Lsoftware/simplicial/a/bf;->aX:Z

    if-eqz v13, :cond_26

    if-nez v16, :cond_26

    .line 3937
    new-instance v13, Lsoftware/simplicial/a/a/af;

    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/bf;->C:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v13, v0}, Lsoftware/simplicial/a/a/af;-><init>(I)V

    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3938
    const/16 v16, 0x1

    .line 3940
    :cond_26
    move-object/from16 v0, v17

    iget-object v13, v0, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_2f

    move-object/from16 v0, v17

    iget-boolean v13, v0, Lsoftware/simplicial/a/bf;->aY:Z

    if-eqz v13, :cond_2f

    if-nez v14, :cond_2f

    .line 3942
    new-instance v13, Lsoftware/simplicial/a/a/ac;

    move-object/from16 v0, v17

    iget v14, v0, Lsoftware/simplicial/a/bf;->C:I

    invoke-direct {v13, v14}, Lsoftware/simplicial/a/a/ac;-><init>(I)V

    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3943
    const/4 v14, 0x1

    move v13, v14

    move/from16 v14, v16

    goto/16 :goto_a

    .line 3947
    :cond_27
    new-instance v5, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->az:I

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3948
    const/4 v2, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v13, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v13, :cond_2a

    .line 3950
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v13, v13, v2

    .line 3952
    iget-boolean v14, v13, Lsoftware/simplicial/a/g;->f:Z

    if-eqz v14, :cond_29

    .line 3954
    invoke-interface {v5, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3948
    :cond_28
    :goto_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 3958
    :cond_29
    iget v14, v13, Lsoftware/simplicial/a/g;->l:F

    iget v0, v13, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    add-float v14, v14, v16

    cmpg-float v14, v14, v12

    if-ltz v14, :cond_28

    .line 3960
    iget v14, v13, Lsoftware/simplicial/a/g;->l:F

    iget v0, v13, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    cmpl-float v14, v14, v6

    if-gtz v14, :cond_28

    .line 3962
    iget v14, v13, Lsoftware/simplicial/a/g;->m:F

    iget v0, v13, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    add-float v14, v14, v16

    cmpg-float v14, v14, v4

    if-ltz v14, :cond_28

    .line 3964
    iget v14, v13, Lsoftware/simplicial/a/g;->m:F

    iget v0, v13, Lsoftware/simplicial/a/g;->n:F

    move/from16 v16, v0

    sub-float v14, v14, v16

    cmpl-float v14, v14, v3

    if-gtz v14, :cond_28

    .line 3967
    invoke-interface {v5, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 3970
    :cond_2a
    new-instance v14, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->au:I

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 3971
    const/4 v2, 0x0

    :goto_f
    move-object/from16 v0, p0

    iget v13, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v2, v13, :cond_2d

    .line 3973
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v13, v13, v2

    .line 3975
    iget-boolean v0, v13, Lsoftware/simplicial/a/bl;->E:Z

    move/from16 v16, v0

    if-eqz v16, :cond_2c

    .line 3977
    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3971
    :cond_2b
    :goto_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 3981
    :cond_2c
    iget v0, v13, Lsoftware/simplicial/a/bl;->l:F

    move/from16 v16, v0

    iget v0, v13, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    add-float v16, v16, v17

    cmpg-float v16, v16, v12

    if-ltz v16, :cond_2b

    .line 3983
    iget v0, v13, Lsoftware/simplicial/a/bl;->l:F

    move/from16 v16, v0

    iget v0, v13, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    cmpl-float v16, v16, v6

    if-gtz v16, :cond_2b

    .line 3985
    iget v0, v13, Lsoftware/simplicial/a/bl;->m:F

    move/from16 v16, v0

    iget v0, v13, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    add-float v16, v16, v17

    cmpg-float v16, v16, v4

    if-ltz v16, :cond_2b

    .line 3987
    iget v0, v13, Lsoftware/simplicial/a/bl;->m:F

    move/from16 v16, v0

    iget v0, v13, Lsoftware/simplicial/a/bl;->n:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    cmpl-float v16, v16, v3

    if-gtz v16, :cond_2b

    .line 3990
    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 3993
    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->m:Lsoftware/simplicial/a/f/al;

    move-object/from16 v0, v19

    iget-object v3, v0, Lsoftware/simplicial/a/bw;->g:Lsoftware/simplicial/a/f/bv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->at:Ljava/util/Collection;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move/from16 v17, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lsoftware/simplicial/a/f/bl;->d:S

    move/from16 v18, v0

    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;Ljava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/bf;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;FLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Collection;IS)Ljava/util/List;

    move-result-object v2

    .line 3996
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 3997
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, v19

    iget-object v5, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v4, v2, v5}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_11

    .line 3999
    :cond_2e
    return-void

    :cond_2f
    move v13, v14

    move/from16 v14, v16

    goto/16 :goto_a

    :cond_30
    move v12, v6

    move v6, v5

    goto/16 :goto_1
.end method

.method private d(I)Lsoftware/simplicial/a/bf;
    .locals 3

    .prologue
    .line 6733
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_2

    .line 6735
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 6737
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v2, :cond_1

    .line 6733
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6740
    :cond_1
    iget v2, v1, Lsoftware/simplicial/a/bf;->A:I

    if-ne v2, p1, :cond_0

    move-object v0, v1

    .line 6745
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(Lsoftware/simplicial/a/bf;)V
    .locals 3

    .prologue
    .line 5506
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    if-nez v0, :cond_0

    .line 5507
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget v1, p1, Lsoftware/simplicial/a/bf;->A:I

    iget-object v2, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-interface {v0, p0, v1, v2}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;II)V

    .line 5508
    :cond_0
    return-void
.end method

.method private e(I)Lsoftware/simplicial/a/bw;
    .locals 2

    .prologue
    .line 7065
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bw;

    return-object v0
.end method

.method private e(Lsoftware/simplicial/a/bf;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5934
    iget-boolean v0, p1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v0, :cond_1

    .line 5972
    :cond_0
    :goto_0
    return-void

    .line 5937
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    if-nez v0, :cond_2

    .line 5939
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v1, "Failed to remove player %d from game %d. Game was empty!"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 5942
    :cond_2
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->g(Lsoftware/simplicial/a/bf;)V

    .line 5943
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->f(Lsoftware/simplicial/a/bf;)V

    .line 5944
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5945
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v0, v4

    iget-byte v0, v0, Lsoftware/simplicial/a/bf;->R:B

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v5

    iget-byte v1, v1, Lsoftware/simplicial/a/bf;->R:B

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p1, Lsoftware/simplicial/a/bf;->R:B

    .line 5947
    :cond_3
    iput-boolean v4, p1, Lsoftware/simplicial/a/bf;->aF:Z

    .line 5948
    iget v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    .line 5949
    instance-of v0, p1, Lsoftware/simplicial/a/i;

    if-eqz v0, :cond_4

    .line 5951
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    iget v1, p1, Lsoftware/simplicial/a/bf;->C:I

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->b:[Lsoftware/simplicial/a/bf;

    iget v3, p1, Lsoftware/simplicial/a/bf;->C:I

    aget-object v2, v2, v3

    aput-object v2, v0, v1

    .line 5952
    iget v0, p0, Lsoftware/simplicial/a/bs;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/bs;->q:I

    .line 5955
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_5

    .line 5957
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v1, v1, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/b/a;->a(I)V

    .line 5960
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v0, :cond_6

    .line 5962
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v0, v0, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5963
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v0, v0, Lsoftware/simplicial/a/c/j;->d:Ljava/util/List;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5966
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_0

    .line 5968
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v0, v0, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v0, v0, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5969
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v0, v0, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v0, v0, Lsoftware/simplicial/a/h/a;->d:Ljava/util/List;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private f()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 241
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->r:S

    .line 242
    invoke-static {}, Lsoftware/simplicial/a/bs;->j()Lsoftware/simplicial/a/bv;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/bs;->aD:Lsoftware/simplicial/a/bv;

    move v0, v1

    .line 244
    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v2, :cond_2

    .line 246
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v2, v0

    .line 248
    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->g()V

    .line 249
    invoke-virtual {v4}, Lsoftware/simplicial/a/bf;->c()V

    move v2, v1

    .line 251
    :goto_1
    iget-object v3, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 253
    iget-object v3, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v3, v2

    .line 255
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->k()V

    .line 257
    iget-boolean v3, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v3, :cond_0

    iget v3, v4, Lsoftware/simplicial/a/bf;->S:I

    if-ge v2, v3, :cond_0

    move v3, v1

    .line 259
    :goto_2
    iget v6, p0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v3, v6, :cond_0

    .line 261
    iget-object v6, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v6, v6, v3

    .line 264
    iget-object v7, v5, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    iget v8, v5, Lsoftware/simplicial/a/bh;->l:F

    iget v9, v5, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v6, v8, v9}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v6

    aput-object v6, v7, v3

    .line 265
    iget v6, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v6, v5, Lsoftware/simplicial/a/bh;->q:I

    .line 259
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 251
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 244
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 271
    :goto_3
    iget v2, p0, Lsoftware/simplicial/a/bs;->ay:I

    if-ge v0, v2, :cond_3

    .line 273
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v2, v2, v0

    .line 275
    invoke-virtual {v2}, Lsoftware/simplicial/a/ae;->g()V

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 278
    :goto_4
    iget v2, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v0, v2, :cond_4

    .line 280
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v2, v2, v0

    .line 282
    invoke-virtual {v2}, Lsoftware/simplicial/a/z;->g()V

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    move v0, v1

    .line 285
    :goto_5
    iget v2, p0, Lsoftware/simplicial/a/bs;->aA:I

    if-ge v0, v2, :cond_5

    .line 287
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v2, v2, v0

    .line 289
    invoke-virtual {v2}, Lsoftware/simplicial/a/bt;->g()V

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    move v0, v1

    .line 292
    :goto_6
    iget v2, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v0, v2, :cond_7

    .line 294
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v3, v2, v0

    .line 296
    invoke-virtual {v3}, Lsoftware/simplicial/a/bl;->g()V

    move v2, v1

    .line 298
    :goto_7
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 300
    iget-object v4, v3, Lsoftware/simplicial/a/bl;->p:[Lsoftware/simplicial/a/cb;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v5, v5, v2

    iget v6, v3, Lsoftware/simplicial/a/bl;->l:F

    iget v7, v3, Lsoftware/simplicial/a/bl;->m:F

    invoke-virtual {v5, v6, v7}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v5

    aput-object v5, v4, v2

    .line 301
    iget v4, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v4, v3, Lsoftware/simplicial/a/bl;->q:I

    .line 298
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 292
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    move v0, v1

    .line 305
    :goto_8
    iget v2, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v0, v2, :cond_9

    .line 307
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v3, v2, v0

    .line 309
    invoke-virtual {v3}, Lsoftware/simplicial/a/ag;->g()V

    move v2, v1

    .line 311
    :goto_9
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    array-length v4, v4

    if-ge v2, v4, :cond_8

    .line 313
    iget-object v4, v3, Lsoftware/simplicial/a/ag;->p:[Lsoftware/simplicial/a/cb;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v5, v5, v2

    iget v6, v3, Lsoftware/simplicial/a/ag;->l:F

    iget v7, v3, Lsoftware/simplicial/a/ag;->m:F

    invoke-virtual {v5, v6, v7}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v5

    aput-object v5, v4, v2

    .line 314
    iget v4, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v4, v3, Lsoftware/simplicial/a/ag;->q:I

    .line 311
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 305
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    move v0, v1

    .line 318
    :goto_a
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v2, v2

    if-ge v0, v2, :cond_c

    .line 320
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v3, v2, v0

    .line 322
    invoke-virtual {v3}, Lsoftware/simplicial/a/bq;->g()V

    .line 324
    iget-object v2, v3, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-ne v2, v4, :cond_b

    .line 318
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_b
    move v2, v1

    .line 327
    :goto_b
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    array-length v4, v4

    if-ge v2, v4, :cond_a

    .line 329
    iget-object v4, v3, Lsoftware/simplicial/a/bq;->p:[Lsoftware/simplicial/a/cb;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v5, v5, v2

    iget v6, v3, Lsoftware/simplicial/a/bq;->l:F

    iget v7, v3, Lsoftware/simplicial/a/bq;->m:F

    invoke-virtual {v5, v6, v7}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v5

    aput-object v5, v4, v2

    .line 330
    iget v4, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v4, v3, Lsoftware/simplicial/a/bq;->q:I

    .line 327
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_c
    move v0, v1

    .line 334
    :goto_c
    iget v2, p0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v0, v2, :cond_d

    .line 336
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v2, v2, v0

    .line 338
    invoke-virtual {v2}, Lsoftware/simplicial/a/ca;->g()V

    .line 334
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_d
    move v0, v1

    .line 341
    :goto_d
    iget v2, p0, Lsoftware/simplicial/a/bs;->ar:I

    if-ge v0, v2, :cond_f

    move v2, v1

    .line 343
    :goto_e
    iget v3, p0, Lsoftware/simplicial/a/bs;->ar:I

    if-ge v2, v3, :cond_e

    .line 345
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->as:[[Lsoftware/simplicial/a/bz;

    aget-object v3, v3, v2

    aget-object v3, v3, v0

    .line 347
    invoke-virtual {v3}, Lsoftware/simplicial/a/bz;->c()V

    .line 343
    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .line 341
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 351
    :cond_f
    :goto_f
    iget v0, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v1, v0, :cond_10

    .line 353
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v0, v0, v1

    .line 355
    invoke-virtual {v0}, Lsoftware/simplicial/a/g;->g()V

    .line 351
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 358
    :cond_10
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->g()V

    .line 359
    return-void
.end method

.method private f(Lsoftware/simplicial/a/bf;)V
    .locals 2

    .prologue
    .line 6005
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-nez v0, :cond_0

    .line 6010
    :goto_0
    return-void

    .line 6008
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v1, v0, Lsoftware/simplicial/a/bx;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/bx;->f:I

    .line 6009
    const/4 v0, 0x0

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_0
.end method

.method private g()V
    .locals 10

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 363
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v4, v5, :cond_0

    iget-short v4, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v4, :cond_0

    .line 365
    iget-short v4, p0, Lsoftware/simplicial/a/bs;->aX:S

    int-to-double v4, v4

    iget v6, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v7, p0, Lsoftware/simplicial/a/bs;->s:I

    sub-int/2addr v6, v7

    int-to-double v6, v6

    const-wide/high16 v8, 0x4034000000000000L    # 20.0

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-short v6, p0, Lsoftware/simplicial/a/bs;->aX:S

    add-int/lit8 v6, v6, -0x3c

    int-to-double v6, v6

    div-double/2addr v4, v6

    .line 366
    cmpl-double v6, v4, v2

    if-lez v6, :cond_2

    .line 368
    :goto_0
    cmpg-double v4, v2, v0

    if-gez v4, :cond_1

    .line 370
    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    .line 371
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v1, p0, Lsoftware/simplicial/a/bs;->aF:F

    iput v1, v0, Lsoftware/simplicial/a/z;->n:F

    .line 373
    :cond_0
    return-void

    :cond_1
    move-wide v0, v2

    goto :goto_1

    :cond_2
    move-wide v2, v4

    goto :goto_0
.end method

.method private g(Lsoftware/simplicial/a/bf;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0x4c4b40

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 6233
    iget-boolean v0, p1, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v0, :cond_1

    .line 6310
    :cond_0
    :goto_0
    return-void

    .line 6236
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    if-ne p1, v0, :cond_2

    .line 6237
    invoke-virtual {p0, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;)V

    .line 6239
    :cond_2
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/bf;->b(J)V

    .line 6241
    instance-of v0, p1, Lsoftware/simplicial/a/i;

    if-nez v0, :cond_3

    .line 6242
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-wide v4, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;JLsoftware/simplicial/a/i/a;)V

    .line 6244
    :cond_3
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-eqz v0, :cond_4

    .line 6245
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    invoke-virtual {v0, p1, v7}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 6248
    :cond_4
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v0, v0, Lsoftware/simplicial/a/ay;->g:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_5

    .line 6250
    sget-object v0, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Capped "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lsoftware/simplicial/a/bf;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'s xp to 2M from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-wide v2, v2, Lsoftware/simplicial/a/ay;->g:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 6251
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iput-wide v10, v0, Lsoftware/simplicial/a/ay;->g:J

    .line 6253
    :cond_5
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget-boolean v1, p0, Lsoftware/simplicial/a/bs;->C:Z

    iput-boolean v1, v0, Lsoftware/simplicial/a/ay;->Q:Z

    .line 6254
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    invoke-interface {v0, p1, v7}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 6256
    instance-of v0, p1, Lsoftware/simplicial/a/i;

    if-nez v0, :cond_6

    .line 6261
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, p1, Lsoftware/simplicial/a/bf;->A:I

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-object v4, p1, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v5, p1, Lsoftware/simplicial/a/bf;->E:[B

    iget-boolean v6, p0, Lsoftware/simplicial/a/bs;->aR:Z

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    :cond_6
    move v0, v7

    .line 6265
    :goto_1
    iget v1, p1, Lsoftware/simplicial/a/bf;->S:I

    if-ge v0, v1, :cond_7

    .line 6267
    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/bf;->b(I)V

    .line 6268
    iget-object v1, p1, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 6265
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6270
    :cond_7
    iput-boolean v8, p1, Lsoftware/simplicial/a/bf;->aj:Z

    move v0, v7

    .line 6272
    :goto_2
    iget v1, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v0, v1, :cond_9

    .line 6274
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v1, v1, v0

    .line 6275
    iget v2, v1, Lsoftware/simplicial/a/g;->g:I

    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v3

    if-ne v2, v3, :cond_8

    .line 6276
    iput v7, v1, Lsoftware/simplicial/a/g;->g:I

    .line 6272
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6279
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_a

    .line 6280
    iput v7, p1, Lsoftware/simplicial/a/bf;->Q:I

    .line 6282
    :cond_a
    const/4 v0, -0x1

    iput-byte v0, p1, Lsoftware/simplicial/a/bf;->aJ:B

    .line 6283
    const/4 v0, 0x0

    iput v0, p1, Lsoftware/simplicial/a/bf;->T:F

    .line 6284
    iput-boolean v7, p1, Lsoftware/simplicial/a/bf;->K:Z

    .line 6286
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_f

    iget-object v0, p1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v0, v0, Lsoftware/simplicial/a/bx;->c:I

    if-ne v0, v8, :cond_f

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v1, p0, Lsoftware/simplicial/a/bs;->s:I

    if-le v0, v1, :cond_f

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_f

    move v0, v7

    .line 6288
    :goto_3
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_e

    .line 6290
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 6292
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_b

    if-ne v1, p1, :cond_c

    .line 6288
    :cond_b
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6295
    :cond_c
    iget-object v2, v1, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->c:I

    if-ne v2, v8, :cond_d

    .line 6296
    iget v2, v1, Lsoftware/simplicial/a/bf;->bi:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/bf;->bi:I

    goto :goto_4

    .line 6298
    :cond_d
    iget v2, v1, Lsoftware/simplicial/a/bf;->bk:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/bf;->bk:I

    goto :goto_4

    .line 6301
    :cond_e
    iget v0, p0, Lsoftware/simplicial/a/bs;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/bs;->B:I

    .line 6302
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/bs;->f(Lsoftware/simplicial/a/bf;)V

    .line 6303
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v0, v0, v7

    invoke-direct {p0, p1, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    .line 6305
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    add-int/lit8 v0, v0, 0xa

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 6308
    :cond_f
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6309
    iget-byte v0, p1, Lsoftware/simplicial/a/bf;->R:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p1, Lsoftware/simplicial/a/bf;->R:B

    goto/16 :goto_0
.end method

.method private h()V
    .locals 7

    .prologue
    .line 389
    :cond_0
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aC:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/a/f/bi;

    .line 390
    if-nez v1, :cond_1

    .line 409
    :goto_1
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aB:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/f/be;

    .line 410
    if-nez v2, :cond_2

    .line 472
    return-void

    .line 395
    :cond_1
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    iget v3, v1, Lsoftware/simplicial/a/f/bi;->a:I

    aget-object v2, v2, v3

    invoke-static {v2}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bh;)Lsoftware/simplicial/a/f/be;

    move-result-object v2

    .line 396
    invoke-virtual {v2, v1}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aB:Ljava/util/Queue;

    invoke-interface {v1, v2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 401
    :catch_0
    move-exception v1

    goto :goto_0

    .line 415
    :cond_2
    :try_start_1
    sget-object v1, Lsoftware/simplicial/a/bs$2;->a:[I

    iget-object v3, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 463
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GS: Uhandled message "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 467
    :catch_1
    move-exception v1

    .line 469
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v4, "Failed to handle message %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v2, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 418
    :pswitch_0
    :try_start_2
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ab;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ab;)V

    goto :goto_1

    .line 421
    :pswitch_1
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/w;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/w;)V

    goto :goto_1

    .line 424
    :pswitch_2
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/v;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/v;)V

    goto :goto_1

    .line 427
    :pswitch_3
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ac;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ac;)V

    goto :goto_1

    .line 430
    :pswitch_4
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/az;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/az;)V

    goto/16 :goto_1

    .line 433
    :pswitch_5
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/u;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/u;)V

    goto/16 :goto_1

    .line 436
    :pswitch_6
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ae;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ae;)V

    goto/16 :goto_1

    .line 439
    :pswitch_7
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ca;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ca;)V

    goto/16 :goto_1

    .line 442
    :pswitch_8
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/cx;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/cx;)V

    goto/16 :goto_1

    .line 445
    :pswitch_9
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/bk;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/bk;)V

    goto/16 :goto_1

    .line 448
    :pswitch_a
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ax;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ax;)V

    goto/16 :goto_1

    .line 451
    :pswitch_b
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/n;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/n;)V

    goto/16 :goto_1

    .line 454
    :pswitch_c
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/bz;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/bz;)V

    goto/16 :goto_1

    .line 457
    :pswitch_d
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/bd;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/bd;)V

    goto/16 :goto_1

    .line 460
    :pswitch_e
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ai;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/ai;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private h(Lsoftware/simplicial/a/bf;)Z
    .locals 18

    .prologue
    .line 6453
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    .line 6454
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_0

    .line 6455
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    .line 6457
    :cond_0
    sget-object v3, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p1

    instance-of v3, v0, Lsoftware/simplicial/a/i;

    if-eqz v3, :cond_2

    .line 6459
    :cond_1
    move-object/from16 v0, p1

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 6460
    const/4 v2, 0x1

    .line 6495
    :goto_0
    return v2

    .line 6463
    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v3, v3, Lsoftware/simplicial/a/az;->M:Z

    if-nez v3, :cond_4

    iget v3, v2, Lsoftware/simplicial/a/e;->lm:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget-object v3, v2, Lsoftware/simplicial/a/e;->lt:Lsoftware/simplicial/a/d;

    if-nez v3, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->ln:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lo:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lp:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lq:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lG:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget-boolean v3, v2, Lsoftware/simplicial/a/e;->lu:Z

    if-nez v3, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lr:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->ls:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->ly:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lz:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lA:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lB:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lC:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget v3, v2, Lsoftware/simplicial/a/e;->lD:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    .line 6481
    :cond_3
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 6482
    const/4 v2, 0x0

    goto :goto_0

    .line 6485
    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v4, v4, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v6, v5, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v6, v6, Lsoftware/simplicial/a/az;->d:I

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v7, v7, Lsoftware/simplicial/a/az;->e:I

    move-object/from16 v0, p1

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v8, v8, Lsoftware/simplicial/a/az;->f:I

    move-object/from16 v0, p1

    iget-object v9, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v9, v9, Lsoftware/simplicial/a/az;->g:I

    move-object/from16 v0, p1

    iget-object v10, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v10, v10, Lsoftware/simplicial/a/az;->h:I

    move-object/from16 v0, p1

    iget-object v11, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v11, v11, Lsoftware/simplicial/a/az;->i:I

    move-object/from16 v0, p1

    iget-object v12, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v12, v12, Lsoftware/simplicial/a/az;->j:I

    move-object/from16 v0, p1

    iget-object v13, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v13, v13, Lsoftware/simplicial/a/az;->k:I

    move-object/from16 v0, p1

    iget-object v14, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v14, v14, Lsoftware/simplicial/a/az;->l:I

    move-object/from16 v0, p1

    iget-object v15, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget v15, v15, Lsoftware/simplicial/a/az;->m:I

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lsoftware/simplicial/a/az;->n:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lsoftware/simplicial/a/az;->o:I

    move/from16 v17, v0

    invoke-static/range {v2 .. v17}, Lsoftware/simplicial/a/e;->a(Lsoftware/simplicial/a/e;Ljava/util/Set;Ljava/util/Set;IIIIIIIIIIIII)Z

    move-result v2

    if-nez v2, :cond_5

    .line 6490
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 6491
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 6494
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 6495
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private i()V
    .locals 15

    .prologue
    .line 477
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v1, v0, :cond_d

    .line 479
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v5, v0, v1

    .line 482
    invoke-virtual {v5}, Lsoftware/simplicial/a/z;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 477
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 485
    :cond_1
    const/4 v3, 0x0

    .line 486
    new-instance v6, Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 487
    const/4 v2, 0x0

    .line 489
    const/4 v0, 0x0

    move v14, v0

    move v0, v2

    move-object v2, v3

    move v3, v14

    :goto_2
    iget v4, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v4, :cond_8

    const/4 v4, 0x1

    if-gt v0, v4, :cond_8

    .line 491
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v7, v4, v3

    .line 493
    iget-boolean v4, v7, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v4, :cond_2

    iget-boolean v4, v7, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v4, :cond_3

    .line 489
    :cond_2
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 496
    :cond_3
    const/4 v4, 0x0

    :goto_4
    iget v8, v7, Lsoftware/simplicial/a/bf;->S:I

    if-ge v4, v8, :cond_2

    .line 498
    iget-object v8, v7, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v8, v8, v4

    .line 499
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v9

    if-nez v9, :cond_4

    iget v9, v7, Lsoftware/simplicial/a/bf;->ax:I

    if-lez v9, :cond_5

    .line 496
    :cond_4
    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 502
    :cond_5
    iget-object v9, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v10, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne v9, v10, :cond_7

    .line 504
    invoke-virtual {v5, v8}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 506
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 507
    iget-object v4, v7, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eq v4, v2, :cond_2

    .line 509
    add-int/lit8 v0, v0, 0x1

    .line 510
    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 511
    iget-object v2, v7, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_3

    .line 513
    :cond_6
    const/4 v2, 0x0

    .line 514
    goto :goto_3

    .line 519
    :cond_7
    iget-object v9, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v10, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v9, v10, :cond_4

    .line 521
    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    mul-float/2addr v9, v10

    invoke-virtual {v5, v8, v9}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 523
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->c()F

    move-result v9

    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->d()F

    move-result v10

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    .line 524
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->e()V

    .line 525
    iget-wide v10, p0, Lsoftware/simplicial/a/bs;->aO:J

    invoke-virtual {v7, v10, v11}, Lsoftware/simplicial/a/bf;->a(J)V

    .line 526
    iget v8, v5, Lsoftware/simplicial/a/z;->d:F

    float-to-double v10, v8

    iget v8, v5, Lsoftware/simplicial/a/z;->d:F

    float-to-double v12, v8

    mul-double/2addr v10, v12

    float-to-double v8, v9

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v8

    double-to-float v8, v8

    iput v8, v5, Lsoftware/simplicial/a/z;->d:F

    goto :goto_5

    .line 532
    :cond_8
    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 534
    iget-object v0, v5, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    if-eq v0, v2, :cond_b

    .line 536
    iget v0, v5, Lsoftware/simplicial/a/z;->b:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_9

    .line 537
    iget v0, v5, Lsoftware/simplicial/a/z;->b:F

    iget v3, p0, Lsoftware/simplicial/a/bs;->ab:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    sub-float/2addr v0, v3

    iput v0, v5, Lsoftware/simplicial/a/z;->b:F

    .line 539
    :cond_9
    iget v0, v5, Lsoftware/simplicial/a/z;->b:F

    const/4 v3, 0x0

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_b

    .line 541
    iget-object v0, v5, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bx;)Ljava/util/List;

    move-result-object v4

    .line 542
    const/4 v0, 0x0

    move v3, v0

    :goto_6
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 543
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget v7, v0, Lsoftware/simplicial/a/bf;->bm:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v0, Lsoftware/simplicial/a/bf;->bm:I

    .line 542
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 544
    :cond_a
    iput-object v2, v5, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    .line 545
    const/4 v0, 0x0

    iput v0, v5, Lsoftware/simplicial/a/z;->b:F

    .line 546
    iget-object v0, v5, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bx;)Ljava/util/List;

    move-result-object v4

    .line 547
    const/4 v0, 0x0

    move v3, v0

    :goto_7
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    .line 548
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget v7, v0, Lsoftware/simplicial/a/bf;->bl:I

    add-int/lit8 v7, v7, 0x1

    iput v7, v0, Lsoftware/simplicial/a/bf;->bl:I

    .line 547
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    .line 552
    :cond_b
    iget-object v0, v5, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    if-ne v0, v2, :cond_0

    .line 554
    iget v0, v5, Lsoftware/simplicial/a/z;->b:F

    iget v3, p0, Lsoftware/simplicial/a/bs;->ab:F

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    add-float/2addr v0, v3

    iput v0, v5, Lsoftware/simplicial/a/z;->b:F

    .line 555
    iget v0, v5, Lsoftware/simplicial/a/z;->b:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_0

    .line 557
    iget v0, v2, Lsoftware/simplicial/a/bx;->e:I

    iget v3, v5, Lsoftware/simplicial/a/z;->c:I

    add-int/2addr v0, v3

    iput v0, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 558
    const/4 v0, 0x0

    move v2, v0

    :goto_8
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 560
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget v4, v5, Lsoftware/simplicial/a/z;->c:I

    invoke-virtual {v0, v3, v4}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;I)V

    .line 561
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bf;

    iget v3, v0, Lsoftware/simplicial/a/bf;->bn:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lsoftware/simplicial/a/bf;->bn:I

    .line 558
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 563
    :cond_c
    invoke-virtual {v5}, Lsoftware/simplicial/a/z;->e()V

    goto/16 :goto_1

    .line 570
    :cond_d
    const/4 v0, 0x0

    :goto_9
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_16

    .line 572
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v1, v0

    .line 574
    iget-boolean v1, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v1, :cond_e

    iget-boolean v1, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v1, :cond_f

    .line 570
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 577
    :cond_f
    const/4 v1, 0x0

    :goto_a
    iget v2, v4, Lsoftware/simplicial/a/bf;->S:I

    if-ge v1, v2, :cond_e

    .line 579
    iget-object v2, v4, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v2, v1

    .line 580
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 577
    :goto_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 585
    :cond_10
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_15

    .line 587
    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v3, p0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    .line 588
    iget v2, p0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v2, v3

    .line 597
    :goto_c
    iget v6, v5, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    const v8, 0x3f2aaaab

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpg-float v6, v6, v3

    if-gez v6, :cond_11

    .line 599
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const v7, 0x3f2aaaab

    mul-float/2addr v6, v7

    add-float/2addr v6, v3

    iput v6, v5, Lsoftware/simplicial/a/bh;->l:F

    .line 600
    instance-of v6, v4, Lsoftware/simplicial/a/i;

    if-eqz v6, :cond_11

    .line 601
    const/4 v6, 0x0

    iput v6, v4, Lsoftware/simplicial/a/bf;->M:F

    .line 603
    :cond_11
    iget v6, v5, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    const v8, 0x3f2aaaab

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    cmpl-float v6, v6, v2

    if-lez v6, :cond_12

    .line 605
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const v7, 0x3f2aaaab

    mul-float/2addr v6, v7

    sub-float v6, v2, v6

    iput v6, v5, Lsoftware/simplicial/a/bh;->l:F

    .line 606
    instance-of v6, v4, Lsoftware/simplicial/a/i;

    if-eqz v6, :cond_12

    .line 607
    const v6, 0x40490fdb    # (float)Math.PI

    iput v6, v4, Lsoftware/simplicial/a/bf;->M:F

    .line 609
    :cond_12
    iget v6, v5, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    const v8, 0x3f2aaaab

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    cmpg-float v6, v6, v3

    if-gez v6, :cond_13

    .line 611
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const v7, 0x3f2aaaab

    mul-float/2addr v6, v7

    add-float/2addr v3, v6

    iput v3, v5, Lsoftware/simplicial/a/bh;->m:F

    .line 612
    instance-of v3, v4, Lsoftware/simplicial/a/i;

    if-eqz v3, :cond_13

    .line 613
    const v3, 0x3fc90fdb

    iput v3, v4, Lsoftware/simplicial/a/bf;->M:F

    .line 615
    :cond_13
    iget v3, v5, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const v7, 0x3f2aaaab

    mul-float/2addr v6, v7

    add-float/2addr v3, v6

    cmpl-float v3, v3, v2

    if-lez v3, :cond_14

    .line 617
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v3

    const v6, 0x3f2aaaab

    mul-float/2addr v3, v6

    sub-float/2addr v2, v3

    iput v2, v5, Lsoftware/simplicial/a/bh;->m:F

    .line 618
    instance-of v2, v4, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_14

    .line 619
    const v2, 0x4096cbe4

    iput v2, v4, Lsoftware/simplicial/a/bf;->M:F

    .line 622
    :cond_14
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->i()V

    goto/16 :goto_b

    .line 592
    :cond_15
    const/4 v3, 0x0

    .line 593
    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v2, v3

    goto/16 :goto_c

    .line 625
    :cond_16
    return-void
.end method

.method private i(Lsoftware/simplicial/a/bf;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 6500
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    .line 6501
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_0

    .line 6502
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    .line 6504
    :cond_0
    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v3, :cond_1

    instance-of v2, p1, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_2

    .line 6506
    :cond_1
    iput-object v0, p1, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    move v0, v1

    .line 6517
    :goto_0
    return v0

    .line 6510
    :cond_2
    iget-object v2, p1, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    invoke-static {v0, v2}, Lsoftware/simplicial/a/af;->a(Lsoftware/simplicial/a/af;Ljava/util/Set;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 6512
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    .line 6513
    const/4 v0, 0x0

    goto :goto_0

    .line 6516
    :cond_3
    iput-object v0, p1, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    move v0, v1

    .line 6517
    goto :goto_0
.end method

.method private j(Lsoftware/simplicial/a/bf;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 6522
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    .line 6523
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_0

    .line 6524
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    .line 6526
    :cond_0
    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v3, :cond_1

    instance-of v2, p1, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_2

    .line 6528
    :cond_1
    iput-object v0, p1, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    move v0, v1

    .line 6539
    :goto_0
    return v0

    .line 6532
    :cond_2
    iget-object v2, p1, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    invoke-static {v0, v2}, Lsoftware/simplicial/a/as;->a(Lsoftware/simplicial/a/as;Ljava/util/Set;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 6534
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    .line 6535
    const/4 v0, 0x0

    goto :goto_0

    .line 6538
    :cond_3
    iput-object v0, p1, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    move v0, v1

    .line 6539
    goto :goto_0
.end method

.method private k(Lsoftware/simplicial/a/bf;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 6550
    iget-byte v0, p1, Lsoftware/simplicial/a/bf;->Z:B

    .line 6551
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_0

    .line 6552
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v0, v0, Lsoftware/simplicial/a/bd;->c:B

    .line 6554
    :cond_0
    iget-object v2, p1, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    invoke-static {v0, v2}, Lsoftware/simplicial/a/bd;->b(BLjava/util/Map;)Lsoftware/simplicial/a/bd;

    move-result-object v2

    .line 6555
    sget-object v3, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v3, v3, Lsoftware/simplicial/a/bd;->c:B

    if-eq v0, v3, :cond_1

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v3, v4, :cond_1

    instance-of v3, p1, Lsoftware/simplicial/a/i;

    if-eqz v3, :cond_3

    .line 6557
    :cond_1
    if-eqz v2, :cond_2

    .line 6558
    iput-object v2, p1, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    :goto_0
    move v0, v1

    .line 6571
    :goto_1
    return v0

    .line 6560
    :cond_2
    invoke-static {v0}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    goto :goto_0

    .line 6564
    :cond_3
    if-nez v2, :cond_4

    .line 6566
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    .line 6567
    const/4 v0, 0x0

    goto :goto_1

    .line 6570
    :cond_4
    iput-object v2, p1, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    move v0, v1

    .line 6571
    goto :goto_1
.end method

.method private l(Lsoftware/simplicial/a/bf;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 6576
    iget v1, p1, Lsoftware/simplicial/a/bf;->af:I

    .line 6577
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_0

    iget v2, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-gtz v2, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v2, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v2, :cond_0

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v2, v2, Lsoftware/simplicial/a/i/a;->c:Z

    if-eqz v2, :cond_1

    :cond_0
    move v1, v0

    .line 6580
    :cond_1
    if-eqz v1, :cond_3

    iget-boolean v2, p1, Lsoftware/simplicial/a/bf;->aw:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v3, :cond_2

    instance-of v2, p1, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_3

    .line 6582
    :cond_2
    invoke-static {v1}, Lsoftware/simplicial/a/ba;->c(I)I

    move-result v0

    iput v0, p1, Lsoftware/simplicial/a/bf;->ah:I

    .line 6583
    iget v0, p1, Lsoftware/simplicial/a/bf;->ah:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v0

    iput v0, p1, Lsoftware/simplicial/a/bf;->ai:I

    .line 6584
    const/4 v0, 0x1

    .line 6589
    :goto_0
    return v0

    .line 6587
    :cond_3
    iget v1, p1, Lsoftware/simplicial/a/bf;->ag:I

    iput v1, p1, Lsoftware/simplicial/a/bf;->ah:I

    .line 6588
    iget v1, p1, Lsoftware/simplicial/a/bf;->ah:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v1

    iput v1, p1, Lsoftware/simplicial/a/bf;->ai:I

    goto :goto_0
.end method

.method private m(Lsoftware/simplicial/a/bf;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6595
    iget v2, p1, Lsoftware/simplicial/a/bf;->ae:I

    .line 6596
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_0

    move v2, v0

    .line 6599
    :cond_0
    if-eqz v2, :cond_1

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v3, v4, :cond_1

    instance-of v3, p1, Lsoftware/simplicial/a/i;

    if-eqz v3, :cond_2

    .line 6601
    :cond_1
    iput v2, p1, Lsoftware/simplicial/a/bf;->ad:I

    move v0, v1

    .line 6612
    :goto_0
    return v0

    .line 6605
    :cond_2
    iget-object v3, p1, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 6607
    iput v0, p1, Lsoftware/simplicial/a/bf;->ad:I

    goto :goto_0

    .line 6611
    :cond_3
    iput v2, p1, Lsoftware/simplicial/a/bf;->ad:I

    move v0, v1

    .line 6612
    goto :goto_0
.end method

.method private n(Lsoftware/simplicial/a/bf;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6629
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v3, :cond_0

    instance-of v2, p1, Lsoftware/simplicial/a/i;

    if-eqz v2, :cond_1

    .line 6631
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->F:[B

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->E:[B

    move v0, v1

    .line 6642
    :goto_0
    return v0

    .line 6635
    :cond_1
    iget-boolean v2, p1, Lsoftware/simplicial/a/bf;->G:Z

    if-nez v2, :cond_2

    .line 6637
    new-array v1, v0, [B

    iput-object v1, p1, Lsoftware/simplicial/a/bf;->E:[B

    goto :goto_0

    .line 6641
    :cond_2
    iget-object v0, p1, Lsoftware/simplicial/a/bf;->F:[B

    iput-object v0, p1, Lsoftware/simplicial/a/bf;->E:[B

    move v0, v1

    .line 6642
    goto :goto_0
.end method

.method private q()V
    .locals 8

    .prologue
    const v7, 0x3fa0d97c

    const v6, -0x405f2684

    const/4 v5, 0x0

    .line 630
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget v0, p0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v2, v0, :cond_c

    .line 632
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v3, v0, v2

    .line 633
    invoke-virtual {v3}, Lsoftware/simplicial/a/ca;->c()V

    .line 635
    iget v0, v3, Lsoftware/simplicial/a/ca;->k:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_0

    .line 636
    iput v7, v3, Lsoftware/simplicial/a/ca;->k:F

    .line 637
    :cond_0
    iget v0, v3, Lsoftware/simplicial/a/ca;->k:F

    cmpg-float v0, v0, v6

    if-gez v0, :cond_1

    .line 638
    iput v6, v3, Lsoftware/simplicial/a/ca;->k:F

    .line 640
    :cond_1
    iget v0, v3, Lsoftware/simplicial/a/ca;->b:F

    cmpg-float v0, v0, v5

    if-ltz v0, :cond_2

    iget v0, v3, Lsoftware/simplicial/a/ca;->d:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_3

    .line 642
    :cond_2
    iget v0, v3, Lsoftware/simplicial/a/ca;->b:F

    iget v1, v3, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    neg-float v0, v0

    .line 643
    iget v1, v3, Lsoftware/simplicial/a/ca;->l:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->l:F

    .line 644
    iget v1, v3, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->b:F

    .line 645
    iget v1, v3, Lsoftware/simplicial/a/ca;->d:F

    add-float/2addr v0, v1

    iput v0, v3, Lsoftware/simplicial/a/ca;->d:F

    .line 646
    iget v0, v3, Lsoftware/simplicial/a/ca;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, v3, Lsoftware/simplicial/a/ca;->z:F

    .line 648
    :cond_3
    iget v0, v3, Lsoftware/simplicial/a/ca;->b:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_4

    iget v0, v3, Lsoftware/simplicial/a/ca;->d:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 650
    :cond_4
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v1, v3, Lsoftware/simplicial/a/ca;->b:F

    iget v4, v3, Lsoftware/simplicial/a/ca;->d:F

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    sub-float/2addr v0, v1

    .line 651
    iget v1, v3, Lsoftware/simplicial/a/ca;->l:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->l:F

    .line 652
    iget v1, v3, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->b:F

    .line 653
    iget v1, v3, Lsoftware/simplicial/a/ca;->d:F

    add-float/2addr v0, v1

    iput v0, v3, Lsoftware/simplicial/a/ca;->d:F

    .line 654
    iget v0, v3, Lsoftware/simplicial/a/ca;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    neg-float v0, v0

    iput v0, v3, Lsoftware/simplicial/a/ca;->z:F

    .line 656
    :cond_5
    iget v0, v3, Lsoftware/simplicial/a/ca;->c:F

    cmpg-float v0, v0, v5

    if-ltz v0, :cond_6

    iget v0, v3, Lsoftware/simplicial/a/ca;->e:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_7

    .line 658
    :cond_6
    iget v0, v3, Lsoftware/simplicial/a/ca;->c:F

    iget v1, v3, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    neg-float v0, v0

    .line 659
    iget v1, v3, Lsoftware/simplicial/a/ca;->m:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->m:F

    .line 660
    iget v1, v3, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->c:F

    .line 661
    iget v1, v3, Lsoftware/simplicial/a/ca;->e:F

    add-float/2addr v0, v1

    iput v0, v3, Lsoftware/simplicial/a/ca;->e:F

    .line 662
    iget v0, v3, Lsoftware/simplicial/a/ca;->A:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, v3, Lsoftware/simplicial/a/ca;->A:F

    .line 664
    :cond_7
    iget v0, v3, Lsoftware/simplicial/a/ca;->c:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_8

    iget v0, v3, Lsoftware/simplicial/a/ca;->e:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_9

    .line 666
    :cond_8
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v1, v3, Lsoftware/simplicial/a/ca;->c:F

    iget v4, v3, Lsoftware/simplicial/a/ca;->e:F

    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    sub-float/2addr v0, v1

    .line 667
    iget v1, v3, Lsoftware/simplicial/a/ca;->m:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->m:F

    .line 668
    iget v1, v3, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v1, v0

    iput v1, v3, Lsoftware/simplicial/a/ca;->c:F

    .line 669
    iget v1, v3, Lsoftware/simplicial/a/ca;->e:F

    add-float/2addr v0, v1

    iput v0, v3, Lsoftware/simplicial/a/ca;->e:F

    .line 670
    iget v0, v3, Lsoftware/simplicial/a/ca;->A:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    neg-float v0, v0

    iput v0, v3, Lsoftware/simplicial/a/ca;->A:F

    .line 673
    :cond_9
    iget-object v0, v3, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 674
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 676
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 677
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gtz v1, :cond_a

    .line 678
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 680
    :cond_a
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 683
    :cond_b
    invoke-virtual {v3}, Lsoftware/simplicial/a/ca;->i()V

    .line 630
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 685
    :cond_c
    return-void
.end method

.method private r()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 689
    move v1, v0

    move v2, v0

    move v3, v0

    .line 692
    :goto_0
    const/16 v4, 0x14

    if-ge v0, v4, :cond_0

    .line 694
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->x:[I

    aget v4, v4, v0

    add-int/2addr v3, v4

    .line 695
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->y:[I

    aget v4, v4, v0

    add-int/2addr v2, v4

    .line 696
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->z:[I

    aget v4, v4, v0

    add-int/2addr v1, v4

    .line 692
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 698
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/bs;->ay:I

    if-ge v3, v0, :cond_1

    iget v0, p0, Lsoftware/simplicial/a/bs;->aA:I

    mul-int/lit8 v0, v0, 0x2

    if-ge v2, v0, :cond_1

    iget v0, p0, Lsoftware/simplicial/a/bs;->az:I

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    if-lt v1, v0, :cond_2

    .line 700
    :cond_1
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HAX DETECTED! Dots: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lsoftware/simplicial/a/bs;->ay:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " SOs: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/a/bs;->aA:I

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " BHs: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/a/bs;->az:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 704
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 700
    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 706
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "HAX EXCEPTION"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 708
    :cond_2
    return-void
.end method

.method private s()V
    .locals 7

    .prologue
    const v5, 0x38d1b717    # 1.0E-4f

    const/4 v4, -0x4

    const/4 v3, 0x1

    const/16 v2, 0xb4

    const/4 v6, 0x0

    .line 712
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1

    .line 714
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v1, p0, Lsoftware/simplicial/a/bs;->o:I

    sub-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x14

    if-nez v0, :cond_1

    .line 715
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    add-int/lit8 v0, v0, -0x1

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 716
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_3

    .line 718
    :cond_2
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v1, 0x7fff

    if-ge v0, v1, :cond_3

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v1, p0, Lsoftware/simplicial/a/bs;->o:I

    sub-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x14

    if-nez v0, :cond_3

    .line 719
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    add-int/lit8 v0, v0, -0x1

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 721
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_4

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_4

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->y()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 722
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 723
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    if-eqz v0, :cond_5

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_5

    .line 724
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 725
    :cond_5
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v0, v6

    iget-byte v0, v0, Lsoftware/simplicial/a/bf;->R:B

    if-eqz v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v0, v0, v3

    iget-byte v0, v0, Lsoftware/simplicial/a/bf;->R:B

    if-nez v0, :cond_7

    .line 726
    :cond_6
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 728
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_9

    :cond_8
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_9

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->z()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 730
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 731
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 733
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_a

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_a

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->A()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 735
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 736
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 738
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_b

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v0, v0, v3

    iget v0, v0, Lsoftware/simplicial/a/bx;->f:I

    if-nez v0, :cond_b

    .line 740
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 741
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 743
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_c

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_c

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->B()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 745
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 746
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 748
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_d

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_d

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->C()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 750
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 751
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 753
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_e

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_e

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->D()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 755
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 756
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 758
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_f

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_f

    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->t()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 760
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v0, v0, 0x14

    iput v0, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 761
    iput-short v6, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 764
    :cond_f
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    iget-short v1, p0, Lsoftware/simplicial/a/bs;->r:S

    if-eq v0, v1, :cond_10

    .line 766
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-nez v0, :cond_11

    .line 767
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->E()V

    .line 816
    :cond_10
    :goto_0
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v0, v1, :cond_19

    move v0, v6

    .line 818
    :goto_1
    iget v1, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v0, v1, :cond_19

    .line 820
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v1, v1, v0

    .line 821
    iput v6, v1, Lsoftware/simplicial/a/bx;->e:I

    .line 822
    iput v6, v1, Lsoftware/simplicial/a/bx;->d:I

    .line 818
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 768
    :cond_11
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/4 v1, -0x5

    if-ne v0, v1, :cond_16

    .line 770
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v0, :cond_12

    .line 772
    iput-short v4, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 773
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget v1, v0, Lsoftware/simplicial/a/c/a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/c/a;->d:I

    .line 774
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget v0, v0, Lsoftware/simplicial/a/c/a;->d:I

    if-le v0, v2, :cond_10

    .line 775
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iput v2, v0, Lsoftware/simplicial/a/c/a;->d:I

    goto :goto_0

    .line 777
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_13

    .line 779
    iput-short v4, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 780
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v1, v0, Lsoftware/simplicial/a/b/a;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/b/a;->e:I

    .line 781
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v0, v0, Lsoftware/simplicial/a/b/a;->e:I

    if-le v0, v2, :cond_10

    .line 782
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iput v2, v0, Lsoftware/simplicial/a/b/a;->e:I

    goto :goto_0

    .line 784
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_14

    .line 786
    iput-short v4, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 787
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v1, v0, Lsoftware/simplicial/a/h/c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/h/c;->f:I

    .line 788
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v0, v0, Lsoftware/simplicial/a/h/c;->f:I

    if-le v0, v2, :cond_10

    .line 789
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iput v2, v0, Lsoftware/simplicial/a/h/c;->f:I

    goto :goto_0

    .line 791
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_15

    .line 793
    iput-short v4, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 794
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v1, v0, Lsoftware/simplicial/a/i/a;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/a/i/a;->m:I

    .line 795
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v0, v0, Lsoftware/simplicial/a/i/a;->m:I

    if-le v0, v2, :cond_10

    .line 797
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iput v2, v0, Lsoftware/simplicial/a/i/a;->m:I

    .line 798
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget v1, p0, Lsoftware/simplicial/a/bs;->aZ:I

    const/4 v2, -0x1

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v4, v4, Lsoftware/simplicial/a/i/a;->b:Z

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v5, v5, Lsoftware/simplicial/a/i/a;->c:Z

    invoke-interface/range {v0 .. v5}, Lsoftware/simplicial/a/al;->a(IIZZZ)V

    goto/16 :goto_0

    .line 802
    :cond_15
    invoke-direct {p0, v3, v3, v5}, Lsoftware/simplicial/a/bs;->a(ZZF)V

    goto/16 :goto_0

    .line 804
    :cond_16
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v1, -0x28

    if-ne v0, v1, :cond_17

    .line 806
    invoke-direct {p0, v6, v3, v5}, Lsoftware/simplicial/a/bs;->a(ZZF)V

    goto/16 :goto_0

    .line 808
    :cond_17
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v1, -0xd

    if-eq v0, v1, :cond_18

    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v1, -0x17

    if-ne v0, v1, :cond_10

    .line 810
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v0, v0, v6

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 811
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->t:S

    iput-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 812
    iput v6, p0, Lsoftware/simplicial/a/bs;->o:I

    goto/16 :goto_0

    .line 825
    :cond_19
    return-void
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 829
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    iget v0, v0, Lsoftware/simplicial/a/bf;->Q:I

    const/16 v1, 0x19

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 18

    .prologue
    .line 1388
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v3, :cond_7

    .line 1390
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v3, v2

    .line 1391
    invoke-virtual {v6}, Lsoftware/simplicial/a/g;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1388
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1395
    :cond_1
    add-int/lit8 v3, v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v3, v4, :cond_4

    .line 1397
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v4, v4, v3

    .line 1398
    invoke-virtual {v4}, Lsoftware/simplicial/a/g;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1395
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1401
    :cond_3
    invoke-virtual {v6, v4}, Lsoftware/simplicial/a/g;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1403
    iget v5, v6, Lsoftware/simplicial/a/g;->n:F

    iget v7, v4, Lsoftware/simplicial/a/g;->n:F

    add-float/2addr v5, v7

    invoke-virtual {v6}, Lsoftware/simplicial/a/g;->h()F

    move-result v7

    sub-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v5, v7

    .line 1404
    iget v7, v6, Lsoftware/simplicial/a/g;->l:F

    iget v8, v4, Lsoftware/simplicial/a/g;->l:F

    sub-float/2addr v7, v8

    .line 1405
    iget v8, v6, Lsoftware/simplicial/a/g;->m:F

    iget v9, v4, Lsoftware/simplicial/a/g;->m:F

    sub-float/2addr v8, v9

    .line 1406
    float-to-double v8, v8

    float-to-double v10, v7

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v7, v8

    .line 1407
    iget v8, v6, Lsoftware/simplicial/a/g;->l:F

    float-to-double v8, v8

    float-to-double v10, v5

    float-to-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, v6, Lsoftware/simplicial/a/g;->l:F

    .line 1408
    iget v8, v6, Lsoftware/simplicial/a/g;->m:F

    float-to-double v8, v8

    float-to-double v10, v5

    float-to-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, v6, Lsoftware/simplicial/a/g;->m:F

    .line 1409
    iget v8, v4, Lsoftware/simplicial/a/g;->l:F

    float-to-double v8, v8

    float-to-double v10, v5

    float-to-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, v4, Lsoftware/simplicial/a/g;->l:F

    .line 1410
    iget v8, v4, Lsoftware/simplicial/a/g;->m:F

    float-to-double v8, v8

    float-to-double v10, v5

    float-to-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    double-to-float v5, v8

    iput v5, v4, Lsoftware/simplicial/a/g;->m:F

    .line 1412
    iget v5, v6, Lsoftware/simplicial/a/g;->c:F

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v8, v8

    add-float/2addr v5, v8

    iput v5, v6, Lsoftware/simplicial/a/g;->c:F

    .line 1413
    iget v5, v6, Lsoftware/simplicial/a/g;->d:F

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v8, v8

    add-float/2addr v5, v8

    iput v5, v6, Lsoftware/simplicial/a/g;->d:F

    .line 1415
    iget v5, v4, Lsoftware/simplicial/a/g;->c:F

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v8, v8

    sub-float/2addr v5, v8

    iput v5, v4, Lsoftware/simplicial/a/g;->c:F

    .line 1416
    iget v5, v4, Lsoftware/simplicial/a/g;->d:F

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    float-to-double v10, v7

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v7, v8

    sub-float/2addr v5, v7

    iput v5, v4, Lsoftware/simplicial/a/g;->d:F

    goto/16 :goto_2

    .line 1420
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_0

    .line 1422
    const/4 v3, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v3, v4, :cond_0

    .line 1424
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v7, v4, v3

    .line 1425
    const/high16 v4, 0x3f000000    # 0.5f

    iget v5, v6, Lsoftware/simplicial/a/g;->n:F

    mul-float/2addr v4, v5

    invoke-virtual {v7, v6, v4}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1427
    invoke-virtual {v6}, Lsoftware/simplicial/a/g;->e()V

    .line 1428
    iget v4, v7, Lsoftware/simplicial/a/z;->d:F

    float-to-double v4, v4

    iget v8, v7, Lsoftware/simplicial/a/z;->d:F

    float-to-double v8, v8

    mul-double/2addr v4, v8

    const-wide v8, 0x4062c00000000000L    # 150.0

    sub-double/2addr v4, v8

    .line 1429
    const-wide/16 v8, 0x0

    cmpg-double v8, v4, v8

    if-gez v8, :cond_5

    .line 1430
    const-wide/16 v4, 0x0

    .line 1431
    :cond_5
    invoke-static {v4, v5}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, v7, Lsoftware/simplicial/a/z;->d:F

    .line 1432
    iget v4, v7, Lsoftware/simplicial/a/z;->d:F

    const/high16 v5, 0x41200000    # 10.0f

    cmpg-float v4, v4, v5

    if-gez v4, :cond_6

    .line 1433
    const/high16 v4, 0x41200000    # 10.0f

    iput v4, v7, Lsoftware/simplicial/a/z;->d:F

    .line 1422
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1440
    :cond_7
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v3, v2, :cond_19

    .line 1442
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v8, v2, v3

    .line 1445
    iget v2, v8, Lsoftware/simplicial/a/ag;->c:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x3c23d70a    # 0.01f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_9

    iget v2, v8, Lsoftware/simplicial/a/ag;->d:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x3c23d70a    # 0.01f

    cmpg-float v2, v2, v4

    if-gez v2, :cond_9

    .line 1440
    :cond_8
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 1449
    :cond_9
    iget-object v2, v8, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-nez v2, :cond_c

    iget-boolean v2, v8, Lsoftware/simplicial/a/ag;->j:Z

    if-nez v2, :cond_c

    .line 1451
    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v4, :cond_c

    .line 1453
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v4, v4, v2

    .line 1455
    iget-object v5, v4, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v6, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v5, v6, :cond_e

    iget v5, v8, Lsoftware/simplicial/a/ag;->n:F

    invoke-virtual {v8, v4, v5}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1457
    new-instance v5, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1458
    const/4 v2, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v6, :cond_b

    .line 1460
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    iget-object v6, v6, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v7, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v6, v7, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    if-eq v6, v4, :cond_a

    .line 1462
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1458
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1465
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/g;

    .line 1466
    iget v4, v2, Lsoftware/simplicial/a/g;->l:F

    iput v4, v8, Lsoftware/simplicial/a/ag;->l:F

    .line 1467
    iget v2, v2, Lsoftware/simplicial/a/g;->m:F

    iput v2, v8, Lsoftware/simplicial/a/ag;->m:F

    .line 1469
    const/4 v2, 0x1

    iput-boolean v2, v8, Lsoftware/simplicial/a/ag;->j:Z

    .line 1476
    :cond_c
    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v2, v4, :cond_8

    .line 1478
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v9, v4, v2

    .line 1480
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    invoke-interface {v4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1476
    :cond_d
    :goto_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 1451
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1483
    :cond_f
    iget v4, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v5, v8, Lsoftware/simplicial/a/ag;->m:F

    invoke-virtual {v9, v4, v5}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    .line 1485
    iget-object v5, v8, Lsoftware/simplicial/a/ag;->p:[Lsoftware/simplicial/a/cb;

    aget-object v5, v5, v2

    if-eq v4, v5, :cond_15

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_15

    iget-object v4, v8, Lsoftware/simplicial/a/ag;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, v4, v2

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_15

    const/4 v4, 0x1

    .line 1488
    :goto_9
    invoke-virtual {v9, v8}, Lsoftware/simplicial/a/ca;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-nez v5, :cond_10

    if-eqz v4, :cond_d

    .line 1491
    :cond_10
    iget v5, v9, Lsoftware/simplicial/a/ca;->d:F

    iget v6, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v6

    .line 1492
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    iget v7, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v6, v7

    .line 1493
    iget v7, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v10, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v7, v10

    .line 1494
    iget v10, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v10, v11

    .line 1495
    mul-float/2addr v7, v5

    mul-float/2addr v10, v6

    add-float/2addr v7, v10

    .line 1496
    mul-float v10, v5, v5

    mul-float v11, v6, v6

    add-float/2addr v10, v11

    .line 1497
    div-float v10, v7, v10

    .line 1501
    const/4 v7, 0x0

    cmpg-float v7, v10, v7

    if-gez v7, :cond_16

    .line 1503
    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    .line 1504
    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    .line 1505
    iget v5, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    .line 1521
    :cond_11
    :goto_a
    iget v11, v8, Lsoftware/simplicial/a/ag;->l:F

    sub-float v7, v11, v7

    .line 1522
    iget v11, v8, Lsoftware/simplicial/a/ag;->m:F

    sub-float v6, v11, v6

    .line 1524
    if-eqz v4, :cond_18

    .line 1526
    neg-float v7, v7

    .line 1527
    neg-float v6, v6

    .line 1528
    iget v4, v8, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v4, v5

    move v5, v6

    move v6, v7

    .line 1535
    :goto_b
    mul-float v7, v6, v6

    mul-float v11, v5, v5

    add-float/2addr v7, v11

    float-to-double v12, v7

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 1536
    const v11, 0x3c23d70a    # 0.01f

    cmpg-float v11, v7, v11

    if-gez v11, :cond_12

    .line 1537
    const v7, 0x3c23d70a    # 0.01f

    .line 1538
    :cond_12
    div-float/2addr v6, v7

    .line 1539
    div-float v11, v5, v7

    .line 1540
    iget v5, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->l:F

    sub-float/2addr v5, v12

    .line 1541
    iget v12, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->m:F

    sub-float/2addr v12, v13

    .line 1542
    iget v13, v8, Lsoftware/simplicial/a/ag;->d:F

    mul-float/2addr v5, v13

    iget v13, v8, Lsoftware/simplicial/a/ag;->c:F

    mul-float/2addr v12, v13

    sub-float v12, v5, v12

    .line 1543
    iget v5, v8, Lsoftware/simplicial/a/ag;->c:F

    mul-float/2addr v5, v6

    iget v13, v8, Lsoftware/simplicial/a/ag;->d:F

    mul-float/2addr v13, v11

    add-float/2addr v13, v5

    .line 1544
    float-to-double v14, v10

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    sub-double v14, v14, v16

    float-to-double v0, v13

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v7

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    double-to-float v5, v14

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v5, v7

    .line 1545
    const/4 v7, 0x0

    cmpg-float v7, v12, v7

    if-gez v7, :cond_13

    .line 1546
    neg-float v5, v5

    .line 1548
    :cond_13
    const/high16 v7, 0x3f800000    # 1.0f

    iget v10, v8, Lsoftware/simplicial/a/ag;->n:F

    const v12, 0x417a6666    # 15.65f

    div-float/2addr v10, v12

    invoke-static {v7, v10}, Ljava/lang/Math;->min(FF)F

    move-result v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v7, v10

    .line 1551
    iget v10, v9, Lsoftware/simplicial/a/ca;->z:F

    iget v12, v8, Lsoftware/simplicial/a/ag;->c:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->z:F

    .line 1552
    iget v10, v9, Lsoftware/simplicial/a/ca;->A:F

    iget v12, v8, Lsoftware/simplicial/a/ag;->d:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->A:F

    .line 1555
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v6

    iget v12, v8, Lsoftware/simplicial/a/ag;->c:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/ag;->c:F

    .line 1556
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v11

    iget v12, v8, Lsoftware/simplicial/a/ag;->d:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/ag;->d:F

    .line 1559
    const/4 v10, 0x0

    cmpl-float v10, v4, v10

    if-lez v10, :cond_14

    .line 1561
    iget v10, v8, Lsoftware/simplicial/a/ag;->l:F

    mul-float/2addr v6, v4

    add-float/2addr v6, v10

    iput v6, v8, Lsoftware/simplicial/a/ag;->l:F

    .line 1562
    iget v6, v8, Lsoftware/simplicial/a/ag;->m:F

    mul-float/2addr v4, v11

    add-float/2addr v4, v6

    iput v4, v8, Lsoftware/simplicial/a/ag;->m:F

    .line 1566
    :cond_14
    iget v4, v9, Lsoftware/simplicial/a/ca;->k:F

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iput v4, v9, Lsoftware/simplicial/a/ca;->k:F

    .line 1568
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 1485
    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 1507
    :cond_16
    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v10, v7

    if-lez v7, :cond_17

    .line 1509
    iget v7, v9, Lsoftware/simplicial/a/ca;->d:F

    .line 1510
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    .line 1511
    iget v5, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/ag;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    goto/16 :goto_a

    .line 1515
    :cond_17
    mul-float/2addr v5, v10

    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v7, v5

    .line 1516
    mul-float v5, v10, v6

    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v6, v5

    .line 1517
    invoke-virtual {v9}, Lsoftware/simplicial/a/ca;->h()F

    move-result v5

    .line 1518
    invoke-static {v5}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 1519
    iget v5, v8, Lsoftware/simplicial/a/ag;->l:F

    iget v11, v8, Lsoftware/simplicial/a/ag;->m:F

    invoke-virtual {v9, v5, v11}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v5

    goto/16 :goto_a

    .line 1531
    :cond_18
    iget v4, v8, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v4, v5

    move v5, v6

    move v6, v7

    goto/16 :goto_b

    .line 1574
    :cond_19
    const/4 v2, 0x0

    move v3, v2

    :goto_c
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v3, v2, :cond_30

    .line 1576
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v8, v2, v3

    .line 1578
    iget-boolean v2, v8, Lsoftware/simplicial/a/bl;->C:Z

    if-nez v2, :cond_1a

    iget-object v2, v8, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    sget-object v4, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    if-eq v2, v4, :cond_1c

    .line 1581
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v2, v4, :cond_2f

    .line 1583
    const/4 v2, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v2, v4, :cond_2f

    .line 1585
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v4, v4, v2

    .line 1586
    const/high16 v5, 0x3f000000    # 0.5f

    iget v6, v8, Lsoftware/simplicial/a/bl;->n:F

    mul-float/2addr v5, v6

    invoke-virtual {v4, v8, v5}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1588
    iget v5, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v6, v4, Lsoftware/simplicial/a/z;->m:F

    sub-float/2addr v5, v6

    float-to-double v6, v5

    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v9, v4, Lsoftware/simplicial/a/z;->l:F

    sub-float/2addr v5, v9

    float-to-double v10, v5

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    .line 1589
    iget v5, v4, Lsoftware/simplicial/a/z;->n:F

    const/high16 v9, 0x3f000000    # 0.5f

    iget v10, v8, Lsoftware/simplicial/a/bl;->n:F

    mul-float/2addr v9, v10

    add-float/2addr v5, v9

    const v9, 0x3a83126f    # 0.001f

    add-float/2addr v5, v9

    .line 1590
    iget v9, v4, Lsoftware/simplicial/a/z;->l:F

    float-to-double v10, v9

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    float-to-double v14, v5

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    double-to-float v9, v10

    iput v9, v8, Lsoftware/simplicial/a/bl;->l:F

    .line 1591
    iget v4, v4, Lsoftware/simplicial/a/z;->m:F

    float-to-double v10, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    float-to-double v4, v5

    mul-double/2addr v4, v6

    add-double/2addr v4, v10

    double-to-float v4, v4

    iput v4, v8, Lsoftware/simplicial/a/bl;->m:F

    .line 1583
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 1601
    :cond_1c
    iget-object v2, v8, Lsoftware/simplicial/a/bl;->h:Lsoftware/simplicial/a/bh;

    if-nez v2, :cond_1f

    iget-boolean v2, v8, Lsoftware/simplicial/a/bl;->j:Z

    if-nez v2, :cond_1f

    iget-object v2, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v4, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_1f

    .line 1603
    const/4 v2, 0x0

    :goto_e
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v4, :cond_1f

    .line 1605
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v4, v4, v2

    .line 1607
    iget-object v5, v4, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v6, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-eq v5, v6, :cond_1d

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->i:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_24

    :cond_1d
    iget v5, v8, Lsoftware/simplicial/a/bl;->n:F

    invoke-virtual {v8, v4, v5}, Lsoftware/simplicial/a/bl;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1609
    iget-object v2, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v5, Lsoftware/simplicial/a/bl$b;->i:Lsoftware/simplicial/a/bl$b;

    if-ne v2, v5, :cond_21

    .line 1611
    iget-object v2, v8, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    if-nez v2, :cond_1e

    .line 1612
    iput-object v4, v8, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 1629
    :cond_1e
    :goto_f
    const/4 v2, 0x1

    iput-boolean v2, v8, Lsoftware/simplicial/a/bl;->j:Z

    .line 1636
    :cond_1f
    iget-object v2, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v4, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-eq v2, v4, :cond_2f

    .line 1638
    const/4 v2, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v2, v4, :cond_2f

    .line 1640
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v9, v4, v2

    .line 1642
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    invoke-interface {v4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 1638
    :cond_20
    :goto_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    .line 1614
    :cond_21
    iget-object v2, v4, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v5, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v2, v5, :cond_1e

    .line 1616
    new-instance v5, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1617
    const/4 v2, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v2, v6, :cond_23

    .line 1619
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    iget-object v6, v6, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v7, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v6, v7, :cond_22

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    if-eq v6, v4, :cond_22

    .line 1621
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v2

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1617
    :cond_22
    add-int/lit8 v2, v2, 0x1

    goto :goto_12

    .line 1624
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/g;

    .line 1625
    iget v4, v2, Lsoftware/simplicial/a/g;->l:F

    iput v4, v8, Lsoftware/simplicial/a/bl;->l:F

    .line 1626
    iget v2, v2, Lsoftware/simplicial/a/g;->m:F

    iput v2, v8, Lsoftware/simplicial/a/bl;->m:F

    .line 1627
    const/4 v2, 0x1

    iput-boolean v2, v8, Lsoftware/simplicial/a/bl;->E:Z

    goto :goto_f

    .line 1603
    :cond_24
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_e

    .line 1645
    :cond_25
    iget v4, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v5, v8, Lsoftware/simplicial/a/bl;->m:F

    invoke-virtual {v9, v4, v5}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    .line 1647
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->p:[Lsoftware/simplicial/a/cb;

    aget-object v5, v5, v2

    if-eq v4, v5, :cond_2b

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_2b

    iget-object v4, v8, Lsoftware/simplicial/a/bl;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, v4, v2

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_2b

    const/4 v4, 0x1

    .line 1649
    :goto_13
    invoke-virtual {v9, v8}, Lsoftware/simplicial/a/ca;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-nez v5, :cond_26

    if-eqz v4, :cond_20

    .line 1652
    :cond_26
    iget v5, v9, Lsoftware/simplicial/a/ca;->d:F

    iget v6, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v6

    .line 1653
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    iget v7, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v6, v7

    .line 1654
    iget v7, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v10, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v7, v10

    .line 1655
    iget v10, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v10, v11

    .line 1656
    mul-float/2addr v7, v5

    mul-float/2addr v10, v6

    add-float/2addr v7, v10

    .line 1657
    mul-float v10, v5, v5

    mul-float v11, v6, v6

    add-float/2addr v10, v11

    .line 1658
    div-float v10, v7, v10

    .line 1662
    const/4 v7, 0x0

    cmpg-float v7, v10, v7

    if-gez v7, :cond_2c

    .line 1664
    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    .line 1665
    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    .line 1666
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    .line 1683
    :cond_27
    :goto_14
    iget v11, v8, Lsoftware/simplicial/a/bl;->l:F

    sub-float v7, v11, v7

    .line 1684
    iget v11, v8, Lsoftware/simplicial/a/bl;->m:F

    sub-float v6, v11, v6

    .line 1686
    if-eqz v4, :cond_2e

    .line 1688
    neg-float v7, v7

    .line 1689
    neg-float v6, v6

    .line 1690
    iget v4, v8, Lsoftware/simplicial/a/bl;->n:F

    add-float/2addr v4, v5

    move v5, v6

    move v6, v7

    .line 1696
    :goto_15
    mul-float v7, v6, v6

    mul-float v11, v5, v5

    add-float/2addr v7, v11

    float-to-double v12, v7

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 1697
    const v11, 0x3c23d70a    # 0.01f

    cmpg-float v11, v7, v11

    if-gez v11, :cond_28

    .line 1698
    const v7, 0x3c23d70a    # 0.01f

    .line 1699
    :cond_28
    div-float/2addr v6, v7

    .line 1700
    div-float v11, v5, v7

    .line 1701
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->l:F

    sub-float/2addr v5, v12

    .line 1702
    iget v12, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->m:F

    sub-float/2addr v12, v13

    .line 1703
    iget v13, v8, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v5, v13

    iget v13, v8, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v12, v13

    sub-float v12, v5, v12

    .line 1704
    iget v5, v8, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v5, v6

    iget v13, v8, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v13, v11

    add-float/2addr v13, v5

    .line 1705
    float-to-double v14, v10

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    sub-double v14, v14, v16

    float-to-double v0, v13

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v7

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    double-to-float v5, v14

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v5, v7

    .line 1706
    const/4 v7, 0x0

    cmpg-float v7, v12, v7

    if-gez v7, :cond_29

    .line 1707
    neg-float v5, v5

    .line 1709
    :cond_29
    const/high16 v7, 0x3f800000    # 1.0f

    iget v10, v8, Lsoftware/simplicial/a/bl;->n:F

    const v12, 0x417a6666    # 15.65f

    div-float/2addr v10, v12

    invoke-static {v7, v10}, Ljava/lang/Math;->min(FF)F

    move-result v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v7, v10

    .line 1712
    iget v10, v9, Lsoftware/simplicial/a/ca;->z:F

    iget v12, v8, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->z:F

    .line 1713
    iget v10, v9, Lsoftware/simplicial/a/ca;->A:F

    iget v12, v8, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->A:F

    .line 1716
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v6

    iget v12, v8, Lsoftware/simplicial/a/bl;->c:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/bl;->c:F

    .line 1717
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v11

    iget v12, v8, Lsoftware/simplicial/a/bl;->d:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/bl;->d:F

    .line 1720
    const/4 v10, 0x0

    cmpl-float v10, v4, v10

    if-lez v10, :cond_2a

    .line 1722
    iget v10, v8, Lsoftware/simplicial/a/bl;->l:F

    mul-float/2addr v6, v4

    add-float/2addr v6, v10

    iput v6, v8, Lsoftware/simplicial/a/bl;->l:F

    .line 1723
    iget v6, v8, Lsoftware/simplicial/a/bl;->m:F

    mul-float/2addr v4, v11

    add-float/2addr v4, v6

    iput v4, v8, Lsoftware/simplicial/a/bl;->m:F

    .line 1727
    :cond_2a
    iget v4, v9, Lsoftware/simplicial/a/ca;->k:F

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iput v4, v9, Lsoftware/simplicial/a/ca;->k:F

    .line 1729
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_11

    .line 1647
    :cond_2b
    const/4 v4, 0x0

    goto/16 :goto_13

    .line 1668
    :cond_2c
    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v10, v7

    if-lez v7, :cond_2d

    .line 1670
    iget v7, v9, Lsoftware/simplicial/a/ca;->d:F

    .line 1671
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    .line 1672
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    goto/16 :goto_14

    .line 1676
    :cond_2d
    mul-float/2addr v5, v10

    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v7, v5

    .line 1677
    mul-float v5, v10, v6

    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v6, v5

    .line 1678
    invoke-virtual {v9}, Lsoftware/simplicial/a/ca;->h()F

    move-result v5

    .line 1679
    invoke-static {v5}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v11

    if-eqz v11, :cond_27

    .line 1680
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v11, v8, Lsoftware/simplicial/a/bl;->m:F

    invoke-virtual {v9, v5, v11}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v5

    goto/16 :goto_14

    .line 1693
    :cond_2e
    iget v4, v8, Lsoftware/simplicial/a/bl;->n:F

    sub-float/2addr v4, v5

    move v5, v6

    move v6, v7

    goto/16 :goto_15

    .line 1574
    :cond_2f
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_c

    .line 1736
    :cond_30
    const/4 v2, 0x0

    :goto_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v3, v3

    if-ge v2, v3, :cond_46

    .line 1738
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v8, v3, v2

    .line 1741
    iget-boolean v3, v8, Lsoftware/simplicial/a/bq;->F:Z

    if-nez v3, :cond_31

    iget-object v3, v8, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    if-eq v3, v4, :cond_32

    .line 1736
    :cond_31
    add-int/lit8 v2, v2, 0x1

    goto :goto_16

    .line 1745
    :cond_32
    iget-boolean v3, v8, Lsoftware/simplicial/a/bq;->D:Z

    if-nez v3, :cond_33

    .line 1747
    const/4 v3, 0x0

    :goto_17
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v3, v4, :cond_33

    .line 1749
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v4, v4, v3

    .line 1751
    invoke-virtual {v8, v4}, Lsoftware/simplicial/a/bq;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-eqz v5, :cond_35

    .line 1753
    iget v3, v8, Lsoftware/simplicial/a/bq;->a:F

    iget v5, v8, Lsoftware/simplicial/a/bq;->a:F

    mul-float/2addr v3, v5

    iget v5, v8, Lsoftware/simplicial/a/bq;->b:F

    iget v6, v8, Lsoftware/simplicial/a/bq;->b:F

    mul-float/2addr v5, v6

    add-float/2addr v3, v5

    float-to-double v6, v3

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    double-to-float v3, v6

    .line 1754
    invoke-virtual {v8}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v3, v5

    const v5, 0x44a28000    # 1300.0f

    div-float/2addr v3, v5

    .line 1755
    iget v5, v4, Lsoftware/simplicial/a/g;->c:F

    iget v6, v8, Lsoftware/simplicial/a/bq;->a:F

    mul-float/2addr v6, v3

    add-float/2addr v5, v6

    iput v5, v4, Lsoftware/simplicial/a/g;->c:F

    .line 1756
    iget v5, v4, Lsoftware/simplicial/a/g;->d:F

    iget v6, v8, Lsoftware/simplicial/a/bq;->b:F

    mul-float/2addr v3, v6

    add-float/2addr v3, v5

    iput v3, v4, Lsoftware/simplicial/a/g;->d:F

    .line 1757
    iget v3, v8, Lsoftware/simplicial/a/bq;->A:I

    iput v3, v4, Lsoftware/simplicial/a/g;->g:I

    .line 1759
    const/4 v3, 0x1

    iput-boolean v3, v8, Lsoftware/simplicial/a/bq;->D:Z

    .line 1760
    const/4 v3, 0x1

    iput-boolean v3, v8, Lsoftware/simplicial/a/bq;->G:Z

    .line 1767
    :cond_33
    iget-boolean v3, v8, Lsoftware/simplicial/a/bq;->E:Z

    if-nez v3, :cond_38

    .line 1769
    const/4 v3, 0x0

    :goto_18
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v3, v4, :cond_38

    .line 1771
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v4, v4, v3

    .line 1772
    iget-object v5, v4, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    sget-object v6, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    if-eq v5, v6, :cond_36

    .line 1769
    :cond_34
    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    .line 1747
    :cond_35
    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    .line 1775
    :cond_36
    invoke-virtual {v8, v4}, Lsoftware/simplicial/a/bq;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-eqz v5, :cond_34

    .line 1777
    iget v3, v4, Lsoftware/simplicial/a/bl;->c:F

    iget v5, v8, Lsoftware/simplicial/a/bq;->a:F

    add-float/2addr v3, v5

    iput v3, v4, Lsoftware/simplicial/a/bl;->c:F

    .line 1778
    iget v3, v4, Lsoftware/simplicial/a/bl;->d:F

    iget v5, v8, Lsoftware/simplicial/a/bq;->b:F

    add-float/2addr v3, v5

    iput v3, v4, Lsoftware/simplicial/a/bl;->d:F

    .line 1779
    iget v3, v4, Lsoftware/simplicial/a/bl;->c:F

    iget v5, v4, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v3, v5

    iget v5, v4, Lsoftware/simplicial/a/bl;->d:F

    iget v6, v4, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v5, v6

    add-float/2addr v3, v5

    .line 1780
    const v5, 0x46fd2000    # 32400.0f

    cmpl-float v3, v3, v5

    if-lez v3, :cond_37

    .line 1782
    iget v3, v4, Lsoftware/simplicial/a/bl;->d:F

    float-to-double v6, v3

    iget v3, v4, Lsoftware/simplicial/a/bl;->c:F

    float-to-double v10, v3

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v3, v6

    .line 1783
    const-wide v6, 0x4066800000000000L    # 180.0

    float-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v6, v10

    double-to-float v5, v6

    iput v5, v4, Lsoftware/simplicial/a/bl;->c:F

    .line 1784
    const-wide v6, 0x4066800000000000L    # 180.0

    float-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v6, v10

    double-to-float v3, v6

    iput v3, v4, Lsoftware/simplicial/a/bl;->d:F

    .line 1786
    :cond_37
    const/4 v3, 0x0

    iput v3, v4, Lsoftware/simplicial/a/bl;->i:F

    .line 1788
    const/4 v3, 0x1

    iput-boolean v3, v8, Lsoftware/simplicial/a/bq;->E:Z

    .line 1789
    const/4 v3, 0x1

    iput-boolean v3, v8, Lsoftware/simplicial/a/bq;->G:Z

    .line 1795
    :cond_38
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_3a

    .line 1797
    const/4 v3, 0x0

    :goto_19
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v3, v4, :cond_3a

    .line 1799
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v4, v4, v3

    .line 1800
    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v8}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v6

    mul-float/2addr v5, v6

    invoke-virtual {v4, v8, v5}, Lsoftware/simplicial/a/z;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_39

    .line 1802
    invoke-virtual {v8}, Lsoftware/simplicial/a/bq;->d()F

    move-result v5

    .line 1803
    sget-object v6, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v6, v8, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 1804
    const/4 v6, 0x1

    iput-boolean v6, v8, Lsoftware/simplicial/a/bq;->G:Z

    .line 1805
    iget v6, v4, Lsoftware/simplicial/a/z;->d:F

    float-to-double v6, v6

    iget v9, v4, Lsoftware/simplicial/a/z;->d:F

    float-to-double v10, v9

    mul-double/2addr v6, v10

    float-to-double v10, v5

    add-double/2addr v6, v10

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    double-to-float v5, v6

    iput v5, v4, Lsoftware/simplicial/a/z;->d:F

    .line 1797
    :cond_39
    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    .line 1812
    :cond_3a
    const/4 v3, 0x0

    :goto_1a
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v3, v4, :cond_31

    .line 1814
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v9, v4, v3

    .line 1816
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    invoke-interface {v4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 1812
    :cond_3b
    :goto_1b
    add-int/lit8 v3, v3, 0x1

    goto :goto_1a

    .line 1819
    :cond_3c
    iget v4, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v5, v8, Lsoftware/simplicial/a/bq;->m:F

    invoke-virtual {v9, v4, v5}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    .line 1821
    iget-object v5, v8, Lsoftware/simplicial/a/bq;->p:[Lsoftware/simplicial/a/cb;

    aget-object v5, v5, v3

    if-eq v4, v5, :cond_42

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_42

    iget-object v4, v8, Lsoftware/simplicial/a/bq;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, v4, v3

    sget-object v5, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v4, v5, :cond_42

    const/4 v4, 0x1

    .line 1823
    :goto_1c
    invoke-virtual {v9, v8}, Lsoftware/simplicial/a/ca;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    if-nez v5, :cond_3d

    if-eqz v4, :cond_3b

    .line 1826
    :cond_3d
    iget v5, v9, Lsoftware/simplicial/a/ca;->d:F

    iget v6, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v6

    .line 1827
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    iget v7, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v6, v7

    .line 1828
    iget v7, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v10, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v7, v10

    .line 1829
    iget v10, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v10, v11

    .line 1830
    mul-float/2addr v7, v5

    mul-float/2addr v10, v6

    add-float/2addr v7, v10

    .line 1831
    mul-float v10, v5, v5

    mul-float v11, v6, v6

    add-float/2addr v10, v11

    .line 1832
    div-float v10, v7, v10

    .line 1836
    const/4 v7, 0x0

    cmpg-float v7, v10, v7

    if-gez v7, :cond_43

    .line 1838
    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    .line 1839
    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    .line 1840
    iget v5, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    .line 1857
    :cond_3e
    :goto_1d
    iget v11, v8, Lsoftware/simplicial/a/bq;->l:F

    sub-float v7, v11, v7

    .line 1858
    iget v11, v8, Lsoftware/simplicial/a/bq;->m:F

    sub-float v6, v11, v6

    .line 1860
    if-eqz v4, :cond_45

    .line 1862
    neg-float v7, v7

    .line 1863
    neg-float v6, v6

    .line 1864
    iget v4, v8, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v4, v5

    move v5, v6

    move v6, v7

    .line 1870
    :goto_1e
    mul-float v7, v6, v6

    mul-float v11, v5, v5

    add-float/2addr v7, v11

    float-to-double v12, v7

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 1871
    const v11, 0x3c23d70a    # 0.01f

    cmpg-float v11, v7, v11

    if-gez v11, :cond_3f

    .line 1872
    const v7, 0x3c23d70a    # 0.01f

    .line 1873
    :cond_3f
    div-float/2addr v6, v7

    .line 1874
    div-float v11, v5, v7

    .line 1875
    iget v5, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->l:F

    sub-float/2addr v5, v12

    .line 1876
    iget v12, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->m:F

    sub-float/2addr v12, v13

    .line 1877
    iget v13, v8, Lsoftware/simplicial/a/bq;->b:F

    mul-float/2addr v5, v13

    iget v13, v8, Lsoftware/simplicial/a/bq;->a:F

    mul-float/2addr v12, v13

    sub-float v12, v5, v12

    .line 1878
    iget v5, v8, Lsoftware/simplicial/a/bq;->a:F

    mul-float/2addr v5, v6

    iget v13, v8, Lsoftware/simplicial/a/bq;->b:F

    mul-float/2addr v13, v11

    add-float/2addr v13, v5

    .line 1879
    float-to-double v14, v10

    const-wide/high16 v16, 0x3fe0000000000000L    # 0.5

    sub-double v14, v14, v16

    float-to-double v0, v13

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    float-to-double v0, v7

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    double-to-float v5, v14

    const/high16 v7, 0x42c80000    # 100.0f

    div-float/2addr v5, v7

    .line 1880
    const/4 v7, 0x0

    cmpg-float v7, v12, v7

    if-gez v7, :cond_40

    .line 1881
    neg-float v5, v5

    .line 1883
    :cond_40
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v8}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v10

    const v12, 0x417a6666    # 15.65f

    div-float/2addr v10, v12

    invoke-static {v7, v10}, Ljava/lang/Math;->min(FF)F

    move-result v7

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v7, v10

    .line 1886
    iget v10, v9, Lsoftware/simplicial/a/ca;->z:F

    iget v12, v8, Lsoftware/simplicial/a/bq;->a:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->z:F

    .line 1887
    iget v10, v9, Lsoftware/simplicial/a/ca;->A:F

    iget v12, v8, Lsoftware/simplicial/a/bq;->b:F

    mul-float/2addr v12, v7

    add-float/2addr v10, v12

    iput v10, v9, Lsoftware/simplicial/a/ca;->A:F

    .line 1890
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v6

    iget v12, v8, Lsoftware/simplicial/a/bq;->a:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/bq;->a:F

    .line 1891
    const/high16 v10, -0x40000000    # -2.0f

    mul-float/2addr v10, v13

    mul-float/2addr v10, v11

    iget v12, v8, Lsoftware/simplicial/a/bq;->b:F

    add-float/2addr v10, v12

    iput v10, v8, Lsoftware/simplicial/a/bq;->b:F

    .line 1894
    const/4 v10, 0x0

    cmpl-float v10, v4, v10

    if-lez v10, :cond_41

    .line 1896
    iget v10, v8, Lsoftware/simplicial/a/bq;->l:F

    mul-float/2addr v6, v4

    add-float/2addr v6, v10

    iput v6, v8, Lsoftware/simplicial/a/bq;->l:F

    .line 1897
    iget v6, v8, Lsoftware/simplicial/a/bq;->m:F

    mul-float/2addr v4, v11

    add-float/2addr v4, v6

    iput v4, v8, Lsoftware/simplicial/a/bq;->m:F

    .line 1901
    :cond_41
    iget v4, v9, Lsoftware/simplicial/a/ca;->k:F

    mul-float/2addr v5, v7

    add-float/2addr v4, v5

    iput v4, v9, Lsoftware/simplicial/a/ca;->k:F

    .line 1903
    iget-object v4, v9, Lsoftware/simplicial/a/ca;->B:Ljava/util/Map;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1b

    .line 1821
    :cond_42
    const/4 v4, 0x0

    goto/16 :goto_1c

    .line 1842
    :cond_43
    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v7, v10, v7

    if-lez v7, :cond_44

    .line 1844
    iget v7, v9, Lsoftware/simplicial/a/ca;->d:F

    .line 1845
    iget v6, v9, Lsoftware/simplicial/a/ca;->e:F

    .line 1846
    iget v5, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v11, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v11, v12

    mul-float/2addr v5, v11

    iget v11, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v12, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v11, v12

    iget v12, v8, Lsoftware/simplicial/a/bq;->m:F

    iget v13, v9, Lsoftware/simplicial/a/ca;->e:F

    sub-float/2addr v12, v13

    mul-float/2addr v11, v12

    add-float/2addr v5, v11

    float-to-double v12, v5

    invoke-static {v12, v13}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v12

    double-to-float v5, v12

    goto/16 :goto_1d

    .line 1850
    :cond_44
    mul-float/2addr v5, v10

    iget v7, v9, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v7, v5

    .line 1851
    mul-float v5, v10, v6

    iget v6, v9, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v6, v5

    .line 1852
    invoke-virtual {v9}, Lsoftware/simplicial/a/ca;->h()F

    move-result v5

    .line 1853
    invoke-static {v5}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v11

    if-eqz v11, :cond_3e

    .line 1854
    iget v5, v8, Lsoftware/simplicial/a/bq;->l:F

    iget v11, v8, Lsoftware/simplicial/a/bq;->m:F

    invoke-virtual {v9, v5, v11}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v5

    goto/16 :goto_1d

    .line 1867
    :cond_45
    iget v4, v8, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v4, v5

    move v5, v6

    move v6, v7

    goto/16 :goto_1e

    .line 1907
    :cond_46
    return-void
.end method

.method private v()V
    .locals 22

    .prologue
    .line 2172
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_1

    .line 2174
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aE:F

    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v2, v3

    .line 2175
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v2, v3

    move v11, v2

    move v12, v3

    .line 2185
    :goto_0
    const/4 v2, 0x0

    move v13, v2

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v13, v2, :cond_21

    .line 2187
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v2, v13

    .line 2189
    iget-boolean v2, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v3, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v2, :cond_2

    .line 2185
    :cond_0
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_1

    .line 2179
    :cond_1
    const/4 v3, 0x0

    .line 2180
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v2, v3

    move v11, v2

    move v12, v3

    goto :goto_0

    .line 2192
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;)V

    .line 2193
    iget v2, v3, Lsoftware/simplicial/a/bf;->M:F

    iput v2, v3, Lsoftware/simplicial/a/bf;->N:F

    .line 2194
    iget v2, v3, Lsoftware/simplicial/a/bf;->O:F

    iput v2, v3, Lsoftware/simplicial/a/bf;->P:F

    .line 2195
    iget v2, v3, Lsoftware/simplicial/a/bf;->T:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 2197
    iget v2, v3, Lsoftware/simplicial/a/bf;->T:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ab:F

    sub-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/bf;->T:F

    .line 2198
    iget v2, v3, Lsoftware/simplicial/a/bf;->T:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_3

    iget v2, v3, Lsoftware/simplicial/a/bf;->S:I

    const/4 v4, 0x1

    if-gt v2, v4, :cond_4

    .line 2199
    :cond_3
    const/4 v2, 0x0

    iput v2, v3, Lsoftware/simplicial/a/bf;->T:F

    .line 2201
    :cond_4
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    const/4 v4, 0x0

    cmpl-float v2, v2, v4

    if-lez v2, :cond_5

    .line 2203
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ab:F

    sub-float/2addr v2, v4

    iput v2, v3, Lsoftware/simplicial/a/bf;->al:F

    .line 2204
    iget v2, v3, Lsoftware/simplicial/a/bf;->al:F

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_5

    .line 2205
    const/4 v2, 0x0

    iput v2, v3, Lsoftware/simplicial/a/bf;->al:F

    .line 2208
    :cond_5
    const/4 v2, 0x0

    move v14, v2

    :goto_2
    iget v2, v3, Lsoftware/simplicial/a/bf;->S:I

    if-ge v14, v2, :cond_0

    .line 2210
    iget-object v2, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v4, v2, v14

    .line 2211
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2208
    :goto_3
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_2

    .line 2214
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->ab:F

    invoke-virtual {v4, v2}, Lsoftware/simplicial/a/bh;->e(F)V

    .line 2216
    iget v2, v4, Lsoftware/simplicial/a/bh;->Q:F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ab:F

    sub-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->Q:F

    .line 2217
    iget v2, v4, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v5, 0x0

    cmpg-float v2, v2, v5

    if-gez v2, :cond_7

    .line 2218
    const/4 v2, 0x0

    iput v2, v4, Lsoftware/simplicial/a/bh;->Q:F

    .line 2220
    :cond_7
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    if-nez v2, :cond_8

    .line 2222
    const/4 v2, -0x1

    iput-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    .line 2224
    iget v5, v3, Lsoftware/simplicial/a/bf;->M:F

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->ab:F

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 2225
    invoke-virtual {v3}, Lsoftware/simplicial/a/bf;->n()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 2227
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    .line 2235
    :cond_8
    :goto_4
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->A:B

    if-nez v2, :cond_9

    .line 2237
    const/4 v2, -0x1

    iput-byte v2, v4, Lsoftware/simplicial/a/bh;->A:B

    .line 2239
    const/16 v2, 0x3c

    iput-byte v2, v4, Lsoftware/simplicial/a/bh;->B:B

    .line 2240
    iget-object v2, v4, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 2242
    :cond_9
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->C:B

    if-ltz v2, :cond_a

    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->C:B

    rem-int/lit8 v2, v2, 0x3

    if-nez v2, :cond_a

    .line 2244
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    double-to-float v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->ab:F

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 2246
    :cond_a
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->E:B

    if-ltz v2, :cond_b

    .line 2248
    iget v2, v4, Lsoftware/simplicial/a/bh;->l:F

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v7}, Ljava/util/Random;->nextFloat()F

    move-result v7

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    sget v6, Lsoftware/simplicial/a/bl;->k:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->l:F

    .line 2249
    iget v2, v4, Lsoftware/simplicial/a/bh;->m:F

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v7}, Ljava/util/Random;->nextFloat()F

    move-result v7

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    sget v6, Lsoftware/simplicial/a/bl;->k:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->m:F

    .line 2252
    :cond_b
    const/4 v2, 0x0

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2253
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->E:B

    if-ltz v2, :cond_c

    .line 2254
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x10

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2255
    :cond_c
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->C:B

    if-ltz v2, :cond_d

    .line 2256
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x4

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2257
    :cond_d
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->A:B

    if-ltz v2, :cond_e

    .line 2258
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2259
    :cond_e
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->B:B

    if-ltz v2, :cond_f

    .line 2260
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x2

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2261
    :cond_f
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->D:B

    if-ltz v2, :cond_10

    .line 2262
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x8

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2263
    :cond_10
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->F:B

    if-ltz v2, :cond_11

    .line 2264
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x20

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2265
    :cond_11
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v2, :cond_12

    .line 2266
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit8 v2, v2, 0x40

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2267
    :cond_12
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->H:B

    if-ltz v2, :cond_13

    .line 2268
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit16 v2, v2, 0x80

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2269
    :cond_13
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->I:B

    if-ltz v2, :cond_14

    .line 2270
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit16 v2, v2, 0x100

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2271
    :cond_14
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v2, :cond_15

    .line 2272
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit16 v2, v2, 0x200

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2273
    :cond_15
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->K:B

    if-ltz v2, :cond_16

    .line 2274
    iget-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    or-int/lit16 v2, v2, 0x400

    int-to-short v2, v2

    iput-short v2, v4, Lsoftware/simplicial/a/bh;->z:S

    .line 2276
    :cond_16
    iget v2, v4, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v5, 0x0

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1d

    .line 2278
    iget v2, v4, Lsoftware/simplicial/a/bh;->P:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    const/high16 v6, 0x42a00000    # 80.0f

    add-float/2addr v5, v6

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2279
    iget v2, v4, Lsoftware/simplicial/a/bh;->P:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    const/high16 v6, 0x42a00000    # 80.0f

    add-float/2addr v5, v6

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    .line 2311
    :cond_17
    :goto_5
    instance-of v5, v3, Lsoftware/simplicial/a/i;
    
    if-eqz v5, :default
    
    const/high16 v2, 0x40100000 # 2.25
    
    iget v5, v4, Lsoftware/simplicial/a/bh;->a:F
    
    mul-float/2addr v2, v5
    
    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F
    
    const/16 v2, 0x40100000 # 2.25
    
    iget v5, v4, Lsoftware/simplicial/a/bh;->b:F
    
    mul-float/2addr v2, v5
    
    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F
    
    goto/16 :set
    
    :default
    const/16 v2, 0x3fa00000 # 1.25
    
    iget v5, v4, Lsoftware/simplicial/a/bh;->a:F
    
    mul-float/2addr v2, v5
    
    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F
    
    const/16 v2, 0x3fa00000 # 1.25
    
    iget v5, v4, Lsoftware/simplicial/a/bh;->b:F
    
    mul-float/2addr v2, v5
    
    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F
    
    :set
    iget v2, v4, Lsoftware/simplicial/a/bh;->l:F

    iget v5, v4, Lsoftware/simplicial/a/bh;->a:F

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->l:F

    .line 2330
    iget v2, v4, Lsoftware/simplicial/a/bh;->m:F

    iget v5, v4, Lsoftware/simplicial/a/bh;->b:F

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v5, v6

    add-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->m:F

    goto/16 :goto_3

    .line 2231
    :cond_1c
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->d()F

    move-result v2

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v2, v5

    invoke-virtual {v4, v2}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 2232
    iget-object v2, v4, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v5, Lsoftware/simplicial/a/a/c;

    iget v6, v3, Lsoftware/simplicial/a/bf;->C:I

    iget v7, v4, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v5, v6, v7}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 2283
    :cond_1d
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->A:B

    if-ltz v2, :cond_1e

    .line 2285
    const/4 v2, 0x0

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2286
    const/4 v2, 0x0

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    .line 2304
    :goto_6
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->F:B

    if-ltz v2, :cond_17

    .line 2306
    iget v2, v4, Lsoftware/simplicial/a/bh;->a:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2307
    iget v2, v4, Lsoftware/simplicial/a/bh;->b:F

    const/high16 v5, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    goto/16 :goto_5

    .line 2288
    :cond_1e
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->E:B

    if-ltz v2, :cond_1f

    .line 2290
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2291
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    goto :goto_6

    .line 2293
    :cond_1f
    iget-byte v2, v4, Lsoftware/simplicial/a/bh;->B:B

    if-ltz v2, :cond_20

    .line 2295
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    iget-byte v5, v4, Lsoftware/simplicial/a/bh;->B:B

    rsub-int/lit8 v5, v5, 0x3c

    int-to-float v5, v5

    mul-float/2addr v2, v5

    const/high16 v5, 0x42700000    # 60.0f

    div-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2296
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    iget-byte v5, v4, Lsoftware/simplicial/a/bh;->B:B

    rsub-int/lit8 v5, v5, 0x3c

    int-to-float v5, v5

    mul-float/2addr v2, v5

    const/high16 v5, 0x42700000    # 60.0f

    div-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    goto/16 :goto_6

    .line 2300
    :cond_20
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->a:F

    .line 2301
    iget v2, v3, Lsoftware/simplicial/a/bf;->N:F

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    iget v2, v3, Lsoftware/simplicial/a/bf;->P:F

    float-to-double v8, v2

    mul-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->b()F

    move-result v5

    mul-float/2addr v2, v5

    iput v2, v4, Lsoftware/simplicial/a/bh;->b:F

    goto/16 :goto_6

    .line 2335
    :cond_21
    const/4 v2, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v3, :cond_33

    .line 2337
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v6, v3, v2

    .line 2339
    iget-boolean v3, v6, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v3, :cond_22

    iget-boolean v3, v6, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_23

    .line 2335
    :cond_22
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 2343
    :cond_23
    const/4 v3, 0x0

    :goto_8
    iget v4, v6, Lsoftware/simplicial/a/bf;->S:I

    if-ge v3, v4, :cond_22

    .line 2345
    iget-object v4, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v7, v4, v3

    .line 2346
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 2343
    :cond_24
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 2349
    :cond_25
    add-int/lit8 v4, v3, 0x1

    :goto_9
    iget v5, v6, Lsoftware/simplicial/a/bf;->S:I

    if-ge v4, v5, :cond_2c

    .line 2351
    iget-object v5, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v8, v5, v4

    .line 2352
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-eqz v5, :cond_27

    .line 2349
    :cond_26
    :goto_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 2356
    :cond_27
    iget v5, v7, Lsoftware/simplicial/a/bh;->l:F

    iget v9, v7, Lsoftware/simplicial/a/bh;->m:F

    iget v10, v8, Lsoftware/simplicial/a/bh;->l:F

    iget v13, v8, Lsoftware/simplicial/a/bh;->m:F

    invoke-static {v5, v9, v10, v13}, Lsoftware/simplicial/a/bs;->a(FFFF)F

    move-result v5

    .line 2357
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    add-float/2addr v9, v10

    cmpl-float v9, v5, v9

    if-gtz v9, :cond_28

    iget v9, v6, Lsoftware/simplicial/a/bf;->T:F

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-nez v9, :cond_26

    .line 2359
    :cond_28
    iget v9, v7, Lsoftware/simplicial/a/bh;->l:F

    iget v10, v8, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v9, v10

    .line 2360
    iget v10, v7, Lsoftware/simplicial/a/bh;->m:F

    iget v13, v8, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v10, v13

    .line 2362
    const/high16 v13, 0x40400000    # 3.0f

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->b()F

    move-result v14

    float-to-double v14, v14

    float-to-double v0, v5

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v16

    mul-double v14, v14, v16

    const v5, 0x45ea6000    # 7500.0f

    iget v0, v6, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v5, v5, v16

    float-to-double v0, v5

    move-wide/from16 v16, v0

    div-double v14, v14, v16

    double-to-float v5, v14

    mul-float/2addr v5, v13

    .line 2364
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v14, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    if-eq v13, v14, :cond_29

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v14, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v13, v14, :cond_2a

    .line 2365
    :cond_29
    const v13, 0x3faaaaab

    mul-float/2addr v5, v13

    .line 2367
    :cond_2a
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->c()F

    move-result v13

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->c()F

    move-result v14

    mul-float/2addr v13, v14

    .line 2368
    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->c()F

    move-result v14

    invoke-virtual {v8}, Lsoftware/simplicial/a/bh;->c()F

    move-result v15

    mul-float/2addr v14, v15

    .line 2369
    add-float v15, v13, v14

    .line 2371
    iget-byte v0, v7, Lsoftware/simplicial/a/bh;->A:B

    move/from16 v16, v0

    if-gez v16, :cond_2b

    .line 2373
    iget v0, v7, Lsoftware/simplicial/a/bh;->l:F

    move/from16 v16, v0

    mul-float v17, v9, v5

    mul-float v17, v17, v14

    div-float v17, v17, v15

    sub-float v16, v16, v17

    move/from16 v0, v16

    iput v0, v7, Lsoftware/simplicial/a/bh;->l:F

    .line 2374
    iget v0, v7, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v16, v0

    mul-float v17, v10, v5

    mul-float v14, v14, v17

    div-float/2addr v14, v15

    sub-float v14, v16, v14

    iput v14, v7, Lsoftware/simplicial/a/bh;->m:F

    .line 2376
    :cond_2b
    iget-byte v14, v8, Lsoftware/simplicial/a/bh;->A:B

    if-gez v14, :cond_26

    .line 2378
    iget v14, v8, Lsoftware/simplicial/a/bh;->l:F

    mul-float/2addr v9, v5

    mul-float/2addr v9, v13

    div-float/2addr v9, v15

    add-float/2addr v9, v14

    iput v9, v8, Lsoftware/simplicial/a/bh;->l:F

    .line 2379
    iget v9, v8, Lsoftware/simplicial/a/bh;->m:F

    mul-float/2addr v5, v10

    mul-float/2addr v5, v13

    div-float/2addr v5, v15

    add-float/2addr v5, v9

    iput v5, v8, Lsoftware/simplicial/a/bh;->m:F

    goto/16 :goto_a

    .line 2384
    :cond_2c
    add-int/lit8 v4, v3, 0x1

    :goto_b
    iget v5, v6, Lsoftware/simplicial/a/bf;->S:I

    if-ge v4, v5, :cond_24

    .line 2386
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v5

    if-nez v5, :cond_24

    iget-byte v5, v7, Lsoftware/simplicial/a/bh;->A:B

    if-gez v5, :cond_24

    .line 2389
    iget-object v5, v6, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v5, v5, v4

    .line 2390
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v8

    if-nez v8, :cond_2d

    iget-byte v8, v5, Lsoftware/simplicial/a/bh;->A:B

    if-ltz v8, :cond_2e

    .line 2384
    :cond_2d
    :goto_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 2394
    :cond_2e
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    .line 2395
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    .line 2396
    invoke-virtual {v7, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;)Z

    .line 2397
    iget v10, v7, Lsoftware/simplicial/a/bh;->o:F

    mul-float v13, v8, v8

    mul-float v14, v9, v9

    invoke-static {v13, v14}, Ljava/lang/Math;->max(FF)F

    move-result v13

    cmpg-float v10, v10, v13

    if-gez v10, :cond_32

    iget v10, v7, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v13, 0x0

    cmpl-float v10, v10, v13

    if-nez v10, :cond_32

    iget v10, v5, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v13, 0x0

    cmpl-float v10, v10, v13

    if-nez v10, :cond_32

    iget v10, v6, Lsoftware/simplicial/a/bf;->T:F

    const/4 v13, 0x0

    cmpl-float v10, v10, v13

    if-nez v10, :cond_32

    .line 2400
    const/4 v8, 0x1

    iput-boolean v8, v6, Lsoftware/simplicial/a/bf;->aY:Z

    .line 2401
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->c()F

    move-result v8

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->c()F

    move-result v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_30

    .line 2403
    iget-byte v8, v5, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v8, :cond_2f

    .line 2404
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v7}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 2405
    :cond_2f
    invoke-virtual {v7, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/bh;)V

    .line 2406
    iget-object v8, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-eqz v8, :cond_2d

    iget-object v8, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    iget-object v8, v8, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-ne v8, v5, :cond_2d

    .line 2407
    iget-object v5, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    invoke-virtual {v5, v6, v7}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    goto :goto_c

    .line 2411
    :cond_30
    iget-byte v8, v7, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v8, :cond_31

    .line 2412
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 2413
    :cond_31
    invoke-virtual {v5, v7}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/bh;)V

    .line 2414
    iget-object v8, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-eqz v8, :cond_2d

    iget-object v8, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    iget-object v8, v8, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-ne v8, v7, :cond_2d

    .line 2415
    iget-object v8, v6, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    invoke-virtual {v8, v6, v5}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    goto :goto_c

    .line 2418
    :cond_32
    iget v10, v7, Lsoftware/simplicial/a/bh;->o:F

    add-float v13, v8, v9

    add-float/2addr v8, v9

    mul-float/2addr v8, v13

    cmpg-float v8, v10, v8

    if-gez v8, :cond_2d

    iget v8, v6, Lsoftware/simplicial/a/bf;->T:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2d

    iget v8, v7, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-nez v8, :cond_2d

    iget v8, v5, Lsoftware/simplicial/a/bh;->Q:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-nez v8, :cond_2d

    .line 2422
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    add-float/2addr v8, v9

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->h()F

    move-result v9

    sub-float/2addr v8, v9

    .line 2423
    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->c()F

    move-result v9

    invoke-virtual {v7}, Lsoftware/simplicial/a/bh;->c()F

    move-result v10

    mul-float/2addr v9, v10

    .line 2424
    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->c()F

    move-result v10

    invoke-virtual {v5}, Lsoftware/simplicial/a/bh;->c()F

    move-result v13

    mul-float/2addr v10, v13

    .line 2425
    add-float v13, v9, v10

    .line 2427
    iget v14, v7, Lsoftware/simplicial/a/bh;->l:F

    iget v15, v5, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v14, v15

    .line 2428
    iget v15, v7, Lsoftware/simplicial/a/bh;->m:F

    iget v0, v5, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    .line 2429
    float-to-double v0, v15

    move-wide/from16 v16, v0

    float-to-double v14, v14

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v14, v15}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    double-to-float v14, v14

    .line 2430
    iget v15, v7, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v0, v15

    move-wide/from16 v16, v0

    float-to-double v0, v8

    move-wide/from16 v18, v0

    float-to-double v0, v14

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    float-to-double v0, v10

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    float-to-double v0, v13

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    add-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v15, v0

    iput v15, v7, Lsoftware/simplicial/a/bh;->l:F

    .line 2431
    iget v15, v7, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v0, v15

    move-wide/from16 v16, v0

    float-to-double v0, v8

    move-wide/from16 v18, v0

    float-to-double v0, v14

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    float-to-double v0, v10

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    float-to-double v0, v13

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    add-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v10, v0

    iput v10, v7, Lsoftware/simplicial/a/bh;->m:F

    .line 2432
    iget v10, v5, Lsoftware/simplicial/a/bh;->l:F

    float-to-double v0, v10

    move-wide/from16 v16, v0

    float-to-double v0, v8

    move-wide/from16 v18, v0

    float-to-double v0, v14

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    mul-double v18, v18, v20

    float-to-double v0, v9

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    float-to-double v0, v13

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    sub-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v10, v0

    iput v10, v5, Lsoftware/simplicial/a/bh;->l:F

    .line 2433
    iget v10, v5, Lsoftware/simplicial/a/bh;->m:F

    float-to-double v0, v10

    move-wide/from16 v16, v0

    float-to-double v0, v8

    move-wide/from16 v18, v0

    float-to-double v14, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double v14, v14, v18

    float-to-double v8, v9

    mul-double/2addr v8, v14

    float-to-double v14, v13

    div-double/2addr v8, v14

    sub-double v8, v16, v8

    double-to-float v8, v8

    iput v8, v5, Lsoftware/simplicial/a/bh;->m:F

    goto/16 :goto_c

    .line 2440
    :cond_33
    const/4 v2, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v3, :cond_45

    .line 2442
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v9, v3, v2

    .line 2444
    iget-boolean v3, v9, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v3, :cond_34

    iget-boolean v3, v9, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v3, :cond_35

    .line 2440
    :cond_34
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 2447
    :cond_35
    const/4 v3, 0x0

    :goto_e
    iget v4, v9, Lsoftware/simplicial/a/bf;->S:I

    if-ge v3, v4, :cond_34

    .line 2449
    iget-object v4, v9, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v10, v4, v3

    .line 2450
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-nez v4, :cond_36

    iget-short v4, v10, Lsoftware/simplicial/a/bh;->z:S

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_37

    .line 2447
    :cond_36
    :goto_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    .line 2454
    :cond_37
    const/4 v4, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v4, v5, :cond_40

    .line 2456
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v13, v5, v4

    .line 2458
    iget v5, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v6, v10, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v13, v5, v6}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v5

    .line 2460
    iget-object v6, v10, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    aget-object v6, v6, v4

    if-eq v5, v6, :cond_3c

    sget-object v6, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v5, v6, :cond_3c

    iget-object v5, v10, Lsoftware/simplicial/a/bh;->p:[Lsoftware/simplicial/a/cb;

    aget-object v5, v5, v4

    sget-object v6, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v5, v6, :cond_3c

    const/4 v5, 0x1

    .line 2462
    :goto_11
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    const/high16 v7, 0x3e800000    # 0.25f

    mul-float/2addr v6, v7

    const/4 v7, 0x0

    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    add-float v14, v6, v7

    .line 2463
    invoke-virtual {v13, v10, v14}, Lsoftware/simplicial/a/ca;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v6

    if-nez v6, :cond_38

    if-eqz v5, :cond_3b

    .line 2466
    :cond_38
    iget v6, v13, Lsoftware/simplicial/a/ca;->d:F

    iget v7, v13, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v6, v7

    .line 2467
    iget v7, v13, Lsoftware/simplicial/a/ca;->e:F

    iget v8, v13, Lsoftware/simplicial/a/ca;->c:F

    sub-float/2addr v7, v8

    .line 2468
    iget v8, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v15, v13, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v8, v15

    .line 2469
    iget v15, v10, Lsoftware/simplicial/a/bh;->m:F

    iget v0, v13, Lsoftware/simplicial/a/ca;->c:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    .line 2470
    mul-float/2addr v8, v6

    mul-float/2addr v15, v7

    add-float/2addr v8, v15

    .line 2471
    mul-float v15, v6, v6

    mul-float v16, v7, v7

    add-float v15, v15, v16

    .line 2472
    div-float v15, v8, v15

    .line 2476
    const/4 v8, 0x0

    cmpg-float v8, v15, v8

    if-gez v8, :cond_3d

    .line 2478
    iget v8, v13, Lsoftware/simplicial/a/ca;->b:F

    .line 2479
    iget v7, v13, Lsoftware/simplicial/a/ca;->c:F

    .line 2480
    iget v6, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v15, v13, Lsoftware/simplicial/a/ca;->b:F

    sub-float/2addr v6, v15

    iget v15, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v0, v13, Lsoftware/simplicial/a/ca;->b:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    mul-float/2addr v6, v15

    iget v15, v10, Lsoftware/simplicial/a/bh;->m:F

    iget v0, v13, Lsoftware/simplicial/a/ca;->c:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    iget v0, v10, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v16, v0

    iget v13, v13, Lsoftware/simplicial/a/ca;->c:F

    sub-float v13, v16, v13

    mul-float/2addr v13, v15

    add-float/2addr v6, v13

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v6, v0

    .line 2497
    :cond_39
    :goto_12
    iget v13, v10, Lsoftware/simplicial/a/bh;->l:F

    sub-float v8, v13, v8

    .line 2498
    iget v13, v10, Lsoftware/simplicial/a/bh;->m:F

    sub-float v7, v13, v7

    .line 2500
    if-eqz v5, :cond_3f

    .line 2502
    neg-float v8, v8

    .line 2503
    neg-float v7, v7

    .line 2504
    iget v5, v10, Lsoftware/simplicial/a/bh;->n:F

    add-float/2addr v5, v6

    sub-float/2addr v5, v14

    move v6, v7

    move v7, v8

    .line 2510
    :goto_13
    mul-float v8, v7, v7

    mul-float v13, v6, v6

    add-float/2addr v8, v13

    float-to-double v14, v8

    invoke-static {v14, v15}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v14

    double-to-float v8, v14

    .line 2511
    const v13, 0x3c23d70a    # 0.01f

    cmpg-float v13, v8, v13

    if-gez v13, :cond_3a

    .line 2512
    const v8, 0x3c23d70a    # 0.01f

    .line 2513
    :cond_3a
    div-float/2addr v7, v8

    .line 2514
    div-float/2addr v6, v8

    .line 2517
    const/4 v8, 0x0

    cmpl-float v8, v5, v8

    if-lez v8, :cond_3b

    .line 2519
    iget v8, v10, Lsoftware/simplicial/a/bh;->l:F

    mul-float/2addr v7, v5

    add-float/2addr v7, v8

    iput v7, v10, Lsoftware/simplicial/a/bh;->l:F

    .line 2520
    iget v7, v10, Lsoftware/simplicial/a/bh;->m:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    iput v5, v10, Lsoftware/simplicial/a/bh;->m:F

    .line 2454
    :cond_3b
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_10

    .line 2460
    :cond_3c
    const/4 v5, 0x0

    goto/16 :goto_11

    .line 2482
    :cond_3d
    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v15, v8

    if-lez v8, :cond_3e

    .line 2484
    iget v8, v13, Lsoftware/simplicial/a/ca;->d:F

    .line 2485
    iget v7, v13, Lsoftware/simplicial/a/ca;->e:F

    .line 2486
    iget v6, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v15, v13, Lsoftware/simplicial/a/ca;->d:F

    sub-float/2addr v6, v15

    iget v15, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v0, v13, Lsoftware/simplicial/a/ca;->d:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    mul-float/2addr v6, v15

    iget v15, v10, Lsoftware/simplicial/a/bh;->m:F

    iget v0, v13, Lsoftware/simplicial/a/ca;->e:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    iget v0, v10, Lsoftware/simplicial/a/bh;->m:F

    move/from16 v16, v0

    iget v13, v13, Lsoftware/simplicial/a/ca;->e:F

    sub-float v13, v16, v13

    mul-float/2addr v13, v15

    add-float/2addr v6, v13

    float-to-double v0, v6

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v6, v0

    goto :goto_12

    .line 2490
    :cond_3e
    mul-float/2addr v6, v15

    iget v8, v13, Lsoftware/simplicial/a/ca;->b:F

    add-float/2addr v8, v6

    .line 2491
    mul-float v6, v15, v7

    iget v7, v13, Lsoftware/simplicial/a/ca;->c:F

    add-float/2addr v7, v6

    .line 2492
    invoke-virtual {v13}, Lsoftware/simplicial/a/ca;->h()F

    move-result v6

    .line 2493
    invoke-static {v6}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v15

    if-eqz v15, :cond_39

    .line 2494
    iget v6, v10, Lsoftware/simplicial/a/bh;->l:F

    iget v15, v10, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v13, v6, v15}, Lsoftware/simplicial/a/ca;->b(FF)F

    move-result v6

    goto/16 :goto_12

    .line 2507
    :cond_3f
    iget v5, v10, Lsoftware/simplicial/a/bh;->n:F

    sub-float/2addr v5, v6

    sub-float/2addr v5, v14

    move v6, v7

    move v7, v8

    goto/16 :goto_13

    .line 2526
    :cond_40
    iget v4, v10, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    const v6, 0x3f2aaaab

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    cmpg-float v4, v4, v12

    if-gez v4, :cond_41

    .line 2528
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    const v5, 0x3f2aaaab

    mul-float/2addr v4, v5

    add-float/2addr v4, v12

    iput v4, v10, Lsoftware/simplicial/a/bh;->l:F

    .line 2529
    instance-of v4, v9, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_41

    .line 2530
    const/4 v4, 0x0

    iput v4, v9, Lsoftware/simplicial/a/bf;->M:F

    .line 2532
    :cond_41
    iget v4, v10, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    const v6, 0x3f2aaaab

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    cmpl-float v4, v4, v11

    if-lez v4, :cond_42

    .line 2534
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    const v5, 0x3f2aaaab

    mul-float/2addr v4, v5

    sub-float v4, v11, v4

    iput v4, v10, Lsoftware/simplicial/a/bh;->l:F

    .line 2535
    instance-of v4, v9, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_42

    .line 2536
    const v4, 0x40490fdb    # (float)Math.PI

    iput v4, v9, Lsoftware/simplicial/a/bf;->M:F

    .line 2538
    :cond_42
    iget v4, v10, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    const v6, 0x3f2aaaab

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    cmpg-float v4, v4, v12

    if-gez v4, :cond_43

    .line 2540
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    const v5, 0x3f2aaaab

    mul-float/2addr v4, v5

    add-float/2addr v4, v12

    iput v4, v10, Lsoftware/simplicial/a/bh;->m:F

    .line 2541
    instance-of v4, v9, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_43

    .line 2542
    const v4, 0x3fc90fdb

    iput v4, v9, Lsoftware/simplicial/a/bf;->M:F

    .line 2544
    :cond_43
    iget v4, v10, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    const v6, 0x3f2aaaab

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    cmpl-float v4, v4, v11

    if-lez v4, :cond_44

    .line 2546
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    const v5, 0x3f2aaaab

    mul-float/2addr v4, v5

    sub-float v4, v11, v4

    iput v4, v10, Lsoftware/simplicial/a/bh;->m:F

    .line 2547
    instance-of v4, v9, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_44

    .line 2548
    const v4, 0x4096cbe4

    iput v4, v9, Lsoftware/simplicial/a/bf;->M:F

    .line 2551
    :cond_44
    invoke-virtual {v10}, Lsoftware/simplicial/a/bh;->i()V

    goto/16 :goto_f

    .line 2556
    :cond_45
    const/4 v2, 0x0

    :goto_14
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v2, v3, :cond_47

    .line 2558
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v3, v3, v2

    .line 2560
    iget-object v4, v3, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-eqz v4, :cond_46

    .line 2562
    iget-object v4, v3, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    iget v4, v4, Lsoftware/simplicial/a/bh;->l:F

    iput v4, v3, Lsoftware/simplicial/a/ag;->l:F

    .line 2563
    iget-object v4, v3, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    iget v4, v4, Lsoftware/simplicial/a/bh;->m:F

    iput v4, v3, Lsoftware/simplicial/a/ag;->m:F

    .line 2556
    :cond_46
    add-int/lit8 v2, v2, 0x1

    goto :goto_14

    .line 2568
    :cond_47
    const/4 v2, 0x0

    :goto_15
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v2, v3, :cond_49

    .line 2570
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v3, v3, v2

    .line 2572
    iget-object v4, v3, Lsoftware/simplicial/a/bl;->g:Lsoftware/simplicial/a/bf;

    if-eqz v4, :cond_48

    .line 2574
    iget-object v4, v3, Lsoftware/simplicial/a/bl;->h:Lsoftware/simplicial/a/bh;

    iget v4, v4, Lsoftware/simplicial/a/bh;->l:F

    iput v4, v3, Lsoftware/simplicial/a/bl;->l:F

    .line 2575
    iget-object v4, v3, Lsoftware/simplicial/a/bl;->h:Lsoftware/simplicial/a/bh;

    iget v4, v4, Lsoftware/simplicial/a/bh;->m:F

    iput v4, v3, Lsoftware/simplicial/a/bl;->m:F

    .line 2568
    :cond_48
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 2578
    :cond_49
    return-void
.end method

.method private w()V
    .locals 23

    .prologue
    .line 2595
    const/4 v4, 0x0

    move v15, v4

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v15, v4, :cond_45

    .line 2597
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v22, v4, v15

    .line 2599
    move-object/from16 v0, v22

    iget-boolean v4, v0, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, v22

    iget-boolean v4, v0, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v4, :cond_0

    invoke-virtual/range {v22 .. v22}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2595
    :cond_0
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    goto :goto_0

    .line 2602
    :cond_1
    const/4 v4, 0x0

    move/from16 v16, v4

    :goto_1
    move-object/from16 v0, v22

    iget v4, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v0, v16

    if-ge v0, v4, :cond_0

    .line 2604
    move-object/from16 v0, v22

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v20, v4, v16

    .line 2605
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2602
    :cond_2
    :goto_2
    add-int/lit8 v4, v16, 0x1

    move/from16 v16, v4

    goto :goto_1

    .line 2608
    :cond_3
    move-object/from16 v0, v22

    iget v4, v0, Lsoftware/simplicial/a/bf;->ax:I

    if-nez v4, :cond_23

    .line 2610
    const/4 v4, 0x0

    move/from16 v17, v4

    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aY:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_16

    .line 2612
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v21, v4, v17

    .line 2614
    move-object/from16 v0, v21

    iget-boolean v4, v0, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v4, :cond_5

    .line 2610
    :cond_4
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    goto :goto_3

    .line 2617
    :cond_5
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_4

    invoke-virtual/range {v21 .. v21}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v4

    if-nez v4, :cond_4

    move-object/from16 v0, v21

    iget v4, v0, Lsoftware/simplicial/a/bf;->ax:I

    if-gtz v4, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->aj:I

    if-lez v4, :cond_6

    move-object/from16 v0, v22

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v21

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v4, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v4, v5, :cond_4

    move-object/from16 v0, v22

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    .line 2621
    :cond_6
    const/4 v4, 0x0

    move/from16 v18, v4

    :goto_4
    move-object/from16 v0, v21

    iget v4, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_4

    .line 2623
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-nez v4, :cond_4

    .line 2626
    move-object/from16 v0, v21

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v9, v4, v18

    .line 2627
    move-object/from16 v0, v20

    if-eq v0, v9, :cond_7

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2621
    :cond_7
    :goto_5
    add-int/lit8 v4, v18, 0x1

    move/from16 v18, v4

    goto :goto_4

    .line 2630
    :cond_8
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 2631
    move-object/from16 v0, v20

    invoke-virtual {v0, v9, v4}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v9, v4, v5}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2633
    const/4 v5, 0x0

    .line 2634
    const/4 v8, 0x0

    .line 2635
    const/4 v6, 0x0

    .line 2636
    const/4 v4, 0x0

    .line 2637
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    const v11, 0x3f866666    # 1.05f

    mul-float/2addr v10, v11

    const/high16 v11, 0x3f400000    # 0.75f

    add-float/2addr v10, v11

    cmpl-float v7, v7, v10

    if-lez v7, :cond_9

    move-object/from16 v19, v9

    move-object/from16 v6, v20

    move-object/from16 v8, v21

    move-object/from16 v5, v22

    .line 2652
    :goto_6
    if-eqz v5, :cond_7

    .line 2654
    move-object/from16 v0, v19

    iget-byte v4, v0, Lsoftware/simplicial/a/bh;->A:B

    if-ltz v4, :cond_b

    .line 2656
    iget v7, v5, Lsoftware/simplicial/a/bf;->M:F

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/a/bs;->ab:F

    const/4 v12, 0x0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v12}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 2657
    invoke-virtual {v5}, Lsoftware/simplicial/a/bf;->n()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2659
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    .line 2668
    :goto_7
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    invoke-virtual {v6, v4}, Lsoftware/simplicial/a/bh;->c(F)V

    .line 2670
    const/4 v4, -0x1

    move-object/from16 v0, v19

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->A:B

    .line 2671
    const/16 v4, 0x3c

    move-object/from16 v0, v19

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->B:B

    goto :goto_5

    .line 2644
    :cond_9
    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v7

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    const v11, 0x3f866666    # 1.05f

    mul-float/2addr v10, v11

    const/high16 v11, 0x3f400000    # 0.75f

    add-float/2addr v10, v11

    cmpl-float v7, v7, v10

    if-lez v7, :cond_46

    move-object/from16 v19, v20

    move-object v6, v9

    move-object/from16 v8, v22

    move-object/from16 v5, v21

    .line 2649
    goto :goto_6

    .line 2663
    :cond_a
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v4, v7

    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v4, v7

    invoke-virtual {v6, v4}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 2664
    iget-object v4, v6, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v7, Lsoftware/simplicial/a/a/c;

    iget v5, v5, Lsoftware/simplicial/a/bf;->C:I

    iget v8, v6, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v7, v5, v8}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2673
    :cond_b
    move-object/from16 v0, v19

    iget-byte v4, v0, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v4, :cond_e

    .line 2675
    iget v4, v6, Lsoftware/simplicial/a/bh;->l:F

    move-object/from16 v0, v19

    iget v5, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float v7, v4, v5

    .line 2676
    iget v4, v6, Lsoftware/simplicial/a/bh;->m:F

    move-object/from16 v0, v19

    iget v5, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float v5, v4, v5

    .line 2677
    mul-float v4, v7, v7

    mul-float v8, v5, v5

    add-float/2addr v4, v8

    float-to-double v10, v4

    invoke-static {v10, v11}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v10

    double-to-float v4, v10

    .line 2678
    const v8, 0x3a83126f    # 0.001f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_c

    .line 2680
    const/4 v7, 0x0

    .line 2681
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2682
    const/high16 v4, 0x3f800000    # 1.0f

    .line 2684
    :cond_c
    div-float/2addr v7, v4

    .line 2685
    div-float/2addr v5, v4

    .line 2686
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v4

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v8

    invoke-static {v4, v8}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->h()F

    move-result v8

    sub-float/2addr v4, v8

    .line 2687
    const/4 v8, 0x0

    cmpg-float v8, v4, v8

    if-gez v8, :cond_d

    .line 2688
    const/4 v4, 0x0

    .line 2689
    :cond_d
    iget v8, v6, Lsoftware/simplicial/a/bh;->l:F

    mul-float/2addr v7, v4

    add-float/2addr v7, v8

    iput v7, v6, Lsoftware/simplicial/a/bh;->l:F

    .line 2690
    iget v7, v6, Lsoftware/simplicial/a/bh;->m:F

    mul-float/2addr v4, v5

    add-float/2addr v4, v7

    iput v4, v6, Lsoftware/simplicial/a/bh;->m:F

    goto/16 :goto_5

    .line 2694
    :cond_e
    iget-object v4, v8, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 2695
    if-eqz v4, :cond_f

    iget-object v7, v4, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    move-object/from16 v0, v19

    if-ne v7, v0, :cond_f

    .line 2697
    iget-object v7, v4, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget-object v9, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v7, v9, :cond_14

    .line 2698
    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v4, v7}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 2707
    :cond_f
    :goto_8
    move-object/from16 v0, v19

    iget-byte v4, v0, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v4, :cond_10

    .line 2708
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 2709
    :cond_10
    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/bh;)V

    .line 2710
    move-object/from16 v0, p0

    iget-wide v10, v0, Lsoftware/simplicial/a/bs;->aO:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->aa:Lsoftware/simplicial/a/bj;

    move-object v7, v5

    move-object/from16 v9, v19

    invoke-virtual/range {v7 .. v14}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;JLsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/bj;)V

    .line 2711
    move-object/from16 v0, p0

    iget-wide v10, v0, Lsoftware/simplicial/a/bs;->aO:J

    invoke-virtual {v8, v10, v11}, Lsoftware/simplicial/a/bf;->a(J)V

    .line 2713
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-ne v4, v7, :cond_13

    .line 2715
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    if-eq v8, v4, :cond_11

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    if-nez v4, :cond_12

    .line 2717
    :cond_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;)V

    .line 2718
    iget v4, v5, Lsoftware/simplicial/a/bf;->Q:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v5, Lsoftware/simplicial/a/bf;->Q:I

    .line 2720
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ac:Lsoftware/simplicial/a/bf;

    if-ne v5, v4, :cond_13

    invoke-virtual {v8}, Lsoftware/simplicial/a/bf;->w()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 2721
    iget v4, v5, Lsoftware/simplicial/a/bf;->Q:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v5, Lsoftware/simplicial/a/bf;->Q:I

    .line 2725
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v4, v7, :cond_7

    iget-object v4, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    if-nez v4, :cond_7

    iget-object v4, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 2726
    const/16 v4, 0xa

    iput-byte v4, v6, Lsoftware/simplicial/a/bh;->C:B

    goto/16 :goto_5

    .line 2699
    :cond_14
    iget-object v7, v5, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-nez v7, :cond_15

    .line 2700
    invoke-virtual {v4, v5, v6}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    goto/16 :goto_8

    .line 2703
    :cond_15
    const/4 v7, 0x1

    invoke-virtual {v4, v8, v7}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 2704
    iget v4, v8, Lsoftware/simplicial/a/bf;->bg:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v8, Lsoftware/simplicial/a/bf;->bg:I

    goto/16 :goto_8

    .line 2732
    :cond_16
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2736
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->ar:I

    if-lez v4, :cond_17

    .line 2738
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->at:Ljava/util/Collection;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 2742
    :cond_17
    const/4 v4, 0x0

    move v5, v4

    :goto_9
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v5, v4, :cond_1b

    .line 2744
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v4, v4, v5

    .line 2746
    iget-object v6, v4, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-eqz v6, :cond_19

    .line 2742
    :cond_18
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_9

    .line 2749
    :cond_19
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    iget v7, v4, Lsoftware/simplicial/a/ag;->n:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 2750
    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v6}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v6

    if-eqz v6, :cond_18

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v4, v6, v7}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 2752
    iget-object v6, v4, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v22

    iget-object v7, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v6, v7, :cond_1a

    .line 2754
    iget-object v6, v4, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    if-nez v6, :cond_18

    iget-boolean v6, v4, Lsoftware/simplicial/a/ag;->e:Z

    if-nez v6, :cond_18

    .line 2756
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v4, v6}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 2757
    iget-object v4, v4, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bx;)Ljava/util/List;

    move-result-object v7

    .line 2758
    const/4 v4, 0x0

    move v6, v4

    :goto_a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_18

    .line 2759
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bf;

    iget v8, v4, Lsoftware/simplicial/a/bf;->bh:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v4, Lsoftware/simplicial/a/bf;->bh:I

    .line 2758
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_a

    .line 2764
    :cond_1a
    move-object/from16 v0, v22

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-nez v6, :cond_18

    move-object/from16 v0, v22

    iget v6, v0, Lsoftware/simplicial/a/bf;->al:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_18

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    iget v7, v4, Lsoftware/simplicial/a/ag;->n:F

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_18

    .line 2766
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    .line 2767
    iget-object v4, v4, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bx;)Ljava/util/List;

    move-result-object v7

    .line 2768
    const/4 v4, 0x0

    move v6, v4

    :goto_b
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_18

    .line 2769
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bf;

    iget v8, v4, Lsoftware/simplicial/a/bf;->bi:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v4, Lsoftware/simplicial/a/bf;->bi:I

    .line 2768
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_b

    .line 2776
    :cond_1b
    const/4 v4, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v4, v5, :cond_23

    .line 2778
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v8, v5, v4

    .line 2781
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    iget v6, v8, Lsoftware/simplicial/a/bl;->n:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 2782
    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v8, v5, v6}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 2784
    sget-object v5, Lsoftware/simplicial/a/bs$2;->e:[I

    iget-object v6, v8, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bl$a;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2776
    :cond_1c
    :goto_d
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 2787
    :pswitch_0
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->g:Lsoftware/simplicial/a/bf;

    if-nez v5, :cond_1c

    move-object/from16 v0, v22

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    if-nez v5, :cond_1c

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    iget v6, v8, Lsoftware/simplicial/a/bl;->n:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_1c

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v22

    if-eq v5, v0, :cond_1c

    .line 2790
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Lsoftware/simplicial/a/bl;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    goto :goto_d

    .line 2794
    :pswitch_1
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->ax:I

    if-nez v5, :cond_1c

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_1d

    move-object/from16 v0, v20

    iget-byte v5, v0, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v5, :cond_1c

    :cond_1d
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v22

    if-ne v5, v0, :cond_1e

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aN:I

    iget v6, v8, Lsoftware/simplicial/a/bl;->D:I

    sub-int/2addr v5, v6

    const/4 v6, 0x2

    if-le v5, v6, :cond_1c

    :cond_1e
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    iget-object v5, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v5, :cond_1f

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    iget-object v5, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v22

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-ne v5, v6, :cond_1f

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->h:Lsoftware/simplicial/a/bl$b;

    if-eq v5, v6, :cond_1f

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->i:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_1c

    .line 2798
    :cond_1f
    move-object/from16 v0, v20

    iget-byte v5, v0, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v5, :cond_21

    .line 2800
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float v7, v5, v6

    .line 2801
    iget v5, v8, Lsoftware/simplicial/a/bl;->m:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float v6, v5, v6

    .line 2802
    mul-float v5, v7, v7

    mul-float v9, v6, v6

    add-float/2addr v5, v9

    float-to-double v10, v5

    invoke-static {v10, v11}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v10

    double-to-float v5, v10

    .line 2803
    const v9, 0x3a83126f    # 0.001f

    cmpg-float v9, v5, v9

    if-gez v9, :cond_20

    .line 2805
    const/4 v7, 0x0

    .line 2806
    const/high16 v6, 0x3f800000    # 1.0f

    .line 2807
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2809
    :cond_20
    div-float/2addr v7, v5

    .line 2810
    div-float v5, v6, v5

    .line 2811
    iget v6, v8, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v6, v7

    iget v9, v8, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v9, v5

    add-float/2addr v6, v9

    .line 2812
    iget v9, v8, Lsoftware/simplicial/a/bl;->c:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v6

    mul-float/2addr v7, v10

    sub-float v7, v9, v7

    iput v7, v8, Lsoftware/simplicial/a/bl;->c:F

    .line 2813
    iget v7, v8, Lsoftware/simplicial/a/bl;->d:F

    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v6, v9

    mul-float/2addr v5, v6

    sub-float v5, v7, v5

    iput v5, v8, Lsoftware/simplicial/a/bl;->d:F

    .line 2815
    iget v5, v8, Lsoftware/simplicial/a/bl;->l:F

    iget v6, v8, Lsoftware/simplicial/a/bl;->c:F

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iput v5, v8, Lsoftware/simplicial/a/bl;->l:F

    .line 2816
    iget v5, v8, Lsoftware/simplicial/a/bl;->m:F

    iget v6, v8, Lsoftware/simplicial/a/bl;->d:F

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iput v5, v8, Lsoftware/simplicial/a/bl;->m:F

    goto/16 :goto_d

    .line 2818
    :cond_21
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v6, Lsoftware/simplicial/a/bl$b;->i:Lsoftware/simplicial/a/bl$b;

    if-ne v5, v6, :cond_22

    .line 2820
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    if-nez v5, :cond_1c

    .line 2822
    move-object/from16 v0, v20

    iput-object v0, v8, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 2823
    const/4 v5, 0x0

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->I:B

    .line 2824
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v6, 0x100

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto/16 :goto_d

    .line 2829
    :cond_22
    sget-object v5, Lsoftware/simplicial/a/bs$2;->d:[I

    iget-object v6, v8, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 2862
    :goto_e
    sget-object v5, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    iput-object v5, v8, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 2863
    const/4 v5, 0x1

    iput-boolean v5, v8, Lsoftware/simplicial/a/bl;->E:Z

    .line 2864
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    iget-boolean v5, v5, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v5, :cond_1c

    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    move-object/from16 v0, v22

    if-eq v5, v0, :cond_1c

    .line 2865
    iget-object v5, v8, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    invoke-virtual {v5}, Lsoftware/simplicial/a/bf;->s()V

    goto/16 :goto_d

    .line 2832
    :pswitch_2
    const/16 v5, 0x3c

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->D:B

    .line 2833
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_e

    .line 2836
    :pswitch_3
    const/16 v5, 0x28

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->A:B

    .line 2837
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_e

    .line 2840
    :pswitch_4
    const/16 v5, 0x3c

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->E:B

    .line 2841
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_e

    .line 2844
    :pswitch_5
    const/16 v5, 0x28

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->C:B

    .line 2845
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto :goto_e

    .line 2848
    :pswitch_6
    const/4 v5, -0x1

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->D:B

    .line 2849
    const/4 v5, -0x1

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->A:B

    .line 2850
    const/4 v5, -0x1

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->B:B

    .line 2851
    const/4 v5, -0x1

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->E:B

    .line 2852
    const/4 v5, -0x1

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->C:B

    .line 2853
    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->d()F

    move-result v6

    const/high16 v7, 0x3e800000    # 0.25f

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    const/high16 v6, 0x447a0000    # 1000.0f

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 2854
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    .line 2855
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->bf:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, v22

    iput v5, v0, Lsoftware/simplicial/a/bf;->bf:I

    goto/16 :goto_e

    .line 2858
    :pswitch_7
    const/16 v5, 0x3c

    move-object/from16 v0, v20

    iput-byte v5, v0, Lsoftware/simplicial/a/bh;->K:B

    .line 2859
    move-object/from16 v0, v20

    iget-object v5, v0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v6, 0x400

    invoke-virtual {v5, v6}, Lsoftware/simplicial/a/a/e;->a(S)V

    goto/16 :goto_e

    .line 2875
    :cond_23
    const/4 v4, 0x0

    :goto_f
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->ay:I

    if-ge v4, v5, :cond_28

    .line 2877
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v6, v5, v4

    .line 2879
    invoke-virtual {v6}, Lsoftware/simplicial/a/ae;->f()Z

    move-result v5

    if-eqz v5, :cond_25

    .line 2875
    :cond_24
    :goto_10
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 2883
    :cond_25
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    sget v7, Lsoftware/simplicial/a/bh;->i:F

    cmpg-float v5, v5, v7

    if-gez v5, :cond_26

    .line 2884
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    sget v7, Lsoftware/simplicial/a/bh;->i:F

    sub-float/2addr v5, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    .line 2887
    :goto_11
    if-eqz v5, :cond_24

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v6, v5, v7}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 2889
    invoke-virtual {v6}, Lsoftware/simplicial/a/ae;->e()V

    .line 2890
    move-object/from16 v0, v22

    instance-of v5, v0, Lsoftware/simplicial/a/i;

    if-eqz v5, :cond_27

    .line 2892
    sget-object v5, Lsoftware/simplicial/a/bs$2;->b:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    invoke-virtual {v6}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_2

    .line 2910
    :goto_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/am;)V

    goto :goto_10

    .line 2886
    :cond_26
    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    goto :goto_11

    .line 2895
    :pswitch_8
    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_12

    .line 2898
    :pswitch_9
    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_12

    .line 2901
    :pswitch_a
    const/high16 v5, 0x40800000    # 4.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_12

    .line 2907
    :cond_27
    const/high16 v5, 0x3f800000    # 1.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_12

    .line 2915
    :cond_28
    const/4 v4, 0x0

    :goto_13
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aA:I

    if-ge v4, v5, :cond_2e

    .line 2917
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v6, v5, v4

    .line 2919
    invoke-virtual {v6}, Lsoftware/simplicial/a/bt;->f()Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 2915
    :cond_29
    :goto_14
    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    .line 2923
    :cond_2a
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    sget v7, Lsoftware/simplicial/a/bh;->i:F

    cmpg-float v5, v5, v7

    if-gez v5, :cond_2c

    .line 2924
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v5

    sget v7, Lsoftware/simplicial/a/bh;->i:F

    sub-float/2addr v5, v7

    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    .line 2927
    :goto_15
    if-eqz v5, :cond_29

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v6, v5, v7}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 2929
    invoke-virtual {v6}, Lsoftware/simplicial/a/bt;->e()V

    .line 2930
    move-object/from16 v0, v22

    instance-of v5, v0, Lsoftware/simplicial/a/i;

    if-eqz v5, :cond_2d

    .line 2932
    sget-object v5, Lsoftware/simplicial/a/bs$2;->b:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    invoke-virtual {v7}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_3

    .line 2950
    :goto_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v7, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-ne v5, v7, :cond_2b

    .line 2951
    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bh;->L:F

    const/high16 v7, 0x3f800000    # 1.0f

    add-float/2addr v5, v7

    move-object/from16 v0, v20

    iput v5, v0, Lsoftware/simplicial/a/bh;->L:F

    .line 2953
    :cond_2b
    iget-object v5, v6, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/bv;Lsoftware/simplicial/a/am;)V

    goto :goto_14

    .line 2926
    :cond_2c
    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;)Z

    move-result v5

    goto :goto_15

    .line 2935
    :pswitch_b
    const/high16 v5, 0x40400000    # 3.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_16

    .line 2938
    :pswitch_c
    const/high16 v5, 0x40c00000    # 6.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_16

    .line 2941
    :pswitch_d
    const/high16 v5, 0x41400000    # 12.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_16

    .line 2947
    :cond_2d
    const/high16 v5, 0x40400000    # 3.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    goto :goto_16

    .line 2958
    :cond_2e
    const/4 v4, 0x0

    :goto_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v5, v5

    if-ge v4, v5, :cond_31

    .line 2960
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v5, v5, v4

    .line 2962
    iget-object v6, v5, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v7, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-eq v6, v7, :cond_2f

    iget v6, v5, Lsoftware/simplicial/a/bq;->k:F

    const v7, 0x3dcccccd    # 0.1f

    cmpg-float v6, v6, v7

    if-gez v6, :cond_30

    .line 2958
    :cond_2f
    :goto_18
    add-int/lit8 v4, v4, 0x1

    goto :goto_17

    .line 2965
    :cond_30
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    invoke-virtual {v5}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v6

    if-eqz v6, :cond_2f

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v5, v6, v7}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v6

    if-eqz v6, :cond_2f

    .line 2968
    invoke-virtual {v5}, Lsoftware/simplicial/a/bq;->d()F

    move-result v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/bh;->a(F)V

    .line 2969
    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/bq;)V

    .line 2971
    sget-object v6, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v6, v5, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 2972
    const/4 v6, 0x1

    iput-boolean v6, v5, Lsoftware/simplicial/a/bq;->G:Z

    goto :goto_18

    .line 2977
    :cond_31
    const/4 v4, 0x0

    :goto_19
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v4, v5, :cond_2

    .line 2979
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v8, v5, v4

    .line 2981
    invoke-virtual {v8}, Lsoftware/simplicial/a/g;->f()Z

    move-result v5

    if-nez v5, :cond_32

    iget v5, v8, Lsoftware/simplicial/a/g;->n:F

    const v6, 0x413bcccc

    cmpg-float v5, v5, v6

    if-gez v5, :cond_33

    .line 2977
    :cond_32
    :goto_1a
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 2985
    :cond_33
    iget v5, v8, Lsoftware/simplicial/a/g;->n:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->n:F

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v5}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 2987
    const v5, 0x417a6666    # 15.65f

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    cmpg-float v5, v5, v6

    if-eqz v5, :cond_3c
    
    move-object/from16 v0, v22
    
    instance-of v5, v0, Lsoftware/simplicial/a/i;
    
    if-eqz v5, :cond_3c

    .line 2989
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v20

    invoke-static {v0, v8, v5, v6}, Lsoftware/simplicial/a/ao;->a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 2991
    move-object/from16 v0, v20

    iget-byte v5, v0, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v5, :cond_36

    .line 2993
    iget v5, v8, Lsoftware/simplicial/a/g;->l:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float v7, v5, v6

    .line 2994
    iget v5, v8, Lsoftware/simplicial/a/g;->m:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float v6, v5, v6

    .line 2995
    mul-float v5, v7, v7

    mul-float v9, v6, v6

    add-float/2addr v5, v9

    float-to-double v10, v5

    invoke-static {v10, v11}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v10

    double-to-float v5, v10

    .line 2996
    const v9, 0x3a83126f    # 0.001f

    cmpg-float v9, v5, v9

    if-gez v9, :cond_34

    .line 2998
    const/4 v7, 0x0

    .line 2999
    const/high16 v6, 0x3f800000    # 1.0f

    .line 3000
    const/high16 v5, 0x3f800000    # 1.0f

    .line 3002
    :cond_34
    div-float/2addr v7, v5

    .line 3003
    div-float/2addr v6, v5

    .line 3004
    iget v5, v8, Lsoftware/simplicial/a/g;->c:F

    mul-float/2addr v5, v7

    iget v9, v8, Lsoftware/simplicial/a/g;->d:F

    mul-float/2addr v9, v6

    add-float/2addr v5, v9

    .line 3005
    iget v9, v8, Lsoftware/simplicial/a/g;->c:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v10, v5

    mul-float/2addr v10, v7

    sub-float/2addr v9, v10

    iput v9, v8, Lsoftware/simplicial/a/g;->c:F

    .line 3006
    iget v9, v8, Lsoftware/simplicial/a/g;->d:F

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v5, v10

    mul-float/2addr v5, v6

    sub-float v5, v9, v5

    iput v5, v8, Lsoftware/simplicial/a/g;->d:F

    .line 3008
    iget v5, v8, Lsoftware/simplicial/a/g;->n:F

    move-object/from16 v0, v20

    iget v9, v0, Lsoftware/simplicial/a/bh;->n:F

    invoke-static {v5, v9}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->h()F

    move-result v9

    sub-float/2addr v5, v9

    .line 3009
    const/4 v9, 0x0

    cmpg-float v9, v5, v9

    if-gez v9, :cond_35

    .line 3010
    const/4 v5, 0x0

    .line 3011
    :cond_35
    iget v9, v8, Lsoftware/simplicial/a/g;->l:F

    mul-float/2addr v7, v5

    add-float/2addr v7, v9

    iput v7, v8, Lsoftware/simplicial/a/g;->l:F

    .line 3012
    iget v7, v8, Lsoftware/simplicial/a/g;->m:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    iput v5, v8, Lsoftware/simplicial/a/g;->m:F

    goto/16 :goto_1a

    .line 3014
    :cond_36
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->ax:I

    if-nez v5, :cond_32

    .line 3016
    invoke-virtual {v8}, Lsoftware/simplicial/a/g;->e()V

    .line 3017
    move-object/from16 v0, p0

    iget-wide v4, v0, Lsoftware/simplicial/a/bs;->aO:J

    iget v6, v8, Lsoftware/simplicial/a/g;->g:I

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v4, v5, v6}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/g;JI)V

    .line 3018
    iget v4, v8, Lsoftware/simplicial/a/g;->g:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v4

    .line 3019
    if-eqz v4, :cond_37

    move-object/from16 v0, v22

    if-eq v4, v0, :cond_37

    iget-object v5, v4, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v5, v5, Lsoftware/simplicial/a/ay;->Y:I

    invoke-virtual/range {v22 .. v22}, Lsoftware/simplicial/a/bf;->k()I

    move-result v6

    if-ne v5, v6, :cond_37

    iget-object v5, v4, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v5, v5, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v6, Lsoftware/simplicial/a/d;->L:Lsoftware/simplicial/a/d;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_37

    .line 3020
    iget-object v4, v4, Lsoftware/simplicial/a/bf;->bs:Ljava/util/Set;

    sget-object v5, Lsoftware/simplicial/a/d;->L:Lsoftware/simplicial/a/d;

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3022
    :cond_37
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v4, v5, :cond_39

    move-object/from16 v0, v22

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    if-nez v4, :cond_39

    .line 3024
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->d()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    const/high16 v5, 0x40c00000    # 6.0f

    div-float v12, v4, v5

    .line 3025
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/a/bs;->ab:F

    move-object/from16 v4, p0

    move-object/from16 v5, v22

    move-object/from16 v6, v20

    invoke-direct/range {v4 .. v12}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 3026
    move-object/from16 v0, v20

    iget-object v4, v0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v5, Lsoftware/simplicial/a/a/c;

    move-object/from16 v0, v22

    iget v6, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, v20

    iget v7, v0, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v5, v6, v7}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3027
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->e()V

    .line 3057
    :cond_38
    :goto_1b
    move-object/from16 v0, v22

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v16, v0

    goto/16 :goto_2

    .line 3031
    :cond_39
    iget-object v4, v8, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v5, Lsoftware/simplicial/a/g$a;->a:Lsoftware/simplicial/a/g$a;

    if-ne v4, v5, :cond_3a

    .line 3032
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    const v5, 0x3f666666    # 0.9f

    mul-float/2addr v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 3036
    :goto_1c
    iget v4, v8, Lsoftware/simplicial/a/g;->l:F

    move-object/from16 v0, v20

    iget v5, v0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v4, v5

    .line 3037
    iget v5, v8, Lsoftware/simplicial/a/g;->m:F

    move-object/from16 v0, v20

    iget v6, v0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v5, v6

    .line 3038
    float-to-double v6, v5

    float-to-double v4, v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v7, v4

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/a/bs;->ab:F

    const/4 v12, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, v22

    move-object/from16 v6, v20

    invoke-direct/range {v4 .. v12}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 3039
    invoke-virtual/range {v22 .. v22}, Lsoftware/simplicial/a/bf;->n()Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 3041
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    .line 3049
    :goto_1d
    move-object/from16 v0, v20

    iget-byte v4, v0, Lsoftware/simplicial/a/bh;->A:B

    if-ltz v4, :cond_38

    .line 3051
    const/4 v4, -0x1

    move-object/from16 v0, v20

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->A:B

    .line 3052
    const/16 v4, 0x3c

    move-object/from16 v0, v20

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->B:B

    goto :goto_1b

    .line 3034
    :cond_3a
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lsoftware/simplicial/a/bh;->b(F)V

    goto :goto_1c

    .line 3045
    :cond_3b
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->d()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x40400000    # 3.0f

    div-float/2addr v4, v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 3046
    move-object/from16 v0, v20

    iget-object v4, v0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v5, Lsoftware/simplicial/a/a/c;

    move-object/from16 v0, v22

    iget v6, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, v20

    iget v7, v0, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v5, v6, v7}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 3063
    :cond_3c
    iget-object v5, v8, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v6, Lsoftware/simplicial/a/g$a;->b:Lsoftware/simplicial/a/g$a;

    if-ne v5, v6, :cond_3e

    .line 3065
    invoke-virtual {v8}, Lsoftware/simplicial/a/g;->e()V

    .line 3066
    const/16 v4, 0x4b

    .line 3067
    move-object/from16 v0, v22

    instance-of v5, v0, Lsoftware/simplicial/a/i;

    if-eqz v5, :cond_3d

    .line 3069
    sget-object v5, Lsoftware/simplicial/a/bs$2;->b:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    invoke-virtual {v6}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_4

    .line 3082
    :cond_3d
    :goto_1e
    int-to-float v5, v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bh;->a(F)V

    .line 3083
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->bf:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, v22

    iput v5, v0, Lsoftware/simplicial/a/bf;->bf:I

    .line 3084
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Lsoftware/simplicial/a/bf;->a(ILsoftware/simplicial/a/am;)V

    .line 3087
    move-object/from16 v0, v22

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v16, v0

    goto/16 :goto_2

    .line 3072
    :pswitch_e
    const/16 v4, 0x4b

    .line 3073
    goto :goto_1e

    .line 3075
    :pswitch_f
    const/16 v4, 0x55

    .line 3076
    goto :goto_1e

    .line 3078
    :pswitch_10
    const/16 v4, 0x96

    goto :goto_1e

    .line 3090
    :cond_3e
    iget-object v5, v8, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v6, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v5, v6, :cond_32

    .line 3092
    new-instance v5, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 3093
    const/4 v4, 0x0

    :goto_1f
    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v4, v6, :cond_40

    .line 3095
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v4

    iget-object v6, v6, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    sget-object v7, Lsoftware/simplicial/a/g$a;->c:Lsoftware/simplicial/a/g$a;

    if-ne v6, v7, :cond_3f

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v4

    if-eq v6, v8, :cond_3f

    .line 3097
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v6, v6, v4

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3093
    :cond_3f
    add-int/lit8 v4, v4, 0x1

    goto :goto_1f

    .line 3100
    :cond_40
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/g;

    .line 3102
    invoke-virtual {v8}, Lsoftware/simplicial/a/g;->e()V

    .line 3103
    invoke-virtual {v4}, Lsoftware/simplicial/a/g;->e()V

    .line 3105
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->bz:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, v22

    iput v5, v0, Lsoftware/simplicial/a/bf;->bz:I

    .line 3106
    const/16 v5, 0x4b

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6}, Lsoftware/simplicial/a/bf;->b(ILsoftware/simplicial/a/am;)V

    .line 3108
    const/16 v5, 0x28

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lsoftware/simplicial/a/bf;->d(I)V

    .line 3109
    move-object/from16 v0, v22

    iget v5, v0, Lsoftware/simplicial/a/bf;->T:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_41

    .line 3110
    const/high16 v5, 0x3e800000    # 0.25f

    move-object/from16 v0, v22

    iput v5, v0, Lsoftware/simplicial/a/bf;->T:F

    .line 3111
    :cond_41
    const/4 v5, 0x1

    move-object/from16 v0, v22

    iput-boolean v5, v0, Lsoftware/simplicial/a/bf;->aj:Z

    .line 3112
    move-object/from16 v0, v22

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    instance-of v5, v5, Lsoftware/simplicial/a/bl;

    if-eqz v5, :cond_42

    .line 3113
    move-object/from16 v0, v22

    iget-object v5, v0, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    check-cast v5, Lsoftware/simplicial/a/bl;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lsoftware/simplicial/a/bl;->E:Z

    .line 3114
    :cond_42
    const/4 v5, 0x0

    :goto_20
    move-object/from16 v0, v22

    iget v6, v0, Lsoftware/simplicial/a/bf;->S:I

    if-ge v5, v6, :cond_44

    .line 3116
    move-object/from16 v0, v22

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v6, v6, v5

    .line 3117
    invoke-virtual/range {v20 .. v20}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v7

    if-eqz v7, :cond_43

    .line 3114
    :goto_21
    add-int/lit8 v5, v5, 0x1

    goto :goto_20

    .line 3120
    :cond_43
    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->d()F

    move-result v7

    .line 3121
    const/high16 v8, 0x40c00000    # 6.0f

    invoke-virtual {v6, v8}, Lsoftware/simplicial/a/bh;->c(F)V

    .line 3122
    invoke-virtual {v6, v7}, Lsoftware/simplicial/a/bh;->b(F)V

    .line 3123
    iget v7, v4, Lsoftware/simplicial/a/g;->l:F

    iput v7, v6, Lsoftware/simplicial/a/bh;->l:F

    .line 3124
    iget v7, v4, Lsoftware/simplicial/a/g;->m:F

    iput v7, v6, Lsoftware/simplicial/a/bh;->m:F

    goto :goto_21

    .line 3128
    :cond_44
    move-object/from16 v0, v22

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    move/from16 v16, v0

    goto/16 :goto_2

    .line 3135
    :cond_45
    return-void

    :cond_46
    move-object/from16 v19, v4

    goto/16 :goto_6

    .line 2784
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2829
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 2892
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 2932
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 3069
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private x()V
    .locals 14

    .prologue
    const/4 v9, 0x0

    const/4 v13, 0x1

    const/high16 v12, 0x40000000    # 2.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 3140
    move v0, v6

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_7

    .line 3142
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v1, v0

    .line 3144
    iget-boolean v1, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v1, :cond_1

    .line 3140
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v6

    .line 3147
    :goto_2
    iget v3, v2, Lsoftware/simplicial/a/bf;->S:I

    if-ge v1, v3, :cond_3

    .line 3149
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v3, v3, v1

    .line 3150
    invoke-virtual {v3}, Lsoftware/simplicial/a/bh;->f()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3152
    invoke-virtual {v2, v1}, Lsoftware/simplicial/a/bf;->b(I)V

    .line 3153
    add-int/lit8 v1, v1, -0x1

    .line 3154
    invoke-direct {p0, v3, v9}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;Lsoftware/simplicial/a/bh;)V

    .line 3147
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3157
    :cond_3
    iget v1, v2, Lsoftware/simplicial/a/bf;->S:I

    if-nez v1, :cond_4

    .line 3158
    iget v1, v2, Lsoftware/simplicial/a/bf;->aD:F

    iget v3, p0, Lsoftware/simplicial/a/bs;->ab:F

    add-float/2addr v1, v3

    iput v1, v2, Lsoftware/simplicial/a/bf;->aD:F

    .line 3159
    :cond_4
    iget v1, v2, Lsoftware/simplicial/a/bf;->S:I

    if-nez v1, :cond_5

    iget-boolean v1, v2, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v1, :cond_5

    .line 3161
    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->e()V

    .line 3162
    iput v7, v2, Lsoftware/simplicial/a/bf;->aD:F

    .line 3163
    iput-boolean v13, v2, Lsoftware/simplicial/a/bf;->aj:Z

    .line 3166
    :cond_5
    invoke-virtual {v2}, Lsoftware/simplicial/a/bf;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3168
    invoke-direct {p0, v2}, Lsoftware/simplicial/a/bs;->g(Lsoftware/simplicial/a/bf;)V

    .line 3169
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v1

    if-nez v1, :cond_6

    instance-of v1, v2, Lsoftware/simplicial/a/i;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v1, v3, :cond_0

    .line 3170
    :cond_6
    invoke-direct {p0, v2, v13, v13, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    goto :goto_1

    .line 3174
    :cond_7
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v4, v0, 0x14

    .line 3177
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->x:[I

    aput v6, v0, v4

    move v0, v6

    .line 3178
    :goto_3
    iget v1, p0, Lsoftware/simplicial/a/bs;->ay:I

    if-ge v0, v1, :cond_9

    .line 3180
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ah:[Lsoftware/simplicial/a/ae;

    aget-object v1, v1, v0

    .line 3183
    invoke-virtual {v1}, Lsoftware/simplicial/a/ae;->f()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3185
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->x:[I

    aget v3, v2, v4

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v4

    .line 3187
    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ae;)V

    .line 3178
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 3192
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->y:[I

    aput v6, v0, v4

    move v0, v6

    .line 3193
    :goto_4
    iget v1, p0, Lsoftware/simplicial/a/bs;->aA:I

    if-ge v0, v1, :cond_f

    .line 3195
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ag:[Lsoftware/simplicial/a/bt;

    aget-object v5, v1, v0

    .line 3198
    invoke-virtual {v5}, Lsoftware/simplicial/a/bt;->f()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 3200
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->y:[I

    aget v2, v1, v4

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v4

    .line 3204
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v1

    const v2, 0x3f2aaaab

    cmpg-float v1, v1, v2

    if-gez v1, :cond_e

    .line 3206
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v2, p0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v1, v2

    div-float v2, v1, v12

    .line 3207
    iget v1, p0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v1, v2

    :goto_5
    move v3, v6

    .line 3218
    :cond_a
    add-int/lit8 v3, v3, 0x1

    .line 3219
    const/high16 v8, 0x40600000    # 3.5f

    invoke-virtual {p0, v8, v2, v1}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v8

    iput v8, v5, Lsoftware/simplicial/a/bt;->l:F

    .line 3220
    const/high16 v8, 0x40600000    # 3.5f

    invoke-virtual {p0, v8, v2, v1}, Lsoftware/simplicial/a/bs;->a(FFF)F

    move-result v8

    iput v8, v5, Lsoftware/simplicial/a/bt;->m:F

    .line 3221
    iget-object v8, v5, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    sget-object v10, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    if-eq v8, v10, :cond_b

    .line 3222
    iget-object v8, p0, Lsoftware/simplicial/a/bs;->aD:Lsoftware/simplicial/a/bv;

    iput-object v8, v5, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    .line 3224
    :cond_b
    const/16 v8, 0xa

    if-ge v3, v8, :cond_c

    invoke-direct {p0, v5}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/ao;

    move-result-object v8

    if-nez v8, :cond_a

    invoke-direct {p0, v5}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/ao;)Lsoftware/simplicial/a/z;

    move-result-object v8

    if-nez v8, :cond_a

    .line 3226
    :cond_c
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3193
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 3212
    :cond_e
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v1, v7

    move v2, v7

    goto :goto_5

    :cond_f
    move v1, v6

    .line 3231
    :goto_6
    iget v0, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v1, v0, :cond_13

    .line 3233
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v2, v0, v1

    .line 3236
    iget-object v0, v2, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    sget-object v3, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    if-ne v0, v3, :cond_10

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v3, p0, Lsoftware/simplicial/a/bs;->p:I

    sub-int/2addr v0, v3

    const/16 v3, 0x3c

    if-lt v0, v3, :cond_10

    .line 3237
    invoke-direct {p0, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bl;)V

    .line 3238
    :cond_10
    iget-object v0, v2, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    if-eqz v0, :cond_12

    iget-object v0, v2, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    invoke-virtual {v0}, Lsoftware/simplicial/a/ao;->f()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 3240
    iget-object v0, v2, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    instance-of v0, v0, Lsoftware/simplicial/a/bh;

    if-eqz v0, :cond_11

    .line 3241
    iget-object v0, v2, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    check-cast v0, Lsoftware/simplicial/a/bh;

    const/4 v3, -0x1

    iput-byte v3, v0, Lsoftware/simplicial/a/bh;->I:B

    .line 3242
    :cond_11
    iput-object v9, v2, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 3231
    :cond_12
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 3247
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->z:[I

    aput v6, v0, v4

    move v0, v6

    .line 3248
    :goto_7
    iget v1, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v0, v1, :cond_15

    .line 3250
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v1, v1, v0

    .line 3252
    invoke-virtual {v1}, Lsoftware/simplicial/a/g;->f()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 3254
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->z:[I

    aget v3, v2, v4

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v4

    .line 3256
    const v2, 0x38d1b717    # 1.0E-4f

    invoke-direct {p0, v1, v2}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/g;F)V

    .line 3248
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_15
    move v8, v6

    .line 3261
    :goto_8
    iget v0, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v8, v0, :cond_18

    .line 3263
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v0, v0, v8

    .line 3266
    invoke-virtual {v0}, Lsoftware/simplicial/a/z;->f()Z

    move-result v1

    if-nez v1, :cond_16

    .line 3261
    :goto_9
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_8

    .line 3269
    :cond_16
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v10, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v5, v10, :cond_17

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v8

    :goto_a
    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/a/z;->a(Ljava/util/Random;F[Lsoftware/simplicial/a/z;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/bx;)V

    goto :goto_9

    :cond_17
    move-object v5, v9

    goto :goto_a

    :cond_18
    move v0, v6

    .line 3273
    :goto_b
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_25

    .line 3275
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v8, v1, v0

    .line 3277
    iget-boolean v1, v8, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v1, :cond_1a

    .line 3273
    :cond_19
    :goto_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 3280
    :cond_1a
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    invoke-virtual {v8, v1, v2, v13}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Z)V

    .line 3282
    iget-object v1, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v1, :cond_1d

    .line 3284
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_1b

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_1b

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_22

    .line 3285
    :cond_1b
    iget-object v1, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v1, Lsoftware/simplicial/a/bx;->e:I

    invoke-virtual {v8}, Lsoftware/simplicial/a/bf;->d()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Lsoftware/simplicial/a/bx;->e:I

    .line 3289
    :cond_1c
    :goto_d
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_1d

    iget-boolean v1, v8, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v1, :cond_1d

    .line 3290
    iget-object v1, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v1, Lsoftware/simplicial/a/bx;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/bx;->d:I

    .line 3293
    :cond_1d
    iget v1, v8, Lsoftware/simplicial/a/bf;->S:I

    if-lez v1, :cond_24

    .line 3295
    iget v2, p0, Lsoftware/simplicial/a/bs;->aE:F

    .line 3296
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    .line 3300
    iput v7, v8, Lsoftware/simplicial/a/bf;->l:F

    .line 3301
    iput v7, v8, Lsoftware/simplicial/a/bf;->m:F

    move v3, v7

    move v4, v1

    move v5, v2

    move v2, v7

    move v1, v6

    .line 3302
    :goto_e
    iget v9, v8, Lsoftware/simplicial/a/bf;->S:I

    if-ge v1, v9, :cond_23

    .line 3304
    iget-object v9, v8, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v9, v9, v1

    .line 3305
    iget v10, v9, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v11

    sub-float/2addr v10, v11

    cmpg-float v10, v10, v5

    if-gez v10, :cond_1e

    .line 3306
    iget v5, v9, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    sub-float/2addr v5, v10

    .line 3307
    :cond_1e
    iget v10, v9, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v11

    add-float/2addr v10, v11

    cmpl-float v10, v10, v3

    if-lez v10, :cond_1f

    .line 3308
    iget v3, v9, Lsoftware/simplicial/a/bh;->l:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    add-float/2addr v3, v10

    .line 3309
    :cond_1f
    iget v10, v9, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v11

    sub-float/2addr v10, v11

    cmpg-float v10, v10, v4

    if-gez v10, :cond_20

    .line 3310
    iget v4, v9, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v10

    sub-float/2addr v4, v10

    .line 3311
    :cond_20
    iget v10, v9, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v11

    add-float/2addr v10, v11

    cmpl-float v10, v10, v2

    if-lez v10, :cond_21

    .line 3312
    iget v2, v9, Lsoftware/simplicial/a/bh;->m:F

    invoke-virtual {v9}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v9

    add-float/2addr v2, v9

    .line 3302
    :cond_21
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 3286
    :cond_22
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_1c

    .line 3287
    iget-object v1, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v1, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lsoftware/simplicial/a/bx;->e:I

    goto/16 :goto_d

    .line 3314
    :cond_23
    add-float v1, v3, v5

    div-float/2addr v1, v12

    iput v1, v8, Lsoftware/simplicial/a/bf;->l:F

    .line 3315
    add-float v1, v2, v4

    div-float/2addr v1, v12

    iput v1, v8, Lsoftware/simplicial/a/bf;->m:F

    goto/16 :goto_c

    .line 3317
    :cond_24
    iget v1, v8, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_19

    .line 3319
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    div-float/2addr v1, v12

    iput v1, v8, Lsoftware/simplicial/a/bf;->l:F

    .line 3320
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    div-float/2addr v1, v12

    iput v1, v8, Lsoftware/simplicial/a/bf;->m:F

    goto/16 :goto_c

    .line 3323
    :cond_25
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_26

    move v0, v6

    .line 3325
    :goto_f
    iget v1, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v0, v1, :cond_26

    .line 3327
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v1, v1, v0

    .line 3328
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v2, v2, v0

    .line 3329
    iget v3, v1, Lsoftware/simplicial/a/z;->n:F

    iget v1, v1, Lsoftware/simplicial/a/z;->n:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v2, Lsoftware/simplicial/a/bx;->e:I

    .line 3325
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_26
    move v0, v6

    .line 3334
    :goto_10
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_29

    .line 3336
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 3338
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v2, :cond_28

    .line 3334
    :cond_27
    :goto_11
    add-int/lit8 v6, v0, 0x1

    move v0, v6

    goto :goto_10

    .line 3341
    :cond_28
    instance-of v2, v1, Lsoftware/simplicial/a/i;

    if-nez v2, :cond_27

    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v2, :cond_27

    .line 3342
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-wide v4, p0, Lsoftware/simplicial/a/bs;->aO:J

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    invoke-virtual/range {v1 .. v6}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;JLsoftware/simplicial/a/i/a;)V

    goto :goto_11

    .line 3344
    :cond_29
    return-void
.end method

.method private y()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 4003
    move v0, v1

    move v2, v1

    move v3, v1

    .line 4005
    :goto_0
    iget v4, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v4, :cond_3

    .line 4007
    iget-object v4, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v4, v4, v0

    .line 4009
    iget-boolean v5, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v5, :cond_1

    .line 4005
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4012
    :cond_1
    iget-boolean v5, v4, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v5, :cond_0

    .line 4015
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v5, v5, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v5, v5, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v6, v4, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4016
    add-int/lit8 v3, v3, 0x1

    .line 4017
    :cond_2
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v5, v5, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v5, v5, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v4, v4, Lsoftware/simplicial/a/bf;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4018
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4020
    :cond_3
    if-eqz v3, :cond_4

    if-nez v2, :cond_5

    :cond_4
    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method private z()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 4025
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    .line 4028
    :goto_0
    iget v6, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v6, :cond_3

    .line 4030
    iget-object v6, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v6, v6, v0

    .line 4032
    iget-boolean v7, v6, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v7, :cond_1

    .line 4028
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4035
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 4036
    iget-boolean v7, v6, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v7, :cond_2

    .line 4037
    add-int/lit8 v4, v4, 0x1

    .line 4038
    :cond_2
    iget-boolean v6, v6, Lsoftware/simplicial/a/bf;->aA:Z

    if-eqz v6, :cond_0

    .line 4039
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4041
    :cond_3
    if-gt v4, v5, :cond_5

    if-lt v3, v5, :cond_5

    if-gt v2, v5, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_5

    :cond_4
    move v1, v5

    :cond_5
    return v1
.end method


# virtual methods
.method protected a()V
    .locals 13

    .prologue
    const/high16 v12, 0x3f000000    # 0.5f

    const v11, 0x3f5eb852    # 0.87f

    const/4 v1, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 1158
    move v0, v1

    :goto_0
    iget v2, p0, Lsoftware/simplicial/a/bs;->ap:I

    if-ge v0, v2, :cond_2

    .line 1160
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aq:[Lsoftware/simplicial/a/ca;

    aget-object v2, v2, v0

    .line 1163
    iget v3, v2, Lsoftware/simplicial/a/ca;->z:F

    iget v4, v2, Lsoftware/simplicial/a/ca;->z:F

    mul-float/2addr v3, v4

    iget v4, v2, Lsoftware/simplicial/a/ca;->A:F

    iget v5, v2, Lsoftware/simplicial/a/ca;->A:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    cmpl-float v3, v3, v10

    if-lez v3, :cond_0

    .line 1165
    iget v3, v2, Lsoftware/simplicial/a/ca;->z:F

    mul-float/2addr v3, v11

    iput v3, v2, Lsoftware/simplicial/a/ca;->z:F

    .line 1166
    iget v3, v2, Lsoftware/simplicial/a/ca;->A:F

    mul-float/2addr v3, v11

    iput v3, v2, Lsoftware/simplicial/a/ca;->A:F

    .line 1168
    :cond_0
    iget v3, v2, Lsoftware/simplicial/a/ca;->k:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3e00adfd

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 1170
    iget v3, v2, Lsoftware/simplicial/a/ca;->k:F

    mul-float/2addr v3, v11

    iput v3, v2, Lsoftware/simplicial/a/ca;->k:F

    .line 1172
    :cond_1
    iget v3, v2, Lsoftware/simplicial/a/ca;->l:F

    iget v4, v2, Lsoftware/simplicial/a/ca;->z:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->l:F

    .line 1173
    iget v3, v2, Lsoftware/simplicial/a/ca;->m:F

    iget v4, v2, Lsoftware/simplicial/a/ca;->A:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->m:F

    .line 1174
    iget v3, v2, Lsoftware/simplicial/a/ca;->j:F

    iget v4, v2, Lsoftware/simplicial/a/ca;->k:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->j:F

    .line 1175
    iget v3, v2, Lsoftware/simplicial/a/ca;->l:F

    const v4, 0x429c8000    # 78.25f

    iget v5, v2, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->b:F

    .line 1176
    iget v3, v2, Lsoftware/simplicial/a/ca;->m:F

    const v4, 0x429c8000    # 78.25f

    iget v5, v2, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->c:F

    .line 1177
    iget v3, v2, Lsoftware/simplicial/a/ca;->l:F

    const v4, 0x429c8000    # 78.25f

    iget v5, v2, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->d:F

    .line 1178
    iget v3, v2, Lsoftware/simplicial/a/ca;->m:F

    const v4, 0x429c8000    # 78.25f

    iget v5, v2, Lsoftware/simplicial/a/ca;->j:F

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ca;->e:F

    .line 1158
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 1182
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 1184
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v2, v0

    .line 1187
    iget-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->b:Lsoftware/simplicial/a/bq$a;

    if-ne v3, v4, :cond_5

    .line 1188
    iget v3, v2, Lsoftware/simplicial/a/bq;->k:F

    iget v4, p0, Lsoftware/simplicial/a/bs;->ab:F

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->k:F

    .line 1192
    :cond_3
    :goto_2
    iget-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v4, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-ne v3, v4, :cond_6

    .line 1182
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1189
    :cond_5
    iget v3, v2, Lsoftware/simplicial/a/bq;->k:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3

    .line 1190
    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v2, Lsoftware/simplicial/a/bq;->k:F

    goto :goto_2

    .line 1196
    :cond_6
    iget-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    if-nez v3, :cond_4

    .line 1198
    iget v3, v2, Lsoftware/simplicial/a/bq;->k:F

    const v4, 0x3f2aaaab

    cmpl-float v3, v3, v4

    if-lez v3, :cond_7

    .line 1200
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    const v4, 0x3ee66666    # 0.45f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->a:F

    .line 1201
    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    const v4, 0x3ee66666    # 0.45f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->b:F

    .line 1203
    :cond_7
    iget v3, v2, Lsoftware/simplicial/a/bq;->k:F

    const v4, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_a

    .line 1205
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    const v4, 0x3f266666    # 0.65f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->a:F

    .line 1206
    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    const v4, 0x3f266666    # 0.65f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->b:F

    .line 1213
    :cond_8
    :goto_4
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v10

    if-gez v3, :cond_9

    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v10

    if-gez v3, :cond_9

    .line 1215
    const/4 v3, 0x1

    iput-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    .line 1216
    const/4 v3, 0x1

    iput-boolean v3, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 1217
    iput v9, v2, Lsoftware/simplicial/a/bq;->a:F

    .line 1218
    iput v9, v2, Lsoftware/simplicial/a/bq;->b:F

    .line 1221
    :cond_9
    iget v3, v2, Lsoftware/simplicial/a/bq;->l:F

    iget v4, v2, Lsoftware/simplicial/a/bq;->a:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 1222
    iget v3, v2, Lsoftware/simplicial/a/bq;->m:F

    iget v4, v2, Lsoftware/simplicial/a/bq;->b:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    goto :goto_3

    .line 1208
    :cond_a
    iget v3, v2, Lsoftware/simplicial/a/bq;->k:F

    cmpl-float v3, v3, v9

    if-lez v3, :cond_8

    .line 1210
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->a:F

    .line 1211
    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/bq;->b:F

    goto :goto_4

    :cond_b
    move v2, v1

    .line 1227
    :goto_5
    iget v0, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v2, v0, :cond_15

    .line 1229
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v3, v0, v2

    .line 1233
    iget-boolean v0, v3, Lsoftware/simplicial/a/bl;->C:Z

    if-nez v0, :cond_11

    .line 1235
    iget v0, v3, Lsoftware/simplicial/a/bl;->i:F

    float-to-double v4, v0

    const-wide/high16 v6, 0x4004000000000000L    # 2.5

    cmpl-double v0, v4, v6

    if-lez v0, :cond_12

    .line 1237
    iget v0, v3, Lsoftware/simplicial/a/bl;->c:F

    mul-float/2addr v0, v12

    iput v0, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 1238
    iget v0, v3, Lsoftware/simplicial/a/bl;->d:F

    mul-float/2addr v0, v12

    iput v0, v3, Lsoftware/simplicial/a/bl;->d:F

    .line 1250
    :cond_c
    :goto_6
    iget v0, v3, Lsoftware/simplicial/a/bl;->c:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v10

    if-gez v0, :cond_14

    iget v0, v3, Lsoftware/simplicial/a/bl;->d:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v10

    if-gez v0, :cond_14

    iget v0, v3, Lsoftware/simplicial/a/bl;->i:F

    cmpl-float v0, v0, v10

    if-ltz v0, :cond_14

    .line 1252
    const/4 v0, 0x1

    iput-boolean v0, v3, Lsoftware/simplicial/a/bl;->C:Z

    .line 1253
    const/4 v0, 0x0

    iput-object v0, v3, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    .line 1254
    iput v9, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 1255
    iput v9, v3, Lsoftware/simplicial/a/bl;->d:F

    .line 1256
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    sget-object v4, Lsoftware/simplicial/a/bl$a;->c:Lsoftware/simplicial/a/bl$a;

    if-ne v0, v4, :cond_d

    .line 1257
    sget-object v0, Lsoftware/simplicial/a/bl$a;->a:Lsoftware/simplicial/a/bl$a;

    iput-object v0, v3, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 1258
    :cond_d
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    instance-of v0, v0, Lsoftware/simplicial/a/bh;

    if-eqz v0, :cond_e

    .line 1259
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    check-cast v0, Lsoftware/simplicial/a/bh;

    const/4 v4, -0x1

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->I:B

    .line 1260
    :cond_e
    const/4 v0, 0x0

    iput-object v0, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 1261
    const/4 v0, 0x0

    iput-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 1268
    :cond_f
    :goto_7
    iget v0, v3, Lsoftware/simplicial/a/bl;->i:F

    iget v4, p0, Lsoftware/simplicial/a/bs;->ab:F

    add-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->i:F

    .line 1269
    iget v0, v3, Lsoftware/simplicial/a/bl;->l:F

    iget v4, v3, Lsoftware/simplicial/a/bl;->c:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->l:F

    .line 1270
    iget v0, v3, Lsoftware/simplicial/a/bl;->m:F

    iget v4, v3, Lsoftware/simplicial/a/bl;->d:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->m:F

    .line 1272
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    if-eqz v0, :cond_10

    .line 1274
    iget v0, v3, Lsoftware/simplicial/a/bl;->l:F

    iget-object v4, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v4, v4, Lsoftware/simplicial/a/ao;->l:F

    sub-float/2addr v0, v4

    .line 1275
    iget v4, v3, Lsoftware/simplicial/a/bl;->m:F

    iget-object v5, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v5, v5, Lsoftware/simplicial/a/ao;->m:F

    sub-float/2addr v4, v5

    .line 1276
    mul-float v5, v0, v0

    mul-float v6, v4, v4

    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v6

    double-to-float v5, v6

    .line 1277
    const v6, 0x444e4000    # 825.0f

    iget v7, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v6, v7

    iget-object v7, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v7, v7, Lsoftware/simplicial/a/ao;->n:F

    div-float/2addr v6, v7

    cmpl-float v6, v5, v6

    if-lez v6, :cond_10

    .line 1279
    iget-object v6, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v7, v6, Lsoftware/simplicial/a/ao;->l:F

    const v8, 0x444e4000    # 825.0f

    mul-float/2addr v0, v8

    div-float/2addr v0, v5

    iget v8, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v0, v8

    iget-object v8, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v8, v8, Lsoftware/simplicial/a/ao;->n:F

    div-float/2addr v0, v8

    add-float/2addr v0, v7

    iput v0, v6, Lsoftware/simplicial/a/ao;->l:F

    .line 1280
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v6, v0, Lsoftware/simplicial/a/ao;->m:F

    const v7, 0x444e4000    # 825.0f

    mul-float/2addr v4, v7

    div-float/2addr v4, v5

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    iget-object v5, v3, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    iget v5, v5, Lsoftware/simplicial/a/ao;->n:F

    div-float/2addr v4, v5

    add-float/2addr v4, v6

    iput v4, v0, Lsoftware/simplicial/a/ao;->m:F

    .line 1284
    :cond_10
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    if-eqz v0, :cond_11

    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v0, :cond_11

    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/bh;->J:B

    const/16 v4, 0xf

    if-ge v0, v4, :cond_11

    .line 1286
    iget v0, v3, Lsoftware/simplicial/a/bl;->l:F

    iget-object v4, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget v4, v4, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v0, v4

    .line 1287
    iget v4, v3, Lsoftware/simplicial/a/bl;->m:F

    iget-object v5, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget v5, v5, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v4, v5

    .line 1288
    iget-object v5, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget v6, v5, Lsoftware/simplicial/a/bh;->l:F

    const/high16 v7, 0x41700000    # 15.0f

    div-float/2addr v0, v7

    add-float/2addr v0, v6

    iput v0, v5, Lsoftware/simplicial/a/bh;->l:F

    .line 1289
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget v3, v0, Lsoftware/simplicial/a/bh;->m:F

    const/high16 v5, 0x41700000    # 15.0f

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v0, Lsoftware/simplicial/a/bh;->m:F

    .line 1227
    :cond_11
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_5

    .line 1240
    :cond_12
    iget v0, v3, Lsoftware/simplicial/a/bl;->i:F

    float-to-double v4, v0

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    cmpl-double v0, v4, v6

    if-lez v0, :cond_13

    .line 1242
    iget v0, v3, Lsoftware/simplicial/a/bl;->c:F

    const v4, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 1243
    iget v0, v3, Lsoftware/simplicial/a/bl;->d:F

    const v4, 0x3f59999a    # 0.85f

    mul-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->d:F

    goto/16 :goto_6

    .line 1245
    :cond_13
    iget v0, v3, Lsoftware/simplicial/a/bl;->i:F

    cmpl-float v0, v0, v9

    if-ltz v0, :cond_c

    .line 1247
    iget v0, v3, Lsoftware/simplicial/a/bl;->c:F

    const v4, 0x3f733333    # 0.95f

    mul-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 1248
    iget v0, v3, Lsoftware/simplicial/a/bl;->d:F

    const v4, 0x3f733333    # 0.95f

    mul-float/2addr v0, v4

    iput v0, v3, Lsoftware/simplicial/a/bl;->d:F

    goto/16 :goto_6

    .line 1263
    :cond_14
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    sget-object v4, Lsoftware/simplicial/a/bl$b;->j:Lsoftware/simplicial/a/bl$b;

    if-ne v0, v4, :cond_f

    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    iget-byte v0, v0, Lsoftware/simplicial/a/bh;->J:B

    if-gtz v0, :cond_f

    .line 1265
    iget-object v0, v3, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    const/4 v4, 0x1

    iput-byte v4, v0, Lsoftware/simplicial/a/bh;->J:B

    goto/16 :goto_7

    :cond_15
    move v0, v1

    .line 1295
    :goto_8
    iget v2, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v0, v2, :cond_1e

    .line 1297
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v2, v2, v0

    .line 1301
    iget-object v3, v2, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-nez v3, :cond_18

    .line 1303
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    iget v4, p0, Lsoftware/simplicial/a/bs;->ab:F

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->i:F

    .line 1304
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_1b

    .line 1306
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    cmpl-float v3, v3, v10

    if-lez v3, :cond_19

    .line 1308
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    const v4, 0x3f0ccccd    # 0.55f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1309
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    const v4, 0x3f0ccccd    # 0.55f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    .line 1341
    :cond_16
    :goto_9
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v10

    if-gez v3, :cond_17

    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v10

    if-gez v3, :cond_17

    .line 1343
    iput v9, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1344
    iput v9, v2, Lsoftware/simplicial/a/ag;->d:F

    .line 1346
    :cond_17
    iget v3, v2, Lsoftware/simplicial/a/ag;->l:F

    iget v4, v2, Lsoftware/simplicial/a/ag;->c:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->l:F

    .line 1347
    iget v3, v2, Lsoftware/simplicial/a/ag;->m:F

    iget v4, v2, Lsoftware/simplicial/a/ag;->d:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->m:F

    .line 1295
    :cond_18
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1311
    :cond_19
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    const v4, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1a

    .line 1313
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    const v4, 0x3f51eb85    # 0.82f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1314
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    const v4, 0x3f51eb85    # 0.82f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    goto :goto_9

    .line 1316
    :cond_1a
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    cmpl-float v3, v3, v9

    if-lez v3, :cond_16

    .line 1318
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    const v4, 0x3f68f5c3    # 0.91f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1319
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    const v4, 0x3f68f5c3    # 0.91f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    goto :goto_9

    .line 1324
    :cond_1b
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    cmpl-float v3, v3, v10

    if-lez v3, :cond_1c

    .line 1326
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    mul-float/2addr v3, v12

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1327
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    mul-float/2addr v3, v12

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    goto :goto_9

    .line 1329
    :cond_1c
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    const v4, 0x3e4ccccd    # 0.2f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1d

    .line 1331
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1332
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    goto/16 :goto_9

    .line 1334
    :cond_1d
    iget v3, v2, Lsoftware/simplicial/a/ag;->i:F

    cmpl-float v3, v3, v9

    if-lez v3, :cond_16

    .line 1336
    iget v3, v2, Lsoftware/simplicial/a/ag;->c:F

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->c:F

    .line 1337
    iget v3, v2, Lsoftware/simplicial/a/ag;->d:F

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/ag;->d:F

    goto/16 :goto_9

    :cond_1e
    move v0, v1

    .line 1352
    :goto_a
    iget v2, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v0, v2, :cond_21

    .line 1354
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v2, v2, v0

    .line 1357
    iget v3, v2, Lsoftware/simplicial/a/g;->c:F

    iget v4, v2, Lsoftware/simplicial/a/g;->c:F

    mul-float/2addr v3, v4

    iget v4, v2, Lsoftware/simplicial/a/g;->d:F

    iget v5, v2, Lsoftware/simplicial/a/g;->d:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    const v4, 0x4131c71c

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1f

    .line 1359
    iget v3, v2, Lsoftware/simplicial/a/g;->c:F

    mul-float/2addr v3, v11

    iput v3, v2, Lsoftware/simplicial/a/g;->c:F

    .line 1360
    iget v3, v2, Lsoftware/simplicial/a/g;->d:F

    mul-float/2addr v3, v11

    iput v3, v2, Lsoftware/simplicial/a/g;->d:F

    .line 1362
    :cond_1f
    iget v3, v2, Lsoftware/simplicial/a/g;->l:F

    iget v4, v2, Lsoftware/simplicial/a/g;->c:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/g;->l:F

    .line 1363
    iget v3, v2, Lsoftware/simplicial/a/g;->m:F

    iget v4, v2, Lsoftware/simplicial/a/g;->d:F

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/g;->m:F

    .line 1364
    iget v3, v2, Lsoftware/simplicial/a/g;->n:F

    const v4, 0x417a6666    # 15.65f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_20

    .line 1366
    iget v3, v2, Lsoftware/simplicial/a/g;->n:F

    const v4, 0x417a6666    # 15.65f

    iget v5, p0, Lsoftware/simplicial/a/bs;->ab:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lsoftware/simplicial/a/g;->n:F

    .line 1367
    iget v3, v2, Lsoftware/simplicial/a/g;->n:F

    const v4, 0x417a6666    # 15.65f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_20

    .line 1369
    const v3, 0x417a6666    # 15.65f

    iput v3, v2, Lsoftware/simplicial/a/g;->n:F

    .line 1352
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1375
    :cond_21
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_22

    .line 1377
    :goto_b
    iget v0, p0, Lsoftware/simplicial/a/bs;->aw:I

    if-ge v1, v0, :cond_22

    .line 1379
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ax:[Lsoftware/simplicial/a/z;

    aget-object v0, v0, v1

    .line 1380
    iget v2, v0, Lsoftware/simplicial/a/z;->n:F

    iget v3, v0, Lsoftware/simplicial/a/z;->d:F

    iget v4, v0, Lsoftware/simplicial/a/z;->n:F

    sub-float/2addr v3, v4

    const v4, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v0, Lsoftware/simplicial/a/z;->n:F

    .line 1377
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 1383
    :cond_22
    return-void
.end method

.method public a(IZ)V
    .locals 12

    .prologue
    .line 6015
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 6016
    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aG:I

    add-int/2addr v1, p1

    iget v2, p0, Lsoftware/simplicial/a/bs;->aP:I

    if-le v1, v2, :cond_a

    iget v1, p0, Lsoftware/simplicial/a/bs;->q:I

    if-lez v1, :cond_a

    .line 6018
    const/4 v2, 0x0

    .line 6019
    const/4 v1, 0x0

    move v3, v1

    move-object v1, v2

    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v3, v2, :cond_3

    .line 6021
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v2, v2, v3

    .line 6023
    iget-boolean v4, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v4, :cond_1

    instance-of v4, v2, Lsoftware/simplicial/a/i;

    if-eqz v4, :cond_1

    if-eqz v0, :cond_0

    iget-object v4, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    if-nez v4, :cond_1

    :cond_0
    move-object v1, v2

    .line 6019
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 6015
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 6027
    :cond_3
    if-eqz v1, :cond_4

    .line 6028
    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->e(Lsoftware/simplicial/a/bf;)V

    goto :goto_0

    .line 6030
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 6066
    :cond_5
    sget-object v1, Lsoftware/simplicial/a/bs;->W:[Ljava/lang/String;

    aget-object v1, v1, v11

    const/4 v2, 0x0

    const-string v3, ""

    invoke-direct {p0, v0, v1, v2, v3}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 6068
    iget v1, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-lez v1, :cond_6

    .line 6069
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->G()Lsoftware/simplicial/a/bx;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    .line 6071
    :cond_6
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_10

    .line 6073
    :cond_7
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v1, :cond_8

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v1, :cond_8

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v1, :cond_8

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_9

    :cond_8
    iget-short v1, p0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v2, -0x1e

    if-gt v1, v2, :cond_a

    :cond_9
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v1, :cond_10

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v1, :cond_10

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v1, :cond_10

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-nez v1, :cond_10

    iget-short v1, p0, Lsoftware/simplicial/a/bs;->aH:S

    iget-short v2, p0, Lsoftware/simplicial/a/bs;->aX:S

    add-int/lit8 v2, v2, -0x3c

    if-ge v1, v2, :cond_10

    .line 6033
    :cond_a
    :goto_2
    iget v0, p0, Lsoftware/simplicial/a/bs;->aG:I

    add-int/2addr v0, p1

    iget v1, p0, Lsoftware/simplicial/a/bs;->aP:I

    if-ge v0, v1, :cond_b

    .line 6035
    const/4 v1, 0x0

    .line 6036
    const/4 v0, 0x0

    move v11, v0

    .line 6037
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    array-length v0, v0

    if-ge v11, v0, :cond_11

    .line 6039
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->c:[Lsoftware/simplicial/a/i;

    aget-object v0, v0, v11

    .line 6040
    iget-boolean v2, v0, Lsoftware/simplicial/a/i;->aF:Z

    if-nez v2, :cond_c

    .line 6046
    :goto_4
    if-nez v0, :cond_d

    .line 6080
    :cond_b
    :goto_5
    return-void

    .line 6037
    :cond_c
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_3

    .line 6050
    :cond_d
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v1, v1, Lsoftware/simplicial/a/i/a;->c:Z

    if-eqz v1, :cond_f

    .line 6051
    new-instance v1, Lsoftware/simplicial/a/bx;

    const-string v2, ""

    const/4 v3, 0x2

    const/4 v4, 0x0

    sget-object v5, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2, v3, v4, v5}, Lsoftware/simplicial/a/bx;-><init>(Ljava/lang/String;IFLsoftware/simplicial/a/am;)V

    iget v9, v1, Lsoftware/simplicial/a/bx;->g:I

    .line 6055
    :goto_6
    sget-object v1, Lsoftware/simplicial/a/bs;->V:[Ljava/lang/Integer;

    aget-object v1, v1, v11

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aW:Lsoftware/simplicial/a/ac;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v7, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-boolean v8, p0, Lsoftware/simplicial/a/bs;->aR:Z

    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->p()I

    move-result v10

    invoke-virtual/range {v0 .. v10}, Lsoftware/simplicial/a/i;->a(IIFFLsoftware/simplicial/a/ac;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;ZII)V

    .line 6059
    :cond_e
    sget-object v1, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    sget-object v3, Lsoftware/simplicial/a/f;->lk:[Lsoftware/simplicial/a/f;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lsoftware/simplicial/a/i;->V:Lsoftware/simplicial/a/e;

    .line 6061
    iget-object v1, v0, Lsoftware/simplicial/a/i;->U:Lsoftware/simplicial/a/e;

    iget-boolean v1, v1, Lsoftware/simplicial/a/e;->lw:Z

    if-nez v1, :cond_e

    .line 6063
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/i;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_5

    .line 6053
    :cond_f
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->o()I

    move-result v9

    goto :goto_6

    .line 6077
    :cond_10
    if-eqz p2, :cond_a

    .line 6078
    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    goto :goto_2

    :cond_11
    move-object v0, v1

    goto :goto_4
.end method

.method public a(Lsoftware/simplicial/a/f/ab;)V
    .locals 10

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3d4ccccd    # 0.05f

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 4819
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-gtz v0, :cond_1

    .line 4861
    :cond_0
    :goto_0
    return-void

    .line 4822
    :cond_1
    iget v0, p1, Lsoftware/simplicial/a/f/ab;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v1

    .line 4823
    if-eqz v1, :cond_0

    .line 4826
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/ab;->c:Z

    if-eqz v0, :cond_2

    iget-byte v0, p1, Lsoftware/simplicial/a/f/ab;->d:B

    iget-byte v2, v1, Lsoftware/simplicial/a/bf;->aL:B

    if-eq v0, v2, :cond_0

    .line 4829
    :cond_2
    iget v0, p1, Lsoftware/simplicial/a/f/ab;->b:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_3

    .line 4830
    iput v6, p1, Lsoftware/simplicial/a/f/ab;->b:F

    .line 4831
    :cond_3
    iget v0, p1, Lsoftware/simplicial/a/f/ab;->b:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    .line 4832
    iput v3, p1, Lsoftware/simplicial/a/f/ab;->b:F

    .line 4834
    :cond_4
    iget-byte v0, p1, Lsoftware/simplicial/a/f/ab;->d:B

    iput-byte v0, v1, Lsoftware/simplicial/a/bf;->aL:B

    .line 4835
    iget v0, p1, Lsoftware/simplicial/a/f/ab;->a:F

    iput v0, v1, Lsoftware/simplicial/a/bf;->M:F

    .line 4836
    iget v0, p1, Lsoftware/simplicial/a/f/ab;->b:F

    iput v0, v1, Lsoftware/simplicial/a/bf;->O:F

    .line 4838
    iget-byte v0, p1, Lsoftware/simplicial/a/f/ab;->e:B

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_7

    move v0, v4

    .line 4839
    :goto_1
    iget-byte v2, p1, Lsoftware/simplicial/a/f/ab;->e:B

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_8

    move v2, v4

    .line 4840
    :goto_2
    iget-byte v3, p1, Lsoftware/simplicial/a/f/ab;->e:B

    and-int/lit8 v3, v3, -0x4

    shr-int/lit8 v9, v3, 0x2

    .line 4842
    if-eqz v0, :cond_5

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v3, v1, Lsoftware/simplicial/a/bf;->aP:I

    sub-int/2addr v0, v3

    const/4 v3, 0x2

    if-lt v0, v3, :cond_5

    .line 4844
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, v1, Lsoftware/simplicial/a/bf;->aP:I

    .line 4845
    iget v0, v1, Lsoftware/simplicial/a/bf;->M:F

    invoke-direct {p0, v1, v0, v5, v7}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;FZF)V

    .line 4847
    :cond_5
    if-eqz v2, :cond_6

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v2, v1, Lsoftware/simplicial/a/bf;->aO:I

    sub-int/2addr v0, v2

    if-lt v0, v4, :cond_6

    .line 4849
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, v1, Lsoftware/simplicial/a/bf;->aO:I

    .line 4850
    invoke-virtual {v1}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v2

    .line 4851
    if-eqz v2, :cond_6

    .line 4852
    iget v3, v1, Lsoftware/simplicial/a/bf;->M:F

    const/4 v8, 0x0

    move-object v0, p0

    move v6, v5

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;FZZZFF)V

    .line 4854
    :cond_6
    if-eqz v9, :cond_0

    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v2, v1, Lsoftware/simplicial/a/bf;->aQ:I

    sub-int/2addr v0, v2

    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    .line 4856
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, v1, Lsoftware/simplicial/a/bf;->aQ:I

    .line 4857
    invoke-virtual {v1}, Lsoftware/simplicial/a/bf;->q()Lsoftware/simplicial/a/bh;

    move-result-object v0

    .line 4858
    if-eqz v0, :cond_0

    .line 4859
    iget-object v2, v0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    new-instance v3, Lsoftware/simplicial/a/a/x;

    iget v1, v1, Lsoftware/simplicial/a/bf;->C:I

    iget v0, v0, Lsoftware/simplicial/a/bh;->c:I

    invoke-direct {v3, v1, v0, v9}, Lsoftware/simplicial/a/a/x;-><init>(III)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    move v0, v5

    .line 4838
    goto :goto_1

    :cond_8
    move v2, v5

    .line 4839
    goto :goto_2
.end method

.method public a(Lsoftware/simplicial/a/f/ac;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5421
    iget v0, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v3

    .line 5422
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    iget v4, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bw;

    .line 5423
    iget v4, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-direct {p0, v4}, Lsoftware/simplicial/a/bs;->b(I)Lsoftware/simplicial/a/f/bl;

    move-result-object v4

    .line 5424
    if-eqz v4, :cond_0

    .line 5425
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 5427
    :cond_0
    if-nez v3, :cond_3

    if-nez v0, :cond_3

    .line 5429
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "Failed to find client %d for removal from game %d! Was client = %s"

    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/Object;

    iget v0, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    iget v0, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v7, 0x2

    if-eqz v4, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 5462
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 5429
    goto :goto_0

    .line 5432
    :cond_3
    if-eqz v3, :cond_6

    .line 5434
    iget-boolean v0, v3, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v0, :cond_5

    .line 5436
    iget-short v0, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v0, :cond_4

    move v0, v2

    .line 5437
    :goto_2
    iget v4, v3, Lsoftware/simplicial/a/bf;->S:I

    if-ge v0, v4, :cond_4

    .line 5438
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bh;)V

    .line 5437
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5439
    :cond_4
    invoke-direct {p0, v3}, Lsoftware/simplicial/a/bs;->g(Lsoftware/simplicial/a/bf;)V

    .line 5442
    :cond_5
    invoke-direct {p0, v3}, Lsoftware/simplicial/a/bs;->e(Lsoftware/simplicial/a/bf;)V

    .line 5444
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_6

    .line 5445
    invoke-virtual {p0, v2, v1}, Lsoftware/simplicial/a/bs;->a(IZ)V

    .line 5448
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_7

    .line 5453
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v1, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/b/a;->a(I)V

    .line 5456
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_1

    .line 5458
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v1, p1, Lsoftware/simplicial/a/f/ac;->ar:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/h/c;->a(I)V

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/f/ae;)V
    .locals 30

    .prologue
    .line 5599
    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/f/ae;->g:Lsoftware/simplicial/a/v;

    .line 5600
    if-nez v4, :cond_1

    .line 5724
    :cond_0
    :goto_0
    return-void

    .line 5603
    :cond_1
    iget-object v2, v4, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v2

    .line 5604
    if-eqz v2, :cond_2

    .line 5606
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Unexpected invocation of OnEnterGameRequestMessage on game %d for player %d already in game!"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v4, v4, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->a:I

    .line 5607
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    .line 5606
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 5611
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v2, :cond_5

    .line 5613
    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget-object v3, v3, Lsoftware/simplicial/a/b/a;->d:Ljava/util/List;

    invoke-static {v2, v3}, Lsoftware/simplicial/a/b/a;->a(ILjava/util/Collection;)Lsoftware/simplicial/a/b/h;

    move-result-object v2

    .line 5615
    if-nez v2, :cond_3

    .line 5617
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to find gladiator AID %d in gladiators list in arena. Client logged out but did not DC during validation."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v4, v4, Lsoftware/simplicial/a/v;->b:I

    .line 5618
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    .line 5617
    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 5621
    :cond_3
    iget-object v3, v2, Lsoftware/simplicial/a/b/h;->b:Lsoftware/simplicial/a/b/b;

    sget-object v5, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    if-eq v3, v5, :cond_4

    .line 5623
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "Gladiator (%d) already assigned to %d . How did %d get here?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v2, Lsoftware/simplicial/a/b/h;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v2, v2, Lsoftware/simplicial/a/b/h;->c:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x2

    iget-object v4, v4, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->a:I

    .line 5624
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v2

    .line 5623
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5628
    :cond_4
    iget-object v3, v4, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iput-object v3, v2, Lsoftware/simplicial/a/b/h;->c:Lsoftware/simplicial/a/f/bl;

    .line 5629
    sget-object v3, Lsoftware/simplicial/a/b/b;->c:Lsoftware/simplicial/a/b/b;

    iput-object v3, v2, Lsoftware/simplicial/a/b/h;->b:Lsoftware/simplicial/a/b/b;

    .line 5632
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_7

    .line 5634
    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v3, v3, Lsoftware/simplicial/a/i/a;->d:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    if-eq v2, v3, :cond_7

    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v3, v3, Lsoftware/simplicial/a/i/a;->e:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    if-eq v2, v3, :cond_7

    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v3, v3, Lsoftware/simplicial/a/i/a;->f:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    if-eq v2, v3, :cond_7

    iget v2, v4, Lsoftware/simplicial/a/v;->b:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-object v3, v3, Lsoftware/simplicial/a/i/a;->g:Lsoftware/simplicial/a/i/d;

    iget v3, v3, Lsoftware/simplicial/a/i/d;->a:I

    if-eq v2, v3, :cond_7

    .line 5637
    :cond_6
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to find contestant for AID %d in match."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v4, v4, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5642
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v2, :cond_a

    .line 5644
    const/4 v2, 0x0

    .line 5645
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v5, v4, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 5647
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v2, v2, Lsoftware/simplicial/a/h/c;->b:Lsoftware/simplicial/a/h/a;

    .line 5654
    :cond_8
    :goto_1
    if-nez v2, :cond_a

    .line 5656
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to find team for AID %d team arena."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v4, v4, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5649
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v3, v3, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    iget-object v3, v3, Lsoftware/simplicial/a/h/a;->c:Ljava/util/List;

    iget v5, v4, Lsoftware/simplicial/a/v;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 5651
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget-object v2, v2, Lsoftware/simplicial/a/h/c;->c:Lsoftware/simplicial/a/h/a;

    goto :goto_1

    .line 5661
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v2

    if-nez v2, :cond_b

    .line 5662
    const/4 v2, 0x1

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/a/bs;->a(IZ)V

    .line 5664
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/v;)Lsoftware/simplicial/a/bf;

    move-result-object v29

    .line 5665
    if-eqz v29, :cond_0

    .line 5668
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v29

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5670
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aj:I

    if-lez v2, :cond_14

    .line 5672
    const/4 v3, 0x0

    .line 5673
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v2, :cond_11

    .line 5675
    iget-object v2, v4, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 5677
    iget-object v2, v4, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 5678
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 5712
    :goto_2
    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_d

    .line 5713
    :cond_c
    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/a/bs;->G()Lsoftware/simplicial/a/bx;

    move-result-object v2

    .line 5714
    :cond_d
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    .line 5719
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    sget-object v3, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aY:I

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aP:I

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ba:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->bb:[B

    move-object/from16 v0, v29

    iget v11, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v13, :cond_15

    const/4 v13, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v14, :cond_16

    const/4 v14, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v15, :cond_17

    const/4 v15, 0x1

    :goto_6
    move-object/from16 v0, v29

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lsoftware/simplicial/a/az;->M:Z

    move/from16 v16, v0

    if-eqz v16, :cond_18

    move-object/from16 v0, v29

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-wide v0, v0, Lsoftware/simplicial/a/az;->b:J

    move-wide/from16 v16, v0

    :goto_7
    move-object/from16 v0, v29

    iget v0, v0, Lsoftware/simplicial/a/bf;->an:I

    move/from16 v18, v0

    move-object/from16 v0, v29

    iget-wide v0, v0, Lsoftware/simplicial/a/bf;->ap:J

    move-wide/from16 v20, v0

    .line 5721
    invoke-static/range {v20 .. v21}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v20, v0

    if-eqz v20, :cond_19

    const/16 v20, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move/from16 v21, v0

    move-object/from16 v0, v29

    iget v0, v0, Lsoftware/simplicial/a/bf;->A:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aT:Z

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    move-object/from16 v24, v0

    .line 5722
    invoke-interface/range {v24 .. v24}, Ljava/util/Map;->size()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->a:Lsoftware/simplicial/a/c/g;

    move-object/from16 v25, v0

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    move/from16 v26, v0

    if-eqz v26, :cond_1b

    const/16 v26, 0x1

    :goto_a
    const-wide/16 v27, 0x0

    .line 5719
    invoke-static/range {v2 .. v28}, Lsoftware/simplicial/a/f/af;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/ba;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/am;IIILjava/lang/String;[BILsoftware/simplicial/a/ap;ZZZJIIZZIZILsoftware/simplicial/a/c/g;ZJ)[B

    move-result-object v2

    move-object/from16 v0, v29

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->aM:[B

    .line 5723
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->d(Lsoftware/simplicial/a/bf;)V

    goto/16 :goto_0

    .line 5679
    :cond_e
    iget-object v2, v4, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v3, v3, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v3, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 5680
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    goto/16 :goto_2

    .line 5683
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 5684
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "Unmatched clanName for client entering clanwar! a %s g %d c %s la %s lb %s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v4, Lsoftware/simplicial/a/v;->e:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v4, v4, Lsoftware/simplicial/a/v;->f:Ljava/lang/String;

    aput-object v4, v6, v7

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v7, v7, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v7, v7, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v7, v6, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v7, v7, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v7, v7, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5690
    :cond_10
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "NULL clanName for client entering clanwar! a %s g %d c %s la %s lb %s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v4, v4, Lsoftware/simplicial/a/v;->e:Ljava/lang/String;

    aput-object v4, v6, v7

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, ""

    aput-object v7, v6, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v7, v7, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v7, v7, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v7, v6, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v7, v7, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v7, v7, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_2

    .line 5694
    :cond_11
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/ae;->f:I

    if-eqz v2, :cond_1c

    .line 5696
    const/4 v2, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v5, :cond_1c

    .line 5698
    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v5, v5, v2

    .line 5700
    invoke-virtual {v5}, Lsoftware/simplicial/a/bf;->k()I

    move-result v6

    iget-object v7, v4, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v7, v7, Lsoftware/simplicial/a/f/bl;->a:I

    if-ne v6, v7, :cond_13

    .line 5696
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 5703
    :cond_13
    iget-boolean v6, v5, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v6, :cond_12

    invoke-virtual {v5}, Lsoftware/simplicial/a/bf;->k()I

    move-result v6

    move-object/from16 v0, p1

    iget v7, v0, Lsoftware/simplicial/a/f/ae;->f:I

    if-ne v6, v7, :cond_12

    iget-object v6, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v6, v6, Lsoftware/simplicial/a/bx;->f:I

    int-to-double v6, v6

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/a/bs;->aY:I

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/a/bs;->aj:I

    div-int/2addr v8, v9

    int-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    add-double/2addr v8, v10

    cmpg-double v6, v6, v8

    if-gez v6, :cond_12

    .line 5706
    iget-object v2, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto/16 :goto_2

    .line 5717
    :cond_14
    const/4 v2, 0x0

    move-object/from16 v0, v29

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto/16 :goto_3

    .line 5719
    :cond_15
    const/4 v13, 0x0

    goto/16 :goto_4

    :cond_16
    const/4 v14, 0x0

    goto/16 :goto_5

    :cond_17
    const/4 v15, 0x0

    goto/16 :goto_6

    :cond_18
    const-wide/16 v16, -0x1

    goto/16 :goto_7

    .line 5721
    :cond_19
    const/16 v20, 0x0

    goto/16 :goto_8

    .line 5722
    :cond_1a
    sget-object v25, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    goto/16 :goto_9

    :cond_1b
    const/16 v26, 0x0

    goto/16 :goto_a

    :cond_1c
    move-object v2, v3

    goto/16 :goto_2
.end method

.method public a(Lsoftware/simplicial/a/f/ax;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 4880
    iget v0, p1, Lsoftware/simplicial/a/f/ax;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 4881
    if-nez v0, :cond_1

    .line 4897
    :cond_0
    :goto_0
    return-void

    .line 4884
    :cond_1
    iget v1, p1, Lsoftware/simplicial/a/f/ax;->b:I

    iget-wide v2, p1, Lsoftware/simplicial/a/f/ax;->c:J

    iget-object v4, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    new-array v7, v9, [B

    iget-boolean v8, p0, Lsoftware/simplicial/a/bs;->aR:Z

    iget-object v10, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v10, :cond_2

    const/4 v9, 0x1

    :cond_2
    iget-boolean v10, p1, Lsoftware/simplicial/a/f/ax;->d:Z

    invoke-virtual/range {v0 .. v10}, Lsoftware/simplicial/a/bf;->a(IJLsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZZZ)V

    .line 4885
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->h(Lsoftware/simplicial/a/bf;)Z

    .line 4886
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->i(Lsoftware/simplicial/a/bf;)Z

    .line 4887
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->k(Lsoftware/simplicial/a/bf;)Z

    .line 4888
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->m(Lsoftware/simplicial/a/bf;)Z

    .line 4889
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->n(Lsoftware/simplicial/a/bf;)Z

    .line 4890
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->l(Lsoftware/simplicial/a/bf;)Z

    .line 4891
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->j(Lsoftware/simplicial/a/bf;)Z

    .line 4893
    iget-boolean v1, p1, Lsoftware/simplicial/a/f/ax;->d:Z

    if-eqz v1, :cond_0

    .line 4894
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget v2, v0, Lsoftware/simplicial/a/bf;->A:I

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v0, v0, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-interface {v1, p0, v2, v0}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;II)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/f/az;)V
    .locals 30

    .prologue
    .line 5197
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/az;->ar:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v28

    .line 5199
    if-nez v28, :cond_1

    .line 5201
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/az;->ar:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/bs;->e(I)Lsoftware/simplicial/a/bw;

    move-result-object v2

    .line 5202
    if-eqz v2, :cond_0

    .line 5204
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v5, v2, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v5, v5, Lsoftware/simplicial/a/f/bl;->b:I

    sget-object v6, Lsoftware/simplicial/a/f/ba;->n:Lsoftware/simplicial/a/f/ba;

    invoke-static {v4, v5, v6}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v4

    iget-object v2, v2, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v3, v4, v2}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 5274
    :cond_0
    :goto_0
    return-void

    .line 5216
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aN:I

    move-object/from16 v0, v28

    iget v3, v0, Lsoftware/simplicial/a/bf;->aR:I

    sub-int/2addr v2, v3

    const/16 v3, 0xa

    if-ge v2, v3, :cond_2

    .line 5218
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    invoke-virtual/range {v28 .. v28}, Lsoftware/simplicial/a/bf;->l()I

    move-result v4

    sget-object v5, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    invoke-static {v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v3

    move-object/from16 v0, v28

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v2, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_0

    .line 5222
    :cond_2
    move-object/from16 v0, v28

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 5223
    move-object/from16 v0, v28

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 5224
    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/f/az;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/az;->b:[B

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/f/az;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4, v5, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5227
    move-object/from16 v0, v28

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/az;->c:Lsoftware/simplicial/a/e;

    if-ne v4, v5, :cond_3

    move-object/from16 v0, v28

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/az;->d:Lsoftware/simplicial/a/af;

    if-ne v4, v5, :cond_3

    move-object/from16 v0, v28

    iget v4, v0, Lsoftware/simplicial/a/bf;->ae:I

    move-object/from16 v0, p1

    iget v5, v0, Lsoftware/simplicial/a/f/az;->e:I

    if-ne v4, v5, :cond_3

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/f/az;->a:Ljava/lang/String;

    .line 5228
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, v28

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->F:[B

    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/a/f/az;->b:[B

    invoke-static {v2, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, v28

    iget-byte v2, v0, Lsoftware/simplicial/a/bf;->Z:B

    move-object/from16 v0, p1

    iget-byte v4, v0, Lsoftware/simplicial/a/f/az;->f:B

    if-ne v2, v4, :cond_3

    move-object/from16 v0, v28

    iget v2, v0, Lsoftware/simplicial/a/bf;->af:I

    move-object/from16 v0, p1

    iget v4, v0, Lsoftware/simplicial/a/f/az;->h:I

    if-ne v2, v4, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/az;->g:Ljava/lang/String;

    .line 5229
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, v28

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/f/az;->i:Lsoftware/simplicial/a/as;

    if-eq v2, v3, :cond_4

    .line 5230
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    move-object/from16 v0, v28

    iget-object v3, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    move-object/from16 v0, v28

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lsoftware/simplicial/a/f/az;->b:[B

    move-object/from16 v0, p1

    iget-object v6, v0, Lsoftware/simplicial/a/f/az;->c:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p1

    iget-object v7, v0, Lsoftware/simplicial/a/f/az;->d:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p1

    iget v8, v0, Lsoftware/simplicial/a/f/az;->e:I

    move-object/from16 v0, p1

    iget-byte v9, v0, Lsoftware/simplicial/a/f/az;->f:B

    move-object/from16 v0, p1

    iget v10, v0, Lsoftware/simplicial/a/f/az;->h:I

    move-object/from16 v0, p1

    iget-object v11, v0, Lsoftware/simplicial/a/f/az;->g:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lsoftware/simplicial/a/f/az;->i:Lsoftware/simplicial/a/as;

    invoke-interface/range {v2 .. v12}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/f/bl;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 5232
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/az;->c:Lsoftware/simplicial/a/e;

    move-object/from16 v0, v28

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    .line 5233
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/az;->d:Lsoftware/simplicial/a/af;

    move-object/from16 v0, v28

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    .line 5234
    move-object/from16 v0, p1

    iget-byte v2, v0, Lsoftware/simplicial/a/f/az;->f:B

    move-object/from16 v0, v28

    iput-byte v2, v0, Lsoftware/simplicial/a/bf;->Z:B

    .line 5235
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/az;->e:I

    move-object/from16 v0, v28

    iput v2, v0, Lsoftware/simplicial/a/bf;->ae:I

    .line 5236
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/az;->b:[B

    move-object/from16 v0, v28

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->F:[B

    .line 5237
    move-object/from16 v0, p1

    iget v2, v0, Lsoftware/simplicial/a/f/az;->h:I

    move-object/from16 v0, v28

    iput v2, v0, Lsoftware/simplicial/a/bf;->af:I

    .line 5238
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/a/f/az;->i:Lsoftware/simplicial/a/as;

    move-object/from16 v0, v28

    iput-object v2, v0, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    .line 5240
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->d(Lsoftware/simplicial/a/bf;)V

    .line 5242
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_5

    move-object/from16 v0, v28

    iget-object v2, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v2, v2, Lsoftware/simplicial/a/bx;->c:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_f

    .line 5244
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_8

    :cond_7
    move-object/from16 v0, p0

    iget-short v2, v0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v3, -0x1e

    if-gt v2, v3, :cond_a

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-short v2, v0, Lsoftware/simplicial/a/bs;->aH:S

    move-object/from16 v0, p0

    iget-short v3, v0, Lsoftware/simplicial/a/bs;->aX:S

    add-int/lit8 v3, v3, -0x3c

    if-lt v2, v3, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v2, v3, :cond_f

    .line 5248
    :cond_a
    move-object/from16 v0, v28

    iget-boolean v2, v0, Lsoftware/simplicial/a/bf;->K:Z

    if-eqz v2, :cond_c

    .line 5250
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->h(Lsoftware/simplicial/a/bf;)Z

    .line 5251
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->i(Lsoftware/simplicial/a/bf;)Z

    .line 5252
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->k(Lsoftware/simplicial/a/bf;)Z

    .line 5253
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->m(Lsoftware/simplicial/a/bf;)Z

    .line 5254
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->n(Lsoftware/simplicial/a/bf;)Z

    .line 5255
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->l(Lsoftware/simplicial/a/bf;)Z

    .line 5256
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->j(Lsoftware/simplicial/a/bf;)Z

    .line 5257
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    invoke-virtual/range {v28 .. v28}, Lsoftware/simplicial/a/bf;->l()I

    move-result v3

    sget-object v4, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    move-object/from16 v0, v28

    iget v5, v0, Lsoftware/simplicial/a/bf;->C:I

    move-object/from16 v0, v28

    iget-object v6, v0, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    move-object/from16 v0, v28

    iget-object v7, v0, Lsoftware/simplicial/a/bf;->E:[B

    move-object/from16 v0, v28

    iget-object v8, v0, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    .line 5258
    invoke-virtual {v8}, Lsoftware/simplicial/a/e;->a()I

    move-result v8

    int-to-short v8, v8

    move-object/from16 v0, v28

    iget-object v9, v0, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    iget-byte v9, v9, Lsoftware/simplicial/a/af;->c:B

    move-object/from16 v0, v28

    iget v10, v0, Lsoftware/simplicial/a/bf;->ad:I

    move-object/from16 v0, v28

    iget v11, v0, Lsoftware/simplicial/a/bf;->ah:I

    move-object/from16 v0, v28

    iget-object v12, v0, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    move-object/from16 v0, v28

    iget v13, v0, Lsoftware/simplicial/a/bf;->A:I

    move-object/from16 v0, v28

    iget-object v14, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v14, v14, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v14, :cond_b

    move-object/from16 v0, v28

    iget-object v14, v0, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v14, v14, Lsoftware/simplicial/a/az;->b:J

    :goto_1
    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->I:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    move-object/from16 v18, v0

    move-object/from16 v0, v28

    iget v0, v0, Lsoftware/simplicial/a/bf;->an:I

    move/from16 v19, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Lsoftware/simplicial/a/bf;->ap:J

    move-wide/from16 v20, v0

    .line 5261
    invoke-static/range {v20 .. v21}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v20

    const/16 v21, 0x0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    move-object/from16 v22, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Lsoftware/simplicial/a/bf;->aq:J

    move-wide/from16 v24, v0

    .line 5263
    invoke-static/range {v24 .. v25}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v23

    const-wide/16 v24, 0x0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    move-object/from16 v26, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    move-object/from16 v28, v0

    .line 5257
    invoke-static/range {v2 .. v28}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;ILjava/lang/String;[BSBIILsoftware/simplicial/a/bx;IJLjava/lang/String;[BLsoftware/simplicial/a/q;IIZLsoftware/simplicial/a/s;IJLsoftware/simplicial/a/bd;Ljava/lang/String;Lsoftware/simplicial/a/as;)[B

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v29

    invoke-interface {v0, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    goto/16 :goto_0

    .line 5258
    :cond_b
    const-wide/16 v14, -0x1

    goto :goto_1

    .line 5265
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v2, v3, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v2, :cond_e

    .line 5266
    :cond_d
    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    goto/16 :goto_0

    .line 5268
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    invoke-virtual/range {v28 .. v28}, Lsoftware/simplicial/a/bf;->l()I

    move-result v4

    sget-object v5, Lsoftware/simplicial/a/f/ba;->h:Lsoftware/simplicial/a/f/ba;

    invoke-static {v3, v4, v5}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B

    move-result-object v3

    move-object/from16 v0, v28

    iget-object v4, v0, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v2, v3, v4}, Lsoftware/simplicial/a/f/bf;->a([BLsoftware/simplicial/a/f/bl;)V

    goto/16 :goto_0

    .line 5273
    :cond_f
    const/4 v3, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-short v2, v0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v2, :cond_10

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v3, v4, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;ZZZ)V

    goto/16 :goto_0

    :cond_10
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public a(Lsoftware/simplicial/a/f/bz;)V
    .locals 3

    .prologue
    .line 4865
    iget-byte v0, p1, Lsoftware/simplicial/a/f/bz;->a:B

    const/4 v1, -0x2

    if-lt v0, v1, :cond_0

    iget-byte v0, p1, Lsoftware/simplicial/a/f/bz;->a:B

    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-lt v0, v1, :cond_1

    .line 4876
    :cond_0
    :goto_0
    return-void

    .line 4868
    :cond_1
    iget v0, p1, Lsoftware/simplicial/a/f/bz;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v1

    .line 4869
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    iget v2, p1, Lsoftware/simplicial/a/f/bz;->ar:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bw;

    .line 4870
    if-eqz v1, :cond_2

    .line 4871
    iget-byte v0, p1, Lsoftware/simplicial/a/f/bz;->a:B

    iput-byte v0, v1, Lsoftware/simplicial/a/bf;->aJ:B

    goto :goto_0

    .line 4872
    :cond_2
    if-eqz v0, :cond_0

    .line 4873
    iget-byte v1, p1, Lsoftware/simplicial/a/f/bz;->a:B

    iput v1, v0, Lsoftware/simplicial/a/bw;->f:I

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/f/ca;)V
    .locals 29

    .prologue
    .line 5728
    move-object/from16 v0, p1

    iget-object v1, v0, Lsoftware/simplicial/a/f/ca;->h:Lsoftware/simplicial/a/v;

    .line 5729
    if-nez v1, :cond_1

    .line 5750
    :cond_0
    :goto_0
    return-void

    .line 5732
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    iget-object v3, v1, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5734
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Unexpected invocation of OnSpectateGameRequest on game %d for player %d already spectating game!"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v1, v1, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v1, v1, Lsoftware/simplicial/a/f/bl;->a:I

    .line 5735
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    .line 5734
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    goto :goto_0

    .line 5739
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/v;)Lsoftware/simplicial/a/bw;

    move-result-object v28

    .line 5740
    if-eqz v28, :cond_0

    .line 5743
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v28

    iget-object v2, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5745
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aY:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/a/bs;->aP:I

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->ba:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->bb:[B

    const/4 v10, -0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v12, :cond_3

    const/4 v12, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v13, :cond_4

    const/4 v13, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v14, :cond_5

    const/4 v14, 0x1

    :goto_3
    const-wide/16 v15, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    const/16 v19, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move/from16 v20, v0

    const/16 v21, -0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aT:Z

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    move-object/from16 v23, v0

    .line 5746
    invoke-interface/range {v23 .. v23}, Ljava/util/Map;->size()I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v24, v0

    if-eqz v24, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->a:Lsoftware/simplicial/a/c/g;

    move-object/from16 v24, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v25, v0

    if-eqz v25, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    move/from16 v25, v0

    if-eqz v25, :cond_8

    const/16 v25, 0x1

    :goto_6
    const-wide/16 v26, 0x0

    .line 5745
    invoke-static/range {v1 .. v27}, Lsoftware/simplicial/a/f/af;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/ba;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/am;IIILjava/lang/String;[BILsoftware/simplicial/a/ap;ZZZJIIZZIZILsoftware/simplicial/a/c/g;ZJ)[B

    move-result-object v1

    move-object/from16 v0, v28

    iput-object v1, v0, Lsoftware/simplicial/a/bw;->h:[B

    .line 5749
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    move-object/from16 v0, v28

    iget v2, v0, Lsoftware/simplicial/a/bw;->k:I

    move-object/from16 v0, v28

    iget-object v3, v0, Lsoftware/simplicial/a/bw;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->a:I

    move-object/from16 v0, p0

    invoke-interface {v1, v0, v2, v3}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;II)V

    goto/16 :goto_0

    .line 5745
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v13, 0x0

    goto :goto_2

    :cond_5
    const/4 v14, 0x0

    goto :goto_3

    :cond_6
    const/16 v19, 0x0

    goto :goto_4

    .line 5746
    :cond_7
    sget-object v24, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    goto :goto_5

    :cond_8
    const/16 v25, 0x0

    goto :goto_6
.end method

.method public a(Lsoftware/simplicial/a/f/cx;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 6357
    iget v0, p1, Lsoftware/simplicial/a/f/cx;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v7

    .line 6358
    if-nez v7, :cond_1

    .line 6360
    iget v0, p1, Lsoftware/simplicial/a/f/cx;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->e(I)Lsoftware/simplicial/a/bw;

    move-result-object v0

    .line 6361
    if-eqz v0, :cond_0

    .line 6363
    iget-boolean v1, p1, Lsoftware/simplicial/a/f/cx;->v:Z

    iput-boolean v1, v0, Lsoftware/simplicial/a/bw;->e:Z

    .line 6364
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bw;)Z

    .line 6433
    :cond_0
    :goto_0
    return-void

    .line 6369
    :cond_1
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->b:Z

    if-eqz v0, :cond_2

    .line 6371
    iget v0, p1, Lsoftware/simplicial/a/f/cx;->a:I

    iput v0, v7, Lsoftware/simplicial/a/bf;->A:I

    .line 6372
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-nez v0, :cond_2

    .line 6374
    iget-object v0, p1, Lsoftware/simplicial/a/f/cx;->d:Ljava/lang/String;

    iput-object v0, v7, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    .line 6375
    iget-object v0, p1, Lsoftware/simplicial/a/f/cx;->e:[B

    iput-object v0, v7, Lsoftware/simplicial/a/bf;->I:[B

    .line 6376
    iget-object v0, p1, Lsoftware/simplicial/a/f/cx;->f:Lsoftware/simplicial/a/q;

    iput-object v0, v7, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    .line 6380
    :cond_2
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->B:Z

    if-eqz v0, :cond_3

    .line 6382
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    iget v1, v7, Lsoftware/simplicial/a/bf;->A:I

    iget-object v2, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    iget-object v4, v7, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v5, v7, Lsoftware/simplicial/a/bf;->E:[B

    iget-boolean v6, p0, Lsoftware/simplicial/a/bs;->aR:Z

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/a/ay;->a(ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Ljava/lang/String;[BZ)V

    .line 6383
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->C:Lsoftware/simplicial/a/az;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/az;->a(Lsoftware/simplicial/a/az;)V

    .line 6384
    iget-wide v0, p0, Lsoftware/simplicial/a/bs;->aO:J

    invoke-virtual {v7, v0, v1}, Lsoftware/simplicial/a/bf;->c(J)V

    .line 6385
    iput-boolean v8, v7, Lsoftware/simplicial/a/bf;->bp:Z

    .line 6387
    :cond_3
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->g:Z

    if-eqz v0, :cond_4

    .line 6389
    iget-object v0, p1, Lsoftware/simplicial/a/f/cx;->h:Lsoftware/simplicial/a/aa;

    iput-object v0, v7, Lsoftware/simplicial/a/bf;->ay:Lsoftware/simplicial/a/aa;

    .line 6390
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->i:Z

    iput-boolean v0, v7, Lsoftware/simplicial/a/bf;->az:Z

    .line 6392
    iput-boolean v8, v7, Lsoftware/simplicial/a/bf;->bq:Z

    .line 6394
    :cond_4
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->j:Z

    if-eqz v0, :cond_5

    .line 6396
    iget v0, p1, Lsoftware/simplicial/a/f/cx;->k:I

    iput v0, v7, Lsoftware/simplicial/a/bf;->an:I

    .line 6397
    iget-wide v0, p1, Lsoftware/simplicial/a/f/cx;->l:J

    iput-wide v0, v7, Lsoftware/simplicial/a/bf;->ap:J

    .line 6398
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v0, v0, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v0, v1}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lsoftware/simplicial/a/aw;->b(I)J

    move-result-wide v0

    iput-wide v0, v7, Lsoftware/simplicial/a/bf;->aK:J

    .line 6399
    iput-boolean v8, v7, Lsoftware/simplicial/a/bf;->bp:Z

    .line 6401
    :cond_5
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->m:Z

    if-eqz v0, :cond_6

    .line 6403
    iget-object v0, p1, Lsoftware/simplicial/a/f/cx;->n:Lsoftware/simplicial/a/s;

    iput-object v0, v7, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    .line 6404
    iget-wide v0, p1, Lsoftware/simplicial/a/f/cx;->o:J

    iput-wide v0, v7, Lsoftware/simplicial/a/bf;->aq:J

    .line 6406
    :cond_6
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->p:Z

    if-eqz v0, :cond_7

    .line 6408
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 6409
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->ar:Ljava/util/Set;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 6410
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 6411
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->as:Ljava/util/Set;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 6412
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 6413
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->au:Ljava/util/Set;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 6414
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 6415
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->av:Ljava/util/Map;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 6416
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->v:Z

    iput-boolean v0, v7, Lsoftware/simplicial/a/bf;->G:Z

    .line 6417
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->w:Z

    iput-boolean v0, v7, Lsoftware/simplicial/a/bf;->aw:Z

    .line 6418
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 6419
    iget-object v0, v7, Lsoftware/simplicial/a/bf;->at:Ljava/util/Set;

    iget-object v1, p1, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 6421
    :cond_7
    iget-boolean v0, p1, Lsoftware/simplicial/a/f/cx;->z:Z

    if-eqz v0, :cond_8

    .line 6426
    :cond_8
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->h(Lsoftware/simplicial/a/bf;)Z

    .line 6427
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->i(Lsoftware/simplicial/a/bf;)Z

    .line 6428
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->m(Lsoftware/simplicial/a/bf;)Z

    .line 6429
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->k(Lsoftware/simplicial/a/bf;)Z

    .line 6430
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->n(Lsoftware/simplicial/a/bf;)Z

    .line 6431
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->l(Lsoftware/simplicial/a/bf;)Z

    .line 6432
    invoke-direct {p0, v7}, Lsoftware/simplicial/a/bs;->j(Lsoftware/simplicial/a/bf;)Z

    goto/16 :goto_0
.end method

.method protected a(Lsoftware/simplicial/a/f/n;)V
    .locals 3

    .prologue
    .line 6437
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-nez v0, :cond_0

    .line 6439
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v1, "Got ClanWarForfeitInternalMessage in non clan war game!"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 6449
    :goto_0
    return-void

    .line 6443
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/n;->a:Lsoftware/simplicial/a/c/j;

    iget-object v0, v0, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v1, v1, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v1, v1, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6444
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iput-object v0, p0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    goto :goto_0

    .line 6445
    :cond_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/n;->a:Lsoftware/simplicial/a/c/j;

    iget-object v0, v0, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    iget-object v1, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v1, v1, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v1, v1, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6446
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iput-object v0, p0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    goto :goto_0

    .line 6448
    :cond_2
    new-instance v0, Lsoftware/simplicial/a/c/j;

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/j;-><init>(Ljava/lang/String;[B)V

    iput-object v0, p0, Lsoftware/simplicial/a/bs;->u:Lsoftware/simplicial/a/c/j;

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/f/u;)V
    .locals 12

    .prologue
    .line 5278
    iget v0, p1, Lsoftware/simplicial/a/f/u;->ar:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->c(I)Lsoftware/simplicial/a/bf;

    move-result-object v11

    .line 5280
    if-nez v11, :cond_1

    .line 5308
    :cond_0
    :goto_0
    return-void

    .line 5286
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iget v1, v11, Lsoftware/simplicial/a/bf;->aS:I

    sub-int/2addr v0, v1

    const/16 v1, 0x28

    if-lt v0, v1, :cond_0

    .line 5288
    iget v0, p0, Lsoftware/simplicial/a/bs;->aN:I

    iput v0, v11, Lsoftware/simplicial/a/bf;->aS:I

    .line 5290
    iget-boolean v0, v11, Lsoftware/simplicial/a/bf;->K:Z

    if-nez v0, :cond_0

    .line 5294
    iget-object v0, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 5295
    iget-object v1, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    .line 5296
    iget-object v2, p1, Lsoftware/simplicial/a/f/u;->a:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/u;->b:[B

    iget-object v4, p1, Lsoftware/simplicial/a/f/u;->i:Ljava/lang/String;

    invoke-direct {p0, v11, v2, v3, v4}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Ljava/lang/String;[BLjava/lang/String;)Z

    .line 5297
    iget-object v2, v11, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    iget-object v3, p1, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    if-ne v2, v3, :cond_2

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    iget-object v3, p1, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    if-ne v2, v3, :cond_2

    iget v2, v11, Lsoftware/simplicial/a/bf;->ae:I

    iget v3, p1, Lsoftware/simplicial/a/f/u;->e:I

    if-ne v2, v3, :cond_2

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    .line 5298
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v11, Lsoftware/simplicial/a/bf;->F:[B

    iget-object v2, p1, Lsoftware/simplicial/a/f/u;->b:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-byte v0, v11, Lsoftware/simplicial/a/bf;->Z:B

    iget-byte v2, p1, Lsoftware/simplicial/a/f/u;->g:B

    if-ne v0, v2, :cond_2

    iget v0, v11, Lsoftware/simplicial/a/bf;->af:I

    iget v2, p1, Lsoftware/simplicial/a/f/u;->j:I

    if-ne v0, v2, :cond_2

    iget-object v0, p1, Lsoftware/simplicial/a/f/u;->i:Ljava/lang/String;

    .line 5299
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v11, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    iget-object v1, p1, Lsoftware/simplicial/a/f/u;->k:Lsoftware/simplicial/a/as;

    if-eq v0, v1, :cond_3

    .line 5300
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    iget-object v1, v11, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-object v2, v11, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/f/u;->b:[B

    iget-object v4, p1, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    iget-object v5, p1, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    iget v6, p1, Lsoftware/simplicial/a/f/u;->e:I

    iget-byte v7, p1, Lsoftware/simplicial/a/f/u;->g:B

    iget v8, p1, Lsoftware/simplicial/a/f/u;->j:I

    iget-object v9, v11, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    iget-object v10, v11, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    invoke-interface/range {v0 .. v10}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/f/bl;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 5302
    :cond_3
    iget-object v0, p1, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    iput-object v0, v11, Lsoftware/simplicial/a/bf;->V:Lsoftware/simplicial/a/e;

    .line 5303
    iget-object v0, p1, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    iput-object v0, v11, Lsoftware/simplicial/a/bf;->X:Lsoftware/simplicial/a/af;

    .line 5304
    iget-byte v0, p1, Lsoftware/simplicial/a/f/u;->g:B

    iput-byte v0, v11, Lsoftware/simplicial/a/bf;->Z:B

    .line 5305
    iget v0, p1, Lsoftware/simplicial/a/f/u;->e:I

    iput v0, v11, Lsoftware/simplicial/a/bf;->ae:I

    .line 5306
    iget-object v0, p1, Lsoftware/simplicial/a/f/u;->b:[B

    iput-object v0, v11, Lsoftware/simplicial/a/bf;->F:[B

    .line 5307
    iget-object v0, p1, Lsoftware/simplicial/a/f/u;->k:Lsoftware/simplicial/a/as;

    iput-object v0, v11, Lsoftware/simplicial/a/bf;->ab:Lsoftware/simplicial/a/as;

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/a/f/w;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 5480
    iget-object v0, p1, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    check-cast v0, Lsoftware/simplicial/a/v;

    .line 5481
    if-nez v0, :cond_1

    .line 5502
    :cond_0
    :goto_0
    return-void

    .line 5484
    :cond_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->e()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5485
    invoke-virtual {p0, v2, v2}, Lsoftware/simplicial/a/bs;->a(IZ)V

    .line 5487
    :cond_2
    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/v;)Lsoftware/simplicial/a/bf;

    move-result-object v8

    .line 5488
    if-eqz v8, :cond_0

    .line 5491
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    iget-object v1, v8, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5493
    iget v0, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-lez v0, :cond_3

    .line 5494
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->G()Lsoftware/simplicial/a/bx;

    move-result-object v0

    invoke-direct {p0, v8, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bx;)V

    .line 5498
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    iget-object v1, v8, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v1, v1, Lsoftware/simplicial/a/f/bl;->a:I

    iget-object v2, v8, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->b:I

    iget v3, p1, Lsoftware/simplicial/a/f/w;->a:I

    iget v4, p0, Lsoftware/simplicial/a/bs;->aZ:I

    iget v5, v8, Lsoftware/simplicial/a/bf;->A:I

    sget-object v6, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    iget v7, p1, Lsoftware/simplicial/a/f/w;->r:F

    invoke-static/range {v0 .. v7}, Lsoftware/simplicial/a/f/x;->a(Lsoftware/simplicial/a/f/bn;IIIIILsoftware/simplicial/a/f/z;F)[B

    move-result-object v0

    iput-object v0, v8, Lsoftware/simplicial/a/bf;->aM:[B

    .line 5501
    invoke-direct {p0, v8}, Lsoftware/simplicial/a/bs;->d(Lsoftware/simplicial/a/bf;)V

    goto :goto_0

    .line 5496
    :cond_3
    const/4 v0, 0x0

    iput-object v0, v8, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    goto :goto_1
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 7070
    sget-object v0, Lsoftware/simplicial/a/f/bh;->c:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 7071
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    .line 7072
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    .line 7073
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->k:Lsoftware/simplicial/a/f/bh;

    .line 7074
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    .line 7075
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->O:Lsoftware/simplicial/a/f/bh;

    .line 7076
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ae:Lsoftware/simplicial/a/f/bh;

    .line 7077
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->R:Lsoftware/simplicial/a/f/bh;

    .line 7078
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aN:Lsoftware/simplicial/a/f/bh;

    .line 7079
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->e:Lsoftware/simplicial/a/f/bh;

    .line 7080
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->ad:Lsoftware/simplicial/a/f/bh;

    .line 7081
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq p1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->q:Lsoftware/simplicial/a/f/bh;

    .line 7082
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 7084
    :cond_0
    const/4 v0, 0x1

    .line 7086
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ZZ)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 6665
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/a/ai;->a(ZZ)Z

    move-result v7

    .line 6668
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v0, :cond_0

    .line 6669
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->b:Lsoftware/simplicial/a/c/j;

    iget-object v1, v0, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v2, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->c:Lsoftware/simplicial/a/c/j;

    iget-object v3, v0, Lsoftware/simplicial/a/c/j;->a:Ljava/lang/String;

    sget-object v4, Lsoftware/simplicial/a/c/f;->c:Lsoftware/simplicial/a/c/f;

    iget-object v5, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget-boolean v6, p0, Lsoftware/simplicial/a/bs;->aR:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/a/bs;->a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V

    .line 6671
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v0, :cond_1

    .line 6672
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v8}, Lsoftware/simplicial/a/bs;->a(Ljava/util/List;Z)V

    .line 6674
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v0, :cond_2

    .line 6675
    invoke-direct {p0, v9, v9, v8}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bf;Z)V

    .line 6677
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->v:Z

    if-nez v0, :cond_3

    .line 6678
    sget-object v0, Lsoftware/simplicial/a/h/d;->c:Lsoftware/simplicial/a/h/d;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/h/d;)V

    .line 6680
    :cond_3
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v0, v1, :cond_5

    .line 6682
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v0

    .line 6684
    iget-boolean v2, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v2, :cond_4

    .line 6686
    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->e(Lsoftware/simplicial/a/bf;)V

    .line 6680
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6690
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 6692
    return v7
.end method

.method protected b()V
    .locals 14

    .prologue
    const v13, 0x3ee66666    # 0.45f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v12, 0x1

    .line 1913
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_5

    .line 1915
    iget v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    .line 1916
    sget v1, Lsoftware/simplicial/a/ai;->S:F

    div-float/2addr v1, v8

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 1917
    sget v0, Lsoftware/simplicial/a/ai;->S:F

    div-float/2addr v0, v8

    .line 1918
    :cond_0
    iget v1, p0, Lsoftware/simplicial/a/bs;->aE:F

    sub-float/2addr v1, v0

    div-float/2addr v1, v8

    .line 1919
    add-float/2addr v0, v1

    :goto_0
    move v3, v4

    .line 1928
    :goto_1
    iget v5, p0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v3, v5, :cond_6

    .line 1930
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v5, v5, v3

    .line 1932
    iget v6, v5, Lsoftware/simplicial/a/g;->l:F

    iget v7, v5, Lsoftware/simplicial/a/g;->n:F

    sub-float/2addr v6, v7

    cmpg-float v6, v6, v1

    if-gez v6, :cond_1

    .line 1934
    iget v6, v5, Lsoftware/simplicial/a/g;->n:F

    add-float/2addr v6, v1

    iput v6, v5, Lsoftware/simplicial/a/g;->l:F

    .line 1935
    iget v6, v5, Lsoftware/simplicial/a/g;->c:F

    neg-float v6, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->c:F

    .line 1937
    :cond_1
    iget v6, v5, Lsoftware/simplicial/a/g;->l:F

    iget v7, v5, Lsoftware/simplicial/a/g;->n:F

    add-float/2addr v6, v7

    cmpl-float v6, v6, v0

    if-lez v6, :cond_2

    .line 1939
    iget v6, v5, Lsoftware/simplicial/a/g;->n:F

    sub-float v6, v0, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->l:F

    .line 1940
    iget v6, v5, Lsoftware/simplicial/a/g;->c:F

    neg-float v6, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->c:F

    .line 1942
    :cond_2
    iget v6, v5, Lsoftware/simplicial/a/g;->m:F

    iget v7, v5, Lsoftware/simplicial/a/g;->n:F

    sub-float/2addr v6, v7

    cmpg-float v6, v6, v1

    if-gez v6, :cond_3

    .line 1944
    iget v6, v5, Lsoftware/simplicial/a/g;->n:F

    add-float/2addr v6, v1

    iput v6, v5, Lsoftware/simplicial/a/g;->m:F

    .line 1945
    iget v6, v5, Lsoftware/simplicial/a/g;->d:F

    neg-float v6, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->d:F

    .line 1947
    :cond_3
    iget v6, v5, Lsoftware/simplicial/a/g;->m:F

    iget v7, v5, Lsoftware/simplicial/a/g;->n:F

    add-float/2addr v6, v7

    cmpl-float v6, v6, v0

    if-lez v6, :cond_4

    .line 1949
    iget v6, v5, Lsoftware/simplicial/a/g;->n:F

    sub-float v6, v0, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->m:F

    .line 1950
    iget v6, v5, Lsoftware/simplicial/a/g;->d:F

    neg-float v6, v6

    iput v6, v5, Lsoftware/simplicial/a/g;->d:F

    .line 1953
    :cond_4
    invoke-virtual {v5}, Lsoftware/simplicial/a/g;->i()V

    .line 1928
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1924
    :cond_5
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v0, v2

    move v1, v2

    goto :goto_0

    .line 1956
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_e

    .line 1958
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v1, p0, Lsoftware/simplicial/a/bs;->aF:F

    sub-float/2addr v0, v1

    div-float v1, v0, v8

    .line 1959
    iget v0, p0, Lsoftware/simplicial/a/bs;->aF:F

    add-float/2addr v0, v1

    :goto_2
    move v3, v4

    .line 1968
    :goto_3
    iget v5, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v3, v5, :cond_13

    .line 1970
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v9, v5, v3

    .line 1973
    iget v5, v9, Lsoftware/simplicial/a/ag;->l:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v2

    if-gez v5, :cond_8

    .line 1975
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v6, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v5, v6, :cond_7

    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    mul-float/2addr v6, v13

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_7

    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    const v7, 0x3f0ccccd    # 0.55f

    mul-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_7

    .line 1977
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v12

    iget v6, v5, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lsoftware/simplicial/a/bx;->e:I

    .line 1978
    iget-short v5, p0, Lsoftware/simplicial/a/bs;->aH:S

    iput-short v5, p0, Lsoftware/simplicial/a/bs;->t:S

    .line 1979
    iget v5, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v5, v5, 0x14

    iput v5, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 1980
    const/16 v5, -0xa

    iput-short v5, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 1981
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    if-eqz v5, :cond_7

    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget-object v5, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v6, v6, v12

    if-ne v5, v6, :cond_7

    .line 1983
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v5, v6, v12}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;I)V

    .line 1984
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    .line 1988
    :cond_7
    iget v5, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v2

    iput v5, v9, Lsoftware/simplicial/a/ag;->l:F

    .line 1989
    iget v5, v9, Lsoftware/simplicial/a/ag;->c:F

    neg-float v5, v5

    iput v5, v9, Lsoftware/simplicial/a/ag;->c:F

    .line 1991
    :cond_8
    iget v5, v9, Lsoftware/simplicial/a/ag;->l:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_a

    .line 1993
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v6, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v5, v6, :cond_9

    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    mul-float/2addr v6, v13

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_9

    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    const v7, 0x3f0ccccd    # 0.55f

    mul-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_9

    .line 1995
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v5, v5, v4

    iget v6, v5, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lsoftware/simplicial/a/bx;->e:I

    .line 1996
    iget-short v5, p0, Lsoftware/simplicial/a/bs;->aH:S

    iput-short v5, p0, Lsoftware/simplicial/a/bs;->t:S

    .line 1997
    iget v5, p0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v5, v5, 0x14

    iput v5, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 1998
    const/16 v5, -0x14

    iput-short v5, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 1999
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    if-eqz v5, :cond_9

    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget-object v5, v5, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v6, v6, v4

    if-ne v5, v6, :cond_9

    .line 2001
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v5, v6, v12}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;I)V

    .line 2002
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    iget v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    .line 2006
    :cond_9
    iget v5, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    iput v5, v9, Lsoftware/simplicial/a/ag;->l:F

    .line 2007
    iget v5, v9, Lsoftware/simplicial/a/ag;->c:F

    neg-float v5, v5

    iput v5, v9, Lsoftware/simplicial/a/ag;->c:F

    .line 2009
    :cond_a
    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v2

    if-gez v5, :cond_b

    .line 2011
    iget v5, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v2

    iput v5, v9, Lsoftware/simplicial/a/ag;->m:F

    .line 2012
    iget v5, v9, Lsoftware/simplicial/a/ag;->d:F

    neg-float v5, v5

    iput v5, v9, Lsoftware/simplicial/a/ag;->d:F

    .line 2014
    :cond_b
    iget v5, v9, Lsoftware/simplicial/a/ag;->m:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    add-float/2addr v5, v6

    iget v6, p0, Lsoftware/simplicial/a/bs;->aE:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_c

    .line 2016
    iget v5, p0, Lsoftware/simplicial/a/bs;->aE:F

    iget v6, v9, Lsoftware/simplicial/a/ag;->n:F

    sub-float/2addr v5, v6

    iput v5, v9, Lsoftware/simplicial/a/ag;->m:F

    .line 2017
    iget v5, v9, Lsoftware/simplicial/a/ag;->d:F

    neg-float v5, v5

    iput v5, v9, Lsoftware/simplicial/a/ag;->d:F

    .line 2020
    :cond_c
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v6, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v5, v6, :cond_12

    iget-object v5, v9, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    if-eqz v5, :cond_12

    move v5, v4

    .line 2022
    :goto_4
    iget v6, p0, Lsoftware/simplicial/a/bs;->aj:I

    if-ge v5, v6, :cond_12

    .line 2024
    iget-object v6, p0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    aget-object v10, v6, v5

    .line 2025
    iget-object v6, v9, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    if-eq v10, v6, :cond_d

    iget-object v6, v9, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    iget-object v6, v6, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eq v10, v6, :cond_f

    .line 2022
    :cond_d
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1964
    :cond_e
    iget v0, p0, Lsoftware/simplicial/a/bs;->aE:F

    add-float/2addr v0, v2

    move v1, v2

    goto/16 :goto_2

    .line 2028
    :cond_f
    const/4 v7, 0x0

    move v8, v4

    .line 2029
    :goto_5
    iget v6, p0, Lsoftware/simplicial/a/bs;->am:I

    if-ge v8, v6, :cond_10

    .line 2031
    iget-object v6, p0, Lsoftware/simplicial/a/bs;->ao:[Lsoftware/simplicial/a/ag;

    aget-object v6, v6, v8

    .line 2033
    iget-object v11, v6, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    if-ne v11, v10, :cond_22

    .line 2029
    :goto_6
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v7, v6

    goto :goto_5

    .line 2037
    :cond_10
    iget-boolean v6, v7, Lsoftware/simplicial/a/ag;->e:Z

    if-eqz v6, :cond_d

    .line 2040
    iget-object v6, v9, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bh;->q_()F

    move-result v6

    sget v7, Lsoftware/simplicial/a/bx;->a:F

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 2041
    iget-object v7, v9, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    invoke-virtual {v7, v10, v6}, Lsoftware/simplicial/a/bh;->a(Lsoftware/simplicial/a/ao;F)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2043
    iget v5, v10, Lsoftware/simplicial/a/bx;->e:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v10, Lsoftware/simplicial/a/bx;->e:I

    .line 2044
    iget v5, v10, Lsoftware/simplicial/a/bx;->e:I

    const/4 v6, 0x3

    if-lt v5, v6, :cond_11

    .line 2046
    iput-short v12, p0, Lsoftware/simplicial/a/bs;->aH:S

    .line 2047
    iget v5, p0, Lsoftware/simplicial/a/bs;->aN:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x14

    iput v5, p0, Lsoftware/simplicial/a/bs;->o:I

    .line 2050
    :cond_11
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    iget-object v6, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v5, v6, v12}, Lsoftware/simplicial/a/bf;->a(Lsoftware/simplicial/a/am;I)V

    .line 2051
    iget-object v5, v9, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    iget v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lsoftware/simplicial/a/bf;->bj:I

    .line 2053
    iget-object v5, p0, Lsoftware/simplicial/a/bs;->ad:Ljava/util/Random;

    invoke-virtual {v9, v5}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 2059
    :cond_12
    invoke-virtual {v9}, Lsoftware/simplicial/a/ag;->i()V

    .line 1968
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_13
    move v2, v4

    .line 2063
    :goto_7
    iget v3, p0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v2, v3, :cond_18

    .line 2065
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v3, v3, v2

    .line 2068
    iget v5, v3, Lsoftware/simplicial/a/bl;->l:F

    iget v6, v3, Lsoftware/simplicial/a/bl;->n:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v1

    if-gez v5, :cond_14

    .line 2070
    iget v5, v3, Lsoftware/simplicial/a/bl;->n:F

    add-float/2addr v5, v1

    iput v5, v3, Lsoftware/simplicial/a/bl;->l:F

    .line 2071
    iget v5, v3, Lsoftware/simplicial/a/bl;->c:F

    neg-float v5, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 2073
    :cond_14
    iget v5, v3, Lsoftware/simplicial/a/bl;->l:F

    iget v6, v3, Lsoftware/simplicial/a/bl;->n:F

    add-float/2addr v5, v6

    cmpl-float v5, v5, v0

    if-lez v5, :cond_15

    .line 2075
    iget v5, v3, Lsoftware/simplicial/a/bl;->n:F

    sub-float v5, v0, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->l:F

    .line 2076
    iget v5, v3, Lsoftware/simplicial/a/bl;->c:F

    neg-float v5, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->c:F

    .line 2078
    :cond_15
    iget v5, v3, Lsoftware/simplicial/a/bl;->m:F

    iget v6, v3, Lsoftware/simplicial/a/bl;->n:F

    sub-float/2addr v5, v6

    cmpg-float v5, v5, v1

    if-gez v5, :cond_16

    .line 2080
    iget v5, v3, Lsoftware/simplicial/a/bl;->n:F

    add-float/2addr v5, v1

    iput v5, v3, Lsoftware/simplicial/a/bl;->m:F

    .line 2081
    iget v5, v3, Lsoftware/simplicial/a/bl;->d:F

    neg-float v5, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->d:F

    .line 2083
    :cond_16
    iget v5, v3, Lsoftware/simplicial/a/bl;->m:F

    iget v6, v3, Lsoftware/simplicial/a/bl;->n:F

    add-float/2addr v5, v6

    cmpl-float v5, v5, v0

    if-lez v5, :cond_17

    .line 2085
    iget v5, v3, Lsoftware/simplicial/a/bl;->n:F

    sub-float v5, v0, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->m:F

    .line 2086
    iget v5, v3, Lsoftware/simplicial/a/bl;->d:F

    neg-float v5, v5

    iput v5, v3, Lsoftware/simplicial/a/bl;->d:F

    .line 2089
    :cond_17
    invoke-virtual {v3}, Lsoftware/simplicial/a/bl;->i()V

    .line 2063
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 2093
    :cond_18
    :goto_8
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v2, v2

    if-ge v4, v2, :cond_21

    .line 2095
    iget-object v2, p0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v2, v4

    .line 2098
    iget v3, v2, Lsoftware/simplicial/a/bq;->l:F

    iget v5, v2, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v3, v5

    cmpg-float v3, v3, v1

    if-gez v3, :cond_19

    .line 2100
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_1d

    iget-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    if-eqz v3, :cond_1d

    .line 2102
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 2103
    iput-boolean v12, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 2111
    :cond_19
    :goto_9
    iget v3, v2, Lsoftware/simplicial/a/bq;->l:F

    iget v5, v2, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v3, v5

    cmpl-float v3, v3, v0

    if-lez v3, :cond_1a

    .line 2113
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_1e

    iget-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    if-eqz v3, :cond_1e

    .line 2115
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 2116
    iput-boolean v12, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 2124
    :cond_1a
    :goto_a
    iget v3, v2, Lsoftware/simplicial/a/bq;->m:F

    iget v5, v2, Lsoftware/simplicial/a/bq;->n:F

    sub-float/2addr v3, v5

    cmpg-float v3, v3, v1

    if-gez v3, :cond_1b

    .line 2126
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_1f

    iget-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    if-eqz v3, :cond_1f

    .line 2128
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 2129
    iput-boolean v12, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 2137
    :cond_1b
    :goto_b
    iget v3, v2, Lsoftware/simplicial/a/bq;->m:F

    iget v5, v2, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v3, v5

    cmpl-float v3, v3, v0

    if-lez v3, :cond_1c

    .line 2139
    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v5, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_20

    iget-boolean v3, v2, Lsoftware/simplicial/a/bq;->F:Z

    if-eqz v3, :cond_20

    .line 2141
    sget-object v3, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    iput-object v3, v2, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    .line 2142
    iput-boolean v12, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 2151
    :cond_1c
    :goto_c
    invoke-virtual {v2}, Lsoftware/simplicial/a/bq;->i()V

    .line 2093
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 2107
    :cond_1d
    iget v3, v2, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v3, v1

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 2108
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    neg-float v3, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->a:F

    goto :goto_9

    .line 2120
    :cond_1e
    iget v3, v2, Lsoftware/simplicial/a/bq;->n:F

    sub-float v3, v0, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->l:F

    .line 2121
    iget v3, v2, Lsoftware/simplicial/a/bq;->a:F

    neg-float v3, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->a:F

    goto :goto_a

    .line 2133
    :cond_1f
    iget v3, v2, Lsoftware/simplicial/a/bq;->n:F

    add-float/2addr v3, v1

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    .line 2134
    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    neg-float v3, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->b:F

    goto :goto_b

    .line 2146
    :cond_20
    iget v3, v2, Lsoftware/simplicial/a/bq;->n:F

    sub-float v3, v0, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->m:F

    .line 2147
    iget v3, v2, Lsoftware/simplicial/a/bq;->b:F

    neg-float v3, v3

    iput v3, v2, Lsoftware/simplicial/a/bq;->b:F

    goto :goto_c

    .line 2153
    :cond_21
    return-void

    :cond_22
    move-object v6, v7

    goto/16 :goto_6
.end method

.method public c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 205
    invoke-super {p0}, Lsoftware/simplicial/a/ai;->c()Z

    .line 208
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->f()V

    .line 209
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->h()V

    .line 210
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->r()V

    .line 211
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->s()V

    .line 212
    iget-short v1, p0, Lsoftware/simplicial/a/bs;->aH:S

    if-lez v1, :cond_0

    .line 214
    iget v1, p0, Lsoftware/simplicial/a/bs;->ab:F

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/bs;->b(F)V

    .line 215
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->a()V

    .line 216
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->u()V

    .line 217
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->b()V

    .line 218
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->v()V

    .line 219
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->w()V

    .line 220
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->i()V

    .line 221
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->q()V

    .line 222
    invoke-direct {p0}, Lsoftware/simplicial/a/bs;->x()V

    .line 224
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/bs;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return v0

    .line 226
    :catch_0
    move-exception v1

    .line 228
    iput-boolean v0, p0, Lsoftware/simplicial/a/bs;->aL:Z

    .line 229
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 230
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 231
    invoke-virtual {v1, v2}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->aJ:Lsoftware/simplicial/a/al;

    invoke-interface {v0, p0, v1}, Lsoftware/simplicial/a/al;->a(Lsoftware/simplicial/a/ai;Ljava/lang/Exception;)V

    .line 233
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "GS CRASHED"

    invoke-static {v0, v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 234
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 23

    .prologue
    .line 3348
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v1, v1, Lsoftware/simplicial/a/b/a;->e:I

    rsub-int v1, v1, 0xb4

    const/4 v2, 0x5

    if-gt v1, v2, :cond_0

    .line 3349
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v3, v3, Lsoftware/simplicial/a/b/a;->e:I

    rsub-int v3, v3, 0xb4

    invoke-static {v2, v3}, Lsoftware/simplicial/a/f/bw;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v1, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 3350
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget v1, v1, Lsoftware/simplicial/a/c/a;->d:I

    rsub-int v1, v1, 0xb4

    const/4 v2, 0x5

    if-gt v1, v2, :cond_1

    .line 3351
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget v3, v3, Lsoftware/simplicial/a/c/a;->d:I

    rsub-int v3, v3, 0xb4

    invoke-static {v2, v3}, Lsoftware/simplicial/a/f/bw;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v1, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 3352
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v1, v1, Lsoftware/simplicial/a/h/c;->f:I

    rsub-int v1, v1, 0xb4

    const/4 v2, 0x5

    if-gt v1, v2, :cond_2

    .line 3353
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v3, v3, Lsoftware/simplicial/a/h/c;->f:I

    rsub-int v3, v3, 0xb4

    invoke-static {v2, v3}, Lsoftware/simplicial/a/f/bw;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v1, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 3354
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v1, v1, Lsoftware/simplicial/a/i/a;->m:I

    rsub-int v1, v1, 0xb4

    const/4 v2, 0x5

    if-gt v1, v2, :cond_3

    .line 3355
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v3, v3, Lsoftware/simplicial/a/i/a;->m:I

    rsub-int v3, v3, 0xb4

    invoke-static {v2, v3}, Lsoftware/simplicial/a/f/bw;->a(Lsoftware/simplicial/a/f/bn;I)[B

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    invoke-interface {v1, v2, v3}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    .line 3357
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    iget v1, v1, Lsoftware/simplicial/a/b/a;->e:I

    const/16 v2, 0xb4

    if-ne v1, v2, :cond_5

    .line 3425
    :cond_4
    :goto_0
    return-void

    .line 3359
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    iget v1, v1, Lsoftware/simplicial/a/c/a;->d:I

    const/16 v2, 0xb4

    if-eq v1, v2, :cond_4

    .line 3361
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    iget v1, v1, Lsoftware/simplicial/a/h/c;->f:I

    const/16 v2, 0xb4

    if-eq v1, v2, :cond_4

    .line 3363
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget v1, v1, Lsoftware/simplicial/a/i/a;->m:I

    const/16 v2, 0xb4

    if-eq v1, v2, :cond_4

    .line 3366
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    invoke-static {v1}, Lsoftware/simplicial/a/ai;->a([Lsoftware/simplicial/a/bf;)Lsoftware/simplicial/a/bf;

    move-result-object v1

    .line 3368
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->b(Lsoftware/simplicial/a/bf;)V

    .line 3370
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/bs;->c(Lsoftware/simplicial/a/bf;)V

    .line 3372
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 3373
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 3374
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->at:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    .line 3375
    const/4 v1, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 3377
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->af:[Lsoftware/simplicial/a/bq;

    aget-object v2, v2, v1

    .line 3379
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/bq;->G:Z

    .line 3375
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3381
    :cond_9
    const/4 v1, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->az:I

    if-ge v1, v2, :cond_a

    .line 3383
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ai:[Lsoftware/simplicial/a/g;

    aget-object v2, v2, v1

    .line 3384
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/g;->f:Z

    .line 3381
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3386
    :cond_a
    const/4 v1, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->au:I

    if-ge v1, v2, :cond_b

    .line 3388
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->av:[Lsoftware/simplicial/a/bl;

    aget-object v2, v2, v1

    .line 3389
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/bl;->E:Z

    .line 3386
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 3392
    :cond_b
    const/4 v1, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v1, v2, :cond_d

    .line 3394
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v3, v2, v1

    .line 3396
    const/4 v2, 0x0

    iput-boolean v2, v3, Lsoftware/simplicial/a/bf;->aj:Z

    .line 3397
    const/4 v2, 0x0

    :goto_5
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    array-length v4, v4

    if-ge v2, v4, :cond_c

    .line 3399
    iget-object v4, v3, Lsoftware/simplicial/a/bf;->z:[Lsoftware/simplicial/a/bh;

    aget-object v4, v4, v2

    .line 3401
    const/4 v5, 0x0

    iput-boolean v5, v4, Lsoftware/simplicial/a/bh;->M:Z

    .line 3397
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 3392
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3405
    :cond_d
    move-object/from16 v0, p0

    iget-short v1, v0, Lsoftware/simplicial/a/bs;->aH:S

    move-object/from16 v0, p0

    iget-short v2, v0, Lsoftware/simplicial/a/bs;->r:S

    if-ne v1, v2, :cond_f

    move-object/from16 v0, p0

    iget-short v1, v0, Lsoftware/simplicial/a/bs;->aH:S

    const/16 v2, 0x7fff

    if-eq v1, v2, :cond_e

    move-object/from16 v0, p0

    iget-short v1, v0, Lsoftware/simplicial/a/bs;->aH:S

    const/4 v2, -0x4

    if-ne v1, v2, :cond_4

    :cond_e
    move-object/from16 v0, p0

    iget v1, v0, Lsoftware/simplicial/a/bs;->aN:I

    rem-int/lit8 v1, v1, 0x14

    if-nez v1, :cond_4

    .line 3407
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-eq v1, v2, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_17

    .line 3410
    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aj:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lsoftware/simplicial/a/bx;

    .line 3411
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 3412
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-short v3, v0, Lsoftware/simplicial/a/bs;->aH:S

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aY:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ba:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->bb:[B

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v11, :cond_11

    const/4 v11, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v12, :cond_12

    const/4 v12, 0x1

    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v13, :cond_13

    const/4 v13, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v14, :cond_14

    const/4 v14, 0x1

    :goto_9
    move-object/from16 v0, p0

    iget v15, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aT:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    move-object/from16 v18, v0

    .line 3413
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v19, v0

    if-eqz v19, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->a:Lsoftware/simplicial/a/c/g;

    move-object/from16 v19, v0

    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->al:[Lsoftware/simplicial/a/bx;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v21, v0

    if-eqz v21, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    move/from16 v21, v0

    if-eqz v21, :cond_16

    const/16 v21, 0x1

    .line 3412
    :goto_b
    invoke-static/range {v1 .. v21}, Lsoftware/simplicial/a/f/co;->a(Lsoftware/simplicial/a/f/bn;[Lsoftware/simplicial/a/bx;S[Lsoftware/simplicial/a/bf;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BZZZZIZZILsoftware/simplicial/a/c/g;[Lsoftware/simplicial/a/bx;Z)[B

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v22

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    goto/16 :goto_0

    :cond_11
    const/4 v11, 0x0

    goto :goto_6

    :cond_12
    const/4 v12, 0x0

    goto :goto_7

    :cond_13
    const/4 v13, 0x0

    goto :goto_8

    :cond_14
    const/4 v14, 0x0

    goto :goto_9

    .line 3413
    :cond_15
    sget-object v19, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    goto :goto_a

    :cond_16
    const/16 v21, 0x0

    goto :goto_b

    .line 3418
    :cond_17
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/a/bs;->aY:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lsoftware/simplicial/a/bf;

    .line 3419
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 3420
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->a:Lsoftware/simplicial/a/f/bf;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/a/bs;->aI:Lsoftware/simplicial/a/f/bn;

    move-object/from16 v0, p0

    iget-short v3, v0, Lsoftware/simplicial/a/bs;->aH:S

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/a/bs;->aY:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/a/bs;->aV:Lsoftware/simplicial/a/aq;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/bs;->aQ:Lsoftware/simplicial/a/ap;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/a/bs;->ba:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/bs;->bb:[B

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v11, :cond_18

    const/4 v11, 0x1

    :goto_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v12, :cond_19

    const/4 v12, 0x1

    :goto_d
    move-object/from16 v0, p0

    iget v13, v0, Lsoftware/simplicial/a/bs;->aZ:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v14, :cond_1a

    const/4 v14, 0x1

    :goto_e
    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v15, :cond_1b

    const/4 v15, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aR:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bs;->aT:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->e:Ljava/util/Map;

    move-object/from16 v18, v0

    .line 3421
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v19, v0

    if-eqz v19, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lsoftware/simplicial/a/c/a;->a:Lsoftware/simplicial/a/c/g;

    move-object/from16 v19, v0

    :goto_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1d

    const/16 v20, 0x1

    .line 3420
    :goto_11
    invoke-static/range {v1 .. v20}, Lsoftware/simplicial/a/f/co;->a(Lsoftware/simplicial/a/f/bn;[Lsoftware/simplicial/a/bf;S[Lsoftware/simplicial/a/bf;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BZZIZZZZILsoftware/simplicial/a/c/g;Z)[B

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/a/bs;->d:Ljava/util/Set;

    move-object/from16 v0, v21

    invoke-interface {v0, v1, v2}, Lsoftware/simplicial/a/f/bf;->a([BLjava/lang/Iterable;)V

    goto/16 :goto_0

    :cond_18
    const/4 v11, 0x0

    goto :goto_c

    :cond_19
    const/4 v12, 0x0

    goto :goto_d

    :cond_1a
    const/4 v14, 0x0

    goto :goto_e

    :cond_1b
    const/4 v15, 0x0

    goto :goto_f

    .line 3421
    :cond_1c
    sget-object v19, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    goto :goto_10

    :cond_1d
    const/16 v20, 0x0

    goto :goto_11
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 7107
    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    iget-boolean v0, v0, Lsoftware/simplicial/a/i/a;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/a/bs;->aR:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 7092
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GID: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lsoftware/simplicial/a/bs;->aZ:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " TR: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v3, p0, Lsoftware/simplicial/a/bs;->aH:S

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Mode: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " A:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->j:Lsoftware/simplicial/a/b/a;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AW:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->i:Lsoftware/simplicial/a/h/c;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " CW:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lsoftware/simplicial/a/bs;->h:Lsoftware/simplicial/a/c/a;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " M: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/a/bs;->k:Lsoftware/simplicial/a/i/a;

    if-eqz v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n MHM:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lsoftware/simplicial/a/bs;->aR:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7094
    :goto_4
    iget v1, p0, Lsoftware/simplicial/a/bs;->aY:I

    if-ge v2, v1, :cond_5

    .line 7096
    iget-object v1, p0, Lsoftware/simplicial/a/bs;->ae:[Lsoftware/simplicial/a/bf;

    aget-object v1, v1, v2

    .line 7097
    iget-boolean v3, v1, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_4

    .line 7094
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_0
    move v0, v2

    .line 7092
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    .line 7100
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "AID: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v1, Lsoftware/simplicial/a/bf;->A:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " Clan: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v1, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 7102
    :cond_5
    return-object v0
.end method
