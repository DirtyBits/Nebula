.class public Lsoftware/simplicial/a/a/ae;
.super Lsoftware/simplicial/a/a/y;
.source "SourceFile"


# instance fields
.field public a:B

.field public b:B

.field public c:B


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 21
    sget-object v0, Lsoftware/simplicial/a/a/y$a;->H:Lsoftware/simplicial/a/a/y$a;

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/a/y;-><init>(Lsoftware/simplicial/a/a/y$a;I)V

    .line 23
    int-to-byte v0, p1

    iput-byte v0, p0, Lsoftware/simplicial/a/a/ae;->a:B

    .line 24
    int-to-byte v0, p2

    iput-byte v0, p0, Lsoftware/simplicial/a/a/ae;->b:B

    .line 25
    int-to-byte v0, p3

    iput-byte v0, p0, Lsoftware/simplicial/a/a/ae;->c:B

    .line 26
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lsoftware/simplicial/a/a/ae;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v0}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 33
    iget-byte v0, p0, Lsoftware/simplicial/a/a/ae;->a:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 34
    iget-byte v0, p0, Lsoftware/simplicial/a/a/ae;->b:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 35
    iget-byte v0, p0, Lsoftware/simplicial/a/a/ae;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 36
    return-void
.end method

.method public a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 41
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->a:B

    add-int/lit8 v2, p1, -0x1

    if-le v1, v2, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->a:B

    if-ltz v1, :cond_0

    .line 43
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->b:B

    add-int/lit8 v2, p2, -0x1

    if-gt v1, v2, :cond_0

    .line 44
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->b:B

    if-ltz v1, :cond_0

    .line 45
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->c:B

    sget-object v2, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_0

    .line 46
    iget-byte v1, p0, Lsoftware/simplicial/a/a/ae;->c:B

    if-ltz v1, :cond_0

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method
