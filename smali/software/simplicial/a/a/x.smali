.class public Lsoftware/simplicial/a/a/x;
.super Lsoftware/simplicial/a/a/y;
.source "SourceFile"


# instance fields
.field public a:B

.field public b:B

.field public c:B


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lsoftware/simplicial/a/a/y$a;->K:Lsoftware/simplicial/a/a/y$a;

    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/a/a/y;-><init>(Lsoftware/simplicial/a/a/y$a;I)V

    .line 20
    int-to-byte v0, p1

    iput-byte v0, p0, Lsoftware/simplicial/a/a/x;->a:B

    .line 21
    int-to-byte v0, p2

    iput-byte v0, p0, Lsoftware/simplicial/a/a/x;->b:B

    .line 22
    int-to-byte v0, p3

    iput-byte v0, p0, Lsoftware/simplicial/a/a/x;->c:B

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bn;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/a/x;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v0}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 30
    iget-byte v0, p0, Lsoftware/simplicial/a/a/x;->a:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 31
    iget-byte v0, p0, Lsoftware/simplicial/a/a/x;->b:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 32
    iget-byte v0, p0, Lsoftware/simplicial/a/a/x;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 33
    return-void
.end method

.method public a(II)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->a:B

    add-int/lit8 v2, p1, -0x1

    if-le v1, v2, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->a:B

    if-ltz v1, :cond_0

    .line 40
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->b:B

    add-int/lit8 v2, p2, -0x1

    if-gt v1, v2, :cond_0

    .line 41
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->b:B

    if-ltz v1, :cond_0

    .line 42
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->c:B

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x1d

    if-gt v1, v2, :cond_0

    .line 43
    iget-byte v1, p0, Lsoftware/simplicial/a/a/x;->c:B

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    .line 45
    const/4 v0, 0x1

    goto :goto_0
.end method
