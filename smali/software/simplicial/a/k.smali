.class public final enum Lsoftware/simplicial/a/k;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/k;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/k;

.field public static final enum b:Lsoftware/simplicial/a/k;

.field public static final enum c:Lsoftware/simplicial/a/k;

.field public static final enum d:Lsoftware/simplicial/a/k;

.field public static final enum e:Lsoftware/simplicial/a/k;

.field public static final enum f:Lsoftware/simplicial/a/k;

.field public static final enum g:Lsoftware/simplicial/a/k;

.field public static final h:[Lsoftware/simplicial/a/k;

.field private static final synthetic i:[Lsoftware/simplicial/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->a:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->b:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "NOT_FOUND"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->c:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "CHALLENGE_PENDING_FOR_CHALLENGER"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->d:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "CHALLENGE_PENDING_FOR_CHALLENGEE"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->e:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "DECLINED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->f:Lsoftware/simplicial/a/k;

    new-instance v0, Lsoftware/simplicial/a/k;

    const-string v1, "INCOMPATIBLE_VERSION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/k;->g:Lsoftware/simplicial/a/k;

    .line 6
    const/4 v0, 0x7

    new-array v0, v0, [Lsoftware/simplicial/a/k;

    sget-object v1, Lsoftware/simplicial/a/k;->a:Lsoftware/simplicial/a/k;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/k;->b:Lsoftware/simplicial/a/k;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/k;->c:Lsoftware/simplicial/a/k;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/k;->d:Lsoftware/simplicial/a/k;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/k;->e:Lsoftware/simplicial/a/k;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/k;->f:Lsoftware/simplicial/a/k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/k;->g:Lsoftware/simplicial/a/k;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/k;->i:[Lsoftware/simplicial/a/k;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/k;->values()[Lsoftware/simplicial/a/k;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/k;->h:[Lsoftware/simplicial/a/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/k;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/k;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/k;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/k;->i:[Lsoftware/simplicial/a/k;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/k;

    return-object v0
.end method
