.class public Lsoftware/simplicial/a/bh;
.super Lsoftware/simplicial/a/h;
.source "SourceFile"


# static fields
.field public static final e:F

.field public static f:F

.field public static g:F

.field public static final h:F

.field public static final i:F


# instance fields
.field public A:B

.field public B:B

.field public C:B

.field public D:B

.field public E:B

.field public F:B

.field public G:B

.field public H:B

.field public I:B

.field public J:B

.field public K:B

.field public L:F

.field public M:Z

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/a/y;",
            ">;"
        }
    .end annotation
.end field

.field public O:Lsoftware/simplicial/a/a/e;

.field public P:F

.field public Q:F

.field public j:I

.field public k:I

.field public z:S


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lsoftware/simplicial/a/bh;->e:F

    .line 18
    const v0, 0x3c23d70a    # 0.01f

    sput v0, Lsoftware/simplicial/a/bh;->f:F

    .line 19
    const/high16 v0, 0x447a0000    # 1000.0f

    sput v0, Lsoftware/simplicial/a/bh;->g:F

    .line 22
    const-wide v0, 0x411e848000000000L    # 500000.0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lsoftware/simplicial/a/bh;->h:F

    .line 28
    sget v0, Lsoftware/simplicial/a/bh;->e:F

    const v1, 0x3fe66666    # 1.8f

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/bh;->i:F

    return-void
.end method

.method public constructor <init>(IIFFFFFI)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Lsoftware/simplicial/a/h;-><init>()V

    .line 44
    iput-short v0, p0, Lsoftware/simplicial/a/bh;->z:S

    .line 45
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->A:B

    .line 46
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->B:B

    .line 47
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->C:B

    .line 48
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->D:B

    .line 49
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->E:B

    .line 50
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->F:B

    .line 51
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->G:B

    .line 52
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->H:B

    .line 53
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->I:B

    .line 54
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->J:B

    .line 55
    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->K:B

    .line 73
    invoke-virtual/range {p0 .. p8}, Lsoftware/simplicial/a/bh;->a(IIFFFFFI)V

    .line 74
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 161
    const/high16 v0, 0x42900000    # 72.0f

    .line 162
    iget v1, p0, Lsoftware/simplicial/a/bh;->s:F

    iget v2, p0, Lsoftware/simplicial/a/bh;->l:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v0

    if-gtz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/bh;->t:F

    iget v2, p0, Lsoftware/simplicial/a/bh;->m:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 164
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/bh;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bh;->s:F

    .line 165
    iget v0, p0, Lsoftware/simplicial/a/bh;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bh;->t:F

    .line 166
    iget v0, p0, Lsoftware/simplicial/a/bh;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bh;->w:F

    .line 174
    :goto_0
    return-void

    .line 170
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bh;->s:F

    iget v1, p0, Lsoftware/simplicial/a/bh;->l:F

    iget v2, p0, Lsoftware/simplicial/a/bh;->s:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bh;->s:F

    .line 171
    iget v0, p0, Lsoftware/simplicial/a/bh;->t:F

    iget v1, p0, Lsoftware/simplicial/a/bh;->m:F

    iget v2, p0, Lsoftware/simplicial/a/bh;->t:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bh;->t:F

    .line 172
    iget v0, p0, Lsoftware/simplicial/a/bh;->w:F

    iget v1, p0, Lsoftware/simplicial/a/bh;->n:F

    iget v2, p0, Lsoftware/simplicial/a/bh;->w:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bh;->w:F

    goto :goto_0
.end method

.method public a(IIFFFFFI)V
    .locals 11

    .prologue
    .line 78
    const/high16 v8, 0x40a00000    # 5.0f

    sget v9, Lsoftware/simplicial/a/bh;->e:F

    const v10, 0x48f42400    # 500000.0f

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-super/range {v1 .. v10}, Lsoftware/simplicial/a/h;->a(IFFFFFFFF)V

    .line 80
    iput p1, p0, Lsoftware/simplicial/a/bh;->j:I

    .line 81
    move/from16 v0, p8

    iput v0, p0, Lsoftware/simplicial/a/bh;->k:I

    .line 84
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    .line 85
    const/4 v1, 0x0

    iput v1, p0, Lsoftware/simplicial/a/bh;->P:F

    .line 86
    const/4 v1, 0x0

    iput v1, p0, Lsoftware/simplicial/a/bh;->Q:F

    .line 88
    const/4 v1, 0x0

    iput-short v1, p0, Lsoftware/simplicial/a/bh;->z:S

    .line 89
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->A:B

    .line 90
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->B:B

    .line 91
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->C:B

    .line 92
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->D:B

    .line 93
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->E:B

    .line 94
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->F:B

    .line 95
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->G:B

    .line 96
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->H:B

    .line 97
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->I:B

    .line 98
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->K:B

    .line 99
    const/4 v1, -0x1

    iput-byte v1, p0, Lsoftware/simplicial/a/bh;->J:B

    .line 100
    new-instance v1, Lsoftware/simplicial/a/a/e;

    const/4 v2, 0x0

    move/from16 v0, p8

    invoke-direct {v1, v0, p2, v2}, Lsoftware/simplicial/a/a/e;-><init>(III)V

    iput-object v1, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    .line 102
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lsoftware/simplicial/a/bh;->L:F

    .line 103
    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/a/y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    iget-short v0, v0, Lsoftware/simplicial/a/a/e;->c:S

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bh;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 185
    invoke-virtual {p1}, Lsoftware/simplicial/a/bh;->d()F

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/bh;->a(F)V

    .line 186
    invoke-virtual {p1}, Lsoftware/simplicial/a/bh;->e()V

    .line 187
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->E:B

    if-ltz v0, :cond_0

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 190
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->E:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->E:B

    .line 191
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->E:B

    .line 193
    :cond_0
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->D:B

    if-ltz v0, :cond_1

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 196
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->D:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->D:B

    .line 197
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->D:B

    .line 199
    :cond_1
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->C:B

    if-ltz v0, :cond_2

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 202
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->C:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->C:B

    .line 203
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->C:B

    .line 205
    :cond_2
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->B:B

    if-ltz v0, :cond_3

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 208
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->B:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->B:B

    .line 209
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->B:B

    .line 211
    :cond_3
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->F:B

    if-ltz v0, :cond_4

    .line 213
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 214
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->F:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->F:B

    .line 215
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->F:B

    .line 217
    :cond_4
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->G:B

    if-ltz v0, :cond_5

    .line 219
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 220
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->G:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->G:B

    .line 221
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->G:B

    .line 223
    :cond_5
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->H:B

    if-ltz v0, :cond_6

    .line 225
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 226
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->H:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->H:B

    .line 227
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->H:B

    .line 229
    :cond_6
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->J:B

    if-ltz v0, :cond_7

    .line 231
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    const/16 v1, 0x200

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/a/e;->a(S)V

    .line 232
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->J:B

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->J:B

    .line 233
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->J:B

    .line 235
    :cond_7
    iget-byte v0, p1, Lsoftware/simplicial/a/bh;->K:B

    if-ltz v0, :cond_8

    .line 236
    iput-byte v2, p1, Lsoftware/simplicial/a/bh;->K:B

    .line 237
    :cond_8
    return-void
.end method

.method public b()F
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lsoftware/simplicial/a/bh;->d:F

    iget v1, p0, Lsoftware/simplicial/a/bh;->L:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Lsoftware/simplicial/a/h;->e()V

    .line 150
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bh;->Q:F

    .line 151
    return-void
.end method

.method public e(F)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, -0x1

    .line 108
    iget v0, p0, Lsoftware/simplicial/a/bh;->L:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    .line 110
    iget v0, p0, Lsoftware/simplicial/a/bh;->L:F

    iget v1, p0, Lsoftware/simplicial/a/bh;->L:F

    sub-float v1, v5, v1

    const/high16 v2, 0x41000000    # 8.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bh;->L:F

    .line 111
    iget v0, p0, Lsoftware/simplicial/a/bh;->L:F

    sub-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 112
    iput v5, p0, Lsoftware/simplicial/a/bh;->L:F

    .line 115
    :cond_0
    invoke-super {p0, p1}, Lsoftware/simplicial/a/h;->e(F)V

    .line 118
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->D:B

    if-le v0, v4, :cond_1

    .line 119
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->D:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->D:B

    .line 120
    :cond_1
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->A:B

    if-le v0, v4, :cond_2

    .line 121
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->A:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->A:B

    .line 122
    :cond_2
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->B:B

    if-le v0, v4, :cond_3

    .line 123
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->B:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->B:B

    .line 124
    :cond_3
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->C:B

    if-le v0, v4, :cond_4

    .line 125
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->C:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->C:B

    .line 126
    :cond_4
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->E:B

    if-le v0, v4, :cond_5

    .line 127
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->E:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->E:B

    .line 128
    :cond_5
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->F:B

    if-le v0, v4, :cond_6

    .line 129
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->F:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->F:B

    .line 130
    :cond_6
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->G:B

    if-le v0, v4, :cond_7

    .line 131
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->G:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->G:B

    .line 132
    :cond_7
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->H:B

    if-le v0, v4, :cond_8

    .line 133
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->H:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->H:B

    .line 134
    :cond_8
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->K:B

    if-le v0, v4, :cond_9

    .line 135
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->K:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->K:B

    .line 136
    :cond_9
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->J:B

    if-le v0, v4, :cond_a

    .line 137
    iget-byte v0, p0, Lsoftware/simplicial/a/bh;->J:B

    add-int/lit8 v0, v0, -0x1

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/bh;->J:B

    .line 138
    :cond_a
    return-void
.end method

.method k()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/a/bh;->O:Lsoftware/simplicial/a/a/e;

    invoke-virtual {v0}, Lsoftware/simplicial/a/a/e;->a()V

    .line 157
    return-void
.end method
