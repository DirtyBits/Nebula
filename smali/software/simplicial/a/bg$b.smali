.class public final enum Lsoftware/simplicial/a/bg$b;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/bg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bg$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lsoftware/simplicial/a/bg$b;

.field public static final B:[Lsoftware/simplicial/a/bg$b;

.field private static final synthetic C:[Lsoftware/simplicial/a/bg$b;

.field public static final enum a:Lsoftware/simplicial/a/bg$b;

.field public static final enum b:Lsoftware/simplicial/a/bg$b;

.field public static final enum c:Lsoftware/simplicial/a/bg$b;

.field public static final enum d:Lsoftware/simplicial/a/bg$b;

.field public static final enum e:Lsoftware/simplicial/a/bg$b;

.field public static final enum f:Lsoftware/simplicial/a/bg$b;

.field public static final enum g:Lsoftware/simplicial/a/bg$b;

.field public static final enum h:Lsoftware/simplicial/a/bg$b;

.field public static final enum i:Lsoftware/simplicial/a/bg$b;

.field public static final enum j:Lsoftware/simplicial/a/bg$b;

.field public static final enum k:Lsoftware/simplicial/a/bg$b;

.field public static final enum l:Lsoftware/simplicial/a/bg$b;

.field public static final enum m:Lsoftware/simplicial/a/bg$b;

.field public static final enum n:Lsoftware/simplicial/a/bg$b;

.field public static final enum o:Lsoftware/simplicial/a/bg$b;

.field public static final enum p:Lsoftware/simplicial/a/bg$b;

.field public static final enum q:Lsoftware/simplicial/a/bg$b;

.field public static final enum r:Lsoftware/simplicial/a/bg$b;

.field public static final enum s:Lsoftware/simplicial/a/bg$b;

.field public static final enum t:Lsoftware/simplicial/a/bg$b;

.field public static final enum u:Lsoftware/simplicial/a/bg$b;

.field public static final enum v:Lsoftware/simplicial/a/bg$b;

.field public static final enum w:Lsoftware/simplicial/a/bg$b;

.field public static final enum x:Lsoftware/simplicial/a/bg$b;

.field public static final enum y:Lsoftware/simplicial/a/bg$b;

.field public static final enum z:Lsoftware/simplicial/a/bg$b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->a:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->b:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "ONLINE_UNKNOWN"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->d:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "FFA_GAME"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->e:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "FFA_CLASSIC_GAME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->f:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "FFA_TIME_GAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->g:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "TEAM_GAME"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->h:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "TEAM_TIME_GAME"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->i:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "CTF_GAME"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->j:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "SURVIVAL_GAME"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->k:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "CHAT_LOBBY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->l:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "SOCCER_GAME"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->m:Lsoftware/simplicial/a/bg$b;

    .line 67
    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "CLAN_WAR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->n:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "ARENA"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->o:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "DOMINATION_GAME"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->p:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "TEAM_ARENA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->q:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "FFA_ULTRA_GAME"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->r:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "ZA_GAME"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->s:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "TOURNEY"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->t:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "PAINT_GAME"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->u:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "TEAM_DEATHMATCH_GAME"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->v:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "X_GAME"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->w:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "X2_GAME"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->x:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "X3_GAME"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->y:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "X4_GAME"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->z:Lsoftware/simplicial/a/bg$b;

    new-instance v0, Lsoftware/simplicial/a/bg$b;

    const-string v1, "X5_GAME"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bg$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bg$b;->A:Lsoftware/simplicial/a/bg$b;

    .line 64
    const/16 v0, 0x1b

    new-array v0, v0, [Lsoftware/simplicial/a/bg$b;

    sget-object v1, Lsoftware/simplicial/a/bg$b;->a:Lsoftware/simplicial/a/bg$b;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bg$b;->b:Lsoftware/simplicial/a/bg$b;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/bg$b;->d:Lsoftware/simplicial/a/bg$b;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/bg$b;->e:Lsoftware/simplicial/a/bg$b;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/bg$b;->f:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/bg$b;->g:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/bg$b;->h:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/bg$b;->i:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/bg$b;->j:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/bg$b;->k:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/bg$b;->l:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/bg$b;->m:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/bg$b;->n:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/bg$b;->o:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/a/bg$b;->p:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/a/bg$b;->q:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lsoftware/simplicial/a/bg$b;->r:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lsoftware/simplicial/a/bg$b;->s:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lsoftware/simplicial/a/bg$b;->t:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lsoftware/simplicial/a/bg$b;->u:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lsoftware/simplicial/a/bg$b;->v:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lsoftware/simplicial/a/bg$b;->w:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lsoftware/simplicial/a/bg$b;->x:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lsoftware/simplicial/a/bg$b;->y:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lsoftware/simplicial/a/bg$b;->z:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lsoftware/simplicial/a/bg$b;->A:Lsoftware/simplicial/a/bg$b;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/bg$b;->C:[Lsoftware/simplicial/a/bg$b;

    .line 68
    invoke-static {}, Lsoftware/simplicial/a/bg$b;->values()[Lsoftware/simplicial/a/bg$b;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/bg$b;->B:[Lsoftware/simplicial/a/bg$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bg$b;
    .locals 1

    .prologue
    .line 64
    const-class v0, Lsoftware/simplicial/a/bg$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg$b;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bg$b;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lsoftware/simplicial/a/bg$b;->C:[Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bg$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bg$b;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 72
    sget-object v1, Lsoftware/simplicial/a/bg$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/bg$b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 104
    :goto_0
    :pswitch_0
    return v0

    .line 90
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 96
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 98
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
