.class public final enum Lsoftware/simplicial/a/ap;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/ap;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/ap;

.field public static final enum b:Lsoftware/simplicial/a/ap;

.field public static final enum c:Lsoftware/simplicial/a/ap;

.field public static final enum d:Lsoftware/simplicial/a/ap;

.field public static final e:[Lsoftware/simplicial/a/ap;

.field private static final synthetic f:[Lsoftware/simplicial/a/ap;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/ap;

    const-string v1, "TINY"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    new-instance v0, Lsoftware/simplicial/a/ap;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    new-instance v0, Lsoftware/simplicial/a/ap;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    new-instance v0, Lsoftware/simplicial/a/ap;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/ap;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ap;->d:Lsoftware/simplicial/a/ap;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/ap;

    sget-object v1, Lsoftware/simplicial/a/ap;->a:Lsoftware/simplicial/a/ap;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/ap;->b:Lsoftware/simplicial/a/ap;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/ap;->d:Lsoftware/simplicial/a/ap;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/ap;->f:[Lsoftware/simplicial/a/ap;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/ap;->values()[Lsoftware/simplicial/a/ap;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/ap;
    .locals 1

    .prologue
    .line 14
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ap;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/ap;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ap;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/ap;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/ap;->f:[Lsoftware/simplicial/a/ap;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/ap;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/ap;

    return-object v0
.end method
