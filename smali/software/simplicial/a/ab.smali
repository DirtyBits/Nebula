.class public final enum Lsoftware/simplicial/a/ab;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/ab;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/ab;

.field public static final enum b:Lsoftware/simplicial/a/ab;

.field public static final enum c:Lsoftware/simplicial/a/ab;

.field public static final enum d:Lsoftware/simplicial/a/ab;

.field public static final enum e:Lsoftware/simplicial/a/ab;

.field public static final enum f:Lsoftware/simplicial/a/ab;

.field public static final enum g:Lsoftware/simplicial/a/ab;

.field public static final enum h:Lsoftware/simplicial/a/ab;

.field public static final enum i:Lsoftware/simplicial/a/ab;

.field public static final enum j:Lsoftware/simplicial/a/ab;

.field public static final enum k:Lsoftware/simplicial/a/ab;

.field public static final enum l:Lsoftware/simplicial/a/ab;

.field public static final enum m:Lsoftware/simplicial/a/ab;

.field public static final enum n:Lsoftware/simplicial/a/ab;

.field public static final enum o:Lsoftware/simplicial/a/ab;

.field public static final enum p:Lsoftware/simplicial/a/ab;

.field public static final enum q:Lsoftware/simplicial/a/ab;

.field public static final r:[Lsoftware/simplicial/a/ab;

.field private static final synthetic s:[Lsoftware/simplicial/a/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->a:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_FFA_TIME"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->b:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_TEAMS_TIME"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->c:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_CTF"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->d:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_SURVIVAL"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->e:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_SOCCER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->f:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "REACH_HS_FFA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->g:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "REACH_HS_TEAMS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->h:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "COLLECT_X_DOTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->i:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "ABSORB_X_BLOBS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->j:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "REACH_HS_FFA_CLASSIC"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->k:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_DOM"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->l:Lsoftware/simplicial/a/ab;

    .line 10
    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_ARENA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->m:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "REACH_HS_FFA_ULTRA"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->n:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "SURVIVE_ZA"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->o:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_PAINT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->p:Lsoftware/simplicial/a/ab;

    new-instance v0, Lsoftware/simplicial/a/ab;

    const-string v1, "WIN_TEAM_DEATHMATCH"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ab;->q:Lsoftware/simplicial/a/ab;

    .line 6
    const/16 v0, 0x11

    new-array v0, v0, [Lsoftware/simplicial/a/ab;

    sget-object v1, Lsoftware/simplicial/a/ab;->a:Lsoftware/simplicial/a/ab;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/ab;->b:Lsoftware/simplicial/a/ab;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/ab;->c:Lsoftware/simplicial/a/ab;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/ab;->d:Lsoftware/simplicial/a/ab;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/ab;->e:Lsoftware/simplicial/a/ab;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/ab;->f:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/ab;->g:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/ab;->h:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/ab;->i:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/ab;->j:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/ab;->k:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/ab;->l:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/ab;->m:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/ab;->n:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/ab;->o:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/a/ab;->p:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/a/ab;->q:Lsoftware/simplicial/a/ab;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/ab;->s:[Lsoftware/simplicial/a/ab;

    .line 12
    invoke-static {}, Lsoftware/simplicial/a/ab;->values()[Lsoftware/simplicial/a/ab;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/ab;->r:[Lsoftware/simplicial/a/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ab;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ab;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/ab;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/ab;->s:[Lsoftware/simplicial/a/ab;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/ab;

    return-object v0
.end method
