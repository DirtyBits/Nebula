.class public Lsoftware/simplicial/a/bl;
.super Lsoftware/simplicial/a/ag;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/bl$a;,
        Lsoftware/simplicial/a/bl$b;
    }
.end annotation


# static fields
.field public static final k:F


# instance fields
.field public A:Lsoftware/simplicial/a/bl$b;

.field public B:Lsoftware/simplicial/a/bl$a;

.field public C:Z

.field public D:I

.field public E:Z

.field public F:F

.field G:Lsoftware/simplicial/a/bf;

.field public H:Lsoftware/simplicial/a/ao;

.field public I:Lsoftware/simplicial/a/bh;

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    sget v0, Lsoftware/simplicial/a/ai;->Q:F

    const v1, 0x3b656042    # 0.0035f

    mul-float/2addr v0, v1

    sput v0, Lsoftware/simplicial/a/bl;->k:F

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lsoftware/simplicial/a/ag;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bl;->C:Z

    .line 37
    iput v2, p0, Lsoftware/simplicial/a/bl;->D:I

    .line 38
    iput-boolean v2, p0, Lsoftware/simplicial/a/bl;->E:Z

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bl;->F:F

    .line 40
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    .line 41
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 42
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 53
    sget v0, Lsoftware/simplicial/a/bl;->a:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->n:F

    .line 54
    return-void
.end method

.method public constructor <init>(ILsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lsoftware/simplicial/a/ag;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bl;->C:Z

    .line 37
    iput v2, p0, Lsoftware/simplicial/a/bl;->D:I

    .line 38
    iput-boolean v2, p0, Lsoftware/simplicial/a/bl;->E:Z

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bl;->F:F

    .line 40
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    .line 41
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 42
    iput-object v1, p0, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 46
    invoke-virtual {p0, p2, p3, p4, p5}, Lsoftware/simplicial/a/bl;->a(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V

    .line 47
    iput p1, p0, Lsoftware/simplicial/a/bl;->z:I

    .line 48
    sget v0, Lsoftware/simplicial/a/bl;->a:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->n:F

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 108
    const/high16 v0, 0x42900000    # 72.0f

    .line 109
    iget v1, p0, Lsoftware/simplicial/a/bl;->s:F

    iget v2, p0, Lsoftware/simplicial/a/bl;->l:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v0

    if-gtz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/bl;->t:F

    iget v2, p0, Lsoftware/simplicial/a/bl;->m:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1

    .line 111
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/bl;->l:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->s:F

    .line 112
    iget v0, p0, Lsoftware/simplicial/a/bl;->m:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->t:F

    .line 113
    iget v0, p0, Lsoftware/simplicial/a/bl;->n:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->w:F

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/bl;->F:F

    .line 122
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/bl;->s:F

    iget v1, p0, Lsoftware/simplicial/a/bl;->l:F

    iget v2, p0, Lsoftware/simplicial/a/bl;->s:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bl;->s:F

    .line 119
    iget v0, p0, Lsoftware/simplicial/a/bl;->t:F

    iget v1, p0, Lsoftware/simplicial/a/bl;->m:F

    iget v2, p0, Lsoftware/simplicial/a/bl;->t:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bl;->t:F

    .line 120
    iget v0, p0, Lsoftware/simplicial/a/bl;->w:F

    iget v1, p0, Lsoftware/simplicial/a/bl;->n:F

    iget v2, p0, Lsoftware/simplicial/a/bl;->w:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/bl;->w:F

    goto :goto_0
.end method

.method public a(F)V
    .locals 2

    .prologue
    const v1, 0x3f19999a    # 0.6f

    .line 97
    iget v0, p0, Lsoftware/simplicial/a/bl;->F:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 99
    iget v0, p0, Lsoftware/simplicial/a/bl;->F:F

    add-float/2addr v0, p1

    iput v0, p0, Lsoftware/simplicial/a/bl;->F:F

    .line 100
    iget v0, p0, Lsoftware/simplicial/a/bl;->F:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 101
    iput v1, p0, Lsoftware/simplicial/a/bl;->F:F

    .line 103
    :cond_0
    return-void
.end method

.method public a(Ljava/util/Random;Z[Z)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 65
    sget-object v3, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 66
    sget-object v6, Lsoftware/simplicial/a/bl$b;->l:Lsoftware/simplicial/a/bl$b;

    if-eq v5, v6, :cond_1

    if-eqz p3, :cond_0

    invoke-virtual {v5}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v6

    array-length v7, p3

    if-ge v6, v7, :cond_1

    invoke-virtual {v5}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v6

    aget-boolean v6, p3, v6

    if-eqz v6, :cond_1

    .line 67
    :cond_0
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/bl$b;->k:Lsoftware/simplicial/a/bl$b;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 69
    if-nez p2, :cond_3

    .line 70
    sget-object v0, Lsoftware/simplicial/a/bl$b;->g:Lsoftware/simplicial/a/bl$b;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 71
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 73
    sget-object v0, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 74
    sget-object v0, Lsoftware/simplicial/a/bl$b;->l:Lsoftware/simplicial/a/bl$b;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 75
    if-nez p2, :cond_4

    .line 76
    sget-object v0, Lsoftware/simplicial/a/bl$b;->g:Lsoftware/simplicial/a/bl$b;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 77
    :cond_4
    sget-object v0, Lsoftware/simplicial/a/bl$b;->k:Lsoftware/simplicial/a/bl$b;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 79
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bl$b;

    iput-object v0, p0, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    .line 81
    sget-object v0, Lsoftware/simplicial/a/bl$a;->b:Lsoftware/simplicial/a/bl$a;

    iput-object v0, p0, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bl;->C:Z

    .line 84
    iput-object v8, p0, Lsoftware/simplicial/a/bl;->G:Lsoftware/simplicial/a/bf;

    .line 85
    iput-object v8, p0, Lsoftware/simplicial/a/bl;->I:Lsoftware/simplicial/a/bh;

    .line 86
    iput v1, p0, Lsoftware/simplicial/a/bl;->D:I

    .line 87
    iput-object v8, p0, Lsoftware/simplicial/a/bl;->H:Lsoftware/simplicial/a/ao;

    .line 89
    invoke-super {p0, p1}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 91
    sget v0, Lsoftware/simplicial/a/bl;->a:F

    iput v0, p0, Lsoftware/simplicial/a/bl;->n:F

    .line 92
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/bl;->C:Z

    .line 130
    return-void
.end method
