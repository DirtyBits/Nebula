.class public final enum Lsoftware/simplicial/a/bv;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/bv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/bv;

.field public static final enum b:Lsoftware/simplicial/a/bv;

.field public static final enum c:Lsoftware/simplicial/a/bv;

.field public static final enum d:Lsoftware/simplicial/a/bv;

.field public static final enum e:Lsoftware/simplicial/a/bv;

.field public static final enum f:Lsoftware/simplicial/a/bv;

.field public static final enum g:Lsoftware/simplicial/a/bv;

.field public static final enum h:Lsoftware/simplicial/a/bv;

.field public static final enum i:Lsoftware/simplicial/a/bv;

.field public static final enum j:Lsoftware/simplicial/a/bv;

.field public static final enum k:Lsoftware/simplicial/a/bv;

.field public static final enum l:Lsoftware/simplicial/a/bv;

.field public static final enum m:Lsoftware/simplicial/a/bv;

.field public static final enum n:Lsoftware/simplicial/a/bv;

.field public static final enum o:Lsoftware/simplicial/a/bv;

.field public static final p:[Lsoftware/simplicial/a/bv;

.field public static final q:[Lsoftware/simplicial/a/bv;

.field private static final synthetic r:[Lsoftware/simplicial/a/bv;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "PUMPKIN"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->a:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "SNOWFLAKE"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->b:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "HEART"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->c:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "LEAF"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->d:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "BIGDOT"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "COIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "PRESENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->g:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "BEAD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->h:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "EGG"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->i:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "RAINDROP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->j:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "NEBULA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->k:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "CANDY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->l:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "SUN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->m:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "MOON"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->n:Lsoftware/simplicial/a/bv;

    new-instance v0, Lsoftware/simplicial/a/bv;

    const-string v1, "NOTE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/bv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/bv;->o:Lsoftware/simplicial/a/bv;

    .line 6
    const/16 v0, 0xf

    new-array v0, v0, [Lsoftware/simplicial/a/bv;

    sget-object v1, Lsoftware/simplicial/a/bv;->a:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bv;->b:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/bv;->c:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/bv;->d:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/bv;->g:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/bv;->h:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/bv;->i:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/bv;->j:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/bv;->k:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/bv;->l:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/bv;->m:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/bv;->n:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/bv;->o:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/bv;->r:[Lsoftware/simplicial/a/bv;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/bv;->values()[Lsoftware/simplicial/a/bv;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    .line 10
    const/16 v0, 0xc

    new-array v0, v0, [Lsoftware/simplicial/a/bv;

    sget-object v1, Lsoftware/simplicial/a/bv;->a:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/bv;->d:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/bv;->g:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/bv;->b:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/bv;->h:Lsoftware/simplicial/a/bv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/bv;->i:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/bv;->j:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/bv;->k:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/bv;->l:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/bv;->m:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/bv;->n:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/bv;->o:Lsoftware/simplicial/a/bv;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/bv;->q:[Lsoftware/simplicial/a/bv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bv;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bv;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/bv;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/bv;->r:[Lsoftware/simplicial/a/bv;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/bv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/bv;

    return-object v0
.end method
