.class public Lsoftware/simplicial/a/f/ai;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[B

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Z

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 26
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;[B)[B
    .locals 4

    .prologue
    .line 55
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ai;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 57
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 59
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 61
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 62
    array-length v0, p4

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 63
    const/4 v0, 0x0

    array-length v1, p4

    invoke-virtual {p0, p4, v0, v1}, Lsoftware/simplicial/a/f/bn;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 65
    :catch_0
    move-exception v0

    .line 67
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 68
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;[BIZJLjava/lang/String;)[B
    .locals 4

    .prologue
    .line 32
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ai;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 34
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 37
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 38
    invoke-virtual {p0, p6, p7}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 39
    array-length v0, p3

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 40
    const/4 v0, 0x0

    array-length v1, p3

    invoke-virtual {p0, p3, v0, v1}, Lsoftware/simplicial/a/f/bn;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 44
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 45
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 78
    :try_start_0
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/ai;->ar:I

    .line 79
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 80
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/ai;->a:Ljava/lang/String;

    .line 81
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/ai;->c:Ljava/lang/String;

    .line 82
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/ai;->d:I

    .line 83
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v2

    iput-boolean v2, p0, Lsoftware/simplicial/a/f/ai;->e:Z

    .line 84
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/f/ai;->f:J

    .line 85
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_1

    .line 87
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/ai;->b:[B

    .line 88
    iget-object v2, p0, Lsoftware/simplicial/a/f/ai;->b:[B

    array-length v2, v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_0

    .line 89
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :catch_0
    move-exception v1

    .line 99
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/ai;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 103
    :goto_0
    return v0

    .line 90
    :cond_0
    :try_start_1
    iget-object v2, p0, Lsoftware/simplicial/a/f/ai;->b:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lsoftware/simplicial/a/f/ai;->b:[B

    array-length v4, v4

    invoke-virtual {v1, v2, v3, v4}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 103
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    const/4 v1, 0x0

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/ai;->b:[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
