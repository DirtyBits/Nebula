.class public Lsoftware/simplicial/a/f/k;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lsoftware/simplicial/a/q;

.field public d:I

.field public e:J

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/k;->f:Z

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/f/k;->g:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;)[B
    .locals 4

    .prologue
    .line 57
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 59
    const-string v0, ""

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 61
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 62
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 63
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 65
    :catch_0
    move-exception v0

    .line 67
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 68
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;Ljava/lang/String;Lsoftware/simplicial/a/q;IJ)[B
    .locals 4

    .prologue
    .line 35
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/k;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 37
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p4}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 40
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 41
    invoke-virtual {p0, p6, p7}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    .line 45
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 46
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 78
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/k;->ar:I

    .line 79
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 81
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/k;->a:Ljava/lang/String;

    .line 82
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/k;->b:Ljava/lang/String;

    .line 83
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 84
    if-ltz v0, :cond_0

    sget-object v2, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 85
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    .line 86
    :cond_1
    sget-object v2, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/k;->c:Lsoftware/simplicial/a/q;

    .line 87
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/k;->d:I

    .line 88
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/f/k;->e:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 90
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/k;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 93
    const/4 v0, 0x0

    goto :goto_0
.end method
