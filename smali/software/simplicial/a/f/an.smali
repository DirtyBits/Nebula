.class public Lsoftware/simplicial/a/f/an;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:[B

.field public e:B

.field public f:I

.field public g:Lsoftware/simplicial/a/d/c;

.field public h:I

.field public i:I

.field public j:I

.field public k:F

.field public l:S

.field public m:S

.field public n:Z

.field public o:[Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lsoftware/simplicial/a/f/bh;->B:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 37
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;FSSZ[Z)[B
    .locals 5

    .prologue
    .line 45
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/f/bh;->B:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v1, p1}, Lsoftware/simplicial/a/f/an;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 46
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 47
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 48
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 49
    invoke-virtual {p8}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 50
    invoke-virtual {p9}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 51
    invoke-virtual/range {p11 .. p11}, Lsoftware/simplicial/a/d/c;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 52
    invoke-virtual {p10}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 53
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 55
    move/from16 v0, p12

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V

    .line 56
    move/from16 v0, p13

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 57
    move/from16 v0, p14

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 58
    move/from16 v0, p15

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 59
    array-length v1, p7

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 60
    const/4 v1, 0x0

    array-length v2, p7

    invoke-virtual {p0, p7, v1, v2}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 61
    if-eqz p16, :cond_0

    move-object/from16 v0, p16

    array-length v1, v0

    :goto_0
    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 62
    if-eqz p16, :cond_1

    .line 63
    move-object/from16 v0, p16

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-boolean v3, p16, v1

    .line 64
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    :cond_0
    const/4 v1, -0x1

    goto :goto_0

    .line 66
    :catch_0
    move-exception v1

    .line 68
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred writing data.\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 69
    const/4 v1, 0x0

    .line 72
    :goto_2
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 79
    :try_start_0
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/an;->ar:I

    .line 80
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 82
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/an;->b:Z

    .line 83
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/an;->f:I

    .line 84
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/an;->h:I

    .line 85
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/an;->e:B

    .line 86
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/an;->j:I

    .line 87
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/d/c;->a(B)Lsoftware/simplicial/a/d/c;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->g:Lsoftware/simplicial/a/d/c;

    .line 88
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/an;->i:I

    .line 89
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->a:Ljava/lang/String;

    .line 90
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->c:Ljava/lang/String;

    .line 92
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readFloat()F

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/an;->k:F

    .line 93
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput-short v1, p0, Lsoftware/simplicial/a/f/an;->l:S

    .line 94
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput-short v1, p0, Lsoftware/simplicial/a/f/an;->m:S

    .line 95
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/an;->n:Z

    .line 96
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v1

    if-lez v1, :cond_1

    .line 98
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->d:[B

    .line 99
    iget-object v1, p0, Lsoftware/simplicial/a/f/an;->d:[B

    array-length v1, v1

    const/16 v3, 0x10

    if-le v1, v3, :cond_0

    .line 100
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :catch_0
    move-exception v1

    .line 125
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/an;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 129
    :goto_0
    return v0

    .line 101
    :cond_0
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/a/f/an;->d:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lsoftware/simplicial/a/f/an;->d:[B

    array-length v4, v4

    invoke-virtual {v2, v1, v3, v4}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 108
    :goto_1
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v1

    if-lez v1, :cond_4

    .line 110
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    .line 111
    if-ltz v1, :cond_2

    .line 113
    new-array v1, v1, [Z

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->o:[Z

    move v1, v0

    .line 114
    :goto_2
    iget-object v3, p0, Lsoftware/simplicial/a/f/an;->o:[Z

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 115
    iget-object v3, p0, Lsoftware/simplicial/a/f/an;->o:[Z

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v4

    aput-boolean v4, v3, v1

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 105
    :cond_1
    const/4 v1, 0x0

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->d:[B

    goto :goto_1

    .line 118
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->o:[Z

    .line 129
    :cond_3
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 121
    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Lsoftware/simplicial/a/f/an;->o:[Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method
