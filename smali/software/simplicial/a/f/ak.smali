.class public Lsoftware/simplicial/a/f/ak;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bi;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/br;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/f/ad;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bu;",
            ">;"
        }
    .end annotation
.end field

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ax:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ak;->b:Ljava/util/List;

    .line 37
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->c:I

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ak;->d:Ljava/util/List;

    .line 40
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->e:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ak;->f:Ljava/util/List;

    .line 43
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->g:I

    .line 44
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->h:I

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ak;->i:Ljava/util/List;

    .line 47
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->j:I

    .line 48
    iput v1, p0, Lsoftware/simplicial/a/f/ak;->k:I

    .line 53
    return-void
.end method

.method private a(IF)Lsoftware/simplicial/a/f/bn;
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 211
    invoke-direct {p0, v0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;IF)V

    .line 212
    invoke-direct {p0}, Lsoftware/simplicial/a/f/ak;->a()V

    .line 213
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 218
    iput v2, p0, Lsoftware/simplicial/a/f/ak;->c:I

    .line 219
    iput v2, p0, Lsoftware/simplicial/a/f/ak;->e:I

    .line 220
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->h:I

    iget v1, p0, Lsoftware/simplicial/a/f/ak;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/f/ak;->h:I

    .line 221
    iput v2, p0, Lsoftware/simplicial/a/f/ak;->g:I

    .line 222
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->k:I

    iget v1, p0, Lsoftware/simplicial/a/f/ak;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/f/ak;->k:I

    .line 223
    iput v2, p0, Lsoftware/simplicial/a/f/ak;->j:I

    .line 224
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bn;IF)V
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ax:Lsoftware/simplicial/a/f/bh;

    invoke-static {p1, v0}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 229
    invoke-virtual {p1, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 230
    invoke-virtual {p1, p3}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V

    .line 231
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->c:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 232
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->e:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 233
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->h:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 234
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->g:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 235
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->k:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 236
    iget v0, p0, Lsoftware/simplicial/a/f/ak;->j:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 237
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bn;)[B
    .locals 4

    .prologue
    .line 195
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    .line 196
    const/16 v1, 0x9

    .line 197
    const/16 v2, 0xa

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->c:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 198
    const/16 v1, 0xb

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->e:I

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 199
    const/16 v2, 0xc

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->h:I

    ushr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 200
    const/16 v1, 0xd

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->h:I

    ushr-int/lit8 v3, v3, 0x0

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 201
    const/16 v2, 0xe

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->g:I

    ushr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 202
    const/16 v1, 0xf

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->g:I

    ushr-int/lit8 v3, v3, 0x0

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 203
    const/16 v2, 0x10

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->k:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 204
    iget v1, p0, Lsoftware/simplicial/a/f/ak;->j:I

    int-to-byte v1, v1

    aput-byte v1, v0, v2

    .line 205
    return-object v0
.end method


# virtual methods
.method public a(IFLjava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/ae;[Lsoftware/simplicial/a/bt;Lsoftware/simplicial/a/f/bl;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IF",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bf;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bq;",
            ">;[",
            "Lsoftware/simplicial/a/ae;",
            "[",
            "Lsoftware/simplicial/a/bt;",
            "Lsoftware/simplicial/a/f/bl;",
            ")",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 62
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/a/f/ak;->a()V

    .line 63
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/ak;->h:I

    .line 64
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/ak;->k:I

    .line 67
    new-instance v6, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v6}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 68
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(IF)Lsoftware/simplicial/a/f/bn;

    move-result-object v3

    .line 70
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/bf;

    .line 72
    iget-boolean v5, v2, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v5, :cond_12

    .line 74
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->c()V

    .line 76
    iget v5, v2, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 77
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->U:Lsoftware/simplicial/a/e;

    invoke-virtual {v5}, Lsoftware/simplicial/a/e;->a()I

    move-result v5

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 78
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x119

    if-lt v5, v8, :cond_0

    .line 80
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->W:Lsoftware/simplicial/a/af;

    iget-byte v5, v5, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 81
    iget v5, v2, Lsoftware/simplicial/a/bf;->ad:I

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 83
    :cond_0
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x177

    if-lt v5, v8, :cond_1

    .line 85
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iget-byte v5, v5, Lsoftware/simplicial/a/bd;->c:B

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 86
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->Y:Lsoftware/simplicial/a/bd;

    iget-short v5, v5, Lsoftware/simplicial/a/bd;->e:S

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 87
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x17f

    if-lt v5, v8, :cond_1

    .line 89
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->ac:Ljava/lang/String;

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 90
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->aa:Lsoftware/simplicial/a/as;

    iget-byte v5, v5, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 93
    :cond_1
    iget v5, v2, Lsoftware/simplicial/a/bf;->ah:I

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 94
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-nez v5, :cond_5

    const/4 v5, -0x1

    :goto_1
    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 95
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 96
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x13f

    if-lt v5, v8, :cond_2

    .line 98
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->E:[B

    array-length v5, v5

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 99
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->E:[B

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 101
    :cond_2
    iget v5, v2, Lsoftware/simplicial/a/bf;->A:I

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 102
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v8, v5, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v8, v9}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 103
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 104
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x14b

    if-lt v5, v8, :cond_4

    .line 106
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->I:[B

    array-length v5, v5

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 107
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->I:[B

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 108
    move-object/from16 v0, p7

    iget-short v5, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x155

    if-lt v5, v8, :cond_3

    .line 109
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    invoke-virtual {v5}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v5

    invoke-virtual {v6, v5}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 110
    :cond_3
    iget-object v2, v2, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    invoke-virtual {v2}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v2

    invoke-virtual {v6, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 114
    :cond_4
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v2

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v5

    add-int/2addr v2, v5

    const/16 v5, 0x200

    if-le v2, v5, :cond_11

    .line 116
    invoke-direct {p0, v3}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(IF)Lsoftware/simplicial/a/f/bn;

    move-result-object v2

    .line 119
    :goto_2
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v8

    invoke-virtual {v2, v3, v5, v8}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 120
    iget v3, p0, Lsoftware/simplicial/a/f/ak;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lsoftware/simplicial/a/f/ak;->c:I

    :goto_3
    move-object v3, v2

    .line 122
    goto/16 :goto_0

    .line 94
    :cond_5
    iget-object v5, v2, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v5, v5, Lsoftware/simplicial/a/bx;->c:I

    goto/16 :goto_1

    .line 124
    :cond_6
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/bq;

    .line 126
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->c()V

    .line 128
    move-object/from16 v0, p7

    iget-short v7, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x14b

    if-lt v7, v8, :cond_8

    .line 129
    iget v7, v2, Lsoftware/simplicial/a/bq;->c:I

    invoke-virtual {v6, v7}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 132
    :goto_5
    iget v7, v2, Lsoftware/simplicial/a/bq;->l:F

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 133
    iget v7, v2, Lsoftware/simplicial/a/bq;->m:F

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 134
    move-object/from16 v0, p7

    iget-short v7, v0, Lsoftware/simplicial/a/f/bl;->d:S

    const/16 v8, 0x14b

    if-lt v7, v8, :cond_9

    .line 135
    invoke-virtual {v2}, Lsoftware/simplicial/a/bq;->c()F

    move-result v2

    const/4 v7, 0x0

    const v8, 0x48f42400    # 500000.0f

    invoke-virtual {v6, v2, v7, v8}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 139
    :goto_6
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v2

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v7

    add-int/2addr v2, v7

    const/16 v7, 0x200

    if-le v2, v7, :cond_7

    .line 141
    invoke-direct {p0, v3}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(IF)Lsoftware/simplicial/a/f/bn;

    move-result-object v3

    .line 144
    :cond_7
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v8

    invoke-virtual {v3, v2, v7, v8}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 145
    iget v2, p0, Lsoftware/simplicial/a/f/ak;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/f/ak;->e:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 184
    :catch_0
    move-exception v2

    .line 186
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception occurred writing data. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v2, v4

    .line 190
    :goto_7
    return-object v2

    .line 131
    :cond_8
    :try_start_1
    iget v7, v2, Lsoftware/simplicial/a/bq;->c:I

    rem-int/lit8 v7, v7, 0x2e

    invoke-virtual {v6, v7}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    goto :goto_5

    .line 137
    :cond_9
    invoke-virtual {v2}, Lsoftware/simplicial/a/bq;->c()F

    move-result v2

    invoke-virtual {v6, v2}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V

    goto :goto_6

    .line 148
    :cond_a
    move-object/from16 v0, p5

    array-length v5, v0

    const/4 v2, 0x0

    :goto_8
    if-ge v2, v5, :cond_c

    aget-object v7, p5, v2

    .line 150
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->c()V

    .line 152
    iget v8, v7, Lsoftware/simplicial/a/ae;->l:F

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 153
    iget v7, v7, Lsoftware/simplicial/a/ae;->m:F

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 155
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v7

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v8

    add-int/2addr v7, v8

    const/16 v8, 0x200

    if-le v7, v8, :cond_b

    .line 157
    invoke-direct {p0, v3}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(IF)Lsoftware/simplicial/a/f/bn;

    move-result-object v3

    .line 160
    :cond_b
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v9

    invoke-virtual {v3, v7, v8, v9}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 161
    iget v7, p0, Lsoftware/simplicial/a/f/ak;->g:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lsoftware/simplicial/a/f/ak;->g:I

    .line 148
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 164
    :cond_c
    move-object/from16 v0, p6

    array-length v5, v0

    const/4 v2, 0x0

    move v10, v2

    move-object v2, v3

    move v3, v10

    :goto_9
    if-ge v3, v5, :cond_e

    aget-object v7, p6, v3

    .line 166
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->c()V

    .line 168
    iget-object v8, v7, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    invoke-virtual {v8}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v8

    invoke-virtual {v6, v8}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 169
    iget v8, v7, Lsoftware/simplicial/a/bt;->l:F

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 170
    iget v7, v7, Lsoftware/simplicial/a/bt;->m:F

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, p2}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 172
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v7

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v8

    add-int/2addr v7, v8

    const/16 v8, 0x200

    if-le v7, v8, :cond_d

    .line 174
    invoke-direct {p0, v2}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/ak;->a(IF)Lsoftware/simplicial/a/f/bn;

    move-result-object v2

    .line 177
    :cond_d
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v9

    invoke-virtual {v2, v7, v8, v9}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 178
    iget v7, p0, Lsoftware/simplicial/a/f/ak;->j:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lsoftware/simplicial/a/f/ak;->j:I

    .line 164
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 181
    :cond_e
    iget v3, p0, Lsoftware/simplicial/a/f/ak;->c:I

    if-gtz v3, :cond_f

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->g:I

    if-gtz v3, :cond_f

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->e:I

    if-gtz v3, :cond_f

    iget v3, p0, Lsoftware/simplicial/a/f/ak;->j:I

    if-lez v3, :cond_10

    .line 182
    :cond_f
    invoke-direct {p0, v2}, Lsoftware/simplicial/a/f/ak;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_10
    move-object v2, v4

    .line 190
    goto/16 :goto_7

    :cond_11
    move-object v2, v3

    goto/16 :goto_2

    :cond_12
    move-object v2, v3

    goto/16 :goto_3
.end method

.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 243
    :try_start_0
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 245
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->a:I

    .line 246
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readFloat()F

    move-result v3

    .line 247
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->c:I

    .line 248
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->e:I

    .line 249
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->h:I

    .line 250
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->g:I

    .line 251
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->k:I

    .line 252
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ak;->j:I

    move v1, v0

    .line 254
    :goto_0
    iget v4, p0, Lsoftware/simplicial/a/f/ak;->c:I

    if-ge v1, v4, :cond_1

    .line 256
    new-instance v4, Lsoftware/simplicial/a/bi;

    invoke-direct {v4}, Lsoftware/simplicial/a/bi;-><init>()V

    .line 257
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bi;->a:I

    .line 258
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    iput-short v5, v4, Lsoftware/simplicial/a/bi;->b:S

    .line 259
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    invoke-static {v5}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->c:Lsoftware/simplicial/a/af;

    .line 260
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bi;->h:I

    .line 261
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    iput-byte v5, v4, Lsoftware/simplicial/a/bi;->e:B

    .line 262
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    iput-short v5, v4, Lsoftware/simplicial/a/bi;->f:S

    .line 263
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->g:Ljava/lang/String;

    .line 264
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    invoke-static {v5}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->d:Lsoftware/simplicial/a/as;

    .line 265
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bi;->i:I

    .line 266
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    iput-byte v5, v4, Lsoftware/simplicial/a/bi;->j:B

    .line 267
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->k:Ljava/lang/String;

    .line 268
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    new-array v5, v5, [B

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->l:[B

    .line 269
    iget-object v5, v4, Lsoftware/simplicial/a/bi;->l:[B

    array-length v5, v5

    const/16 v6, 0x10

    if-le v5, v6, :cond_0

    .line 270
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :catch_0
    move-exception v1

    .line 318
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/ak;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 321
    :goto_1
    return v0

    .line 271
    :cond_0
    :try_start_1
    iget-object v5, v4, Lsoftware/simplicial/a/bi;->l:[B

    invoke-virtual {v2, v5}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 272
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bi;->m:I

    .line 273
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    iput-short v5, v4, Lsoftware/simplicial/a/bi;->n:S

    .line 274
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->o:Ljava/lang/String;

    .line 275
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    new-array v5, v5, [B

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->p:[B

    .line 276
    iget-object v5, v4, Lsoftware/simplicial/a/bi;->p:[B

    invoke-virtual {v2, v5}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 277
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    invoke-static {v5}, Lsoftware/simplicial/a/q;->a(B)Lsoftware/simplicial/a/q;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->q:Lsoftware/simplicial/a/q;

    .line 278
    sget-object v5, Lsoftware/simplicial/a/s;->d:[Lsoftware/simplicial/a/s;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v6

    aget-object v5, v5, v6

    iput-object v5, v4, Lsoftware/simplicial/a/bi;->r:Lsoftware/simplicial/a/s;

    .line 281
    iget-object v5, p0, Lsoftware/simplicial/a/f/ak;->b:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_1
    move v1, v0

    .line 284
    :goto_2
    iget v4, p0, Lsoftware/simplicial/a/f/ak;->e:I

    if-ge v1, v4, :cond_2

    .line 286
    new-instance v4, Lsoftware/simplicial/a/br;

    invoke-direct {v4}, Lsoftware/simplicial/a/br;-><init>()V

    .line 287
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/br;->a:I

    .line 288
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/br;->b:F

    .line 289
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/br;->c:F

    .line 290
    const/4 v5, 0x0

    const v6, 0x48f42400    # 500000.0f

    invoke-virtual {v2, v5, v6}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/br;->d:F

    .line 292
    iget-object v5, p0, Lsoftware/simplicial/a/f/ak;->d:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    move v1, v0

    .line 295
    :goto_3
    iget v4, p0, Lsoftware/simplicial/a/f/ak;->g:I

    if-ge v1, v4, :cond_3

    .line 297
    new-instance v4, Lsoftware/simplicial/a/f/ad;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/ad;-><init>()V

    .line 298
    iget v5, p0, Lsoftware/simplicial/a/f/ak;->h:I

    add-int/2addr v5, v1

    iput v5, v4, Lsoftware/simplicial/a/f/ad;->a:I

    .line 299
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/f/ad;->b:F

    .line 300
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/f/ad;->c:F

    .line 302
    iget-object v5, p0, Lsoftware/simplicial/a/f/ak;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move v1, v0

    .line 305
    :goto_4
    iget v4, p0, Lsoftware/simplicial/a/f/ak;->j:I

    if-ge v1, v4, :cond_4

    .line 307
    new-instance v4, Lsoftware/simplicial/a/bu;

    invoke-direct {v4}, Lsoftware/simplicial/a/bu;-><init>()V

    .line 308
    iget v5, p0, Lsoftware/simplicial/a/f/ak;->k:I

    add-int/2addr v5, v1

    iput v5, v4, Lsoftware/simplicial/a/bu;->a:I

    .line 309
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bu;->b:I

    .line 310
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bu;->c:F

    .line 311
    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bu;->d:F

    .line 313
    iget-object v5, p0, Lsoftware/simplicial/a/f/ak;->i:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 305
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 321
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_1
.end method
