.class public Lsoftware/simplicial/a/f/at;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lsoftware/simplicial/a/f/bh;->y:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/at;->a:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 14

    .prologue
    .line 113
    :try_start_0
    new-instance v10, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v10, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 115
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v11

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/a/f/at;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 117
    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v11, :cond_2

    .line 119
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    .line 120
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 121
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 122
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v6

    .line 123
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 124
    and-int/lit8 v2, v0, 0x3

    ushr-int/lit8 v7, v2, 0x0

    .line 125
    and-int/lit8 v0, v0, 0x4

    ushr-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v8, 0x1

    .line 126
    :goto_1
    invoke-virtual {v10}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    .line 127
    if-ltz v6, :cond_0

    sget-object v0, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    array-length v0, v0

    if-ge v6, v0, :cond_0

    if-ltz v7, :cond_0

    sget-object v0, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    array-length v0, v0

    if-ge v7, v0, :cond_0

    .line 128
    iget-object v12, p0, Lsoftware/simplicial/a/f/at;->a:Ljava/util/List;

    new-instance v0, Lsoftware/simplicial/a/d/b;

    const/4 v5, 0x0

    sget-object v13, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    aget-object v6, v13, v6

    sget-object v13, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    aget-object v7, v13, v7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/a/d/b;-><init>(ILjava/lang/String;IIILsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_0
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 125
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 131
    :catch_0
    move-exception v0

    .line 133
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/at;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 134
    const/4 v0, 0x0

    .line 137
    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method
