.class public Lsoftware/simplicial/a/f/cu;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/i/e;

.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aL:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/f/cu;->b:I

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cu;->c:Z

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cu;->d:Z

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 48
    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 50
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/i/e;->a(B)Lsoftware/simplicial/a/i/e;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/cu;->a:Lsoftware/simplicial/a/i/e;

    .line 51
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/cu;->b:I

    .line 52
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cu;->c:Z

    .line 53
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cu;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/cu;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 58
    const/4 v0, 0x0

    goto :goto_0
.end method
