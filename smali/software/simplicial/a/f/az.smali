.class public Lsoftware/simplicial/a/f/az;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[B

.field public c:Lsoftware/simplicial/a/e;

.field public d:Lsoftware/simplicial/a/af;

.field public e:I

.field public f:B

.field public g:Ljava/lang/String;

.field public h:I

.field public i:Lsoftware/simplicial/a/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lsoftware/simplicial/a/f/bh;->k:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 31
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILjava/lang/String;[BBISBILjava/lang/String;Lsoftware/simplicial/a/as;)[B
    .locals 4

    .prologue
    .line 38
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->k:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/az;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 39
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 40
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 42
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 43
    array-length v0, p3

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 44
    const/4 v0, 0x0

    array-length v1, p3

    invoke-virtual {p0, p3, v0, v1}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 45
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 46
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 47
    invoke-virtual {p0, p9}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 48
    iget-byte v0, p10, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 50
    :catch_0
    move-exception v0

    .line 52
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 53
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 63
    :try_start_0
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/az;->ar:I

    .line 64
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 65
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v2

    .line 66
    if-ltz v2, :cond_0

    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 67
    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    aget-object v2, v3, v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->c:Lsoftware/simplicial/a/e;

    .line 70
    :goto_0
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->a:Ljava/lang/String;

    .line 72
    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->d:Lsoftware/simplicial/a/af;

    .line 73
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/az;->e:I

    .line 74
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->b:[B

    .line 75
    sget-object v2, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v2, v2, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v2, p0, Lsoftware/simplicial/a/f/az;->f:B

    .line 76
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/az;->h:I

    .line 77
    const-string v2, ""

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->g:Ljava/lang/String;

    .line 78
    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->i:Lsoftware/simplicial/a/as;

    .line 79
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 81
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->d:Lsoftware/simplicial/a/af;

    .line 82
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/az;->e:I

    .line 84
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 86
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->b:[B

    .line 87
    iget-object v2, p0, Lsoftware/simplicial/a/f/az;->b:[B

    array-length v2, v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_1

    .line 88
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :catch_0
    move-exception v1

    .line 109
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/az;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 113
    :goto_1
    return v0

    .line 69
    :cond_0
    :try_start_1
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->c:Lsoftware/simplicial/a/e;

    goto :goto_0

    .line 89
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/a/f/az;->b:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lsoftware/simplicial/a/f/az;->b:[B

    array-length v4, v4

    invoke-virtual {v1, v2, v3, v4}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 91
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 93
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput-byte v2, p0, Lsoftware/simplicial/a/f/az;->f:B

    .line 94
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 96
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/az;->h:I

    .line 97
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 99
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/az;->g:Ljava/lang/String;

    .line 100
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/az;->i:Lsoftware/simplicial/a/as;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 113
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
