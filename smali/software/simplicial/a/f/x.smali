.class public Lsoftware/simplicial/a/f/x;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/f/z;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    .line 25
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IIIIILsoftware/simplicial/a/f/z;F)[B
    .locals 4

    .prologue
    .line 52
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p3}, Lsoftware/simplicial/a/f/x;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 54
    invoke-virtual {p6}, Lsoftware/simplicial/a/f/z;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 55
    sget-object v0, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne p6, v0, :cond_0

    .line 57
    invoke-virtual {p0, p1}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 58
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 59
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 60
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 61
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 66
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 67
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 77
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/x;->ar:I

    .line 78
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 80
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 81
    if-ltz v0, :cond_0

    sget-object v2, Lsoftware/simplicial/a/f/z;->e:[Lsoftware/simplicial/a/f/z;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 82
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/f/z;->c:Lsoftware/simplicial/a/f/z;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/z;->ordinal()I

    move-result v0

    .line 83
    :cond_1
    sget-object v2, Lsoftware/simplicial/a/f/z;->e:[Lsoftware/simplicial/a/f/z;

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v2, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne v0, v2, :cond_5

    .line 86
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/x;->c:I

    .line 87
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/x;->b:I

    .line 88
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/x;->d:I

    .line 89
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/x;->e:I

    .line 90
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readFloat()F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    .line 91
    iget v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    .line 93
    :cond_3
    iget v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_4

    .line 94
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    .line 95
    :cond_4
    iget v0, p0, Lsoftware/simplicial/a/f/x;->f:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    .line 96
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lsoftware/simplicial/a/f/x;->f:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_5
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 99
    :catch_0
    move-exception v0

    .line 101
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/x;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 102
    const/4 v0, 0x0

    goto :goto_0
.end method
