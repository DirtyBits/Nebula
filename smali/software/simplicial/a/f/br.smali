.class public Lsoftware/simplicial/a/f/br;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lsoftware/simplicial/a/ay;

.field public c:Ljava/lang/String;

.field public d:S


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 22
    sget-object v0, Lsoftware/simplicial/a/f/bh;->Q:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 15
    iput v1, p0, Lsoftware/simplicial/a/f/br;->a:I

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/br;->c:Ljava/lang/String;

    .line 18
    iput-short v1, p0, Lsoftware/simplicial/a/f/br;->d:S

    .line 23
    return-void
.end method

.method public constructor <init>(ILsoftware/simplicial/a/ay;Ljava/lang/String;S)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 27
    sget-object v0, Lsoftware/simplicial/a/f/bh;->Q:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 15
    iput v1, p0, Lsoftware/simplicial/a/f/br;->a:I

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/br;->c:Ljava/lang/String;

    .line 18
    iput-short v1, p0, Lsoftware/simplicial/a/f/br;->d:S

    .line 28
    iput p1, p0, Lsoftware/simplicial/a/f/br;->a:I

    .line 29
    iput-object p2, p0, Lsoftware/simplicial/a/f/br;->b:Lsoftware/simplicial/a/ay;

    .line 30
    iput-object p3, p0, Lsoftware/simplicial/a/f/br;->c:Ljava/lang/String;

    .line 31
    iput-short p4, p0, Lsoftware/simplicial/a/f/br;->d:S

    .line 32
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)[B
    .locals 4

    .prologue
    .line 86
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->Q:Lsoftware/simplicial/a/f/bh;

    invoke-static {p1, v0}, Lsoftware/simplicial/a/f/br;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 88
    iget-object v0, p0, Lsoftware/simplicial/a/f/br;->b:Lsoftware/simplicial/a/ay;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/ay;->a(Lsoftware/simplicial/a/f/bn;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 90
    :catch_0
    move-exception v0

    .line 92
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 93
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 104
    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 106
    new-instance v1, Lsoftware/simplicial/a/ay;

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/ay;-><init>(Lsoftware/simplicial/a/f/bm;)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/br;->b:Lsoftware/simplicial/a/ay;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 108
    :catch_0
    move-exception v0

    .line 110
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/br;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 111
    const/4 v0, 0x0

    goto :goto_0
.end method
