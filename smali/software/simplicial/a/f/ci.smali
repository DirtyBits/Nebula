.class public Lsoftware/simplicial/a/f/ci;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lsoftware/simplicial/a/h/a$a;

.field public d:Z

.field public e:I

.field public f:I

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/h/f;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field k:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ap:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 17
    iput v1, p0, Lsoftware/simplicial/a/f/ci;->a:I

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->b:Ljava/lang/String;

    .line 19
    sget-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->c:Lsoftware/simplicial/a/h/a$a;

    .line 20
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/ci;->d:Z

    .line 21
    iput v1, p0, Lsoftware/simplicial/a/f/ci;->e:I

    .line 22
    iput v1, p0, Lsoftware/simplicial/a/f/ci;->f:I

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->g:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->h:Ljava/util/List;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->i:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/ci;->j:Ljava/util/List;

    .line 28
    iput v1, p0, Lsoftware/simplicial/a/f/ci;->k:I

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 84
    :try_start_0
    new-instance v4, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 85
    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/ci;->b:Ljava/lang/String;

    .line 86
    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/ci;->a:I

    .line 88
    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 89
    and-int/lit8 v2, v3, 0x7

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v5, v2

    .line 90
    and-int/lit8 v2, v3, 0x8

    ushr-int/lit8 v2, v2, 0x3

    int-to-byte v2, v2

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/ci;->d:Z

    .line 91
    and-int/lit8 v2, v3, 0x30

    ushr-int/lit8 v2, v2, 0x4

    int-to-byte v2, v2

    iput v2, p0, Lsoftware/simplicial/a/f/ci;->e:I

    .line 92
    and-int/lit16 v2, v3, 0xc0

    ushr-int/lit8 v2, v2, 0x6

    int-to-byte v2, v2

    iput v2, p0, Lsoftware/simplicial/a/f/ci;->f:I

    .line 93
    sget-object v2, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    iput-object v2, p0, Lsoftware/simplicial/a/f/ci;->c:Lsoftware/simplicial/a/h/a$a;

    .line 94
    if-ltz v5, :cond_0

    sget-object v2, Lsoftware/simplicial/a/h/a$a;->g:[Lsoftware/simplicial/a/h/a$a;

    array-length v2, v2

    if-ge v5, v2, :cond_0

    .line 95
    sget-object v2, Lsoftware/simplicial/a/h/a$a;->g:[Lsoftware/simplicial/a/h/a$a;

    aget-object v2, v2, v5

    iput-object v2, p0, Lsoftware/simplicial/a/f/ci;->c:Lsoftware/simplicial/a/h/a$a;

    .line 97
    :cond_0
    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/ci;->k:I

    .line 98
    iget-object v2, p0, Lsoftware/simplicial/a/f/ci;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 99
    iget-object v2, p0, Lsoftware/simplicial/a/f/ci;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    move v3, v1

    .line 100
    :goto_1
    iget v2, p0, Lsoftware/simplicial/a/f/ci;->k:I

    if-ge v3, v2, :cond_4

    .line 102
    iget-object v2, p0, Lsoftware/simplicial/a/f/ci;->h:Ljava/util/List;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v2, p0, Lsoftware/simplicial/a/f/ci;->g:Ljava/util/List;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    .line 106
    iget-object v6, p0, Lsoftware/simplicial/a/f/ci;->j:Ljava/util/List;

    and-int/lit8 v2, v5, 0x1

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    and-int/lit8 v2, v5, 0x6

    ushr-int/lit8 v2, v2, 0x1

    int-to-byte v5, v2

    .line 109
    sget-object v2, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    .line 110
    if-ltz v5, :cond_1

    sget-object v6, Lsoftware/simplicial/a/h/f;->c:[Lsoftware/simplicial/a/h/f;

    array-length v6, v6

    if-ge v5, v6, :cond_1

    .line 111
    sget-object v2, Lsoftware/simplicial/a/h/f;->c:[Lsoftware/simplicial/a/h/f;

    aget-object v2, v2, v5

    .line 112
    :cond_1
    iget-object v5, p0, Lsoftware/simplicial/a/f/ci;->i:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    move v2, v1

    .line 90
    goto :goto_0

    :cond_3
    move v2, v1

    .line 106
    goto :goto_2

    .line 115
    :catch_0
    move-exception v0

    .line 117
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/ci;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 121
    :cond_4
    return v0
.end method
