.class public Lsoftware/simplicial/a/f/b;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[I

.field public c:[B

.field public d:[B

.field public e:[S


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lsoftware/simplicial/a/f/bh;->T:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/f/b;->a:I

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v0, 0x0

    .line 59
    :try_start_0
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 61
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    .line 62
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    if-le v1, v4, :cond_0

    const/16 v1, 0x64

    iput v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    .line 63
    :cond_0
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    if-gez v1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    .line 65
    :cond_1
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    new-array v1, v1, [I

    iput-object v1, p0, Lsoftware/simplicial/a/f/b;->b:[I

    .line 66
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/b;->c:[B

    .line 67
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/b;->d:[B

    .line 68
    iget v1, p0, Lsoftware/simplicial/a/f/b;->a:I

    new-array v1, v1, [S

    iput-object v1, p0, Lsoftware/simplicial/a/f/b;->e:[S

    move v1, v0

    .line 69
    :goto_0
    iget v3, p0, Lsoftware/simplicial/a/f/b;->a:I

    if-ge v1, v3, :cond_2

    .line 71
    iget-object v3, p0, Lsoftware/simplicial/a/f/b;->b:[I

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v4

    aput v4, v3, v1

    .line 72
    iget-object v3, p0, Lsoftware/simplicial/a/f/b;->c:[B

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v1

    .line 73
    iget-object v3, p0, Lsoftware/simplicial/a/f/b;->d:[B

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v1

    .line 74
    iget-object v3, p0, Lsoftware/simplicial/a/f/b;->e:[S

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v4

    aput-short v4, v3, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 79
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/b;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 83
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
