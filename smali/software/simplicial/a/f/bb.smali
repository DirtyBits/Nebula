.class public Lsoftware/simplicial/a/f/bb;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/f/ba;

.field public b:J

.field public c:B

.field public d:Ljava/lang/String;

.field public e:[B

.field public f:S

.field public g:B

.field public h:I

.field public i:B

.field public j:S

.field public k:Ljava/lang/String;

.field public l:I

.field public m:B

.field public n:I

.field public o:J

.field public p:Ljava/lang/String;

.field public q:[B

.field public r:Lsoftware/simplicial/a/q;

.field public s:I

.field public t:I

.field public u:Z

.field public v:Lsoftware/simplicial/a/s;

.field public w:I

.field public x:Lsoftware/simplicial/a/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 49
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;)[B
    .locals 2

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;J)[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;ILjava/lang/String;[BSBIILsoftware/simplicial/a/bx;IJLjava/lang/String;[BLsoftware/simplicial/a/q;IIZLsoftware/simplicial/a/s;IJLsoftware/simplicial/a/bd;Ljava/lang/String;Lsoftware/simplicial/a/as;)[B
    .locals 6

    .prologue
    .line 84
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v2, p1}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 85
    invoke-virtual {p2}, Lsoftware/simplicial/a/f/ba;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 86
    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne p2, v2, :cond_0

    .line 88
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 89
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 90
    invoke-virtual {p0, p9}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 91
    if-nez p10, :cond_1

    const/4 v2, -0x1

    :goto_0
    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 92
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 93
    move/from16 v0, p11

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 94
    move-wide/from16 v0, p12

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 95
    move-object/from16 v0, p14

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 96
    move/from16 v0, p17

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 97
    move/from16 v0, p18

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 98
    move/from16 v0, p19

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 99
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 100
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 101
    array-length v2, p5

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 102
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 103
    move-object/from16 v0, p15

    array-length v2, v0

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 104
    move-object/from16 v0, p15

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 105
    invoke-virtual/range {p20 .. p20}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 106
    move/from16 v0, p21

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 107
    invoke-virtual/range {p16 .. p16}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 108
    move-object/from16 v0, p24

    iget-byte v2, v0, Lsoftware/simplicial/a/bd;->c:B

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 109
    move-object/from16 v0, p24

    iget-short v2, v0, Lsoftware/simplicial/a/bd;->e:S

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 110
    move-object/from16 v0, p25

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 111
    move-object/from16 v0, p26

    iget-byte v2, v0, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 113
    :cond_0
    move-wide/from16 v0, p22

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v2

    :goto_1
    return-object v2

    .line 91
    :cond_1
    :try_start_1
    move-object/from16 v0, p10

    iget v2, v0, Lsoftware/simplicial/a/bx;->c:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v2

    .line 117
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception occurred writing data. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 118
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/f/ba;J)[B
    .locals 5

    .prologue
    .line 58
    sget-object v0, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne p2, v0, :cond_0

    .line 59
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "result"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/bb;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 64
    invoke-virtual {p2}, Lsoftware/simplicial/a/f/ba;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 65
    invoke-virtual {p0, p3, p4}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 69
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 70
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 128
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/bb;->ar:I

    .line 129
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 130
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/f/ba;->a(B)Lsoftware/simplicial/a/f/ba;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    .line 131
    iget-object v1, p0, Lsoftware/simplicial/a/f/bb;->a:Lsoftware/simplicial/a/f/ba;

    sget-object v2, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    if-ne v1, v2, :cond_3

    .line 133
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/bb;->c:B

    .line 134
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput-short v1, p0, Lsoftware/simplicial/a/f/bb;->f:S

    .line 135
    iget-short v1, p0, Lsoftware/simplicial/a/f/bb;->f:S

    if-ltz v1, :cond_0

    iget-short v1, p0, Lsoftware/simplicial/a/f/bb;->f:S

    sget-object v2, Lsoftware/simplicial/a/f;->lk:[Lsoftware/simplicial/a/f;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 136
    :cond_0
    sget-object v1, Lsoftware/simplicial/a/f;->a:Lsoftware/simplicial/a/f;

    invoke-virtual {v1}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lsoftware/simplicial/a/f/bb;->f:S

    .line 137
    :cond_1
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->l:I

    .line 138
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/bb;->m:B

    .line 139
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->d:Ljava/lang/String;

    .line 140
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->n:I

    .line 141
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/a/f/bb;->o:J

    .line 142
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->p:Ljava/lang/String;

    .line 143
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->s:I

    .line 144
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->t:I

    .line 145
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/bb;->u:Z

    .line 146
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/bb;->g:B

    .line 147
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->h:I

    .line 148
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->e:[B

    .line 149
    iget-object v1, p0, Lsoftware/simplicial/a/f/bb;->e:[B

    array-length v1, v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_2

    .line 150
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :catch_0
    move-exception v0

    .line 166
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/bb;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 167
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    .line 151
    :cond_2
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/a/f/bb;->e:[B

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 152
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->q:[B

    .line 153
    iget-object v1, p0, Lsoftware/simplicial/a/f/bb;->q:[B

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 154
    sget-object v1, Lsoftware/simplicial/a/s;->d:[Lsoftware/simplicial/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->v:Lsoftware/simplicial/a/s;

    .line 155
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/bb;->w:I

    .line 156
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/q;->a(B)Lsoftware/simplicial/a/q;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->r:Lsoftware/simplicial/a/q;

    .line 157
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/bb;->i:B

    .line 158
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v1

    iput-short v1, p0, Lsoftware/simplicial/a/f/bb;->j:S

    .line 159
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->k:Ljava/lang/String;

    .line 160
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/bb;->x:Lsoftware/simplicial/a/as;

    .line 162
    :cond_3
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/a/f/bb;->b:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 170
    const/4 v0, 0x1

    goto :goto_0
.end method
