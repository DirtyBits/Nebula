.class public Lsoftware/simplicial/a/f/cx;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public A:J

.field public B:Z

.field public C:Lsoftware/simplicial/a/az;

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public E:Z

.field public F:I

.field public G:Z

.field public H:J

.field public a:I

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[B

.field public f:Lsoftware/simplicial/a/q;

.field public g:Z

.field public h:Lsoftware/simplicial/a/aa;

.field public i:Z

.field public j:Z

.field public k:I

.field public l:J

.field public m:Z

.field public n:Lsoftware/simplicial/a/s;

.field public o:J

.field public p:Z

.field public q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation
.end field

.field public s:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Lsoftware/simplicial/a/bc;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 117
    sget-object v0, Lsoftware/simplicial/a/f/bh;->R:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/f/cx;->a:I

    .line 32
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->b:Z

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->c:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->d:Ljava/lang/String;

    .line 35
    new-array v0, v1, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->e:[B

    .line 36
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->f:Lsoftware/simplicial/a/q;

    .line 38
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->g:Z

    .line 39
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->h:Lsoftware/simplicial/a/aa;

    .line 40
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->i:Z

    .line 42
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->j:Z

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/f/cx;->k:I

    .line 44
    iput-wide v2, p0, Lsoftware/simplicial/a/f/cx;->l:J

    .line 46
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->m:Z

    .line 48
    iput-wide v2, p0, Lsoftware/simplicial/a/f/cx;->o:J

    .line 50
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->p:Z

    .line 51
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    .line 52
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    .line 53
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    .line 55
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    .line 59
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->x:Z

    .line 60
    iput-object v4, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    .line 62
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->z:Z

    .line 63
    iput-wide v2, p0, Lsoftware/simplicial/a/f/cx;->A:J

    .line 65
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->B:Z

    .line 66
    iput-object v4, p0, Lsoftware/simplicial/a/f/cx;->C:Lsoftware/simplicial/a/az;

    .line 68
    iput-object v4, p0, Lsoftware/simplicial/a/f/cx;->D:Ljava/util/List;

    .line 69
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->E:Z

    .line 71
    iput v1, p0, Lsoftware/simplicial/a/f/cx;->F:I

    .line 73
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cx;->G:Z

    .line 74
    iput-wide v2, p0, Lsoftware/simplicial/a/f/cx;->H:J

    .line 118
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)[B
    .locals 4

    .prologue
    .line 337
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->R:Lsoftware/simplicial/a/f/bh;

    iget v1, p0, Lsoftware/simplicial/a/f/cx;->ar:I

    invoke-static {p1, v0, v1}, Lsoftware/simplicial/a/f/cx;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 339
    iget v0, p0, Lsoftware/simplicial/a/f/cx;->a:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 341
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->b:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 342
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->b:Z

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->e:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 347
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->e:[B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 348
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 351
    :cond_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->g:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 352
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->g:Z

    if-eqz v0, :cond_1

    .line 354
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->h:Lsoftware/simplicial/a/aa;

    iget-byte v0, v0, Lsoftware/simplicial/a/aa;->r:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 355
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->i:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 358
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->j:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 359
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->j:Z

    if-eqz v0, :cond_2

    .line 361
    iget v0, p0, Lsoftware/simplicial/a/f/cx;->k:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 362
    iget-wide v0, p0, Lsoftware/simplicial/a/f/cx;->l:J

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v0

    .line 363
    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 366
    :cond_2
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->m:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 367
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->m:Z

    if-eqz v0, :cond_3

    .line 369
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->n:Lsoftware/simplicial/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 370
    iget-wide v0, p0, Lsoftware/simplicial/a/f/cx;->o:J

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ba;->a(J)I

    move-result v0

    .line 371
    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 374
    :cond_3
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->p:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 375
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->p:Z

    if-eqz v0, :cond_9

    .line 377
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 378
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/e;

    .line 379
    invoke-virtual {v0}, Lsoftware/simplicial/a/e;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v0

    .line 420
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 421
    const/4 v0, 0x0

    .line 424
    :goto_1
    return-object v0

    .line 380
    :cond_4
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 381
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/af;

    .line 382
    iget-byte v0, v0, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    goto :goto_2

    .line 383
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/as;

    .line 385
    iget-byte v0, v0, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    goto :goto_3

    .line 386
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 387
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 388
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    goto :goto_4

    .line 389
    :cond_7
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->v:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 390
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 391
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bd;

    .line 393
    iget-byte v2, v0, Lsoftware/simplicial/a/bd;->c:B

    invoke-virtual {p1, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 394
    iget-short v0, v0, Lsoftware/simplicial/a/bd;->e:S

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    goto :goto_5

    .line 396
    :cond_8
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->w:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 399
    :cond_9
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->x:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 400
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->x:Z

    if-eqz v0, :cond_a

    .line 402
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    iget-boolean v0, v0, Lsoftware/simplicial/a/bc;->a:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 403
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    iget-boolean v0, v0, Lsoftware/simplicial/a/bc;->b:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 406
    :cond_a
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->z:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 407
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->z:Z

    if-eqz v0, :cond_b

    .line 409
    iget-wide v0, p0, Lsoftware/simplicial/a/f/cx;->A:J

    invoke-virtual {p1, v0, v1}, Lsoftware/simplicial/a/f/bn;->writeLong(J)V

    .line 412
    :cond_b
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->B:Z

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 413
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->B:Z

    if-eqz v0, :cond_c

    .line 415
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->C:Lsoftware/simplicial/a/az;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/az;->a(Lsoftware/simplicial/a/f/bn;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 424
    :cond_c
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    goto/16 :goto_1
.end method

.method public b(Lsoftware/simplicial/a/f/bi;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 432
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/cx;->ar:I

    .line 433
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 435
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/cx;->a:I

    .line 437
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->b:Z

    .line 438
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->b:Z

    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->c:Ljava/lang/String;

    .line 441
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->d:Ljava/lang/String;

    .line 442
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->e:[B

    .line 443
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->e:[B

    invoke-virtual {v2, v0}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 444
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/q;->a(B)Lsoftware/simplicial/a/q;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->f:Lsoftware/simplicial/a/q;

    .line 447
    :cond_0
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->g:Z

    .line 448
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->g:Z

    if-eqz v0, :cond_1

    .line 450
    sget-object v0, Lsoftware/simplicial/a/aa;->q:Ljava/util/Map;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->h:Lsoftware/simplicial/a/aa;

    .line 451
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->i:Z

    .line 454
    :cond_1
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->j:Z

    .line 455
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->j:Z

    if-eqz v0, :cond_2

    .line 457
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/cx;->k:I

    .line 458
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    .line 459
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v4

    iput-wide v4, p0, Lsoftware/simplicial/a/f/cx;->l:J

    .line 462
    :cond_2
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->m:Z

    .line 463
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->m:Z

    if-eqz v0, :cond_3

    .line 465
    sget-object v0, Lsoftware/simplicial/a/s;->d:[Lsoftware/simplicial/a/s;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->n:Lsoftware/simplicial/a/s;

    .line 466
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    .line 467
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->b(I)J

    move-result-wide v4

    iput-wide v4, p0, Lsoftware/simplicial/a/f/cx;->o:J

    .line 470
    :cond_3
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->p:Z

    .line 471
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->p:Z

    if-eqz v0, :cond_9

    .line 473
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    .line 474
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    move v0, v1

    .line 475
    :goto_0
    if-ge v0, v3, :cond_4

    .line 476
    iget-object v4, p0, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    sget-object v5, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v6

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 475
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    :cond_4
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 478
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    move v0, v1

    .line 479
    :goto_1
    if-ge v0, v3, :cond_5

    .line 480
    iget-object v4, p0, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    invoke-static {v5}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 479
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 481
    :cond_5
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 482
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    move v0, v1

    .line 483
    :goto_2
    if-ge v0, v3, :cond_6

    .line 484
    iget-object v4, p0, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    invoke-static {v5}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 483
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 485
    :cond_6
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 486
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, v3}, Ljava/util/LinkedHashSet;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    move v0, v1

    .line 487
    :goto_3
    if-ge v0, v3, :cond_7

    .line 488
    iget-object v4, p0, Lsoftware/simplicial/a/f/cx;->u:Ljava/util/Set;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 487
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 489
    :cond_7
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->v:Z

    .line 490
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 491
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    move v0, v1

    .line 492
    :goto_4
    if-ge v0, v3, :cond_8

    .line 494
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/bd;->a(IS)Lsoftware/simplicial/a/bd;

    move-result-object v4

    .line 495
    iget-object v5, p0, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    iget-byte v6, v4, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 497
    :cond_8
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->w:Z

    .line 500
    :cond_9
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->x:Z

    .line 501
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->x:Z

    if-eqz v0, :cond_a

    .line 503
    new-instance v0, Lsoftware/simplicial/a/bc;

    invoke-direct {v0}, Lsoftware/simplicial/a/bc;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    .line 504
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, v0, Lsoftware/simplicial/a/bc;->a:Z

    .line 505
    iget-object v0, p0, Lsoftware/simplicial/a/f/cx;->y:Lsoftware/simplicial/a/bc;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, v0, Lsoftware/simplicial/a/bc;->b:Z

    .line 508
    :cond_a
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->z:Z

    .line 509
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->z:Z

    if-eqz v0, :cond_b

    .line 511
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lsoftware/simplicial/a/f/cx;->A:J

    .line 514
    :cond_b
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->B:Z

    .line 515
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cx;->B:Z

    if-eqz v0, :cond_c

    .line 517
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/f/bm;)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cx;->C:Lsoftware/simplicial/a/az;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 526
    :cond_c
    const/4 v0, 0x1

    :goto_5
    return v0

    .line 520
    :catch_0
    move-exception v0

    .line 522
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/cx;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 523
    goto :goto_5
.end method
