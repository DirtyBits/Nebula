.class public Lsoftware/simplicial/a/f/al;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# static fields
.field public static final a:Lsoftware/simplicial/a/f/bv;


# instance fields
.field public A:[B

.field public B:[B

.field public C:[F

.field public D:[F

.field public E:[F

.field public F:[B

.field public G:[B

.field public H:[B

.field public I:[B

.field public J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public N:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public O:[B

.field public P:[B

.field public Q:[B

.field public R:[F

.field public S:[F

.field public T:[B

.field public U:[F

.field public V:[F

.field public W:[F

.field public X:[F

.field public Y:[B

.field public Z:[B

.field public aa:[B

.field public ab:Z

.field public ac:F

.field public ad:B

.field public ae:B

.field public af:B

.field public ag:B

.field public ah:S

.field public ai:B

.field public aj:B

.field public ak:B

.field public al:B

.field public am:B

.field public an:B

.field public ao:B

.field public ap:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/f/bn;",
            ">;"
        }
    .end annotation
.end field

.field private as:I

.field private at:Lsoftware/simplicial/a/f/bn;

.field public b:[B

.field public c:[F

.field public d:[F

.field public e:[B

.field public f:[F

.field public g:[F

.field public h:[F

.field public i:[F

.field public j:[Lsoftware/simplicial/a/a/y;

.field public k:[B

.field public l:[B

.field public m:[F

.field public n:[F

.field public o:[F

.field public p:[F

.field public q:[B

.field public r:[F

.field public s:[F

.field public t:[S

.field public u:[F

.field public v:[F

.field public w:[B

.field public x:[B

.field public y:[F

.field public z:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lsoftware/simplicial/a/f/bv;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bv;-><init>()V

    sput-object v0, Lsoftware/simplicial/a/f/al;->a:Lsoftware/simplicial/a/f/bv;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    const/16 v2, 0x24

    const/4 v0, 0x0

    .line 166
    sget-object v1, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v1}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 127
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->J:Ljava/util/List;

    .line 128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->K:Ljava/util/List;

    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->L:Ljava/util/List;

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->M:Ljava/util/List;

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->N:Ljava/util/List;

    .line 145
    iput-boolean v0, p0, Lsoftware/simplicial/a/f/al;->ab:Z

    .line 147
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    .line 148
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    .line 149
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->af:B

    .line 150
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    .line 151
    iput-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    .line 152
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    .line 153
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    .line 154
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    .line 155
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    .line 156
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    .line 157
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    .line 158
    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    .line 159
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lsoftware/simplicial/a/f/al;->ap:Ljava/util/List;

    .line 160
    iput v0, p0, Lsoftware/simplicial/a/f/al;->as:I

    .line 168
    :goto_0
    if-ge v0, v3, :cond_0

    .line 170
    iget-object v1, p0, Lsoftware/simplicial/a/f/al;->ap:Ljava/util/List;

    new-instance v2, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bv;SF)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 797
    iget v0, p0, Lsoftware/simplicial/a/f/al;->as:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/a/f/al;->as:I

    .line 798
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->ap:Ljava/util/List;

    iget v1, p0, Lsoftware/simplicial/a/f/al;->as:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/bn;

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    .line 800
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ak:B

    .line 801
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ad:B

    .line 802
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->al:B

    .line 803
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->am:B

    .line 804
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->af:B

    .line 805
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->aj:B

    .line 806
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ag:B

    .line 807
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ai:B

    .line 808
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ae:B

    .line 809
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->an:B

    .line 810
    iput-byte v2, p0, Lsoftware/simplicial/a/f/al;->ao:B

    .line 811
    iput-short v2, p0, Lsoftware/simplicial/a/f/al;->ah:S

    .line 814
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    sget-object v1, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    invoke-static {v0, v1}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 816
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bv;->b()B

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 817
    const/4 v0, 0x2

    .line 818
    const/16 v1, 0x113

    if-lt p2, v1, :cond_2

    .line 820
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    const/4 v1, 0x0

    sget v3, Lsoftware/simplicial/a/ai;->T:F

    invoke-virtual {v0, p3, v1, v3}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 821
    const/4 v0, 0x5

    move v3, v0

    .line 823
    :goto_0
    const/16 v0, 0x14b

    if-lt p2, v0, :cond_0

    const/16 v0, 0x8

    :goto_1
    move v1, v2

    .line 824
    :goto_2
    if-ge v1, v0, :cond_1

    .line 825
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 824
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 823
    :cond_0
    const/4 v0, 0x7

    goto :goto_1

    .line 826
    :cond_1
    add-int/2addr v0, v3

    .line 828
    return v0

    :cond_2
    move v3, v0

    goto :goto_0
.end method

.method private a(I)[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 833
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v1

    .line 834
    const/16 v0, 0x113

    if-lt p1, v0, :cond_1

    const/4 v0, 0x5

    .line 835
    :goto_0
    add-int/lit8 v2, v0, 0x0

    aput-byte v5, v1, v2

    .line 836
    add-int/lit8 v2, v0, 0x0

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->ak:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0x1f

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 837
    add-int/lit8 v2, v0, 0x0

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->ad:B

    shl-int/lit8 v4, v4, 0x5

    and-int/lit16 v4, v4, 0xe0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 838
    add-int/lit8 v2, v0, 0x1

    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->am:B

    aput-byte v3, v1, v2

    .line 839
    add-int/lit8 v2, v0, 0x2

    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ag:B

    aput-byte v3, v1, v2

    .line 840
    add-int/lit8 v2, v0, 0x3

    aput-byte v5, v1, v2

    .line 841
    add-int/lit8 v2, v0, 0x3

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->af:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 842
    add-int/lit8 v2, v0, 0x3

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->aj:B

    shl-int/lit8 v4, v4, 0x4

    and-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 843
    add-int/lit8 v2, v0, 0x4

    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->al:B

    aput-byte v3, v1, v2

    .line 844
    add-int/lit8 v2, v0, 0x5

    aput-byte v5, v1, v2

    .line 845
    add-int/lit8 v2, v0, 0x5

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->ai:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 846
    add-int/lit8 v2, v0, 0x5

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->ae:B

    shl-int/lit8 v4, v4, 0x4

    and-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 847
    add-int/lit8 v2, v0, 0x6

    aput-byte v5, v1, v2

    .line 848
    add-int/lit8 v2, v0, 0x6

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->an:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0x1f

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 849
    add-int/lit8 v2, v0, 0x6

    aget-byte v3, v1, v2

    iget-byte v4, p0, Lsoftware/simplicial/a/f/al;->ao:B

    shl-int/lit8 v4, v4, 0x5

    and-int/lit16 v4, v4, 0xe0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 850
    const/16 v2, 0x14b

    if-lt p1, v2, :cond_0

    .line 851
    add-int/lit8 v0, v0, 0x7

    iget-short v2, p0, Lsoftware/simplicial/a/f/al;->ah:S

    add-int/lit8 v2, v2, -0x80

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 853
    :cond_0
    return-object v1

    .line 834
    :cond_1
    const/4 v0, 0x2

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bv;Ljava/util/List;Ljava/util/List;[Lsoftware/simplicial/a/bf;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;FLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/Collection;IS)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/f/bv;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ae;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g;",
            ">;[",
            "Lsoftware/simplicial/a/bf;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/a/y;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ag;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/z;",
            ">;F",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bt;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bl;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ca;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;IS)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 178
    new-instance v9, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v9, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 182
    const/4 v4, -0x1

    :try_start_0
    move-object/from16 v0, p0

    iput v4, v0, Lsoftware/simplicial/a/f/al;->as:I

    .line 183
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v7

    .line 185
    if-eqz p8, :cond_0

    .line 187
    const/4 v6, 0x0

    .line 188
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    move-result v5

    .line 189
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_0
    move v10, v7

    move v8, v4

    .line 192
    :goto_1
    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_66

    .line 194
    add-int/lit8 v4, v8, 0x7

    const/16 v11, 0x59c

    if-le v4, v11, :cond_7

    move v4, v5

    .line 212
    :goto_2
    if-eqz v4, :cond_9

    move v7, v8

    .line 227
    :cond_0
    :goto_3
    if-eqz p6, :cond_1

    .line 229
    const/4 v6, 0x0

    .line 230
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v5

    .line 231
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_4
    move v12, v7

    move v8, v4

    .line 234
    :goto_5
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v12, v4, :cond_63

    .line 236
    add-int/lit8 v4, v8, 0xc

    const/16 v10, 0x59c

    if-le v4, v10, :cond_a

    move v4, v5

    .line 281
    :goto_6
    if-eqz v4, :cond_11

    move v7, v8

    .line 295
    :cond_1
    :goto_7
    if-eqz p5, :cond_2

    .line 297
    const/4 v6, 0x0

    .line 298
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v5

    .line 299
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_8
    move v11, v7

    move v8, v4

    .line 302
    :goto_9
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v4

    if-ge v11, v4, :cond_5f

    .line 304
    add-int/lit8 v4, v8, 0x7

    const/16 v10, 0x59c

    if-le v4, v10, :cond_12

    move v4, v5

    .line 322
    :goto_a
    if-eqz v4, :cond_15

    move v7, v8

    .line 336
    :cond_2
    :goto_b
    if-eqz p2, :cond_3

    .line 338
    const/4 v6, 0x0

    .line 339
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v5

    .line 340
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_c
    move v10, v7

    move v8, v4

    .line 343
    :goto_d
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_5c

    .line 345
    add-int/lit8 v4, v8, 0x8

    const/16 v11, 0x59c

    if-le v4, v11, :cond_16

    move v4, v5

    .line 363
    :goto_e
    if-eqz v4, :cond_18

    move v7, v8

    .line 377
    :cond_3
    :goto_f
    if-eqz p11, :cond_4

    .line 379
    const/4 v6, 0x0

    .line 380
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->size()I

    move-result v5

    .line 381
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_10
    move v10, v7

    move v8, v4

    .line 384
    :goto_11
    invoke-interface/range {p11 .. p11}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_59

    .line 386
    add-int/lit8 v4, v8, 0x8

    const/16 v11, 0x59c

    if-le v4, v11, :cond_19

    move v4, v5

    .line 405
    :goto_12
    if-eqz v4, :cond_1b

    move v7, v8

    .line 420
    :cond_4
    :goto_13
    if-eqz p3, :cond_5

    .line 422
    const/4 v6, 0x0

    .line 423
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v5

    .line 424
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_14
    move v10, v7

    move v8, v4

    .line 427
    :goto_15
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_56

    .line 429
    const/16 v4, 0xf6

    move/from16 v0, p16

    if-gt v0, v4, :cond_1c

    const/16 v4, 0xc

    :goto_16
    add-int/2addr v4, v8

    const/16 v11, 0x59c

    if-le v4, v11, :cond_1d

    move v4, v5

    .line 463
    :goto_17
    if-eqz v4, :cond_20

    move v7, v8

    .line 477
    :cond_5
    :goto_18
    if-eqz p9, :cond_6

    .line 479
    const/4 v6, 0x0

    .line 480
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v5

    .line 481
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_19
    move v11, v7

    move v8, v4

    .line 484
    :goto_1a
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v4

    if-ge v11, v4, :cond_53

    .line 486
    add-int/lit8 v4, v8, 0xb

    const/16 v10, 0x59c

    if-le v4, v10, :cond_21

    move v4, v5

    .line 508
    :goto_1b
    if-eqz v4, :cond_24

    move v7, v8

    .line 523
    :cond_6
    :goto_1c
    if-eqz p4, :cond_26

    .line 525
    const/4 v6, 0x0

    .line 526
    move-object/from16 v0, p4

    array-length v5, v0

    .line 527
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v5

    move/from16 v5, v16

    :goto_1d
    move v11, v6

    move v8, v7

    move v7, v4

    .line 530
    :goto_1e
    move-object/from16 v0, p4

    array-length v4, v0

    if-ge v11, v4, :cond_50

    .line 532
    aget-object v10, p4, v11

    .line 533
    iget-object v4, v10, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_25

    iget-boolean v4, v10, Lsoftware/simplicial/a/bf;->aj:Z

    if-nez v4, :cond_25

    .line 535
    add-int/lit8 v4, v7, -0x1

    .line 536
    add-int/lit8 v6, v6, 0x1

    move v7, v8

    .line 530
    :goto_1f
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    move v8, v7

    move v7, v4

    goto :goto_1e

    .line 197
    :cond_7
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ad:B

    const/4 v11, 0x7

    if-lt v4, v11, :cond_8

    .line 199
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM FLs %d/7 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p8 .. p8}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 200
    const/4 v5, 0x1

    move v4, v5

    .line 201
    goto/16 :goto_2

    .line 204
    :cond_8
    move-object/from16 v0, p8

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/ag;

    .line 205
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v12, v4, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget v12, v12, Lsoftware/simplicial/a/bx;->c:I

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 206
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ag;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 207
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/ag;->m:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 209
    add-int/lit8 v8, v8, 0x7

    .line 210
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ad:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ad:B

    .line 192
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_1

    .line 215
    :cond_9
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ad:B

    add-int/2addr v7, v5

    .line 216
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ad:B

    sub-int v5, v6, v5

    .line 218
    if-lez v5, :cond_65

    .line 220
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 224
    :goto_20
    if-gtz v5, :cond_64

    move v7, v6

    goto/16 :goto_3

    .line 239
    :cond_a
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->am:B

    const/16 v10, 0x7f

    if-lt v4, v10, :cond_b

    .line 241
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM FSBs %d/127 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 242
    const/4 v5, 0x1

    move v4, v5

    .line 243
    goto/16 :goto_6

    .line 246
    :cond_b
    move-object/from16 v0, p6

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bq;

    .line 247
    const/16 v10, 0x14b

    move/from16 v0, p16

    if-lt v0, v10, :cond_c

    iget v10, v4, Lsoftware/simplicial/a/bq;->c:I

    move v11, v10

    .line 248
    :goto_21
    const/16 v10, 0xf6

    move/from16 v0, p16

    if-gt v0, v10, :cond_e

    .line 250
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v10, v11}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 251
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->C:I

    invoke-virtual {v10, v11}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 252
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v11, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 253
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->m:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v11, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 254
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v11, v4, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v13, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-ne v11, v13, :cond_d

    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    :goto_22
    invoke-virtual {v10, v4}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V

    .line 255
    add-int/lit8 v4, v8, 0xc

    .line 279
    :goto_23
    move-object/from16 v0, p0

    iget-byte v8, v0, Lsoftware/simplicial/a/f/al;->am:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    move-object/from16 v0, p0

    iput-byte v8, v0, Lsoftware/simplicial/a/f/al;->am:B

    .line 234
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    move v8, v4

    goto/16 :goto_5

    .line 247
    :cond_c
    iget v10, v4, Lsoftware/simplicial/a/bq;->c:I

    rem-int/lit8 v10, v10, 0x2e

    move v11, v10

    goto :goto_21

    .line 254
    :cond_d
    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->c()F

    move-result v4

    goto :goto_22

    .line 259
    :cond_e
    const/4 v13, 0x0

    .line 260
    iget-object v10, v4, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v14, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-ne v10, v14, :cond_10

    const/4 v10, 0x1

    :goto_24
    shl-int/lit8 v10, v10, 0x0

    and-int/lit8 v10, v10, 0x1

    int-to-byte v10, v10

    or-int/2addr v10, v13

    int-to-byte v10, v10

    .line 261
    shl-int/lit8 v11, v11, 0x1

    and-int/lit16 v11, v11, 0xfe

    int-to-byte v11, v11

    or-int/2addr v10, v11

    int-to-byte v10, v10

    .line 262
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v11, v10}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 263
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->C:I

    invoke-virtual {v10, v11}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 264
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v11, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 265
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->m:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v11, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 266
    add-int/lit8 v8, v8, 0x8

    .line 267
    const/16 v10, 0x139

    move/from16 v0, p16

    if-lt v0, v10, :cond_f

    .line 269
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v11, v4, Lsoftware/simplicial/a/bq;->j:F

    const/4 v13, 0x0

    const v14, 0x40c90fdb

    invoke-virtual {v10, v11, v13, v14}, Lsoftware/simplicial/a/f/bn;->a(FFF)V

    .line 270
    add-int/lit8 v8, v8, 0x1

    .line 272
    :cond_f
    iget-object v10, v4, Lsoftware/simplicial/a/bq;->z:Lsoftware/simplicial/a/bq$a;

    sget-object v11, Lsoftware/simplicial/a/bq$a;->a:Lsoftware/simplicial/a/bq$a;

    if-eq v10, v11, :cond_62

    .line 274
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4}, Lsoftware/simplicial/a/bq;->c()F

    move-result v4

    const/4 v11, 0x0

    const v13, 0x48f42400    # 500000.0f

    invoke-virtual {v10, v4, v11, v13}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 275
    add-int/lit8 v4, v8, 0x3

    goto/16 :goto_23

    .line 260
    :cond_10
    const/4 v10, 0x0

    goto :goto_24

    .line 283
    :cond_11
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->am:B

    add-int/2addr v7, v5

    .line 284
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->am:B

    sub-int v5, v6, v5

    .line 286
    if-lez v5, :cond_61

    .line 288
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 292
    :goto_25
    if-gtz v5, :cond_60

    move v7, v6

    goto/16 :goto_7

    .line 307
    :cond_12
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->al:B

    const/16 v10, 0x7f

    if-lt v4, v10, :cond_13

    .line 309
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM SBs %d/127 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 310
    const/4 v5, 0x1

    move v4, v5

    .line 311
    goto/16 :goto_a

    .line 314
    :cond_13
    move-object/from16 v0, p5

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bq;

    .line 315
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    const/16 v10, 0x14b

    move/from16 v0, p16

    if-lt v0, v10, :cond_14

    iget v10, v4, Lsoftware/simplicial/a/bq;->c:I

    :goto_26
    invoke-virtual {v12, v10}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 316
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/bq;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 317
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/bq;->m:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 319
    add-int/lit8 v8, v8, 0x7

    .line 320
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->al:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->al:B

    .line 302
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto/16 :goto_9

    .line 315
    :cond_14
    iget v10, v4, Lsoftware/simplicial/a/bq;->c:I

    rem-int/lit8 v10, v10, 0x2e

    goto :goto_26

    .line 324
    :cond_15
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->al:B

    add-int/2addr v7, v5

    .line 325
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->al:B

    sub-int v5, v6, v5

    .line 327
    if-lez v5, :cond_5e

    .line 329
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 333
    :goto_27
    if-gtz v5, :cond_5d

    move v7, v6

    goto/16 :goto_b

    .line 348
    :cond_16
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ag:B

    const/16 v11, 0x7f

    if-lt v4, v11, :cond_17

    .line 350
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM Dots %d/127 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 351
    const/4 v5, 0x1

    move v4, v5

    .line 352
    goto/16 :goto_e

    .line 355
    :cond_17
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/ae;

    .line 356
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-short v12, v4, Lsoftware/simplicial/a/ae;->b:S

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 357
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ae;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 358
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/ae;->m:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 360
    add-int/lit8 v8, v8, 0x8

    .line 361
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ag:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ag:B

    .line 343
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_d

    .line 365
    :cond_18
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ag:B

    add-int/2addr v7, v5

    .line 366
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ag:B

    sub-int v5, v6, v5

    .line 368
    if-lez v5, :cond_5b

    .line 370
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 374
    :goto_28
    if-gtz v5, :cond_5a

    move v7, v6

    goto/16 :goto_f

    .line 389
    :cond_19
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ai:B

    const/16 v11, 0xf

    if-lt v4, v11, :cond_1a

    .line 391
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM SOs %d/15 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p11 .. p11}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 392
    const/4 v5, 0x1

    move v4, v5

    .line 393
    goto/16 :goto_12

    .line 396
    :cond_1a
    move-object/from16 v0, p11

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bt;

    .line 397
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-byte v12, v4, Lsoftware/simplicial/a/bt;->a:B

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 398
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v12, v4, Lsoftware/simplicial/a/bt;->b:Lsoftware/simplicial/a/bv;

    invoke-virtual {v12}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v12

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 399
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/bt;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 400
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/bt;->m:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 402
    add-int/lit8 v8, v8, 0x8

    .line 403
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ai:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ai:B

    .line 384
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_11

    .line 408
    :cond_1b
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ai:B

    add-int/2addr v7, v5

    .line 409
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ai:B

    sub-int v5, v6, v5

    .line 411
    if-lez v5, :cond_58

    .line 413
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 414
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 417
    :goto_29
    if-gtz v5, :cond_57

    move v7, v6

    goto/16 :goto_13

    .line 429
    :cond_1c
    const/16 v4, 0x9

    goto/16 :goto_16

    .line 432
    :cond_1d
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->aj:B

    const/16 v11, 0xf

    if-lt v4, v11, :cond_1e

    .line 434
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM BHs %d/15 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 435
    const/4 v5, 0x1

    move v4, v5

    .line 436
    goto/16 :goto_17

    .line 439
    :cond_1e
    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/g;

    .line 440
    const/16 v11, 0xf6

    move/from16 v0, p16

    if-gt v0, v11, :cond_1f

    .line 442
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/g;->b:I

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 443
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v12, v4, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    invoke-virtual {v12}, Lsoftware/simplicial/a/g$a;->ordinal()I

    move-result v12

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 444
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/g;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 445
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/g;->m:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 446
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/g;->n:F

    invoke-virtual {v11, v4}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V

    .line 447
    add-int/lit8 v4, v8, 0xc

    .line 461
    :goto_2a
    move-object/from16 v0, p0

    iget-byte v8, v0, Lsoftware/simplicial/a/f/al;->aj:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    move-object/from16 v0, p0

    iput-byte v8, v0, Lsoftware/simplicial/a/f/al;->aj:B

    .line 427
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move v8, v4

    goto/16 :goto_15

    .line 451
    :cond_1f
    const/4 v11, 0x0

    .line 452
    iget-object v12, v4, Lsoftware/simplicial/a/g;->a:Lsoftware/simplicial/a/g$a;

    invoke-virtual {v12}, Lsoftware/simplicial/a/g$a;->ordinal()I

    move-result v12

    shl-int/lit8 v12, v12, 0x0

    and-int/lit8 v12, v12, 0x3

    int-to-byte v12, v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    .line 453
    iget v12, v4, Lsoftware/simplicial/a/g;->b:I

    shl-int/lit8 v12, v12, 0x2

    and-int/lit16 v12, v12, 0xfc

    int-to-byte v12, v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    .line 454
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v12, v11}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 455
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/g;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 456
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/g;->m:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 457
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/g;->n:F

    const/4 v12, 0x0

    const v13, 0x427a6666    # 62.6f

    invoke-virtual {v11, v4, v12, v13}, Lsoftware/simplicial/a/f/bn;->b(FFF)V

    .line 458
    add-int/lit8 v4, v8, 0x9

    goto :goto_2a

    .line 465
    :cond_20
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->aj:B

    add-int/2addr v7, v5

    .line 466
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->aj:B

    sub-int v5, v6, v5

    .line 468
    if-lez v5, :cond_55

    .line 470
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 471
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 474
    :goto_2b
    if-gtz v5, :cond_54

    move v7, v6

    goto/16 :goto_18

    .line 489
    :cond_21
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ae:B

    const/16 v10, 0xf

    if-lt v4, v10, :cond_22

    .line 491
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM CPs %d/15 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 492
    const/4 v5, 0x1

    move v4, v5

    .line 493
    goto/16 :goto_1b

    .line 496
    :cond_22
    move-object/from16 v0, p9

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/z;

    .line 497
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v10, v4, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    if-nez v10, :cond_23

    const/4 v10, -0x1

    :goto_2c
    invoke-virtual {v12, v10}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 498
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/z;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 499
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/z;->m:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 500
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/z;->n:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 501
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/z;->b:F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v10, v4, v12, v13}, Lsoftware/simplicial/a/f/bn;->a(FFF)V

    .line 503
    add-int/lit8 v8, v8, 0xb

    .line 504
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ae:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ae:B

    .line 484
    add-int/lit8 v4, v11, 0x1

    move v11, v4

    goto/16 :goto_1a

    .line 497
    :cond_23
    iget-object v10, v4, Lsoftware/simplicial/a/z;->a:Lsoftware/simplicial/a/bx;

    iget v10, v10, Lsoftware/simplicial/a/bx;->c:I

    goto :goto_2c

    .line 511
    :cond_24
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ae:B

    add-int/2addr v7, v5

    .line 512
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ae:B

    sub-int v5, v6, v5

    .line 514
    if-lez v5, :cond_52

    .line 516
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 520
    :goto_2d
    if-gtz v5, :cond_51

    move v7, v6

    goto/16 :goto_1c

    .line 540
    :cond_25
    const/16 v4, 0xf6

    move/from16 v0, p16

    if-gt v0, v4, :cond_2a

    const/16 v4, 0xd

    .line 541
    :goto_2e
    iget-object v12, v10, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    mul-int/2addr v4, v12

    add-int/lit8 v12, v4, 0x3

    .line 542
    add-int v4, v8, v12

    const/16 v13, 0x59c

    if-le v4, v13, :cond_2b

    move v4, v5

    .line 587
    :goto_2f
    if-eqz v4, :cond_34

    move v7, v8

    .line 602
    :cond_26
    if-eqz p12, :cond_27

    .line 604
    const/4 v6, 0x0

    .line 605
    invoke-interface/range {p12 .. p12}, Ljava/util/List;->size()I

    move-result v5

    .line 606
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_30
    move v10, v7

    move v8, v4

    .line 609
    :goto_31
    invoke-interface/range {p12 .. p12}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_4e

    .line 611
    add-int/lit8 v4, v8, 0x8

    const/16 v11, 0x59c

    if-le v4, v11, :cond_35

    move v4, v5

    .line 635
    :goto_32
    if-eqz v4, :cond_37

    move v7, v8

    .line 650
    :cond_27
    :goto_33
    if-eqz p13, :cond_28

    .line 652
    const/4 v6, 0x0

    .line 653
    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v5

    .line 654
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v7

    move v7, v6

    move v6, v5

    move/from16 v5, v16

    :goto_34
    move v10, v7

    move v8, v4

    .line 657
    :goto_35
    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_4b

    .line 659
    add-int/lit8 v4, v8, 0xd

    const/16 v11, 0x59c

    if-le v4, v11, :cond_38

    move v4, v5

    .line 681
    :goto_36
    if-eqz v4, :cond_3a

    move v7, v8

    .line 696
    :cond_28
    :goto_37
    if-eqz p14, :cond_29

    .line 698
    invoke-interface/range {p14 .. p14}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 699
    const/4 v4, 0x0

    move v5, v4

    .line 700
    :goto_38
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_29

    .line 702
    :goto_39
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_48

    .line 704
    add-int/lit8 v4, v7, 0x3

    const/16 v8, 0x59c

    if-le v4, v8, :cond_3b

    move v4, v5

    .line 722
    :goto_3a
    if-eqz v4, :cond_3d

    .line 733
    :cond_29
    if-eqz p7, :cond_3f

    .line 735
    const/4 v6, 0x0

    .line 736
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v5

    .line 737
    const/4 v4, 0x0

    move/from16 v16, v4

    move v4, v5

    move/from16 v5, v16

    :goto_3b
    move v10, v6

    move v8, v7

    move v7, v4

    .line 740
    :goto_3c
    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v4

    if-ge v10, v4, :cond_46

    .line 742
    move-object/from16 v0, p7

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/a/y;

    .line 743
    iget-object v11, v4, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v12, Lsoftware/simplicial/a/a/y$a;->K:Lsoftware/simplicial/a/a/y$a;

    if-ne v11, v12, :cond_3e

    const/16 v11, 0x151

    move/from16 v0, p16

    if-ge v0, v11, :cond_3e

    .line 745
    add-int/lit8 v6, v6, 0x1

    .line 746
    add-int/lit8 v4, v7, -0x1

    move v7, v8

    .line 740
    :goto_3d
    add-int/lit8 v8, v10, 0x1

    move v10, v8

    move v8, v7

    move v7, v4

    goto :goto_3c

    .line 540
    :cond_2a
    const/16 v4, 0xc

    goto/16 :goto_2e

    .line 545
    :cond_2b
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ak:B

    const/16 v13, 0x1f

    if-lt v4, v13, :cond_2c

    .line 547
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM PLs ?/31 GID %d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 548
    const/4 v5, 0x1

    move v4, v5

    .line 549
    goto/16 :goto_2f

    .line 552
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v13, v10, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual {v4, v13}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 553
    const/4 v4, 0x0

    .line 554
    iget v13, v10, Lsoftware/simplicial/a/bf;->T:F

    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v14

    double-to-int v13, v14

    shl-int/lit8 v13, v13, 0x0

    and-int/lit8 v13, v13, 0x3f

    int-to-byte v13, v13

    or-int/2addr v4, v13

    int-to-byte v4, v4

    .line 555
    iget-byte v13, v10, Lsoftware/simplicial/a/bf;->am:B

    shl-int/lit8 v13, v13, 0x6

    and-int/lit16 v13, v13, 0xc0

    int-to-byte v13, v13

    or-int/2addr v4, v13

    int-to-byte v4, v4

    .line 556
    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v13, v4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 557
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-object v13, v10, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v4, v13}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 558
    iget-object v4, v10, Lsoftware/simplicial/a/bf;->L:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2d
    :goto_3e
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_33

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bh;

    .line 560
    const/16 v10, 0xf6

    move/from16 v0, p16

    if-gt v0, v10, :cond_2f

    .line 562
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v14, v4, Lsoftware/simplicial/a/bh;->c:I

    invoke-virtual {v10, v14}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 563
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-short v14, v4, Lsoftware/simplicial/a/bh;->z:S

    invoke-virtual {v10, v14}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 564
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v14, v4, Lsoftware/simplicial/a/bh;->l:F

    const/4 v15, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v14, v15, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 565
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v14, v4, Lsoftware/simplicial/a/bh;->m:F

    const/4 v15, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v14, v15, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 566
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-boolean v14, v4, Lsoftware/simplicial/a/bh;->M:Z

    if-eqz v14, :cond_2e

    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    :goto_3f
    invoke-virtual {v10, v4}, Lsoftware/simplicial/a/f/bn;->writeFloat(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3e

    .line 786
    :catch_0
    move-exception v4

    .line 788
    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception occurred writing data. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 789
    const/4 v4, 0x0

    .line 792
    :goto_40
    return-object v4

    .line 566
    :cond_2e
    :try_start_1
    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    goto :goto_3f

    .line 570
    :cond_2f
    const/4 v14, 0x0

    .line 571
    iget-short v10, v4, Lsoftware/simplicial/a/bh;->z:S

    if-eqz v10, :cond_31

    const/4 v10, 0x1

    :goto_41
    shl-int/lit8 v10, v10, 0x0

    and-int/lit8 v10, v10, 0x1

    int-to-byte v10, v10

    or-int/2addr v10, v14

    int-to-byte v14, v10

    .line 572
    iget-boolean v10, v4, Lsoftware/simplicial/a/bh;->M:Z

    if-eqz v10, :cond_32

    const/4 v10, 0x1

    :goto_42
    shl-int/lit8 v10, v10, 0x1

    and-int/lit8 v10, v10, 0x2

    int-to-byte v10, v10

    or-int/2addr v10, v14

    int-to-byte v10, v10

    .line 573
    iget v14, v4, Lsoftware/simplicial/a/bh;->c:I

    shl-int/lit8 v14, v14, 0x2

    and-int/lit16 v14, v14, 0xfc

    int-to-byte v14, v14

    or-int/2addr v10, v14

    int-to-byte v10, v10

    .line 574
    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v14, v10}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 575
    iget-short v10, v4, Lsoftware/simplicial/a/bh;->z:S

    if-eqz v10, :cond_30

    .line 576
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget-short v14, v4, Lsoftware/simplicial/a/bh;->z:S

    invoke-virtual {v10, v14}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 577
    :cond_30
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v14, v4, Lsoftware/simplicial/a/bh;->l:F

    const/4 v15, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v14, v15, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 578
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v14, v4, Lsoftware/simplicial/a/bh;->m:F

    const/4 v15, 0x0

    move/from16 v0, p10

    invoke-virtual {v10, v14, v15, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 579
    iget-boolean v10, v4, Lsoftware/simplicial/a/bh;->M:Z

    if-nez v10, :cond_2d

    .line 580
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4}, Lsoftware/simplicial/a/bh;->c()F

    move-result v4

    const/4 v14, 0x0

    const v15, 0x48f42400    # 500000.0f

    invoke-virtual {v10, v4, v14, v15}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    goto/16 :goto_3e

    .line 571
    :cond_31
    const/4 v10, 0x0

    goto :goto_41

    .line 572
    :cond_32
    const/4 v10, 0x0

    goto :goto_42

    .line 584
    :cond_33
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ak:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ak:B

    .line 585
    add-int v4, v8, v12

    move/from16 v16, v7

    move v7, v4

    move/from16 v4, v16

    goto/16 :goto_1f

    .line 590
    :cond_34
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ak:B

    add-int/2addr v6, v5

    .line 591
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ak:B

    sub-int v5, v7, v5

    .line 593
    if-lez v5, :cond_4f

    .line 595
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v7

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 596
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v7

    .line 599
    :goto_43
    if-lez v5, :cond_26

    move/from16 v16, v4

    move v4, v5

    move/from16 v5, v16

    goto/16 :goto_1d

    .line 614
    :cond_35
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->an:B

    const/16 v11, 0x1f

    if-lt v4, v11, :cond_36

    .line 616
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM PUs %d/31 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p12 .. p12}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 617
    const/4 v5, 0x1

    move v4, v5

    .line 618
    goto/16 :goto_32

    .line 621
    :cond_36
    move-object/from16 v0, p12

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bl;

    .line 622
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/bl;->z:I

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 623
    const/4 v11, 0x0

    .line 624
    iget-object v12, v4, Lsoftware/simplicial/a/bl;->B:Lsoftware/simplicial/a/bl$a;

    invoke-virtual {v12}, Lsoftware/simplicial/a/bl$a;->ordinal()I

    move-result v12

    shl-int/lit8 v12, v12, 0x0

    and-int/lit8 v12, v12, 0x3

    int-to-byte v12, v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    .line 625
    iget-object v12, v4, Lsoftware/simplicial/a/bl;->A:Lsoftware/simplicial/a/bl$b;

    invoke-virtual {v12}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v12

    shl-int/lit8 v12, v12, 0x2

    and-int/lit16 v12, v12, 0xfc

    int-to-byte v12, v12

    or-int/2addr v11, v12

    int-to-byte v11, v11

    .line 626
    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v12, v11}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 627
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/bl;->l:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 628
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/bl;->m:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 630
    add-int/lit8 v8, v8, 0x8

    .line 631
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->an:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->an:B

    .line 609
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_31

    .line 638
    :cond_37
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->an:B

    add-int/2addr v7, v5

    .line 639
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->an:B

    sub-int v5, v6, v5

    .line 641
    if-lez v5, :cond_4d

    .line 643
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 644
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 647
    :goto_44
    if-gtz v5, :cond_4c

    move v7, v6

    goto/16 :goto_33

    .line 662
    :cond_38
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ao:B

    const/4 v11, 0x7

    if-lt v4, v11, :cond_39

    .line 664
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM Ws %d/7 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 665
    const/4 v5, 0x1

    move v4, v5

    .line 666
    goto/16 :goto_36

    .line 669
    :cond_39
    move-object/from16 v0, p13

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/ca;

    .line 670
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ca;->a:I

    invoke-virtual {v11, v12}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 671
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ca;->b:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 672
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ca;->c:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 673
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v12, v4, Lsoftware/simplicial/a/ca;->d:F

    const/4 v13, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v12, v13, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 674
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    iget v4, v4, Lsoftware/simplicial/a/ca;->e:F

    const/4 v12, 0x0

    move/from16 v0, p10

    invoke-virtual {v11, v4, v12, v0}, Lsoftware/simplicial/a/f/bn;->c(FFF)V

    .line 676
    add-int/lit8 v8, v8, 0xd

    .line 677
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ao:B

    add-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    move-object/from16 v0, p0

    iput-byte v4, v0, Lsoftware/simplicial/a/f/al;->ao:B

    .line 657
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_35

    .line 684
    :cond_3a
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ao:B

    add-int/2addr v7, v5

    .line 685
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->ao:B

    sub-int v5, v6, v5

    .line 687
    if-lez v5, :cond_4a

    .line 689
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 690
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v6

    .line 693
    :goto_45
    if-gtz v5, :cond_49

    move v7, v6

    goto/16 :goto_37

    .line 707
    :cond_3b
    move-object/from16 v0, p0

    iget-short v4, v0, Lsoftware/simplicial/a/f/al;->ah:S

    const/16 v8, 0xff

    if-lt v4, v8, :cond_3c

    .line 709
    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "GUM Ts %d/255 GID %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-interface/range {p14 .. p14}, Ljava/util/Collection;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    const/4 v10, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 710
    const/4 v4, 0x1

    .line 711
    goto/16 :goto_3a

    .line 714
    :cond_3c
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/bz;

    .line 715
    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4, v8}, Lsoftware/simplicial/a/bz;->a(Lsoftware/simplicial/a/f/bn;)V

    .line 717
    add-int/lit8 v7, v7, 0x3

    .line 718
    move-object/from16 v0, p0

    iget-short v4, v0, Lsoftware/simplicial/a/f/al;->ah:S

    add-int/lit8 v4, v4, 0x1

    int-to-short v4, v4

    move-object/from16 v0, p0

    iput-short v4, v0, Lsoftware/simplicial/a/f/al;->ah:S

    goto/16 :goto_39

    .line 725
    :cond_3d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_47

    .line 727
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 728
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I

    move-result v7

    move v5, v4

    goto/16 :goto_38

    .line 749
    :cond_3e
    iget v11, v4, Lsoftware/simplicial/a/a/y;->f:I

    add-int/2addr v11, v8

    const/16 v12, 0x59c

    if-le v11, v12, :cond_42

    move v4, v5

    .line 765
    :goto_46
    if-eqz v4, :cond_44

    .line 780
    :cond_3f
    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ak:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ag:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->af:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->aj:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->al:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ad:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->am:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ai:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ae:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->an:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-byte v4, v0, Lsoftware/simplicial/a/f/al;->ao:B

    if-gtz v4, :cond_40

    move-object/from16 v0, p0

    iget-short v4, v0, Lsoftware/simplicial/a/f/al;->ah:S

    if-lez v4, :cond_41

    .line 783
    :cond_40
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v4

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_41
    move-object v4, v9

    .line 792
    goto/16 :goto_40

    .line 752
    :cond_42
    move-object/from16 v0, p0

    iget-byte v11, v0, Lsoftware/simplicial/a/f/al;->af:B

    const/16 v12, 0xf

    if-lt v11, v12, :cond_43

    .line 754
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "GUM Es %d/15 GID %d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static/range {p15 .. p15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v5, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 755
    const/4 v5, 0x1

    move v4, v5

    .line 756
    goto/16 :goto_46

    .line 760
    :cond_43
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/a/f/al;->at:Lsoftware/simplicial/a/f/bn;

    invoke-virtual {v4, v11}, Lsoftware/simplicial/a/a/y;->a(Lsoftware/simplicial/a/f/bn;)V

    .line 762
    iget v4, v4, Lsoftware/simplicial/a/a/y;->f:I

    add-int/2addr v4, v8

    .line 763
    move-object/from16 v0, p0

    iget-byte v8, v0, Lsoftware/simplicial/a/f/al;->af:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    move-object/from16 v0, p0

    iput-byte v8, v0, Lsoftware/simplicial/a/f/al;->af:B

    move/from16 v16, v7

    move v7, v4

    move/from16 v4, v16

    goto/16 :goto_3d

    .line 768
    :cond_44
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->af:B

    add-int/2addr v6, v5

    .line 769
    move-object/from16 v0, p0

    iget-byte v5, v0, Lsoftware/simplicial/a/f/al;->af:B

    sub-int v5, v7, v5

    .line 771
    if-lez v5, :cond_45

    .line 773
    move-object/from16 v0, p0

    move/from16 v1, p16

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/al;->a(I)[B

    move-result-object v7

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 774
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p16

    move/from16 v3, p10

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/al;->a(Lsoftware/simplicial/a/f/bv;SF)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v7

    .line 777
    :goto_47
    if-lez v5, :cond_3f

    move/from16 v16, v4

    move v4, v5

    move/from16 v5, v16

    goto/16 :goto_3b

    :cond_45
    move v7, v8

    goto :goto_47

    :cond_46
    move v4, v5

    goto/16 :goto_46

    :cond_47
    move v5, v4

    goto/16 :goto_38

    :cond_48
    move v4, v5

    goto/16 :goto_3a

    :cond_49
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_34

    :cond_4a
    move v6, v8

    goto/16 :goto_45

    :cond_4b
    move v4, v5

    goto/16 :goto_36

    :cond_4c
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_30

    :cond_4d
    move v6, v8

    goto/16 :goto_44

    :cond_4e
    move v4, v5

    goto/16 :goto_32

    :cond_4f
    move v7, v8

    goto/16 :goto_43

    :cond_50
    move v4, v5

    goto/16 :goto_2f

    :cond_51
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_19

    :cond_52
    move v6, v8

    goto/16 :goto_2d

    :cond_53
    move v4, v5

    goto/16 :goto_1b

    :cond_54
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_14

    :cond_55
    move v6, v8

    goto/16 :goto_2b

    :cond_56
    move v4, v5

    goto/16 :goto_17

    :cond_57
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_10

    :cond_58
    move v6, v8

    goto/16 :goto_29

    :cond_59
    move v4, v5

    goto/16 :goto_12

    :cond_5a
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_c

    :cond_5b
    move v6, v8

    goto/16 :goto_28

    :cond_5c
    move v4, v5

    goto/16 :goto_e

    :cond_5d
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_8

    :cond_5e
    move v6, v8

    goto/16 :goto_27

    :cond_5f
    move v4, v5

    goto/16 :goto_a

    :cond_60
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_4

    :cond_61
    move v6, v8

    goto/16 :goto_25

    :cond_62
    move v4, v8

    goto/16 :goto_23

    :cond_63
    move v4, v5

    goto/16 :goto_6

    :cond_64
    move/from16 v16, v4

    move v4, v6

    move v6, v5

    move/from16 v5, v16

    goto/16 :goto_0

    :cond_65
    move v6, v8

    goto/16 :goto_20

    :cond_66
    move v4, v5

    goto/16 :goto_2
.end method

.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 860
    :try_start_0
    new-instance v6, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v6, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 861
    sget-object v0, Lsoftware/simplicial/a/f/al;->a:Lsoftware/simplicial/a/f/bv;

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-virtual {v0, v3}, Lsoftware/simplicial/a/f/bv;->a(B)V

    .line 862
    sget-object v0, Lsoftware/simplicial/a/f/al;->a:Lsoftware/simplicial/a/f/bv;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bv;->c()Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/a/f/al;->ab:Z

    .line 863
    const/4 v0, 0x0

    sget v3, Lsoftware/simplicial/a/ai;->T:F

    invoke-virtual {v6, v0, v3}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/al;->ac:F

    .line 866
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 867
    and-int/lit8 v3, v0, 0x1f

    ushr-int/lit8 v3, v3, 0x0

    int-to-byte v3, v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/al;->ak:B

    .line 868
    and-int/lit16 v0, v0, 0xe0

    ushr-int/lit8 v0, v0, 0x5

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    .line 869
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    .line 870
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    .line 871
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 872
    and-int/lit8 v3, v0, 0xf

    ushr-int/lit8 v3, v3, 0x0

    int-to-byte v3, v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/al;->af:B

    .line 873
    and-int/lit16 v0, v0, 0xf0

    ushr-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    .line 874
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    .line 875
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 876
    and-int/lit8 v3, v0, 0xf

    ushr-int/lit8 v3, v3, 0x0

    int-to-byte v3, v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/al;->ai:B

    .line 877
    and-int/lit16 v0, v0, 0xf0

    ushr-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    .line 878
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 879
    and-int/lit8 v3, v0, 0x1f

    ushr-int/lit8 v3, v3, 0x0

    int-to-byte v3, v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/al;->an:B

    .line 880
    and-int/lit16 v0, v0, 0xe0

    ushr-int/lit8 v0, v0, 0x5

    int-to-byte v0, v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    .line 881
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    add-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    .line 883
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    if-lez v0, :cond_0

    .line 885
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->b:[B

    .line 886
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->c:[F

    .line 887
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ad:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->d:[F

    move v0, v2

    .line 888
    :goto_0
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ad:B

    if-ge v0, v3, :cond_0

    .line 890
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->b:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 891
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->c:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 892
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->d:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 888
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 896
    :cond_0
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    if-lez v0, :cond_3

    .line 898
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->k:[B

    .line 899
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->l:[B

    .line 900
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->m:[F

    .line 901
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->n:[F

    .line 902
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->p:[F

    .line 903
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->o:[F

    move v3, v2

    .line 904
    :goto_1
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->am:B

    if-ge v3, v0, :cond_3

    .line 912
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 913
    and-int/lit8 v0, v4, 0x1

    ushr-int/lit8 v0, v0, 0x0

    int-to-byte v0, v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 914
    :goto_2
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->k:[B

    and-int/lit16 v4, v4, 0xfe

    ushr-int/lit8 v4, v4, 0x1

    int-to-byte v4, v4

    aput-byte v4, v5, v3

    .line 915
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->l:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    aput-byte v5, v4, v3

    .line 916
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->m:[F

    const/4 v5, 0x0

    iget v7, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v5, v7}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    aput v5, v4, v3

    .line 917
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->n:[F

    const/4 v5, 0x0

    iget v7, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v5, v7}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v5

    aput v5, v4, v3

    .line 918
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->o:[F

    const/4 v5, 0x0

    const v7, 0x40c90fdb

    invoke-virtual {v6, v5, v7}, Lsoftware/simplicial/a/f/bm;->a(FF)F

    move-result v5

    aput v5, v4, v3

    .line 919
    if-nez v0, :cond_2

    .line 920
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->p:[F

    const/4 v4, 0x0

    const v5, 0x48f42400    # 500000.0f

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v0, v3

    .line 904
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 913
    goto :goto_2

    .line 922
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->p:[F

    const/high16 v4, -0x800000    # Float.NEGATIVE_INFINITY

    aput v4, v0, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1256
    :catch_0
    move-exception v0

    .line 1258
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v2

    .line 1262
    :goto_4
    return v0

    .line 926
    :cond_3
    :try_start_1
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    if-lez v0, :cond_4

    .line 928
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->q:[B

    .line 929
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->r:[F

    .line 930
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->al:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->s:[F

    move v0, v2

    .line 931
    :goto_5
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->al:B

    if-ge v0, v3, :cond_4

    .line 933
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->q:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 934
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->r:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 935
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->s:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 931
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 939
    :cond_4
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    if-lez v0, :cond_5

    .line 941
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    new-array v0, v0, [S

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->t:[S

    .line 942
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->u:[F

    .line 943
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ag:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->v:[F

    move v0, v2

    .line 944
    :goto_6
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ag:B

    if-ge v0, v3, :cond_5

    .line 946
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->t:[S

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v4

    aput-short v4, v3, v0

    .line 947
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->u:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 948
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->v:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 944
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 952
    :cond_5
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    if-lez v0, :cond_6

    .line 954
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->w:[B

    .line 955
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->x:[B

    .line 956
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->y:[F

    .line 957
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ai:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->z:[F

    move v0, v2

    .line 958
    :goto_7
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ai:B

    if-ge v0, v3, :cond_6

    .line 960
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->w:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 961
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->x:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 962
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->y:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 963
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->z:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 958
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 967
    :cond_6
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    if-lez v0, :cond_7

    .line 969
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->A:[B

    .line 970
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->B:[B

    .line 971
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->C:[F

    .line 972
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->D:[F

    .line 973
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->aj:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->E:[F

    move v0, v2

    .line 974
    :goto_8
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->aj:B

    if-ge v0, v3, :cond_7

    .line 976
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 977
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->B:[B

    and-int/lit8 v5, v3, 0x3

    ushr-int/lit8 v5, v5, 0x0

    int-to-byte v5, v5

    aput-byte v5, v4, v0

    .line 978
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->A:[B

    and-int/lit16 v3, v3, 0xfc

    ushr-int/lit8 v3, v3, 0x2

    int-to-byte v3, v3

    aput-byte v3, v4, v0

    .line 979
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->C:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 980
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->D:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 981
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->E:[F

    const/4 v4, 0x0

    const v5, 0x427a6666    # 62.6f

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->b(FF)F

    move-result v4

    aput v4, v3, v0

    .line 974
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 985
    :cond_7
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    if-lez v0, :cond_8

    .line 987
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->e:[B

    .line 988
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->f:[F

    .line 989
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->g:[F

    .line 990
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->h:[F

    .line 991
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ae:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->i:[F

    move v0, v2

    .line 992
    :goto_9
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ae:B

    if-ge v0, v3, :cond_8

    .line 994
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->e:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 995
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->f:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 996
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->g:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 997
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->h:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 998
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->i:[F

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->a(FF)F

    move-result v4

    aput v4, v3, v0

    .line 992
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 1002
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1003
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1004
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1005
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1006
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->N:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1007
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    if-lez v0, :cond_e

    .line 1009
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->F:[B

    .line 1010
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->G:[B

    .line 1011
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->H:[B

    .line 1012
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->I:[B

    move v5, v2

    .line 1013
    :goto_a
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ak:B

    if-ge v5, v0, :cond_e

    .line 1015
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->F:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    aput-byte v3, v0, v5

    .line 1016
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 1017
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->G:[B

    and-int/lit8 v4, v0, 0x3f

    ushr-int/lit8 v4, v4, 0x0

    int-to-byte v4, v4

    aput-byte v4, v3, v5

    .line 1018
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->H:[B

    and-int/lit16 v0, v0, 0xc0

    ushr-int/lit8 v0, v0, 0x6

    int-to-byte v0, v0

    aput-byte v0, v3, v5

    .line 1019
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->I:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    aput-byte v3, v0, v5

    move v4, v2

    .line 1020
    :goto_b
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->I:[B

    aget-byte v0, v0, v5

    if-ge v4, v0, :cond_d

    .line 1028
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v7

    .line 1029
    and-int/lit8 v0, v7, 0x1

    ushr-int/lit8 v0, v0, 0x0

    if-ne v0, v1, :cond_9

    move v3, v1

    .line 1030
    :goto_c
    and-int/lit8 v0, v7, 0x2

    ushr-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_a

    move v0, v1

    .line 1031
    :goto_d
    and-int/lit16 v7, v7, 0xfc

    ushr-int/lit8 v7, v7, 0x2

    int-to-byte v7, v7

    .line 1032
    iget-object v8, p0, Lsoftware/simplicial/a/f/al;->J:Ljava/util/List;

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1033
    if-eqz v3, :cond_b

    .line 1034
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->K:Ljava/util/List;

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1037
    :goto_e
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->L:Ljava/util/List;

    const/4 v7, 0x0

    iget v8, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v7, v8}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1038
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->M:Ljava/util/List;

    const/4 v7, 0x0

    iget v8, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v7, v8}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1039
    if-nez v0, :cond_c

    .line 1040
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->N:Ljava/util/List;

    const/4 v3, 0x0

    const v7, 0x48f42400    # 500000.0f

    invoke-virtual {v6, v3, v7}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1020
    :goto_f
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_b

    :cond_9
    move v3, v2

    .line 1029
    goto :goto_c

    :cond_a
    move v0, v2

    .line 1030
    goto :goto_d

    .line 1036
    :cond_b
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->K:Ljava/util/List;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 1042
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/a/f/al;->N:Ljava/util/List;

    const/high16 v3, -0x800000    # Float.NEGATIVE_INFINITY

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 1013
    :cond_d
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_a

    .line 1047
    :cond_e
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    if-lez v0, :cond_11

    .line 1049
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->O:[B

    .line 1050
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->P:[B

    .line 1051
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->Q:[B

    .line 1052
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->R:[F

    .line 1053
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->an:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->S:[F

    move v0, v2

    .line 1054
    :goto_10
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->an:B

    if-ge v0, v3, :cond_11

    .line 1056
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->O:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 1057
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1058
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->Q:[B

    and-int/lit8 v5, v3, 0x3

    ushr-int/lit8 v5, v5, 0x0

    int-to-byte v5, v5

    aput-byte v5, v4, v0

    .line 1059
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->P:[B

    and-int/lit16 v3, v3, 0xfc

    ushr-int/lit8 v3, v3, 0x2

    int-to-byte v3, v3

    aput-byte v3, v4, v0

    .line 1060
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->P:[B

    aget-byte v3, v3, v0

    if-ltz v3, :cond_f

    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->P:[B

    aget-byte v3, v3, v0

    sget-object v4, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v4, v4

    if-lt v3, v4, :cond_10

    .line 1061
    :cond_f
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->P:[B

    sget-object v4, Lsoftware/simplicial/a/bl$b;->l:Lsoftware/simplicial/a/bl$b;

    invoke-virtual {v4}, Lsoftware/simplicial/a/bl$b;->ordinal()I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 1062
    :cond_10
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->R:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1063
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->S:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1054
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 1067
    :cond_11
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    if-lez v0, :cond_12

    .line 1069
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->T:[B

    .line 1070
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->U:[F

    .line 1071
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->V:[F

    .line 1072
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->W:[F

    .line 1073
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->ao:B

    new-array v0, v0, [F

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->X:[F

    move v0, v2

    .line 1074
    :goto_11
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->ao:B

    if-ge v0, v3, :cond_12

    .line 1076
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->T:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 1077
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->U:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1078
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->V:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1079
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->W:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1080
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->X:[F

    const/4 v4, 0x0

    iget v5, p0, Lsoftware/simplicial/a/f/al;->ac:F

    invoke-virtual {v6, v4, v5}, Lsoftware/simplicial/a/f/bm;->c(FF)F

    move-result v4

    aput v4, v3, v0

    .line 1074
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 1084
    :cond_12
    iget-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    if-lez v0, :cond_13

    .line 1086
    iget-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->Y:[B

    .line 1087
    iget-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->Z:[B

    .line 1088
    iget-short v0, p0, Lsoftware/simplicial/a/f/al;->ah:S

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->aa:[B

    move v0, v2

    .line 1089
    :goto_12
    iget-short v3, p0, Lsoftware/simplicial/a/f/al;->ah:S

    if-ge v0, v3, :cond_13

    .line 1091
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->Y:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    add-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 1092
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->Z:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    add-int/lit16 v4, v4, 0x80

    int-to-byte v4, v4

    aput-byte v4, v3, v0

    .line 1093
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->aa:[B

    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aput-byte v4, v3, v0

    .line 1089
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 1097
    :cond_13
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->af:B

    if-lez v0, :cond_19

    .line 1099
    iget-byte v0, p0, Lsoftware/simplicial/a/f/al;->af:B

    new-array v0, v0, [Lsoftware/simplicial/a/a/y;

    iput-object v0, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    move v0, v2

    .line 1100
    :goto_13
    iget-byte v3, p0, Lsoftware/simplicial/a/f/al;->af:B

    if-ge v0, v3, :cond_19

    .line 1102
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1103
    if-ltz v3, :cond_14

    sget-object v4, Lsoftware/simplicial/a/a/y;->d:[Lsoftware/simplicial/a/a/y$a;

    array-length v4, v4

    if-lt v3, v4, :cond_15

    .line 1104
    :cond_14
    sget-object v3, Lsoftware/simplicial/a/a/y$a;->a:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v3}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v3

    int-to-byte v3, v3

    .line 1105
    :cond_15
    sget-object v4, Lsoftware/simplicial/a/a/y;->d:[Lsoftware/simplicial/a/a/y$a;

    aget-object v3, v4, v3

    .line 1110
    sget-object v4, Lsoftware/simplicial/a/f/al$1;->a:[I

    invoke-virtual {v3}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 1247
    :goto_14
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    array-length v3, v3

    if-ge v0, v3, :cond_16

    .line 1249
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/ai;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/ai;-><init>()V

    aput-object v4, v3, v0

    .line 1247
    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    .line 1113
    :pswitch_0
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1114
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1115
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v7, Lsoftware/simplicial/a/a/c;

    invoke-direct {v7, v3, v4}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    aput-object v7, v5, v0

    .line 1100
    :cond_16
    :goto_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 1118
    :pswitch_1
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/d;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/d;-><init>()V

    aput-object v4, v3, v0

    goto :goto_15

    .line 1121
    :pswitch_2
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/r;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/r;-><init>()V

    aput-object v4, v3, v0

    goto :goto_15

    .line 1124
    :pswitch_3
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/s;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/s;-><init>()V

    aput-object v4, v3, v0

    goto :goto_15

    .line 1127
    :pswitch_4
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1128
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1129
    if-ltz v3, :cond_17

    sget-object v5, Lsoftware/simplicial/a/bv;->p:[Lsoftware/simplicial/a/bv;

    array-length v5, v5

    if-lt v3, v5, :cond_18

    .line 1130
    :cond_17
    sget-object v3, Lsoftware/simplicial/a/bv;->e:Lsoftware/simplicial/a/bv;

    invoke-virtual {v3}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v3

    int-to-byte v3, v3

    .line 1131
    :cond_18
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v7, Lsoftware/simplicial/a/a/v;

    invoke-direct {v7, v3, v4}, Lsoftware/simplicial/a/a/v;-><init>(II)V

    aput-object v7, v5, v0

    goto :goto_15

    .line 1134
    :pswitch_5
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/t;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/t;-><init>()V

    aput-object v4, v3, v0

    goto :goto_15

    .line 1137
    :pswitch_6
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1138
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1139
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v7, Lsoftware/simplicial/a/a/w;

    invoke-direct {v7, v3, v4}, Lsoftware/simplicial/a/a/w;-><init>(II)V

    aput-object v7, v5, v0

    goto :goto_15

    .line 1142
    :pswitch_7
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1143
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/af;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/af;-><init>(I)V

    aput-object v5, v4, v0

    goto :goto_15

    .line 1146
    :pswitch_8
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1147
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/ac;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/ac;-><init>(I)V

    aput-object v5, v4, v0

    goto :goto_15

    .line 1150
    :pswitch_9
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/f;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/f;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1153
    :pswitch_a
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/g;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/g;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1156
    :pswitch_b
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/h;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/h;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1159
    :pswitch_c
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/j;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/j;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1162
    :pswitch_d
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/i;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/i;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1165
    :pswitch_e
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    .line 1166
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/a;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/a;-><init>(S)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1169
    :pswitch_f
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v3

    .line 1170
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/aj;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/aj;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1173
    :pswitch_10
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    .line 1174
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/aa;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/aa;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1177
    :pswitch_11
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1178
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/b;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/b;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1181
    :pswitch_12
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    .line 1182
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/k;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/k;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1185
    :pswitch_13
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v3

    .line 1186
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/ab;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/ab;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1189
    :pswitch_14
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readLong()J

    move-result-wide v4

    .line 1190
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1191
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v7

    .line 1192
    iget-object v8, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v9, Lsoftware/simplicial/a/a/ak;

    invoke-direct {v9, v4, v5, v3, v7}, Lsoftware/simplicial/a/a/ak;-><init>(JBI)V

    aput-object v9, v8, v0

    goto/16 :goto_15

    .line 1195
    :pswitch_15
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1196
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v4

    .line 1197
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v7, Lsoftware/simplicial/a/a/ad;

    invoke-direct {v7, v3, v4}, Lsoftware/simplicial/a/a/ad;-><init>(II)V

    aput-object v7, v5, v0

    goto/16 :goto_15

    .line 1200
    :pswitch_16
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1201
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v4

    .line 1202
    iget-object v5, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v7, Lsoftware/simplicial/a/a/n;

    invoke-direct {v7, v3, v4}, Lsoftware/simplicial/a/a/n;-><init>(IZ)V

    aput-object v7, v5, v0

    goto/16 :goto_15

    .line 1205
    :pswitch_17
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1206
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/l;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/l;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1209
    :pswitch_18
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    .line 1210
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/m;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/m;-><init>(S)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1213
    :pswitch_19
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/u;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/u;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1216
    :pswitch_1a
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/o;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/o;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1219
    :pswitch_1b
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/p;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/p;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1222
    :pswitch_1c
    iget-object v3, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v4, Lsoftware/simplicial/a/a/q;

    invoke-direct {v4}, Lsoftware/simplicial/a/a/q;-><init>()V

    aput-object v4, v3, v0

    goto/16 :goto_15

    .line 1225
    :pswitch_1d
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1226
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1227
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    .line 1228
    iget-object v7, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v8, Lsoftware/simplicial/a/a/e;

    invoke-direct {v8, v3, v4, v5}, Lsoftware/simplicial/a/a/e;-><init>(III)V

    aput-object v8, v7, v0

    goto/16 :goto_15

    .line 1231
    :pswitch_1e
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1232
    iget-object v4, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v5, Lsoftware/simplicial/a/a/ag;

    invoke-direct {v5, v3}, Lsoftware/simplicial/a/a/ag;-><init>(I)V

    aput-object v5, v4, v0

    goto/16 :goto_15

    .line 1235
    :pswitch_1f
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1236
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1237
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    .line 1238
    iget-object v7, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v8, Lsoftware/simplicial/a/a/ae;

    invoke-direct {v8, v3, v4, v5}, Lsoftware/simplicial/a/a/ae;-><init>(III)V

    aput-object v8, v7, v0

    goto/16 :goto_15

    .line 1241
    :pswitch_20
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 1242
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 1243
    invoke-virtual {v6}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    .line 1244
    iget-object v7, p0, Lsoftware/simplicial/a/f/al;->j:[Lsoftware/simplicial/a/a/y;

    new-instance v8, Lsoftware/simplicial/a/a/x;

    invoke-direct {v8, v3, v4, v5}, Lsoftware/simplicial/a/a/x;-><init>(III)V

    aput-object v8, v7, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_15

    :cond_19
    move v0, v1

    .line 1262
    goto/16 :goto_4

    .line 1110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch
.end method
