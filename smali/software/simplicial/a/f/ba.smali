.class public final enum Lsoftware/simplicial/a/f/ba;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/f/ba;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/f/ba;

.field public static final enum b:Lsoftware/simplicial/a/f/ba;

.field public static final enum c:Lsoftware/simplicial/a/f/ba;

.field public static final enum d:Lsoftware/simplicial/a/f/ba;

.field public static final enum e:Lsoftware/simplicial/a/f/ba;

.field public static final enum f:Lsoftware/simplicial/a/f/ba;

.field public static final enum g:Lsoftware/simplicial/a/f/ba;

.field public static final enum h:Lsoftware/simplicial/a/f/ba;

.field public static final enum i:Lsoftware/simplicial/a/f/ba;

.field public static final enum j:Lsoftware/simplicial/a/f/ba;

.field public static final enum k:Lsoftware/simplicial/a/f/ba;

.field public static final enum l:Lsoftware/simplicial/a/f/ba;

.field public static final enum m:Lsoftware/simplicial/a/f/ba;

.field public static final enum n:Lsoftware/simplicial/a/f/ba;

.field public static final enum o:Lsoftware/simplicial/a/f/ba;

.field public static final enum p:Lsoftware/simplicial/a/f/ba;

.field public static final enum q:Lsoftware/simplicial/a/f/ba;

.field public static final enum r:Lsoftware/simplicial/a/f/ba;

.field public static final enum s:Lsoftware/simplicial/a/f/ba;

.field public static final enum t:Lsoftware/simplicial/a/f/ba;

.field public static final enum u:Lsoftware/simplicial/a/f/ba;

.field public static final enum v:Lsoftware/simplicial/a/f/ba;

.field public static final enum w:Lsoftware/simplicial/a/f/ba;

.field public static final enum x:Lsoftware/simplicial/a/f/ba;

.field public static y:[Lsoftware/simplicial/a/f/ba;

.field private static final synthetic z:[Lsoftware/simplicial/a/f/ba;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "NAME_TAKEN"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "NAME_INVALID"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "FULL"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->d:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "GAME_NOT_FOUND"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->e:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "FRIEND_NOT_FOUND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->f:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "DIED_THIS_ROUND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->h:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "CLAN_WAR_NOT_FOUND"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->i:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "CLAN_NOT_FOUND"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->j:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "ACCOUNT_NOT_FOUND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->k:Lsoftware/simplicial/a/f/ba;

    .line 9
    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "LACK_PERMISSION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->l:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "REQUEST_TIMED_OUT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->m:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "YOU_ARE_SPECTATING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->n:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "PLEASE_WAIT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "IS_ARENA"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->p:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "ACCOUNT_IN_USE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->q:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "UPDATE_AVAILABLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->r:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "INVALID_TOKEN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->s:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "BANNED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->t:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "NOT_SIGNED_IN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->u:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "TOURNAMENTS_DISABLED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->v:Lsoftware/simplicial/a/f/ba;

    .line 10
    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "MUTED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->w:Lsoftware/simplicial/a/f/ba;

    new-instance v0, Lsoftware/simplicial/a/f/ba;

    const-string v1, "FRIEND_ALREADY_TEAMED"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/f/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/f/ba;->x:Lsoftware/simplicial/a/f/ba;

    .line 6
    const/16 v0, 0x18

    new-array v0, v0, [Lsoftware/simplicial/a/f/ba;

    sget-object v1, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/f/ba;->b:Lsoftware/simplicial/a/f/ba;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/f/ba;->c:Lsoftware/simplicial/a/f/ba;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/f/ba;->d:Lsoftware/simplicial/a/f/ba;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/f/ba;->e:Lsoftware/simplicial/a/f/ba;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/f/ba;->f:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/f/ba;->h:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/f/ba;->i:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/f/ba;->j:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lsoftware/simplicial/a/f/ba;->k:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lsoftware/simplicial/a/f/ba;->l:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lsoftware/simplicial/a/f/ba;->m:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lsoftware/simplicial/a/f/ba;->n:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lsoftware/simplicial/a/f/ba;->o:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lsoftware/simplicial/a/f/ba;->p:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lsoftware/simplicial/a/f/ba;->q:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lsoftware/simplicial/a/f/ba;->r:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lsoftware/simplicial/a/f/ba;->s:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lsoftware/simplicial/a/f/ba;->t:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lsoftware/simplicial/a/f/ba;->u:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lsoftware/simplicial/a/f/ba;->v:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lsoftware/simplicial/a/f/ba;->w:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lsoftware/simplicial/a/f/ba;->x:Lsoftware/simplicial/a/f/ba;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/f/ba;->z:[Lsoftware/simplicial/a/f/ba;

    .line 11
    invoke-static {}, Lsoftware/simplicial/a/f/ba;->values()[Lsoftware/simplicial/a/f/ba;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/f/ba;->y:[Lsoftware/simplicial/a/f/ba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/f/ba;
    .locals 1

    .prologue
    .line 31
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/ba;->y:[Lsoftware/simplicial/a/f/ba;

    array-length v0, v0

    if-ge p0, v0, :cond_0

    .line 32
    sget-object v0, Lsoftware/simplicial/a/f/ba;->y:[Lsoftware/simplicial/a/f/ba;

    aget-object v0, v0, p0

    .line 33
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/f/z;)Lsoftware/simplicial/a/f/ba;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lsoftware/simplicial/a/f/ba$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/f/z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 26
    sget-object v0, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    :goto_0
    return-object v0

    .line 18
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/f/ba;->a:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 20
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/f/ba;->e:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 22
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/f/ba;->g:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 24
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/f/ba;->q:Lsoftware/simplicial/a/f/ba;

    goto :goto_0

    .line 15
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/f/ba;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/f/ba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/ba;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/f/ba;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/f/ba;->z:[Lsoftware/simplicial/a/f/ba;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/f/ba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/f/ba;

    return-object v0
.end method
