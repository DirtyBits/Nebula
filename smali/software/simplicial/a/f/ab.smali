.class public Lsoftware/simplicial/a/f/ab;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:F

.field public c:Z

.field public d:B

.field public e:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lsoftware/simplicial/a/f/bh;->c:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 26
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IFFBZZI)[B
    .locals 4

    .prologue
    .line 32
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->c:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ab;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 33
    const/4 v0, 0x0

    const v1, 0x40c90fdb

    invoke-virtual {p0, p2, v0, v1}, Lsoftware/simplicial/a/f/bn;->b(FFF)V

    .line 34
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p3, v0, v1}, Lsoftware/simplicial/a/f/bn;->a(FFF)V

    .line 35
    const/4 v0, 0x0

    .line 36
    if-eqz p5, :cond_0

    .line 37
    const/4 v0, 0x1

    int-to-byte v0, v0

    .line 38
    :cond_0
    if-eqz p6, :cond_1

    .line 39
    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    .line 40
    :cond_1
    if-eqz p7, :cond_2

    .line 41
    shl-int/lit8 v1, p7, 0x2

    and-int/lit8 v1, v1, -0x4

    or-int/2addr v0, v1

    int-to-byte v0, v0

    .line 42
    :cond_2
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 43
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 47
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 48
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 59
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/ab;->ar:I

    .line 60
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 62
    const/4 v1, 0x0

    const v2, 0x40c90fdb

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/f/bm;->b(FF)F

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ab;->a:F

    .line 63
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/f/bm;->a(FF)F

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ab;->b:F

    .line 64
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    iput-byte v1, p0, Lsoftware/simplicial/a/f/ab;->d:B

    .line 65
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    iput-byte v0, p0, Lsoftware/simplicial/a/f/ab;->e:B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 67
    :catch_0
    move-exception v0

    .line 69
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/ab;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 70
    const/4 v0, 0x0

    goto :goto_0
.end method
