.class public Lsoftware/simplicial/a/f/cp;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lsoftware/simplicial/a/i/c;

.field public c:Z

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aI:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 24
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IILsoftware/simplicial/a/i/c;ZZI)[B
    .locals 4

    .prologue
    .line 30
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aI:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/cp;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 32
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 33
    invoke-virtual {p3}, Lsoftware/simplicial/a/i/c;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 34
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 35
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 36
    if-eqz p5, :cond_0

    .line 37
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 39
    :catch_0
    move-exception v0

    .line 41
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 42
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    :try_start_0
    iget v2, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v2, p0, Lsoftware/simplicial/a/f/cp;->ar:I

    .line 54
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 55
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/cp;->a:I

    .line 56
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/i/c;->a(B)Lsoftware/simplicial/a/i/c;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/cp;->b:Lsoftware/simplicial/a/i/c;

    .line 57
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v3

    if-lez v3, :cond_2

    .line 59
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, p0, Lsoftware/simplicial/a/f/cp;->c:Z

    .line 60
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v3

    if-lez v3, :cond_1

    .line 62
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, p0, Lsoftware/simplicial/a/f/cp;->d:Z

    .line 63
    iget-boolean v3, p0, Lsoftware/simplicial/a/f/cp;->d:Z

    if-eqz v3, :cond_0

    .line 64
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/cp;->e:I

    .line 83
    :goto_0
    return v0

    .line 66
    :cond_0
    const/4 v2, -0x1

    iput v2, p0, Lsoftware/simplicial/a/f/cp;->e:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 79
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/cp;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 80
    goto :goto_0

    .line 70
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/cp;->d:Z

    .line 71
    const/4 v2, -0x1

    iput v2, p0, Lsoftware/simplicial/a/f/cp;->e:I

    goto :goto_0

    .line 75
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lsoftware/simplicial/a/f/cp;->c:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
