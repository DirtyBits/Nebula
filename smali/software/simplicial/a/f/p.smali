.class public Lsoftware/simplicial/a/f/p;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/c/i;",
            ">;"
        }
    .end annotation
.end field

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lsoftware/simplicial/a/f/bh;->s:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/p;->a:Ljava/util/List;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 98
    :try_start_0
    new-instance v7, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v7, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 99
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/p;->b:I

    .line 101
    const/4 v2, 0x0

    .line 103
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v8

    .line 104
    iget-object v3, p0, Lsoftware/simplicial/a/f/p;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    move v4, v1

    move-object v5, v2

    .line 105
    :goto_0
    if-ge v4, v8, :cond_6

    .line 107
    new-instance v6, Lsoftware/simplicial/a/c/i;

    invoke-direct {v6}, Lsoftware/simplicial/a/c/i;-><init>()V

    .line 109
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->g:Ljava/lang/String;

    .line 110
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v2, v2, [B

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->h:[B

    .line 111
    iget-object v2, v6, Lsoftware/simplicial/a/c/i;->h:[B

    invoke-virtual {v7, v2}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 112
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, v6, Lsoftware/simplicial/a/c/i;->c:I

    .line 113
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput v2, v6, Lsoftware/simplicial/a/c/i;->a:I

    .line 115
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 116
    and-int/lit8 v2, v3, 0x7

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    .line 117
    and-int/lit8 v9, v3, 0x18

    ushr-int/lit8 v9, v9, 0x3

    int-to-byte v9, v9

    .line 118
    and-int/lit8 v3, v3, 0x20

    ushr-int/lit8 v3, v3, 0x5

    int-to-byte v3, v3

    if-ne v3, v0, :cond_4

    move v3, v0

    :goto_1
    iput-boolean v3, v6, Lsoftware/simplicial/a/c/i;->i:Z

    .line 120
    if-ltz v2, :cond_0

    sget-object v3, Lsoftware/simplicial/a/c/h;->g:[Lsoftware/simplicial/a/c/h;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 121
    :cond_0
    sget-object v2, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    invoke-virtual {v2}, Lsoftware/simplicial/a/c/h;->ordinal()I

    move-result v2

    int-to-byte v2, v2

    .line 122
    :cond_1
    sget-object v3, Lsoftware/simplicial/a/c/h;->g:[Lsoftware/simplicial/a/c/h;

    aget-object v2, v3, v2

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    .line 124
    invoke-static {v9}, Lsoftware/simplicial/a/c/g;->a(B)Lsoftware/simplicial/a/c/g;

    move-result-object v2

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->e:Lsoftware/simplicial/a/c/g;

    .line 126
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 127
    and-int/lit8 v2, v3, 0xf

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    .line 128
    and-int/lit8 v3, v3, 0x10

    ushr-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    if-ne v3, v0, :cond_5

    move v3, v0

    :goto_2
    iput-boolean v3, v6, Lsoftware/simplicial/a/c/i;->j:Z

    .line 130
    if-ltz v2, :cond_2

    sget-object v3, Lsoftware/simplicial/a/c/e;->r:[Lsoftware/simplicial/a/c/e;

    array-length v3, v3

    if-lt v2, v3, :cond_3

    .line 131
    :cond_2
    sget-object v2, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    invoke-virtual {v2}, Lsoftware/simplicial/a/c/e;->ordinal()I

    move-result v2

    int-to-byte v2, v2

    .line 132
    :cond_3
    sget-object v3, Lsoftware/simplicial/a/c/e;->r:[Lsoftware/simplicial/a/c/e;

    aget-object v2, v3, v2

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->f:Lsoftware/simplicial/a/c/e;

    .line 134
    iget-object v2, v6, Lsoftware/simplicial/a/c/i;->e:Lsoftware/simplicial/a/c/g;

    invoke-static {v2}, Lsoftware/simplicial/a/c/a;->a(Lsoftware/simplicial/a/c/g;)I

    move-result v2

    iput v2, v6, Lsoftware/simplicial/a/c/i;->b:I

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v6, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    .line 138
    iget v2, v6, Lsoftware/simplicial/a/c/i;->c:I

    iget v3, p0, Lsoftware/simplicial/a/f/p;->b:I

    if-ne v2, v3, :cond_8

    move-object v3, v6

    .line 141
    :goto_3
    iget-object v2, p0, Lsoftware/simplicial/a/f/p;->a:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v5, v3

    goto/16 :goto_0

    :cond_4
    move v3, v1

    .line 118
    goto :goto_1

    :cond_5
    move v3, v1

    .line 128
    goto :goto_2

    .line 144
    :cond_6
    if-eqz v5, :cond_7

    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_7

    .line 146
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    move v2, v1

    .line 147
    :goto_4
    if-ge v2, v3, :cond_7

    .line 149
    new-instance v4, Lsoftware/simplicial/a/c/c;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lsoftware/simplicial/a/c/c;-><init>(I)V

    .line 150
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v6

    iput v6, v4, Lsoftware/simplicial/a/c/c;->b:I

    .line 151
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lsoftware/simplicial/a/c/c;->d:Ljava/lang/String;

    .line 152
    invoke-virtual {v7}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v6

    iput-boolean v6, v4, Lsoftware/simplicial/a/c/c;->c:Z

    .line 153
    iget-object v6, v5, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 157
    :catch_0
    move-exception v0

    .line 159
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/p;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 163
    :cond_7
    return v0

    :cond_8
    move-object v3, v5

    goto :goto_3
.end method
