.class public Lsoftware/simplicial/a/f/cv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/a/f/bg;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/a/f/cv$a;
    }
.end annotation


# instance fields
.field private A:I

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:I

.field private F:Lsoftware/simplicial/a/f/aa;

.field private G:Lsoftware/simplicial/a/f/cw;

.field private H:Ljava/net/DatagramSocket;

.field private I:Ljava/net/InetAddress;

.field private J:Ljava/lang/Thread;

.field private K:Ljava/net/DatagramSocket;

.field private L:Ljava/net/InetAddress;

.field private M:Ljava/lang/Thread;

.field private N:I

.field private O:Ljava/util/Timer;

.field private P:Ljava/util/Timer;

.field private Q:S

.field a:J

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field c:J

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field e:Lsoftware/simplicial/a/f/cv$a;

.field f:Lsoftware/simplicial/a/f/cv$a;

.field g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/Object;

.field private final j:Ljava/lang/Object;

.field private k:J

.field private l:Ljava/util/Random;

.field private m:I

.field private n:I

.field private o:Z

.field private p:I

.field private q:I

.field private r:Lsoftware/simplicial/a/x;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:[B

.field private v:Lsoftware/simplicial/a/e;

.field private w:Lsoftware/simplicial/a/af;

.field private x:Lsoftware/simplicial/a/as;

.field private y:B

.field private z:I


# direct methods
.method public constructor <init>(Lsoftware/simplicial/a/f/cw;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->i:Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->j:Ljava/lang/Object;

    .line 46
    iput-wide v6, p0, Lsoftware/simplicial/a/f/cv;->a:J

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->b:Ljava/util/List;

    .line 48
    iput-wide v6, p0, Lsoftware/simplicial/a/f/cv;->c:J

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->d:Ljava/util/List;

    .line 50
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->e:Lsoftware/simplicial/a/f/cv$a;

    .line 51
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->f:Lsoftware/simplicial/a/f/cv$a;

    .line 52
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    sget-object v2, Lsoftware/simplicial/a/f/bh;->w:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x1

    sget-object v3, Lsoftware/simplicial/a/f/bh;->u:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->g:Ljava/util/Set;

    .line 53
    iput-wide v6, p0, Lsoftware/simplicial/a/f/cv;->k:J

    .line 54
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->l:Ljava/util/Random;

    .line 55
    iput v5, p0, Lsoftware/simplicial/a/f/cv;->m:I

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->s:Ljava/lang/String;

    .line 72
    iput v5, p0, Lsoftware/simplicial/a/f/cv;->D:I

    .line 73
    iput v5, p0, Lsoftware/simplicial/a/f/cv;->E:I

    .line 74
    sget-object v0, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->F:Lsoftware/simplicial/a/f/aa;

    .line 76
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    .line 77
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    .line 78
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    .line 79
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    .line 80
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->L:Ljava/net/InetAddress;

    .line 81
    iput-object v4, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    .line 88
    new-instance v0, Lsoftware/simplicial/a/f/cv$1;

    invoke-direct {v0, p0}, Lsoftware/simplicial/a/f/cv$1;-><init>(Lsoftware/simplicial/a/f/cv;)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->h:Ljava/lang/Runnable;

    .line 153
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    .line 154
    return-void
.end method

.method static synthetic A(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->E:I

    return v0
.end method

.method static synthetic B(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->D:I

    return v0
.end method

.method static synthetic C(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    return-object v0
.end method

.method static synthetic D(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/f/cw;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;I)I
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lsoftware/simplicial/a/f/cv;->m:I

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;)J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lsoftware/simplicial/a/f/cv;->k:J

    return-wide v0
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;J)J
    .locals 1

    .prologue
    .line 28
    iput-wide p1, p0, Lsoftware/simplicial/a/f/cv;->k:J

    return-wide p1
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Ljava/net/DatagramSocket;)Ljava/net/DatagramSocket;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    return-object p1
.end method

.method private declared-synchronized a()V
    .locals 4

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->e:Lsoftware/simplicial/a/f/cv$a;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->e:Lsoftware/simplicial/a/f/cv$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    .line 223
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 225
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    :cond_2
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 234
    :catch_0
    move-exception v0

    .line 236
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 241
    :catch_1
    move-exception v0

    .line 243
    :try_start_5
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occured while disconnecting socket.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/net/DatagramSocket;Ljava/net/DatagramSocket;)V
    .locals 1

    .prologue
    .line 698
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    if-ne v0, p1, :cond_0

    .line 699
    iput-object p2, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    .line 700
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    if-ne v0, p1, :cond_1

    .line 701
    iput-object p2, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 702
    :cond_1
    monitor-exit p0

    return-void

    .line 698
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/bx;)V
    .locals 5

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bx;->a:I

    iget v1, p0, Lsoftware/simplicial/a/f/cv;->m:I

    if-eq v0, v1, :cond_1

    .line 319
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "Ignoring %s message for client %d."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lsoftware/simplicial/a/f/bx;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bh;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lsoftware/simplicial/a/f/bx;->ar:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 323
    :cond_1
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lsoftware/simplicial/a/f/bx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p1, Lsoftware/simplicial/a/f/bx;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/cv;->a(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Ljava/net/DatagramSocket;Ljava/net/DatagramSocket;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/a/f/cv;->a(Ljava/net/DatagramSocket;Ljava/net/DatagramSocket;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/bx;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/bx;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/x;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/x;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/y;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/y;)V

    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/x;)V
    .locals 5

    .prologue
    .line 329
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/x;->ar:I

    iget v1, p0, Lsoftware/simplicial/a/f/cv;->m:I

    if-eq v0, v1, :cond_1

    .line 331
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "Ignoring %s message for client %d."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lsoftware/simplicial/a/f/x;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f/bh;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lsoftware/simplicial/a/f/x;->ar:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 335
    :cond_1
    :try_start_1
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne v0, v1, :cond_2

    .line 337
    iget v0, p1, Lsoftware/simplicial/a/f/x;->c:I

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->E:I

    .line 338
    iget v0, p1, Lsoftware/simplicial/a/f/x;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->D:I

    .line 340
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->k()V

    .line 341
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->i()V

    .line 342
    sget-object v0, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/aa;)V

    .line 345
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    invoke-interface {v0, p1}, Lsoftware/simplicial/a/f/cw;->a(Lsoftware/simplicial/a/f/be;)V

    .line 347
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-eq v0, v1, :cond_0

    .line 348
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/y;)V
    .locals 2

    .prologue
    .line 307
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/y;->ar:I

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->E:I

    .line 308
    iget v0, p1, Lsoftware/simplicial/a/f/y;->a:I

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->D:I

    .line 310
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lsoftware/simplicial/a/f/y;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p1, Lsoftware/simplicial/a/f/y;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/cv;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    :cond_0
    monitor-exit p0

    return-void

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lsoftware/simplicial/a/f/cv;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->l:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->m:I

    return v0
.end method

.method static synthetic d(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->n:I

    return v0
.end method

.method static synthetic e(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->p:I

    return v0
.end method

.method static synthetic f(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->q:I

    return v0
.end method

.method static synthetic g(Lsoftware/simplicial/a/f/cv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->s:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized g()V
    .locals 4

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->f:Lsoftware/simplicial/a/f/cv$a;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->f:Lsoftware/simplicial/a/f/cv$a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    .line 256
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    .line 258
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    .line 262
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 279
    :cond_2
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->L:Ljava/net/InetAddress;

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 281
    monitor-exit p0

    return-void

    .line 267
    :catch_0
    move-exception v0

    .line 269
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 274
    :catch_1
    move-exception v0

    .line 276
    :try_start_5
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occured while disconnecting socket.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic h(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/x;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->r:Lsoftware/simplicial/a/x;

    return-object v0
.end method

.method private declared-synchronized h()V
    .locals 3

    .prologue
    .line 287
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->i:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 289
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 294
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    .line 296
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 302
    :goto_0
    monitor-exit p0

    return-void

    .line 296
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 298
    :catch_0
    move-exception v0

    .line 300
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Exception in StopKeepAliveTimer"

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 287
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic i(Lsoftware/simplicial/a/f/cv;)S
    .locals 1

    .prologue
    .line 28
    iget-short v0, p0, Lsoftware/simplicial/a/f/cv;->Q:S

    return v0
.end method

.method private declared-synchronized i()V
    .locals 7

    .prologue
    .line 364
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lsoftware/simplicial/a/f/cv;->i:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 366
    :try_start_1
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->h()V

    .line 371
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 373
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    .line 374
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->P:Ljava/util/Timer;

    new-instance v1, Lsoftware/simplicial/a/f/cv$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/a/f/cv$2;-><init>(Lsoftware/simplicial/a/f/cv;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 459
    :cond_0
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    monitor-exit p0

    return-void

    .line 459
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 364
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 6

    .prologue
    .line 464
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 466
    :try_start_1
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->k()V

    .line 471
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->O:Ljava/util/Timer;

    .line 472
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->O:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/a/f/cv$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/a/f/cv$3;-><init>(Lsoftware/simplicial/a/f/cv;)V

    const-wide/16 v4, 0xbb8

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 482
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    monitor-exit p0

    return-void

    .line 482
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 464
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic j(Lsoftware/simplicial/a/f/cv;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cv;->o:Z

    return v0
.end method

.method static synthetic k(Lsoftware/simplicial/a/f/cv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->t:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized k()V
    .locals 3

    .prologue
    .line 489
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 491
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->O:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->O:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 496
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->O:Ljava/util/Timer;

    .line 498
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    :goto_0
    monitor-exit p0

    return-void

    .line 498
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 500
    :catch_0
    move-exception v0

    .line 502
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "Exception in StopConnectTimer"

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 489
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized l()I
    .locals 1

    .prologue
    .line 688
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->L:Ljava/net/InetAddress;

    .line 689
    if-nez v0, :cond_0

    .line 690
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :cond_0
    if-nez v0, :cond_1

    .line 692
    const/4 v0, 0x0

    .line 693
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 688
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic l(Lsoftware/simplicial/a/f/cv;)[B
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->u:[B

    return-object v0
.end method

.method static synthetic m(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/e;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->v:Lsoftware/simplicial/a/e;

    return-object v0
.end method

.method static synthetic n(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/af;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->w:Lsoftware/simplicial/a/af;

    return-object v0
.end method

.method static synthetic o(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->z:I

    return v0
.end method

.method static synthetic p(Lsoftware/simplicial/a/f/cv;)B
    .locals 1

    .prologue
    .line 28
    iget-byte v0, p0, Lsoftware/simplicial/a/f/cv;->y:B

    return v0
.end method

.method static synthetic q(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->A:I

    return v0
.end method

.method static synthetic r(Lsoftware/simplicial/a/f/cv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/as;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->x:Lsoftware/simplicial/a/as;

    return-object v0
.end method

.method static synthetic t(Lsoftware/simplicial/a/f/cv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lsoftware/simplicial/a/f/cv;)Ljava/net/DatagramSocket;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    return-object v0
.end method

.method static synthetic v(Lsoftware/simplicial/a/f/cv;)Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->I:Ljava/net/InetAddress;

    return-object v0
.end method

.method static synthetic w(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->N:I

    return v0
.end method

.method static synthetic x(Lsoftware/simplicial/a/f/cv;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->J:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic y(Lsoftware/simplicial/a/f/cv;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->j()V

    return-void
.end method

.method static synthetic z(Lsoftware/simplicial/a/f/cv;)I
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->l()I

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Lsoftware/simplicial/a/f/aa;)V
    .locals 1

    .prologue
    .line 595
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->F:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_1

    .line 602
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 598
    :cond_1
    :try_start_1
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->F:Lsoftware/simplicial/a/f/aa;

    .line 600
    sget-object v0, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    if-ne p1, v0, :cond_0

    .line 601
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    invoke-interface {v0}, Lsoftware/simplicial/a/f/cw;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 595
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lsoftware/simplicial/a/f/ac;)V
    .locals 2

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/ac;->b:I

    iget v1, p0, Lsoftware/simplicial/a/f/cv;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_0

    .line 358
    :goto_0
    monitor-exit p0

    return-void

    .line 356
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->f()V

    .line 357
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    invoke-interface {v0, p1}, Lsoftware/simplicial/a/f/cw;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a([B)V
    .locals 3

    .prologue
    .line 510
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->F:Lsoftware/simplicial/a/f/aa;

    sget-object v1, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-eq v0, v1, :cond_1

    .line 583
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    const/4 v1, 0x0

    aget-byte v1, p1, v1

    aget-object v0, v0, v1

    .line 525
    :try_start_0
    new-instance v1, Ljava/net/DatagramPacket;

    array-length v2, p1

    invoke-direct {v1, p1, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 527
    sget-object v2, Lsoftware/simplicial/a/f/cv$4;->a:[I

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 564
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    .line 565
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 567
    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 579
    :catch_0
    move-exception v0

    .line 581
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 535
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    .line 536
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 538
    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    goto :goto_0

    .line 549
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->H:Ljava/net/DatagramSocket;

    .line 550
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 552
    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 527
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 614
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->g()V

    .line 616
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 617
    iget v1, p0, Lsoftware/simplicial/a/f/cv;->E:I

    iget v2, p0, Lsoftware/simplicial/a/f/cv;->D:I

    iget v3, p0, Lsoftware/simplicial/a/f/cv;->m:I

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/v;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v1

    .line 618
    new-instance v2, Ljava/net/DatagramPacket;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v0

    invoke-direct {v2, v1, v0}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 620
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->L:Ljava/net/InetAddress;

    .line 622
    new-instance v0, Ljava/net/DatagramSocket;

    invoke-direct {v0}, Ljava/net/DatagramSocket;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    .line 623
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V

    .line 624
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setSendBufferSize(I)V

    .line 625
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->L:Ljava/net/InetAddress;

    iget v3, p0, Lsoftware/simplicial/a/f/cv;->N:I

    invoke-virtual {v0, v1, v3}, Ljava/net/DatagramSocket;->connect(Ljava/net/InetAddress;I)V

    .line 626
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    invoke-virtual {v0, v2}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 627
    new-instance v0, Lsoftware/simplicial/a/f/cv$a;

    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->K:Ljava/net/DatagramSocket;

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/a/f/cv$a;-><init>(Lsoftware/simplicial/a/f/cv;Ljava/net/DatagramSocket;)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->f:Lsoftware/simplicial/a/f/cv$a;

    .line 628
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lsoftware/simplicial/a/f/cv;->f:Lsoftware/simplicial/a/f/cv$a;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    .line 629
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->M:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 646
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    .line 641
    :catch_0
    move-exception v0

    .line 643
    :try_start_1
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 644
    const/4 v0, 0x0

    goto :goto_0

    .line 614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;ILsoftware/simplicial/a/f/cw;IIILjava/lang/String;Lsoftware/simplicial/a/x;SZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)Z
    .locals 4

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-ne v1, v2, :cond_0

    .line 165
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->f()V

    .line 166
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/f/aa;->b:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 167
    const/4 v1, 0x1

    .line 200
    :goto_0
    monitor-exit p0

    return v1

    .line 169
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    if-eq v1, v2, :cond_2

    .line 171
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UDP Client was not disconnected. ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lsoftware/simplicial/a/f/cv;->e()Lsoftware/simplicial/a/f/aa;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/aa;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 172
    const/4 v1, 0x0

    goto :goto_0

    .line 175
    :cond_2
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv;->C:Ljava/lang/String;

    .line 176
    iput p2, p0, Lsoftware/simplicial/a/f/cv;->N:I

    .line 177
    iput-object p3, p0, Lsoftware/simplicial/a/f/cv;->G:Lsoftware/simplicial/a/f/cw;

    .line 178
    iput-short p9, p0, Lsoftware/simplicial/a/f/cv;->Q:S

    .line 179
    iput p4, p0, Lsoftware/simplicial/a/f/cv;->n:I

    .line 180
    iput-boolean p10, p0, Lsoftware/simplicial/a/f/cv;->o:Z

    .line 181
    iput p5, p0, Lsoftware/simplicial/a/f/cv;->p:I

    .line 182
    iput p6, p0, Lsoftware/simplicial/a/f/cv;->q:I

    .line 183
    iput-object p7, p0, Lsoftware/simplicial/a/f/cv;->s:Ljava/lang/String;

    .line 184
    iput-object p8, p0, Lsoftware/simplicial/a/f/cv;->r:Lsoftware/simplicial/a/x;

    .line 185
    iput-object p11, p0, Lsoftware/simplicial/a/f/cv;->t:Ljava/lang/String;

    .line 186
    move-object/from16 v0, p12

    array-length v1, v0

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/cv;->u:[B

    .line 187
    move-object/from16 v0, p13

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->v:Lsoftware/simplicial/a/e;

    .line 188
    move-object/from16 v0, p14

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->w:Lsoftware/simplicial/a/af;

    .line 189
    move/from16 v0, p15

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->z:I

    .line 190
    move/from16 v0, p16

    iput-byte v0, p0, Lsoftware/simplicial/a/f/cv;->y:B

    .line 191
    move/from16 v0, p17

    iput v0, p0, Lsoftware/simplicial/a/f/cv;->A:I

    .line 192
    move-object/from16 v0, p18

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->B:Ljava/lang/String;

    .line 193
    move-object/from16 v0, p19

    iput-object v0, p0, Lsoftware/simplicial/a/f/cv;->x:Lsoftware/simplicial/a/as;

    .line 195
    sget-object v1, Lsoftware/simplicial/a/f/aa;->b:Lsoftware/simplicial/a/f/aa;

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/aa;)V

    .line 197
    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lsoftware/simplicial/a/f/cv;->h:Ljava/lang/Runnable;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 198
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    const/4 v1, 0x1

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->m:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 656
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->E:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 661
    iget v0, p0, Lsoftware/simplicial/a/f/cv;->D:I

    return v0
.end method

.method public declared-synchronized e()Lsoftware/simplicial/a/f/aa;
    .locals 1

    .prologue
    .line 587
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv;->F:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 4

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 206
    iget v1, p0, Lsoftware/simplicial/a/f/cv;->D:I

    iget v2, p0, Lsoftware/simplicial/a/f/cv;->E:I

    iget v3, p0, Lsoftware/simplicial/a/f/cv;->m:I

    invoke-static {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/ac;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/cv;->a([B)V

    .line 208
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->k()V

    .line 209
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->h()V

    .line 211
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->a()V

    .line 212
    invoke-direct {p0}, Lsoftware/simplicial/a/f/cv;->g()V

    .line 214
    sget-object v0, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/aa;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
