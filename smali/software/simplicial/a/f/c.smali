.class public Lsoftware/simplicial/a/f/c;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lsoftware/simplicial/a/f/bh;->L:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 17
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;I)[B
    .locals 4

    .prologue
    .line 23
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->L:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/c;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 25
    :catch_0
    move-exception v0

    .line 27
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 28
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 1

    .prologue
    .line 36
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/c;->ar:I

    .line 37
    const/4 v0, 0x1

    return v0
.end method
