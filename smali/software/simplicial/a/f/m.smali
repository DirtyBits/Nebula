.class public Lsoftware/simplicial/a/f/m;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Lsoftware/simplicial/a/c/b;

.field public b:Lsoftware/simplicial/a/c/e;

.field public c:Lsoftware/simplicial/a/c/g;

.field public d:I

.field public e:Z

.field public f:Z

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lsoftware/simplicial/a/f/bh;->X:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 28
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;IZZI)[B
    .locals 4

    .prologue
    .line 35
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->X:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/m;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 36
    invoke-virtual {p2}, Lsoftware/simplicial/a/c/b;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 37
    invoke-virtual {p3}, Lsoftware/simplicial/a/c/e;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 38
    invoke-virtual {p4}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 39
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 40
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 41
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 42
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v0

    .line 46
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 47
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 57
    :try_start_0
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/m;->ar:I

    .line 58
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 60
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    .line 61
    if-ltz v1, :cond_0

    sget-object v3, Lsoftware/simplicial/a/c/b;->h:[Lsoftware/simplicial/a/c/b;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 62
    :cond_0
    sget-object v1, Lsoftware/simplicial/a/c/b;->a:Lsoftware/simplicial/a/c/b;

    invoke-virtual {v1}, Lsoftware/simplicial/a/c/b;->ordinal()I

    move-result v1

    int-to-byte v1, v1

    .line 63
    :cond_1
    sget-object v3, Lsoftware/simplicial/a/c/b;->h:[Lsoftware/simplicial/a/c/b;

    aget-object v1, v3, v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/m;->a:Lsoftware/simplicial/a/c/b;

    .line 65
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    .line 66
    if-ltz v1, :cond_2

    sget-object v3, Lsoftware/simplicial/a/c/e;->r:[Lsoftware/simplicial/a/c/e;

    array-length v3, v3

    if-lt v1, v3, :cond_3

    .line 67
    :cond_2
    sget-object v1, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    invoke-virtual {v1}, Lsoftware/simplicial/a/c/e;->ordinal()I

    move-result v1

    int-to-byte v1, v1

    .line 68
    :cond_3
    sget-object v3, Lsoftware/simplicial/a/c/e;->r:[Lsoftware/simplicial/a/c/e;

    aget-object v1, v3, v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/m;->b:Lsoftware/simplicial/a/c/e;

    .line 70
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/c/g;->a(B)Lsoftware/simplicial/a/c/g;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/m;->c:Lsoftware/simplicial/a/c/g;

    .line 72
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/m;->d:I

    .line 73
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/m;->e:Z

    .line 74
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v1

    if-lez v1, :cond_4

    .line 76
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v1

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/m;->f:Z

    .line 77
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/m;->g:I

    .line 91
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 81
    :cond_4
    const/4 v1, 0x0

    iput-boolean v1, p0, Lsoftware/simplicial/a/f/m;->f:Z

    .line 82
    const/4 v1, -0x1

    iput v1, p0, Lsoftware/simplicial/a/f/m;->g:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    .line 87
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/m;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method
