.class public Lsoftware/simplicial/a/f/h;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lsoftware/simplicial/a/b/b;

.field public c:Lsoftware/simplicial/a/b/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lsoftware/simplicial/a/f/bh;->ah:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 19
    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 43
    :try_start_0
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 45
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/a/f/h;->a:I

    .line 46
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 47
    if-ltz v0, :cond_0

    sget-object v2, Lsoftware/simplicial/a/b/b;->f:[Lsoftware/simplicial/a/b/b;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 48
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    invoke-virtual {v0}, Lsoftware/simplicial/a/b/b;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    .line 49
    :cond_1
    sget-object v2, Lsoftware/simplicial/a/b/b;->f:[Lsoftware/simplicial/a/b/b;

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/h;->b:Lsoftware/simplicial/a/b/b;

    .line 50
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    .line 51
    if-ltz v0, :cond_2

    sget-object v1, Lsoftware/simplicial/a/b/e;->g:[Lsoftware/simplicial/a/b/e;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    .line 52
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/b/e;->a:Lsoftware/simplicial/a/b/e;

    invoke-virtual {v0}, Lsoftware/simplicial/a/b/e;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    .line 53
    :cond_3
    sget-object v1, Lsoftware/simplicial/a/b/e;->g:[Lsoftware/simplicial/a/b/e;

    aget-object v0, v1, v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/h;->c:Lsoftware/simplicial/a/b/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/h;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 58
    const/4 v0, 0x0

    goto :goto_0
.end method
