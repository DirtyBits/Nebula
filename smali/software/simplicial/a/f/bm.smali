.class public Lsoftware/simplicial/a/f/bm;
.super Ljava/io/DataInputStream;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 15
    return-void
.end method


# virtual methods
.method public a(FF)F
    .locals 2

    .prologue
    .line 19
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 20
    int-to-float v0, v0

    sub-float v1, p2, p1

    mul-float/2addr v0, v1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 21
    return v0
.end method

.method public a()I
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 43
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 44
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 45
    shl-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x0

    add-int/2addr v0, v1

    return v0
.end method

.method public b(FF)F
    .locals 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    int-to-float v0, v0

    sub-float v1, p2, p1

    mul-float/2addr v0, v1

    const v1, 0x477fff00    # 65535.0f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 27
    return v0
.end method

.method public c(FF)F
    .locals 3

    .prologue
    .line 32
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 33
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 34
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 35
    shl-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x0

    add-int/2addr v0, v1

    .line 36
    int-to-float v0, v0

    sub-float v1, p2, p1

    mul-float/2addr v0, v1

    const v1, 0x4b7fffff    # 1.6777215E7f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 37
    return v0
.end method
