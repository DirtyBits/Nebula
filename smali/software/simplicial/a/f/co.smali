.class public Lsoftware/simplicial/a/f/co;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:[I

.field public c:S

.field public d:B

.field public e:B

.field public f:[B

.field public g:[B

.field public h:[S

.field public i:Ljava/lang/String;

.field public j:[B

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Lsoftware/simplicial/a/ap;

.field public q:Lsoftware/simplicial/a/aq;

.field public r:Lsoftware/simplicial/a/am;

.field public s:Z

.field public t:I

.field public u:Z

.field public v:Z

.field public w:Lsoftware/simplicial/a/c/g;

.field public x:[B

.field public y:[B


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 55
    sget-object v0, Lsoftware/simplicial/a/f/bh;->u:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 23
    new-array v0, v2, [B

    iput-object v0, p0, Lsoftware/simplicial/a/f/co;->a:[B

    .line 24
    new-array v0, v2, [I

    iput-object v0, p0, Lsoftware/simplicial/a/f/co;->b:[I

    .line 25
    const/16 v0, 0x7fff

    iput-short v0, p0, Lsoftware/simplicial/a/f/co;->c:S

    .line 26
    iput-byte v1, p0, Lsoftware/simplicial/a/f/co;->d:B

    .line 27
    iput-byte v1, p0, Lsoftware/simplicial/a/f/co;->e:B

    .line 56
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;[Lsoftware/simplicial/a/bf;S[Lsoftware/simplicial/a/bf;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BZZIZZZZILsoftware/simplicial/a/c/g;Z)[B
    .locals 10

    .prologue
    .line 63
    .line 68
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->u:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v2}, Lsoftware/simplicial/a/f/co;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 71
    move-object/from16 v0, p8

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 72
    move/from16 v0, p12

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 73
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 74
    invoke-virtual {p5}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 75
    const/4 v2, 0x0

    .line 76
    invoke-virtual/range {p6 .. p6}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v3

    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x3

    int-to-byte v3, v3

    or-int/2addr v2, v3

    int-to-byte v2, v2

    .line 77
    invoke-virtual/range {p7 .. p7}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    shl-int/lit8 v3, v3, 0x2

    and-int/lit8 v3, v3, 0xc

    int-to-byte v3, v3

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 78
    if-eqz p10, :cond_0

    const/4 v2, 0x1

    :goto_0
    shl-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 79
    if-eqz p11, :cond_1

    const/4 v2, 0x1

    :goto_1
    shl-int/lit8 v2, v2, 0x5

    and-int/lit8 v2, v2, 0x20

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 80
    if-eqz p13, :cond_2

    const/4 v2, 0x1

    :goto_2
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 81
    if-eqz p15, :cond_3

    const/4 v2, 0x1

    :goto_3
    shl-int/lit8 v2, v2, 0x7

    and-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v2, v2

    .line 82
    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 84
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 85
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v5

    .line 86
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 87
    const/4 v3, 0x0

    .line 88
    const/4 v2, 0x0

    move v4, v2

    move v2, v3

    :goto_4
    if-ge v4, p4, :cond_7

    .line 90
    aget-object v3, p3, v4

    iget-boolean v3, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_4

    if-nez p14, :cond_4

    .line 88
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 78
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 79
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 80
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 81
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 93
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 95
    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v3, v3, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v3, :cond_5

    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v6, v3, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v3

    :goto_6
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 96
    const/4 v3, 0x0

    .line 97
    aget-object v6, p3, v4

    iget v6, v6, Lsoftware/simplicial/a/bf;->C:I

    shl-int/lit8 v6, v6, 0x0

    and-int/lit8 v6, v6, 0x1f

    int-to-byte v6, v6

    or-int/2addr v3, v6

    int-to-byte v6, v3

    .line 98
    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v3, :cond_6

    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->c:I

    :goto_7
    shl-int/lit8 v3, v3, 0x5

    and-int/lit16 v3, v3, 0xe0

    int-to-byte v3, v3

    or-int/2addr v3, v6

    int-to-byte v3, v3

    .line 99
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 143
    :catch_0
    move-exception v2

    .line 145
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception occurred writing data. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 146
    const/4 v2, 0x0

    .line 151
    :goto_8
    return-object v2

    .line 95
    :cond_5
    const/4 v3, -0x1

    goto :goto_6

    .line 98
    :cond_6
    const/4 v3, 0x7

    goto :goto_7

    .line 103
    :cond_7
    const/4 v4, 0x0

    .line 104
    :try_start_1
    array-length v6, p1

    const/4 v3, 0x0

    move v9, v3

    move v3, v4

    move v4, v9

    :goto_9
    if-ge v4, v6, :cond_a

    aget-object v7, p1, v4

    .line 106
    iget-boolean v8, v7, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v8, :cond_9

    if-nez p14, :cond_9

    .line 104
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 109
    :cond_9
    iget v8, v7, Lsoftware/simplicial/a/bf;->C:I

    invoke-virtual {p0, v8}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 110
    invoke-virtual {v7}, Lsoftware/simplicial/a/bf;->d()I

    move-result v7

    invoke-virtual {p0, v7}, Lsoftware/simplicial/a/f/bn;->a(I)V

    .line 111
    add-int/lit8 v3, v3, 0x1

    .line 112
    const/4 v7, 0x5

    if-lt v3, v7, :cond_8

    .line 115
    :cond_a
    :goto_a
    const/4 v4, 0x5

    if-ge v3, v4, :cond_b

    .line 117
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 118
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->a(I)V

    .line 115
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 121
    :cond_b
    const/4 v4, 0x0

    .line 122
    if-eqz p16, :cond_d

    const/4 v3, 0x1

    :goto_b
    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 123
    const/16 v4, 0x7f

    move/from16 v0, p17

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    and-int/lit16 v4, v4, 0xfe

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 124
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 126
    const/4 v4, 0x0

    .line 127
    if-eqz p14, :cond_e

    const/4 v3, 0x1

    :goto_c
    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 128
    invoke-virtual/range {p18 .. p18}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    and-int/lit8 v4, v4, 0x7e

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v4, v3

    .line 129
    if-eqz p19, :cond_f

    const/4 v3, 0x1

    :goto_d
    shl-int/lit8 v3, v3, 0x7

    and-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 130
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 132
    move-object/from16 v0, p9

    array-length v3, v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 133
    move-object/from16 v0, p9

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 135
    if-eqz p14, :cond_c

    if-nez p15, :cond_c

    .line 137
    const/4 v3, 0x0

    .line 138
    const/4 v4, 0x0

    aget-object v4, p3, v4

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 139
    const/4 v4, 0x1

    aget-object v4, p3, v4

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    shl-int/lit8 v4, v4, 0x4

    and-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 140
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 149
    :cond_c
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v3

    .line 150
    int-to-byte v2, v2

    aput-byte v2, v3, v5

    move-object v2, v3

    .line 151
    goto/16 :goto_8

    .line 122
    :cond_d
    const/4 v3, 0x0

    goto :goto_b

    .line 127
    :cond_e
    const/4 v3, 0x0

    goto :goto_c

    .line 129
    :cond_f
    const/4 v3, 0x0

    goto :goto_d
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;[Lsoftware/simplicial/a/bx;S[Lsoftware/simplicial/a/bf;ILsoftware/simplicial/a/am;Lsoftware/simplicial/a/aq;Lsoftware/simplicial/a/ap;Ljava/lang/String;[BZZZZIZZILsoftware/simplicial/a/c/g;[Lsoftware/simplicial/a/bx;Z)[B
    .locals 8

    .prologue
    .line 158
    .line 162
    :try_start_0
    sget-object v2, Lsoftware/simplicial/a/f/bh;->u:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v2}, Lsoftware/simplicial/a/f/co;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;)V

    .line 165
    move-object/from16 v0, p8

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 166
    move/from16 v0, p14

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 167
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 168
    invoke-virtual {p5}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 169
    const/4 v2, 0x0

    .line 170
    invoke-virtual {p6}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v3

    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x3

    int-to-byte v3, v3

    or-int/2addr v2, v3

    int-to-byte v2, v2

    .line 171
    invoke-virtual {p7}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v3

    shl-int/lit8 v3, v3, 0x2

    and-int/lit8 v3, v3, 0xc

    int-to-byte v3, v3

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 172
    if-eqz p10, :cond_0

    const/4 v2, 0x1

    :goto_0
    shl-int/lit8 v2, v2, 0x4

    and-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 173
    if-eqz p11, :cond_1

    const/4 v2, 0x1

    :goto_1
    shl-int/lit8 v2, v2, 0x5

    and-int/lit8 v2, v2, 0x20

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 174
    if-eqz p12, :cond_2

    const/4 v2, 0x1

    :goto_2
    shl-int/lit8 v2, v2, 0x6

    and-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v3, v2

    .line 175
    if-eqz p15, :cond_3

    const/4 v2, 0x1

    :goto_3
    shl-int/lit8 v2, v2, 0x7

    and-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    or-int/2addr v2, v3

    int-to-byte v2, v2

    .line 176
    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 177
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 178
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->b()I

    move-result v5

    .line 179
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 180
    const/4 v3, 0x0

    .line 181
    const/4 v2, 0x0

    move v4, v2

    move v2, v3

    :goto_4
    if-ge v4, p4, :cond_7

    .line 183
    aget-object v3, p3, v4

    iget-boolean v3, v3, Lsoftware/simplicial/a/bf;->aF:Z

    if-nez v3, :cond_4

    if-nez p13, :cond_4

    .line 181
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    .line 172
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 173
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 174
    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 175
    :cond_3
    const/4 v2, 0x0

    goto :goto_3

    .line 186
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 188
    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-boolean v3, v3, Lsoftware/simplicial/a/az;->M:Z

    if-eqz v3, :cond_5

    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->aC:Lsoftware/simplicial/a/az;

    iget-wide v6, v3, Lsoftware/simplicial/a/az;->b:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v3

    :goto_6
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 189
    const/4 v3, 0x0

    .line 190
    aget-object v6, p3, v4

    iget v6, v6, Lsoftware/simplicial/a/bf;->C:I

    shl-int/lit8 v6, v6, 0x0

    and-int/lit8 v6, v6, 0x1f

    int-to-byte v6, v6

    or-int/2addr v3, v6

    int-to-byte v6, v3

    .line 191
    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    if-eqz v3, :cond_6

    aget-object v3, p3, v4

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ak:Lsoftware/simplicial/a/bx;

    iget v3, v3, Lsoftware/simplicial/a/bx;->c:I

    :goto_7
    shl-int/lit8 v3, v3, 0x5

    and-int/lit16 v3, v3, 0xe0

    int-to-byte v3, v3

    or-int/2addr v3, v6

    int-to-byte v3, v3

    .line 192
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    .line 249
    :catch_0
    move-exception v2

    .line 251
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception occurred writing data. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 252
    const/4 v2, 0x0

    .line 257
    :goto_8
    return-object v2

    .line 188
    :cond_5
    const/4 v3, -0x1

    goto :goto_6

    .line 191
    :cond_6
    const/4 v3, 0x7

    goto :goto_7

    .line 196
    :cond_7
    const/4 v3, 0x0

    :goto_9
    const/4 v4, 0x5

    if-ge v3, v4, :cond_9

    .line 198
    :try_start_1
    array-length v4, p1

    if-ge v3, v4, :cond_8

    .line 200
    aget-object v4, p1, v3

    iget v4, v4, Lsoftware/simplicial/a/bx;->c:I

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 201
    aget-object v4, p1, v3

    iget v4, v4, Lsoftware/simplicial/a/bx;->e:I

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->a(I)V

    .line 196
    :goto_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 205
    :cond_8
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 206
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lsoftware/simplicial/a/f/bn;->a(I)V

    goto :goto_a

    .line 210
    :cond_9
    const/4 v4, 0x0

    .line 211
    if-eqz p16, :cond_d

    const/4 v3, 0x1

    :goto_b
    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 212
    const/16 v4, 0x7f

    move/from16 v0, p17

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    and-int/lit16 v4, v4, 0xfe

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 213
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 215
    const/4 v4, 0x0

    .line 216
    if-eqz p13, :cond_e

    const/4 v3, 0x1

    :goto_c
    shl-int/lit8 v3, v3, 0x0

    and-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 217
    invoke-virtual/range {p18 .. p18}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    and-int/lit8 v4, v4, 0x7e

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v4, v3

    .line 218
    if-eqz p20, :cond_f

    const/4 v3, 0x1

    :goto_d
    shl-int/lit8 v3, v3, 0x7

    and-int/lit16 v3, v3, 0x80

    int-to-byte v3, v3

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 219
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 221
    move-object/from16 v0, p9

    array-length v3, v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 222
    move-object/from16 v0, p9

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->write([B)V

    .line 224
    if-eqz p13, :cond_a

    if-nez p15, :cond_a

    .line 226
    const/4 v3, 0x0

    .line 227
    const/4 v4, 0x0

    aget-object v4, p3, v4

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    shl-int/lit8 v4, v4, 0x0

    and-int/lit8 v4, v4, 0xf

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 228
    const/4 v4, 0x1

    aget-object v4, p3, v4

    iget-byte v4, v4, Lsoftware/simplicial/a/bf;->R:B

    shl-int/lit8 v4, v4, 0x4

    and-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 229
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 232
    :cond_a
    sget-object v3, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne p5, v3, :cond_c

    .line 234
    invoke-static {p5, p7}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v4

    .line 235
    const/4 v3, 0x0

    .line 236
    const/4 v6, 0x0

    aget-object v6, p19, v6

    iget v6, v6, Lsoftware/simplicial/a/bx;->d:I

    shl-int/lit8 v6, v6, 0x0

    and-int/lit8 v6, v6, 0xf

    int-to-byte v6, v6

    or-int/2addr v3, v6

    int-to-byte v3, v3

    .line 237
    const/4 v6, 0x1

    aget-object v6, p19, v6

    iget v6, v6, Lsoftware/simplicial/a/bx;->d:I

    shl-int/lit8 v6, v6, 0x4

    and-int/lit16 v6, v6, 0xf0

    int-to-byte v6, v6

    or-int/2addr v3, v6

    int-to-byte v3, v3

    .line 238
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 239
    const/4 v3, 0x2

    if-le v4, v3, :cond_c

    .line 241
    const/4 v3, 0x0

    .line 242
    const/4 v6, 0x2

    aget-object v6, p19, v6

    iget v6, v6, Lsoftware/simplicial/a/bx;->d:I

    shl-int/lit8 v6, v6, 0x0

    and-int/lit8 v6, v6, 0xf

    int-to-byte v6, v6

    or-int/2addr v3, v6

    int-to-byte v3, v3

    .line 243
    const/4 v6, 0x3

    if-le v4, v6, :cond_b

    .line 244
    const/4 v4, 0x3

    aget-object v4, p19, v4

    iget v4, v4, Lsoftware/simplicial/a/bx;->d:I

    shl-int/lit8 v4, v4, 0x4

    and-int/lit16 v4, v4, 0xf0

    int-to-byte v4, v4

    or-int/2addr v3, v4

    int-to-byte v3, v3

    .line 245
    :cond_b
    invoke-virtual {p0, v3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 255
    :cond_c
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v3

    .line 256
    int-to-byte v2, v2

    aput-byte v2, v3, v5

    move-object v2, v3

    .line 257
    goto/16 :goto_8

    .line 211
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 216
    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_c

    .line 218
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_d
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 264
    :try_start_0
    new-instance v3, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v3, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 265
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->i:Ljava/lang/String;

    .line 266
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/co;->k:I

    .line 267
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v2

    iput-short v2, p0, Lsoftware/simplicial/a/f/co;->c:S

    .line 268
    sget-object v2, Lsoftware/simplicial/a/am;->s:[Lsoftware/simplicial/a/am;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    aget-object v2, v2, v4

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->r:Lsoftware/simplicial/a/am;

    .line 269
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 270
    sget-object v2, Lsoftware/simplicial/a/aq;->c:[Lsoftware/simplicial/a/aq;

    and-int/lit8 v5, v4, 0x3

    ushr-int/lit8 v5, v5, 0x0

    int-to-byte v5, v5

    aget-object v2, v2, v5

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->q:Lsoftware/simplicial/a/aq;

    .line 271
    sget-object v2, Lsoftware/simplicial/a/ap;->e:[Lsoftware/simplicial/a/ap;

    and-int/lit8 v5, v4, 0xc

    ushr-int/lit8 v5, v5, 0x2

    int-to-byte v5, v5

    aget-object v2, v2, v5

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->p:Lsoftware/simplicial/a/ap;

    .line 272
    and-int/lit8 v2, v4, 0x10

    ushr-int/lit8 v2, v2, 0x4

    int-to-byte v2, v2

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_0
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->l:Z

    .line 273
    and-int/lit8 v2, v4, 0x20

    ushr-int/lit8 v2, v2, 0x5

    int-to-byte v2, v2

    if-ne v2, v0, :cond_3

    move v2, v0

    :goto_1
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->m:Z

    .line 274
    and-int/lit8 v2, v4, 0x40

    ushr-int/lit8 v2, v2, 0x6

    int-to-byte v2, v2

    if-ne v2, v0, :cond_4

    move v2, v0

    :goto_2
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->n:Z

    .line 275
    and-int/lit16 v2, v4, 0x80

    ushr-int/lit8 v2, v2, 0x7

    int-to-byte v2, v2

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_3
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->o:Z

    .line 277
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput-byte v2, p0, Lsoftware/simplicial/a/f/co;->d:B

    .line 278
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput-byte v2, p0, Lsoftware/simplicial/a/f/co;->e:B

    .line 279
    iget-byte v2, p0, Lsoftware/simplicial/a/f/co;->e:B

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->f:[B

    .line 281
    iget-byte v2, p0, Lsoftware/simplicial/a/f/co;->e:B

    new-array v2, v2, [S

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->h:[S

    .line 282
    iget-byte v2, p0, Lsoftware/simplicial/a/f/co;->e:B

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->g:[B

    move v2, v1

    .line 283
    :goto_4
    iget-byte v4, p0, Lsoftware/simplicial/a/f/co;->e:B

    if-ge v2, v4, :cond_6

    .line 286
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->h:[S

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v5

    aput-short v5, v4, v2

    .line 288
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 289
    iget-object v5, p0, Lsoftware/simplicial/a/f/co;->f:[B

    and-int/lit8 v6, v4, 0x1f

    ushr-int/lit8 v6, v6, 0x0

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    .line 290
    iget-object v5, p0, Lsoftware/simplicial/a/f/co;->g:[B

    and-int/lit16 v4, v4, 0xe0

    ushr-int/lit8 v4, v4, 0x5

    int-to-byte v4, v4

    aput-byte v4, v5, v2

    .line 291
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->g:[B

    aget-byte v4, v4, v2

    if-ltz v4, :cond_0

    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->g:[B

    aget-byte v4, v4, v2

    const/4 v5, 0x4

    if-lt v4, v5, :cond_1

    .line 292
    :cond_0
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->g:[B

    const/4 v5, -0x1

    aput-byte v5, v4, v2

    .line 283
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_2
    move v2, v1

    .line 272
    goto :goto_0

    :cond_3
    move v2, v1

    .line 273
    goto :goto_1

    :cond_4
    move v2, v1

    .line 274
    goto :goto_2

    :cond_5
    move v2, v1

    .line 275
    goto :goto_3

    :cond_6
    move v2, v1

    .line 295
    :goto_5
    const/4 v4, 0x5

    if-ge v2, v4, :cond_7

    .line 297
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->a:[B

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v5

    aput-byte v5, v4, v2

    .line 298
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->b:[I

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->a()I

    move-result v5

    aput v5, v4, v2

    .line 295
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 301
    :cond_7
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 302
    and-int/lit8 v2, v4, 0x1

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    if-ne v2, v0, :cond_a

    move v2, v0

    :goto_6
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->s:Z

    .line 303
    and-int/lit16 v2, v4, 0xfe

    ushr-int/lit8 v2, v2, 0x1

    iput v2, p0, Lsoftware/simplicial/a/f/co;->t:I

    .line 305
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 306
    and-int/lit8 v2, v4, 0x1

    ushr-int/lit8 v2, v2, 0x0

    int-to-byte v2, v2

    if-ne v2, v0, :cond_b

    move v2, v0

    :goto_7
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->u:Z

    .line 307
    and-int/lit8 v2, v4, 0x7e

    ushr-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    invoke-static {v2}, Lsoftware/simplicial/a/c/g;->a(B)Lsoftware/simplicial/a/c/g;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->w:Lsoftware/simplicial/a/c/g;

    .line 308
    and-int/lit16 v2, v4, 0x80

    ushr-int/lit8 v2, v2, 0x7

    int-to-byte v2, v2

    if-ne v2, v0, :cond_c

    move v2, v0

    :goto_8
    iput-boolean v2, p0, Lsoftware/simplicial/a/f/co;->v:Z

    .line 310
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->j:[B

    .line 311
    iget-object v2, p0, Lsoftware/simplicial/a/f/co;->j:[B

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 313
    iget-boolean v2, p0, Lsoftware/simplicial/a/f/co;->u:Z

    if-eqz v2, :cond_8

    iget-boolean v2, p0, Lsoftware/simplicial/a/f/co;->o:Z

    if-nez v2, :cond_8

    .line 315
    const/4 v2, 0x2

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/co;->x:[B

    .line 316
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    .line 317
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->x:[B

    const/4 v5, 0x0

    and-int/lit8 v6, v2, 0xf

    ushr-int/lit8 v6, v6, 0x0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 318
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->x:[B

    const/4 v5, 0x1

    and-int/lit16 v2, v2, 0xf0

    ushr-int/lit8 v2, v2, 0x4

    int-to-byte v2, v2

    aput-byte v2, v4, v5

    .line 321
    :cond_8
    iget-object v2, p0, Lsoftware/simplicial/a/f/co;->r:Lsoftware/simplicial/a/am;

    sget-object v4, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v2, v4, :cond_9

    .line 323
    iget-object v2, p0, Lsoftware/simplicial/a/f/co;->r:Lsoftware/simplicial/a/am;

    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->p:Lsoftware/simplicial/a/ap;

    invoke-static {v2, v4}, Lsoftware/simplicial/a/ai;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v2

    .line 324
    new-array v4, v2, [B

    iput-object v4, p0, Lsoftware/simplicial/a/f/co;->y:[B

    .line 325
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v4

    .line 326
    iget-object v5, p0, Lsoftware/simplicial/a/f/co;->y:[B

    const/4 v6, 0x0

    and-int/lit8 v7, v4, 0xf

    ushr-int/lit8 v7, v7, 0x0

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 327
    iget-object v5, p0, Lsoftware/simplicial/a/f/co;->y:[B

    const/4 v6, 0x1

    and-int/lit16 v4, v4, 0xf0

    ushr-int/lit8 v4, v4, 0x4

    int-to-byte v4, v4

    aput-byte v4, v5, v6

    .line 328
    if-le v2, v8, :cond_9

    .line 330
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 331
    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->y:[B

    const/4 v5, 0x2

    and-int/lit8 v6, v3, 0xf

    ushr-int/lit8 v6, v6, 0x0

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    .line 332
    if-le v2, v9, :cond_9

    .line 333
    iget-object v2, p0, Lsoftware/simplicial/a/f/co;->y:[B

    const/4 v4, 0x3

    and-int/lit16 v3, v3, 0xf0

    ushr-int/lit8 v3, v3, 0x4

    int-to-byte v3, v3

    aput-byte v3, v2, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    :cond_9
    :goto_9
    return v0

    :cond_a
    move v2, v1

    .line 302
    goto/16 :goto_6

    :cond_b
    move v2, v1

    .line 306
    goto/16 :goto_7

    :cond_c
    move v2, v1

    .line 308
    goto/16 :goto_8

    .line 337
    :catch_0
    move-exception v0

    .line 339
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/co;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 340
    goto :goto_9
.end method
