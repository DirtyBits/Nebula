.class public Lsoftware/simplicial/a/f/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:S

.field public e:Ljava/net/InetSocketAddress;

.field public f:Ljava/net/DatagramSocket;

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->a:I

    .line 15
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->b:I

    .line 16
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->c:I

    .line 17
    iput-short v0, p0, Lsoftware/simplicial/a/f/bl;->d:S

    .line 19
    iput-object v1, p0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 20
    iput-object v1, p0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 22
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->g:I

    .line 23
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->h:I

    .line 28
    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/a/f/bl;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->a:I

    .line 15
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->b:I

    .line 16
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->c:I

    .line 17
    iput-short v0, p0, Lsoftware/simplicial/a/f/bl;->d:S

    .line 19
    iput-object v1, p0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 20
    iput-object v1, p0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 22
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->g:I

    .line 23
    iput v0, p0, Lsoftware/simplicial/a/f/bl;->h:I

    .line 32
    iget v0, p1, Lsoftware/simplicial/a/f/bl;->a:I

    iput v0, p0, Lsoftware/simplicial/a/f/bl;->a:I

    .line 33
    iget v0, p1, Lsoftware/simplicial/a/f/bl;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/bl;->b:I

    .line 34
    iget v0, p1, Lsoftware/simplicial/a/f/bl;->c:I

    iput v0, p0, Lsoftware/simplicial/a/f/bl;->c:I

    .line 35
    iget-short v0, p1, Lsoftware/simplicial/a/f/bl;->d:S

    iput-short v0, p0, Lsoftware/simplicial/a/f/bl;->d:S

    .line 37
    iget-object v0, p1, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    iput-object v0, p0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    .line 38
    iget-object v0, p1, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    iput-object v0, p0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    .line 40
    iget v0, p1, Lsoftware/simplicial/a/f/bl;->g:I

    iput v0, p0, Lsoftware/simplicial/a/f/bl;->g:I

    .line 41
    iget v0, p1, Lsoftware/simplicial/a/f/bl;->h:I

    iput v0, p0, Lsoftware/simplicial/a/f/bl;->h:I

    .line 42
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 77
    instance-of v1, p1, Lsoftware/simplicial/a/f/bl;

    if-nez v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lsoftware/simplicial/a/f/bl;->a:I

    check-cast p1, Lsoftware/simplicial/a/f/bl;

    iget v2, p1, Lsoftware/simplicial/a/f/bl;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EP "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/f/bl;->e:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/f/bl;->f:Ljava/net/DatagramSocket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " V "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-short v1, p0, Lsoftware/simplicial/a/f/bl;->d:S

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
