.class public Lsoftware/simplicial/a/f/as;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:S

.field public b:B

.field public c:Lsoftware/simplicial/a/d/c;

.field public d:Lsoftware/simplicial/a/am;

.field public e:Lsoftware/simplicial/a/ap;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lsoftware/simplicial/a/f/bh;->x:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 30
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ISBLsoftware/simplicial/a/d/c;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)[B
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 54
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/f/bh;->x:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v1, p1}, Lsoftware/simplicial/a/f/as;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 55
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 56
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 57
    invoke-virtual {p4}, Lsoftware/simplicial/a/d/c;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 58
    if-nez p5, :cond_0

    move v1, v0

    :goto_0
    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 59
    if-nez p6, :cond_1

    move v1, v0

    :goto_1
    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 60
    if-nez p7, :cond_2

    move v1, v0

    :goto_2
    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 61
    if-nez p8, :cond_4

    move v1, v0

    :goto_3
    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 62
    if-nez p9, :cond_5

    :goto_4
    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_5
    return-object v0

    .line 58
    :cond_0
    :try_start_1
    invoke-virtual {p5}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {p6}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    goto :goto_1

    .line 60
    :cond_2
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 61
    :cond_4
    invoke-virtual {p8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_3

    .line 62
    :cond_5
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto :goto_4

    .line 64
    :catch_0
    move-exception v0

    .line 66
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data.\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 67
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v0, 0x1

    .line 77
    :try_start_0
    iget v2, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v2, p0, Lsoftware/simplicial/a/f/as;->ar:I

    .line 78
    new-instance v2, Lsoftware/simplicial/a/f/bm;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 80
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    iput-short v3, p0, Lsoftware/simplicial/a/f/as;->a:S

    .line 81
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/as;->b:B

    .line 82
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/d/c;->a(B)Lsoftware/simplicial/a/d/c;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->c:Lsoftware/simplicial/a/d/c;

    .line 83
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v3

    if-lez v3, :cond_1

    .line 85
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/am;->a(B)Lsoftware/simplicial/a/am;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->d:Lsoftware/simplicial/a/am;

    .line 86
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/ap;->a(B)Lsoftware/simplicial/a/ap;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->e:Lsoftware/simplicial/a/ap;

    .line 87
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    .line 88
    if-nez v3, :cond_2

    .line 89
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->f:Ljava/lang/Boolean;

    .line 94
    :goto_0
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->g:Ljava/lang/Integer;

    .line 95
    iget-object v3, p0, Lsoftware/simplicial/a/f/as;->g:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 96
    const/4 v3, 0x0

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->g:Ljava/lang/Integer;

    .line 97
    :cond_0
    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/as;->h:Ljava/lang/Integer;

    .line 98
    iget-object v2, p0, Lsoftware/simplicial/a/f/as;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 99
    const/4 v2, 0x0

    iput-object v2, p0, Lsoftware/simplicial/a/f/as;->h:Ljava/lang/Integer;

    .line 108
    :cond_1
    :goto_1
    return v0

    .line 90
    :cond_2
    if-ne v3, v0, :cond_3

    .line 91
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->f:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    :catch_0
    move-exception v0

    .line 104
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/as;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v0, v1

    .line 105
    goto :goto_1

    .line 93
    :cond_3
    const/4 v3, 0x0

    :try_start_1
    iput-object v3, p0, Lsoftware/simplicial/a/f/as;->f:Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
