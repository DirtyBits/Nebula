.class public Lsoftware/simplicial/a/f/w;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:B

.field public g:S

.field public h:Z

.field public i:Lsoftware/simplicial/a/e;

.field public j:Ljava/lang/String;

.field public k:[B

.field public l:Lsoftware/simplicial/a/af;

.field public m:I

.field public n:B

.field public o:I

.field public p:Ljava/lang/String;

.field public q:Lsoftware/simplicial/a/as;

.field public r:F

.field public s:Ljava/lang/Object;

.field public t:Lsoftware/simplicial/a/f/bj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/a/f/w;->r:F

    .line 47
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IIIILjava/lang/String;Lsoftware/simplicial/a/x;SZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)[B
    .locals 5

    .prologue
    .line 55
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lsoftware/simplicial/a/f/w;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 57
    invoke-virtual {p0, p1}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 58
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 59
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 60
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 61
    if-nez p5, :cond_0

    const-string p5, ""

    :cond_0
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p6}, Lsoftware/simplicial/a/x;->ordinal()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 63
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 64
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeBoolean(Z)V

    .line 65
    invoke-virtual/range {p11 .. p11}, Lsoftware/simplicial/a/e;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 66
    invoke-virtual {p0, p9}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 67
    move-object/from16 v0, p12

    iget-byte v1, v0, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 68
    move/from16 v0, p13

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 69
    array-length v1, p10

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 70
    const/4 v1, 0x0

    array-length v2, p10

    invoke-virtual {p0, p10, v1, v2}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 71
    move/from16 v0, p14

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 72
    move/from16 v0, p15

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 73
    move-object/from16 v0, p16

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 74
    move-object/from16 v0, p17

    iget-byte v1, v0, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v1

    :goto_0
    return-object v1

    .line 76
    :catch_0
    move-exception v1

    .line 78
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred writing data. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 79
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 89
    :try_start_0
    move-object v0, p1

    check-cast v0, Lsoftware/simplicial/a/f/bj;

    move-object v1, v0

    iput-object v1, p0, Lsoftware/simplicial/a/f/w;->t:Lsoftware/simplicial/a/f/bj;

    .line 90
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v3}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 92
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->a:I

    .line 93
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->b:I

    .line 94
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->c:I

    .line 95
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->d:I

    .line 96
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->e:Ljava/lang/String;

    .line 97
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/w;->f:B

    .line 98
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    iput-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    .line 99
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readBoolean()Z

    move-result v3

    iput-boolean v3, p0, Lsoftware/simplicial/a/f/w;->h:Z

    .line 100
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0xfb

    if-le v3, v4, :cond_0

    .line 102
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/e;->a(S)Lsoftware/simplicial/a/e;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->i:Lsoftware/simplicial/a/e;

    .line 103
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->j:Ljava/lang/String;

    .line 110
    :goto_0
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0x119

    if-lt v3, v4, :cond_1

    .line 112
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->l:Lsoftware/simplicial/a/af;

    .line 113
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->m:I

    .line 120
    :goto_1
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0x13f

    if-lt v3, v4, :cond_3

    .line 122
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->k:[B

    .line 123
    iget-object v3, p0, Lsoftware/simplicial/a/f/w;->k:[B

    array-length v3, v3

    const/16 v4, 0x10

    if-le v3, v4, :cond_2

    .line 124
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :catch_0
    move-exception v1

    .line 152
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception occurred parsing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lsoftware/simplicial/a/f/w;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move v1, v2

    .line 156
    :goto_2
    return v1

    .line 107
    :cond_0
    :try_start_1
    sget-object v3, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->i:Lsoftware/simplicial/a/e;

    .line 108
    const-string v3, "NULL"

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->j:Ljava/lang/String;

    goto :goto_0

    .line 117
    :cond_1
    sget-object v3, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->l:Lsoftware/simplicial/a/af;

    .line 118
    const/4 v3, 0x0

    iput v3, p0, Lsoftware/simplicial/a/f/w;->m:I

    goto :goto_1

    .line 125
    :cond_2
    iget-object v3, p0, Lsoftware/simplicial/a/f/w;->k:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lsoftware/simplicial/a/f/w;->k:[B

    array-length v5, v5

    invoke-virtual {v1, v3, v4, v5}, Lsoftware/simplicial/a/f/bm;->readFully([BII)V

    .line 131
    :goto_3
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0x177

    if-lt v3, v4, :cond_4

    .line 132
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v3

    iput-byte v3, p0, Lsoftware/simplicial/a/f/w;->n:B

    .line 135
    :goto_4
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0x17d

    if-lt v3, v4, :cond_5

    .line 136
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v3

    iput v3, p0, Lsoftware/simplicial/a/f/w;->o:I

    .line 139
    :goto_5
    iget-short v3, p0, Lsoftware/simplicial/a/f/w;->g:S

    const/16 v4, 0x17f

    if-lt v3, v4, :cond_6

    .line 141
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->p:Ljava/lang/String;

    .line 142
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/w;->q:Lsoftware/simplicial/a/as;

    .line 156
    :goto_6
    const/4 v1, 0x1

    goto :goto_2

    .line 129
    :cond_3
    const/4 v3, 0x0

    new-array v3, v3, [B

    iput-object v3, p0, Lsoftware/simplicial/a/f/w;->k:[B

    goto :goto_3

    .line 134
    :cond_4
    sget-object v3, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v3, v3, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v3, p0, Lsoftware/simplicial/a/f/w;->n:B

    goto :goto_4

    .line 138
    :cond_5
    const/4 v3, 0x0

    iput v3, p0, Lsoftware/simplicial/a/f/w;->o:I

    goto :goto_5

    .line 146
    :cond_6
    const-string v1, ""

    iput-object v1, p0, Lsoftware/simplicial/a/f/w;->p:Ljava/lang/String;

    .line 147
    sget-object v1, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v1, p0, Lsoftware/simplicial/a/f/w;->q:Lsoftware/simplicial/a/as;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6
.end method

.method public a(Lsoftware/simplicial/a/f/bn;)[B
    .locals 4

    .prologue
    .line 251
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lsoftware/simplicial/a/f/w;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 253
    iget v0, p0, Lsoftware/simplicial/a/f/w;->a:I

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 254
    iget-short v0, p0, Lsoftware/simplicial/a/f/w;->g:S

    invoke-virtual {p1, v0}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 257
    iget-object v0, p0, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    check-cast v0, Lsoftware/simplicial/a/v;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/v;->a(Lsoftware/simplicial/a/f/bn;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    invoke-virtual {p1}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 259
    :catch_0
    move-exception v0

    .line 261
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 262
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 229
    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lsoftware/simplicial/a/f/w;->ar:I

    .line 230
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 232
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/w;->a:I

    .line 233
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v2

    iput-short v2, p0, Lsoftware/simplicial/a/f/w;->g:S

    .line 235
    invoke-static {v1}, Lsoftware/simplicial/a/v;->a(Lsoftware/simplicial/a/f/bm;)Lsoftware/simplicial/a/v;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 237
    :catch_0
    move-exception v1

    .line 239
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/w;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsoftware/simplicial/a/f/w;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " wrapper "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/f/w;->t:Lsoftware/simplicial/a/f/bj;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " data "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
