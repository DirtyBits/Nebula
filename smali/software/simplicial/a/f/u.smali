.class public Lsoftware/simplicial/a/f/u;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[B

.field public c:Lsoftware/simplicial/a/e;

.field public d:Lsoftware/simplicial/a/af;

.field public e:I

.field public f:I

.field public g:B

.field public h:S

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Lsoftware/simplicial/a/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 33
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;ILsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILjava/lang/String;[BIBSILjava/lang/String;Lsoftware/simplicial/a/as;)[B
    .locals 5

    .prologue
    .line 40
    :try_start_0
    sget-object v1, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v1, p1}, Lsoftware/simplicial/a/f/u;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 41
    invoke-virtual {p2}, Lsoftware/simplicial/a/e;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 42
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p7}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 44
    iget-byte v1, p3, Lsoftware/simplicial/a/af;->c:B

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 45
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 46
    array-length v1, p6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 47
    const/4 v1, 0x0

    array-length v2, p6

    invoke-virtual {p0, p6, v1, v2}, Lsoftware/simplicial/a/f/bn;->write([BII)V

    .line 48
    invoke-virtual {p0, p8}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V

    .line 49
    invoke-virtual {p0, p9}, Lsoftware/simplicial/a/f/bn;->writeShort(I)V

    .line 50
    invoke-virtual {p0, p10}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 51
    move-object/from16 v0, p11

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 52
    move-object/from16 v0, p12

    iget-byte v1, v0, Lsoftware/simplicial/a/as;->c:B

    invoke-virtual {p0, v1}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v1

    :goto_0
    return-object v1

    .line 54
    :catch_0
    move-exception v1

    .line 56
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred writing data.\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 57
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 67
    :try_start_0
    iget v1, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v1, p0, Lsoftware/simplicial/a/f/u;->ar:I

    .line 68
    new-instance v1, Lsoftware/simplicial/a/f/bm;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 69
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v2

    .line 70
    if-ltz v2, :cond_0

    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 71
    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    aget-object v2, v3, v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    .line 74
    :goto_0
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->a:Ljava/lang/String;

    .line 75
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/u;->f:I

    .line 77
    sget-object v2, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    .line 78
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/u;->e:I

    .line 79
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->b:[B

    .line 80
    sget-object v2, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iget-byte v2, v2, Lsoftware/simplicial/a/bd;->c:B

    iput-byte v2, p0, Lsoftware/simplicial/a/f/u;->g:B

    .line 81
    const/4 v2, 0x0

    iput-short v2, p0, Lsoftware/simplicial/a/f/u;->h:S

    .line 82
    const/4 v2, 0x0

    iput v2, p0, Lsoftware/simplicial/a/f/u;->j:I

    .line 83
    sget-object v2, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->k:Lsoftware/simplicial/a/as;

    .line 84
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 86
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->d:Lsoftware/simplicial/a/af;

    .line 87
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/u;->e:I

    .line 89
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 91
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->b:[B

    .line 92
    iget-object v2, p0, Lsoftware/simplicial/a/f/u;->b:[B

    array-length v2, v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_1

    .line 93
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "INVALID ALIAS COLORS LENGTH!"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :catch_0
    move-exception v1

    .line 115
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred parsing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/a/f/u;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 119
    :goto_1
    return v0

    .line 73
    :cond_0
    :try_start_1
    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->c:Lsoftware/simplicial/a/e;

    goto :goto_0

    .line 94
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/a/f/u;->b:[B

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/f/bm;->readFully([B)V

    .line 96
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 98
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v2

    iput-byte v2, p0, Lsoftware/simplicial/a/f/u;->g:B

    .line 99
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readShort()S

    move-result v2

    iput-short v2, p0, Lsoftware/simplicial/a/f/u;->h:S

    .line 100
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 102
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/a/f/u;->j:I

    .line 103
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v2

    if-lez v2, :cond_2

    .line 105
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lsoftware/simplicial/a/f/u;->i:Ljava/lang/String;

    .line 106
    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v1

    invoke-static {v1}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/u;->k:Lsoftware/simplicial/a/as;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 119
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
