.class public Lsoftware/simplicial/a/f/ae;
.super Lsoftware/simplicial/a/f/be;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Lsoftware/simplicial/a/p;

.field public f:I

.field public g:Lsoftware/simplicial/a/v;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    sget-object v0, Lsoftware/simplicial/a/f/bh;->O:Lsoftware/simplicial/a/f/bh;

    invoke-direct {p0, v0}, Lsoftware/simplicial/a/f/be;-><init>(Lsoftware/simplicial/a/f/bh;)V

    .line 23
    iput v1, p0, Lsoftware/simplicial/a/f/ae;->f:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/ae;->g:Lsoftware/simplicial/a/v;

    .line 25
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/ae;->h:Z

    .line 30
    return-void
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IIILjava/lang/String;I)[B
    .locals 4

    .prologue
    .line 51
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->O:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ae;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 52
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 53
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 54
    invoke-virtual {p0, p4}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0, p5}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 56
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 58
    :catch_0
    move-exception v0

    .line 60
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 61
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/f/bn;IILsoftware/simplicial/a/p;)[B
    .locals 4

    .prologue
    .line 71
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->O:Lsoftware/simplicial/a/f/bh;

    invoke-static {p0, v0, p1}, Lsoftware/simplicial/a/f/ae;->a(Lsoftware/simplicial/a/f/bn;Lsoftware/simplicial/a/f/bh;I)V

    .line 72
    invoke-virtual {p0, p2}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 73
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 74
    const-string v0, ""

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeUTF(Ljava/lang/String;)V

    .line 75
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeInt(I)V

    .line 76
    invoke-virtual {p3}, Lsoftware/simplicial/a/p;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/f/bn;->writeByte(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    invoke-virtual {p0}, Lsoftware/simplicial/a/f/bn;->a()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred writing data. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 81
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/f/bi;)Z
    .locals 4

    .prologue
    .line 91
    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/bi;->b:I

    iput v0, p0, Lsoftware/simplicial/a/f/ae;->ar:I

    .line 92
    new-instance v0, Lsoftware/simplicial/a/f/bm;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p1, Lsoftware/simplicial/a/f/bi;->c:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/f/bm;-><init>(Ljava/io/InputStream;)V

    .line 94
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ae;->a:I

    .line 95
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ae;->b:I

    .line 96
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/a/f/ae;->c:Ljava/lang/String;

    .line 97
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readInt()I

    move-result v1

    iput v1, p0, Lsoftware/simplicial/a/f/ae;->d:I

    .line 98
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->available()I

    move-result v1

    if-lez v1, :cond_0

    .line 99
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bm;->readByte()B

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/p;->a(I)Lsoftware/simplicial/a/p;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/a/f/ae;->e:Lsoftware/simplicial/a/p;

    .line 109
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/f/ae;->e:Lsoftware/simplicial/a/p;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occurred parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/a/f/ae;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 106
    const/4 v0, 0x0

    goto :goto_1
.end method
