.class Lsoftware/simplicial/a/f/cv$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/f/cv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field final synthetic c:Lsoftware/simplicial/a/f/cv;

.field private d:Ljava/net/DatagramSocket;


# direct methods
.method constructor <init>(Lsoftware/simplicial/a/f/cv;Ljava/net/DatagramSocket;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 711
    iput-object p1, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 706
    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    .line 707
    iput-boolean v0, p0, Lsoftware/simplicial/a/f/cv$a;->b:Z

    .line 712
    iput-object p2, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    .line 713
    return-void
.end method

.method private a(Ljava/net/DatagramSocket;Ljava/lang/Exception;)Ljava/net/DatagramSocket;
    .locals 13

    .prologue
    .line 850
    new-instance v4, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bn;-><init>()V

    .line 851
    const/4 v2, 0x0

    .line 852
    invoke-virtual {p1}, Ljava/net/DatagramSocket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 856
    const/4 v1, 0x0

    .line 857
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 860
    :cond_0
    iget-boolean v3, p0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    if-eqz v3, :cond_3

    .line 900
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_5

    .line 901
    :cond_2
    const/4 v2, 0x0

    .line 905
    :goto_1
    return-object v2

    .line 865
    :cond_3
    :try_start_0
    invoke-virtual {p1}, Ljava/net/DatagramSocket;->close()V

    .line 866
    const-wide/16 v8, 0x3e8

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 867
    const/4 v3, 0x1

    .line 875
    :goto_2
    if-eqz v3, :cond_4

    .line 879
    :try_start_1
    sget-object v3, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Reinstating socket"

    invoke-static {v3, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 881
    iget-object v3, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v3}, Lsoftware/simplicial/a/f/cv;->A(Lsoftware/simplicial/a/f/cv;)I

    move-result v3

    iget-object v5, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v5}, Lsoftware/simplicial/a/f/cv;->B(Lsoftware/simplicial/a/f/cv;)I

    move-result v5

    iget-object v8, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v8}, Lsoftware/simplicial/a/f/cv;->z(Lsoftware/simplicial/a/f/cv;)I

    move-result v8

    invoke-static {v4, v3, v5, v8}, Lsoftware/simplicial/a/f/bd;->a(Lsoftware/simplicial/a/f/bn;III)[B

    move-result-object v5

    .line 883
    new-instance v3, Ljava/net/DatagramSocket;

    invoke-direct {v3}, Ljava/net/DatagramSocket;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 884
    const/high16 v2, 0x10000

    :try_start_2
    invoke-virtual {v3, v2}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V

    .line 885
    const/high16 v2, 0x10000

    invoke-virtual {v3, v2}, Ljava/net/DatagramSocket;->setSendBufferSize(I)V

    .line 886
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    iget-object v8, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v8}, Lsoftware/simplicial/a/f/cv;->w(Lsoftware/simplicial/a/f/cv;)I

    move-result v8

    invoke-virtual {v3, v2, v8}, Ljava/net/DatagramSocket;->connect(Ljava/net/InetAddress;I)V

    .line 887
    new-instance v2, Ljava/net/DatagramPacket;

    array-length v8, v5

    invoke-direct {v2, v5, v8}, Ljava/net/DatagramPacket;-><init>([BI)V

    invoke-virtual {v3, v2}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 889
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v5, "Successfully reinstated socket."

    invoke-static {v2, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 890
    const/4 v1, 0x1

    move-object v2, v3

    .line 898
    :cond_4
    :goto_3
    if-nez v1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x1388

    cmp-long v3, v8, v10

    if-ltz v3, :cond_0

    goto :goto_0

    .line 869
    :catch_0
    move-exception v3

    .line 871
    const/4 v3, 0x0

    goto :goto_2

    .line 892
    :catch_1
    move-exception v2

    .line 894
    :goto_4
    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v8, "Unexpected exception occured while reinstating socket."

    invoke-static {v5, v8, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v2, v3

    goto :goto_3

    .line 903
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v0, p1, v2}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/cv;Ljava/net/DatagramSocket;Ljava/net/DatagramSocket;)V

    goto :goto_1

    .line 892
    :catch_2
    move-exception v3

    move-object v12, v3

    move-object v3, v2

    move-object v2, v12

    goto :goto_4
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 718
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 720
    const/16 v0, 0x59d

    new-array v4, v0, [B

    .line 723
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    if-nez v0, :cond_2

    .line 727
    :try_start_0
    new-instance v0, Ljava/net/DatagramPacket;

    array-length v2, v4

    invoke-direct {v0, v4, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 730
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v2, v0}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 731
    invoke-virtual {v0}, Ljava/net/DatagramPacket;->getLength()I

    move-result v5

    .line 732
    if-lt v5, v3, :cond_1

    const/16 v0, 0x59c

    if-le v5, v0, :cond_3

    .line 734
    :cond_1
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received invalid sized packet: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 826
    :catch_0
    move-exception v0

    .line 828
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected exception occurred while reading data input stream for socket "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 830
    iget-boolean v2, p0, Lsoftware/simplicial/a/f/cv$a;->a:Z

    if-eqz v2, :cond_a

    .line 846
    :cond_2
    return-void

    .line 738
    :cond_3
    const/4 v0, 0x0

    :try_start_1
    aget-byte v6, v4, v0

    .line 759
    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    .line 760
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    .line 761
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    .line 762
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->U:Lsoftware/simplicial/a/f/bh;

    .line 763
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    .line 764
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    .line 765
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->K:Lsoftware/simplicial/a/f/bh;

    .line 766
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->z:Lsoftware/simplicial/a/f/bh;

    .line 767
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v6, v0, :cond_4

    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 768
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v6, v0, :cond_5

    .line 770
    :cond_4
    const/4 v2, 0x5

    .line 771
    const/4 v0, 0x1

    aget-byte v0, v4, v0

    shl-int/lit8 v0, v0, 0x18

    const/high16 v7, -0x1000000

    and-int/2addr v0, v7

    const/4 v7, 0x2

    aget-byte v7, v4, v7

    shl-int/lit8 v7, v7, 0x10

    const/high16 v8, 0xff0000

    and-int/2addr v7, v8

    add-int/2addr v0, v7

    const/4 v7, 0x3

    aget-byte v7, v4, v7

    shl-int/lit8 v7, v7, 0x8

    const v8, 0xff00

    and-int/2addr v7, v8

    add-int/2addr v0, v7

    const/4 v7, 0x4

    aget-byte v7, v4, v7

    shl-int/lit8 v7, v7, 0x0

    and-int/lit16 v7, v7, 0xff

    add-int/2addr v0, v7

    .line 778
    :goto_1
    sub-int v7, v5, v2

    new-array v7, v7, [B

    .line 779
    const/4 v8, 0x0

    sub-int/2addr v5, v2

    invoke-static {v4, v2, v7, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 784
    sget-object v2, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    if-ne v6, v2, :cond_6

    .line 786
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v6, v0, v7}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 787
    new-instance v0, Lsoftware/simplicial/a/f/y;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/y;-><init>()V

    .line 788
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/y;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 789
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2, v0}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/y;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 841
    :catch_1
    move-exception v0

    .line 843
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected exception occurred while reading data input stream for socket "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    move v2, v3

    .line 775
    goto :goto_1

    .line 791
    :cond_6
    :try_start_2
    sget-object v2, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    if-ne v6, v2, :cond_7

    .line 793
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v6, v0, v7}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 794
    new-instance v0, Lsoftware/simplicial/a/f/x;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/x;-><init>()V

    .line 795
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/x;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 796
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v2, v0}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/x;)V

    goto/16 :goto_0

    .line 798
    :cond_7
    sget-object v2, Lsoftware/simplicial/a/f/bh;->o:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    if-ne v6, v2, :cond_8

    .line 800
    new-instance v0, Lsoftware/simplicial/a/f/bi;

    const/4 v2, 0x0

    invoke-direct {v0, v6, v2, v7}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 801
    new-instance v2, Lsoftware/simplicial/a/f/bx;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bx;-><init>()V

    .line 802
    invoke-virtual {v2, v0}, Lsoftware/simplicial/a/f/bx;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v0, v2}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/cv;Lsoftware/simplicial/a/f/bx;)V

    goto/16 :goto_0

    .line 805
    :cond_8
    sget-object v2, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v2

    if-ne v6, v2, :cond_9

    .line 807
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v6, v0, v7}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 808
    new-instance v0, Lsoftware/simplicial/a/f/ac;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/ac;-><init>()V

    .line 809
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/ac;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 810
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/a/f/cv;->a(Lsoftware/simplicial/a/f/ac;)V

    goto/16 :goto_0

    .line 814
    :cond_9
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v6, v0, v7}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 818
    :try_start_3
    iget-object v0, p0, Lsoftware/simplicial/a/f/cv$a;->c:Lsoftware/simplicial/a/f/cv;

    invoke-static {v0}, Lsoftware/simplicial/a/f/cv;->D(Lsoftware/simplicial/a/f/cv;)Lsoftware/simplicial/a/f/cw;

    move-result-object v0

    invoke-interface {v0, v2}, Lsoftware/simplicial/a/f/cw;->a(Lsoftware/simplicial/a/f/bi;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 820
    :catch_2
    move-exception v0

    .line 822
    :try_start_4
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error routing message "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 833
    :cond_a
    iput-boolean v3, p0, Lsoftware/simplicial/a/f/cv$a;->b:Z

    .line 834
    iget-object v2, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    invoke-direct {p0, v2, v0}, Lsoftware/simplicial/a/f/cv$a;->a(Ljava/net/DatagramSocket;Ljava/lang/Exception;)Ljava/net/DatagramSocket;

    move-result-object v0

    .line 835
    if-eqz v0, :cond_2

    .line 836
    iput-object v0, p0, Lsoftware/simplicial/a/f/cv$a;->d:Ljava/net/DatagramSocket;

    .line 839
    iput-boolean v1, p0, Lsoftware/simplicial/a/f/cv$a;->b:Z

    goto/16 :goto_0
.end method
