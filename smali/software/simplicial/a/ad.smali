.class public Lsoftware/simplicial/a/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:F


# instance fields
.field b:I

.field c:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const v0, 0x3b03126f    # 0.002f

    sput v0, Lsoftware/simplicial/a/ad;->a:F

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    int-to-float v0, p1

    iput v0, p0, Lsoftware/simplicial/a/ad;->c:F

    .line 31
    const/16 v0, 0x28

    iput v0, p0, Lsoftware/simplicial/a/ad;->b:I

    .line 32
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 47
    iget v0, p0, Lsoftware/simplicial/a/ad;->b:I

    if-lez v0, :cond_1

    .line 48
    iget v0, p0, Lsoftware/simplicial/a/ad;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/ad;->b:I

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/ad;->c:F

    const v1, 0x3ccccccd    # 0.025f

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ad;->c:F

    .line 52
    iget v0, p0, Lsoftware/simplicial/a/ad;->c:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 53
    iput v2, p0, Lsoftware/simplicial/a/ad;->c:F

    goto :goto_0
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x41f00000    # 30.0f

    .line 36
    const/16 v0, 0x3c

    iput v0, p0, Lsoftware/simplicial/a/ad;->b:I

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    sget v1, Lsoftware/simplicial/a/ad;->a:F

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    .line 38
    iget v1, p0, Lsoftware/simplicial/a/ad;->c:F

    mul-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ad;->c:F

    .line 39
    iget v0, p0, Lsoftware/simplicial/a/ad;->c:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 40
    iput v2, p0, Lsoftware/simplicial/a/ad;->c:F

    .line 43
    :cond_0
    return-void
.end method
