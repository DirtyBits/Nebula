.class public Lsoftware/simplicial/a/ba;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final a:[C

.field public static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/ba;->a:[C

    .line 11
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/a/ba;->b:Z

    return-void
.end method

.method public static a(J)I
    .locals 4

    .prologue
    .line 129
    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    .line 130
    const/4 v0, -0x1

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v0, p0, v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 133
    if-gez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Integer;)I
    .locals 4

    .prologue
    .line 198
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 199
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, 0xff00

    and-int/2addr v1, v2

    shr-int/lit8 v1, v1, 0x8

    .line 200
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    shr-int/lit8 v2, v2, 0x10

    .line 201
    add-int/lit8 v0, v0, 0x1e

    .line 202
    add-int/lit8 v1, v1, 0x1e

    .line 203
    add-int/lit8 v2, v2, 0x1e

    .line 204
    const/high16 v3, -0x1000000

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 15
    if-nez p0, :cond_1

    .line 26
    :cond_0
    :goto_0
    return v1

    .line 18
    :cond_1
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 19
    array-length v0, v2

    const/4 v3, 0x4

    if-lt v0, v3, :cond_0

    .line 22
    array-length v0, v2

    new-array v3, v0, [I

    move v0, v1

    .line 23
    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 24
    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v0

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 26
    :cond_2
    aget v0, v3, v1

    and-int/lit16 v0, v0, 0xff

    const v1, 0xff00

    const/4 v2, 0x1

    aget v2, v3, v2

    shl-int/lit8 v2, v2, 0x8

    and-int/2addr v1, v2

    add-int/2addr v0, v1

    const/high16 v1, 0xff0000

    const/4 v2, 0x2

    aget v2, v3, v2

    shl-int/lit8 v2, v2, 0x10

    and-int/2addr v1, v2

    add-int/2addr v0, v1

    const/high16 v1, -0x1000000

    const/4 v2, 0x3

    aget v2, v3, v2

    shl-int/lit8 v2, v2, 0x18

    and-int/2addr v1, v2

    add-int/2addr v1, v0

    goto :goto_0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    new-array v0, v2, [I

    .line 35
    ushr-int/lit8 v1, p0, 0x0

    and-int/lit16 v1, v1, 0xff

    aput v1, v0, v4

    .line 36
    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    aput v1, v0, v5

    .line 37
    ushr-int/lit8 v1, p0, 0x10

    and-int/lit16 v1, v1, 0xff

    aput v1, v0, v6

    .line 38
    ushr-int/lit8 v1, p0, 0x18

    and-int/lit16 v1, v1, 0xff

    aput v1, v0, v7

    .line 40
    const-string v1, "%d.%d.%d.%d"

    new-array v2, v2, [Ljava/lang/Object;

    aget v3, v0, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    aget v3, v0, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    aget v3, v0, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    aget v0, v0, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    :try_start_0
    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 98
    const-string v0, "ERROR"

    goto :goto_0
.end method

.method public static a([BI)Ljava/lang/String;
    .locals 6

    .prologue
    .line 45
    mul-int/lit8 v0, p1, 0x3

    new-array v1, v0, [C

    .line 46
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 48
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    .line 49
    mul-int/lit8 v3, v0, 0x3

    sget-object v4, Lsoftware/simplicial/a/ba;->a:[C

    ushr-int/lit8 v5, v2, 0x4

    aget-char v4, v4, v5

    aput-char v4, v1, v3

    .line 50
    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Lsoftware/simplicial/a/ba;->a:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v4, v2

    aput-char v2, v1, v3

    .line 51
    mul-int/lit8 v2, v0, 0x3

    add-int/lit8 v2, v2, 0x2

    const/16 v3, 0x20

    aput-char v3, v1, v2

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, p2, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, p1, :cond_1

    .line 74
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "\n"

    .line 75
    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u00e1\u2026\u00a0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "\u00e1\u2026\u00a0"

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ".*(?:(?:\\.|dot|d0t)\\W*(?:aero|asia|biz|cat|com|coop|info|int|jobs|mobi|museum|name|net|org|post|pro|tel|travel|xxx|edu|gov|mil|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\\b).*"

    .line 76
    invoke-virtual {p0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 73
    :cond_1
    return v0
.end method

.method public static b(J)D
    .locals 4

    .prologue
    .line 147
    long-to-double v0, p0

    const-wide v2, 0x414b774000000000L    # 3600000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(I)J
    .locals 6

    .prologue
    .line 140
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 141
    const-wide/16 v0, -0x1

    .line 142
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    int-to-long v2, p0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 58
    const/16 v1, 0x10

    invoke-static {p0, v0, v1}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lsoftware/simplicial/a/by;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(I)I
    .locals 5

    .prologue
    const/16 v0, 0xe1

    const/16 v1, 0x82

    .line 209
    and-int/lit16 v4, p0, 0xff

    .line 210
    const v2, 0xff00

    and-int/2addr v2, p0

    shr-int/lit8 v3, v2, 0x8

    .line 211
    const/high16 v2, 0xff0000

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0x10

    .line 212
    if-ge v4, v1, :cond_3

    if-ge v3, v1, :cond_3

    if-ge v2, v1, :cond_3

    move v2, v1

    move v3, v1

    .line 218
    :goto_0
    if-le v3, v0, :cond_0

    move v3, v0

    .line 220
    :cond_0
    if-le v2, v0, :cond_1

    move v2, v0

    .line 222
    :cond_1
    if-le v1, v0, :cond_2

    .line 224
    :goto_1
    const/high16 v1, -0x1000000

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v1, v2

    move v2, v3

    move v3, v4

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 63
    const/16 v1, 0x10

    invoke-static {p0, v0, v1}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lsoftware/simplicial/a/bs;->X:Ljava/util/Set;

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lsoftware/simplicial/a/by;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(I)I
    .locals 4

    .prologue
    .line 229
    and-int/lit16 v0, p0, 0xff

    .line 230
    const v1, 0xff00

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x8

    .line 231
    const/high16 v2, 0xff0000

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0x10

    .line 232
    const/high16 v3, -0x1000000

    and-int/2addr v3, p0

    shr-int/lit8 v3, v3, 0x18

    .line 233
    shl-int/lit8 v3, v3, 0x18

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    return v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 68
    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lsoftware/simplicial/a/by;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 82
    const/16 v0, 0xa

    const/16 v1, 0x5f

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_0
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 87
    :cond_1
    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    const-string v0, "[^\\x00-\\x7F]"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    const-string v1, ".*(?:(?:\\.|dot|d0t)\\W*(?:aero|asia|biz|cat|club|com|coop|info|int|jobs|mobi|museum|name|net|org|post|pro|tel|travel|xxx|edu|gov|mil|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\\b).*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const-string p0, "Message Deleted"

    .line 107
    :cond_0
    return-object p0
.end method
