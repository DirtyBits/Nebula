.class public final enum Lsoftware/simplicial/a/h/a$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/a/h/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/h/a$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/h/a$a;

.field public static final enum b:Lsoftware/simplicial/a/h/a$a;

.field public static final enum c:Lsoftware/simplicial/a/h/a$a;

.field public static final enum d:Lsoftware/simplicial/a/h/a$a;

.field public static final enum e:Lsoftware/simplicial/a/h/a$a;

.field public static final enum f:Lsoftware/simplicial/a/h/a$a;

.field public static final g:[Lsoftware/simplicial/a/h/a$a;

.field private static final synthetic h:[Lsoftware/simplicial/a/h/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 78
    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "VALIDATING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->b:Lsoftware/simplicial/a/h/a$a;

    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "FORMING"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->c:Lsoftware/simplicial/a/h/a$a;

    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->d:Lsoftware/simplicial/a/h/a$a;

    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "COMPETING"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->e:Lsoftware/simplicial/a/h/a$a;

    new-instance v0, Lsoftware/simplicial/a/h/a$a;

    const-string v1, "DONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/h/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    .line 76
    const/4 v0, 0x6

    new-array v0, v0, [Lsoftware/simplicial/a/h/a$a;

    sget-object v1, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/h/a$a;->b:Lsoftware/simplicial/a/h/a$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/h/a$a;->c:Lsoftware/simplicial/a/h/a$a;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/h/a$a;->d:Lsoftware/simplicial/a/h/a$a;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/h/a$a;->e:Lsoftware/simplicial/a/h/a$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->h:[Lsoftware/simplicial/a/h/a$a;

    .line 79
    invoke-static {}, Lsoftware/simplicial/a/h/a$a;->values()[Lsoftware/simplicial/a/h/a$a;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/h/a$a;->g:[Lsoftware/simplicial/a/h/a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/a$a;
    .locals 1

    .prologue
    .line 76
    const-class v0, Lsoftware/simplicial/a/h/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/h/a$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/h/a$a;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lsoftware/simplicial/a/h/a$a;->h:[Lsoftware/simplicial/a/h/a$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/h/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/h/a$a;

    return-object v0
.end method
