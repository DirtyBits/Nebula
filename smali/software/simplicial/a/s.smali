.class public final enum Lsoftware/simplicial/a/s;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/s;

.field public static final enum b:Lsoftware/simplicial/a/s;

.field public static final enum c:Lsoftware/simplicial/a/s;

.field public static d:[Lsoftware/simplicial/a/s;

.field private static final synthetic e:[Lsoftware/simplicial/a/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/s;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    new-instance v0, Lsoftware/simplicial/a/s;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    new-instance v0, Lsoftware/simplicial/a/s;

    const-string v1, "ULTRA"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/a/s;

    sget-object v1, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/a/s;->e:[Lsoftware/simplicial/a/s;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/s;->values()[Lsoftware/simplicial/a/s;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/s;->d:[Lsoftware/simplicial/a/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/s;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/s;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/s;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/s;->e:[Lsoftware/simplicial/a/s;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/s;

    return-object v0
.end method
