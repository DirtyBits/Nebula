.class public final enum Lsoftware/simplicial/a/x;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/x;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/x;

.field public static final enum b:Lsoftware/simplicial/a/x;

.field public static final enum c:Lsoftware/simplicial/a/x;

.field public static final enum d:Lsoftware/simplicial/a/x;

.field public static final e:[Lsoftware/simplicial/a/x;

.field private static final synthetic f:[Lsoftware/simplicial/a/x;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lsoftware/simplicial/a/x;

    const-string v1, "ONLINE"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    new-instance v0, Lsoftware/simplicial/a/x;

    const-string v1, "APPEAR_OFFLINE"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/x;->b:Lsoftware/simplicial/a/x;

    new-instance v0, Lsoftware/simplicial/a/x;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/x;->c:Lsoftware/simplicial/a/x;

    new-instance v0, Lsoftware/simplicial/a/x;

    const-string v1, "DND"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/x;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/x;->d:Lsoftware/simplicial/a/x;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/x;

    sget-object v1, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/x;->b:Lsoftware/simplicial/a/x;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/x;->c:Lsoftware/simplicial/a/x;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/x;->d:Lsoftware/simplicial/a/x;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/x;->f:[Lsoftware/simplicial/a/x;

    .line 11
    invoke-static {}, Lsoftware/simplicial/a/x;->values()[Lsoftware/simplicial/a/x;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/x;->e:[Lsoftware/simplicial/a/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/x;
    .locals 1

    .prologue
    .line 7
    const-class v0, Lsoftware/simplicial/a/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/x;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/x;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lsoftware/simplicial/a/x;->f:[Lsoftware/simplicial/a/x;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/x;

    return-object v0
.end method
