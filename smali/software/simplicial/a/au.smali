.class public final enum Lsoftware/simplicial/a/au;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/au;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/au;

.field public static final enum b:Lsoftware/simplicial/a/au;

.field public static final enum c:Lsoftware/simplicial/a/au;

.field public static final enum d:Lsoftware/simplicial/a/au;

.field public static final enum e:Lsoftware/simplicial/a/au;

.field public static final enum f:Lsoftware/simplicial/a/au;

.field public static final enum g:Lsoftware/simplicial/a/au;

.field public static final enum h:Lsoftware/simplicial/a/au;

.field public static final enum i:Lsoftware/simplicial/a/au;

.field public static final enum j:Lsoftware/simplicial/a/au;

.field public static final k:[Lsoftware/simplicial/a/au;

.field private static final synthetic l:[Lsoftware/simplicial/a/au;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->a:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "REQUESTING"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->b:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->c:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->d:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->e:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "EXPIRED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->f:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "NOT_FOUND"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->g:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "INVITE_PENDING_FOR_SENDER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->h:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "INVITE_PENDING_FOR_TARGET"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->i:Lsoftware/simplicial/a/au;

    new-instance v0, Lsoftware/simplicial/a/au;

    const-string v1, "INCOMPATIBLE_VERSION"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/au;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/au;->j:Lsoftware/simplicial/a/au;

    .line 6
    const/16 v0, 0xa

    new-array v0, v0, [Lsoftware/simplicial/a/au;

    sget-object v1, Lsoftware/simplicial/a/au;->a:Lsoftware/simplicial/a/au;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/au;->b:Lsoftware/simplicial/a/au;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/au;->c:Lsoftware/simplicial/a/au;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/au;->d:Lsoftware/simplicial/a/au;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/au;->e:Lsoftware/simplicial/a/au;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/au;->f:Lsoftware/simplicial/a/au;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/au;->g:Lsoftware/simplicial/a/au;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/au;->h:Lsoftware/simplicial/a/au;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/au;->i:Lsoftware/simplicial/a/au;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lsoftware/simplicial/a/au;->j:Lsoftware/simplicial/a/au;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/au;->l:[Lsoftware/simplicial/a/au;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/au;->values()[Lsoftware/simplicial/a/au;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/au;->k:[Lsoftware/simplicial/a/au;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/au;
    .locals 1

    .prologue
    .line 14
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/au;->k:[Lsoftware/simplicial/a/au;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 15
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/au;->a:Lsoftware/simplicial/a/au;

    .line 16
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/au;->k:[Lsoftware/simplicial/a/au;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/au;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/au;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/au;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/au;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/au;->l:[Lsoftware/simplicial/a/au;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/au;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/au;

    return-object v0
.end method
