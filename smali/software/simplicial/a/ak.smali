.class public final enum Lsoftware/simplicial/a/ak;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/ak;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/ak;

.field public static final enum b:Lsoftware/simplicial/a/ak;

.field public static final enum c:Lsoftware/simplicial/a/ak;

.field public static final enum d:Lsoftware/simplicial/a/ak;

.field public static final e:[Lsoftware/simplicial/a/ak;

.field private static final synthetic f:[Lsoftware/simplicial/a/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/ak;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ak;->a:Lsoftware/simplicial/a/ak;

    new-instance v0, Lsoftware/simplicial/a/ak;

    const-string v1, "NEBULOUS"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ak;->b:Lsoftware/simplicial/a/ak;

    new-instance v0, Lsoftware/simplicial/a/ak;

    const-string v1, "ORBOROUS"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ak;->c:Lsoftware/simplicial/a/ak;

    new-instance v0, Lsoftware/simplicial/a/ak;

    const-string v1, "REBELLIOUS"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/ak;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/ak;->d:Lsoftware/simplicial/a/ak;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/ak;

    sget-object v1, Lsoftware/simplicial/a/ak;->a:Lsoftware/simplicial/a/ak;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/ak;->b:Lsoftware/simplicial/a/ak;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/ak;->c:Lsoftware/simplicial/a/ak;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/ak;->d:Lsoftware/simplicial/a/ak;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/ak;->f:[Lsoftware/simplicial/a/ak;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/ak;->values()[Lsoftware/simplicial/a/ak;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/ak;->e:[Lsoftware/simplicial/a/ak;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ak;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/ak;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ak;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/ak;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/ak;->f:[Lsoftware/simplicial/a/ak;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/ak;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/ak;

    return-object v0
.end method
