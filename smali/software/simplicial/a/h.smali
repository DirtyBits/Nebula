.class public abstract Lsoftware/simplicial/a/h;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:F

.field public c:I

.field protected d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 21
    const/4 v1, -0x1

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    invoke-virtual/range {v0 .. v9}, Lsoftware/simplicial/a/h;->a(IFFFFFFFF)V

    .line 22
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/h;->b(F)V

    .line 37
    return-void
.end method

.method public a(IFFFFFFFF)V
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lsoftware/simplicial/a/h;->n:F

    invoke-super {p0, p2, p3, v0}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 120
    iput p4, p0, Lsoftware/simplicial/a/h;->a:F

    .line 121
    iput p5, p0, Lsoftware/simplicial/a/h;->b:F

    .line 122
    iput p1, p0, Lsoftware/simplicial/a/h;->c:I

    .line 123
    iput p7, p0, Lsoftware/simplicial/a/h;->f:F

    .line 124
    iput p8, p0, Lsoftware/simplicial/a/h;->h:F

    .line 125
    iput p9, p0, Lsoftware/simplicial/a/h;->g:F

    .line 126
    invoke-virtual {p0, p6}, Lsoftware/simplicial/a/h;->c(F)V

    .line 127
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lsoftware/simplicial/a/h;->d:F

    return v0
.end method

.method public b(F)V
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lsoftware/simplicial/a/h;->f:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 42
    iget p1, p0, Lsoftware/simplicial/a/h;->f:F

    .line 43
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/h;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 44
    iget p1, p0, Lsoftware/simplicial/a/h;->g:F

    .line 49
    :cond_1
    iput p1, p0, Lsoftware/simplicial/a/h;->i:F

    .line 50
    return-void
.end method

.method public c()F
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    return v0
.end method

.method public c(F)V
    .locals 6

    .prologue
    .line 54
    iget v0, p0, Lsoftware/simplicial/a/h;->f:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 55
    iget p1, p0, Lsoftware/simplicial/a/h;->f:F

    .line 56
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/h;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 57
    iget p1, p0, Lsoftware/simplicial/a/h;->g:F

    .line 59
    :cond_1
    iput p1, p0, Lsoftware/simplicial/a/h;->i:F

    .line 61
    iput p1, p0, Lsoftware/simplicial/a/h;->e:F

    .line 62
    float-to-double v0, p1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/h;->n:F

    .line 63
    const/high16 v0, 0x42100000    # 36.0f

    iget v1, p0, Lsoftware/simplicial/a/h;->h:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    iget v2, p0, Lsoftware/simplicial/a/h;->n:F

    float-to-double v2, v2

    const-wide v4, 0x3fd89d89d89d89d8L    # 0.3846153846153846

    invoke-static {v2, v3, v4, v5}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/h;->d:F

    .line 64
    return-void
.end method

.method public d()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    return v0
.end method

.method public d(F)V
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lsoftware/simplicial/a/h;->f:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 84
    iget p1, p0, Lsoftware/simplicial/a/h;->f:F

    .line 85
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/h;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 86
    iget p1, p0, Lsoftware/simplicial/a/h;->g:F

    .line 88
    :cond_1
    iput p1, p0, Lsoftware/simplicial/a/h;->i:F

    .line 89
    return-void
.end method

.method public e(F)V
    .locals 6

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    .line 93
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    iget v1, p0, Lsoftware/simplicial/a/h;->f:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 94
    iget v0, p0, Lsoftware/simplicial/a/h;->f:F

    iput v0, p0, Lsoftware/simplicial/a/h;->i:F

    .line 95
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    iget v1, p0, Lsoftware/simplicial/a/h;->g:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 96
    iget v0, p0, Lsoftware/simplicial/a/h;->g:F

    iput v0, p0, Lsoftware/simplicial/a/h;->i:F

    .line 98
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 99
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    float-to-double v0, v0

    iget v2, p0, Lsoftware/simplicial/a/h;->i:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/a/h;->b(F)V

    .line 101
    :cond_2
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    iget v1, p0, Lsoftware/simplicial/a/h;->i:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 115
    :goto_0
    return-void

    .line 104
    :cond_3
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    iget v1, p0, Lsoftware/simplicial/a/h;->i:F

    iget v2, p0, Lsoftware/simplicial/a/h;->e:F

    sub-float/2addr v1, v2

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/h;->e:F

    .line 105
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    iget v1, p0, Lsoftware/simplicial/a/h;->i:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 106
    iget v0, p0, Lsoftware/simplicial/a/h;->i:F

    iput v0, p0, Lsoftware/simplicial/a/h;->e:F

    .line 108
    :cond_4
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    iget v1, p0, Lsoftware/simplicial/a/h;->f:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 109
    iget v0, p0, Lsoftware/simplicial/a/h;->f:F

    iput v0, p0, Lsoftware/simplicial/a/h;->e:F

    .line 110
    :cond_5
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    iget v1, p0, Lsoftware/simplicial/a/h;->g:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    .line 111
    iget v0, p0, Lsoftware/simplicial/a/h;->g:F

    iput v0, p0, Lsoftware/simplicial/a/h;->e:F

    .line 113
    :cond_6
    iget v0, p0, Lsoftware/simplicial/a/h;->e:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/h;->n:F

    .line 114
    const/high16 v0, 0x420c0000    # 35.0f

    iget v1, p0, Lsoftware/simplicial/a/h;->h:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    iget v2, p0, Lsoftware/simplicial/a/h;->n:F

    float-to-double v2, v2

    const-wide v4, 0x3fd7b425ed097b42L    # 0.37037037037037035

    invoke-static {v2, v3, v4, v5}, Lsoftware/simplicial/a/bb;->a(DD)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/h;->d:F

    goto :goto_0
.end method

.method public q_()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lsoftware/simplicial/a/h;->n:F

    return v0
.end method
