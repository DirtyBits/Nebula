.class public final enum Lsoftware/simplicial/a/c/g;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/c/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/c/g;

.field public static final enum b:Lsoftware/simplicial/a/c/g;

.field public static final enum c:Lsoftware/simplicial/a/c/g;

.field public static final enum d:Lsoftware/simplicial/a/c/g;

.field public static final e:[Lsoftware/simplicial/a/c/g;

.field private static final synthetic f:[Lsoftware/simplicial/a/c/g;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lsoftware/simplicial/a/c/g;

    const-string v1, "TINY"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    new-instance v0, Lsoftware/simplicial/a/c/g;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/g;->b:Lsoftware/simplicial/a/c/g;

    new-instance v0, Lsoftware/simplicial/a/c/g;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/g;->c:Lsoftware/simplicial/a/c/g;

    new-instance v0, Lsoftware/simplicial/a/c/g;

    const-string v1, "DUO"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/c/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/c/g;->d:Lsoftware/simplicial/a/c/g;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/c/g;

    sget-object v1, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/c/g;->b:Lsoftware/simplicial/a/c/g;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/c/g;->c:Lsoftware/simplicial/a/c/g;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/c/g;->d:Lsoftware/simplicial/a/c/g;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/a/c/g;->f:[Lsoftware/simplicial/a/c/g;

    .line 13
    invoke-static {}, Lsoftware/simplicial/a/c/g;->values()[Lsoftware/simplicial/a/c/g;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/c/g;->e:[Lsoftware/simplicial/a/c/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/c/g;
    .locals 1

    .prologue
    .line 19
    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/c/g;->e:[Lsoftware/simplicial/a/c/g;

    aget-object v0, v0, p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    return-object v0

    .line 21
    :catch_0
    move-exception v0

    .line 23
    sget-object v0, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/c/g;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lsoftware/simplicial/a/c/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/g;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/c/g;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lsoftware/simplicial/a/c/g;->f:[Lsoftware/simplicial/a/c/g;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/c/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/c/g;

    return-object v0
.end method
