.class public Lsoftware/simplicial/a/ag;
.super Lsoftware/simplicial/a/ao;
.source "SourceFile"


# static fields
.field public static final a:F


# instance fields
.field public b:Lsoftware/simplicial/a/bx;

.field public c:F

.field public d:F

.field public e:Z

.field public f:Lsoftware/simplicial/a/bf;

.field public g:Lsoftware/simplicial/a/bf;

.field public h:Lsoftware/simplicial/a/bh;

.field public i:F

.field public j:Z

.field private k:Lsoftware/simplicial/a/am;

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    sget v0, Lsoftware/simplicial/a/bx;->a:F

    sput v0, Lsoftware/simplicial/a/ag;->a:F

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 17
    iput-boolean v1, p0, Lsoftware/simplicial/a/ag;->e:Z

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/ag;->i:F

    .line 22
    iput-boolean v1, p0, Lsoftware/simplicial/a/ag;->j:Z

    .line 29
    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Lsoftware/simplicial/a/ao;-><init>()V

    .line 17
    iput-boolean v1, p0, Lsoftware/simplicial/a/ag;->e:Z

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/ag;->i:F

    .line 22
    iput-boolean v1, p0, Lsoftware/simplicial/a/ag;->j:Z

    .line 33
    invoke-virtual {p0, p1, p2, p3, p4}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/util/Random;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget v1, v0, Lsoftware/simplicial/a/bx;->l:F

    iget-object v0, p0, Lsoftware/simplicial/a/ag;->k:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/util/Random;->nextFloat()F

    move-result v0

    iget v2, p0, Lsoftware/simplicial/a/ag;->z:F

    mul-float/2addr v0, v2

    :goto_0
    sget v2, Lsoftware/simplicial/a/ag;->a:F

    invoke-super {p0, v1, v0, v2}, Lsoftware/simplicial/a/ao;->a(FFF)V

    .line 49
    iput-boolean v4, p0, Lsoftware/simplicial/a/ag;->j:Z

    .line 51
    iput v3, p0, Lsoftware/simplicial/a/ag;->c:F

    .line 52
    iput v3, p0, Lsoftware/simplicial/a/ag;->d:F

    .line 53
    iput-boolean v4, p0, Lsoftware/simplicial/a/ag;->e:Z

    .line 54
    iput v3, p0, Lsoftware/simplicial/a/ag;->i:F

    .line 56
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    .line 59
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    iget v0, v0, Lsoftware/simplicial/a/bx;->m:F

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bf;Lsoftware/simplicial/a/bh;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    invoke-virtual {p0, v0, v2}, Lsoftware/simplicial/a/ag;->a(Lsoftware/simplicial/a/bf;Z)V

    .line 65
    iput-object p1, p0, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    .line 66
    iput-object p2, p0, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    .line 67
    iget v0, p2, Lsoftware/simplicial/a/bh;->l:F

    iput v0, p0, Lsoftware/simplicial/a/ag;->l:F

    .line 68
    iget v0, p2, Lsoftware/simplicial/a/bh;->m:F

    iput v0, p0, Lsoftware/simplicial/a/ag;->m:F

    .line 69
    iput v1, p0, Lsoftware/simplicial/a/ag;->c:F

    .line 70
    iput v1, p0, Lsoftware/simplicial/a/ag;->d:F

    .line 71
    iput-boolean v2, p0, Lsoftware/simplicial/a/ag;->e:Z

    .line 72
    iput v1, p0, Lsoftware/simplicial/a/ag;->i:F

    .line 73
    iput-boolean v2, p0, Lsoftware/simplicial/a/ag;->j:Z

    .line 75
    iput-object p0, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 76
    iget v0, p1, Lsoftware/simplicial/a/bf;->bk:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lsoftware/simplicial/a/bf;->bk:I

    .line 77
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bf;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    iput-object v0, p0, Lsoftware/simplicial/a/ag;->f:Lsoftware/simplicial/a/bf;

    .line 82
    iput-object v1, p0, Lsoftware/simplicial/a/ag;->g:Lsoftware/simplicial/a/bf;

    .line 83
    iput-object v1, p0, Lsoftware/simplicial/a/ag;->h:Lsoftware/simplicial/a/bh;

    .line 84
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/a/ag;->i:F

    .line 86
    if-eqz p1, :cond_1

    .line 88
    iput-object v1, p1, Lsoftware/simplicial/a/bf;->aG:Lsoftware/simplicial/a/ag;

    .line 89
    if-eqz p2, :cond_1

    .line 91
    instance-of v0, p0, Lsoftware/simplicial/a/bl;

    if-nez v0, :cond_1

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->k:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_0

    .line 94
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p1, Lsoftware/simplicial/a/bf;->al:F

    .line 95
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/a/ag;->k:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1

    .line 96
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p1, Lsoftware/simplicial/a/bf;->al:F

    .line 100
    :cond_1
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bx;Lsoftware/simplicial/a/am;Ljava/util/Random;F)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lsoftware/simplicial/a/ag;->b:Lsoftware/simplicial/a/bx;

    .line 39
    iput-object p2, p0, Lsoftware/simplicial/a/ag;->k:Lsoftware/simplicial/a/am;

    .line 40
    iput p4, p0, Lsoftware/simplicial/a/ag;->z:F

    .line 42
    invoke-virtual {p0, p3}, Lsoftware/simplicial/a/ag;->a(Ljava/util/Random;)V

    .line 43
    return-void
.end method
