.class public abstract Lsoftware/simplicial/a/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field public l:F

.field public m:F

.field public n:F

.field public o:F

.field p:[Lsoftware/simplicial/a/cb;

.field q:I

.field r:F

.field public s:F

.field public t:F

.field public u:F

.field public v:F

.field public w:F

.field public x:F

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v1, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 12
    iput-boolean v2, p0, Lsoftware/simplicial/a/ao;->a:Z

    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/a/cb;

    iput-object v0, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/ao;->q:I

    .line 15
    iput v1, p0, Lsoftware/simplicial/a/ao;->r:F

    .line 22
    iput v2, p0, Lsoftware/simplicial/a/ao;->y:I

    .line 27
    return-void
.end method

.method private a(FFFF)F
    .locals 2

    .prologue
    .line 166
    sub-float v0, p1, p3

    .line 167
    sub-float v1, p2, p4

    .line 168
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 169
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lsoftware/simplicial/a/ao;->r:F

    .line 170
    iget v0, p0, Lsoftware/simplicial/a/ao;->o:F

    return v0
.end method

.method static a(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 36
    array-length v0, p2

    if-nez v0, :cond_1

    move v1, v2

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 39
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/ao;->q:I

    if-eq v0, p3, :cond_3

    move v0, v1

    .line 41
    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_2

    .line 42
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, p2, v0

    iget v5, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v6, p0, Lsoftware/simplicial/a/ao;->m:F

    invoke-virtual {v4, v5, v6}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    aput-object v4, v3, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 43
    :cond_2
    iput p3, p0, Lsoftware/simplicial/a/ao;->q:I

    .line 46
    :cond_3
    iget v0, p1, Lsoftware/simplicial/a/ao;->q:I

    if-eq v0, p3, :cond_5

    move v0, v1

    .line 48
    :goto_2
    array-length v3, p2

    if-ge v0, v3, :cond_4

    .line 49
    iget-object v3, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, p2, v0

    iget v5, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v6, p1, Lsoftware/simplicial/a/ao;->m:F

    invoke-virtual {v4, v5, v6}, Lsoftware/simplicial/a/ca;->c(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    aput-object v4, v3, v0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 50
    :cond_4
    iput p3, p1, Lsoftware/simplicial/a/ao;->q:I

    :cond_5
    move v0, v1

    .line 53
    :goto_3
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 54
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    sget-object v4, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v3, v4, :cond_6

    iget-object v3, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    sget-object v4, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    iget-object v4, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, v4, v0

    if-ne v3, v4, :cond_0

    .line 53
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public static b(Lsoftware/simplicial/a/ao;Lsoftware/simplicial/a/ao;[Lsoftware/simplicial/a/ca;I)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    array-length v0, p2

    if-nez v0, :cond_1

    move v1, v2

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/ao;->q:I

    if-eq v0, p3, :cond_3

    move v0, v1

    .line 67
    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_2

    .line 68
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, p2, v0

    iget v5, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v6, p0, Lsoftware/simplicial/a/ao;->t:F

    invoke-virtual {v4, v5, v6}, Lsoftware/simplicial/a/ca;->d(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    aput-object v4, v3, v0

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 69
    :cond_2
    iput p3, p0, Lsoftware/simplicial/a/ao;->q:I

    .line 72
    :cond_3
    iget v0, p1, Lsoftware/simplicial/a/ao;->q:I

    if-eq v0, p3, :cond_5

    move v0, v1

    .line 74
    :goto_2
    array-length v3, p2

    if-ge v0, v3, :cond_4

    .line 75
    iget-object v3, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, p2, v0

    iget v5, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v6, p1, Lsoftware/simplicial/a/ao;->t:F

    invoke-virtual {v4, v5, v6}, Lsoftware/simplicial/a/ca;->d(FF)Lsoftware/simplicial/a/cb;

    move-result-object v4

    aput-object v4, v3, v0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 76
    :cond_4
    iput p3, p1, Lsoftware/simplicial/a/ao;->q:I

    :cond_5
    move v0, v1

    .line 79
    :goto_3
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 80
    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    sget-object v4, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v3, v4, :cond_6

    iget-object v3, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    sget-object v4, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v3, v3, v0

    iget-object v4, p1, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    aget-object v4, v4, v0

    if-ne v3, v4, :cond_0

    .line 79
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v1, v2

    .line 83
    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 125
    iget v0, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v1, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->s:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ao;->s:F

    .line 126
    iget v0, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v1, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->t:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ao;->t:F

    .line 127
    iget v0, p0, Lsoftware/simplicial/a/ao;->w:F

    iget v1, p0, Lsoftware/simplicial/a/ao;->n:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->w:F

    sub-float/2addr v1, v2

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/a/ao;->w:F

    .line 128
    return-void
.end method

.method public a(FFF)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 88
    iput p1, p0, Lsoftware/simplicial/a/ao;->l:F

    .line 89
    iput p2, p0, Lsoftware/simplicial/a/ao;->m:F

    .line 90
    iput p1, p0, Lsoftware/simplicial/a/ao;->s:F

    .line 91
    iput p2, p0, Lsoftware/simplicial/a/ao;->t:F

    .line 92
    iput p1, p0, Lsoftware/simplicial/a/ao;->u:F

    .line 93
    iput p2, p0, Lsoftware/simplicial/a/ao;->v:F

    .line 94
    iput p3, p0, Lsoftware/simplicial/a/ao;->n:F

    .line 95
    iput p3, p0, Lsoftware/simplicial/a/ao;->w:F

    .line 97
    iput v1, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 98
    iput-boolean v0, p0, Lsoftware/simplicial/a/ao;->a:Z

    .line 99
    iput v1, p0, Lsoftware/simplicial/a/ao;->r:F

    .line 100
    iput v0, p0, Lsoftware/simplicial/a/ao;->y:I

    .line 102
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 104
    iget-object v1, p0, Lsoftware/simplicial/a/ao;->p:[Lsoftware/simplicial/a/cb;

    sget-object v2, Lsoftware/simplicial/a/cb;->a:Lsoftware/simplicial/a/cb;

    aput-object v2, v1, v0

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_0
    return-void
.end method

.method a(I)V
    .locals 1

    .prologue
    .line 243
    if-lez p1, :cond_1

    iget v0, p0, Lsoftware/simplicial/a/ao;->y:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lsoftware/simplicial/a/ao;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/ao;->l:F

    iput v0, p0, Lsoftware/simplicial/a/ao;->s:F

    .line 246
    iget v0, p0, Lsoftware/simplicial/a/ao;->m:F

    iput v0, p0, Lsoftware/simplicial/a/ao;->t:F

    .line 247
    iget v0, p0, Lsoftware/simplicial/a/ao;->n:F

    iput v0, p0, Lsoftware/simplicial/a/ao;->w:F

    .line 250
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/ao;->y:I

    if-ne v0, p1, :cond_2

    .line 254
    :goto_0
    return-void

    .line 253
    :cond_2
    iput p1, p0, Lsoftware/simplicial/a/ao;->y:I

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/ao;)Z
    .locals 6

    .prologue
    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 132
    iget v1, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 134
    :cond_0
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 144
    :cond_1
    :goto_0
    return v0

    .line 137
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 139
    :cond_3
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    goto :goto_0

    .line 143
    :cond_4
    iget v1, p0, Lsoftware/simplicial/a/ao;->n:F

    iget v2, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    .line 144
    iget v2, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v5, p1, Lsoftware/simplicial/a/ao;->m:F

    invoke-direct {p0, v2, v3, v4, v5}, Lsoftware/simplicial/a/ao;->a(FFFF)F

    move-result v2

    mul-float/2addr v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/ao;F)Z
    .locals 6

    .prologue
    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 149
    iget v1, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 151
    :cond_0
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 161
    :cond_1
    :goto_0
    return v0

    .line 154
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->m:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 156
    :cond_3
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    goto :goto_0

    .line 160
    :cond_4
    iget v1, p0, Lsoftware/simplicial/a/ao;->n:F

    iget v2, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    .line 161
    iget v2, p0, Lsoftware/simplicial/a/ao;->l:F

    iget v3, p0, Lsoftware/simplicial/a/ao;->m:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->l:F

    iget v5, p1, Lsoftware/simplicial/a/ao;->m:F

    invoke-direct {p0, v2, v3, v4, v5}, Lsoftware/simplicial/a/ao;->a(FFFF)F

    move-result v2

    mul-float/2addr v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/ao;)Z
    .locals 6

    .prologue
    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 185
    iget v1, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 187
    :cond_0
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 198
    :cond_1
    :goto_0
    return v0

    .line 190
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    iget v2, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 192
    :cond_3
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    goto :goto_0

    .line 196
    :cond_4
    iget v1, p0, Lsoftware/simplicial/a/ao;->n:F

    iget v2, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    .line 198
    iget v2, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v5, p1, Lsoftware/simplicial/a/ao;->t:F

    invoke-direct {p0, v2, v3, v4, v5}, Lsoftware/simplicial/a/ao;->a(FFFF)F

    move-result v2

    mul-float/2addr v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/ao;F)Z
    .locals 6

    .prologue
    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    const/4 v0, 0x0

    .line 203
    iget v1, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    iget v1, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 205
    :cond_0
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    .line 216
    :cond_1
    :goto_0
    return v0

    .line 208
    :cond_2
    iget v1, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_3

    iget v1, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v2, p0, Lsoftware/simplicial/a/ao;->n:F

    sub-float/2addr v1, v2

    add-float/2addr v1, p2

    iget v2, p1, Lsoftware/simplicial/a/ao;->t:F

    iget v3, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 210
    :cond_3
    iput v4, p0, Lsoftware/simplicial/a/ao;->r:F

    iput v4, p0, Lsoftware/simplicial/a/ao;->o:F

    goto :goto_0

    .line 214
    :cond_4
    iget v1, p0, Lsoftware/simplicial/a/ao;->n:F

    iget v2, p1, Lsoftware/simplicial/a/ao;->n:F

    add-float/2addr v1, v2

    sub-float/2addr v1, p2

    .line 216
    iget v2, p0, Lsoftware/simplicial/a/ao;->s:F

    iget v3, p0, Lsoftware/simplicial/a/ao;->t:F

    iget v4, p1, Lsoftware/simplicial/a/ao;->s:F

    iget v5, p1, Lsoftware/simplicial/a/ao;->t:F

    invoke-direct {p0, v2, v3, v4, v5}, Lsoftware/simplicial/a/ao;->a(FFFF)F

    move-result v2

    mul-float/2addr v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/a/ao;->a:Z

    .line 111
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lsoftware/simplicial/a/ao;->a:Z

    return v0
.end method

.method g()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/a/ao;->a:Z

    .line 121
    return-void
.end method

.method h()F
    .locals 2

    .prologue
    .line 175
    iget v0, p0, Lsoftware/simplicial/a/ao;->r:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 177
    iget v0, p0, Lsoftware/simplicial/a/ao;->o:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lsoftware/simplicial/a/ao;->r:F

    .line 180
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/ao;->r:F

    return v0
.end method

.method i()V
    .locals 3

    .prologue
    .line 221
    iget v0, p0, Lsoftware/simplicial/a/ao;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object x NAN detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_0
    iget v0, p0, Lsoftware/simplicial/a/ao;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object x INF detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_1
    iget v0, p0, Lsoftware/simplicial/a/ao;->m:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object y NAN detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_2
    iget v0, p0, Lsoftware/simplicial/a/ao;->m:F

    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 228
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object y INF detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_3
    iget v0, p0, Lsoftware/simplicial/a/ao;->n:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 230
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object radius NAN detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_4
    iget v0, p0, Lsoftware/simplicial/a/ao;->n:F

    invoke-static {v0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 232
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object radius INF detected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_5
    return-void
.end method

.method j()V
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lsoftware/simplicial/a/ao;->y:I

    if-lez v0, :cond_0

    .line 238
    iget v0, p0, Lsoftware/simplicial/a/ao;->y:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/a/ao;->y:I

    .line 239
    :cond_0
    return-void
.end method
