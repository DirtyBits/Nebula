.class public Lsoftware/simplicial/a/bb;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(D)D
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 14
    cmpg-double v2, p0, v0

    if-gtz v2, :cond_1

    .line 16
    cmpg-double v2, p0, v0

    if-gez v2, :cond_0

    .line 17
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "NEGATIVE SQUARE ROOT!"

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 20
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(DD)D
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 25
    cmpg-double v2, p0, v0

    if-gtz v2, :cond_1

    .line 27
    cmpg-double v2, p0, v0

    if-gez v2, :cond_0

    .line 28
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "NEGATIVE ROOT!"

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 31
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto :goto_0
.end method
