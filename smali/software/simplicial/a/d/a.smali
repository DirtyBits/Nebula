.class public final enum Lsoftware/simplicial/a/d/a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/d/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/d/a;

.field public static final enum b:Lsoftware/simplicial/a/d/a;

.field public static final enum c:Lsoftware/simplicial/a/d/a;

.field public static final enum d:Lsoftware/simplicial/a/d/a;

.field public static final enum e:Lsoftware/simplicial/a/d/a;

.field public static final f:[Lsoftware/simplicial/a/d/a;

.field private static final synthetic g:[Lsoftware/simplicial/a/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/d/a;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/d/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/a;->a:Lsoftware/simplicial/a/d/a;

    new-instance v0, Lsoftware/simplicial/a/d/a;

    const-string v1, "NAME_TAKEN"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/d/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/a;->b:Lsoftware/simplicial/a/d/a;

    new-instance v0, Lsoftware/simplicial/a/d/a;

    const-string v1, "USER_NAME_INVALID"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/d/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/a;->c:Lsoftware/simplicial/a/d/a;

    new-instance v0, Lsoftware/simplicial/a/d/a;

    const-string v1, "LOBBY_NAME_INVALID"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/d/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/a;->d:Lsoftware/simplicial/a/d/a;

    new-instance v0, Lsoftware/simplicial/a/d/a;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/d/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/a;->e:Lsoftware/simplicial/a/d/a;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lsoftware/simplicial/a/d/a;

    sget-object v1, Lsoftware/simplicial/a/d/a;->a:Lsoftware/simplicial/a/d/a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/a/d/a;->b:Lsoftware/simplicial/a/d/a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/d/a;->c:Lsoftware/simplicial/a/d/a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/d/a;->d:Lsoftware/simplicial/a/d/a;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/d/a;->e:Lsoftware/simplicial/a/d/a;

    aput-object v1, v0, v6

    sput-object v0, Lsoftware/simplicial/a/d/a;->g:[Lsoftware/simplicial/a/d/a;

    .line 9
    invoke-static {}, Lsoftware/simplicial/a/d/a;->values()[Lsoftware/simplicial/a/d/a;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/d/a;->f:[Lsoftware/simplicial/a/d/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/d/a;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/d/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/d/a;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/d/a;->g:[Lsoftware/simplicial/a/d/a;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/d/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/d/a;

    return-object v0
.end method
