.class public final enum Lsoftware/simplicial/a/d/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/a/d/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/a/d/c;

.field public static final enum b:Lsoftware/simplicial/a/d/c;

.field public static final enum c:Lsoftware/simplicial/a/d/c;

.field public static final enum d:Lsoftware/simplicial/a/d/c;

.field public static final enum e:Lsoftware/simplicial/a/d/c;

.field public static final enum f:Lsoftware/simplicial/a/d/c;

.field public static final enum g:Lsoftware/simplicial/a/d/c;

.field public static final enum h:Lsoftware/simplicial/a/d/c;

.field public static final enum i:Lsoftware/simplicial/a/d/c;

.field public static final j:[Lsoftware/simplicial/a/d/c;

.field private static final synthetic k:[Lsoftware/simplicial/a/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->a:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "PUBLIC_GAME"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->b:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "PRIVATE_GAME"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "CHAT"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "FRIENDS"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->e:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "CLAN_INVITES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->f:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "CLAN_MEMBERS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->g:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "CLAN_WAR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    new-instance v0, Lsoftware/simplicial/a/d/c;

    const-string v1, "ARENA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/a/d/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->a:Lsoftware/simplicial/a/d/c;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/a/d/c;->b:Lsoftware/simplicial/a/d/c;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/a/d/c;->e:Lsoftware/simplicial/a/d/c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/a/d/c;->f:Lsoftware/simplicial/a/d/c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lsoftware/simplicial/a/d/c;->g:Lsoftware/simplicial/a/d/c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/a/d/c;->k:[Lsoftware/simplicial/a/d/c;

    .line 10
    invoke-static {}, Lsoftware/simplicial/a/d/c;->values()[Lsoftware/simplicial/a/d/c;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/a/d/c;->j:[Lsoftware/simplicial/a/d/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(B)Lsoftware/simplicial/a/d/c;
    .locals 1

    .prologue
    .line 14
    if-ltz p0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/d/c;->j:[Lsoftware/simplicial/a/d/c;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 15
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/d/c;->a:Lsoftware/simplicial/a/d/c;

    .line 16
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lsoftware/simplicial/a/d/c;->j:[Lsoftware/simplicial/a/d/c;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/d/c;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/a/d/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/c;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/a/d/c;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/a/d/c;->k:[Lsoftware/simplicial/a/d/c;

    invoke-virtual {v0}, [Lsoftware/simplicial/a/d/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/a/d/c;

    return-object v0
.end method
