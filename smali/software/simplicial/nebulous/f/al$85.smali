.class Lsoftware/simplicial/nebulous/f/al$85;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$ah;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$ah;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$ah;)V
    .locals 0

    .prologue
    .line 5290
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$85;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$85;->a:Lsoftware/simplicial/nebulous/f/al$ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    .line 5294
    const-string v0, "SpinType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 5295
    const-string v0, "SpinData"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 5296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v4, "NextSpinRemainingMs"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 5298
    const-string v0, "SpinsRemaining"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5299
    const-string v0, "SpinsRemaining"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 5303
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$85;->a:Lsoftware/simplicial/nebulous/f/al$ah;

    invoke-interface/range {v1 .. v6}, Lsoftware/simplicial/nebulous/f/al$ah;->a(Ljava/lang/String;IJI)V

    .line 5304
    return-void

    .line 5301
    :cond_0
    const/4 v6, 0x1

    goto :goto_0
.end method
