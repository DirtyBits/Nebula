.class public Lsoftware/simplicial/nebulous/f/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/f/ag$a;,
        Lsoftware/simplicial/nebulous/f/ag$b;
    }
.end annotation


# static fields
.field public static final a:B


# instance fields
.field public A:I

.field public B:I

.field public C:Lsoftware/simplicial/a/ac;

.field public D:I

.field public E:Z

.field public F:I

.field public G:I

.field public H:I

.field public I:I

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:Ljava/lang/String;

.field public M:Ljava/lang/String;

.field public N:Ljava/lang/String;

.field public O:Ljava/lang/String;

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:Z

.field public Y:Lsoftware/simplicial/a/x;

.field public Z:Ljava/lang/String;

.field public aA:[Ljava/lang/String;

.field public aB:[[B

.field public aC:Z

.field private aD:Lsoftware/simplicial/nebulous/f/ag$b;

.field private aE:I

.field private aF:[Ljava/lang/String;

.field public aa:[B

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Lsoftware/simplicial/a/n;

.field public af:Lsoftware/simplicial/a/q;

.field public ag:S

.field public ah:Z

.field public ai:Z

.field public aj:Lsoftware/simplicial/a/c/e;

.field public ak:Lsoftware/simplicial/a/b/d;

.field public al:Lsoftware/simplicial/a/h/f;

.field public am:Lsoftware/simplicial/a/c/g;

.field public an:Z

.field public ao:F

.field public ap:F

.field public aq:Ljava/lang/String;

.field public ar:Ljava/lang/String;

.field public as:Z

.field public at:Z

.field public au:Z

.field public av:Z

.field public aw:Ljava/lang/String;

.field public ax:[B

.field public ay:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/j;",
            ">;"
        }
    .end annotation
.end field

.field public az:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lsoftware/simplicial/nebulous/f/ag$a;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/am;",
            "Lsoftware/simplicial/a/az;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Z

.field public h:Lsoftware/simplicial/nebulous/c/b;

.field public i:Lsoftware/simplicial/nebulous/c/a;

.field public j:Lsoftware/simplicial/nebulous/c/e;

.field public k:Lsoftware/simplicial/a/bn;

.field public l:Lsoftware/simplicial/a/e;

.field public m:Lsoftware/simplicial/a/e;

.field public n:Lsoftware/simplicial/a/af;

.field public o:Lsoftware/simplicial/a/af;

.field public p:Lsoftware/simplicial/a/as;

.field public q:Lsoftware/simplicial/a/as;

.field public r:Lsoftware/simplicial/a/bd;

.field public s:Lsoftware/simplicial/a/bd;

.field public t:I

.field public u:Lsoftware/simplicial/a/am;

.field public v:Lsoftware/simplicial/a/ap;

.field public w:Lsoftware/simplicial/nebulous/f/aj;

.field public x:Z

.field public y:I

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const/16 v0, -0x2900

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/aa;->a(I)B

    move-result v0

    sput-byte v0, Lsoftware/simplicial/nebulous/f/ag;->a:B

    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/nebulous/f/ag$b;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Lsoftware/simplicial/nebulous/f/ag$a;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/f/ag$a;-><init>(Lsoftware/simplicial/nebulous/f/ag;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    .line 66
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    .line 68
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    .line 69
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    .line 70
    sget-object v0, Lsoftware/simplicial/nebulous/c/b;->a:Lsoftware/simplicial/nebulous/c/b;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    .line 71
    sget-object v0, Lsoftware/simplicial/nebulous/c/a;->a:Lsoftware/simplicial/nebulous/c/a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    .line 72
    sget-object v0, Lsoftware/simplicial/nebulous/c/e;->a:Lsoftware/simplicial/nebulous/c/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;

    .line 73
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    .line 74
    sget-object v0, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    .line 75
    sget-object v0, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    .line 76
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    .line 77
    sget-object v0, Lsoftware/simplicial/a/af;->a:Lsoftware/simplicial/a/af;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    .line 78
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 79
    sget-object v0, Lsoftware/simplicial/a/as;->a:Lsoftware/simplicial/a/as;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    .line 80
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    .line 81
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    .line 82
    iput v1, p0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    .line 83
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 84
    sget-object v0, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    .line 85
    sget-object v0, Lsoftware/simplicial/nebulous/f/aj;->a:Lsoftware/simplicial/nebulous/f/aj;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->w:Lsoftware/simplicial/nebulous/f/aj;

    .line 86
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->x:Z

    .line 87
    const/16 v0, 0x32

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->y:I

    .line 88
    const/16 v0, 0x28

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->z:I

    .line 89
    const/16 v0, 0x32

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->A:I

    .line 90
    const/16 v0, 0x14

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->B:I

    .line 91
    sget-object v0, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    .line 92
    const/16 v0, 0x12

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    .line 93
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->E:Z

    .line 94
    const/4 v0, 0x7

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    .line 95
    const/16 v0, 0x14

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    .line 96
    const/16 v0, 0x15

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    .line 97
    const/4 v0, 0x7

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->I:I

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->J:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->K:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->L:Ljava/lang/String;

    .line 101
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    .line 103
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->O:Ljava/lang/String;

    .line 104
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->P:Z

    .line 105
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->Q:Z

    .line 106
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    .line 107
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->S:Z

    .line 108
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->T:Z

    .line 109
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->U:Z

    .line 110
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->V:Z

    .line 111
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    .line 112
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->X:Z

    .line 113
    sget-object v0, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    .line 114
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 115
    const/16 v0, 0xb

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    .line 116
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ab:Z

    .line 117
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ac:Z

    .line 118
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    .line 119
    sget-object v0, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    .line 120
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 121
    const/16 v0, 0x12c

    iput-short v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    .line 122
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->ah:Z

    .line 123
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->ai:Z

    .line 124
    sget-object v0, Lsoftware/simplicial/a/c/e;->a:Lsoftware/simplicial/a/c/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    .line 125
    sget-object v0, Lsoftware/simplicial/a/b/d;->c:Lsoftware/simplicial/a/b/d;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    .line 126
    sget-object v0, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->al:Lsoftware/simplicial/a/h/f;

    .line 127
    sget-object v0, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->am:Lsoftware/simplicial/a/c/g;

    .line 128
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    .line 129
    iput v4, p0, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    .line 130
    iput v4, p0, Lsoftware/simplicial/nebulous/f/ag;->ap:F

    .line 131
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    .line 132
    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    .line 133
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    .line 134
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    .line 135
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->au:Z

    .line 136
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->av:Z

    .line 137
    const-string v0, "https://s3.amazonaws.com/simplicialsoftware.skins/"

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aw:Ljava/lang/String;

    .line 139
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    .line 141
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    .line 142
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    .line 143
    const/16 v0, 0xa

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    .line 144
    invoke-static {v5}, Lsoftware/simplicial/a/ba;->c(I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aE:I

    .line 145
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aC:Z

    .line 146
    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aF:[Ljava/lang/String;

    .line 150
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/ag;->aD:Lsoftware/simplicial/nebulous/f/ag$b;

    .line 152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->b:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->c:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->d:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->e:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->i:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->j:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->k:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->f:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->g:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->h:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->l:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->m:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->n:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->o:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->p:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->q:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->r:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->s:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    invoke-static {v0, v5}, Ljava/util/Arrays;->fill([BB)V

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    sget-byte v1, Lsoftware/simplicial/nebulous/f/ag;->a:B

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([BB)V

    .line 175
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)Lsoftware/simplicial/nebulous/f/o;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;)",
            "Lsoftware/simplicial/nebulous/f/o;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1118
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1125
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o;

    .line 1127
    iget v4, v0, Lsoftware/simplicial/nebulous/f/o;->b:I

    if-ne v4, v2, :cond_0

    .line 1130
    :goto_0
    return-object v0

    .line 1120
    :catch_0
    move-exception v0

    .line 1122
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    .line 1123
    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1130
    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/content/SharedPreferences;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 988
    .line 991
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 992
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 993
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 994
    const/4 v3, 0x1

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 995
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 999
    new-instance v3, Ljava/io/File;

    const-string v4, "LOCAL_CUSTOM_SKIN"

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1000
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1007
    :goto_0
    if-nez v0, :cond_0

    .line 1012
    :try_start_1
    const-string v1, "CUSTOM_SKIN"

    const/4 v2, 0x0

    invoke-interface {p2, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/o;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1013
    invoke-interface {p2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "CUSTOM_SKIN"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1014
    invoke-virtual {p0, v0, p1}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/graphics/Bitmap;Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1024
    :cond_0
    :goto_1
    return-object v0

    .line 1002
    :catch_0
    move-exception v0

    .line 1004
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    goto :goto_0

    .line 1016
    :catch_1
    move-exception v1

    .line 1018
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    .line 1019
    iput v5, p0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    .line 1020
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public a(I[B)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1249
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    aget-object v0, v0, p1

    array-length v1, p2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    aget-object v2, v2, p1

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v3, p2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1250
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(Lsoftware/simplicial/a/bd;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1272
    sget-object v0, Lsoftware/simplicial/a/bd;->a:Lsoftware/simplicial/a/bd;

    if-ne p1, v0, :cond_0

    .line 1273
    const-string v0, ""

    .line 1275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aF:[Ljava/lang/String;

    iget-byte v1, p1, Lsoftware/simplicial/a/bd;->c:B

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x40

    const/4 v0, 0x0

    .line 1135
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 1139
    :try_start_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "neb_skin_cache/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1140
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1141
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1142
    const/4 v2, 0x1

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1143
    const/4 v2, 0x0

    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 1144
    const/16 v2, 0x80

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1145
    const/16 v2, 0x80

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1147
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v6, v5

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_0

    aget-object v1, v5, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1149
    if-lt v0, v9, :cond_1

    .line 1166
    :cond_0
    :goto_1
    return-object v3

    .line 1153
    :cond_1
    :try_start_1
    new-instance v7, Lsoftware/simplicial/nebulous/f/o;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {v7, v8, v1}, Lsoftware/simplicial/nebulous/f/o;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1154
    add-int/lit8 v0, v0, 0x1

    .line 1147
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1156
    :catch_0
    move-exception v1

    .line 1158
    :try_start_2
    sget-object v7, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 1162
    :catch_1
    move-exception v0

    .line 1164
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public a(ILjava/lang/String;Lsoftware/simplicial/nebulous/application/MainActivity;Z)Lsoftware/simplicial/nebulous/f/j;
    .locals 7

    .prologue
    const/16 v5, 0x3e8

    const v6, 0x7f0801cf

    const v3, 0x7f080102

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1191
    if-nez p3, :cond_0

    move-object v0, v1

    .line 1227
    :goto_0
    return-object v0

    .line 1194
    :cond_0
    iget-object v0, p3, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 1196
    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802b4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v0, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 1197
    goto :goto_0

    .line 1200
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v5, :cond_2

    .line 1203
    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1204
    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080083

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1205
    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1202
    invoke-static {p3, v0, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 1206
    goto :goto_0

    .line 1209
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/j;

    .line 1211
    iget v0, v0, Lsoftware/simplicial/nebulous/f/j;->a:I

    if-ne v0, p1, :cond_3

    .line 1213
    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v0, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 1214
    goto/16 :goto_0

    .line 1218
    :cond_4
    new-instance v0, Lsoftware/simplicial/nebulous/f/j;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/f/j;-><init>()V

    .line 1219
    iput p1, v0, Lsoftware/simplicial/nebulous/f/j;->a:I

    .line 1220
    iput-object p2, v0, Lsoftware/simplicial/nebulous/f/j;->b:Ljava/lang/String;

    .line 1221
    iput-boolean p4, v0, Lsoftware/simplicial/nebulous/f/j;->c:Z

    .line 1222
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1223
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1225
    iget-object v1, p3, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1, p1, v4}, Lsoftware/simplicial/nebulous/f/al;->a(IZ)V

    .line 1226
    iget-object v1, p3, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1, p1, v4}, Lsoftware/simplicial/nebulous/f/al;->c(IZ)V

    goto/16 :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 1260
    if-eqz p1, :cond_0

    .line 1261
    invoke-static {p1}, Lsoftware/simplicial/a/ba;->c(I)I

    move-result p1

    .line 1262
    :cond_0
    iput p1, p0, Lsoftware/simplicial/nebulous/f/ag;->aE:I

    .line 1263
    return-void
.end method

.method public a(ILjava/lang/String;[B)V
    .locals 2

    .prologue
    .line 1243
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    aput-object p2, v0, p1

    .line 1244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    array-length v1, p3

    invoke-static {p3, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    aput-object v1, v0, p1

    .line 1245
    return-void
.end method

.method public a(ILsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 2

    .prologue
    .line 1176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1177
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1179
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/j;

    .line 1180
    iget v0, v0, Lsoftware/simplicial/nebulous/f/j;->a:I

    if-ne v0, p1, :cond_0

    .line 1181
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1183
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1185
    if-eqz p2, :cond_2

    .line 1186
    iget-object v0, p2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lsoftware/simplicial/nebulous/f/al;->d(IZ)V

    .line 1187
    :cond_2
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1061
    :try_start_0
    new-instance v4, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "neb_skin_cache/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1062
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 1066
    :try_start_1
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v5, v2, v0

    .line 1068
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, p2}, Lsoftware/simplicial/nebulous/f/ag;->a(Ljava/lang/String;Ljava/util/List;)Lsoftware/simplicial/nebulous/f/o;

    move-result-object v6

    .line 1069
    if-eqz v6, :cond_0

    .line 1070
    invoke-interface {p2, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1066
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1072
    :cond_0
    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1075
    :catch_0
    move-exception v0

    .line 1077
    :try_start_2
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1081
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v1

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1083
    const/16 v1, 0x40

    if-lt v3, v1, :cond_3

    .line 1111
    :cond_2
    :goto_3
    return-void

    .line 1086
    :cond_3
    const/4 v2, 0x0

    .line 1090
    :try_start_3
    new-instance v6, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, v0, Lsoftware/simplicial/nebulous/f/o;->b:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1091
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1092
    :try_start_4
    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v0, v2, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1100
    if-eqz v1, :cond_4

    .line 1101
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 1104
    :cond_4
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 1105
    goto :goto_2

    .line 1094
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 1096
    :goto_5
    :try_start_6
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1100
    if-eqz v1, :cond_4

    .line 1101
    :try_start_7
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_4

    .line 1107
    :catch_2
    move-exception v0

    .line 1109
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 1100
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v2, :cond_5

    .line 1101
    :try_start_8
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_5
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 1100
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_6

    .line 1094
    :catch_3
    move-exception v0

    goto :goto_5
.end method

.method public a(Landroid/content/Context;Lsoftware/simplicial/nebulous/d/d;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 894
    new-instance v1, Lsoftware/simplicial/a/az;

    invoke-direct {v1}, Lsoftware/simplicial/a/az;-><init>()V

    .line 897
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 898
    :try_start_1
    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/d/d;->e()Ljava/util/List;

    move-result-object v0

    .line 899
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 901
    new-instance v0, Lsoftware/simplicial/nebulous/d/a;

    invoke-direct {v0, p1}, Lsoftware/simplicial/nebulous/d/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/d/a;->a()Ljava/util/List;

    move-result-object v2

    .line 902
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 903
    invoke-virtual {p2, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/d;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 913
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 915
    :goto_1
    const-string v2, "Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to load single player stats. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "OK"

    invoke-static {p1, v2, v1, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 918
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/a/az;->M:Z

    .line 919
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    .line 921
    if-eqz p2, :cond_2

    .line 923
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->b:Lsoftware/simplicial/a/az;

    .line 924
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->c:Lsoftware/simplicial/a/az;

    .line 925
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->d:Lsoftware/simplicial/a/az;

    .line 926
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->e:Lsoftware/simplicial/a/az;

    .line 927
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->l:Lsoftware/simplicial/a/az;

    .line 928
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->f:Lsoftware/simplicial/a/az;

    .line 929
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->g:Lsoftware/simplicial/a/az;

    .line 930
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->h:Lsoftware/simplicial/a/az;

    .line 931
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->i:Lsoftware/simplicial/a/az;

    .line 932
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->j:Lsoftware/simplicial/a/az;

    .line 933
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->k:Lsoftware/simplicial/a/az;

    .line 934
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->m:Lsoftware/simplicial/a/az;

    .line 935
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->n:Lsoftware/simplicial/a/az;

    .line 936
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->o:Lsoftware/simplicial/a/az;

    .line 937
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->p:Lsoftware/simplicial/a/az;

    .line 938
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->q:Lsoftware/simplicial/a/az;

    .line 939
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->r:Lsoftware/simplicial/a/az;

    .line 940
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    sget-object v1, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    invoke-virtual {p2, v1}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/az;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->s:Lsoftware/simplicial/a/az;

    .line 964
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 965
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->b:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 967
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->c:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->d:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->e:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->i:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->j:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 972
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->k:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 973
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->f:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->g:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->h:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 976
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->l:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 977
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->m:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->n:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 979
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->o:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->p:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 981
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->q:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->r:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 983
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->s:Lsoftware/simplicial/a/az;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    return-void

    :cond_0
    move-object v0, v2

    .line 905
    :cond_1
    :try_start_2
    iget-object v2, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 906
    iget-object v0, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->bj:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 907
    iget-object v0, v1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    sget-object v2, Lsoftware/simplicial/a/d;->bp:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 908
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag$a;->t:Ljava/util/Set;

    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/d/d;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 909
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag$a;->u:Ljava/util/Set;

    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/d/d;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 910
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag$a;->w:Ljava/util/Map;

    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/d/d;->d()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 911
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag$a;->v:Ljava/util/Set;

    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/d/d;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 916
    goto/16 :goto_2

    .line 944
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->b:Lsoftware/simplicial/a/az;

    .line 945
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->c:Lsoftware/simplicial/a/az;

    .line 946
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->d:Lsoftware/simplicial/a/az;

    .line 947
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->e:Lsoftware/simplicial/a/az;

    .line 948
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->l:Lsoftware/simplicial/a/az;

    .line 949
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->f:Lsoftware/simplicial/a/az;

    .line 950
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->g:Lsoftware/simplicial/a/az;

    .line 951
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->h:Lsoftware/simplicial/a/az;

    .line 952
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->i:Lsoftware/simplicial/a/az;

    .line 953
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->j:Lsoftware/simplicial/a/az;

    .line 954
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->k:Lsoftware/simplicial/a/az;

    .line 955
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->m:Lsoftware/simplicial/a/az;

    .line 956
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->n:Lsoftware/simplicial/a/az;

    .line 957
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->o:Lsoftware/simplicial/a/az;

    .line 958
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->p:Lsoftware/simplicial/a/az;

    .line 959
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->q:Lsoftware/simplicial/a/az;

    .line 960
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->r:Lsoftware/simplicial/a/az;

    .line 961
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    new-instance v1, Lsoftware/simplicial/a/az;

    sget-object v2, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/am;)V

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag$a;->s:Lsoftware/simplicial/a/az;

    goto/16 :goto_3

    .line 913
    :catch_1
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto/16 :goto_1
.end method

.method public a(Landroid/content/SharedPreferences$Editor;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 181
    :try_start_0
    const-string v0, "soundEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 182
    const-string v0, "playerAlias"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 183
    const-string v0, "namesEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 184
    const-string v0, "avatarsEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 185
    const-string v0, "controlMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/c/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 186
    const-string v0, "buttonMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/c/a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 187
    const-string v0, "navButtonMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/c/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 188
    const-string v0, "region"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 189
    const-string v0, "singlePlayerAvatar"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    iget-object v2, v2, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 190
    const-string v0, "multiPlayerAvatar"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    iget-object v2, v2, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 191
    const-string v0, "singlePlayerEjectSkin"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    iget-byte v2, v2, Lsoftware/simplicial/a/af;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 192
    const-string v0, "multiPlayerEjectSkin"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    iget-byte v2, v2, Lsoftware/simplicial/a/af;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 193
    const-string v0, "singlePlayerHat"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    iget-byte v2, v2, Lsoftware/simplicial/a/as;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 194
    const-string v0, "multiPlayerHat"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    iget-byte v2, v2, Lsoftware/simplicial/a/as;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 195
    const-string v0, "singlePlayerPet"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    iget-byte v2, v2, Lsoftware/simplicial/a/bd;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 196
    const-string v0, "multiPlayerPet"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    iget-byte v2, v2, Lsoftware/simplicial/a/bd;->c:B

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 197
    const-string v0, "gameMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-virtual {v2}, Lsoftware/simplicial/a/am;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 198
    const-string v0, "gameSize"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    invoke-virtual {v2}, Lsoftware/simplicial/a/ap;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 199
    const-string v0, "theme"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->w:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/aj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 200
    const-string v0, "bordersEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->x:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 201
    const-string v0, "blobColor"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aE:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 202
    const-string v0, "blobColorEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aC:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 203
    const-string v0, "stickSize"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->y:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 204
    const-string v0, "stickMargin"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 205
    const-string v0, "buttonSize"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->A:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 206
    const-string v0, "buttonMargin"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->B:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 207
    const-string v0, "difficulty"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    invoke-virtual {v2}, Lsoftware/simplicial/a/ac;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 208
    const-string v0, "numberPlayersSP"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 209
    const-string v0, "delayDisconnect"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->E:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 210
    const-string v0, "maxPlayersPub"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->F:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 211
    const-string v0, "maxPlayersChat"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 212
    const-string v0, "maxPlayersPriv"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 213
    const-string v0, "minPlayersPriv"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->I:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 214
    const-string v0, "pubGameName"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->J:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 215
    const-string v0, "privGameName"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->K:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 216
    const-string v0, "chatName"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->L:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 217
    const-string v0, "loginTicket"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 218
    const-string v0, "accountName"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 219
    const-string v0, "googleProfilePicURL"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->O:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 220
    const-string v0, "dontShowFindingGroupsWarning"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->P:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 221
    const-string v0, "hatsEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->Q:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 222
    const-string v0, "emotesEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 223
    const-string v0, "petsEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->S:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 224
    const-string v0, "downloadSkins"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->T:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 225
    const-string v0, "specialEffectsEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->U:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 226
    const-string v0, "mushinessEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->V:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 227
    const-string v0, "inGameChatEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 228
    const-string v0, "inGameMailEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->X:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 229
    const-string v0, "desiredStatus"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    invoke-virtual {v2}, Lsoftware/simplicial/a/x;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 230
    const-string v0, "clanName"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    .line 231
    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 232
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clanColors["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    aget-byte v3, v3, v0

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_0
    const-string v0, "showClanNames"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ab:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 234
    const-string v0, "showLevels"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ac:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 235
    const-string v0, "speedClickToggle"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 236
    const-string v0, "chatMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    invoke-virtual {v2}, Lsoftware/simplicial/a/n;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 237
    const-string v0, "clanRole"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    invoke-virtual {v2}, Lsoftware/simplicial/a/q;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 238
    const-string v0, "gameDuration"

    iget-short v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 239
    const-string v0, "colorBlind"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ah:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 240
    const-string v0, "dontShowFFAClassicRules"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ai:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 241
    const-string v0, "cwMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    invoke-virtual {v2}, Lsoftware/simplicial/a/c/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 242
    const-string v0, "cwSize"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->am:Lsoftware/simplicial/a/c/g;

    invoke-virtual {v2}, Lsoftware/simplicial/a/c/g;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 243
    const-string v0, "showAllClanWars"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 244
    const-string v0, "arenaMode"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v2}, Lsoftware/simplicial/a/b/d;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 245
    const-string v0, "teamArenaSize"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->al:Lsoftware/simplicial/a/h/f;

    invoke-virtual {v2}, Lsoftware/simplicial/a/h/f;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 246
    const-string v0, "controlOpacity"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 247
    const-string v0, "uiOpacity"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ap:F

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 248
    const-string v0, "desiredLocaleLanguage"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 249
    const-string v0, "desiredLocaleCountry"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 250
    const-string v0, "useCustomAvatar"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 251
    const-string v0, "mayhem"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 252
    const-string v0, "challengesEnabled"

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/f/ag;->au:Z

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 253
    const-string v0, "skinURLBase"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aw:Ljava/lang/String;

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 254
    const-string v0, "customSkinID"

    iget v2, p0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    .line 255
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 256
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playerAliasColors["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    aget-byte v3, v3, v0

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 257
    :cond_1
    const-string v0, "blockedAccountCount"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move v2, v1

    .line 258
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 260
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/j;

    .line 261
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockedAID["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, v0, Lsoftware/simplicial/nebulous/f/j;->a:I

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 262
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockedName["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lsoftware/simplicial/nebulous/f/j;->b:Ljava/lang/String;

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 263
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "blockedShowAID["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/j;->c:Z

    invoke-interface {p1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 265
    :goto_3
    const/16 v2, 0xa

    if-ge v0, v2, :cond_3

    .line 269
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "namePresets["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 270
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nameColorPresets["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    aget-object v3, v3, v0

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 265
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 277
    :goto_5
    :try_start_2
    sget-object v1, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v1, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-ge v0, v1, :cond_4

    .line 281
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "petNames["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->aF:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 277
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 288
    :cond_4
    :try_start_4
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 294
    :goto_7
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 292
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_7

    .line 283
    :catch_1
    move-exception v1

    goto :goto_6

    .line 272
    :catch_2
    move-exception v2

    goto :goto_4
.end method

.method public a(Landroid/content/SharedPreferences;Landroid/app/Activity;)V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 300
    :try_start_0
    const-string v0, "loginTicket"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4f

    .line 307
    :goto_0
    :try_start_1
    const-string v0, "soundEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->d:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4e

    .line 314
    :goto_1
    :try_start_2
    const-string v0, "playerAlias"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Blob "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Random;-><init>(J)V

    const v5, 0x1869f

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4d

    .line 321
    :goto_2
    :try_start_3
    const-string v0, "namesEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->f:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4c

    .line 328
    :goto_3
    :try_start_4
    const-string v0, "avatarsEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->g:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4b

    .line 335
    :goto_4
    :try_start_5
    const-string v0, "bordersEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->x:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4a

    .line 342
    :goto_5
    :try_start_6
    const-string v0, "blobColor"

    new-instance v3, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/ba;->c(I)I

    move-result v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aE:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_49

    .line 349
    :goto_6
    :try_start_7
    const-string v0, "stickSize"

    const/16 v3, 0x32

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->y:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_48

    .line 356
    :goto_7
    :try_start_8
    const-string v0, "stickMargin"

    const/16 v3, 0x28

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->z:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_47

    .line 363
    :goto_8
    :try_start_9
    const-string v0, "buttonSize"

    const/16 v3, 0x32

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->A:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_46

    .line 370
    :goto_9
    :try_start_a
    const-string v0, "buttonMargin"

    const/16 v3, 0x14

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->B:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_45

    .line 377
    :goto_a
    :try_start_b
    const-string v0, "numberPlayersSP"

    const/16 v3, 0x12

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->D:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_44

    .line 384
    :goto_b
    :try_start_c
    const-string v0, "delayDisconnect"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->E:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_43

    .line 391
    :goto_c
    :try_start_d
    const-string v0, "maxPlayersPub"

    const/4 v3, 0x7

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->F:I
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_42

    .line 398
    :goto_d
    :try_start_e
    const-string v0, "maxPlayersChat"

    const/16 v3, 0x14

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->G:I
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_41

    .line 405
    :goto_e
    :try_start_f
    const-string v0, "maxPlayersPriv"

    const/16 v3, 0x15

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->H:I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_40

    .line 412
    :goto_f
    :try_start_10
    const-string v0, "minPlayersPriv"

    const/4 v3, 0x7

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->I:I
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3f

    .line 419
    :goto_10
    :try_start_11
    const-string v0, "pubGameName"

    const-string v3, ""

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->J:Ljava/lang/String;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3e

    .line 426
    :goto_11
    :try_start_12
    const-string v0, "privGameName"

    const-string v3, ""

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->K:Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3d

    .line 433
    :goto_12
    :try_start_13
    const-string v0, "chatName"

    const-string v3, ""

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->L:Ljava/lang/String;
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3c

    .line 440
    :goto_13
    :try_start_14
    const-string v0, "accountName"

    const-string v3, ""

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_3b

    .line 447
    :goto_14
    :try_start_15
    const-string v0, "googleProfilePicURL"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->O:Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3a

    .line 454
    :goto_15
    :try_start_16
    const-string v0, "dontShowFindingGroupsWarning"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->P:Z
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_39

    .line 461
    :goto_16
    :try_start_17
    const-string v0, "emotesEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->R:Z
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_38

    .line 468
    :goto_17
    :try_start_18
    const-string v0, "hatsEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->Q:Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_37

    .line 475
    :goto_18
    :try_start_19
    const-string v0, "petsEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->S:Z
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_36

    .line 482
    :goto_19
    :try_start_1a
    const-string v0, "downloadSkins"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->T:Z
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_35

    .line 489
    :goto_1a
    :try_start_1b
    const-string v0, "specialEffectsEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->U:Z
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_34

    .line 496
    :goto_1b
    :try_start_1c
    const-string v0, "mushinessEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->V:Z
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_33

    .line 503
    :goto_1c
    :try_start_1d
    const-string v0, "inGameChatEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->W:Z
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_32

    .line 510
    :goto_1d
    :try_start_1e
    const-string v0, "inGameMailEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->X:Z
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_31

    .line 517
    :goto_1e
    :try_start_1f
    const-string v0, "clanName"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_30

    :goto_1f
    move v0, v1

    .line 522
    :goto_20
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 526
    :try_start_20
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "clanColors["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-byte v5, Lsoftware/simplicial/nebulous/f/ag;->a:B

    invoke-interface {p1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_2f

    .line 522
    :goto_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_20

    .line 534
    :cond_0
    :try_start_21
    const-string v0, "showClanNames"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ab:Z
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_2e

    .line 541
    :goto_22
    :try_start_22
    const-string v0, "showLevels"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ac:Z
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_2d

    .line 548
    :goto_23
    :try_start_23
    const-string v0, "speedClickToggle"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ad:Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_2c

    .line 555
    :goto_24
    :try_start_24
    const-string v0, "gameDuration"

    const/16 v3, 0x12c

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ag:S
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_2b

    .line 562
    :goto_25
    :try_start_25
    const-string v0, "colorBlind"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ah:Z
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_2a

    .line 569
    :goto_26
    :try_start_26
    const-string v0, "dontShowFFAClassicRules"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ai:Z
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_29

    .line 576
    :goto_27
    :try_start_27
    const-string v0, "showAllClanWars"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->an:Z
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_28

    .line 583
    :goto_28
    :try_start_28
    const-string v0, "controlOpacity"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ao:F
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_27

    .line 590
    :goto_29
    :try_start_29
    const-string v0, "uiOpacity"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ap:F
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_26

    .line 597
    :goto_2a
    :try_start_2a
    const-string v0, "desiredLocaleLanguage"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_25

    .line 604
    :goto_2b
    :try_start_2b
    const-string v0, "desiredLocaleCountry"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_24

    .line 611
    :goto_2c
    :try_start_2c
    const-string v0, "useCustomAvatar"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->as:Z
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_23

    .line 618
    :goto_2d
    :try_start_2d
    const-string v0, "customSkinID"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ag;->t:I
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_22

    .line 625
    :goto_2e
    :try_start_2e
    const-string v0, "mayhem"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->at:Z
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_21

    .line 633
    :goto_2f
    :try_start_2f
    const-string v0, "arenaMode"

    sget-object v3, Lsoftware/simplicial/a/b/d;->c:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v3}, Lsoftware/simplicial/a/b/d;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/b/d;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/b/d;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_20

    .line 640
    :goto_30
    :try_start_30
    const-string v0, "teamArenaSize"

    sget-object v3, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    invoke-virtual {v3}, Lsoftware/simplicial/a/h/f;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/h/f;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/h/f;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->al:Lsoftware/simplicial/a/h/f;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_30} :catch_1f

    .line 647
    :goto_31
    :try_start_31
    const-string v0, "desiredStatus"

    sget-object v3, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    invoke-virtual {v3}, Lsoftware/simplicial/a/x;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/x;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/x;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_31} :catch_1e

    .line 654
    :goto_32
    :try_start_32
    const-string v0, "cwMode"

    sget-object v3, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    invoke-virtual {v3}, Lsoftware/simplicial/a/am;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/c/e;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/c/e;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_32} :catch_1d

    .line 661
    :goto_33
    :try_start_33
    const-string v0, "cwSize"

    sget-object v3, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    invoke-virtual {v3}, Lsoftware/simplicial/a/c/g;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/c/g;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/c/g;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->am:Lsoftware/simplicial/a/c/g;
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_1c

    .line 668
    :goto_34
    :try_start_34
    const-string v0, "chatMode"

    sget-object v3, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    invoke-virtual {v3}, Lsoftware/simplicial/a/n;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/n;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/n;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_34} :catch_1b

    .line 675
    :goto_35
    :try_start_35
    const-string v0, "clanRole"

    sget-object v3, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-virtual {v3}, Lsoftware/simplicial/a/q;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_1a

    .line 682
    :goto_36
    :try_start_36
    const-string v0, "region"

    invoke-static {}, Lsoftware/simplicial/nebulous/f/aa;->a()Lsoftware/simplicial/a/bn;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/bn;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/bn;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/bn;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_36} :catch_19

    .line 689
    :goto_37
    :try_start_37
    const-string v0, "controlMode"

    sget-object v3, Lsoftware/simplicial/nebulous/c/b;->a:Lsoftware/simplicial/nebulous/c/b;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/b;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/c/b;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/c/b;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_18

    .line 696
    :goto_38
    :try_start_38
    const-string v0, "buttonMode"

    sget-object v3, Lsoftware/simplicial/nebulous/c/a;->a:Lsoftware/simplicial/nebulous/c/a;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/a;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/c/a;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/c/a;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_38} :catch_17

    .line 703
    :goto_39
    :try_start_39
    const-string v0, "navButtonMode"

    sget-object v3, Lsoftware/simplicial/nebulous/c/e;->a:Lsoftware/simplicial/nebulous/c/e;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/e;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/c/e;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/c/e;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_39} :catch_16

    .line 710
    :goto_3a
    :try_start_3a
    const-string v0, "gameSize"

    sget-object v3, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    invoke-virtual {v3}, Lsoftware/simplicial/a/ap;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ap;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3a} :catch_15

    .line 717
    :goto_3b
    :try_start_3b
    const-string v0, "theme"

    sget-object v3, Lsoftware/simplicial/nebulous/f/aj;->a:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/aj;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/aj;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/aj;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->w:Lsoftware/simplicial/nebulous/f/aj;
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3b} :catch_14

    .line 724
    :goto_3c
    :try_start_3c
    const-string v0, "difficulty"

    sget-object v3, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    invoke-virtual {v3}, Lsoftware/simplicial/a/ac;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ac;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/ac;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3c} :catch_13

    .line 731
    :goto_3d
    :try_start_3d
    sget-object v0, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    const-string v3, "singlePlayerAvatar"

    sget-object v4, Lsoftware/simplicial/a/f;->a:Lsoftware/simplicial/a/f;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lsoftware/simplicial/a/f;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_3d} :catch_12

    .line 738
    :goto_3e
    :try_start_3e
    sget-object v0, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    const-string v3, "multiPlayerAvatar"

    sget-object v4, Lsoftware/simplicial/a/f;->a:Lsoftware/simplicial/a/f;

    invoke-virtual {v4}, Lsoftware/simplicial/a/f;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lsoftware/simplicial/a/f;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/f;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3e} :catch_11

    .line 745
    :goto_3f
    :try_start_3f
    const-string v0, "singlePlayerEjectSkin"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_3f} :catch_10

    .line 752
    :goto_40
    :try_start_40
    const-string v0, "multiPlayerEjectSkin"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_40} :catch_f

    .line 759
    :goto_41
    :try_start_41
    const-string v0, "singlePlayerHat"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_41} :catch_e

    .line 766
    :goto_42
    :try_start_42
    const-string v0, "multiPlayerHat"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_42} :catch_d

    .line 773
    :goto_43
    :try_start_43
    const-string v0, "singlePlayerPet"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_43} :catch_c

    .line 780
    :goto_44
    :try_start_44
    const-string v0, "multiPlayerPet"

    const/4 v3, -0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_44} :catch_b

    .line 787
    :goto_45
    :try_start_45
    const-string v0, "gameMode"

    sget-object v3, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    invoke-virtual {v3}, Lsoftware/simplicial/a/am;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/am;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/am;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_45} :catch_a

    .line 794
    :goto_46
    :try_start_46
    const-string v0, "challengesEnabled"

    const/4 v3, 0x1

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->au:Z
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_46} :catch_9

    .line 801
    :goto_47
    :try_start_47
    const-string v0, "skinURLBase"

    const-string v3, "https://s3.amazonaws.com/simplicialsoftware.skins/"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aw:Ljava/lang/String;
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_47} :catch_8

    :goto_48
    move v0, v1

    .line 806
    :goto_49
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 810
    :try_start_48
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "playerAliasColors["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-interface {p1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v3, v0
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_48 .. :try_end_48} :catch_7

    .line 806
    :goto_4a
    add-int/lit8 v0, v0, 0x1

    goto :goto_49

    .line 819
    :cond_1
    :try_start_49
    const-string v0, "blockedAccountCount"

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_49} :catch_5

    move-result v0

    .line 820
    :try_start_4a
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    .line 821
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    iput-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_4a .. :try_end_4a} :catch_6

    :goto_4b
    move v3, v1

    .line 827
    :goto_4c
    if-ge v3, v0, :cond_2

    .line 831
    :try_start_4b
    new-instance v4, Lsoftware/simplicial/nebulous/f/j;

    invoke-direct {v4}, Lsoftware/simplicial/nebulous/f/j;-><init>()V

    .line 832
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "blockedAID["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/nebulous/f/j;->a:I

    .line 833
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "blockedName["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/nebulous/f/j;->b:Ljava/lang/String;

    .line 834
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "blockedShowAID["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {p1, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, v4, Lsoftware/simplicial/nebulous/f/j;->c:Z

    .line 835
    iget-object v5, p0, Lsoftware/simplicial/nebulous/f/ag;->ay:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 836
    iget-object v5, p0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v4, v4, Lsoftware/simplicial/nebulous/f/j;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_4b
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_4b} :catch_4

    .line 827
    :goto_4d
    add-int/lit8 v3, v3, 0x1

    goto :goto_4c

    :cond_2
    move v0, v1

    .line 844
    :goto_4e
    const/16 v3, 0xa

    if-ge v0, v3, :cond_3

    .line 848
    :try_start_4c
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "namePresets["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "---"

    invoke-interface {p1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 849
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nameColorPresets["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [B

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    aput-object v4, v3, v0
    :try_end_4c
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_4c} :catch_3

    .line 844
    :goto_4f
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 857
    :cond_3
    :goto_50
    sget-object v0, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 861
    :try_start_4d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aF:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "petNames["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_4d} :catch_2

    .line 857
    :goto_51
    add-int/lit8 v1, v1, 0x1

    goto :goto_50

    .line 872
    :cond_4
    :try_start_4e
    const-string v0, "Client.VERSION"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_4e} :catch_0

    move-result v0

    .line 879
    :goto_52
    const/16 v1, 0x181

    if-eq v0, v1, :cond_5

    .line 881
    :try_start_4f
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->aD:Lsoftware/simplicial/nebulous/f/ag$b;

    invoke-interface {v1, v0}, Lsoftware/simplicial/nebulous/f/ag$b;->d(I)V

    .line 882
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/app/Activity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 883
    const-string v1, "Client.VERSION"

    const/16 v2, 0x181

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 884
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_4f} :catch_1

    .line 890
    :cond_5
    :goto_53
    return-void

    .line 874
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_52

    .line 887
    :catch_1
    move-exception v0

    goto :goto_53

    .line 863
    :catch_2
    move-exception v0

    goto :goto_51

    .line 851
    :catch_3
    move-exception v3

    goto :goto_4f

    .line 838
    :catch_4
    move-exception v4

    goto/16 :goto_4d

    .line 823
    :catch_5
    move-exception v0

    move v0, v1

    goto/16 :goto_4b

    :catch_6
    move-exception v3

    goto/16 :goto_4b

    .line 812
    :catch_7
    move-exception v3

    goto/16 :goto_4a

    .line 803
    :catch_8
    move-exception v0

    goto/16 :goto_48

    .line 796
    :catch_9
    move-exception v0

    goto/16 :goto_47

    .line 789
    :catch_a
    move-exception v0

    goto/16 :goto_46

    .line 782
    :catch_b
    move-exception v0

    goto/16 :goto_45

    .line 775
    :catch_c
    move-exception v0

    goto/16 :goto_44

    .line 768
    :catch_d
    move-exception v0

    goto/16 :goto_43

    .line 761
    :catch_e
    move-exception v0

    goto/16 :goto_42

    .line 754
    :catch_f
    move-exception v0

    goto/16 :goto_41

    .line 747
    :catch_10
    move-exception v0

    goto/16 :goto_40

    .line 740
    :catch_11
    move-exception v0

    goto/16 :goto_3f

    .line 733
    :catch_12
    move-exception v0

    goto/16 :goto_3e

    .line 726
    :catch_13
    move-exception v0

    goto/16 :goto_3d

    .line 719
    :catch_14
    move-exception v0

    goto/16 :goto_3c

    .line 712
    :catch_15
    move-exception v0

    goto/16 :goto_3b

    .line 705
    :catch_16
    move-exception v0

    goto/16 :goto_3a

    .line 698
    :catch_17
    move-exception v0

    goto/16 :goto_39

    .line 691
    :catch_18
    move-exception v0

    goto/16 :goto_38

    .line 684
    :catch_19
    move-exception v0

    goto/16 :goto_37

    .line 677
    :catch_1a
    move-exception v0

    goto/16 :goto_36

    .line 670
    :catch_1b
    move-exception v0

    goto/16 :goto_35

    .line 663
    :catch_1c
    move-exception v0

    goto/16 :goto_34

    .line 656
    :catch_1d
    move-exception v0

    goto/16 :goto_33

    .line 649
    :catch_1e
    move-exception v0

    goto/16 :goto_32

    .line 642
    :catch_1f
    move-exception v0

    goto/16 :goto_31

    .line 635
    :catch_20
    move-exception v0

    goto/16 :goto_30

    .line 627
    :catch_21
    move-exception v0

    goto/16 :goto_2f

    .line 620
    :catch_22
    move-exception v0

    goto/16 :goto_2e

    .line 613
    :catch_23
    move-exception v0

    goto/16 :goto_2d

    .line 606
    :catch_24
    move-exception v0

    goto/16 :goto_2c

    .line 599
    :catch_25
    move-exception v0

    goto/16 :goto_2b

    .line 592
    :catch_26
    move-exception v0

    goto/16 :goto_2a

    .line 585
    :catch_27
    move-exception v0

    goto/16 :goto_29

    .line 578
    :catch_28
    move-exception v0

    goto/16 :goto_28

    .line 571
    :catch_29
    move-exception v0

    goto/16 :goto_27

    .line 564
    :catch_2a
    move-exception v0

    goto/16 :goto_26

    .line 557
    :catch_2b
    move-exception v0

    goto/16 :goto_25

    .line 550
    :catch_2c
    move-exception v0

    goto/16 :goto_24

    .line 543
    :catch_2d
    move-exception v0

    goto/16 :goto_23

    .line 536
    :catch_2e
    move-exception v0

    goto/16 :goto_22

    .line 528
    :catch_2f
    move-exception v3

    goto/16 :goto_21

    .line 519
    :catch_30
    move-exception v0

    goto/16 :goto_1f

    .line 512
    :catch_31
    move-exception v0

    goto/16 :goto_1e

    .line 505
    :catch_32
    move-exception v0

    goto/16 :goto_1d

    .line 498
    :catch_33
    move-exception v0

    goto/16 :goto_1c

    .line 491
    :catch_34
    move-exception v0

    goto/16 :goto_1b

    .line 484
    :catch_35
    move-exception v0

    goto/16 :goto_1a

    .line 477
    :catch_36
    move-exception v0

    goto/16 :goto_19

    .line 470
    :catch_37
    move-exception v0

    goto/16 :goto_18

    .line 463
    :catch_38
    move-exception v0

    goto/16 :goto_17

    .line 456
    :catch_39
    move-exception v0

    goto/16 :goto_16

    .line 449
    :catch_3a
    move-exception v0

    goto/16 :goto_15

    .line 442
    :catch_3b
    move-exception v0

    goto/16 :goto_14

    .line 435
    :catch_3c
    move-exception v0

    goto/16 :goto_13

    .line 428
    :catch_3d
    move-exception v0

    goto/16 :goto_12

    .line 421
    :catch_3e
    move-exception v0

    goto/16 :goto_11

    .line 414
    :catch_3f
    move-exception v0

    goto/16 :goto_10

    .line 407
    :catch_40
    move-exception v0

    goto/16 :goto_f

    .line 400
    :catch_41
    move-exception v0

    goto/16 :goto_e

    .line 393
    :catch_42
    move-exception v0

    goto/16 :goto_d

    .line 386
    :catch_43
    move-exception v0

    goto/16 :goto_c

    .line 379
    :catch_44
    move-exception v0

    goto/16 :goto_b

    .line 372
    :catch_45
    move-exception v0

    goto/16 :goto_a

    .line 365
    :catch_46
    move-exception v0

    goto/16 :goto_9

    .line 358
    :catch_47
    move-exception v0

    goto/16 :goto_8

    .line 351
    :catch_48
    move-exception v0

    goto/16 :goto_7

    .line 344
    :catch_49
    move-exception v0

    goto/16 :goto_6

    .line 337
    :catch_4a
    move-exception v0

    goto/16 :goto_5

    .line 330
    :catch_4b
    move-exception v0

    goto/16 :goto_4

    .line 323
    :catch_4c
    move-exception v0

    goto/16 :goto_3

    .line 316
    :catch_4d
    move-exception v0

    goto/16 :goto_2

    .line 309
    :catch_4e
    move-exception v0

    goto/16 :goto_1

    .line 302
    :catch_4f
    move-exception v0

    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1031
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1032
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1034
    const/4 v2, 0x0

    .line 1037
    :try_start_1
    new-instance v3, Ljava/io/File;

    const-string v1, "LOCAL_CUSTOM_SKIN"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1038
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1039
    :try_start_2
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x50

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1047
    if-eqz v1, :cond_0

    .line 1048
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1055
    :cond_0
    :goto_0
    return-void

    .line 1041
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1043
    :goto_1
    :try_start_4
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1047
    if-eqz v1, :cond_0

    .line 1048
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 1051
    :catch_1
    move-exception v0

    .line 1053
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1047
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_1

    .line 1048
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_1
    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 1047
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1041
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public a(Lsoftware/simplicial/a/bd;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aF:[Ljava/lang/String;

    iget-byte v1, p1, Lsoftware/simplicial/a/bd;->c:B

    aput-object p2, v0, v1

    .line 1286
    return-void
.end method

.method public a([B)V
    .locals 3

    .prologue
    .line 1232
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1234
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 1235
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    aget-byte v2, p1, v0

    aput-byte v2, v1, v0

    .line 1232
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1237
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    sget-byte v2, Lsoftware/simplicial/nebulous/f/ag;->a:B

    aput-byte v2, v1, v0

    goto :goto_1

    .line 1239
    :cond_1
    return-void
.end method

.method public a()[B
    .locals 3

    .prologue
    .line 1171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1255
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aC:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/f/ag;->aE:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1267
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/f/ag;->a(Lsoftware/simplicial/a/bd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1280
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/f/ag;->a(Lsoftware/simplicial/a/bd;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
