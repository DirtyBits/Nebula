.class Lsoftware/simplicial/nebulous/f/al$56;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Ljava/util/Map;IZLsoftware/simplicial/nebulous/f/al$k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al$k;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;ILjava/lang/String;Lsoftware/simplicial/nebulous/f/al$k;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$56;->e:Lsoftware/simplicial/nebulous/f/al;

    iput p2, p0, Lsoftware/simplicial/nebulous/f/al$56;->a:I

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$56;->b:Ljava/lang/String;

    iput-object p4, p0, Lsoftware/simplicial/nebulous/f/al$56;->c:Lsoftware/simplicial/nebulous/f/al$k;

    iput-object p5, p0, Lsoftware/simplicial/nebulous/f/al$56;->d:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 499
    move-object v2, v3

    move-object v4, v3

    move-object v5, v3

    .line 504
    :goto_0
    iget v1, p0, Lsoftware/simplicial/nebulous/f/al$56;->a:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_f

    .line 508
    add-int/lit8 v1, v0, 0x1

    .line 509
    const/4 v0, 0x2

    if-le v1, v0, :cond_0

    .line 512
    const-wide/16 v6, 0xbb8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 515
    :cond_0
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 516
    const-string v6, "UTF-8"

    invoke-virtual {v0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 518
    const-string v0, "https://www.simplicialsoftware.com"

    .line 519
    invoke-static {}, Lsoftware/simplicial/nebulous/f/al;->i()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 520
    const-string v0, "http://52.6.241.44"

    .line 522
    :cond_1
    new-instance v7, Ljava/net/URL;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v8, p0, Lsoftware/simplicial/nebulous/f/al$56;->b:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 523
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 525
    const/16 v5, 0x1d4c

    :try_start_1
    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 526
    const/16 v5, 0x2710

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 528
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 529
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 530
    const-string v5, "POST"

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 532
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    move-result-object v5

    .line 533
    :try_start_2
    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write([B)V

    .line 535
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 536
    invoke-static {v4}, Lsoftware/simplicial/a/f/bo;->a(Ljava/io/InputStream;)[B

    move-result-object v6

    .line 537
    new-instance v2, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v2, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 586
    if-eqz v4, :cond_2

    .line 587
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 589
    :cond_2
    if-eqz v5, :cond_3

    .line 590
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 592
    :cond_3
    if-eqz v0, :cond_4

    .line 593
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_4
    :goto_1
    move-object v0, v2

    .line 602
    :goto_2
    return-object v0

    .line 595
    :catch_0
    move-exception v0

    .line 597
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 539
    :catch_1
    move-exception v2

    move-object v10, v2

    move-object v2, v4

    move-object v4, v0

    move-object v0, v5

    move-object v5, v10

    .line 541
    :goto_3
    const/4 v6, 0x0

    :try_start_4
    invoke-static {v6}, Lsoftware/simplicial/nebulous/f/al;->l(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 543
    if-eqz v4, :cond_11

    .line 545
    const/4 v7, 0x0

    .line 548
    :try_start_5
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    .line 549
    if-eqz v2, :cond_10

    .line 551
    invoke-static {v2}, Lsoftware/simplicial/a/f/bo;->a(Ljava/io/InputStream;)[B

    move-result-object v8

    .line 552
    new-instance v6, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-direct {v6, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 562
    :goto_4
    if-eqz v3, :cond_5

    .line 563
    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 572
    :cond_5
    :goto_5
    :try_start_7
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-class v8, Ljava/net/UnknownHostException;

    if-ne v7, v8, :cond_6

    .line 574
    invoke-static {}, Lsoftware/simplicial/nebulous/f/al;->i()Z

    move-result v7

    if-nez v7, :cond_6

    .line 575
    const/4 v7, 0x1

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/al;->b(Z)Z

    .line 578
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lsoftware/simplicial/nebulous/f/al;->l(Ljava/lang/String;)Ljava/lang/String;

    .line 580
    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 586
    if-eqz v2, :cond_7

    .line 587
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 589
    :cond_7
    if-eqz v0, :cond_8

    .line 590
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 592
    :cond_8
    if-eqz v4, :cond_9

    .line 593
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_9
    :goto_6
    move-object v5, v4

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    .line 598
    goto/16 :goto_0

    .line 555
    :catch_2
    move-exception v6

    .line 562
    if-eqz v3, :cond_a

    .line 563
    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_a
    move-object v6, v3

    .line 568
    goto :goto_5

    .line 565
    :catch_3
    move-exception v6

    move-object v6, v3

    .line 569
    goto :goto_5

    .line 560
    :catchall_0
    move-exception v1

    .line 562
    if-eqz v3, :cond_b

    .line 563
    :try_start_a
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 568
    :cond_b
    :goto_7
    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 584
    :catchall_1
    move-exception v1

    move-object v5, v4

    move-object v4, v2

    move-object v2, v0

    move-object v0, v1

    .line 586
    :goto_8
    if-eqz v4, :cond_c

    .line 587
    :try_start_c
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 589
    :cond_c
    if-eqz v2, :cond_d

    .line 590
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 592
    :cond_d
    if-eqz v5, :cond_e

    .line 593
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    .line 598
    :cond_e
    :goto_9
    throw v0

    .line 595
    :catch_4
    move-exception v5

    .line 597
    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6

    .line 595
    :catch_5
    move-exception v1

    .line 597
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_9

    .line 602
    :cond_f
    const-string v0, "ERROR"

    goto/16 :goto_2

    .line 565
    :catch_6
    move-exception v7

    goto/16 :goto_5

    :catch_7
    move-exception v3

    goto :goto_7

    .line 584
    :catchall_2
    move-exception v1

    move-object v2, v5

    move-object v5, v0

    move-object v0, v1

    goto :goto_8

    :catchall_3
    move-exception v0

    goto :goto_8

    :catchall_4
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    goto :goto_8

    .line 539
    :catch_8
    move-exception v0

    move-object v10, v0

    move-object v0, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v10

    goto/16 :goto_3

    :catch_9
    move-exception v5

    move-object v10, v2

    move-object v2, v4

    move-object v4, v0

    move-object v0, v10

    goto/16 :goto_3

    :cond_10
    move-object v6, v3

    goto/16 :goto_4

    :cond_11
    move-object v6, v3

    goto/16 :goto_5
.end method

.method protected a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 608
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$56;->e:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    .line 609
    if-nez v2, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 614
    :cond_1
    :try_start_0
    const-string v1, "ERROR"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 616
    iget-object v1, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v1, p1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 635
    :goto_1
    if-eqz v0, :cond_0

    .line 639
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$56;->c:Lsoftware/simplicial/nebulous/f/al$k;

    invoke-interface {v1, v0}, Lsoftware/simplicial/nebulous/f/al$k;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 641
    :catch_0
    move-exception v0

    .line 643
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$56;->e:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$56;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 644
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 620
    :cond_2
    :try_start_2
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 621
    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 623
    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 624
    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 629
    :catch_1
    move-exception v1

    .line 631
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 495
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$56;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 495
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$56;->a(Ljava/lang/String;)V

    return-void
.end method
