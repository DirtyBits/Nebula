.class Lsoftware/simplicial/nebulous/f/v$3;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/v;->a(Ljava/util/List;Lsoftware/simplicial/nebulous/f/al$w;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lsoftware/simplicial/a/ax;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al$w;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/v;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/v;Ljava/util/List;Lsoftware/simplicial/nebulous/f/al$w;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/v$3;->c:Lsoftware/simplicial/nebulous/f/v;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/v$3;->a:Ljava/util/List;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/v$3;->b:Lsoftware/simplicial/nebulous/f/al$w;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/ax;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 375
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 376
    const-string v3, "ITEM_ID_LIST"

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v$3;->a:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 381
    :try_start_0
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/v$3;->c:Lsoftware/simplicial/nebulous/f/v;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v$3;->c:Lsoftware/simplicial/nebulous/f/v;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/v;->a(Lsoftware/simplicial/nebulous/f/v;)Lcom/a/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v$3;->c:Lsoftware/simplicial/nebulous/f/v;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/v;->a(Lsoftware/simplicial/nebulous/f/v;)Lcom/a/a/a/a;

    move-result-object v0

    const/4 v4, 0x3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/f/v$3;->c:Lsoftware/simplicial/nebulous/f/v;

    invoke-static {v5}, Lsoftware/simplicial/nebulous/f/v;->b(Lsoftware/simplicial/nebulous/f/v;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "inapp"

    invoke-interface {v0, v4, v5, v6, v2}, Lcom/a/a/a/a;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 387
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 394
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 395
    if-nez v2, :cond_3

    .line 397
    const-string v2, "DETAILS_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 398
    if-nez v0, :cond_1

    move-object v0, v1

    .line 426
    :goto_0
    return-object v0

    .line 386
    :cond_0
    :try_start_2
    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    .line 387
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 389
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 391
    goto :goto_0

    .line 400
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 401
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 404
    new-instance v3, Lsoftware/simplicial/a/ax;

    invoke-direct {v3}, Lsoftware/simplicial/a/ax;-><init>()V

    .line 407
    :try_start_4
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 409
    const-string v0, "productId"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 410
    const-string v5, "price"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 412
    iput-object v0, v3, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    .line 413
    iput-object v4, v3, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    .line 415
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 417
    :catch_1
    move-exception v0

    .line 419
    sget-object v3, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 423
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 426
    goto :goto_0
.end method

.method protected a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/ax;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 432
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v$3;->b:Lsoftware/simplicial/nebulous/f/al$w;

    invoke-interface {v0, p1}, Lsoftware/simplicial/nebulous/f/al$w;->a(Ljava/util/ArrayList;)V

    .line 433
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 371
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/v$3;->a([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 371
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/v$3;->a(Ljava/util/ArrayList;)V

    return-void
.end method
