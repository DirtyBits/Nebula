.class public final enum Lsoftware/simplicial/nebulous/f/aj;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/f/aj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/f/aj;

.field public static final enum b:Lsoftware/simplicial/nebulous/f/aj;

.field public static final enum c:Lsoftware/simplicial/nebulous/f/aj;

.field public static final enum d:Lsoftware/simplicial/nebulous/f/aj;

.field public static final e:[Lsoftware/simplicial/nebulous/f/aj;

.field private static final synthetic f:[Lsoftware/simplicial/nebulous/f/aj;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/nebulous/f/aj;

    const-string v1, "SPACE_DARK"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->a:Lsoftware/simplicial/nebulous/f/aj;

    new-instance v0, Lsoftware/simplicial/nebulous/f/aj;

    const-string v1, "GRID_DARK"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/f/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->b:Lsoftware/simplicial/nebulous/f/aj;

    new-instance v0, Lsoftware/simplicial/nebulous/f/aj;

    const-string v1, "GRID_WHITE"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/f/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->c:Lsoftware/simplicial/nebulous/f/aj;

    new-instance v0, Lsoftware/simplicial/nebulous/f/aj;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/f/aj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->d:Lsoftware/simplicial/nebulous/f/aj;

    .line 6
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/nebulous/f/aj;

    sget-object v1, Lsoftware/simplicial/nebulous/f/aj;->a:Lsoftware/simplicial/nebulous/f/aj;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/f/aj;->b:Lsoftware/simplicial/nebulous/f/aj;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/f/aj;->c:Lsoftware/simplicial/nebulous/f/aj;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/f/aj;->d:Lsoftware/simplicial/nebulous/f/aj;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->f:[Lsoftware/simplicial/nebulous/f/aj;

    .line 9
    invoke-static {}, Lsoftware/simplicial/nebulous/f/aj;->values()[Lsoftware/simplicial/nebulous/f/aj;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/f/aj;->e:[Lsoftware/simplicial/nebulous/f/aj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/aj;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/nebulous/f/aj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/aj;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/f/aj;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/nebulous/f/aj;->f:[Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/f/aj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/f/aj;

    return-object v0
.end method
