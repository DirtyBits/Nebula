.class Lsoftware/simplicial/nebulous/f/al$31;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$l;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$l;)V
    .locals 0

    .prologue
    .line 2334
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$31;->a:Lsoftware/simplicial/nebulous/f/al$l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2340
    if-eqz p1, :cond_1

    .line 2342
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2345
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2346
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 2347
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$31;->a:Lsoftware/simplicial/nebulous/f/al$l;

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al$l;->a(ZLjava/lang/String;I)V

    .line 2373
    :goto_0
    return-void

    .line 2351
    :cond_0
    const-string v0, "ItemType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2352
    const-string v1, "ItemID"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2353
    const-string v2, "CoinsSpent"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 2354
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v4, "Coins"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2355
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v4, "ClanCoins"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2357
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->c(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/f/e;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;I)V

    .line 2359
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/al$31;->a:Lsoftware/simplicial/nebulous/f/al$l;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v0, v1}, Lsoftware/simplicial/nebulous/f/al$l;->a(ZLjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2368
    :catch_0
    move-exception v0

    .line 2370
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$31;->a:Lsoftware/simplicial/nebulous/f/al$l;

    const-string v2, ""

    invoke-interface {v1, v6, v2, v6}, Lsoftware/simplicial/nebulous/f/al$l;->a(ZLjava/lang/String;I)V

    .line 2371
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to make coin purchase: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2364
    :cond_1
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$31;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080027

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 2365
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$31;->a:Lsoftware/simplicial/nebulous/f/al$l;

    const/4 v1, 0x0

    const-string v2, ""

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al$l;->a(ZLjava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
