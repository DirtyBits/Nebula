.class Lsoftware/simplicial/nebulous/f/al$67;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$z;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al$z;

.field final synthetic d:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;ILjava/lang/String;Lsoftware/simplicial/nebulous/f/al$z;)V
    .locals 0

    .prologue
    .line 663
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$67;->d:Lsoftware/simplicial/nebulous/f/al;

    iput p2, p0, Lsoftware/simplicial/nebulous/f/al$67;->a:I

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$67;->b:Ljava/lang/String;

    iput-object p4, p0, Lsoftware/simplicial/nebulous/f/al$67;->c:Lsoftware/simplicial/nebulous/f/al$z;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 667
    move-object v2, v3

    move-object v4, v3

    move-object v5, v3

    .line 672
    :goto_0
    iget v1, p0, Lsoftware/simplicial/nebulous/f/al$67;->a:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_f

    .line 676
    add-int/lit8 v1, v0, 0x1

    .line 677
    const/4 v0, 0x2

    if-le v1, v0, :cond_0

    .line 680
    const-wide/16 v6, 0xbb8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 683
    :cond_0
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 684
    const-string v6, "UTF-8"

    invoke-virtual {v0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 686
    const-string v0, "https://www.simplicialsoftware.com"

    .line 687
    invoke-static {}, Lsoftware/simplicial/nebulous/f/al;->i()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 688
    const-string v0, "http://52.6.241.44"

    .line 690
    :cond_1
    new-instance v7, Ljava/net/URL;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v8, p0, Lsoftware/simplicial/nebulous/f/al$67;->b:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 691
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 693
    const/16 v5, 0x1d4c

    :try_start_1
    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 694
    const/16 v5, 0x2710

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 696
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 697
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 698
    const-string v5, "POST"

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 700
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    move-result-object v5

    .line 701
    :try_start_2
    invoke-virtual {v5, v6}, Ljava/io/OutputStream;->write([B)V

    .line 703
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 704
    invoke-static {v4}, Lsoftware/simplicial/a/f/bo;->a(Ljava/io/InputStream;)[B

    move-result-object v6

    .line 705
    new-instance v2, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v2, v6, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 754
    if-eqz v4, :cond_2

    .line 755
    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 757
    :cond_2
    if-eqz v5, :cond_3

    .line 758
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 760
    :cond_3
    if-eqz v0, :cond_4

    .line 761
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_4
    :goto_1
    move-object v0, v2

    .line 770
    :goto_2
    return-object v0

    .line 763
    :catch_0
    move-exception v0

    .line 765
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 707
    :catch_1
    move-exception v2

    move-object v10, v2

    move-object v2, v4

    move-object v4, v0

    move-object v0, v5

    move-object v5, v10

    .line 709
    :goto_3
    const/4 v6, 0x0

    :try_start_4
    invoke-static {v6}, Lsoftware/simplicial/nebulous/f/al;->l(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 711
    if-eqz v4, :cond_11

    .line 713
    const/4 v7, 0x0

    .line 716
    :try_start_5
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v2

    .line 717
    if-eqz v2, :cond_10

    .line 719
    invoke-static {v2}, Lsoftware/simplicial/a/f/bo;->a(Ljava/io/InputStream;)[B

    move-result-object v8

    .line 720
    new-instance v6, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-direct {v6, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 730
    :goto_4
    if-eqz v3, :cond_5

    .line 731
    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 740
    :cond_5
    :goto_5
    :try_start_7
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-class v8, Ljava/net/UnknownHostException;

    if-ne v7, v8, :cond_6

    .line 742
    invoke-static {}, Lsoftware/simplicial/nebulous/f/al;->i()Z

    move-result v7

    if-nez v7, :cond_6

    .line 743
    const/4 v7, 0x1

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/al;->b(Z)Z

    .line 746
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lsoftware/simplicial/nebulous/f/al;->l(Ljava/lang/String;)Ljava/lang/String;

    .line 748
    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 754
    if-eqz v2, :cond_7

    .line 755
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 757
    :cond_7
    if-eqz v0, :cond_8

    .line 758
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 760
    :cond_8
    if-eqz v4, :cond_9

    .line 761
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_9
    :goto_6
    move-object v5, v4

    move-object v4, v2

    move-object v2, v0

    move v0, v1

    .line 766
    goto/16 :goto_0

    .line 723
    :catch_2
    move-exception v6

    .line 730
    if-eqz v3, :cond_a

    .line 731
    :try_start_9
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_a
    move-object v6, v3

    .line 736
    goto :goto_5

    .line 733
    :catch_3
    move-exception v6

    move-object v6, v3

    .line 737
    goto :goto_5

    .line 728
    :catchall_0
    move-exception v1

    .line 730
    if-eqz v3, :cond_b

    .line 731
    :try_start_a
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 736
    :cond_b
    :goto_7
    :try_start_b
    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 752
    :catchall_1
    move-exception v1

    move-object v5, v4

    move-object v4, v2

    move-object v2, v0

    move-object v0, v1

    .line 754
    :goto_8
    if-eqz v4, :cond_c

    .line 755
    :try_start_c
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 757
    :cond_c
    if-eqz v2, :cond_d

    .line 758
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 760
    :cond_d
    if-eqz v5, :cond_e

    .line 761
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    .line 766
    :cond_e
    :goto_9
    throw v0

    .line 763
    :catch_4
    move-exception v5

    .line 765
    sget-object v6, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v5}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6

    .line 763
    :catch_5
    move-exception v1

    .line 765
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_9

    .line 770
    :cond_f
    const-string v0, "ERROR"

    goto/16 :goto_2

    .line 733
    :catch_6
    move-exception v7

    goto/16 :goto_5

    :catch_7
    move-exception v3

    goto :goto_7

    .line 752
    :catchall_2
    move-exception v1

    move-object v2, v5

    move-object v5, v0

    move-object v0, v1

    goto :goto_8

    :catchall_3
    move-exception v0

    goto :goto_8

    :catchall_4
    move-exception v1

    move-object v5, v0

    move-object v0, v1

    goto :goto_8

    .line 707
    :catch_8
    move-exception v0

    move-object v10, v0

    move-object v0, v2

    move-object v2, v4

    move-object v4, v5

    move-object v5, v10

    goto/16 :goto_3

    :catch_9
    move-exception v5

    move-object v10, v2

    move-object v2, v4

    move-object v4, v0

    move-object v0, v10

    goto/16 :goto_3

    :cond_10
    move-object v6, v3

    goto/16 :goto_4

    :cond_11
    move-object v6, v3

    goto/16 :goto_5
.end method

.method protected a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 776
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$67;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    .line 777
    if-nez v2, :cond_0

    .line 803
    :goto_0
    return-void

    .line 782
    :cond_0
    :try_start_0
    const-string v1, "ERROR"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 784
    iget-object v1, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v1, p1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 802
    :goto_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$67;->c:Lsoftware/simplicial/nebulous/f/al$z;

    invoke-interface {v1, v0}, Lsoftware/simplicial/nebulous/f/al$z;->a(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 788
    :cond_1
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 789
    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 791
    const-string v3, "Error"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 792
    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    .line 796
    :catch_0
    move-exception v1

    .line 798
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 663
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$67;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 663
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$67;->a(Ljava/lang/String;)V

    return-void
.end method
