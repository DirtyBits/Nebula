.class Lsoftware/simplicial/nebulous/f/al$47;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$t;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$t;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$t;)V
    .locals 0

    .prologue
    .line 3152
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$47;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$47;->a:Lsoftware/simplicial/nebulous/f/al$t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 8

    .prologue
    .line 3158
    if-eqz p1, :cond_1

    .line 3161
    :try_start_0
    const-string v0, "Mods"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 3163
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3165
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 3167
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 3168
    new-instance v4, Lsoftware/simplicial/a/bg;

    invoke-direct {v4}, Lsoftware/simplicial/a/bg;-><init>()V

    .line 3169
    const-string v5, "Id"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bg;->b:I

    .line 3170
    const-string v5, "Name"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    .line 3171
    const-string v5, "ClanName"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 3172
    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bg;->e:[B

    .line 3173
    const-string v5, "XP"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v4, Lsoftware/simplicial/a/bg;->g:J

    .line 3174
    const-string v5, "LastPlayedUtc"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3175
    const-string v5, "LastPlayedUtc"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    iput-object v3, v4, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    .line 3178
    :goto_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3177
    :cond_0
    const/4 v3, 0x0

    iput-object v3, v4, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3184
    :catch_0
    move-exception v0

    .line 3186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$47;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Failed to get mods."

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 3188
    :cond_1
    :goto_2
    return-void

    .line 3181
    :cond_2
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$47;->a:Lsoftware/simplicial/nebulous/f/al$t;

    invoke-interface {v0, v2}, Lsoftware/simplicial/nebulous/f/al$t;->a(Ljava/util/ArrayList;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
