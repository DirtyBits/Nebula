.class public Lsoftware/simplicial/nebulous/f/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field private b:Lcom/facebook/a/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    .line 32
    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserId(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "log_out"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v1, "log_out"

    invoke-virtual {v0, v1}, Lcom/facebook/a/g;->a(Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->setUserId(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "login"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v1, "login"

    invoke-virtual {v0, v1}, Lcom/facebook/a/g;->a(Ljava/lang/String;)V

    .line 53
    :cond_0
    return-void
.end method

.method public a(ILsoftware/simplicial/a/bj;)V
    .locals 4

    .prologue
    .line 252
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 253
    const-string v1, "level"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 254
    const-string v1, "character"

    invoke-virtual {p2}, Lsoftware/simplicial/a/bj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "level_up"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 257
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v0, :cond_0

    .line 259
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 260
    const-string v1, "fb_level"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 261
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "fb_mobile_level_achieved"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 263
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    .line 38
    invoke-static {}, Lcom/facebook/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    sget-object v0, Lcom/facebook/y;->e:Lcom/facebook/y;

    invoke-static {v0}, Lcom/facebook/p;->a(Lcom/facebook/y;)V

    .line 41
    invoke-static {p1}, Lcom/facebook/a/g;->a(Landroid/content/Context;)Lcom/facebook/a/g;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    .line 43
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "join_group"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 81
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "join_group"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 83
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;DLjava/lang/String;)V
    .locals 4

    .prologue
    .line 112
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 113
    const-string v1, "transaction_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    invoke-static {p2, p3}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-static {p4}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/facebook/a/g;->a(Ljava/math/BigDecimal;Ljava/util/Currency;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 122
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string v1, "item_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v1, "virtual_currency_name"

    const-string v2, "plasma"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v1, "value"

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 92
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "spend_virtual_currency"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 93
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "spend_virtual_currency"

    int-to-double v4, p2

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;DLandroid/os/Bundle;)V

    .line 95
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 231
    const-string v1, "option"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "set_options"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 235
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "set_options"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 237
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/aa;)V
    .locals 3

    .prologue
    .line 281
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 282
    const-string v1, "dq"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "dq_complete"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 285
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "dq_complete"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 287
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/af;)V
    .locals 3

    .prologue
    .line 175
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 176
    const-string v1, "content_type"

    const-string v2, "eject_skin"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v1, "item_id"

    invoke-virtual {p1}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 180
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 182
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/as;)V
    .locals 3

    .prologue
    .line 186
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 187
    const-string v1, "content_type"

    const-string v2, "hat"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v1, "item_id"

    invoke-virtual {p1}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 191
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 193
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bd;)V
    .locals 3

    .prologue
    .line 197
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    const-string v1, "content_type"

    const-string v2, "pet"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v1, "item_id"

    invoke-virtual {p1}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 202
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 204
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/d;Lsoftware/simplicial/a/bj;)V
    .locals 4

    .prologue
    .line 267
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 268
    const-string v1, "achievement_id"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-short v3, p1, Lsoftware/simplicial/a/d;->bw:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "unlock_achievement"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v0, :cond_0

    .line 273
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 274
    const-string v1, "fb_description"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-short v3, p1, Lsoftware/simplicial/a/d;->bw:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "fb_mobile_achievement_unlocked"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 277
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/a/e;)V
    .locals 3

    .prologue
    .line 164
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165
    const-string v1, "content_type"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v1, "item_id"

    invoke-virtual {p1}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 169
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 171
    :cond_0
    return-void
.end method

.method public a(Lsoftware/simplicial/nebulous/f/c;)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    const-string v1, "content_type"

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "item_id"

    const-string v2, "nebulous"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "share"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 71
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "share"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 73
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 209
    const-string v1, "content_type"

    const-string v2, "custom_skin"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v1, "item_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 213
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 215
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 99
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string v1, "item_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v1, "virtual_currency_name"

    const-string v2, "plasma"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v1, "value"

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 104
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "earn_virtual_currency"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 105
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "earn_virtual_currency"

    int-to-double v4, p2

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;DLandroid/os/Bundle;)V

    .line 107
    :cond_0
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 219
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 220
    const-string v1, "content_type"

    const-string v2, "player_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v1, "item_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 224
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "select_content"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 226
    :cond_0
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 301
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v1, "HAX_DETECTED"

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 302
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v1, "HAX_DETECTED"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 304
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 241
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 242
    const-string v1, "game"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->a:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const-string v2, "launch_game"

    invoke-virtual {v1, v2, v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 245
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/e;->b:Lcom/facebook/a/g;

    const-string v2, "launch_game"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/a/g;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 247
    :cond_0
    return-void
.end method
