.class public Lsoftware/simplicial/nebulous/f/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Landroid/graphics/Bitmap;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:Lsoftware/simplicial/nebulous/f/al;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/f/p;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lsoftware/simplicial/nebulous/f/al;Landroid/content/res/Resources;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/nebulous/f/al;",
            "Landroid/content/res/Resources;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    .line 27
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/p;->c:Lsoftware/simplicial/nebulous/f/al;

    .line 28
    sget-object v1, Lsoftware/simplicial/nebulous/f/p;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    :try_start_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 31
    const v0, 0x7f020432

    invoke-static {p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    .line 32
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 34
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v1

    .line 36
    :try_start_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o;

    .line 37
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    new-instance v4, Lsoftware/simplicial/nebulous/f/o;

    iget v5, v0, Lsoftware/simplicial/nebulous/f/o;->b:I

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v0}, Lsoftware/simplicial/nebulous/f/o;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 32
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 38
    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 39
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/f/p;)Ljava/util/List;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    return-object v0
.end method

.method private a(I)Lsoftware/simplicial/nebulous/f/o;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 43
    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v4

    .line 46
    const/4 v3, -0x1

    .line 47
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 48
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 50
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o;

    .line 51
    add-int/lit8 v3, v3, 0x1

    .line 53
    iget v5, v0, Lsoftware/simplicial/nebulous/f/o;->b:I

    if-ne v5, p1, :cond_0

    .line 59
    :goto_0
    if-nez v0, :cond_3

    .line 62
    new-instance v0, Lsoftware/simplicial/nebulous/f/o;

    invoke-direct {v0, p1}, Lsoftware/simplicial/nebulous/f/o;-><init>(I)V

    .line 63
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v2, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 64
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/16 v5, 0x80

    if-le v2, v5, :cond_1

    .line 65
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 70
    :goto_2
    if-eqz v0, :cond_2

    :try_start_1
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v5, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v2, v5, :cond_2

    const/16 v2, 0x20

    if-lt v3, v2, :cond_2

    .line 72
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 76
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :cond_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(ILsoftware/simplicial/nebulous/f/al$ac;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 83
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v1

    .line 85
    :try_start_0
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/f/p;->a(I)Lsoftware/simplicial/nebulous/f/o;

    move-result-object v0

    .line 86
    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v2, v3, :cond_1

    .line 87
    :cond_0
    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    monitor-exit v1

    .line 119
    :goto_0
    return-object v0

    .line 88
    :cond_1
    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->d:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v2, v3, :cond_2

    iget v2, v0, Lsoftware/simplicial/nebulous/f/o;->e:I

    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    .line 89
    sget-object v0, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    monitor-exit v1

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 90
    :cond_2
    :try_start_1
    sget-object v2, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    iput-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 91
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->c:Lsoftware/simplicial/nebulous/f/al;

    new-instance v2, Lsoftware/simplicial/nebulous/f/p$1;

    invoke-direct {v2, p0, v0, p2}, Lsoftware/simplicial/nebulous/f/p$1;-><init>(Lsoftware/simplicial/nebulous/f/p;Lsoftware/simplicial/nebulous/f/o;Lsoftware/simplicial/nebulous/f/al$ac;)V

    invoke-virtual {v1, p1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$ac;)V

    .line 119
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILsoftware/simplicial/nebulous/f/al$ag;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 125
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v1

    .line 127
    :try_start_0
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/f/p;->a(I)Lsoftware/simplicial/nebulous/f/o;

    move-result-object v0

    .line 128
    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    if-eq v2, v3, :cond_0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v2, v3, :cond_1

    .line 129
    :cond_0
    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    monitor-exit v1

    .line 161
    :goto_0
    return-object v0

    .line 130
    :cond_1
    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$a;->d:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v2, v3, :cond_2

    iget v2, v0, Lsoftware/simplicial/nebulous/f/o;->e:I

    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    .line 131
    sget-object v0, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    monitor-exit v1

    goto :goto_0

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 132
    :cond_2
    :try_start_1
    sget-object v2, Lsoftware/simplicial/nebulous/f/o$a;->b:Lsoftware/simplicial/nebulous/f/o$a;

    iput-object v2, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    .line 133
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->c:Lsoftware/simplicial/nebulous/f/al;

    new-instance v2, Lsoftware/simplicial/nebulous/f/p$2;

    invoke-direct {v2, p0, v0, p2}, Lsoftware/simplicial/nebulous/f/p$2;-><init>(Lsoftware/simplicial/nebulous/f/p;Lsoftware/simplicial/nebulous/f/o;Lsoftware/simplicial/nebulous/f/al$ag;)V

    invoke-virtual {v1, p1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$ag;)V

    .line 161
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 166
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v1

    .line 168
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 169
    monitor-exit v1

    .line 170
    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 175
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    monitor-enter v2

    .line 177
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/p;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/o;

    .line 178
    iget-object v4, v0, Lsoftware/simplicial/nebulous/f/o;->d:Lsoftware/simplicial/nebulous/f/o$a;

    sget-object v5, Lsoftware/simplicial/nebulous/f/o$a;->c:Lsoftware/simplicial/nebulous/f/o$a;

    if-ne v4, v5, :cond_0

    .line 179
    new-instance v4, Lsoftware/simplicial/nebulous/f/o;

    iget v5, v0, Lsoftware/simplicial/nebulous/f/o;->b:I

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/o;->c:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5, v0}, Lsoftware/simplicial/nebulous/f/o;-><init>(ILandroid/graphics/Bitmap;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    return-object v1
.end method
