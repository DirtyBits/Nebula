.class Lsoftware/simplicial/nebulous/f/al$21;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/o$b;ILjava/lang/String;Lsoftware/simplicial/nebulous/f/al$af;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/o$b;

.field final synthetic b:I

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al$af;

.field final synthetic d:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/o$b;ILsoftware/simplicial/nebulous/f/al$af;)V
    .locals 0

    .prologue
    .line 1895
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$21;->a:Lsoftware/simplicial/nebulous/f/o$b;

    iput p3, p0, Lsoftware/simplicial/nebulous/f/al$21;->b:I

    iput-object p4, p0, Lsoftware/simplicial/nebulous/f/al$21;->c:Lsoftware/simplicial/nebulous/f/al$af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    .line 1901
    if-eqz p1, :cond_0

    .line 1903
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1906
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Error"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 1930
    :cond_0
    :goto_0
    return-void

    .line 1910
    :cond_1
    const-string v0, "Status"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/q;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/q;

    move-result-object v1

    .line 1911
    const-string v0, "Response"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1912
    const-string v0, "NewSkinID"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1914
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v4, "Coins"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1915
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v4, "ClanCoins"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 1916
    const-string v0, "CoinsSpent"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1918
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->c(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/f/e;

    move-result-object v5

    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->a:Lsoftware/simplicial/nebulous/f/o$b;

    sget-object v6, Lsoftware/simplicial/nebulous/f/o$b;->a:Lsoftware/simplicial/nebulous/f/o$b;

    if-ne v0, v6, :cond_2

    const-string v0, "UPLOADED_ACCOUNT_SKIN"

    :goto_1
    invoke-virtual {v5, v0, v4}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;I)V

    .line 1922
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->c:Lsoftware/simplicial/nebulous/f/al$af;

    invoke-interface {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al$af;->a(Lsoftware/simplicial/nebulous/f/q;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1926
    :catch_0
    move-exception v0

    .line 1928
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$21;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Unknown Error UploadSkin."

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1918
    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UPLOADED_CLAN_SKIN_"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, p0, Lsoftware/simplicial/nebulous/f/al$21;->b:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method
