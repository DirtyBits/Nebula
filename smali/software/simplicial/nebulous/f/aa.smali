.class public final Lsoftware/simplicial/nebulous/f/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/UUID;

.field public static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v0, "7167f922-5730-11e6-8b77-86f30ca893d3"

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/f/aa;->a:Ljava/util/UUID;

    .line 51
    const/16 v0, 0xff

    invoke-static {v0, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lsoftware/simplicial/nebulous/f/aa;->b:I

    return-void
.end method

.method public static a(I)B
    .locals 4

    .prologue
    const/high16 v2, 0x40e00000    # 7.0f

    const/high16 v3, 0x437f0000    # 255.0f

    .line 609
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    shl-int/lit8 v0, v0, 0x5

    .line 610
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    shl-int/lit8 v1, v1, 0x2

    .line 611
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    const/high16 v3, 0x40400000    # 3.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    shl-int/lit8 v2, v2, 0x0

    .line 612
    or-int/2addr v0, v1

    or-int/2addr v0, v2

    int-to-byte v0, v0

    return v0
.end method

.method public static a(B)I
    .locals 4

    .prologue
    const/high16 v2, 0x40e00000    # 7.0f

    const/high16 v3, 0x437f0000    # 255.0f

    .line 601
    and-int/lit16 v0, p0, 0xe0

    shr-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    mul-float/2addr v0, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 602
    and-int/lit8 v1, p0, 0x1c

    shr-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 603
    and-int/lit8 v2, p0, 0x3

    shr-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    mul-float/2addr v2, v3

    const/high16 v3, 0x40400000    # 3.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    .line 604
    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public static a(IZ)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 696
    if-eqz p1, :cond_0

    .line 698
    sget v0, Lsoftware/simplicial/nebulous/f/aa;->b:I

    .line 715
    :goto_0
    return v0

    .line 702
    :cond_0
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    .line 703
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 704
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 705
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    .line 706
    add-int/lit8 v3, v1, -0x32

    .line 707
    add-int/lit8 v2, v2, -0x46

    .line 708
    add-int/lit8 v1, v5, -0x28

    .line 709
    if-gez v3, :cond_1

    move v3, v0

    .line 711
    :cond_1
    if-gez v2, :cond_2

    move v2, v0

    .line 713
    :cond_2
    if-gez v1, :cond_3

    .line 715
    :goto_1
    invoke-static {v4, v3, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lsoftware/simplicial/a/am;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 392
    .line 393
    sget-object v1, Lsoftware/simplicial/nebulous/f/aa$1;->d:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 442
    :goto_0
    :pswitch_0
    return v0

    .line 403
    :pswitch_1
    const/4 v0, 0x1

    .line 404
    goto :goto_0

    .line 406
    :pswitch_2
    const/4 v0, 0x2

    .line 407
    goto :goto_0

    .line 409
    :pswitch_3
    const/4 v0, 0x3

    .line 410
    goto :goto_0

    .line 412
    :pswitch_4
    const/4 v0, 0x4

    .line 413
    goto :goto_0

    .line 415
    :pswitch_5
    const/4 v0, 0x5

    .line 416
    goto :goto_0

    .line 418
    :pswitch_6
    const/4 v0, 0x6

    .line 419
    goto :goto_0

    .line 421
    :pswitch_7
    const/4 v0, 0x7

    .line 422
    goto :goto_0

    .line 424
    :pswitch_8
    const/16 v0, 0x8

    .line 425
    goto :goto_0

    .line 427
    :pswitch_9
    const/16 v0, 0x9

    .line 428
    goto :goto_0

    .line 430
    :pswitch_a
    const/16 v0, 0xa

    .line 431
    goto :goto_0

    .line 433
    :pswitch_b
    const/16 v0, 0xb

    .line 434
    goto :goto_0

    .line 436
    :pswitch_c
    const/16 v0, 0xc

    .line 437
    goto :goto_0

    .line 439
    :pswitch_d
    const/16 v0, 0xd

    goto :goto_0

    .line 393
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_8
        :pswitch_d
        :pswitch_2
        :pswitch_c
        :pswitch_b
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/q;)I
    .locals 2

    .prologue
    .line 730
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 743
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 733
    :pswitch_0
    const v0, 0x7f02019b

    goto :goto_0

    .line 735
    :pswitch_1
    const v0, 0x7f0202ff

    goto :goto_0

    .line 737
    :pswitch_2
    const v0, 0x7f020075

    goto :goto_0

    .line 739
    :pswitch_3
    const v0, 0x7f020407

    goto :goto_0

    .line 741
    :pswitch_4
    const v0, 0x7f02019e

    goto :goto_0

    .line 730
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;
    .locals 10

    .prologue
    const/16 v9, 0x12

    const/4 v6, 0x1

    const/16 v1, 0xff

    const/4 v2, 0x0

    .line 565
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 569
    if-eqz p2, :cond_1

    .line 570
    invoke-static {v2, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    :goto_0
    move v1, v2

    move v3, v2

    move v4, v2

    .line 575
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 578
    add-int/lit8 v3, v3, -0x1

    .line 579
    if-gtz v3, :cond_5

    .line 581
    invoke-virtual {p0, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    .line 582
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    .line 583
    add-int/2addr v4, v3

    .line 584
    if-ne v3, v6, :cond_5

    move v5, v6

    .line 588
    :goto_2
    if-eqz v5, :cond_0

    .line 590
    if-ltz v1, :cond_3

    array-length v5, p1

    if-ge v1, v5, :cond_3

    .line 591
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    aget-byte v8, p1, v1

    invoke-static {v8}, Lsoftware/simplicial/nebulous/f/aa;->a(B)I

    move-result v8

    invoke-direct {v5, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {v7, v5, v1, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 575
    :cond_0
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 571
    :cond_1
    if-eqz p3, :cond_2

    .line 572
    invoke-static {v1, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 574
    :cond_2
    const/16 v0, 0xd7

    invoke-static {v1, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 593
    :cond_3
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {v7, v5, v1, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3

    .line 596
    :cond_4
    return-object v7

    :cond_5
    move v5, v2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/CharSequence;Lsoftware/simplicial/a/p;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 769
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "sizes"

    const-string v3, "array"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 771
    const-string v0, ""

    .line 772
    if-eqz p1, :cond_0

    .line 773
    new-array v0, v7, [Ljava/lang/CharSequence;

    aput-object p1, v0, v5

    const-string v2, "\n"

    aput-object v2, v0, v6

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 775
    :cond_0
    sget-object v2, Lsoftware/simplicial/nebulous/f/aa$1;->p:[I

    invoke-virtual {p2}, Lsoftware/simplicial/a/p;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 794
    :goto_0
    return-object p1

    .line 778
    :pswitch_0
    new-array v2, v9, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v8

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 780
    :pswitch_1
    new-array v2, v9, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v7

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 782
    :pswitch_2
    new-array v2, v9, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v6

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 784
    :pswitch_3
    new-array v2, v9, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v5

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080219

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 786
    :pswitch_4
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v8

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    const-string v0, " "

    aput-object v0, v2, v9

    const/4 v0, 0x5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080219

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_0

    .line 788
    :pswitch_5
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v7

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    const-string v0, " "

    aput-object v0, v2, v9

    const/4 v0, 0x5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080219

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_0

    .line 790
    :pswitch_6
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v6

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    const-string v0, " "

    aput-object v0, v2, v9

    const/4 v0, 0x5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080219

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_0

    .line 792
    :pswitch_7
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v5

    aget-object v0, v1, v5

    aput-object v0, v2, v6

    const-string v0, " "

    aput-object v0, v2, v7

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080186

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    const-string v0, " "

    aput-object v0, v2, v9

    const/4 v0, 0x5

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080219

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_0

    .line 775
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;[B)Ljava/lang/CharSequence;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 535
    const/4 v0, 0x0

    move v1, v2

    move v3, v2

    move v4, v2

    .line 538
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v1, v5, :cond_2

    array-length v5, p1

    if-ge v1, v5, :cond_2

    .line 541
    add-int/lit8 v3, v3, -0x1

    .line 542
    if-gtz v3, :cond_4

    .line 544
    invoke-virtual {p0, v4}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    .line 545
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    .line 546
    add-int/2addr v4, v3

    .line 547
    if-ne v3, v6, :cond_4

    move v5, v6

    .line 551
    :goto_1
    aget-byte v7, p1, v1

    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    if-eqz v5, :cond_1

    .line 553
    if-nez v0, :cond_0

    .line 554
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 555
    :cond_0
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    aget-byte v7, p1, v1

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/aa;->a(B)I

    move-result v7

    invoke-direct {v5, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v7, v1, 0x1

    const/16 v8, 0x12

    invoke-virtual {v0, v5, v1, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 538
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 558
    :cond_2
    if-eqz v0, :cond_3

    .line 560
    :goto_2
    return-object v0

    :cond_3
    move-object v0, p0

    goto :goto_2

    :cond_4
    move v5, v2

    goto :goto_1
.end method

.method public static a(ILandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    packed-switch p0, :pswitch_data_0

    .line 236
    :pswitch_0
    const-string v0, "---"

    :goto_0
    return-object v0

    .line 228
    :pswitch_1
    const v0, 0x7f080331

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 230
    :pswitch_2
    const v0, 0x7f080332

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 232
    :pswitch_3
    const v0, 0x7f080333

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 234
    :pswitch_4
    const v0, 0x7f080334

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 225
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static a(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 207
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    long-to-int v0, v0

    .line 208
    const-string v1, "%d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    rem-int/lit16 v4, v0, 0xe10

    div-int/lit8 v4, v4, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Lsoftware/simplicial/a/q;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 97
    const v0, 0x7f0801c8

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 87
    :pswitch_0
    const v0, 0x7f08016b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 89
    :pswitch_1
    const v0, 0x7f080072

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91
    :pswitch_2
    const v0, 0x7f080002

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :pswitch_3
    const v0, 0x7f0800fe

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :pswitch_4
    const v0, 0x7f080188

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 721
    const-string v0, ""

    .line 722
    if-eqz p1, :cond_0

    .line 723
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08008d

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 724
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 725
    return-object v0
.end method

.method public static a(Lsoftware/simplicial/a/ak;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 473
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->l:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/ak;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 482
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 476
    :pswitch_0
    const v0, 0x7f0803cd

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 478
    :pswitch_1
    const v0, 0x7f0803ce

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 480
    :pswitch_2
    const v0, 0x7f0803cf

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 473
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->d:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 179
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 147
    :pswitch_0
    const v0, 0x7f08011d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 149
    :pswitch_1
    const v0, 0x7f080124

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 151
    :pswitch_2
    const v0, 0x7f08032d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 153
    :pswitch_3
    const v0, 0x7f080120

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_4
    const v0, 0x7f080122

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    :pswitch_5
    const v0, 0x7f080076

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 159
    :pswitch_6
    const v0, 0x7f0800dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 161
    :pswitch_7
    const v0, 0x7f0801db

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :pswitch_8
    const v0, 0x7f0802a0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 165
    :pswitch_9
    const v0, 0x7f0802a1

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 167
    :pswitch_a
    const v0, 0x7f0802a2

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 169
    :pswitch_b
    const v0, 0x7f080252

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 171
    :pswitch_c
    const v0, 0x7f08024e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 177
    :pswitch_d
    const v0, 0x7f080103

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    if-eqz p1, :cond_0

    .line 57
    const-string v0, ""

    .line 58
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/ap;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 102
    const v0, 0x7f070004

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 105
    invoke-virtual {p0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    .line 106
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/b/b;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 305
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->i:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 318
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 308
    :pswitch_0
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 310
    :pswitch_1
    const v0, 0x7f080259

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 312
    :pswitch_2
    const v0, 0x7f0802e4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 314
    :pswitch_3
    const v0, 0x7f080159

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 316
    :pswitch_4
    const v0, 0x7f0800df

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 305
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/b/d;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 260
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->f:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 267
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 263
    :pswitch_0
    const v0, 0x7f08032f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 265
    :pswitch_1
    const v0, 0x7f08011d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/b/e;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 284
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->h:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 300
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 287
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 289
    :pswitch_1
    const v0, 0x7f08002b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 291
    :pswitch_2
    const v0, 0x7f08002c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 293
    :pswitch_3
    const v0, 0x7f08015f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 295
    :pswitch_4
    const v0, 0x7f0802e4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 297
    :pswitch_5
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/b/f;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 488
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->m:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/b/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 497
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 491
    :pswitch_0
    const v0, 0x7f0800e1

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 493
    :pswitch_1
    const v0, 0x7f0802e8

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 495
    :pswitch_2
    const v0, 0x7f080171

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/bj;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->b:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/bj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 118
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 114
    :pswitch_0
    const v0, 0x7f08024b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_1
    const v0, 0x7f08018b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/bn;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 447
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->k:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/bn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 468
    const-string v0, ""

    :goto_0
    return-object v0

    .line 450
    :pswitch_0
    const-string v0, "DEBUG"

    goto :goto_0

    .line 452
    :pswitch_1
    const-string v0, "DEBUG_GLOBAL"

    goto :goto_0

    .line 454
    :pswitch_2
    const v0, 0x7f0802d4

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 456
    :pswitch_3
    const v0, 0x7f0802d5

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 458
    :pswitch_4
    const v0, 0x7f080119

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 460
    :pswitch_5
    const v0, 0x7f08027d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 462
    :pswitch_6
    const v0, 0x7f080105

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 464
    :pswitch_7
    const v0, 0x7f08027e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 466
    :pswitch_8
    const v0, 0x7f080035

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 447
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/c/e;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->c:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/c/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 138
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 126
    :pswitch_0
    const v0, 0x7f080003

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_1
    const v0, 0x7f080122

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 130
    :pswitch_2
    const v0, 0x7f080076

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 132
    :pswitch_3
    const v0, 0x7f0800dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 134
    :pswitch_4
    const v0, 0x7f080252

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 136
    :pswitch_5
    const v0, 0x7f08024e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/h/a$a;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 323
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->j:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/h/a$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 336
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 326
    :pswitch_0
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 328
    :pswitch_1
    const v0, 0x7f080259

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 330
    :pswitch_2
    const v0, 0x7f08012d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 332
    :pswitch_3
    const v0, 0x7f080159

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 334
    :pswitch_4
    const v0, 0x7f0800df

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 323
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/h/f;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->g:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/h/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 279
    const v0, 0x7f0801ab

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 275
    :pswitch_0
    const v0, 0x7f080331

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 277
    :pswitch_1
    const v0, 0x7f080332

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 272
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/i/e;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 617
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->n:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/i/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 630
    const-string v0, "NULL"

    :goto_0
    return-object v0

    .line 620
    :pswitch_0
    const v0, 0x7f08017d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 622
    :pswitch_1
    const v0, 0x7f0800c3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 624
    :pswitch_2
    const v0, 0x7f08022e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 626
    :pswitch_3
    const v0, 0x7f08012d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 628
    :pswitch_4
    const v0, 0x7f0801c6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 617
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/a/s;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 636
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->o:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 645
    const-string v0, "NULL"

    :goto_0
    return-object v0

    .line 639
    :pswitch_0
    const-string v0, ""

    goto :goto_0

    .line 641
    :pswitch_1
    const v0, 0x7f080036

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 643
    :pswitch_2
    const v0, 0x7f0802d7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 636
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lsoftware/simplicial/nebulous/f/ab;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 3

    .prologue
    const v2, 0x7f08023c

    .line 241
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->e:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ab;->a:Lsoftware/simplicial/nebulous/f/ac;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/ac;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 254
    const v0, 0x7f0802dc

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 244
    :pswitch_0
    const v0, 0x7f0800e3

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 246
    :pswitch_1
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 248
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f08008d

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 250
    :pswitch_3
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 252
    :pswitch_4
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 241
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a([F)Ljava/nio/FloatBuffer;
    .locals 2

    .prologue
    .line 685
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 686
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 687
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 688
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 689
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 691
    return-object v0
.end method

.method public static a()Lsoftware/simplicial/a/bn;
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    .line 185
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v0

    .line 186
    const/4 v1, -0x4

    if-lt v0, v1, :cond_0

    const/4 v1, -0x3

    if-gt v0, v1, :cond_0

    .line 187
    sget-object v0, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    .line 202
    :goto_0
    return-object v0

    .line 188
    :cond_0
    const/4 v1, -0x6

    if-lt v0, v1, :cond_1

    const/4 v1, -0x5

    if-gt v0, v1, :cond_1

    .line 189
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 190
    :cond_1
    const/16 v1, -0xc

    if-lt v0, v1, :cond_2

    const/4 v1, -0x7

    if-gt v0, v1, :cond_2

    .line 191
    sget-object v0, Lsoftware/simplicial/a/bn;->b:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 192
    :cond_2
    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 193
    sget-object v0, Lsoftware/simplicial/a/bn;->g:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 194
    :cond_3
    const/16 v1, 0x9

    if-lt v0, v1, :cond_4

    const/16 v1, 0xc

    if-gt v0, v1, :cond_4

    .line 195
    sget-object v0, Lsoftware/simplicial/a/bn;->d:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 196
    :cond_4
    const/4 v1, 0x5

    if-lt v0, v1, :cond_5

    const/16 v1, 0x8

    if-gt v0, v1, :cond_5

    .line 197
    sget-object v0, Lsoftware/simplicial/a/bn;->e:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 198
    :cond_5
    const/4 v1, -0x2

    if-lt v0, v1, :cond_6

    const/4 v1, 0x4

    if-gt v0, v1, :cond_6

    .line 199
    sget-object v0, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    goto :goto_0

    .line 201
    :cond_6
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    goto :goto_0
.end method

.method public static a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 651
    const/4 v0, 0x0

    .line 652
    sget-object v1, Lsoftware/simplicial/nebulous/f/aa$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 671
    :goto_0
    if-nez v0, :cond_0

    .line 673
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 674
    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 681
    :goto_1
    return-void

    .line 655
    :pswitch_0
    const v0, 0x7f02019b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 658
    :pswitch_1
    const v0, 0x7f0202ff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 661
    :pswitch_2
    const v0, 0x7f020075

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 664
    :pswitch_3
    const v0, 0x7f020407

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 667
    :pswitch_4
    const v0, 0x7f02019e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 677
    :cond_0
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 678
    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 679
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 680
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b()I
    .locals 8

    .prologue
    const/16 v1, 0x78

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 503
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 504
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd\'T\'HH:mm:ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 505
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 506
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 509
    :try_start_0
    const-string v4, "2015-10-01T00:00:00"

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 516
    :goto_0
    new-instance v2, Ljava/util/GregorianCalendar;

    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 517
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 518
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 519
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 521
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 522
    mul-int/lit8 v3, v3, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    .line 524
    if-gez v0, :cond_0

    .line 525
    const/4 v0, 0x0

    .line 527
    :cond_0
    if-le v0, v1, :cond_1

    move v0, v1

    .line 530
    :cond_1
    return v0

    .line 511
    :catch_0
    move-exception v2

    .line 513
    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v2}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static b(Lsoftware/simplicial/a/q;)I
    .locals 4

    .prologue
    const/16 v3, 0xff

    const/4 v2, 0x0

    .line 749
    sget-object v0, Lsoftware/simplicial/nebulous/f/aa$1;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 763
    const/16 v0, 0xdf

    invoke-static {v0, v3, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    :goto_0
    return v0

    .line 752
    :pswitch_0
    const/16 v0, 0xdc

    invoke-static {v3, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 754
    :pswitch_1
    const/16 v0, 0xa5

    invoke-static {v3, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 756
    :pswitch_2
    const/16 v0, 0x4b

    const/16 v1, 0x96

    invoke-static {v0, v1, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 758
    :pswitch_3
    invoke-static {v2, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 760
    :pswitch_4
    const/16 v0, 0xe1

    invoke-static {v2, v0, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_0

    .line 749
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static b(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 213
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    long-to-int v0, v0

    .line 214
    const-string v1, "%d:%02d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    div-int/lit16 v4, v0, 0xe10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    rem-int/lit16 v0, v0, 0xe10

    div-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(J)Ljava/lang/String;
    .locals 6

    .prologue
    .line 219
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    long-to-int v0, v0

    .line 220
    const-string v1, "%d:%02d:%02d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    div-int/lit16 v4, v0, 0xe10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    rem-int/lit16 v4, v0, 0xe10

    div-int/lit8 v4, v4, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(J)Lsoftware/simplicial/a/am;
    .locals 2

    .prologue
    .line 341
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    .line 342
    long-to-int v1, p0

    packed-switch v1, :pswitch_data_0

    .line 387
    :goto_0
    return-object v0

    .line 345
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 348
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 351
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/a/am;->b:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 354
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/a/am;->j:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 357
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/a/am;->c:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 360
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/a/am;->d:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 363
    :pswitch_6
    sget-object v0, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 366
    :pswitch_7
    sget-object v0, Lsoftware/simplicial/a/am;->l:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 369
    :pswitch_8
    sget-object v0, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 372
    :pswitch_9
    sget-object v0, Lsoftware/simplicial/a/am;->f:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 375
    :pswitch_a
    sget-object v0, Lsoftware/simplicial/a/am;->g:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 378
    :pswitch_b
    sget-object v0, Lsoftware/simplicial/a/am;->i:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 381
    :pswitch_c
    sget-object v0, Lsoftware/simplicial/a/am;->e:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 384
    :pswitch_d
    sget-object v0, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    goto :goto_0

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
