.class public Lsoftware/simplicial/nebulous/f/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/a/f/bf;
.implements Lsoftware/simplicial/a/f/bg;


# instance fields
.field public a:Lsoftware/simplicial/a/bs;

.field public b:Lsoftware/simplicial/a/t;

.field public c:Lsoftware/simplicial/nebulous/b/d;

.field public d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    .line 33
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    .line 34
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    .line 36
    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->e:I

    .line 37
    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->f:I

    .line 38
    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->g:I

    .line 42
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/ah;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 43
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/w;)V
    .locals 5

    .prologue
    .line 151
    iget-object v0, p1, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    check-cast v0, Lsoftware/simplicial/a/v;

    .line 152
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ah;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->a:I

    .line 153
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ah;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->b:I

    .line 154
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, p1, Lsoftware/simplicial/a/f/w;->a:I

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->c:I

    .line 155
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-short v2, p1, Lsoftware/simplicial/a/f/w;->g:S

    iput-short v2, v1, Lsoftware/simplicial/a/f/bl;->d:S

    .line 157
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    invoke-virtual {v1, p1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V

    .line 159
    new-instance v1, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v1}, Lsoftware/simplicial/a/f/bn;-><init>()V

    iget-object v2, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->c:I

    iget-object v3, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->b:I

    iget-object v0, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v0, v0, Lsoftware/simplicial/a/f/bl;->a:I

    const-string v4, ""

    invoke-static {v1, v2, v3, v0, v4}, Lsoftware/simplicial/a/f/y;->a(Lsoftware/simplicial/a/f/bn;IIILjava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/f/ah;->a([BZ)V

    .line 160
    return-void
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/x;)V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-ne v0, v1, :cond_0

    .line 181
    iget v0, p1, Lsoftware/simplicial/a/f/x;->c:I

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->e:I

    .line 182
    iget v0, p1, Lsoftware/simplicial/a/f/x;->b:I

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->f:I

    .line 185
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/f/x;->a:Lsoftware/simplicial/a/f/z;

    sget-object v1, Lsoftware/simplicial/a/f/z;->a:Lsoftware/simplicial/a/f/z;

    if-eq v0, v1, :cond_1

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->p()V

    .line 188
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lsoftware/simplicial/a/f/y;)V
    .locals 7

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    iget v0, p1, Lsoftware/simplicial/a/f/y;->ar:I

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->e:I

    .line 166
    iget v0, p1, Lsoftware/simplicial/a/f/y;->a:I

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->f:I

    .line 167
    iget v0, p1, Lsoftware/simplicial/a/f/y;->b:I

    iput v0, p0, Lsoftware/simplicial/nebulous/f/ah;->g:I

    .line 169
    new-instance v6, Lsoftware/simplicial/a/f/v;

    invoke-direct {v6}, Lsoftware/simplicial/a/f/v;-><init>()V

    .line 170
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/f/ah;->c()I

    move-result v0

    iput v0, v6, Lsoftware/simplicial/a/f/v;->ar:I

    .line 171
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/f/ah;->d()I

    move-result v0

    iput v0, v6, Lsoftware/simplicial/a/f/v;->a:I

    .line 172
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/f/ah;->b()I

    move-result v0

    iput v0, v6, Lsoftware/simplicial/a/f/v;->b:I

    .line 173
    new-instance v0, Lsoftware/simplicial/a/f/bj;

    iget-object v1, v6, Lsoftware/simplicial/a/f/v;->aq:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v1}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v1

    iget v2, v6, Lsoftware/simplicial/a/f/v;->ar:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/a/f/bj;-><init>(II[BLjava/net/InetSocketAddress;Ljava/net/DatagramSocket;)V

    iput-object v0, v6, Lsoftware/simplicial/a/f/v;->c:Lsoftware/simplicial/a/f/bj;

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    invoke-virtual {v0, v6}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    monitor-exit p0

    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a([BZ)V
    .locals 6

    .prologue
    const/high16 v5, -0x1000000

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 77
    :try_start_0
    array-length v4, p1

    .line 78
    const/4 v1, 0x0

    aget-byte v1, p1, v1

    .line 79
    if-eqz p2, :cond_6

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    invoke-virtual {v3, v1}, Lsoftware/simplicial/a/bs;->a(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 81
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x18

    and-int/2addr v0, v5

    const/4 v2, 0x2

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    add-int/2addr v0, v2

    const/4 v2, 0x3

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x8

    const v3, 0xff00

    and-int/2addr v2, v3

    add-int/2addr v0, v2

    const/4 v2, 0x4

    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x0

    and-int/lit16 v2, v2, 0xff

    add-int/2addr v2, v0

    .line 82
    add-int/lit8 v0, v4, -0x5

    new-array v3, v0, [B

    .line 83
    const/4 v0, 0x5

    const/4 v5, 0x0

    add-int/lit8 v4, v4, -0x5

    invoke-static {p1, v0, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    sget-object v0, Lsoftware/simplicial/a/f/bh;->d:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v1, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aC:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 86
    :cond_0
    new-instance v0, Lsoftware/simplicial/a/f/bj;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/a/f/bj;-><init>(II[BLjava/net/InetSocketAddress;Ljava/net/DatagramSocket;)V

    move-object v2, v0

    .line 90
    :goto_0
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    aget-object v0, v0, v1

    invoke-static {v0}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bh;)Lsoftware/simplicial/a/f/be;

    move-result-object v0

    .line 92
    iget-object v1, v0, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->Q:Lsoftware/simplicial/a/f/bh;

    if-eq v1, v3, :cond_1

    iget-object v1, v0, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->R:Lsoftware/simplicial/a/f/bh;

    if-eq v1, v3, :cond_1

    iget-object v1, v0, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    if-ne v1, v3, :cond_4

    .line 96
    :cond_1
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/be;->b(Lsoftware/simplicial/a/f/bi;)Z

    .line 103
    :goto_1
    iget-object v1, v0, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v2, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    if-ne v1, v2, :cond_5

    .line 104
    check-cast v0, Lsoftware/simplicial/a/f/w;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/f/ah;->a(Lsoftware/simplicial/a/f/w;)V

    .line 147
    :cond_2
    :goto_2
    return-void

    .line 88
    :cond_3
    new-instance v0, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    move-object v2, v0

    goto :goto_0

    .line 100
    :cond_4
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 143
    :catch_0
    move-exception v0

    .line 145
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 106
    :cond_5
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V

    goto :goto_2

    .line 108
    :cond_6
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    if-eqz v3, :cond_2

    invoke-static {v1}, Lsoftware/simplicial/a/t;->a(I)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {v1}, Lsoftware/simplicial/a/t;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 112
    :cond_7
    sget-object v3, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    if-eq v1, v3, :cond_8

    sget-object v3, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    if-eq v1, v3, :cond_8

    sget-object v3, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    .line 113
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    if-eq v1, v3, :cond_8

    sget-object v3, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    if-eq v1, v3, :cond_8

    sget-object v3, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 114
    invoke-virtual {v3}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v3

    if-ne v1, v3, :cond_9

    .line 116
    :cond_8
    const/4 v2, 0x5

    .line 117
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    shl-int/lit8 v0, v0, 0x18

    and-int/2addr v0, v5

    const/4 v3, 0x2

    aget-byte v3, p1, v3

    shl-int/lit8 v3, v3, 0x10

    const/high16 v5, 0xff0000

    and-int/2addr v3, v5

    add-int/2addr v0, v3

    const/4 v3, 0x3

    aget-byte v3, p1, v3

    shl-int/lit8 v3, v3, 0x8

    const v5, 0xff00

    and-int/2addr v3, v5

    add-int/2addr v0, v3

    const/4 v3, 0x4

    aget-byte v3, p1, v3

    shl-int/lit8 v3, v3, 0x0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v0, v3

    .line 120
    :cond_9
    sub-int v3, v4, v2

    new-array v3, v3, [B

    .line 121
    const/4 v5, 0x0

    sub-int/2addr v4, v2

    invoke-static {p1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v1, v0, v3}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 125
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_a

    .line 127
    new-instance v0, Lsoftware/simplicial/a/f/y;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/y;-><init>()V

    .line 128
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/y;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/f/ah;->a(Lsoftware/simplicial/a/f/y;)V

    goto/16 :goto_2

    .line 131
    :cond_a
    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_b

    .line 133
    new-instance v0, Lsoftware/simplicial/a/f/x;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/x;-><init>()V

    .line 134
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/x;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/f/ah;->a(Lsoftware/simplicial/a/f/x;)V

    goto/16 :goto_2

    .line 139
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bi;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method


# virtual methods
.method public a([B)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lsoftware/simplicial/nebulous/f/ah;->a([BZ)V

    .line 71
    :cond_0
    return-void
.end method

.method public a([BLjava/lang/Iterable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/Iterable",
            "<",
            "Lsoftware/simplicial/a/f/bl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/bl;

    .line 50
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    if-eqz v2, :cond_1

    iget v2, v0, Lsoftware/simplicial/a/f/bl;->a:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->r()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 51
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lsoftware/simplicial/nebulous/f/ah;->a([BZ)V

    .line 52
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    invoke-virtual {v2, p1, v0}, Lsoftware/simplicial/nebulous/b/d;->a([BLsoftware/simplicial/a/f/bl;)V

    goto :goto_0

    .line 55
    :cond_2
    return-void
.end method

.method public a([BLsoftware/simplicial/a/f/bl;)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    if-eqz v0, :cond_0

    iget v0, p2, Lsoftware/simplicial/a/f/bl;->a:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->r()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lsoftware/simplicial/nebulous/f/ah;->a([BZ)V

    .line 62
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/ah;->c:Lsoftware/simplicial/nebulous/b/d;

    invoke-virtual {v0, p1, p2}, Lsoftware/simplicial/nebulous/b/d;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 64
    :cond_1
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lsoftware/simplicial/nebulous/f/ah;->g:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lsoftware/simplicial/nebulous/f/ah;->e:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lsoftware/simplicial/nebulous/f/ah;->f:I

    return v0
.end method

.method public declared-synchronized e()Lsoftware/simplicial/a/f/aa;
    .locals 1

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    sget-object v0, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()V
    .locals 0

    .prologue
    .line 220
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
