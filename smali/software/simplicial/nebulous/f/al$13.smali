.class Lsoftware/simplicial/nebulous/f/al$13;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/a/h/f;ZLsoftware/simplicial/nebulous/f/al$e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$e;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$e;)V
    .locals 0

    .prologue
    .line 1510
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$13;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$13;->a:Lsoftware/simplicial/nebulous/f/al$e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 1516
    if-eqz p1, :cond_0

    .line 1518
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1521
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$13;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Error"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 1522
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$13;->a:Lsoftware/simplicial/nebulous/f/al$e;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lsoftware/simplicial/nebulous/f/al$e;->a(Z)V

    .line 1534
    :cond_0
    :goto_0
    return-void

    .line 1526
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$13;->a:Lsoftware/simplicial/nebulous/f/al$e;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lsoftware/simplicial/nebulous/f/al$e;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1530
    :catch_0
    move-exception v0

    .line 1532
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$13;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v1, "Unknown Error."

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
