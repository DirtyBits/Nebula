.class Lsoftware/simplicial/nebulous/f/al$22;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$k;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$ae;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$ae;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$ae;)V
    .locals 0

    .prologue
    .line 1943
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$22;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$22;->a:Lsoftware/simplicial/nebulous/f/al$ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1947
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1948
    sget-object v2, Lsoftware/simplicial/a/bn;->j:[Lsoftware/simplicial/a/bn;

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v2, v0

    .line 1949
    invoke-virtual {v5}, Lsoftware/simplicial/a/bn;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1950
    invoke-virtual {v5}, Lsoftware/simplicial/a/bn;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lsoftware/simplicial/a/bn;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1948
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1951
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1952
    const-string v0, "ModAIDs"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1954
    const-string v0, "ModAIDs"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    move v0, v1

    .line 1955
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 1956
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1955
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1958
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1959
    const-string v0, "clanAllies"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1961
    const-string v0, "clanAllies"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    move v0, v1

    .line 1962
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_3

    .line 1963
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1962
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1965
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1966
    const-string v0, "clanEnemies"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1968
    const-string v0, "clanEnemies"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 1969
    :goto_3
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 1970
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1969
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1972
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$22;->a:Lsoftware/simplicial/nebulous/f/al$ae;

    const-string v1, "SkinURLBase"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UploadSizeLimitBytes"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface/range {v0 .. v6}, Lsoftware/simplicial/nebulous/f/al$ae;->a(Ljava/lang/String;ILjava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 1973
    return-void
.end method
