.class Lsoftware/simplicial/nebulous/f/al$35;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;)V
    .locals 0

    .prologue
    .line 2521
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$35;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 2527
    if-eqz p1, :cond_0

    .line 2529
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2532
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2533
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$35;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 2567
    :cond_0
    :goto_0
    return-void

    .line 2537
    :cond_1
    const-string v0, "Purchases"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 2538
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2540
    const-string v0, "Coins"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2541
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$35;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2543
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 2545
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 2547
    new-instance v4, Lsoftware/simplicial/a/bm;

    invoke-direct {v4}, Lsoftware/simplicial/a/bm;-><init>()V

    .line 2548
    const-string v5, "OrderID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2549
    const-string v5, "OrderID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 2550
    :cond_2
    const-string v5, "TargetAccountID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2551
    const-string v5, "TargetAccountID"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lsoftware/simplicial/a/bm;->g:I

    .line 2552
    :cond_3
    const-string v5, "Type"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    .line 2553
    const-string v5, "ActivatedUtc"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2554
    invoke-static {v3}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    iput-object v3, v4, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    .line 2556
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2543
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2559
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$35;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2563
    :catch_0
    move-exception v0

    .line 2565
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$35;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to request purchases: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
