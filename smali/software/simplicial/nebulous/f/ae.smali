.class public final enum Lsoftware/simplicial/nebulous/f/ae;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/f/ae;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/f/ae;

.field public static final enum b:Lsoftware/simplicial/nebulous/f/ae;

.field public static final enum c:Lsoftware/simplicial/nebulous/f/ae;

.field public static final enum d:Lsoftware/simplicial/nebulous/f/ae;

.field public static final enum e:Lsoftware/simplicial/nebulous/f/ae;

.field public static final enum f:Lsoftware/simplicial/nebulous/f/ae;

.field private static final synthetic g:[Lsoftware/simplicial/nebulous/f/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "HARASSMENT"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->a:Lsoftware/simplicial/nebulous/f/ae;

    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "THREATS"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->b:Lsoftware/simplicial/nebulous/f/ae;

    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "SPAM"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->c:Lsoftware/simplicial/nebulous/f/ae;

    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->d:Lsoftware/simplicial/nebulous/f/ae;

    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "INAPPROPRIATE_SKIN"

    invoke-direct {v0, v1, v7}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->e:Lsoftware/simplicial/nebulous/f/ae;

    new-instance v0, Lsoftware/simplicial/nebulous/f/ae;

    const-string v1, "MAIL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/ae;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->f:Lsoftware/simplicial/nebulous/f/ae;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lsoftware/simplicial/nebulous/f/ae;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ae;->a:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/f/ae;->b:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/f/ae;->c:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/nebulous/f/ae;->d:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v1, v0, v6

    sget-object v1, Lsoftware/simplicial/nebulous/f/ae;->e:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lsoftware/simplicial/nebulous/f/ae;->f:Lsoftware/simplicial/nebulous/f/ae;

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/f/ae;->g:[Lsoftware/simplicial/nebulous/f/ae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/ae;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/nebulous/f/ae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/ae;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/f/ae;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->g:[Lsoftware/simplicial/nebulous/f/ae;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/f/ae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/f/ae;

    return-object v0
.end method
