.class Lsoftware/simplicial/nebulous/f/al$88;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->b(Lsoftware/simplicial/nebulous/f/x;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/x;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/x;)V
    .locals 0

    .prologue
    .line 5454
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$88;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$88;->a:Lsoftware/simplicial/nebulous/f/x;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 5461
    .line 5464
    :cond_0
    const-wide/16 v2, 0xa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 5465
    invoke-static {}, Lcom/facebook/z;->a()Lcom/facebook/z;

    move-result-object v2

    .line 5466
    add-int/lit8 v1, v1, 0x1

    .line 5467
    if-nez v2, :cond_1

    const/16 v3, 0x64

    if-lt v1, v3, :cond_0

    .line 5470
    :cond_1
    new-instance v1, Ljava/net/URL;

    const/16 v3, 0x100

    const/16 v4, 0x100

    invoke-virtual {v2, v3, v4}, Lcom/facebook/z;->a(II)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 5471
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 5472
    const/4 v3, 0x1

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 5473
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 5474
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 5475
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 5476
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5482
    :goto_0
    return-object v0

    .line 5478
    :catch_0
    move-exception v1

    .line 5480
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/al$88;->a:Lsoftware/simplicial/nebulous/f/x;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lsoftware/simplicial/nebulous/f/x;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 5488
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$88;->a:Lsoftware/simplicial/nebulous/f/x;

    invoke-interface {v0, p1}, Lsoftware/simplicial/nebulous/f/x;->a(Landroid/graphics/Bitmap;)V

    .line 5489
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5454
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$88;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5454
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$88;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
