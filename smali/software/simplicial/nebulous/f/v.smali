.class public Lsoftware/simplicial/nebulous/f/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private c:Lcom/a/a/a/a;

.field private d:Landroid/content/ServiceConnection;

.field private e:Lsoftware/simplicial/a/bm;

.field private f:Z

.field private g:I

.field private h:Lsoftware/simplicial/a/bm;

.field private i:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->a:Z

    .line 45
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    .line 47
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    .line 48
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->f:Z

    .line 49
    iput v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    .line 52
    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->i:Landroid/os/Bundle;

    .line 56
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 57
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/f/v;)Lcom/a/a/a/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/f/v;Lcom/a/a/a/a;)Lcom/a/a/a/a;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/f/v;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/f/v;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/f/v;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 342
    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 343
    const-string v2, "purchaseToken"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 345
    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    if-nez v2, :cond_0

    .line 365
    :goto_0
    monitor-exit p0

    return v0

    .line 349
    :cond_0
    const/4 v3, 0x3

    :try_start_1
    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4, v1}, Lcom/a/a/a/a;->b(ILjava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 351
    if-nez v1, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    iget-object v2, v2, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 352
    const/4 v2, 0x0

    iput-object v2, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    .line 354
    :cond_1
    if-eqz v1, :cond_2

    .line 356
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "consumePurchase returned "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 360
    :catch_0
    move-exception v0

    .line 362
    :try_start_2
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 365
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 96
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 99
    :try_start_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v3, :cond_0

    .line 164
    :try_start_2
    iget v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v1

    .line 167
    :goto_0
    monitor-exit p0

    return-object v0

    .line 101
    :cond_0
    :try_start_3
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    const/4 v5, 0x3

    iget-object v6, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "inapp"

    invoke-interface {v3, v5, v6, v7}, Lcom/a/a/a/a;->a(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 102
    :goto_1
    if-nez v0, :cond_2

    .line 164
    :try_start_4
    iget v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v0, v1

    .line 103
    goto :goto_0

    :cond_1
    move v0, v4

    .line 101
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 108
    :cond_3
    :try_start_5
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    const/4 v5, 0x3

    iget-object v6, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "inapp"

    invoke-interface {v3, v5, v6, v7, v0}, Lcom/a/a/a/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 109
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 111
    const-string v0, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 112
    const-string v3, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 114
    if-eqz v0, :cond_7

    .line 116
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v4

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 120
    const-string v9, "purchaseState"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v9

    if-nez v9, :cond_6

    if-eqz v6, :cond_6

    .line 124
    :try_start_6
    new-instance v9, Lsoftware/simplicial/a/bm;

    invoke-direct {v9}, Lsoftware/simplicial/a/bm;-><init>()V

    .line 126
    const-string v10, "orderId"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 127
    const-string v10, "orderId"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 128
    :cond_4
    const-string v10, "productId"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    .line 129
    iput-object v0, v9, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    .line 130
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v9, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    .line 131
    const-string v0, "targetAccountID"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 132
    const-string v0, "targetAccountID"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v9, Lsoftware/simplicial/a/bm;->g:I

    .line 134
    :cond_5
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 141
    :cond_6
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    .line 142
    goto :goto_2

    .line 136
    :catch_0
    move-exception v0

    .line 138
    :try_start_7
    sget-object v8, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 157
    :catch_1
    move-exception v0

    .line 159
    :try_start_8
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 164
    :try_start_9
    iget v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-object v0, v1

    .line 160
    goto/16 :goto_0

    .line 144
    :cond_7
    :try_start_a
    const-string v0, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_4
    if-nez v0, :cond_3

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->f:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 164
    :try_start_b
    iget v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-object v0, v2

    .line 167
    goto/16 :goto_0

    .line 148
    :cond_8
    :try_start_c
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v3, "Failed to get purchases"

    invoke-static {v0, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v3, "Failed to find purchases for this device."

    const/4 v5, 0x0

    invoke-static {v0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-object v0, v1

    .line 150
    goto :goto_4

    .line 164
    :catchall_0
    move-exception v0

    :try_start_d
    iget v1, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 96
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/f/v;->h()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 175
    if-nez v1, :cond_0

    .line 176
    const/4 v0, 0x0

    .line 182
    :goto_0
    monitor-exit p0

    return-object v0

    .line 178
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 179
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    const-string v4, "remove_ads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 180
    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v0, v1

    .line 182
    goto :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, -0x1

    .line 202
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 204
    :try_start_0
    const-string v0, "RESPONSE_CODE"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 205
    const-string v1, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 206
    const-string v2, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 208
    if-ne p2, v3, :cond_0

    if-eqz v1, :cond_0

    .line 210
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "purchase response code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 212
    if-eqz v0, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 219
    const-string v3, "productId"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 221
    new-instance v4, Lsoftware/simplicial/a/bm;

    invoke-direct {v4}, Lsoftware/simplicial/a/bm;-><init>()V

    .line 222
    iput-object v3, v4, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    .line 223
    const-string v5, "orderId"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 224
    const-string v5, "orderId"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 225
    :cond_2
    iput-object v1, v4, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    .line 226
    iput-object v2, v4, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 229
    :try_start_2
    const-string v1, "developerPayload"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v4, Lsoftware/simplicial/a/bm;->g:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 235
    :goto_1
    :try_start_3
    iput-object v4, p0, Lsoftware/simplicial/nebulous/f/v;->h:Lsoftware/simplicial/a/bm;

    .line 236
    const-string v1, "remove_ads"

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 237
    iput-object v4, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    .line 239
    :cond_3
    const-string v1, "productId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->i:Landroid/os/Bundle;

    const-string v2, "DETAILS_LIST"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 245
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 246
    const-string v0, "productId"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 249
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    iget-object v1, v4, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    const-string v2, "price_amount_micros"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-double v4, v4

    const-wide v6, 0x412e848000000000L    # 1000000.0

    div-double/2addr v4, v6

    const-string v2, "price_currency_code"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v5, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;DLjava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 258
    :catch_0
    move-exception v0

    .line 260
    :try_start_4
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 265
    :catch_1
    move-exception v0

    .line 267
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 231
    :catch_2
    move-exception v1

    .line 233
    const/4 v1, -0x1

    :try_start_5
    iput v1, v4, Lsoftware/simplicial/a/bm;->g:I
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->a:Z

    .line 63
    new-instance v0, Lsoftware/simplicial/nebulous/f/v$1;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/f/v$1;-><init>(Lsoftware/simplicial/nebulous/f/v;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->d:Landroid/content/ServiceConnection;

    .line 84
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->d:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 87
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;I)V
    .locals 8

    .prologue
    .line 275
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 320
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 278
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 279
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 281
    const-string v3, "ITEM_ID_LIST"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 283
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    const/4 v3, 0x3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "inapp"

    invoke-interface {v1, v3, v4, v5, v2}, Lcom/a/a/a/a;->a(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->i:Landroid/os/Bundle;

    .line 304
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->c:Lcom/a/a/a/a;

    const/4 v2, 0x3

    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v5, "inapp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v4, p1

    invoke-interface/range {v1 .. v6}, Lcom/a/a/a/a;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 306
    if-eqz v1, :cond_0

    .line 308
    const-string v2, "BUY_INTENT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/app/PendingIntent;

    move-object v2, v0

    .line 309
    if-eqz v2, :cond_0

    .line 312
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 313
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    const/16 v3, 0x3e9

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    .line 314
    const/4 v1, 0x1

    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/f/v;->a:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 316
    :catch_0
    move-exception v1

    .line 318
    :try_start_2
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 275
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public a(Ljava/util/List;Lsoftware/simplicial/nebulous/f/al$w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lsoftware/simplicial/nebulous/f/al$w;",
            ")V"
        }
    .end annotation

    .prologue
    .line 370
    new-instance v0, Lsoftware/simplicial/nebulous/f/v$3;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/f/v$3;-><init>(Lsoftware/simplicial/nebulous/f/v;Ljava/util/List;Lsoftware/simplicial/nebulous/f/al$w;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 434
    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/v$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 436
    return-void
.end method

.method public declared-synchronized a(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    new-instance v0, Lsoftware/simplicial/nebulous/f/v$2;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/f/v$2;-><init>(Lsoftware/simplicial/nebulous/f/v;Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)V

    .line 335
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 195
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 91
    const-string v0, "remove_ads"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->d:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 190
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 191
    return-void
.end method

.method public declared-synchronized c()Z
    .locals 2

    .prologue
    .line 441
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->f:Z

    if-nez v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/f/v;->g:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 442
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/f/v;->a()Ljava/util/List;

    .line 443
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 448
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->a:Z

    return v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 453
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/v;->a:Z

    .line 454
    return-void
.end method

.method public f()Lsoftware/simplicial/a/bm;
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->h:Lsoftware/simplicial/a/bm;

    .line 459
    const/4 v1, 0x0

    iput-object v1, p0, Lsoftware/simplicial/nebulous/f/v;->h:Lsoftware/simplicial/a/bm;

    .line 460
    return-object v0
.end method

.method public g()Lsoftware/simplicial/a/bm;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/v;->e:Lsoftware/simplicial/a/bm;

    return-object v0
.end method
