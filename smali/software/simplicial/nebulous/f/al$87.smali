.class Lsoftware/simplicial/nebulous/f/al$87;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/x;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/x;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Ljava/lang/String;Lsoftware/simplicial/nebulous/f/x;)V
    .locals 0

    .prologue
    .line 5417
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$87;->c:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$87;->a:Ljava/lang/String;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$87;->b:Lsoftware/simplicial/nebulous/f/x;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5423
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$87;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 5424
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    .line 5425
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 5426
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 5428
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 5429
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 5430
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 5431
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v1, 0x100

    const/16 v2, 0x100

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 5432
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$87;->b:Lsoftware/simplicial/nebulous/f/x;

    invoke-interface {v1, v0}, Lsoftware/simplicial/nebulous/f/x;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5438
    :goto_0
    return-object v4

    .line 5434
    :catch_0
    move-exception v0

    .line 5436
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$87;->b:Lsoftware/simplicial/nebulous/f/x;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lsoftware/simplicial/nebulous/f/x;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 5444
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$87;->b:Lsoftware/simplicial/nebulous/f/x;

    invoke-interface {v0, p1}, Lsoftware/simplicial/nebulous/f/x;->a(Landroid/graphics/Bitmap;)V

    .line 5445
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5417
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$87;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 5417
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/al$87;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
