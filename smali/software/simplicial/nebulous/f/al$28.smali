.class Lsoftware/simplicial/nebulous/f/al$28;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al$b;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/al$b;)V
    .locals 0

    .prologue
    .line 2210
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$28;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$28;->a:Lsoftware/simplicial/nebulous/f/al$b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 2216
    if-eqz p1, :cond_0

    .line 2218
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2243
    :cond_0
    :goto_0
    return-void

    .line 2224
    :cond_1
    const-string v0, "XPMultiplier"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2225
    const-string v1, "XPMultiplierDurationRemainingS"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2227
    const-string v2, "ClickType"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/s;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/a/s;

    move-result-object v2

    .line 2228
    const-string v3, "ClickDurationS"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2230
    const-string v4, "AlternateClick"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2231
    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/al$28;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    const-string v5, "AlternateClick"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, v4, Lsoftware/simplicial/nebulous/f/b;->o:Z

    .line 2235
    :goto_1
    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/al$28;->a:Lsoftware/simplicial/nebulous/f/al$b;

    invoke-interface {v4, v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al$b;->a(IILsoftware/simplicial/a/s;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2239
    :catch_0
    move-exception v0

    .line 2241
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$28;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to request active effects: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2233
    :cond_2
    :try_start_1
    iget-object v4, p0, Lsoftware/simplicial/nebulous/f/al$28;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lsoftware/simplicial/nebulous/f/b;->o:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
