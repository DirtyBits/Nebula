.class Lsoftware/simplicial/nebulous/f/al$25;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$ag;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field a:Z

.field final synthetic b:I

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al$ag;

.field final synthetic d:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;ILsoftware/simplicial/nebulous/f/al$ag;)V
    .locals 1

    .prologue
    .line 2055
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$25;->d:Lsoftware/simplicial/nebulous/f/al;

    iput p2, p0, Lsoftware/simplicial/nebulous/f/al$25;->b:I

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$25;->c:Lsoftware/simplicial/nebulous/f/al$ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2056
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/al$25;->a:Z

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 5

    .prologue
    .line 2069
    if-eqz p1, :cond_0

    .line 2071
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_2

    .line 2092
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/f/al$25;->a:Z

    if-nez v0, :cond_1

    .line 2094
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$25;->c:Lsoftware/simplicial/nebulous/f/al$ag;

    iget v1, p0, Lsoftware/simplicial/nebulous/f/al$25;->b:I

    const/4 v2, 0x0

    sget-object v3, Lsoftware/simplicial/nebulous/f/q;->b:Lsoftware/simplicial/nebulous/f/q;

    invoke-interface {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/al$ag;->a(ILandroid/graphics/Bitmap;Lsoftware/simplicial/nebulous/f/q;)V

    .line 2096
    :cond_1
    return-void

    .line 2077
    :cond_2
    :try_start_1
    const-string v0, "SkinStatus"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/q;->valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/q;

    move-result-object v1

    .line 2078
    const-string v0, "SkinData"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2080
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WS GotSkin "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lsoftware/simplicial/nebulous/f/al$25;->b:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v2, :cond_3

    const-string v0, " SUCCESS"

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 2082
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/f/al$25;->a:Z

    .line 2083
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$25;->c:Lsoftware/simplicial/nebulous/f/al$ag;

    iget v3, p0, Lsoftware/simplicial/nebulous/f/al$25;->b:I

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/o;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-interface {v0, v3, v2, v1}, Lsoftware/simplicial/nebulous/f/al$ag;->a(ILandroid/graphics/Bitmap;Lsoftware/simplicial/nebulous/f/q;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2087
    :catch_0
    move-exception v0

    .line 2089
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$25;->d:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to GetSkinData: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2080
    :cond_3
    :try_start_2
    const-string v0, " FAILED"
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method
