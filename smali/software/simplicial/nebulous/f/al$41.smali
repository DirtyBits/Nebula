.class Lsoftware/simplicial/nebulous/f/al$41;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;)V
    .locals 0

    .prologue
    .line 2859
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 18

    .prologue
    .line 2865
    if-eqz p1, :cond_0

    .line 2867
    :try_start_0
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2870
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2872
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 2874
    const-string v2, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2876
    sget-object v2, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2879
    :try_start_1
    const-string v3, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    .line 2884
    :goto_0
    :try_start_2
    sget-object v3, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-ne v2, v3, :cond_0

    .line 2886
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v3, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iput-object v3, v2, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 2887
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v3, 0x0

    iput-object v3, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 2888
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v2}, Lsoftware/simplicial/a/cc;->b()V

    .line 2934
    :cond_0
    :goto_1
    return-void

    .line 2894
    :cond_1
    const-string v2, "ClanName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2895
    invoke-static/range {p1 .. p1}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v5

    .line 2896
    const-string v2, "ClanMemberCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 2897
    sget-object v8, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2900
    :try_start_3
    const-string v2, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v8

    .line 2905
    :goto_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v8, v2, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 2906
    sget-object v2, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-ne v8, v2, :cond_5

    .line 2907
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v3, 0x0

    iput-object v3, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 2910
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v2, v5}, Lsoftware/simplicial/nebulous/f/ag;->a([B)V

    .line 2911
    const-string v2, "Motd"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2912
    const-string v2, "Public"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 2913
    const-string v2, "MinLevelToJoin"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 2914
    sget-object v2, Lsoftware/simplicial/nebulous/f/al;->b:Lb/a/a/c/a;

    const-string v3, "MinRoleToStartClanWar"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lsoftware/simplicial/a/q;

    .line 2915
    sget-object v11, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    .line 2916
    const-string v2, "MinRoleToJoinClanWar"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2917
    sget-object v2, Lsoftware/simplicial/nebulous/f/al;->b:Lb/a/a/c/a;

    const-string v3, "MinRoleToJoinClanWar"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/q;

    move-object v11, v2

    .line 2918
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v3, "ClanCoins"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v2, v12, v13}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 2919
    const-wide/16 v12, 0x0

    .line 2920
    const-wide/16 v14, 0x0

    .line 2921
    const-string v2, "CreateClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2922
    const-string v2, "CreateClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 2923
    :cond_3
    const-string v2, "RenameClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2924
    const-string v2, "RenameClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 2925
    :cond_4
    const-string v2, "clanID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 2926
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v3, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual/range {v3 .. v17}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JJII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_1

    .line 2930
    :catch_0
    move-exception v2

    .line 2932
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to request clan info: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2909
    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$41;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v4, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_3

    .line 2902
    :catch_1
    move-exception v2

    goto/16 :goto_2

    .line 2881
    :catch_2
    move-exception v3

    goto/16 :goto_0
.end method
