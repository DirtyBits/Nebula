.class Lsoftware/simplicial/nebulous/f/al$86;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/d;

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;Lsoftware/simplicial/nebulous/f/d;)V
    .locals 0

    .prologue
    .line 5317
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/f/al$86;->a:Lsoftware/simplicial/nebulous/f/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 5321
    if-eqz p1, :cond_4

    .line 5328
    :try_start_0
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5329
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 5333
    :cond_0
    const-string v0, "HasFriendRequests"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 5334
    const-string v0, "HasClanInvites"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 5335
    const-string v0, "ClanName"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5336
    invoke-static {p1}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v7

    .line 5338
    const-string v0, "ShowRateRequest"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "ShowRateRequest"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 5339
    const-string v0, "ShowRateRequest"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 5341
    :goto_0
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5344
    :try_start_1
    const-string v8, "ClanRole"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 5349
    :goto_1
    :try_start_2
    iget-object v8, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v8}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v8

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 5350
    iget-object v9, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v9}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v9

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v9, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 5351
    sget-object v9, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-ne v0, v9, :cond_5

    .line 5352
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v9, 0x0

    iput-object v9, v3, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 5355
    :goto_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v3, v7}, Lsoftware/simplicial/nebulous/f/ag;->a([B)V

    .line 5358
    const-string v3, "MOTD"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "MOTD"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 5359
    const-string v3, "MOTD"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5362
    :goto_3
    const-string v7, "ServerMessage"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "ServerMessage"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 5363
    const-string v4, "ServerMessage"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5366
    :cond_1
    const-string v7, "NewMail"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 5367
    const-string v6, "NewMail"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 5369
    :cond_2
    const-string v7, "ServerMail"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 5370
    iget-object v7, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v7

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    const-string v9, "ServerMail"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v7, Lsoftware/simplicial/nebulous/f/b;->o:Z

    .line 5391
    :goto_4
    iget-object v7, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v7

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v9, "Coins"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v7, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 5392
    sget-object v7, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-eq v8, v7, :cond_3

    sget-object v7, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-ne v0, v7, :cond_3

    .line 5393
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0}, Lsoftware/simplicial/a/cc;->b()V

    .line 5394
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/al$86;->a:Lsoftware/simplicial/nebulous/f/d;

    invoke-interface/range {v0 .. v6}, Lsoftware/simplicial/nebulous/f/d;->a(ZZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 5402
    :cond_4
    :goto_5
    return-void

    .line 5354
    :cond_5
    iget-object v9, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v9}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v9

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v3, v9, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 5396
    :catch_0
    move-exception v0

    .line 5398
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to parse alerts result."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    .line 5399
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_5

    .line 5372
    :cond_6
    :try_start_3
    iget-object v7, p0, Lsoftware/simplicial/nebulous/f/al$86;->b:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v7

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    const/4 v9, 0x0

    iput-boolean v9, v7, Lsoftware/simplicial/nebulous/f/b;->o:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    .line 5346
    :catch_1
    move-exception v8

    goto/16 :goto_1

    :cond_7
    move-object v3, v4

    goto/16 :goto_3

    :cond_8
    move v5, v6

    goto/16 :goto_0
.end method
