.class Lsoftware/simplicial/nebulous/f/al$73;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(ZILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;)V
    .locals 0

    .prologue
    .line 4851
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 18

    .prologue
    .line 4857
    if-eqz p1, :cond_1

    .line 4859
    :try_start_0
    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4862
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v3, "Error"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;Z)V

    .line 4865
    :cond_0
    const-string v2, "ClanName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4866
    invoke-static/range {p1 .. p1}, Lsoftware/simplicial/nebulous/f/al;->a(Lorg/json/JSONObject;)[B

    move-result-object v5

    .line 4867
    const-string v2, "ClanMemberCount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 4868
    sget-object v8, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4871
    :try_start_1
    const-string v2, "ClanRole"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/q;->a(Ljava/lang/String;)Lsoftware/simplicial/a/q;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    .line 4876
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v8, v2, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    .line 4877
    sget-object v2, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    if-ne v8, v2, :cond_2

    .line 4878
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v3, 0x0

    iput-object v3, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 4881
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v2, v5}, Lsoftware/simplicial/nebulous/f/ag;->a([B)V

    .line 4882
    const-string v2, "Motd"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 4883
    const-string v2, "Public"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 4884
    const-string v2, "MinLevelToJoin"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 4885
    sget-object v2, Lsoftware/simplicial/nebulous/f/al;->b:Lb/a/a/c/a;

    const-string v3, "MinRoleToStartClanWar"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lsoftware/simplicial/a/q;

    .line 4886
    sget-object v2, Lsoftware/simplicial/nebulous/f/al;->b:Lb/a/a/c/a;

    const-string v3, "MinRoleToJoinClanWar"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lsoftware/simplicial/a/q;

    .line 4887
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    const-string v3, "ClanCoins"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v2, v12, v13}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 4888
    const-string v2, "CreateClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 4889
    const-string v2, "RenameClanPrice"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 4890
    const-string v2, "clanID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 4891
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v3, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual/range {v3 .. v17}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JJII)V

    .line 4898
    :cond_1
    :goto_2
    return-void

    .line 4880
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v4, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 4894
    :catch_0
    move-exception v2

    .line 4896
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/al$73;->a:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to update clan info:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 4873
    :catch_1
    move-exception v2

    goto/16 :goto_0
.end method
