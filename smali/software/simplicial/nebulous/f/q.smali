.class public final enum Lsoftware/simplicial/nebulous/f/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/f/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/f/q;

.field public static final enum b:Lsoftware/simplicial/nebulous/f/q;

.field public static final enum c:Lsoftware/simplicial/nebulous/f/q;

.field public static final enum d:Lsoftware/simplicial/nebulous/f/q;

.field public static final enum e:Lsoftware/simplicial/nebulous/f/q;

.field private static final synthetic f:[Lsoftware/simplicial/nebulous/f/q;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Lsoftware/simplicial/nebulous/f/q;

    const-string v1, "UNUSED"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->a:Lsoftware/simplicial/nebulous/f/q;

    new-instance v0, Lsoftware/simplicial/nebulous/f/q;

    const-string v1, "IN_REVIEW"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/f/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->b:Lsoftware/simplicial/nebulous/f/q;

    new-instance v0, Lsoftware/simplicial/nebulous/f/q;

    const-string v1, "REFUNDED"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/f/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->c:Lsoftware/simplicial/nebulous/f/q;

    new-instance v0, Lsoftware/simplicial/nebulous/f/q;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/f/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->d:Lsoftware/simplicial/nebulous/f/q;

    new-instance v0, Lsoftware/simplicial/nebulous/f/q;

    const-string v1, "APPROVED"

    invoke-direct {v0, v1, v6}, Lsoftware/simplicial/nebulous/f/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->e:Lsoftware/simplicial/nebulous/f/q;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lsoftware/simplicial/nebulous/f/q;

    sget-object v1, Lsoftware/simplicial/nebulous/f/q;->a:Lsoftware/simplicial/nebulous/f/q;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/f/q;->b:Lsoftware/simplicial/nebulous/f/q;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/f/q;->c:Lsoftware/simplicial/nebulous/f/q;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/f/q;->d:Lsoftware/simplicial/nebulous/f/q;

    aput-object v1, v0, v5

    sget-object v1, Lsoftware/simplicial/nebulous/f/q;->e:Lsoftware/simplicial/nebulous/f/q;

    aput-object v1, v0, v6

    sput-object v0, Lsoftware/simplicial/nebulous/f/q;->f:[Lsoftware/simplicial/nebulous/f/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/f/q;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lsoftware/simplicial/nebulous/f/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/q;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/f/q;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lsoftware/simplicial/nebulous/f/q;->f:[Lsoftware/simplicial/nebulous/f/q;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/f/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/f/q;

    return-object v0
.end method
