.class Lsoftware/simplicial/nebulous/f/al$51;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$z;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$v;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lsoftware/simplicial/nebulous/f/al$v;

.field final synthetic c:Lsoftware/simplicial/nebulous/f/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/f/al;ZLsoftware/simplicial/nebulous/f/al$v;)V
    .locals 0

    .prologue
    .line 3393
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/f/al$51;->a:Z

    iput-object p3, p0, Lsoftware/simplicial/nebulous/f/al$51;->b:Lsoftware/simplicial/nebulous/f/al$v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;)V
    .locals 17

    .prologue
    .line 3399
    if-eqz p1, :cond_2

    .line 3401
    :try_start_0
    const-string v2, "Mail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 3403
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 3405
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 3407
    invoke-virtual {v14, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 3408
    new-instance v3, Lsoftware/simplicial/nebulous/f/y;

    const-string v4, "MsgID"

    invoke-virtual {v13, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string v6, "FromAID"

    invoke-virtual {v13, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v7, "FromName"

    invoke-virtual {v13, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "ToAID"

    .line 3409
    invoke-virtual {v13, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v9, "ToName"

    invoke-virtual {v13, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "Subject"

    .line 3410
    invoke-virtual {v13, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "IsNew"

    invoke-virtual {v13, v11}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    const-string v12, "TimeSent"

    invoke-virtual {v13, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v12

    const-string v16, "TimeExpires"

    .line 3411
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lb/a/a/b/a;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, Lsoftware/simplicial/nebulous/f/y;-><init>(JILjava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/util/Date;Ljava/util/Date;)V

    .line 3412
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3405
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3415
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->a:Z

    if-eqz v2, :cond_1

    .line 3417
    new-instance v3, Lsoftware/simplicial/nebulous/f/y;

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    const v7, 0x7f080243

    invoke-virtual {v2, v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    .line 3418
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v2, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    .line 3419
    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    const v10, 0x7f0802ee

    invoke-virtual {v2, v10}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    new-instance v13, Ljava/util/Date;

    invoke-direct {v13}, Ljava/util/Date;-><init>()V

    invoke-direct/range {v3 .. v13}, Lsoftware/simplicial/nebulous/f/y;-><init>(JILjava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/util/Date;Ljava/util/Date;)V

    .line 3420
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3423
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->b:Lsoftware/simplicial/nebulous/f/al$v;

    invoke-interface {v2, v15}, Lsoftware/simplicial/nebulous/f/al$v;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3430
    :cond_2
    :goto_1
    return-void

    .line 3426
    :catch_0
    move-exception v2

    .line 3428
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/al$51;->c:Lsoftware/simplicial/nebulous/f/al;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    const-string v3, "Failed to GetMailList."

    invoke-virtual {v2, v3}, Lsoftware/simplicial/a/cc;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
