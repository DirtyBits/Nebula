.class public Lsoftware/simplicial/nebulous/f/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/t;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    .line 20
    iput-object p1, p0, Lsoftware/simplicial/nebulous/f/u;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 21
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 39
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/t;

    .line 41
    iget v2, v0, Lsoftware/simplicial/nebulous/f/t;->a:I

    if-ne p1, v2, :cond_0

    .line 43
    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/f/t;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :cond_1
    monitor-exit p0

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/u;->b(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v0

    .line 27
    invoke-virtual {v0, p2}, Lsoftware/simplicial/nebulous/f/t;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    monitor-exit p0

    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 76
    iget v2, v0, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/f/u;->b(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v2

    .line 77
    iget-object v3, v0, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/f/t;->a(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, v0, Lsoftware/simplicial/a/bg;->h:Lsoftware/simplicial/a/bg$a;

    iput-object v0, v2, Lsoftware/simplicial/nebulous/f/t;->d:Lsoftware/simplicial/a/bg$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(I)Lsoftware/simplicial/nebulous/f/t;
    .locals 3

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/u;->c(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v0

    .line 52
    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lsoftware/simplicial/nebulous/f/t;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lsoftware/simplicial/nebulous/f/t;-><init>(ILjava/lang/String;)V

    .line 55
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    monitor-exit p0

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 32
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/u;->b(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/u;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lsoftware/simplicial/nebulous/f/t;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    monitor-exit p0

    return-void

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/t;

    .line 91
    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/f/t;->c:Z

    if-nez v2, :cond_1

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/t;->d:Lsoftware/simplicial/a/bg$a;

    sget-object v2, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v0, v2, :cond_0

    .line 93
    :cond_1
    const/4 v0, 0x1

    .line 96
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized c(I)Lsoftware/simplicial/nebulous/f/t;
    .locals 3

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/t;

    .line 64
    iget v2, v0, Lsoftware/simplicial/nebulous/f/t;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v2, :cond_0

    .line 69
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 102
    return-void
.end method

.method public d(I)V
    .locals 5

    .prologue
    .line 106
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/t;

    .line 111
    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/t;->d:Lsoftware/simplicial/a/bg$a;

    sget-object v4, Lsoftware/simplicial/a/bg$a;->b:Lsoftware/simplicial/a/bg$a;

    if-ne v3, v4, :cond_0

    .line 113
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 123
    :goto_1
    return-void

    .line 120
    :cond_2
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/f/u;->c(I)Lsoftware/simplicial/nebulous/f/t;

    move-result-object v0

    .line 121
    iget-object v1, p0, Lsoftware/simplicial/nebulous/f/u;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method
