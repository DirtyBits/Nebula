.class public Lsoftware/simplicial/nebulous/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:I

.field private E:I

.field private F:I

.field private volatile G:Z

.field private H:Z

.field public final a:F

.field public final b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:Z

.field public i:Z

.field public j:F

.field public k:F

.field public l:F

.field public m:F

.field public n:Landroid/graphics/Rect;

.field public o:Landroid/graphics/Rect;

.field public p:Landroid/graphics/Rect;

.field public q:Landroid/graphics/Rect;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/nebulous/c/c;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:J

.field private v:Landroid/util/DisplayMetrics;

.field private w:Lsoftware/simplicial/nebulous/f/ag;

.field private x:Lsoftware/simplicial/a/t;

.field private y:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private z:F


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/f/ag;Lsoftware/simplicial/a/t;Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 7

    .prologue
    const/high16 v6, 0x42200000    # 40.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    .line 63
    iput v3, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    .line 64
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/c/d;->u:J

    .line 66
    const/high16 v0, 0x428c0000    # 70.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->a:F

    .line 67
    iput v6, p0, Lsoftware/simplicial/nebulous/c/d;->b:F

    .line 68
    const/high16 v0, 0x428c0000    # 70.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    .line 69
    iput v6, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    .line 71
    iput v5, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    .line 72
    iput v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    .line 73
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    .line 74
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->i:Z

    .line 75
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->j:F

    .line 76
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->k:F

    .line 77
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 78
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    .line 81
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    .line 82
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->q:Landroid/graphics/Rect;

    .line 87
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->z:F

    .line 88
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->A:F

    .line 89
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->B:F

    .line 90
    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->C:F

    .line 91
    iput v4, p0, Lsoftware/simplicial/nebulous/c/d;->D:I

    .line 92
    iput v4, p0, Lsoftware/simplicial/nebulous/c/d;->E:I

    .line 93
    iput v4, p0, Lsoftware/simplicial/nebulous/c/d;->F:I

    .line 94
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z

    .line 100
    iput-object p1, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    .line 101
    iput-object p2, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 102
    iput-object p3, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 104
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/c/d;->a()V

    .line 105
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    .line 106
    return-void
.end method

.method private a(Lsoftware/simplicial/nebulous/c/c;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x19

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 436
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 437
    iget-object v0, p1, Lsoftware/simplicial/nebulous/c/c;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    .line 438
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    .line 440
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 441
    if-nez v0, :cond_2

    .line 442
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    move v2, v1

    .line 447
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_4

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/c/c;

    .line 450
    iget-object v1, v0, Lsoftware/simplicial/nebulous/c/c;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 451
    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    .line 453
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 454
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-gt v4, v3, :cond_3

    .line 455
    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    :goto_2
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 460
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_6

    move v0, v3

    :goto_3
    move v2, v0

    .line 462
    goto :goto_1

    .line 444
    :cond_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 457
    :cond_3
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 464
    :cond_4
    if-eqz v2, :cond_5

    .line 466
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x21

    if-gt v0, v1, :cond_5

    .line 468
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->p()V

    .line 469
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/c/d;->a()V

    .line 470
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/e;->d()V

    .line 471
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080102

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08006a

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f0801cf

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 474
    :cond_5
    return-void

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method private a(ZZ)Z
    .locals 5

    .prologue
    const/high16 v4, -0x40800000    # -1.0f

    .line 771
    sget-object v0, Lsoftware/simplicial/nebulous/c/d$1;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 793
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 774
    :pswitch_0
    if-eqz p1, :cond_1

    .line 776
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 777
    iget-wide v2, p0, Lsoftware/simplicial/nebulous/c/d;->u:J

    sub-long v2, v0, v2

    .line 778
    iput-wide v0, p0, Lsoftware/simplicial/nebulous/c/d;->u:J

    .line 779
    new-instance v0, Lsoftware/simplicial/nebulous/c/c;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v4, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {v0, v2, v3, v1}, Lsoftware/simplicial/nebulous/c/c;-><init>(JLandroid/graphics/PointF;)V

    .line 780
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/c/d;->a(Lsoftware/simplicial/nebulous/c/c;)V

    .line 782
    if-eqz p2, :cond_0

    .line 783
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->j()V

    .line 791
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 785
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->i()V

    goto :goto_1

    .line 789
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->k()V

    goto :goto_1

    .line 771
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(FF)V
    .locals 8

    .prologue
    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    .line 478
    mul-float v0, p1, p1

    mul-float v1, p2, p2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Lsoftware/simplicial/a/bb;->a(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 479
    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 480
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    .line 481
    :cond_0
    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    div-float v1, v0, v1

    .line 482
    float-to-double v2, p2

    float-to-double v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v0, v2

    .line 483
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v3, Lsoftware/simplicial/nebulous/c/b;->d:Lsoftware/simplicial/nebulous/c/b;

    if-ne v2, v3, :cond_1

    .line 485
    float-to-double v2, v0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 486
    float-to-double v2, v0

    cmpl-double v2, v2, v6

    if-lez v2, :cond_1

    .line 487
    float-to-double v2, v0

    sub-double/2addr v2, v6

    double-to-float v0, v2

    .line 490
    :cond_1
    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    float-to-double v2, v2

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->e:F

    mul-float/2addr v4, v1

    float-to-double v4, v4

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->j:F

    .line 491
    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    float-to-double v2, v2

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->e:F

    mul-float/2addr v4, v1

    float-to-double v4, v4

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lsoftware/simplicial/nebulous/c/d;->k:F

    .line 494
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->C()Lsoftware/simplicial/a/bf;

    move-result-object v2

    .line 495
    if-eqz v2, :cond_2

    .line 497
    iput v0, v2, Lsoftware/simplicial/a/bf;->M:F

    .line 498
    iput v1, v2, Lsoftware/simplicial/a/bf;->O:F

    .line 500
    :cond_2
    return-void
.end method

.method private c(FF)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/high16 v2, 0x40000000    # 2.0f

    .line 504
    sget-object v0, Lsoftware/simplicial/nebulous/c/d$1;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/c/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 555
    :cond_0
    :goto_0
    return-void

    .line 507
    :pswitch_0
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 508
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    .line 509
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    goto :goto_0

    .line 512
    :pswitch_1
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 513
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    .line 514
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    goto :goto_0

    .line 517
    :pswitch_2
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 518
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    .line 519
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    goto :goto_0

    .line 522
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    sget-object v1, Lsoftware/simplicial/nebulous/c/a;->a:Lsoftware/simplicial/nebulous/c/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    sget-object v1, Lsoftware/simplicial/nebulous/c/a;->d:Lsoftware/simplicial/nebulous/c/a;

    if-ne v0, v1, :cond_2

    .line 524
    :cond_1
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1, v2}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 525
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1, v2}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    .line 537
    :goto_1
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    goto :goto_0

    .line 527
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    sget-object v1, Lsoftware/simplicial/nebulous/c/a;->b:Lsoftware/simplicial/nebulous/c/a;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    sget-object v1, Lsoftware/simplicial/nebulous/c/a;->e:Lsoftware/simplicial/nebulous/c/a;

    if-ne v0, v1, :cond_4

    .line 529
    :cond_3
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0, v1}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 530
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1, v2}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    goto :goto_1

    .line 534
    :cond_4
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    div-float/2addr v0, v2

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 535
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1, v2}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    goto :goto_1

    .line 540
    :pswitch_4
    cmpl-float v0, p1, v4

    if-ltz v0, :cond_5

    cmpl-float v0, p2, v4

    if-ltz v0, :cond_5

    .line 542
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    if-nez v0, :cond_0

    .line 544
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    .line 545
    iput p1, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    .line 546
    iput p2, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    goto/16 :goto_0

    .line 551
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    goto/16 :goto_0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 735
    sget-object v0, Lsoftware/simplicial/nebulous/c/d$1;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 741
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 738
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 739
    const/4 v0, 0x1

    goto :goto_0

    .line 735
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private f()Z
    .locals 12

    .prologue
    const/4 v10, 0x1

    .line 746
    sget-object v0, Lsoftware/simplicial/nebulous/c/d$1;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 766
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 749
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    move v0, v10

    .line 750
    goto :goto_0

    .line 752
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    iget-boolean v0, v0, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    :goto_1
    move v0, v10

    .line 764
    goto :goto_0

    .line 755
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 756
    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    :goto_2
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 757
    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    :goto_3
    iget-object v5, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 758
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 759
    invoke-virtual {v6}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v6

    sget-object v7, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    :goto_4
    iget-object v7, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    .line 760
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 761
    invoke-virtual {v8}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v8

    sget-object v9, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v8, v9, :cond_4

    iget-object v8, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->c()Ljava/lang/String;

    move-result-object v8

    :goto_5
    iget-object v9, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 762
    invoke-virtual {v9}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v9

    sget-object v11, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v9, v11, :cond_5

    iget-object v9, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 755
    :goto_6
    invoke-virtual/range {v0 .. v9}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILsoftware/simplicial/a/bd;ILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto :goto_1

    .line 756
    :cond_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    goto :goto_2

    .line 757
    :cond_2
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    goto :goto_3

    .line 759
    :cond_3
    iget-object v6, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    goto :goto_4

    .line 761
    :cond_4
    iget-object v8, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 762
    :cond_5
    iget-object v9, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    goto :goto_6

    .line 746
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 798
    sget-object v0, Lsoftware/simplicial/nebulous/c/d$1;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 804
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 801
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->h()V

    .line 802
    const/4 v0, 0x1

    goto :goto_0

    .line 798
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->t:I

    .line 113
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/c/d;->u:J

    .line 114
    return-void
.end method

.method public declared-synchronized a(FF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 150
    :cond_1
    cmpg-float v0, p1, v1

    if-lez v0, :cond_0

    cmpg-float v0, p2, v1

    if-lez v0, :cond_0

    .line 153
    :try_start_1
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_2

    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    .line 156
    :cond_2
    iput p1, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    .line 157
    iput p2, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    .line 159
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/c/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 133
    :goto_0
    monitor-exit p0

    return-void

    .line 130
    :cond_0
    const/high16 v0, 0x42200000    # 40.0f

    add-int/lit8 v1, p1, -0x37

    int-to-float v1, v1

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    :try_start_1
    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    .line 131
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0, v1}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->e:F

    .line 132
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/c/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    invoke-static {v0, p1}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->e:F

    .line 120
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    .line 122
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/c/d;->a(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/hardware/SensorEvent;I)V
    .locals 8

    .prologue
    const/high16 v6, 0x40a00000    # 5.0f

    .line 172
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v1, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v1, :cond_1

    .line 217
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 175
    :cond_1
    :try_start_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 176
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    .line 180
    packed-switch p2, :pswitch_data_0

    move v7, v1

    move v1, v0

    move v0, v7

    .line 201
    :goto_1
    float-to-double v2, v0

    const-wide v4, 0x400666666db6db6eL    # 2.8000000544956754

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 204
    const v2, 0x401ccccd    # 2.45f

    .line 205
    iget v3, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    mul-float/2addr v1, v3

    div-float/2addr v1, v2

    .line 206
    iget v3, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    mul-float/2addr v0, v3

    div-float/2addr v0, v2

    .line 208
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v6

    if-gez v2, :cond_2

    .line 210
    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->B:F

    .line 211
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->C:F

    .line 213
    :cond_2
    iput v1, p0, Lsoftware/simplicial/nebulous/c/d;->B:F

    .line 214
    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->C:F

    .line 216
    invoke-direct {p0, v1, v0}, Lsoftware/simplicial/nebulous/c/d;->b(FF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 183
    :pswitch_0
    neg-float v2, v0

    .line 184
    neg-float v0, v1

    move v1, v2

    .line 185
    goto :goto_1

    .line 189
    :pswitch_1
    neg-float v0, v0

    .line 190
    goto :goto_1

    :pswitch_2
    move v7, v1

    move v1, v0

    move v0, v7

    .line 192
    goto :goto_1

    .line 195
    :pswitch_3
    neg-float v1, v1

    .line 196
    goto :goto_1

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized a(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 681
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getSource()I

    move-result v2

    and-int/lit16 v2, v2, 0x401

    const/16 v3, 0x401

    if-ne v2, v3, :cond_0

    .line 683
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 685
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v0, v1

    .line 711
    :goto_0
    monitor-exit p0

    return v0

    .line 688
    :sswitch_0
    :try_start_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    if-eqz v2, :cond_1

    .line 689
    :goto_1
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lsoftware/simplicial/nebulous/c/d;->a(ZZ)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 688
    goto :goto_1

    .line 691
    :sswitch_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    if-nez v2, :cond_2

    .line 693
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/c/d;->a(ZZ)Z

    move-result v0

    goto :goto_0

    .line 697
    :cond_2
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    if-nez v2, :cond_3

    move v1, v0

    :cond_3
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/c/d;->H:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 702
    :sswitch_2
    :try_start_2
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/c/d;->g()Z

    move-result v0

    goto :goto_0

    .line 705
    :sswitch_3
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/c/d;->e()Z

    move-result v0

    goto :goto_0

    .line 707
    :sswitch_4
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/c/d;->f()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_0

    .line 685
    :sswitch_data_0
    .sparse-switch
        0x52 -> :sswitch_3
        0x60 -> :sswitch_0
        0x61 -> :sswitch_1
        0x63 -> :sswitch_2
        0x64 -> :sswitch_2
        0x6c -> :sswitch_4
        0x6d -> :sswitch_3
    .end sparse-switch
.end method

.method public declared-synchronized a(Landroid/view/MotionEvent;)Z
    .locals 19

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/c/d;->G:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    .line 222
    const/4 v2, 0x0

    .line 431
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 224
    :cond_1
    const/4 v5, 0x0

    .line 225
    const/4 v4, 0x0

    .line 228
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->B()Lsoftware/simplicial/a/u;

    move-result-object v9

    .line 229
    sget-object v3, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    .line 230
    const/4 v2, 0x0

    .line 231
    if-eqz v9, :cond_23

    .line 233
    invoke-virtual {v9}, Lsoftware/simplicial/a/u;->f()Z

    move-result v5

    .line 234
    invoke-virtual {v9}, Lsoftware/simplicial/a/u;->g()Z

    move-result v4

    .line 235
    iget-boolean v2, v9, Lsoftware/simplicial/a/u;->e:Z

    .line 236
    invoke-virtual {v9}, Lsoftware/simplicial/a/u;->e()Lsoftware/simplicial/a/bf;

    move-result-object v3

    iget-object v3, v3, Lsoftware/simplicial/a/bf;->ao:Lsoftware/simplicial/a/s;

    move v6, v2

    move v7, v4

    move v8, v5

    move-object v5, v3

    .line 239
    :goto_1
    const/4 v4, 0x0

    .line 244
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v10

    .line 246
    const/4 v3, 0x0

    .line 248
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    const v11, 0x1000010

    and-int/2addr v2, v11

    const v11, 0x1000010

    if-ne v2, v11, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v11, 0x2

    if-ne v2, v11, :cond_a

    .line 250
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v7

    .line 252
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v5

    .line 253
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v4

    .line 254
    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v3

    invoke-virtual {v7, v2, v3}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v3

    .line 255
    const/4 v2, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v8

    invoke-virtual {v7, v2, v8}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v2

    .line 256
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v8

    invoke-virtual {v3}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v10

    cmpg-float v8, v8, v10

    if-gtz v8, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    invoke-virtual {v2}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v10

    cmpg-float v8, v8, v10

    if-gtz v8, :cond_2

    .line 258
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v5

    .line 259
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v4

    .line 260
    const/16 v2, 0xf

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v3

    invoke-virtual {v7, v2, v3}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v3

    .line 261
    const/16 v2, 0x10

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v8

    invoke-virtual {v7, v2, v8}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v2

    .line 263
    :cond_2
    if-eqz v3, :cond_22

    if-eqz v2, :cond_22

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v8

    invoke-virtual {v3}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v10

    cmpg-float v8, v8, v10

    if-gtz v8, :cond_22

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v8

    invoke-virtual {v2}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v10

    cmpg-float v8, v8, v10

    if-gtz v8, :cond_22

    .line 265
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v5

    .line 266
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v4

    .line 267
    const/16 v2, 0xb

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v3

    invoke-virtual {v7, v2, v3}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v3

    .line 268
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v8

    invoke-virtual {v7, v2, v8}, Landroid/view/InputDevice;->getMotionRange(II)Landroid/view/InputDevice$MotionRange;

    move-result-object v2

    move-object/from16 v18, v3

    move v3, v5

    move v5, v4

    move-object/from16 v4, v18

    .line 271
    :goto_2
    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-virtual {v4}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v7, v7, v8

    if-gez v7, :cond_8

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-virtual {v2}, Landroid/view/InputDevice$MotionRange;->getFlat()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v7, v2

    if-gez v2, :cond_8

    .line 273
    :cond_3
    const/4 v3, 0x0

    .line 274
    const/4 v2, 0x0

    .line 283
    :goto_3
    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    .line 284
    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    .line 285
    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_4

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_9

    :cond_4
    if-eqz v6, :cond_9

    const/4 v2, 0x1

    :goto_4
    move v3, v2

    .line 387
    :cond_5
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_6

    if-eqz v3, :cond_20

    .line 389
    :cond_6
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lsoftware/simplicial/nebulous/c/d;->i:Z

    .line 391
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v5, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;

    if-eq v4, v5, :cond_0

    .line 393
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lsoftware/simplicial/nebulous/c/d;->c(FF)V

    .line 395
    if-eqz v3, :cond_7

    .line 397
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->l:F

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    .line 398
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->m:F

    add-float/2addr v3, v4

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    .line 401
    :cond_7
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->l:F

    sub-float/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v4, v4

    const/high16 v5, 0x43200000    # 160.0f

    div-float/2addr v4, v5

    div-float/2addr v3, v4

    .line 402
    move-object/from16 v0, p0

    iget v4, v0, Lsoftware/simplicial/nebulous/c/d;->m:F

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    sub-float/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v5, v5

    const/high16 v6, 0x43200000    # 160.0f

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    .line 404
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lsoftware/simplicial/nebulous/c/d;->b(FF)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 221
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 278
    :cond_8
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/c/d;->d:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v7, v7

    mul-float/2addr v2, v7

    const/high16 v7, 0x43200000    # 160.0f

    div-float/2addr v2, v7

    mul-float/2addr v2, v3

    invoke-virtual {v4}, Landroid/view/InputDevice$MotionRange;->getRange()F

    move-result v3

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v3, v7

    div-float v3, v2, v3

    .line 279
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/c/d;->d:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v7, v7

    mul-float/2addr v2, v7

    const/high16 v7, 0x43200000    # 160.0f

    div-float/2addr v2, v7

    mul-float/2addr v2, v5

    invoke-virtual {v4}, Landroid/view/InputDevice$MotionRange;->getRange()F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    div-float/2addr v2, v4

    goto/16 :goto_3

    .line 285
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 290
    :cond_a
    const/4 v2, 0x0

    move/from16 v18, v2

    move v2, v4

    move/from16 v4, v18

    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ge v4, v6, :cond_5

    .line 292
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    .line 293
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v11

    .line 295
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v12

    .line 296
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v13

    .line 298
    if-nez v10, :cond_b

    if-eqz v4, :cond_c

    :cond_b
    const/4 v14, 0x5

    if-ne v10, v14, :cond_19

    if-ne v4, v11, :cond_19

    .line 302
    :cond_c
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    float-to-int v14, v12

    float-to-int v15, v13

    invoke-virtual {v11, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    move-result v11

    if-eqz v11, :cond_10

    if-eqz v8, :cond_10

    .line 304
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 305
    move-object/from16 v0, p0

    iget-wide v0, v0, Lsoftware/simplicial/nebulous/c/d;->u:J

    move-wide/from16 v16, v0

    sub-long v16, v14, v16

    .line 306
    move-object/from16 v0, p0

    iput-wide v14, v0, Lsoftware/simplicial/nebulous/c/d;->u:J

    .line 307
    new-instance v2, Lsoftware/simplicial/nebulous/c/c;

    new-instance v11, Landroid/graphics/PointF;

    invoke-direct {v11, v12, v13}, Landroid/graphics/PointF;-><init>(FF)V

    move-wide/from16 v0, v16

    invoke-direct {v2, v0, v1, v11}, Lsoftware/simplicial/nebulous/c/c;-><init>(JLandroid/graphics/PointF;)V

    .line 308
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/nebulous/c/d;->a(Lsoftware/simplicial/nebulous/c/c;)V

    .line 310
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    if-nez v2, :cond_f

    .line 311
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->i()V

    .line 315
    :goto_6
    move-object/from16 v0, p0

    iput v6, v0, Lsoftware/simplicial/nebulous/c/d;->D:I

    .line 316
    const/4 v2, 0x1

    .line 290
    :cond_e
    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 313
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->j()V

    goto :goto_6

    .line 318
    :cond_10
    sget-object v11, Lsoftware/simplicial/a/s;->b:Lsoftware/simplicial/a/s;

    if-eq v5, v11, :cond_11

    sget-object v11, Lsoftware/simplicial/a/s;->c:Lsoftware/simplicial/a/s;

    if-ne v5, v11, :cond_14

    :cond_11
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    float-to-int v14, v12

    float-to-int v15, v13

    .line 319
    invoke-virtual {v11, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    move-result v11

    if-eqz v11, :cond_14

    if-eqz v8, :cond_14

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->D:I

    const/4 v14, -0x1

    if-ne v11, v14, :cond_14

    .line 321
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 322
    move-object/from16 v0, p0

    iget-wide v0, v0, Lsoftware/simplicial/nebulous/c/d;->u:J

    move-wide/from16 v16, v0

    sub-long v16, v14, v16

    .line 323
    move-object/from16 v0, p0

    iput-wide v14, v0, Lsoftware/simplicial/nebulous/c/d;->u:J

    .line 324
    new-instance v2, Lsoftware/simplicial/nebulous/c/c;

    new-instance v11, Landroid/graphics/PointF;

    invoke-direct {v11, v12, v13}, Landroid/graphics/PointF;-><init>(FF)V

    move-wide/from16 v0, v16

    invoke-direct {v2, v0, v1, v11}, Lsoftware/simplicial/nebulous/c/c;-><init>(JLandroid/graphics/PointF;)V

    .line 325
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/nebulous/c/d;->a(Lsoftware/simplicial/nebulous/c/c;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    if-nez v2, :cond_12

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->j()V

    .line 332
    :goto_8
    move-object/from16 v0, p0

    iput v6, v0, Lsoftware/simplicial/nebulous/c/d;->D:I

    .line 333
    const/4 v2, 0x1

    .line 334
    goto :goto_7

    .line 330
    :cond_12
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    if-nez v2, :cond_13

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    goto :goto_8

    :cond_13
    const/4 v2, 0x0

    goto :goto_9

    .line 335
    :cond_14
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    float-to-int v14, v12

    float-to-int v15, v13

    invoke-virtual {v11, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    move-result v11

    if-eqz v11, :cond_15

    if-eqz v7, :cond_15

    .line 337
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->h()V

    .line 338
    move-object/from16 v0, p0

    iput v6, v0, Lsoftware/simplicial/nebulous/c/d;->E:I

    .line 339
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 341
    :cond_15
    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->q:Landroid/graphics/Rect;

    float-to-int v14, v12

    float-to-int v15, v13

    invoke-virtual {v11, v14, v15}, Landroid/graphics/Rect;->contains(II)Z

    move-result v11

    if-eqz v11, :cond_18

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v14, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-eq v11, v14, :cond_16

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v14, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-ne v11, v14, :cond_18

    .line 343
    :cond_16
    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->an:Z

    if-nez v2, :cond_17

    const/4 v2, 0x1

    :goto_a
    iput-boolean v2, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->an:Z

    .line 344
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 343
    :cond_17
    const/4 v2, 0x0

    goto :goto_a

    .line 346
    :cond_18
    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    const/4 v14, -0x1

    if-ne v11, v14, :cond_e

    .line 348
    move-object/from16 v0, p0

    iput v12, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    .line 349
    move-object/from16 v0, p0

    iput v13, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    .line 350
    move-object/from16 v0, p0

    iput v6, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    .line 351
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 354
    :cond_19
    const/4 v14, 0x2

    if-ne v10, v14, :cond_1a

    .line 356
    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    if-ne v6, v11, :cond_e

    .line 358
    move-object/from16 v0, p0

    iput v12, v0, Lsoftware/simplicial/nebulous/c/d;->z:F

    .line 359
    move-object/from16 v0, p0

    iput v13, v0, Lsoftware/simplicial/nebulous/c/d;->A:F

    .line 360
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 363
    :cond_1a
    const/4 v12, 0x1

    if-ne v10, v12, :cond_1b

    if-eqz v4, :cond_1d

    :cond_1b
    const/4 v12, 0x6

    if-ne v10, v12, :cond_1c

    if-eq v4, v11, :cond_1d

    :cond_1c
    const/4 v11, 0x3

    if-ne v10, v11, :cond_e

    .line 367
    :cond_1d
    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->D:I

    if-ne v6, v11, :cond_1e

    .line 369
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->k()V

    .line 370
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/c/d;->D:I

    .line 371
    const/4 v2, 0x1

    .line 373
    :cond_1e
    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->E:I

    if-ne v6, v11, :cond_1f

    .line 375
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/c/d;->E:I

    .line 376
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 378
    :cond_1f
    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    if-ne v6, v11, :cond_e

    .line 380
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/c/d;->F:I

    .line 381
    const/4 v2, 0x1

    goto/16 :goto_7

    .line 409
    :cond_20
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lsoftware/simplicial/nebulous/c/d;->i:Z

    .line 411
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v4, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;

    if-eq v3, v4, :cond_0

    .line 413
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/c/d;->l:F

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/c/d;->j:F

    .line 414
    move-object/from16 v0, p0

    iget v3, v0, Lsoftware/simplicial/nebulous/c/d;->m:F

    move-object/from16 v0, p0

    iput v3, v0, Lsoftware/simplicial/nebulous/c/d;->k:F

    .line 416
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v4, Lsoftware/simplicial/nebulous/c/b;->e:Lsoftware/simplicial/nebulous/c/b;

    if-eq v3, v4, :cond_21

    .line 418
    if-eqz v9, :cond_21

    .line 420
    invoke-virtual {v9}, Lsoftware/simplicial/a/u;->e()Lsoftware/simplicial/a/bf;

    move-result-object v3

    .line 421
    if-eqz v3, :cond_21

    .line 423
    const/4 v4, 0x0

    iput v4, v3, Lsoftware/simplicial/a/bf;->O:F

    .line 427
    :cond_21
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v4, Lsoftware/simplicial/nebulous/c/b;->b:Lsoftware/simplicial/nebulous/c/b;

    if-ne v3, v4, :cond_0

    .line 428
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lsoftware/simplicial/nebulous/c/d;->h:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_22
    move-object/from16 v18, v3

    move v3, v5

    move v5, v4

    move-object/from16 v4, v18

    goto/16 :goto_2

    :cond_23
    move v6, v2

    move v7, v4

    move v8, v5

    move-object v5, v3

    goto/16 :goto_1
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 164
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    monitor-exit p0

    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(I)V
    .locals 2

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 143
    :goto_0
    monitor-exit p0

    return-void

    .line 140
    :cond_0
    const/high16 v0, 0x428c0000    # 70.0f

    add-int/lit8 v1, p1, -0x28

    int-to-float v1, v1

    add-float/2addr v0, v1

    :try_start_1
    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->c:F

    .line 141
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->d:F

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->y:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0, v1}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->e:F

    .line 142
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/c/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 717
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getSource()I

    move-result v1

    and-int/lit16 v1, v1, 0x401

    const/16 v2, 0x401

    if-ne v1, v2, :cond_0

    .line 719
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 721
    packed-switch p1, :pswitch_data_0

    .line 730
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 724
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/c/d;->a(ZZ)Z

    move-result v0

    goto :goto_0

    .line 726
    :pswitch_1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/c/d;->a(ZZ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 721
    :pswitch_data_0
    .packed-switch 0x60
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized c()V
    .locals 14

    .prologue
    const v3, 0x3ba3d70a    # 0.005f

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    .line 559
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->G:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 677
    :goto_0
    monitor-exit p0

    return-void

    .line 562
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->F:I

    .line 563
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->D:I

    .line 564
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->E:I

    .line 565
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->k()V

    .line 566
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, -0x40800000    # -1.0f

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/c/d;->c(FF)V

    .line 567
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->i:Z

    .line 568
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->l:F

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->j:F

    .line 569
    iget v0, p0, Lsoftware/simplicial/nebulous/c/d;->m:F

    iput v0, p0, Lsoftware/simplicial/nebulous/c/d;->k:F

    .line 570
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->x:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->C()Lsoftware/simplicial/a/bf;

    move-result-object v0

    .line 571
    if-eqz v0, :cond_1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v2, Lsoftware/simplicial/nebulous/c/b;->e:Lsoftware/simplicial/nebulous/c/b;

    if-eq v1, v2, :cond_1

    .line 572
    const/4 v1, 0x0

    iput v1, v0, Lsoftware/simplicial/a/bf;->O:F

    .line 573
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v1, Lsoftware/simplicial/nebulous/c/b;->b:Lsoftware/simplicial/nebulous/c/b;

    if-ne v0, v1, :cond_2

    .line 574
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->h:Z

    .line 577
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->A:I

    const/16 v1, 0x32

    if-lt v0, v1, :cond_3

    .line 578
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->A:I

    int-to-float v0, v0

    const/high16 v1, 0x420c0000    # 35.0f

    sub-float/2addr v0, v1

    const/high16 v1, 0x42480000    # 50.0f

    div-float/2addr v0, v1

    const v1, 0x3e2aaaab

    const v2, 0x3eaaaaab

    invoke-static {v0, v1, v2}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v0

    .line 582
    :goto_1
    iget v1, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 583
    mul-float/2addr v0, v1

    .line 584
    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->B:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    .line 585
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/ag;->B:I

    add-int/lit8 v2, v2, 0x32

    int-to-float v2, v2

    mul-float/2addr v2, v3

    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    .line 586
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v1, v3

    .line 587
    sget-object v4, Lsoftware/simplicial/nebulous/c/d$1;->b:[I

    iget-object v5, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/c/a;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 674
    :goto_2
    const/high16 v0, 0x42700000    # 60.0f

    iget-object v1, p0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 675
    const/high16 v1, 0x42700000    # 60.0f

    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->v:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x43200000    # 160.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 676
    iget-object v2, p0, Lsoftware/simplicial/nebulous/c/d;->q:Landroid/graphics/Rect;

    iget v3, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    int-to-float v0, v0

    sub-float v0, v3, v0

    float-to-int v0, v0

    const/4 v3, 0x0

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    float-to-int v4, v4

    invoke-virtual {v2, v0, v3, v4, v1}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 559
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 580
    :cond_3
    :try_start_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/c/d;->w:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->A:I

    int-to-float v0, v0

    const/high16 v1, 0x41700000    # 15.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x42480000    # 50.0f

    div-float/2addr v0, v1

    const v1, 0x3d088889

    const v2, 0x3e2aaaab

    invoke-static {v0, v1, v2}, Lb/a/a/d/a/a;->a(FFF)F

    move-result v0

    goto :goto_1

    .line 590
    :pswitch_0
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    float-to-int v5, v1

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v7, v1, v0

    sub-float/2addr v6, v7

    float-to-int v6, v6

    add-float v7, v1, v0

    float-to-int v7, v7

    iget v8, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v8, v1

    float-to-int v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 595
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    add-float v5, v1, v2

    add-float/2addr v5, v0

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v7, v1, v0

    sub-float/2addr v6, v7

    add-float/2addr v6, v3

    float-to-int v6, v6

    add-float v7, v1, v0

    add-float/2addr v7, v2

    add-float/2addr v7, v0

    float-to-int v7, v7

    iget v8, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v8, v1

    add-float/2addr v8, v3

    float-to-int v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 600
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    add-float v5, v1, v2

    add-float/2addr v5, v0

    add-float/2addr v5, v2

    add-float/2addr v5, v0

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    float-to-double v6, v6

    float-to-double v8, v1

    float-to-double v10, v0

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    sub-double/2addr v6, v8

    float-to-double v8, v3

    add-double/2addr v6, v8

    double-to-int v6, v6

    add-float v7, v1, v0

    add-float/2addr v7, v2

    add-float/2addr v7, v0

    add-float/2addr v2, v7

    float-to-double v8, v2

    float-to-double v10, v0

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    double-to-int v0, v8

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float v1, v2, v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    .line 607
    :pswitch_1
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v6, v1, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v7, v1, v0

    sub-float/2addr v6, v7

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    sub-float/2addr v7, v1

    float-to-int v7, v7

    iget v8, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v8, v1

    float-to-int v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 611
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v6, v1, v0

    add-float/2addr v6, v2

    add-float/2addr v6, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v7, v1, v0

    sub-float/2addr v6, v7

    add-float/2addr v6, v3

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v8, v1, v2

    add-float/2addr v8, v0

    sub-float/2addr v7, v8

    float-to-int v7, v7

    iget v8, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v8, v1

    add-float/2addr v8, v3

    float-to-int v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 616
    iget-object v4, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    float-to-double v6, v5

    add-float v5, v1, v0

    add-float/2addr v5, v2

    add-float/2addr v5, v0

    add-float/2addr v5, v2

    float-to-double v8, v5

    float-to-double v10, v0

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    sub-double/2addr v6, v8

    double-to-int v5, v6

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    float-to-double v6, v6

    float-to-double v8, v1

    float-to-double v10, v0

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    sub-double/2addr v6, v8

    float-to-double v8, v3

    add-double/2addr v6, v8

    double-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v8, v1, v2

    add-float/2addr v8, v0

    add-float/2addr v2, v8

    add-float/2addr v0, v2

    sub-float v0, v7, v0

    float-to-int v0, v0

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float v1, v2, v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    .line 623
    :pswitch_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    float-to-int v4, v1

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v6, v1, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    add-float v6, v1, v0

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 628
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v5, v1, v0

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v6, v1, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    sub-float/2addr v6, v1

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 633
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    float-to-double v4, v4

    add-float v6, v1, v0

    add-float/2addr v6, v2

    float-to-double v6, v6

    float-to-double v8, v0

    mul-double/2addr v8, v12

    add-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    float-to-double v6, v5

    float-to-double v8, v1

    float-to-double v10, v0

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    sub-double/2addr v6, v8

    double-to-int v5, v6

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float/2addr v2, v1

    add-float/2addr v0, v2

    sub-float v0, v6, v0

    float-to-int v0, v0

    iget v2, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float v1, v2, v1

    float-to-int v1, v1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    .line 640
    :pswitch_3
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    float-to-int v4, v1

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v6, v1, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    add-float v6, v1, v0

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 645
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    float-to-int v4, v1

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v5, v1

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    sub-float/2addr v5, v0

    float-to-int v5, v5

    add-float v6, v1, v0

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    sub-float/2addr v7, v0

    sub-float/2addr v7, v2

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 650
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    float-to-int v4, v1

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v5, v1

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    float-to-double v6, v5

    float-to-double v8, v0

    mul-double/2addr v8, v12

    sub-double/2addr v6, v8

    double-to-int v5, v6

    float-to-double v6, v1

    float-to-double v8, v0

    mul-double/2addr v8, v12

    add-double/2addr v6, v8

    double-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float v1, v7, v1

    sub-float/2addr v1, v0

    sub-float/2addr v1, v2

    sub-float v0, v1, v0

    sub-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    .line 657
    :pswitch_4
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->p:Landroid/graphics/Rect;

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v5, v1, v0

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    add-float v6, v1, v0

    sub-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    sub-float/2addr v6, v1

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 662
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->n:Landroid/graphics/Rect;

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    add-float v5, v1, v0

    sub-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v5, v1

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    sub-float/2addr v5, v0

    float-to-int v5, v5

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    sub-float/2addr v6, v1

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v7, v1

    sub-float/2addr v7, v0

    sub-float/2addr v7, v2

    float-to-int v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 667
    iget-object v3, p0, Lsoftware/simplicial/nebulous/c/d;->o:Landroid/graphics/Rect;

    iget v4, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    float-to-double v4, v4

    float-to-double v6, v1

    float-to-double v8, v0

    mul-double/2addr v8, v12

    add-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float/2addr v5, v1

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    sub-float/2addr v5, v0

    sub-float/2addr v5, v2

    float-to-double v6, v5

    float-to-double v8, v0

    mul-double/2addr v8, v12

    sub-double/2addr v6, v8

    double-to-int v5, v6

    iget v6, p0, Lsoftware/simplicial/nebulous/c/d;->f:F

    sub-float/2addr v6, v1

    float-to-int v6, v6

    iget v7, p0, Lsoftware/simplicial/nebulous/c/d;->g:F

    sub-float v1, v7, v1

    sub-float/2addr v1, v0

    sub-float/2addr v1, v2

    sub-float v0, v1, v0

    sub-float/2addr v0, v2

    float-to-int v0, v0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 587
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 809
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/c/d;->H:Z

    return v0
.end method
