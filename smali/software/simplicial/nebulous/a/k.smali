.class public Lsoftware/simplicial/nebulous/a/k;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/n;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private b:Lsoftware/simplicial/nebulous/f/ad;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/ad;)V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f040069

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 26
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/k;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 27
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/k;->b:Lsoftware/simplicial/nebulous/f/ad;

    .line 28
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/k;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/k;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/k;)Lsoftware/simplicial/nebulous/f/ad;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/k;->b:Lsoftware/simplicial/nebulous/f/ad;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 33
    .line 34
    if-nez p2, :cond_0

    .line 35
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/k;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04006a

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 38
    :cond_0
    const v0, 0x7f0d009c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 39
    const v1, 0x7f0d02ca

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 40
    const v2, 0x7f0d02c2

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 42
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 44
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/k;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/n;

    .line 46
    const-string v3, "%s (%d) -> %s (%d)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lsoftware/simplicial/nebulous/f/n;->b:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x1

    iget v6, v1, Lsoftware/simplicial/nebulous/f/n;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, v1, Lsoftware/simplicial/nebulous/f/n;->d:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, v1, Lsoftware/simplicial/nebulous/f/n;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    new-instance v0, Lsoftware/simplicial/nebulous/a/k$1;

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/a/k$1;-><init>(Lsoftware/simplicial/nebulous/a/k;Lsoftware/simplicial/nebulous/f/n;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-object p2
.end method
