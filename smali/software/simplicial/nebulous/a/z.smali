.class public Lsoftware/simplicial/nebulous/a/z;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bm;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f040079

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 31
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/z;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 32
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/z;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/z;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8

    .line 37
    .line 38
    if-nez p2, :cond_0

    .line 39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/z;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040079

    invoke-virtual {v0, v1, p3, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 41
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/z;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 43
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 44
    const v2, 0x7f0d0085

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 45
    const v3, 0x7f0d02bf

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 46
    const v4, 0x7f0d02f5

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 47
    const v5, 0x7f0d02f4

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 48
    const v6, 0x7f0d02f6

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 49
    iget-object v7, v0, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    .line 50
    iget v8, v0, Lsoftware/simplicial/a/bm;->g:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_1

    .line 51
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " -> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lsoftware/simplicial/a/bm;->g:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 52
    :cond_1
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v1, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v1, v0, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    if-eqz v1, :cond_2

    .line 57
    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v1, v2, v7}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    .line 58
    iget-object v2, v0, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_2
    iget-boolean v1, v0, Lsoftware/simplicial/a/bm;->e:Z

    if-eqz v1, :cond_3

    .line 63
    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 64
    invoke-virtual {v4, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 65
    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    :goto_0
    new-instance v1, Lsoftware/simplicial/nebulous/a/z$1;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/z$1;-><init>(Lsoftware/simplicial/nebulous/a/z;Lsoftware/simplicial/a/bm;)V

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    new-instance v1, Lsoftware/simplicial/nebulous/a/z$2;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/z$2;-><init>(Lsoftware/simplicial/nebulous/a/z;Lsoftware/simplicial/a/bm;)V

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-object p2

    .line 69
    :cond_3
    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 70
    invoke-virtual {v4, v11}, Landroid/widget/Button;->setVisibility(I)V

    .line 71
    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
