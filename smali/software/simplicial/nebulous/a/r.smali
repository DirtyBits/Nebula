.class public Lsoftware/simplicial/nebulous/a/r;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Landroid/graphics/BitmapFactory$Options;

.field private final b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 20
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    .line 24
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 30
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/r;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x1e

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 45
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 51
    .line 53
    if-nez p2, :cond_0

    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04007d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 56
    :cond_0
    const v0, 0x7f0d02fe

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 57
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 58
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "emote_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/r;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/r;->a:Landroid/graphics/BitmapFactory$Options;

    .line 57
    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 59
    new-instance v1, Lsoftware/simplicial/nebulous/a/r$1;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/a/r$1;-><init>(Lsoftware/simplicial/nebulous/a/r;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-object p2
.end method
