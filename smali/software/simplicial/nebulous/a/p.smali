.class public Lsoftware/simplicial/nebulous/a/p;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/c/c;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Z

.field private final b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f04006f

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 28
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/p;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 29
    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/a/p;->a:Z

    .line 30
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/p;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/p;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 35
    .line 36
    if-nez p2, :cond_0

    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/p;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04006f

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 39
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/p;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/c;

    .line 41
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 43
    const v2, 0x7f0d02c9

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 44
    const v3, 0x7f0d02c2

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 46
    iget-object v4, v0, Lsoftware/simplicial/a/c/c;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lsoftware/simplicial/a/c/c;->d:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-boolean v4, v0, Lsoftware/simplicial/a/c/c;->c:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/p;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 49
    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/a/p;->a:Z

    if-eqz v1, :cond_4

    .line 51
    iget-object v1, v0, Lsoftware/simplicial/a/c/c;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 53
    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 54
    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 56
    invoke-virtual {v2, v8}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    new-instance v1, Lsoftware/simplicial/nebulous/a/p$1;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/p$1;-><init>(Lsoftware/simplicial/nebulous/a/p;Lsoftware/simplicial/a/c/c;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :goto_2
    return-object p2

    .line 46
    :cond_1
    const-string v4, ""

    goto :goto_0

    .line 47
    :cond_2
    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/p;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c007a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    goto :goto_1

    .line 68
    :cond_3
    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 69
    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 71
    new-instance v0, Lsoftware/simplicial/nebulous/a/p$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/a/p$2;-><init>(Lsoftware/simplicial/nebulous/a/p;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 85
    :cond_4
    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 86
    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_2
.end method
