.class public Lsoftware/simplicial/nebulous/a/u;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/y;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private b:Z


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f040073

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/a/u;->b:Z

    .line 40
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 41
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/u;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/u;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/a/u;->b:Z

    return v0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 164
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/a/u;->b:Z

    .line 165
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17

    .prologue
    .line 46
    .line 47
    if-nez p2, :cond_0

    .line 48
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f040073

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 50
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lsoftware/simplicial/nebulous/a/u;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/nebulous/f/y;

    .line 52
    const v3, 0x7f0d02ee

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 53
    const v4, 0x7f0d0252

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 54
    const v5, 0x7f0d02ef

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 55
    const v6, 0x7f0d02c2

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 57
    const-string v9, ""

    .line 59
    iget v7, v2, Lsoftware/simplicial/nebulous/f/y;->b:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v8

    if-ne v7, v8, :cond_4

    iget v7, v2, Lsoftware/simplicial/nebulous/f/y;->d:I

    .line 60
    :goto_0
    iget v8, v2, Lsoftware/simplicial/nebulous/f/y;->b:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v10}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v10

    if-ne v8, v10, :cond_5

    iget-object v8, v2, Lsoftware/simplicial/nebulous/f/y;->e:Ljava/lang/String;

    .line 61
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 62
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v11, 0x7f080189

    invoke-virtual {v10, v11}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 65
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v10}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c0084

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 66
    const/4 v11, 0x0

    .line 68
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v12

    .line 69
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, " "

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 70
    new-instance v9, Landroid/text/SpannableString;

    invoke-direct {v9, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v13, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v14, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x12

    move/from16 v0, v16

    invoke-virtual {v9, v13, v14, v15, v0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 72
    iget-boolean v13, v2, Lsoftware/simplicial/nebulous/f/y;->g:Z

    if-eqz v13, :cond_1

    .line 73
    new-instance v13, Landroid/text/style/StyleSpan;

    const/4 v14, 0x1

    invoke-direct {v13, v14}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v14, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v15, 0x0

    invoke-virtual {v9, v13, v14, v8, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 74
    :cond_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-static {v10, v7}, Lsoftware/simplicial/nebulous/f/aa;->a(IZ)I

    move-result v7

    .line 75
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v7, 0x12

    invoke-virtual {v9, v8, v11, v12, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 76
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    new-instance v3, Landroid/text/SpannableString;

    iget-object v7, v2, Lsoftware/simplicial/nebulous/f/y;->f:Ljava/lang/String;

    invoke-direct {v3, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 79
    iget-boolean v7, v2, Lsoftware/simplicial/nebulous/f/y;->g:Z

    if-eqz v7, :cond_2

    .line 80
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v8, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v3, v7, v8, v9, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 81
    :cond_2
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 83
    sget-object v4, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, v2, Lsoftware/simplicial/nebulous/f/y;->i:Ljava/util/Date;

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v8, v9, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 84
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v10, 0x7f0800e4

    invoke-virtual {v7, v10}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 85
    iget-boolean v4, v2, Lsoftware/simplicial/nebulous/f/y;->g:Z

    if-eqz v4, :cond_3

    .line 86
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v7, 0x1

    invoke-direct {v4, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v7, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v3, v4, v7, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 87
    :cond_3
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    const-wide/16 v10, 0x5

    cmp-long v3, v8, v10

    if-gez v3, :cond_7

    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    :goto_3
    new-instance v3, Lsoftware/simplicial/nebulous/a/u$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lsoftware/simplicial/nebulous/a/u$1;-><init>(Lsoftware/simplicial/nebulous/a/u;Lsoftware/simplicial/nebulous/f/y;)V

    invoke-virtual {v6, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    new-instance v3, Lsoftware/simplicial/nebulous/a/u$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lsoftware/simplicial/nebulous/a/u$2;-><init>(Lsoftware/simplicial/nebulous/a/u;)V

    invoke-virtual {v6, v3}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 149
    new-instance v3, Lsoftware/simplicial/nebulous/a/u$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lsoftware/simplicial/nebulous/a/u$3;-><init>(Lsoftware/simplicial/nebulous/a/u;Lsoftware/simplicial/nebulous/f/y;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    return-object p2

    .line 59
    :cond_4
    iget v7, v2, Lsoftware/simplicial/nebulous/f/y;->b:I

    goto/16 :goto_0

    .line 60
    :cond_5
    iget-object v8, v2, Lsoftware/simplicial/nebulous/f/y;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 64
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    .line 91
    :cond_7
    const-wide/16 v10, 0x1e

    cmp-long v3, v8, v10

    if-gez v3, :cond_8

    .line 92
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 94
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/u;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c004c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method
