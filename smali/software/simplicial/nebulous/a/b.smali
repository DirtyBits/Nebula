.class public Lsoftware/simplicial/nebulous/a/b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/f;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field b:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 25
    const v0, 0x7f040065

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v1, v1, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/b;->b:Ljava/text/DateFormat;

    .line 26
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/b;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 27
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 32
    .line 33
    if-nez p2, :cond_0

    .line 34
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/b;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040065

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    :cond_0
    const v0, 0x7f0d02c7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/b;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/nebulous/f/f;

    .line 40
    iget-object v2, v1, Lsoftware/simplicial/nebulous/f/f;->d:Ljava/util/Date;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/b;->b:Ljava/text/DateFormat;

    iget-object v3, v1, Lsoftware/simplicial/nebulous/f/f;->d:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 42
    :goto_0
    const-string v3, "%s vs. %s\n%s - %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lsoftware/simplicial/nebulous/f/f;->a:Ljava/lang/String;

    aput-object v5, v4, v6

    const/4 v5, 0x1

    iget-object v6, v1, Lsoftware/simplicial/nebulous/f/f;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, v1, Lsoftware/simplicial/nebulous/f/f;->c:Lsoftware/simplicial/a/b/f;

    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/b;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 43
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v6, v7}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/b/f;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v2, v4, v5

    .line 42
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 44
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    sget-object v2, Lsoftware/simplicial/nebulous/a/b$1;->a:[I

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/f;->c:Lsoftware/simplicial/a/b/f;

    invoke-virtual {v1}, Lsoftware/simplicial/a/b/f;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 59
    :goto_1
    return-object p2

    .line 40
    :cond_1
    const-string v2, "???"

    goto :goto_0

    .line 49
    :pswitch_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/b;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 52
    :pswitch_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/b;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 55
    :pswitch_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/b;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
