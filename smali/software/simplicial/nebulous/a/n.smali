.class public Lsoftware/simplicial/nebulous/a/n;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    const v0, 0x1090008

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 30
    check-cast p1, Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 31
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/n;->a:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/a/n;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 43
    const/4 v0, 0x0

    .line 44
    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04006d

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    .line 47
    :goto_0
    const v0, 0x7f0d02e8

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 48
    const v1, 0x7f0d02e9

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 50
    sget-object v2, Lsoftware/simplicial/nebulous/f/al;->b:Lb/a/a/c/a;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->a:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/q;

    .line 52
    sget-object v5, Lsoftware/simplicial/nebulous/a/n$1;->a:[I

    invoke-virtual {v2}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v2

    aget v2, v5, v2

    packed-switch v2, :pswitch_data_0

    move v1, v4

    .line 78
    :goto_1
    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 87
    :goto_2
    return-object v3

    .line 55
    :pswitch_0
    const v2, 0x7f02019b

    .line 56
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f08016b

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 57
    goto :goto_1

    .line 59
    :pswitch_1
    const v2, 0x7f0202ff

    .line 60
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080072

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 61
    goto :goto_1

    .line 63
    :pswitch_2
    const v2, 0x7f020075

    .line 64
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080002

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 65
    goto :goto_1

    .line 67
    :pswitch_3
    const v2, 0x7f020407

    .line 68
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0800fe

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 69
    goto :goto_1

    .line 71
    :pswitch_4
    const v2, 0x7f02019e

    .line 72
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/n;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080188

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 73
    goto :goto_1

    .line 84
    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_1
    move-object v3, v0

    goto/16 :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
