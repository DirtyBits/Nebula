.class public final enum Lsoftware/simplicial/nebulous/a/s$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/a/s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/a/s$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/a/s$a;

.field public static final enum b:Lsoftware/simplicial/nebulous/a/s$a;

.field public static final enum c:Lsoftware/simplicial/nebulous/a/s$a;

.field private static final synthetic d:[Lsoftware/simplicial/nebulous/a/s$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 714
    new-instance v0, Lsoftware/simplicial/nebulous/a/s$a;

    const-string v1, "MANAGING"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/s$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/a/s$a;->a:Lsoftware/simplicial/nebulous/a/s$a;

    new-instance v0, Lsoftware/simplicial/nebulous/a/s$a;

    const-string v1, "SELECTING"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/a/s$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/a/s$a;->b:Lsoftware/simplicial/nebulous/a/s$a;

    new-instance v0, Lsoftware/simplicial/nebulous/a/s$a;

    const-string v1, "SELECTING_MOD"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/a/s$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/a/s$a;->c:Lsoftware/simplicial/nebulous/a/s$a;

    .line 712
    const/4 v0, 0x3

    new-array v0, v0, [Lsoftware/simplicial/nebulous/a/s$a;

    sget-object v1, Lsoftware/simplicial/nebulous/a/s$a;->a:Lsoftware/simplicial/nebulous/a/s$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/a/s$a;->b:Lsoftware/simplicial/nebulous/a/s$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/a/s$a;->c:Lsoftware/simplicial/nebulous/a/s$a;

    aput-object v1, v0, v4

    sput-object v0, Lsoftware/simplicial/nebulous/a/s$a;->d:[Lsoftware/simplicial/nebulous/a/s$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 712
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/a/s$a;
    .locals 1

    .prologue
    .line 712
    const-class v0, Lsoftware/simplicial/nebulous/a/s$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/a/s$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/a/s$a;
    .locals 1

    .prologue
    .line 712
    sget-object v0, Lsoftware/simplicial/nebulous/a/s$a;->d:[Lsoftware/simplicial/nebulous/a/s$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/a/s$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/a/s$a;

    return-object v0
.end method
