.class public Lsoftware/simplicial/nebulous/a/ad;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f040077

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 32
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/ad;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 33
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/ad;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ad;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 38
    .line 39
    if-nez p2, :cond_0

    .line 40
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ad;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040077

    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 42
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/ad;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 44
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 45
    const v2, 0x7f0d02f3

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    const v3, 0x7f0d0085

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 47
    const v4, 0x7f0d009d

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 48
    const v5, 0x7f0d009f

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 49
    const v6, 0x7f0d02c2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 51
    iget-object v7, v0, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 54
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/ad;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v7, 0x7f0c0039

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 56
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 57
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-static {v1, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 64
    :goto_0
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    new-instance v1, Lsoftware/simplicial/nebulous/a/ad$1;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/ad$1;-><init>(Lsoftware/simplicial/nebulous/a/ad;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v6, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    new-instance v1, Lsoftware/simplicial/nebulous/a/ad$2;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/ad$2;-><init>(Lsoftware/simplicial/nebulous/a/ad;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-object p2

    .line 61
    :cond_1
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    sget-object v1, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v1, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_0
.end method
