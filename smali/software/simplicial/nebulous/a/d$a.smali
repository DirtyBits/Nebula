.class Lsoftware/simplicial/nebulous/a/d$a;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lsoftware/simplicial/nebulous/a/d$b;",
        "Ljava/lang/Void;",
        "Lsoftware/simplicial/nebulous/a/d$b;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/d;


# direct methods
.method private constructor <init>(Lsoftware/simplicial/nebulous/a/d;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/a/d$a;-><init>(Lsoftware/simplicial/nebulous/a/d;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Lsoftware/simplicial/nebulous/a/d$b;)Lsoftware/simplicial/nebulous/a/d$b;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 268
    aget-object v0, p1, v3

    .line 270
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 271
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 272
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 273
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 274
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 276
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->a(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$b;->b:Landroid/view/View;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$b;->a:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 281
    :goto_0
    return-object v0

    .line 279
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$b;->a:Lsoftware/simplicial/a/e;

    invoke-virtual {v4}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v6

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/a/d$b;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected a(Lsoftware/simplicial/nebulous/a/d$b;)V
    .locals 23

    .prologue
    .line 287
    invoke-super/range {p0 .. p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 289
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/d$b;->c:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$b;->b:Landroid/view/View;

    .line 293
    move-object/from16 v0, p1

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/d$b;->a:Lsoftware/simplicial/a/e;

    .line 295
    const v3, 0x7f0d02f8

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v18, v3

    check-cast v18, Landroid/widget/ImageView;

    .line 296
    const v3, 0x7f0d02fa

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v19, v3

    check-cast v19, Landroid/widget/TextView;

    .line 297
    const v3, 0x7f0d02fc

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v20, v3

    check-cast v20, Landroid/widget/TextView;

    .line 298
    const v3, 0x7f0d02fb

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v21, v3

    check-cast v21, Landroid/widget/LinearLayout;

    .line 299
    const v3, 0x7f0d02f9

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v22, v3

    check-cast v22, Landroid/widget/ImageView;

    .line 301
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->a(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v2, :cond_0

    .line 304
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$b;->c:Landroid/graphics/Bitmap;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->c(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->d(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v5}, Lsoftware/simplicial/nebulous/a/d;->e(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/a/d;->f(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    .line 307
    invoke-static {v7}, Lsoftware/simplicial/nebulous/a/d;->g(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v8}, Lsoftware/simplicial/nebulous/a/d;->h(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v9}, Lsoftware/simplicial/nebulous/a/d;->i(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v10}, Lsoftware/simplicial/nebulous/a/d;->j(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v11}, Lsoftware/simplicial/nebulous/a/d;->k(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v12}, Lsoftware/simplicial/nebulous/a/d;->l(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v13}, Lsoftware/simplicial/nebulous/a/d;->m(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v14}, Lsoftware/simplicial/nebulous/a/d;->n(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    .line 308
    invoke-static {v15}, Lsoftware/simplicial/nebulous/a/d;->o(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lsoftware/simplicial/nebulous/a/d;->p(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lsoftware/simplicial/nebulous/a/d;->q(Lsoftware/simplicial/nebulous/a/d;)I

    move-result v17

    .line 306
    invoke-static/range {v2 .. v17}, Lsoftware/simplicial/a/e;->a(Lsoftware/simplicial/a/e;Ljava/util/Set;Ljava/util/Set;IIIIIIIIIIIII)Z

    move-result v3

    .line 309
    if-eqz v3, :cond_2

    .line 310
    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 314
    :goto_1
    iget-object v4, v2, Lsoftware/simplicial/a/e;->lt:Lsoftware/simplicial/a/d;

    if-eqz v4, :cond_4

    .line 316
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 317
    if-eqz v3, :cond_3

    .line 318
    const v4, 0x3f2aaaab

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 326
    :goto_2
    iget v4, v2, Lsoftware/simplicial/a/e;->lm:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_6

    .line 328
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    const/16 v4, 0x55

    const/16 v5, 0xaa

    const/16 v6, 0xff

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 330
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lm:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    if-eqz v3, :cond_5

    .line 332
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 459
    :goto_3
    iget v4, v2, Lsoftware/simplicial/a/e;->lG:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_21

    .line 461
    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 462
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c002f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 463
    iget v4, v2, Lsoftware/simplicial/a/e;->lG:I

    if-ltz v4, :cond_1f

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    iget v2, v2, Lsoftware/simplicial/a/e;->lG:I

    int-to-long v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    :goto_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    if-eqz v3, :cond_20

    .line 466
    const v2, 0x3f2aaaab

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 312
    :cond_2
    const/16 v4, 0xdc

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto/16 :goto_1

    .line 320
    :cond_3
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_2

    .line 323
    :cond_4
    const/16 v4, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 334
    :cond_5
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_3

    .line 336
    :cond_6
    iget v4, v2, Lsoftware/simplicial/a/e;->ln:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_8

    .line 338
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c006c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 340
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->ln:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    if-eqz v3, :cond_7

    .line 342
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 344
    :cond_7
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 346
    :cond_8
    iget v4, v2, Lsoftware/simplicial/a/e;->lo:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_a

    .line 348
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 349
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0014

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 350
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lo:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    if-eqz v3, :cond_9

    .line 352
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 354
    :cond_9
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 356
    :cond_a
    iget v4, v2, Lsoftware/simplicial/a/e;->lp:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_c

    .line 358
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 360
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    if-eqz v3, :cond_b

    .line 362
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 364
    :cond_b
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 366
    :cond_c
    iget v4, v2, Lsoftware/simplicial/a/e;->lq:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_e

    .line 368
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 369
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c004a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 370
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lq:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    if-eqz v3, :cond_d

    .line 372
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 374
    :cond_d
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 376
    :cond_e
    iget v4, v2, Lsoftware/simplicial/a/e;->lr:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_10

    .line 378
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 379
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0079

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 380
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lr:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    if-eqz v3, :cond_f

    .line 382
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 384
    :cond_f
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 386
    :cond_10
    iget v4, v2, Lsoftware/simplicial/a/e;->ls:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_12

    .line 388
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 389
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 390
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->ls:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    if-eqz v3, :cond_11

    .line 392
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 394
    :cond_11
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 396
    :cond_12
    iget v4, v2, Lsoftware/simplicial/a/e;->ly:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_14

    .line 398
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 399
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c001d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 400
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->ly:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    if-eqz v3, :cond_13

    .line 402
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 404
    :cond_13
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 406
    :cond_14
    iget v4, v2, Lsoftware/simplicial/a/e;->lz:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_16

    .line 408
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 409
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0080

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 410
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lz:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    if-eqz v3, :cond_15

    .line 412
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 414
    :cond_15
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 416
    :cond_16
    iget v4, v2, Lsoftware/simplicial/a/e;->lA:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_18

    .line 418
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 420
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lA:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    if-eqz v3, :cond_17

    .line 422
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 424
    :cond_17
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 426
    :cond_18
    iget v4, v2, Lsoftware/simplicial/a/e;->lB:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1a

    .line 428
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 429
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0093

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 430
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lB:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    if-eqz v3, :cond_19

    .line 432
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 434
    :cond_19
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 436
    :cond_1a
    iget v4, v2, Lsoftware/simplicial/a/e;->lC:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1c

    .line 438
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 439
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 440
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lC:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    if-eqz v3, :cond_1b

    .line 442
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 444
    :cond_1b
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 446
    :cond_1c
    iget v4, v2, Lsoftware/simplicial/a/e;->lD:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1e

    .line 448
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 449
    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$a;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0054

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 450
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lsoftware/simplicial/a/e;->lD:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    if-eqz v3, :cond_1d

    .line 452
    const v4, 0x3f2aaaab

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 454
    :cond_1d
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    goto/16 :goto_3

    .line 457
    :cond_1e
    const/16 v4, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 463
    :cond_1f
    const-string v2, "---"

    goto/16 :goto_4

    .line 468
    :cond_20
    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 472
    :cond_21
    const/16 v2, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 262
    check-cast p1, [Lsoftware/simplicial/nebulous/a/d$b;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$a;->a([Lsoftware/simplicial/nebulous/a/d$b;)Lsoftware/simplicial/nebulous/a/d$b;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 262
    check-cast p1, Lsoftware/simplicial/nebulous/a/d$b;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$a;->a(Lsoftware/simplicial/nebulous/a/d$b;)V

    return-void
.end method
