.class Lsoftware/simplicial/nebulous/a/l$10;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/l;->a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/l;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/l;)V
    .locals 0

    .prologue
    .line 660
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/l$10;->a:Lsoftware/simplicial/nebulous/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 664
    check-cast p1, Lsoftware/simplicial/a/bg;

    .line 665
    check-cast p2, Lsoftware/simplicial/a/bg;

    .line 667
    iget-boolean v0, p1, Lsoftware/simplicial/a/bg;->i:Z

    iget-boolean v1, p2, Lsoftware/simplicial/a/bg;->i:Z

    if-eq v0, v1, :cond_2

    .line 668
    iget-boolean v0, p2, Lsoftware/simplicial/a/bg;->i:Z

    if-eqz v0, :cond_1

    const v0, 0x7fffffff

    .line 677
    :cond_0
    :goto_0
    return v0

    .line 668
    :cond_1
    const/high16 v0, -0x80000000

    goto :goto_0

    .line 670
    :cond_2
    iget-object v0, p2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v0}, Lsoftware/simplicial/a/q;->a()I

    move-result v0

    iget-object v1, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v1}, Lsoftware/simplicial/a/q;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 671
    if-nez v0, :cond_0

    .line 673
    iget-object v0, p2, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bg$b;->a()I

    move-result v0

    iget-object v1, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v1}, Lsoftware/simplicial/a/bg$b;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 674
    if-nez v0, :cond_0

    .line 677
    iget-object v0, p2, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
