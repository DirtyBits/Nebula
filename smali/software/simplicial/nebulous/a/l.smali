.class public Lsoftware/simplicial/nebulous/a/l;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/a/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private final b:Lsoftware/simplicial/nebulous/a/l$a;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/l$a;)V
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f04006b

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 44
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 45
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/l;->b:Lsoftware/simplicial/nebulous/a/l$a;

    .line 46
    return-void
.end method

.method private a(Lsoftware/simplicial/a/aq;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 621
    if-eqz p2, :cond_0

    .line 622
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080149

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 630
    :goto_0
    return-object v0

    .line 623
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/a/l$2;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/aq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 630
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802dc

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 626
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080204

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 628
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801ff

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 623
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method private a(Lsoftware/simplicial/a/bg;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageView;)V
    .locals 9

    .prologue
    .line 370
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    if-ne v2, v3, :cond_4

    sget-object v2, Lsoftware/simplicial/a/ai;->Y:Lsoftware/simplicial/a/ak;

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    if-ne v2, v3, :cond_4

    const/4 v2, 0x1

    .line 372
    :goto_0
    iget-boolean v3, p1, Lsoftware/simplicial/a/bg;->i:Z

    if-eqz v3, :cond_5

    const v3, 0x7f0201a6

    :goto_1
    move-object/from16 v0, p12

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 374
    sget-object v3, Lsoftware/simplicial/nebulous/a/l$2;->a:[I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    invoke-virtual {v4}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 394
    const/16 v3, 0x8

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 395
    const/16 v3, 0x8

    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 396
    const/16 v3, 0x8

    move-object/from16 v0, p11

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 400
    :goto_2
    sget-object v3, Lsoftware/simplicial/nebulous/a/l$2;->a:[I

    iget-object v4, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v4}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 427
    :goto_3
    sget-object v3, Lsoftware/simplicial/nebulous/a/l$2;->b:[I

    iget-object v4, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v4}, Lsoftware/simplicial/a/bg$b;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    .line 489
    const/16 v3, 0x8

    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 490
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 491
    const/16 v3, 0x8

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 495
    :goto_4
    const/4 v3, 0x0

    const/16 v4, 0xff

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 496
    const-string v3, ""

    .line 497
    iget-boolean v4, p1, Lsoftware/simplicial/a/bg;->q:Z

    if-eqz v4, :cond_0

    .line 498
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080186

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 499
    :cond_0
    const/4 v4, 0x0

    .line 500
    sget-object v5, Lsoftware/simplicial/nebulous/a/l$2;->b:[I

    iget-object v6, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    invoke-virtual {v6}, Lsoftware/simplicial/a/bg$b;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_3

    .line 587
    const/16 v5, 0xff

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 588
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802dc

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 591
    :goto_5
    if-eqz v4, :cond_1

    .line 592
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lsoftware/simplicial/a/bg;->k:Lsoftware/simplicial/a/aq;

    iget-boolean v5, p1, Lsoftware/simplicial/a/bg;->o:Z

    invoke-direct {p0, v4, v5}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/a/aq;Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 593
    :cond_1
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08017a

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lsoftware/simplicial/a/bg;->g:J

    invoke-static {v4, v5}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 596
    iget-object v4, p1, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    if-eqz v4, :cond_2

    .line 598
    const/4 v4, 0x3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v4, v5}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v4

    .line 599
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lsoftware/simplicial/a/bg;->p:Ljava/util/Date;

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 601
    :cond_2
    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 603
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v4, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    if-eq v3, v4, :cond_3

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v4, Lsoftware/simplicial/a/bg$b;->f:Lsoftware/simplicial/a/bg$b;

    if-eq v3, v4, :cond_3

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v4, Lsoftware/simplicial/a/bg$b;->a:Lsoftware/simplicial/a/bg$b;

    if-eq v3, v4, :cond_3

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    sget-object v4, Lsoftware/simplicial/a/bg$b;->b:Lsoftware/simplicial/a/bg$b;

    if-ne v3, v4, :cond_1e

    .line 606
    :cond_3
    const/16 v2, 0x8

    invoke-virtual {p5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 617
    :goto_6
    return-void

    .line 370
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 372
    :cond_5
    const v3, 0x7f0201a7

    goto/16 :goto_1

    .line 377
    :pswitch_0
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_6

    const/4 v3, 0x0

    :goto_7
    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 378
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_7

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_7

    const/4 v3, 0x0

    :goto_8
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 379
    const/4 v3, 0x0

    move-object/from16 v0, p11

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 377
    :cond_6
    const/16 v3, 0x8

    goto :goto_7

    .line 378
    :cond_7
    const/16 v3, 0x8

    goto :goto_8

    .line 382
    :pswitch_1
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_8

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_b

    :cond_8
    const/4 v3, 0x0

    :goto_9
    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 383
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_9

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_c

    :cond_9
    const/4 v3, 0x0

    :goto_a
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 384
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_a

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_a

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_d

    :cond_a
    const/4 v3, 0x0

    :goto_b
    move-object/from16 v0, p11

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 382
    :cond_b
    const/16 v3, 0x8

    goto :goto_9

    .line 383
    :cond_c
    const/16 v3, 0x8

    goto :goto_a

    .line 384
    :cond_d
    const/16 v3, 0x8

    goto :goto_b

    .line 387
    :pswitch_2
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_f

    const/4 v3, 0x0

    :goto_c
    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 388
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_10

    const/4 v3, 0x0

    :goto_d
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 389
    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_e

    iget-object v3, p1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_11

    :cond_e
    const/4 v3, 0x0

    :goto_e
    move-object/from16 v0, p11

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 387
    :cond_f
    const/16 v3, 0x8

    goto :goto_c

    .line 388
    :cond_10
    const/16 v3, 0x8

    goto :goto_d

    .line 389
    :cond_11
    const/16 v3, 0x8

    goto :goto_e

    .line 403
    :pswitch_3
    const v3, 0x7f02019b

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 404
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 407
    :pswitch_4
    const v3, 0x7f0202ff

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 408
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 411
    :pswitch_5
    const v3, 0x7f020075

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 412
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 415
    :pswitch_6
    const v3, 0x7f020407

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 416
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 419
    :pswitch_7
    const v3, 0x7f02019e

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 420
    const/4 v3, 0x0

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 423
    :pswitch_8
    const/4 v3, 0x4

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 431
    :pswitch_9
    const/16 v3, 0x8

    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 432
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 433
    const/16 v3, 0x8

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 436
    :pswitch_a
    const/16 v3, 0x8

    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 437
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 438
    const/16 v3, 0x8

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 441
    :pswitch_b
    if-eqz v2, :cond_12

    const/4 v3, 0x0

    :goto_f
    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 442
    if-eqz v2, :cond_13

    const/4 v3, 0x0

    :goto_10
    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 443
    if-eqz v2, :cond_14

    const/4 v3, 0x0

    :goto_11
    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 441
    :cond_12
    const/16 v3, 0x8

    goto :goto_f

    .line 442
    :cond_13
    const/16 v3, 0x8

    goto :goto_10

    .line 443
    :cond_14
    const/16 v3, 0x8

    goto :goto_11

    .line 446
    :pswitch_c
    if-eqz v2, :cond_15

    const/4 v3, 0x0

    :goto_12
    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 447
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 448
    if-eqz v2, :cond_16

    const/4 v3, 0x0

    :goto_13
    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 446
    :cond_15
    const/16 v3, 0x8

    goto :goto_12

    .line 448
    :cond_16
    const/16 v3, 0x8

    goto :goto_13

    .line 451
    :pswitch_d
    if-eqz v2, :cond_17

    const/4 v3, 0x0

    :goto_14
    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 452
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 453
    if-eqz v2, :cond_18

    const/4 v3, 0x0

    :goto_15
    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 451
    :cond_17
    const/16 v3, 0x8

    goto :goto_14

    .line 453
    :cond_18
    const/16 v3, 0x8

    goto :goto_15

    .line 456
    :pswitch_e
    if-eqz v2, :cond_19

    const/4 v3, 0x0

    :goto_16
    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 457
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 458
    if-eqz v2, :cond_1a

    const/4 v3, 0x0

    :goto_17
    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 456
    :cond_19
    const/16 v3, 0x8

    goto :goto_16

    .line 458
    :cond_1a
    const/16 v3, 0x8

    goto :goto_17

    .line 461
    :pswitch_f
    const/16 v3, 0x8

    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 462
    const/16 v3, 0x8

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 463
    const/16 v3, 0x8

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 482
    :pswitch_10
    if-eqz v2, :cond_1b

    const/4 v3, 0x0

    :goto_18
    invoke-virtual {p6, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 483
    if-eqz v2, :cond_1c

    const/4 v3, 0x0

    :goto_19
    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 484
    if-eqz v2, :cond_1d

    const/4 v3, 0x0

    :goto_1a
    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 482
    :cond_1b
    const/16 v3, 0x8

    goto :goto_18

    .line 483
    :cond_1c
    const/16 v3, 0x8

    goto :goto_19

    .line 484
    :cond_1d
    const/16 v3, 0x8

    goto :goto_1a

    .line 503
    :pswitch_11
    const/16 v5, 0xff

    const/16 v6, 0xff

    const/16 v7, 0xff

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 504
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f08017d

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 507
    :pswitch_12
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 510
    :pswitch_13
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080076

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 511
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 512
    goto/16 :goto_5

    .line 514
    :pswitch_14
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0800dc

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 515
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 516
    goto/16 :goto_5

    .line 518
    :pswitch_15
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0801db

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 519
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 520
    goto/16 :goto_5

    .line 522
    :pswitch_16
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08011d

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 523
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 524
    goto/16 :goto_5

    .line 526
    :pswitch_17
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080124

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 527
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 528
    goto/16 :goto_5

    .line 530
    :pswitch_18
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08032d

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 531
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 532
    goto/16 :goto_5

    .line 534
    :pswitch_19
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080122

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 535
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 536
    goto/16 :goto_5

    .line 538
    :pswitch_1a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080252

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 539
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 540
    goto/16 :goto_5

    .line 542
    :pswitch_1b
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 545
    :pswitch_1c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080095

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 548
    :pswitch_1d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080031

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 551
    :pswitch_1e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802a6

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 554
    :pswitch_1f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0802c5

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 558
    :pswitch_20
    const/16 v5, 0xff

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 559
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f0801d2

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 562
    :pswitch_21
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a1

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 563
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 564
    goto/16 :goto_5

    .line 566
    :pswitch_22
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a2

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 567
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 568
    goto/16 :goto_5

    .line 570
    :pswitch_23
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0802a0

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 571
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 572
    goto/16 :goto_5

    .line 578
    :pswitch_24
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080103

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 579
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 580
    goto/16 :goto_5

    .line 582
    :pswitch_25
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f08024e

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 583
    const/4 v3, 0x1

    move v8, v3

    move-object v3, v4

    move v4, v8

    .line 584
    goto/16 :goto_5

    .line 610
    :cond_1e
    const/4 v3, 0x0

    invoke-virtual {p5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 611
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/ak;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, p1, Lsoftware/simplicial/a/bg;->n:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 613
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bn;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 614
    invoke-virtual {p5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    if-eqz v2, :cond_1f

    const/4 v2, 0x0

    const/16 v3, 0xff

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    :goto_1b
    invoke-virtual {p5, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_6

    :cond_1f
    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    goto :goto_1b

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 400
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 427
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_10
    .end packed-switch

    .line 500
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_20
        :pswitch_20
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_12
        :pswitch_21
        :pswitch_23
        :pswitch_19
        :pswitch_17
        :pswitch_18
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_1a
        :pswitch_25
        :pswitch_22
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_24
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 636
    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/l;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 639
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/a/l;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    move v3, v2

    .line 641
    :goto_1
    array-length v4, p1

    if-ge v3, v4, :cond_3

    .line 643
    iget v4, v0, Lsoftware/simplicial/a/bg;->b:I

    aget v5, p1, v3

    if-ne v4, v5, :cond_1

    .line 645
    aget-object v4, p2, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    .line 646
    aget-object v4, p3, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->k:Lsoftware/simplicial/a/aq;

    .line 647
    aget-boolean v4, p4, v3

    iput-boolean v4, v0, Lsoftware/simplicial/a/bg;->o:Z

    .line 648
    aget-object v4, p6, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->m:Lsoftware/simplicial/a/ak;

    .line 649
    aget-object v4, p5, v3

    iput-object v4, v0, Lsoftware/simplicial/a/bg;->l:Lsoftware/simplicial/a/bn;

    .line 650
    aget-short v3, p7, v3

    iput-short v3, v0, Lsoftware/simplicial/a/bg;->n:S

    .line 651
    const/4 v3, 0x1

    .line 655
    :goto_2
    if-nez v3, :cond_0

    .line 656
    sget-object v3, Lsoftware/simplicial/a/bg$b;->c:Lsoftware/simplicial/a/bg$b;

    iput-object v3, v0, Lsoftware/simplicial/a/bg;->j:Lsoftware/simplicial/a/bg$b;

    .line 636
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 641
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 659
    :cond_2
    new-instance v0, Lsoftware/simplicial/nebulous/a/l$10;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/a/l$10;-><init>(Lsoftware/simplicial/nebulous/a/l;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/a/l;->sort(Ljava/util/Comparator;)V

    .line 681
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    .line 682
    return-void

    :cond_3
    move v3, v2

    goto :goto_2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14

    .prologue
    .line 51
    .line 52
    if-nez p2, :cond_0

    .line 53
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f04006b

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/l;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/bg;

    .line 57
    const v1, 0x7f0d02e0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 58
    const v1, 0x7f0d009c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 59
    const v4, 0x7f0d0085

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 60
    const v5, 0x7f0d02db

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 61
    const v6, 0x7f0d02dc

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 62
    iget-object v7, v2, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    const-string v1, ""

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    const v1, 0x7f0d02c9

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 66
    const v1, 0x7f0d02df

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    .line 67
    const v1, 0x7f0d02c5

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 68
    const v1, 0x7f0d02dd

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    .line 69
    const v1, 0x7f0d02de

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    .line 70
    const v1, 0x7f0d02c2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageButton;

    .line 71
    const v1, 0x7f0d02e1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    move-object v1, p0

    .line 73
    invoke-direct/range {v1 .. v13}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/a/bg;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageButton;Landroid/widget/ImageView;)V

    .line 75
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l;->b:Lsoftware/simplicial/nebulous/a/l$a;

    sget-object v3, Lsoftware/simplicial/nebulous/a/l$a;->a:Lsoftware/simplicial/nebulous/a/l$a;

    if-ne v1, v3, :cond_4

    .line 77
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v1, v3, :cond_2

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-ne v1, v3, :cond_1

    iget-object v1, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v1, v3, :cond_2

    iget-object v1, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-eq v1, v3, :cond_2

    iget-object v1, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-eq v1, v3, :cond_2

    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-ne v1, v3, :cond_3

    iget-object v1, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->b:Lsoftware/simplicial/a/q;

    if-eq v1, v3, :cond_2

    iget-object v1, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v3, Lsoftware/simplicial/a/q;->e:Lsoftware/simplicial/a/q;

    if-ne v1, v3, :cond_3

    .line 80
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 84
    :goto_0
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$1;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$1;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v7, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$3;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$3;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$4;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$4;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v9, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$5;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$5;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v10, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 279
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$6;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$6;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v11, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$7;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$7;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v12, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$8;

    invoke-direct {v1, p0, v2}, Lsoftware/simplicial/nebulous/a/l$8;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    :goto_1
    new-instance v1, Lsoftware/simplicial/nebulous/a/l$9;

    invoke-direct {v1, p0, v2, v13}, Lsoftware/simplicial/nebulous/a/l$9;-><init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;Landroid/widget/ImageView;)V

    invoke-virtual {v13, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    return-object p2

    .line 82
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 345
    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 346
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 347
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 348
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 349
    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 350
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method
