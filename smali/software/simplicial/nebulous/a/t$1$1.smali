.class Lsoftware/simplicial/nebulous/a/t$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/t$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/t$1;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/t$1;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->a(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 106
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->a(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->g:Ljava/util/Set;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/a/t$1;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->b(Lsoftware/simplicial/nebulous/a/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 98
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->b(Lsoftware/simplicial/nebulous/a/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    iget v0, v0, Lsoftware/simplicial/a/d/b;->a:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget v2, v2, Lsoftware/simplicial/nebulous/a/t$1;->b:I

    if-ne v0, v2, :cond_2

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->b(Lsoftware/simplicial/nebulous/a/t;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 104
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/t;->a(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget v1, v1, Lsoftware/simplicial/nebulous/a/t$1;->b:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/t;->a(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/a/t;->a(ILsoftware/simplicial/a/d/c;)V

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t$1$1;->a:Lsoftware/simplicial/nebulous/a/t$1;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/t$1;->c:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/t;->notifyDataSetChanged()V

    goto :goto_0

    .line 96
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
