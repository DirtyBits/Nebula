.class Lsoftware/simplicial/nebulous/a/l$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/l;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/bg;

.field final synthetic b:Lsoftware/simplicial/nebulous/a/l;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/l$4;->a:Lsoftware/simplicial/a/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$4;->a:Lsoftware/simplicial/a/bg;

    iget-boolean v0, v0, Lsoftware/simplicial/a/bg;->o:Z

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 177
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080285

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 179
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 181
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 182
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 184
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/a/l$4$1;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/a/l$4$1;-><init>(Lsoftware/simplicial/nebulous/a/l$4;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 196
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 198
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 199
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 200
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 201
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 202
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 223
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 207
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 208
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 209
    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    const v3, 0x7f08024f

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$4;->a:Lsoftware/simplicial/a/bg;

    iget-object v2, v2, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 210
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080318

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/l$4$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/l$4$2;-><init>(Lsoftware/simplicial/nebulous/a/l$4;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$4;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 221
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
