.class public Lsoftware/simplicial/nebulous/a/m;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f04006b

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 32
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 33
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/m;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 38
    .line 39
    if-nez p2, :cond_0

    .line 40
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v3, 0x7f04006c

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 42
    :cond_0
    invoke-virtual/range {p0 .. p1}, Lsoftware/simplicial/nebulous/a/m;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/bg;

    .line 44
    const v3, 0x7f0d02e0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 45
    const v4, 0x7f0d009c

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 46
    const v5, 0x7f0d00a0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 47
    const v6, 0x7f0d02db

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 48
    const v7, 0x7f0d02e3

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    .line 49
    const v8, 0x7f0d02e5

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    .line 50
    const v9, 0x7f0d02e4

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    .line 51
    const v10, 0x7f0d01dc

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RadioGroup;

    .line 52
    const v11, 0x7f0d02e6

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/CheckBox;

    .line 53
    const v12, 0x7f0d02e7

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/CheckBox;

    .line 55
    iget-object v13, v2, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "("

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v13, v2, Lsoftware/simplicial/a/bg;->b:I

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, ")"

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v13, 0x7f08017a

    invoke-virtual {v5, v13}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v14, v2, Lsoftware/simplicial/a/bg;->g:J

    invoke-static {v14, v15}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 59
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    sget-object v4, Lsoftware/simplicial/nebulous/a/m$4;->a:[I

    iget-object v5, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v5}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 88
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 90
    iget-boolean v3, v2, Lsoftware/simplicial/a/bg;->s:Z

    if-eqz v3, :cond_4

    .line 91
    invoke-virtual {v8}, Landroid/widget/RadioButton;->toggle()V

    .line 97
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->c:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_6

    .line 99
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 100
    const/4 v3, 0x1

    invoke-virtual {v9, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 101
    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 102
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 112
    :goto_2
    new-instance v3, Lsoftware/simplicial/nebulous/a/m$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lsoftware/simplicial/nebulous/a/m$1;-><init>(Lsoftware/simplicial/nebulous/a/m;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v10, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_7

    :cond_2
    const/4 v3, 0x1

    :goto_3
    invoke-virtual {v11, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 159
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 160
    iget-boolean v3, v2, Lsoftware/simplicial/a/bg;->t:Z

    invoke-virtual {v11, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 161
    new-instance v3, Lsoftware/simplicial/nebulous/a/m$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lsoftware/simplicial/nebulous/a/m$2;-><init>(Lsoftware/simplicial/nebulous/a/m;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v11, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v3, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/m;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-ne v3, v4, :cond_8

    :cond_3
    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v12, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 175
    const/4 v3, 0x0

    invoke-virtual {v12, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 176
    iget-boolean v3, v2, Lsoftware/simplicial/a/bg;->u:Z

    invoke-virtual {v12, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 177
    new-instance v3, Lsoftware/simplicial/nebulous/a/m$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lsoftware/simplicial/nebulous/a/m$3;-><init>(Lsoftware/simplicial/nebulous/a/m;Lsoftware/simplicial/a/bg;)V

    invoke-virtual {v12, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 190
    return-object p2

    .line 64
    :pswitch_0
    const v4, 0x7f02019b

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 65
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 68
    :pswitch_1
    const v4, 0x7f0202ff

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 72
    :pswitch_2
    const v4, 0x7f020075

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 73
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 76
    :pswitch_3
    const v4, 0x7f020407

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 80
    :pswitch_4
    const v4, 0x7f02019e

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 84
    :pswitch_5
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 92
    :cond_4
    iget-boolean v3, v2, Lsoftware/simplicial/a/bg;->r:Z

    if-eqz v3, :cond_5

    .line 93
    invoke-virtual {v9}, Landroid/widget/RadioButton;->toggle()V

    goto/16 :goto_1

    .line 95
    :cond_5
    invoke-virtual {v7}, Landroid/widget/RadioButton;->toggle()V

    goto/16 :goto_1

    .line 106
    :cond_6
    const/4 v3, 0x0

    invoke-virtual {v10, v3}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 107
    const/4 v3, 0x0

    invoke-virtual {v9, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 108
    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 109
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto/16 :goto_2

    .line 158
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 174
    :cond_8
    const/4 v3, 0x0

    goto :goto_4

    .line 61
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
