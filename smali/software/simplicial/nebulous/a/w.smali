.class public Lsoftware/simplicial/nebulous/a/w;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# static fields
.field private static final b:[I


# instance fields
.field public final a:[Z

.field private final c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lsoftware/simplicial/nebulous/a/w;->b:[I

    return-void

    :array_0
    .array-data 4
        0x7f020197
        0x7f020384
        0x7f020076
        0x7f0203bb
        0x7f020401
        0x7f0203b9
        0x7f020391
        0x7f0201a4
        0x7f020066
        0x7f0201ab
        0x7f020073
    .end array-data
.end method

.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 2

    .prologue
    .line 28
    const v0, 0x7f040075

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 22
    sget-object v0, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [Z

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/w;->a:[Z

    .line 30
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/w;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/w;->a:[Z

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 33
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lsoftware/simplicial/a/bl$b;->m:[Lsoftware/simplicial/a/bl$b;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 44
    .line 45
    if-nez p2, :cond_0

    .line 46
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/w;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040075

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 48
    :cond_0
    const v0, 0x7f0d02f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 49
    const v1, 0x7f0d0143

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 51
    sget-object v2, Lsoftware/simplicial/nebulous/a/w;->b:[I

    aget v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 52
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 53
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/w;->a:[Z

    aget-boolean v0, v0, p1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 54
    new-instance v0, Lsoftware/simplicial/nebulous/a/w$1;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/a/w$1;-><init>(Lsoftware/simplicial/nebulous/a/w;I)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 63
    return-object p2
.end method
