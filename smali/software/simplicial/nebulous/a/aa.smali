.class public Lsoftware/simplicial/nebulous/a/aa;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/g;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f04007b

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 23
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/aa;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 24
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x7f0c00fb

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 29
    .line 30
    if-nez p2, :cond_0

    .line 31
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/aa;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04007b

    invoke-virtual {v0, v1, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 33
    :cond_0
    const v0, 0x7f0d009c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 34
    const v1, 0x7f0d02c4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 35
    const v2, 0x7f0d02ce

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 36
    const v3, 0x7f0d02d2

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 37
    const v4, 0x7f0d02d5

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 39
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/aa;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/nebulous/f/g;

    .line 41
    iget-object v6, v5, Lsoftware/simplicial/nebulous/f/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    iget-object v0, v5, Lsoftware/simplicial/nebulous/f/g;->d:Lsoftware/simplicial/a/h/f;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/aa;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v0, v6}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/h/f;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-boolean v6, v5, Lsoftware/simplicial/nebulous/f/g;->c:Z

    if-eqz v6, :cond_1

    .line 44
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/aa;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v7, 0x7f080186

    invoke-virtual {v6, v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    iget-object v0, v5, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 51
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 52
    iget-object v0, v5, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 55
    iget-object v0, v5, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v9, :cond_2

    .line 57
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    iget-object v0, v5, Lsoftware/simplicial/nebulous/f/g;->e:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/h;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/h;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 66
    :goto_0
    return-object p2

    .line 63
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
