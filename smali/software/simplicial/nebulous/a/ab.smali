.class public Lsoftware/simplicial/nebulous/a/ab;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private b:I

.field private c:Lsoftware/simplicial/a/b/b;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f040064

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 28
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/ab;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public a(ILsoftware/simplicial/a/b/b;)V
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lsoftware/simplicial/nebulous/a/ab;->b:I

    .line 93
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    .line 95
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/ab;->notifyDataSetChanged()V

    .line 96
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0c00fb

    const/16 v6, 0x8

    const/4 v7, 0x0

    .line 33
    .line 34
    if-nez p2, :cond_0

    .line 35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040064

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 37
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/ab;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/b/g;

    .line 39
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 40
    const v2, 0x7f0d02c4

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 42
    const v3, 0x7f0d02c5

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 43
    const v4, 0x7f0d02c2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 45
    iget-object v5, v0, Lsoftware/simplicial/a/b/g;->f:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget v5, v0, Lsoftware/simplicial/a/b/g;->c:I

    iget v8, p0, Lsoftware/simplicial/nebulous/a/ab;->b:I

    if-ne v5, v8, :cond_2

    move v5, v6

    :goto_0
    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 47
    iget v5, v0, Lsoftware/simplicial/a/b/g;->c:I

    iget v8, p0, Lsoftware/simplicial/nebulous/a/ab;->b:I

    if-ne v5, v8, :cond_3

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    sget-object v8, Lsoftware/simplicial/a/b/b;->b:Lsoftware/simplicial/a/b/b;

    if-ne v5, v8, :cond_3

    :goto_1
    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 49
    if-nez p1, :cond_8

    .line 51
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    sget-object v6, Lsoftware/simplicial/a/b/b;->b:Lsoftware/simplicial/a/b/b;

    if-ne v5, v6, :cond_4

    .line 52
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0093

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    :goto_2
    iget-object v1, v0, Lsoftware/simplicial/a/b/g;->e:Lsoftware/simplicial/a/b/d;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v1, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/b/d;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 66
    iget-boolean v5, v0, Lsoftware/simplicial/a/b/g;->g:Z

    if-eqz v5, :cond_1

    .line 67
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080186

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    :cond_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    new-instance v1, Lsoftware/simplicial/nebulous/a/ab$1;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/ab$1;-><init>(Lsoftware/simplicial/nebulous/a/ab;Lsoftware/simplicial/a/b/g;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    new-instance v0, Lsoftware/simplicial/nebulous/a/ab$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/a/ab$2;-><init>(Lsoftware/simplicial/nebulous/a/ab;)V

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-object p2

    :cond_2
    move v5, v7

    .line 46
    goto :goto_0

    :cond_3
    move v7, v6

    .line 47
    goto :goto_1

    .line 53
    :cond_4
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    sget-object v6, Lsoftware/simplicial/a/b/b;->e:Lsoftware/simplicial/a/b/b;

    if-ne v5, v6, :cond_5

    .line 54
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c006c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 55
    :cond_5
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    sget-object v6, Lsoftware/simplicial/a/b/b;->c:Lsoftware/simplicial/a/b/b;

    if-ne v5, v6, :cond_6

    .line 56
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c007a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 57
    :cond_6
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->c:Lsoftware/simplicial/a/b/b;

    sget-object v6, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    if-ne v5, v6, :cond_7

    .line 58
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0012

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 60
    :cond_7
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 63
    :cond_8
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/ab;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2
.end method
