.class public Lsoftware/simplicial/nebulous/a/ac;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field a:I

.field b:Ljava/lang/String;

.field c:Lsoftware/simplicial/a/h/a$a;

.field d:I

.field e:I

.field private final f:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f04007e

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 33
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 34
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/ac;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 0

    .prologue
    .line 121
    iput p1, p0, Lsoftware/simplicial/nebulous/a/ac;->a:I

    .line 122
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/ac;->b:Ljava/lang/String;

    .line 123
    iput-object p3, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    .line 124
    iput p4, p0, Lsoftware/simplicial/nebulous/a/ac;->d:I

    .line 125
    iput p5, p0, Lsoftware/simplicial/nebulous/a/ac;->e:I

    .line 127
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/ac;->notifyDataSetChanged()V

    .line 128
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f080186

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 39
    .line 40
    if-nez p2, :cond_0

    .line 41
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04007e

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 43
    :cond_0
    const v0, 0x7f0d009c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 44
    const v1, 0x7f0d0085

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 45
    const v2, 0x7f0d02c4

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 47
    const v3, 0x7f0d02c5

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 48
    const v4, 0x7f0d02c2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 50
    invoke-virtual {p2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/ac;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    .line 53
    instance-of v5, v5, Lsoftware/simplicial/a/h/g;

    if-eqz v5, :cond_2

    .line 55
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/ac;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/h/g;

    .line 57
    iget-object v6, v5, Lsoftware/simplicial/a/h/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    iget-object v0, v5, Lsoftware/simplicial/a/h/g;->c:Lsoftware/simplicial/a/h/f;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/h/f;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 61
    iget-boolean v1, v5, Lsoftware/simplicial/a/h/g;->d:Z

    if-eqz v1, :cond_1

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    :cond_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v3, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 66
    new-instance v0, Lsoftware/simplicial/nebulous/a/ac$1;

    invoke-direct {v0, p0, v5}, Lsoftware/simplicial/nebulous/a/ac$1;-><init>(Lsoftware/simplicial/nebulous/a/ac;Lsoftware/simplicial/a/h/g;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 116
    :goto_0
    return-object p2

    .line 79
    :cond_2
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/ac;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/h/b;

    .line 81
    iget-object v6, v5, Lsoftware/simplicial/a/h/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v0, v6}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/h/a$a;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    sget-object v6, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, v6, :cond_4

    .line 85
    invoke-virtual {p2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 97
    :goto_1
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lsoftware/simplicial/nebulous/a/ac;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsoftware/simplicial/nebulous/a/ac;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    iget-boolean v1, v5, Lsoftware/simplicial/a/h/b;->d:Z

    if-eqz v1, :cond_3

    .line 100
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    invoke-virtual {v3, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 105
    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 106
    new-instance v0, Lsoftware/simplicial/nebulous/a/ac$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/a/ac$2;-><init>(Lsoftware/simplicial/nebulous/a/ac;)V

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 86
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    sget-object v6, Lsoftware/simplicial/a/h/a$a;->c:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, v6, :cond_5

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c0056

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 88
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    sget-object v6, Lsoftware/simplicial/a/h/a$a;->d:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, v6, :cond_6

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c0093

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 90
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    sget-object v6, Lsoftware/simplicial/a/h/a$a;->e:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, v6, :cond_7

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c007a

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 92
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->c:Lsoftware/simplicial/a/h/a$a;

    sget-object v6, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    if-ne v0, v6, :cond_8

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c0012

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 95
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/ac;->f:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f0c00fb

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method
