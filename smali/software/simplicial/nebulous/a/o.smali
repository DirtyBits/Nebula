.class public Lsoftware/simplicial/nebulous/a/o;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private b:I


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f04006e

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 33
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 34
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/o;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lsoftware/simplicial/nebulous/a/o;->b:I

    .line 149
    return-void
.end method

.method public a(ILsoftware/simplicial/a/c/h;II)V
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/o;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 155
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/a/o;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/c/i;

    .line 157
    iget v2, v0, Lsoftware/simplicial/a/c/i;->c:I

    if-ne v2, p1, :cond_0

    .line 159
    iput-object p2, v0, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    .line 160
    iput p3, v0, Lsoftware/simplicial/a/c/i;->a:I

    .line 161
    iput p4, v0, Lsoftware/simplicial/a/c/i;->b:I

    .line 153
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 164
    :cond_1
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14

    .prologue
    .line 39
    .line 40
    if-nez p2, :cond_0

    .line 41
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f04006e

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 43
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/o;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsoftware/simplicial/a/c/i;

    .line 45
    const v2, 0x7f0d009c

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 46
    const v3, 0x7f0d0085

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 47
    const v4, 0x7f0d02c4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 49
    const v5, 0x7f0d02df

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    .line 50
    const v6, 0x7f0d02c5

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 51
    const v7, 0x7f0d02c2

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 53
    const v8, 0x7f0d02ea

    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    .line 55
    iget-object v9, v1, Lsoftware/simplicial/a/c/i;->g:Ljava/lang/String;

    iget-object v10, v1, Lsoftware/simplicial/a/c/i;->h:[B

    iget-object v11, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v12, v1, Lsoftware/simplicial/a/c/i;->g:Ljava/lang/String;

    .line 56
    invoke-interface {v11, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    iget-object v12, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v13, v1, Lsoftware/simplicial/a/c/i;->g:Ljava/lang/String;

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    .line 55
    invoke-static {v9, v10, v11, v12}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    sget-object v2, Lsoftware/simplicial/nebulous/a/o$4;->a:[I

    iget-object v9, v1, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    invoke-virtual {v9}, Lsoftware/simplicial/a/c/h;->ordinal()I

    move-result v9

    aget v2, v2, v9

    packed-switch v2, :pswitch_data_0

    .line 86
    :goto_0
    const-string v2, ""

    .line 87
    iget-boolean v3, v1, Lsoftware/simplicial/a/c/i;->j:Z

    if-eqz v3, :cond_1

    .line 88
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f080186

    invoke-virtual {v3, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 89
    :cond_1
    iget-boolean v3, v1, Lsoftware/simplicial/a/c/i;->i:Z

    if-eqz v3, :cond_3

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lsoftware/simplicial/a/c/i;->f:Lsoftware/simplicial/a/c/e;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v3, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/c/e;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lsoftware/simplicial/a/c/i;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lsoftware/simplicial/a/c/i;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 93
    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    sget-object v3, Lsoftware/simplicial/a/c/h;->f:Lsoftware/simplicial/a/c/h;

    if-eq v2, v3, :cond_4

    iget v2, v1, Lsoftware/simplicial/a/c/i;->c:I

    iget v3, p0, Lsoftware/simplicial/nebulous/a/o;->b:I

    if-eq v2, v3, :cond_4

    iget-boolean v2, v1, Lsoftware/simplicial/a/c/i;->i:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 96
    iget v2, v1, Lsoftware/simplicial/a/c/i;->a:I

    iget v3, v1, Lsoftware/simplicial/a/c/i;->b:I

    if-ge v2, v3, :cond_5

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 97
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    sget-object v3, Lsoftware/simplicial/a/c/h;->e:Lsoftware/simplicial/a/c/h;

    if-ne v2, v3, :cond_6

    iget v2, v1, Lsoftware/simplicial/a/c/i;->c:I

    iget v3, p0, Lsoftware/simplicial/nebulous/a/o;->b:I

    if-eq v2, v3, :cond_6

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 98
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    sget-object v3, Lsoftware/simplicial/a/c/h;->b:Lsoftware/simplicial/a/c/h;

    if-eq v2, v3, :cond_2

    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->d:Lsoftware/simplicial/a/c/h;

    sget-object v3, Lsoftware/simplicial/a/c/h;->c:Lsoftware/simplicial/a/c/h;

    if-ne v2, v3, :cond_7

    :cond_2
    iget v2, v1, Lsoftware/simplicial/a/c/i;->c:I

    iget v3, p0, Lsoftware/simplicial/nebulous/a/o;->b:I

    if-ne v2, v3, :cond_7

    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 101
    new-instance v2, Lsoftware/simplicial/nebulous/a/o$1;

    invoke-direct {v2, p0, v1}, Lsoftware/simplicial/nebulous/a/o$1;-><init>(Lsoftware/simplicial/nebulous/a/o;Lsoftware/simplicial/a/c/i;)V

    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    new-instance v2, Lsoftware/simplicial/nebulous/a/o$2;

    invoke-direct {v2, p0, v1}, Lsoftware/simplicial/nebulous/a/o$2;-><init>(Lsoftware/simplicial/nebulous/a/o;Lsoftware/simplicial/a/c/i;)V

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    new-instance v2, Lsoftware/simplicial/nebulous/a/o$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/o$3;-><init>(Lsoftware/simplicial/nebulous/a/o;)V

    invoke-virtual {v7, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_b

    .line 128
    new-instance v3, Lsoftware/simplicial/nebulous/a/p;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v5

    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/c/c;

    iget v2, v2, Lsoftware/simplicial/a/c/c;->b:I

    if-ne v5, v2, :cond_8

    const/4 v2, 0x1

    :goto_6
    invoke-direct {v3, v4, v2}, Lsoftware/simplicial/nebulous/a/p;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V

    .line 129
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/c/c;

    .line 130
    invoke-virtual {v3, v2}, Lsoftware/simplicial/nebulous/a/p;->add(Ljava/lang/Object;)V

    goto :goto_7

    .line 61
    :pswitch_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f0802dc

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    const/16 v2, 0xff

    const/16 v9, 0xff

    const/16 v10, 0xff

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 65
    :pswitch_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f080159

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const/16 v2, 0xff

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 69
    :pswitch_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08012d

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    const/4 v2, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 73
    :pswitch_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f080250

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const/16 v2, 0xff

    const/16 v9, 0x96

    const/4 v10, 0x0

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 77
    :pswitch_4
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08006c

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const/4 v2, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xff

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 81
    :pswitch_5
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f080259

    invoke-virtual {v2, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const/16 v2, 0xff

    const/16 v9, 0xff

    const/4 v10, 0x0

    invoke-static {v2, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 92
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lsoftware/simplicial/a/c/i;->f:Lsoftware/simplicial/a/c/e;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/a/o;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v3, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/c/e;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lsoftware/simplicial/a/c/i;->b:I

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/o;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v3, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(ILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 95
    :cond_4
    const/16 v2, 0x8

    goto/16 :goto_2

    .line 96
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 97
    :cond_6
    const/16 v2, 0x8

    goto/16 :goto_4

    .line 98
    :cond_7
    const/16 v2, 0x8

    goto/16 :goto_5

    .line 128
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 131
    :cond_9
    iget-object v2, v1, Lsoftware/simplicial/a/c/i;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v1, v1, Lsoftware/simplicial/a/c/i;->e:Lsoftware/simplicial/a/c/g;

    invoke-static {v1}, Lsoftware/simplicial/a/c/a;->a(Lsoftware/simplicial/a/c/g;)I

    move-result v1

    if-ge v2, v1, :cond_a

    .line 132
    new-instance v1, Lsoftware/simplicial/a/c/c;

    const/4 v2, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v1, v2, v4, v5, v6}, Lsoftware/simplicial/a/c/c;-><init>(IIZLjava/lang/String;)V

    invoke-virtual {v3, v1}, Lsoftware/simplicial/nebulous/a/p;->add(Ljava/lang/Object;)V

    .line 133
    :cond_a
    invoke-virtual {v8, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 134
    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/a/p;->notifyDataSetChanged()V

    .line 135
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 143
    :goto_8
    return-object p2

    .line 139
    :cond_b
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 140
    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_8

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
