.class public Lsoftware/simplicial/nebulous/a/j;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/nebulous/f/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f04006b

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 26
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/j;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 27
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/j;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/j;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 32
    .line 33
    if-nez p2, :cond_0

    .line 34
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/j;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f04006a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/j;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/m;

    .line 38
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 39
    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/m;->b:[B

    iget-object v4, p0, Lsoftware/simplicial/nebulous/a/j;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v5, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    .line 40
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/j;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v6, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 39
    invoke-static {v2, v3, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    const v1, 0x7f0d02ca

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 43
    const v2, 0x7f0d02c2

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 45
    new-instance v3, Lsoftware/simplicial/nebulous/a/j$1;

    invoke-direct {v3, p0, v0}, Lsoftware/simplicial/nebulous/a/j$1;-><init>(Lsoftware/simplicial/nebulous/a/j;Lsoftware/simplicial/nebulous/f/m;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    new-instance v1, Lsoftware/simplicial/nebulous/a/j$2;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/j$2;-><init>(Lsoftware/simplicial/nebulous/a/j;Lsoftware/simplicial/nebulous/f/m;)V

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    new-instance v1, Lsoftware/simplicial/nebulous/a/j$3;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/a/j$3;-><init>(Lsoftware/simplicial/nebulous/a/j;Lsoftware/simplicial/nebulous/f/m;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-object p2
.end method
