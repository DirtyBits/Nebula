.class Lsoftware/simplicial/nebulous/a/d$c;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lsoftware/simplicial/nebulous/a/d$d;",
        "Ljava/lang/Void;",
        "Lsoftware/simplicial/nebulous/a/d$d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/a/d;


# direct methods
.method private constructor <init>(Lsoftware/simplicial/nebulous/a/d;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsoftware/simplicial/nebulous/a/d;Lsoftware/simplicial/nebulous/a/d$1;)V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/a/d$c;-><init>(Lsoftware/simplicial/nebulous/a/d;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Lsoftware/simplicial/nebulous/a/d$d;)Lsoftware/simplicial/nebulous/a/d$d;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 511
    aget-object v0, p1, v3

    .line 513
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 514
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 515
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 516
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 517
    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 519
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->r(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$d;->b:Landroid/view/View;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, v0, Lsoftware/simplicial/nebulous/a/d$d;->a:Lsoftware/simplicial/a/af;

    if-eq v2, v3, :cond_0

    .line 520
    const/4 v0, 0x0

    .line 525
    :goto_0
    return-object v0

    .line 522
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    .line 523
    invoke-static {v3}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, v0, Lsoftware/simplicial/nebulous/a/d$d;->a:Lsoftware/simplicial/a/af;

    invoke-virtual {v4}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v6

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 522
    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/a/d$d;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected a(Lsoftware/simplicial/nebulous/a/d$d;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 531
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 533
    if-eqz p1, :cond_0

    iget-object v0, p1, Lsoftware/simplicial/nebulous/a/d$d;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$d;->b:Landroid/view/View;

    .line 537
    iget-object v6, p1, Lsoftware/simplicial/nebulous/a/d$d;->a:Lsoftware/simplicial/a/af;

    .line 539
    const v0, 0x7f0d02f8

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 540
    const v1, 0x7f0d02fa

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 541
    const v2, 0x7f0d02fc

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 542
    const v3, 0x7f0d02fb

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 543
    const v4, 0x7f0d02f9

    invoke-virtual {v5, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 545
    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v7}, Lsoftware/simplicial/nebulous/a/d;->r(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v6, :cond_0

    .line 548
    iget-object v5, p1, Lsoftware/simplicial/nebulous/a/d$d;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 550
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v5}, Lsoftware/simplicial/nebulous/a/d;->s(Lsoftware/simplicial/nebulous/a/d;)Ljava/util/Set;

    move-result-object v5

    invoke-static {v6, v5}, Lsoftware/simplicial/a/af;->a(Lsoftware/simplicial/a/af;Ljava/util/Set;)Z

    move-result v5

    .line 551
    if-eqz v5, :cond_2

    .line 552
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 556
    :goto_1
    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 557
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 559
    iget v0, v6, Lsoftware/simplicial/a/af;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 561
    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 562
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/d$c;->a:Lsoftware/simplicial/nebulous/a/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/d;->b(Lsoftware/simplicial/nebulous/a/d;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 563
    iget v0, v6, Lsoftware/simplicial/a/af;->d:I

    if-ltz v0, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget v1, v6, Lsoftware/simplicial/a/af;->d:I

    int-to-long v6, v1

    invoke-virtual {v0, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 565
    if-eqz v5, :cond_4

    .line 566
    const v0, 0x3f2aaaab

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 554
    :cond_2
    const/16 v7, 0xdc

    invoke-static {v7, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setColorFilter(I)V

    goto :goto_1

    .line 563
    :cond_3
    const-string v0, "---"

    goto :goto_2

    .line 568
    :cond_4
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setAlpha(F)V

    goto/16 :goto_0

    .line 572
    :cond_5
    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 505
    check-cast p1, [Lsoftware/simplicial/nebulous/a/d$d;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$c;->a([Lsoftware/simplicial/nebulous/a/d$d;)Lsoftware/simplicial/nebulous/a/d$d;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 505
    check-cast p1, Lsoftware/simplicial/nebulous/a/d$d;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/d$c;->a(Lsoftware/simplicial/nebulous/a/d$d;)V

    return-void
.end method
