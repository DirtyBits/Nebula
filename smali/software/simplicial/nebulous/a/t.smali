.class public Lsoftware/simplicial/nebulous/a/t;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lsoftware/simplicial/nebulous/e/b;

.field private d:Lsoftware/simplicial/a/d/c;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/util/List;Lsoftware/simplicial/nebulous/e/b;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/nebulous/application/MainActivity;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;",
            "Lsoftware/simplicial/nebulous/e/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    const v0, 0x7f040072

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    sget-object v0, Lsoftware/simplicial/a/d/c;->a:Lsoftware/simplicial/a/d/c;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->d:Lsoftware/simplicial/a/d/c;

    .line 34
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 35
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    .line 36
    iput-object p3, p0, Lsoftware/simplicial/nebulous/a/t;->c:Lsoftware/simplicial/nebulous/e/b;

    .line 37
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/application/MainActivity;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/a/t;)Ljava/util/List;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/a/t;)Lsoftware/simplicial/nebulous/e/b;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->c:Lsoftware/simplicial/nebulous/e/b;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;Lsoftware/simplicial/a/d/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;",
            "Lsoftware/simplicial/a/d/c;",
            ")V"
        }
    .end annotation

    .prologue
    .line 144
    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/t;->d:Lsoftware/simplicial/a/d/c;

    .line 145
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    .line 146
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/a/t;->notifyDataSetChanged()V

    .line 147
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 49
    .line 50
    if-nez p2, :cond_0

    .line 51
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040072

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 53
    :cond_0
    const v0, 0x7f0d01c9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    const v1, 0x7f0d02ec

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 55
    const v2, 0x7f0d02c4

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 56
    const v3, 0x7f0d02ed

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 57
    const v4, 0x7f0d02c5

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    .line 59
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/d/b;

    iget-object v8, v5, Lsoftware/simplicial/a/d/b;->b:Ljava/lang/String;

    .line 60
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/d/b;

    iget-object v9, v5, Lsoftware/simplicial/a/d/b;->f:Lsoftware/simplicial/a/am;

    .line 61
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/d/b;

    iget v10, v5, Lsoftware/simplicial/a/d/b;->a:I

    .line 62
    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lsoftware/simplicial/a/d/b;

    iget-boolean v5, v5, Lsoftware/simplicial/a/d/b;->h:Z

    .line 63
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    iget v0, v0, Lsoftware/simplicial/a/d/b;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v11, "/"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    iget v0, v0, Lsoftware/simplicial/a/d/b;->d:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->d:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_3

    move v0, v6

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->d:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_2

    .line 68
    const-string v0, ""

    .line 69
    if-eqz v5, :cond_1

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080186

    invoke-virtual {v1, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    iget-object v0, v0, Lsoftware/simplicial/a/d/b;->f:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v0, v6, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    iget-object v0, v0, Lsoftware/simplicial/a/d/b;->g:Lsoftware/simplicial/a/ap;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/a/t;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 72
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 71
    invoke-static {v0, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/ap;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/t;->d:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_4

    :goto_1
    invoke-virtual {v4, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 78
    new-instance v0, Lsoftware/simplicial/nebulous/a/t$1;

    invoke-direct {v0, p0, v8, v10}, Lsoftware/simplicial/nebulous/a/t$1;-><init>(Lsoftware/simplicial/nebulous/a/t;Ljava/lang/String;I)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    new-instance v0, Lsoftware/simplicial/nebulous/a/t$2;

    invoke-direct {v0, p0, v8}, Lsoftware/simplicial/nebulous/a/t$2;-><init>(Lsoftware/simplicial/nebulous/a/t;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    new-instance v0, Lsoftware/simplicial/nebulous/a/t$3;

    invoke-direct {v0, p0, v8, v9}, Lsoftware/simplicial/nebulous/a/t$3;-><init>(Lsoftware/simplicial/nebulous/a/t;Ljava/lang/String;Lsoftware/simplicial/a/am;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    return-object p2

    :cond_3
    move v0, v7

    .line 65
    goto/16 :goto_0

    :cond_4
    move v6, v7

    .line 76
    goto :goto_1
.end method
