.class Lsoftware/simplicial/nebulous/a/l$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/a/l;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/bg;

.field final synthetic b:Lsoftware/simplicial/nebulous/a/l;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/a/l;Lsoftware/simplicial/a/bg;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    sget-object v1, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-ne v0, v1, :cond_0

    .line 234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080203

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget-object v1, v1, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f08031b

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 235
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    .line 236
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 237
    invoke-static {v2}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v2

    const v3, 0x7f08002f

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 238
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 239
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f080318

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/a/l$5$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/a/l$5$1;-><init>(Lsoftware/simplicial/nebulous/a/l$5;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    .line 267
    invoke-static {v1}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 275
    :goto_0
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget-object v1, v1, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v1}, Lsoftware/simplicial/a/q;->b()Lsoftware/simplicial/a/q;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    .line 272
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/a/l;->a(Lsoftware/simplicial/nebulous/a/l;)Lsoftware/simplicial/nebulous/application/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget v1, v1, Lsoftware/simplicial/a/bg;->b:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/a/l$5;->a:Lsoftware/simplicial/a/bg;

    iget-object v2, v2, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/a/q;)V

    .line 273
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/l$5;->b:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    goto :goto_0
.end method
