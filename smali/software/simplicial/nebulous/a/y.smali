.class public Lsoftware/simplicial/nebulous/a/y;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lsoftware/simplicial/a/bg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f040077

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 29
    iput-object p1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 30
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    .line 35
    .line 36
    if-nez p2, :cond_0

    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040077

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 39
    :cond_0
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/a/y;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 41
    const v1, 0x7f0d009c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 42
    const v2, 0x7f0d02f3

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 43
    const v3, 0x7f0d0085

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 44
    const v4, 0x7f0d009d

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 45
    const v5, 0x7f0d009f

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 46
    const v6, 0x7f0d02c2

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    .line 47
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 48
    iget-object v6, v0, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    iget-boolean v1, v0, Lsoftware/simplicial/a/bg;->a:Z

    if-eqz v1, :cond_4

    .line 52
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080319

    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 56
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v1

    iget-boolean v1, v1, Lsoftware/simplicial/a/u;->r:Z

    if-eqz v1, :cond_1

    .line 58
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    const/16 v0, 0xc8

    const/16 v1, 0x96

    const/16 v3, 0xff

    invoke-static {v0, v1, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 60
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 68
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 120
    :goto_1
    return-object p2

    .line 64
    :cond_1
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/a/bg;->e:[B

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v7, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 65
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v8, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 64
    invoke-static {v1, v3, v6, v7}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, v0, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 70
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 72
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v7, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 73
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 72
    invoke-static {v1, v3, v6, v0}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_1

    .line 79
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 82
    :cond_4
    iget v1, v0, Lsoftware/simplicial/a/bg;->b:I

    const/4 v6, -0x2

    if-ne v1, v6, :cond_5

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080058

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 86
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto/16 :goto_1

    .line 88
    :cond_5
    iget v1, v0, Lsoftware/simplicial/a/bg;->b:I

    const/4 v6, -0x1

    if-gt v1, v6, :cond_6

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c9

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto/16 :goto_1

    .line 96
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v7, 0x7f08017a

    invoke-virtual {v6, v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v6, v0, Lsoftware/simplicial/a/bg;->g:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 99
    iget-object v1, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v1

    iget-boolean v1, v1, Lsoftware/simplicial/a/u;->r:Z

    if-eqz v1, :cond_7

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "{"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const/16 v0, 0xc8

    const/16 v1, 0x96

    const/16 v3, 0xff

    invoke-static {v0, v1, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 111
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 107
    :cond_7
    iget-object v1, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/a/bg;->e:[B

    iget-object v6, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v7, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 108
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    iget-object v7, p0, Lsoftware/simplicial/nebulous/a/y;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v8, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    .line 107
    invoke-static {v1, v3, v6, v7}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, v0, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_2

    .line 115
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    invoke-static {v0, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto/16 :goto_1
.end method
