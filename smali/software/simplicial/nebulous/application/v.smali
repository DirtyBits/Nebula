.class public Lsoftware/simplicial/nebulous/application/v;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Spinner;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/CheckBox;

.field h:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lsoftware/simplicial/nebulous/application/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/v;->a:Ljava/lang/String;

    .line 29
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/v;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    .line 182
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/v;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 183
    new-instance v4, Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f04009e

    invoke-direct {v4, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 184
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    sget-object v2, Lsoftware/simplicial/a/c/e;->g:Lsoftware/simplicial/a/c/e;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    sget-object v2, Lsoftware/simplicial/a/c/e;->e:Lsoftware/simplicial/a/c/e;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    sget-object v2, Lsoftware/simplicial/a/c/e;->j:Lsoftware/simplicial/a/c/e;

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x3

    .line 185
    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    .line 186
    aget-object v5, v3, v2

    invoke-virtual {v4, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 185
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 184
    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 187
    :cond_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 188
    add-int/lit8 v2, v0, -0x1

    if-le v1, v2, :cond_3

    .line 189
    add-int/lit8 v0, v0, -0x1

    .line 190
    :goto_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 191
    return-void

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/v;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/v;->a()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 198
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v1, Lsoftware/simplicial/a/c/b;->b:Lsoftware/simplicial/a/c/b;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->am:Lsoftware/simplicial/a/c/g;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/v;->h:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;ZZ)V

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 201
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 205
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 41
    const v0, 0x7f040039

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    const v0, 0x7f0d012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    .line 44
    const v0, 0x7f0d013a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    .line 45
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->e:Landroid/widget/Button;

    .line 46
    const v0, 0x7f0d013b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->f:Landroid/widget/Button;

    .line 47
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->g:Landroid/widget/CheckBox;

    .line 48
    const v0, 0x7f0d011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->h:Landroid/widget/CheckBox;

    .line 50
    return-object v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 56
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 58
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 59
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 61
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f04009e

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 62
    sget-object v0, Lsoftware/simplicial/nebulous/application/v$4;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->aj:Lsoftware/simplicial/a/c/e;

    invoke-virtual {v1}, Lsoftware/simplicial/a/c/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 86
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/v$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/v$1;-><init>(Lsoftware/simplicial/nebulous/application/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 121
    sget-object v0, Lsoftware/simplicial/nebulous/application/v$4;->b:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->am:Lsoftware/simplicial/a/c/g;

    invoke-virtual {v1}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 136
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/v$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/v$2;-><init>(Lsoftware/simplicial/nebulous/application/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->g:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/v;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->g:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/v$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/v$3;-><init>(Lsoftware/simplicial/nebulous/application/v;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    return-void

    .line 65
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 68
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 71
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 74
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v8}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 77
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 80
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->c:Landroid/widget/Spinner;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_1

    .line 124
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    .line 127
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v6}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    .line 130
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v7}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    .line 133
    :pswitch_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/v;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v8}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
