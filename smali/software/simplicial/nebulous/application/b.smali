.class public Lsoftware/simplicial/nebulous/application/b;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/nebulous/f/al$a;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/Button;

.field d:Landroid/widget/ListView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field private h:Lsoftware/simplicial/a/az;

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lsoftware/simplicial/nebulous/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lsoftware/simplicial/nebulous/application/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/b;->a:Ljava/lang/String;

    .line 41
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/b;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a(Lsoftware/simplicial/a/az;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/b;->h:Lsoftware/simplicial/a/az;

    .line 191
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/b;->d()V

    .line 192
    return-void
.end method

.method private b(Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/b;->i:Ljava/util/Map;

    .line 197
    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/b;->j:Ljava/util/Map;

    .line 198
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/b;->d()V

    .line 199
    return-void
.end method

.method private c()V
    .locals 8

    .prologue
    const/16 v1, 0x8

    const/4 v7, 0x0

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->h:Lsoftware/simplicial/a/az;

    if-nez v0, :cond_0

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->i:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_2

    .line 173
    sget-object v0, Lsoftware/simplicial/a/d;->bu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->h:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    sget-object v2, Lsoftware/simplicial/a/d;->bj:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v0, v2

    sget-object v2, Lsoftware/simplicial/a/d;->bp:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v0, v2

    .line 182
    :goto_2
    mul-int/lit8 v2, v0, 0x64

    div-int/2addr v2, v1

    .line 184
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/b;->f:Landroid/widget/TextView;

    const-string v4, "%s: %d / %d (%d%%)"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const v6, 0x7f080202

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/b;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 178
    :cond_2
    sget-object v0, Lsoftware/simplicial/a/d;->bt:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->h:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    goto :goto_2
.end method

.method private d()V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/b;->c()V

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->k:Lsoftware/simplicial/nebulous/a/a;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->h:Lsoftware/simplicial/a/az;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/a/a;->a(Lsoftware/simplicial/a/az;)V

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->k:Lsoftware/simplicial/nebulous/a/a;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->i:Ljava/util/Map;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/b;->j:Ljava/util/Map;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/a;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->k:Lsoftware/simplicial/nebulous/a/a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/a;->notifyDataSetChanged()V

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 253
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    return-void
.end method

.method public a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/b;->b(Ljava/util/Map;Ljava/util/Map;)V

    .line 144
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-direct {p0, p1, p4}, Lsoftware/simplicial/nebulous/application/b;->a(Lsoftware/simplicial/a/az;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 87
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 57
    const v0, 0x7f040027

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 59
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->c:Landroid/widget/Button;

    .line 60
    const v0, 0x7f0d00b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->d:Landroid/widget/ListView;

    .line 61
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->e:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0d00b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->f:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0d00b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->g:Landroid/widget/TextView;

    .line 65
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 121
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 92
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 94
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/az;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/b;->a(Lsoftware/simplicial/a/az;Ljava/lang/String;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_1

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->F:Lsoftware/simplicial/nebulous/f/ak;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ak;->a:Lsoftware/simplicial/nebulous/f/ak;

    if-ne v0, v1, :cond_2

    .line 104
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-virtual {v0, v2, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/b;)V

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-virtual {v0, v3, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZILsoftware/simplicial/nebulous/f/al$a;)V

    .line 113
    :cond_1
    :goto_0
    return-void

    .line 109
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;Ljava/lang/String;Lsoftware/simplicial/a/b;)V

    .line 110
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZILsoftware/simplicial/nebulous/f/al$a;)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    new-instance v1, Lsoftware/simplicial/nebulous/a/a;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v4, :cond_0

    sget-object v0, Lsoftware/simplicial/a/d;->bu:Ljava/util/List;

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lsoftware/simplicial/nebulous/a/a;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/List;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->k:Lsoftware/simplicial/nebulous/a/a;

    .line 77
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/b;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/b;->k:Lsoftware/simplicial/nebulous/a/a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    return-void

    .line 75
    :cond_0
    sget-object v0, Lsoftware/simplicial/a/d;->bt:Ljava/util/List;

    goto :goto_0
.end method

.method public p_()V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Lsoftware/simplicial/a/az;

    invoke-direct {v0}, Lsoftware/simplicial/a/az;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/b;->a(Lsoftware/simplicial/a/az;Ljava/lang/String;)V

    .line 150
    return-void
.end method
