.class Lsoftware/simplicial/nebulous/application/bj$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/bj;->b(Lsoftware/simplicial/nebulous/application/bc$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/bc$a;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/bj;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/bj;Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/bj$4;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/be;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 175
    :goto_0
    return-void

    .line 163
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/be;

    .line 165
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bj$4;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/application/bj;->b(Lsoftware/simplicial/nebulous/application/bj;Lsoftware/simplicial/nebulous/application/bc$a;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/bj;->d:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bj;->j:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 173
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bj;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/bj;->g:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/1024"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj$4;->b:Lsoftware/simplicial/nebulous/application/bj;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj$4;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/bj;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method
