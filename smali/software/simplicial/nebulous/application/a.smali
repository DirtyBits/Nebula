.class public Lsoftware/simplicial/nebulous/application/a;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/nebulous/f/al$y;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field A:Landroid/widget/LinearLayout;

.field B:Landroid/widget/EditText;

.field C:Landroid/widget/Button;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/ImageView;

.field g:Landroid/widget/ImageView;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/ImageButton;

.field l:Landroid/widget/LinearLayout;

.field m:Landroid/widget/LinearLayout;

.field n:Landroid/widget/LinearLayout;

.field o:Lcom/google/android/gms/common/SignInButton;

.field p:Lcom/facebook/login/widget/LoginButton;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/Button;

.field t:Landroid/widget/Button;

.field u:Landroid/widget/Button;

.field v:Landroid/widget/Button;

.field w:Landroid/widget/Button;

.field x:Landroid/widget/Button;

.field y:Landroid/widget/Button;

.field z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-class v0, Lsoftware/simplicial/nebulous/application/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/a;->a:Ljava/lang/String;

    .line 66
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/a;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/a;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 408
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 437
    :goto_0
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->a()Lsoftware/simplicial/a/f/aa;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/f/aa;->a:Lsoftware/simplicial/a/f/aa;

    if-eq v0, v1, :cond_1

    .line 413
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 414
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 415
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080273

    .line 416
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801cf

    .line 417
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/a$2;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/a$2;-><init>(Lsoftware/simplicial/nebulous/application/a;Z)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    .line 430
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 434
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/f/al;->a(Z)V

    .line 435
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 245
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 318
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->k:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->o:Lcom/google/android/gms/common/SignInButton;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/common/SignInButton;->setEnabled(Z)V

    .line 262
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_2

    .line 264
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080275

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 267
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 257
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->o:Lcom/google/android/gms/common/SignInButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/SignInButton;->setEnabled(Z)V

    .line 259
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->j:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 273
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 275
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080272

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 278
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    invoke-static {v1, v2, v5, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 288
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 292
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 284
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_2

    .line 296
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-eqz v0, :cond_5

    .line 298
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080247

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 301
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 309
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801c9

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    sget-object v0, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 312
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private d()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 518
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 520
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 521
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 522
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 523
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 525
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/a$3;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/a$3;-><init>(Lsoftware/simplicial/nebulous/application/a;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 543
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 545
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 547
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 548
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 549
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 550
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 551
    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/16 v6, 0x8

    .line 555
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 557
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080245

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 558
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 559
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 560
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 561
    new-array v2, v3, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x10

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 562
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 564
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/a$4;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/a$4;-><init>(Lsoftware/simplicial/nebulous/application/a;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 592
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 594
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 596
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 597
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 598
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 599
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/Window;->clearFlags(I)V

    .line 600
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 398
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 392
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 404
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    .line 368
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 350
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 334
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    .line 336
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 337
    if-eqz p1, :cond_0

    .line 339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$y;)V

    .line 340
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802ee

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080354

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 342
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 373
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    .line 374
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 628
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 629
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    invoke-static {}, Lcom/facebook/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->e()Lcom/facebook/f;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/f;->a(IILandroid/content/Intent;)Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 442
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->o:Lcom/google/android/gms/common/SignInButton;

    if-ne p1, v0, :cond_1

    .line 444
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->m()V

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    if-eqz v0, :cond_2

    .line 448
    invoke-static {}, Lcom/facebook/a;->a()Lcom/facebook/a;

    move-result-object v0

    if-nez v0, :cond_0

    .line 450
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/f/al;->c:Z

    .line 451
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    goto :goto_0

    .line 454
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 456
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/f/al;->a(Z)V

    .line 457
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    goto :goto_0

    .line 459
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 461
    invoke-direct {p0, v4}, Lsoftware/simplicial/nebulous/application/a;->b(Z)V

    goto :goto_0

    .line 463
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 465
    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/a;->b(Z)V

    goto :goto_0

    .line 467
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 469
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->e()V

    goto :goto_0

    .line 471
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->z:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 473
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 474
    const v1, 0x7f0801a3

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 475
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 477
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0800d0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 479
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->k:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_8

    .line 481
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 483
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->t:Landroid/widget/Button;

    if-eq p1, v0, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->x:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 485
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 486
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->s:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 488
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->u:Landroid/widget/Button;

    if-eq p1, v0, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->y:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 490
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 491
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->t:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 493
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 495
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 497
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->s:Landroid/widget/Button;

    if-ne p1, v0, :cond_e

    .line 499
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->W:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 501
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->w:Landroid/widget/Button;

    if-ne p1, v0, :cond_f

    .line 503
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->d()V

    goto/16 :goto_0

    .line 505
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->v:Landroid/widget/Button;

    if-ne p1, v0, :cond_10

    .line 507
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->af:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 509
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->C:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 511
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/al$y;)V

    .line 512
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 232
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/ao;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 233
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->A:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->A:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 100
    const v0, 0x7f040026

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 102
    const v0, 0x7f0d00a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/login/widget/LoginButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    .line 106
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "public_profile"

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/facebook/login/widget/LoginButton;->setReadPermissions([Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->e()Lcom/facebook/f;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/a$1;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/a$1;-><init>(Lsoftware/simplicial/nebulous/application/a;)V

    invoke-virtual {v0, v2, v3}, Lcom/facebook/login/widget/LoginButton;->a(Lcom/facebook/f;Lcom/facebook/j;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    const v0, 0x7f0d009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->c:Landroid/widget/TextView;

    .line 162
    const v0, 0x7f0d009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->d:Landroid/widget/TextView;

    .line 163
    const v0, 0x7f0d009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->f:Landroid/widget/ImageView;

    .line 164
    const v0, 0x7f0d009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->g:Landroid/widget/ImageView;

    .line 165
    const v0, 0x7f0d00a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->e:Landroid/widget/TextView;

    .line 166
    const v0, 0x7f0d00b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->h:Landroid/widget/Button;

    .line 167
    const v0, 0x7f0d00b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->i:Landroid/widget/Button;

    .line 168
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->j:Landroid/widget/Button;

    .line 169
    const v0, 0x7f0d009b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->k:Landroid/widget/ImageButton;

    .line 170
    const v0, 0x7f0d00a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->l:Landroid/widget/LinearLayout;

    .line 171
    const v0, 0x7f0d00a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->m:Landroid/widget/LinearLayout;

    .line 172
    const v0, 0x7f0d00b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->n:Landroid/widget/LinearLayout;

    .line 173
    const v0, 0x7f0d00a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/SignInButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->o:Lcom/google/android/gms/common/SignInButton;

    .line 174
    const v0, 0x7f0d00a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->q:Landroid/widget/Button;

    .line 175
    const v0, 0x7f0d00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->r:Landroid/widget/Button;

    .line 176
    const v0, 0x7f0d00ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->s:Landroid/widget/Button;

    .line 177
    const v0, 0x7f0d00ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->t:Landroid/widget/Button;

    .line 178
    const v0, 0x7f0d00ad

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->u:Landroid/widget/Button;

    .line 179
    const v0, 0x7f0d00ae

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->v:Landroid/widget/Button;

    .line 180
    const v0, 0x7f0d00af

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->w:Landroid/widget/Button;

    .line 181
    const v0, 0x7f0d00b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->x:Landroid/widget/Button;

    .line 182
    const v0, 0x7f0d00b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->y:Landroid/widget/Button;

    .line 183
    const v0, 0x7f0d00a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->z:Landroid/widget/Button;

    .line 184
    const v0, 0x7f0d009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->A:Landroid/widget/LinearLayout;

    .line 185
    const v0, 0x7f0d00a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    .line 186
    const v0, 0x7f0d00a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->C:Landroid/widget/Button;

    .line 188
    return-object v1

    .line 155
    :catch_0
    move-exception v0

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 619
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 622
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 606
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 608
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ak;->a:Lsoftware/simplicial/nebulous/f/ak;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->F:Lsoftware/simplicial/nebulous/f/ak;

    .line 609
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 611
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 612
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/al$y;)V

    .line 614
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 194
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 196
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->z:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->o:Lcom/google/android/gms/common/SignInButton;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/SignInButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->t:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->r:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->s:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->u:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->v:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->w:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->x:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->y:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->C:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->p:Lcom/facebook/login/widget/LoginButton;

    invoke-virtual {v0, p0}, Lcom/facebook/login/widget/LoginButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    const v1, 0x20001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->B:Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x808

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 218
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/a;->c()V

    .line 356
    return-void
.end method
