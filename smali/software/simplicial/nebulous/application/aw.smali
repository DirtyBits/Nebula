.class public Lsoftware/simplicial/nebulous/application/aw;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/Button;

.field n:Lsoftware/simplicial/nebulous/f/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lsoftware/simplicial/nebulous/application/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/aw;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/aw;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0801cf

    const v5, 0x7f08005d

    const v4, 0x7f08002f

    const v2, 0x1080027

    const/4 v3, 0x0

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 192
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 193
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 194
    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0800e8

    .line 195
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 196
    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/aw$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/aw$4;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 208
    invoke-virtual {v1, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/y;->b:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 214
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/y;->d:I

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/y;->e:Ljava/lang/String;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 222
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/y;->f:Ljava/lang/String;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v3, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/y;->b:I

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/y;->c:Ljava/lang/String;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    goto :goto_1

    .line 226
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 228
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 229
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 230
    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080216

    .line 231
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 232
    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/aw$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/aw$5;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 244
    invoke-virtual {v1, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 246
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 248
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 249
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 250
    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801cd

    .line 251
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 252
    invoke-virtual {v1, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/aw$6;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/aw$6;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 264
    invoke-virtual {v1, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 50
    const v0, 0x7f040055

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 52
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->c:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f0d024f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->d:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f0d0250

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->e:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0d0251

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->f:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f0d0252

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->g:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f0d0253

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->h:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0d0254

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->k:Landroid/widget/Button;

    .line 59
    const v0, 0x7f0d0255

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->l:Landroid/widget/Button;

    .line 60
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->m:Landroid/widget/Button;

    .line 61
    const v0, 0x7f0d023f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->i:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0d00d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->j:Landroid/widget/Button;

    .line 64
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 175
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 176
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-wide v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ai:J

    new-instance v1, Lsoftware/simplicial/nebulous/application/aw$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aw$3;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v2, v3, v1}, Lsoftware/simplicial/nebulous/f/al;->a(JLsoftware/simplicial/nebulous/f/al$aa;)V

    .line 170
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->d:Landroid/widget/TextView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/aw$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aw$1;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw;->e:Landroid/widget/TextView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/aw$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aw$2;-><init>(Lsoftware/simplicial/nebulous/application/aw;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    return-void
.end method
