.class public Lsoftware/simplicial/nebulous/application/al;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$ad;
.implements Lsoftware/simplicial/nebulous/f/al$l;
.implements Lsoftware/simplicial/nebulous/f/al$m;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/application/bc$a;


# instance fields
.field c:I

.field d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

.field e:[Landroid/widget/ImageView;

.field f:[Landroid/widget/TextView;

.field g:[Landroid/widget/TextView;

.field h:[Landroid/widget/Button;

.field i:[Landroid/widget/Button;

.field j:[Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field private l:[Landroid/graphics/Bitmap;

.field private m:[I

.field private n:[Lsoftware/simplicial/nebulous/f/q;

.field private o:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lsoftware/simplicial/nebulous/application/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/al;->a:Ljava/lang/String;

    .line 46
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 42
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/al;->c:I

    .line 49
    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->e:[Landroid/widget/ImageView;

    .line 50
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->f:[Landroid/widget/TextView;

    .line 51
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->g:[Landroid/widget/TextView;

    .line 52
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    .line 53
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->i:[Landroid/widget/Button;

    .line 54
    new-array v0, v1, [Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->j:[Landroid/widget/Button;

    .line 56
    new-array v0, v1, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->l:[Landroid/graphics/Bitmap;

    .line 57
    new-array v0, v1, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    .line 58
    new-array v0, v1, [Lsoftware/simplicial/nebulous/f/q;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->n:[Lsoftware/simplicial/nebulous/f/q;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->o:Landroid/graphics/Bitmap;

    return-void
.end method

.method private a(I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 237
    iput p1, p0, Lsoftware/simplicial/nebulous/application/al;->c:I

    .line 239
    if-ltz p1, :cond_0

    .line 241
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->g:[Landroid/widget/TextView;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 242
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v5}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v5

    int-to-long v6, p1

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 246
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->g:[Landroid/widget/TextView;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 247
    const-string v5, "---"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    .line 250
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    array-length v0, v0

    if-ge v2, v0, :cond_3

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    aget-object v3, v0, v2

    .line 253
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->o:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->n:[Lsoftware/simplicial/nebulous/f/q;

    aget-object v0, v0, v2

    sget-object v4, Lsoftware/simplicial/nebulous/f/q;->b:Lsoftware/simplicial/nebulous/f/q;

    if-eq v0, v4, :cond_2

    if-ltz p1, :cond_2

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 250
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move v0, v1

    .line 253
    goto :goto_3

    .line 255
    :cond_3
    return-void
.end method

.method private a(ILandroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 272
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->l:[Landroid/graphics/Bitmap;

    aput-object p2, v0, p1

    .line 273
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->e:[Landroid/widget/ImageView;

    aget-object v0, v0, p1

    .line 274
    sget-object v1, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    if-ne p2, v1, :cond_0

    .line 275
    const v1, 0x108003f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 285
    :goto_0
    return-void

    .line 276
    :cond_0
    if-eqz p2, :cond_1

    .line 277
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 283
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 279
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    invoke-virtual {v2}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawable"

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private a(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 106
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->e:[Landroid/widget/ImageView;

    const v0, 0x7f0d0300

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, p1

    .line 107
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->f:[Landroid/widget/TextView;

    const v0, 0x7f0d0085

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 108
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->g:[Landroid/widget/TextView;

    const v0, 0x7f0d0301

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, p1

    .line 109
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    const v0, 0x7f0d02ad

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 110
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->i:[Landroid/widget/Button;

    const v0, 0x7f0d0302

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 111
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->j:[Landroid/widget/Button;

    const v0, 0x7f0d0303

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, p1

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    aget-object v0, v0, p1

    new-instance v1, Lsoftware/simplicial/nebulous/application/al$2;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/al$2;-><init>(Lsoftware/simplicial/nebulous/application/al;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->i:[Landroid/widget/Button;

    aget-object v0, v0, p1

    new-instance v1, Lsoftware/simplicial/nebulous/application/al$3;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/al$3;-><init>(Lsoftware/simplicial/nebulous/application/al;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->j:[Landroid/widget/Button;

    aget-object v0, v0, p1

    new-instance v1, Lsoftware/simplicial/nebulous/application/al$4;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/al$4;-><init>(Lsoftware/simplicial/nebulous/application/al;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    return-void
.end method

.method private a(ILsoftware/simplicial/nebulous/f/q;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 291
    :try_start_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->n:[Lsoftware/simplicial/nebulous/f/q;

    aput-object p2, v2, p1

    .line 292
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->f:[Landroid/widget/TextView;

    aget-object v3, v2, p1

    .line 293
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    aget-object v4, v2, p1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->o:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    sget-object v2, Lsoftware/simplicial/nebulous/f/q;->b:Lsoftware/simplicial/nebulous/f/q;

    if-eq p2, v2, :cond_1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/al;->c:I

    if-ltz v2, :cond_1

    move v2, v0

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 294
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->j:[Landroid/widget/Button;

    aget-object v2, v2, p1

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v4, v4, p1

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 295
    sget-object v0, Lsoftware/simplicial/nebulous/application/al$6;->a:[I

    invoke-virtual {p2}, Lsoftware/simplicial/nebulous/f/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 348
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v1

    .line 293
    goto :goto_0

    :cond_2
    move v0, v1

    .line 294
    goto :goto_1

    .line 298
    :pswitch_0
    const v0, 0x7f08010d

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 299
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 300
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v0, v0, p1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 303
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 344
    :catch_0
    move-exception v0

    .line 346
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 307
    :pswitch_1
    const v0, 0x7f08015a

    :try_start_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 308
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0093

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 309
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v0, v0, p1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 312
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->invalidate()V

    goto/16 :goto_2

    .line 316
    :pswitch_2
    const v0, 0x7f08022c

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 317
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 318
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v0, v0, p1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c006c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 321
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->invalidate()V

    goto/16 :goto_2

    .line 325
    :pswitch_3
    const v0, 0x7f080232

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 326
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 327
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v0, v0, p1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 330
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->invalidate()V

    goto/16 :goto_2

    .line 334
    :pswitch_4
    const v0, 0x7f08002e

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 335
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 336
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aget v0, v0, p1

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->invalidate()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 399
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/al;->o:Landroid/graphics/Bitmap;

    .line 400
    if-nez p1, :cond_0

    .line 401
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020432

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 404
    :goto_0
    return-void

    .line 403
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/al;ILandroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/al;ILsoftware/simplicial/nebulous/f/q;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/al;->a(ILsoftware/simplicial/nebulous/f/q;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/al;)[Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->l:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/al;)[I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/be;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 233
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/be;

    .line 224
    sget-object v2, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v2, v3, :cond_2

    .line 225
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "UPLOADED_ACCOUNT_SKIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 226
    iget v2, v0, Lsoftware/simplicial/a/be;->c:I

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(I)V

    .line 227
    :cond_2
    sget-object v2, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v2, v3, :cond_1

    .line 228
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "UPLOADED_CLAN_SKIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/al;->a(I)V

    goto :goto_1

    .line 232
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/al;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 353
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 395
    :goto_0
    return-void

    .line 357
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    array-length v0, v0

    if-ge v2, v0, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 359
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/q;

    .line 360
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 361
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    aput v1, v3, v2

    .line 364
    if-eqz v1, :cond_2

    .line 366
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    new-instance v4, Lsoftware/simplicial/nebulous/application/al$5;

    invoke-direct {v4, p0, v2}, Lsoftware/simplicial/nebulous/application/al$5;-><init>(Lsoftware/simplicial/nebulous/application/al;I)V

    invoke-virtual {v3, v1, v4}, Lsoftware/simplicial/nebulous/f/p;->a(ILsoftware/simplicial/nebulous/f/al$ag;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 378
    if-eqz v1, :cond_1

    .line 379
    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/graphics/Bitmap;)V

    .line 386
    :cond_1
    :goto_2
    invoke-direct {p0, v2, v0}, Lsoftware/simplicial/nebulous/application/al;->a(ILsoftware/simplicial/nebulous/f/q;)V

    .line 357
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 383
    :cond_2
    invoke-direct {p0, v2, v5}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/graphics/Bitmap;)V

    goto :goto_2

    .line 388
    :cond_3
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->m:[I

    array-length v0, v0

    if-ge v2, v0, :cond_4

    .line 390
    invoke-direct {p0, v2, v5}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/graphics/Bitmap;)V

    .line 391
    sget-object v0, Lsoftware/simplicial/nebulous/f/q;->a:Lsoftware/simplicial/nebulous/f/q;

    invoke-direct {p0, v2, v0}, Lsoftware/simplicial/nebulous/application/al;->a(ILsoftware/simplicial/nebulous/f/q;)V

    .line 388
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 394
    :cond_4
    sget-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/al;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public a(ZLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    if-eqz p1, :cond_0

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    sget-object v1, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bc$a;->a()Lsoftware/simplicial/nebulous/f/o$b;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/o$b;Lsoftware/simplicial/nebulous/f/al$ad;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 64
    const v0, 0x7f04004a

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f0d01e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 69
    const/4 v0, 0x1

    const v2, 0x7f0d01ea

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 70
    const/4 v0, 0x2

    const v2, 0x7f0d01eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 71
    const/4 v0, 0x3

    const v2, 0x7f0d01ec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 72
    const/4 v0, 0x4

    const v2, 0x7f0d01ed

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 73
    const/4 v0, 0x5

    const v2, 0x7f0d01ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 74
    const/4 v0, 0x6

    const v2, 0x7f0d01ef

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 75
    const/4 v0, 0x7

    const v2, 0x7f0d01f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 76
    const/16 v0, 0x8

    const v2, 0x7f0d01f1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 77
    const/16 v0, 0x9

    const v2, 0x7f0d01f2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 78
    const/16 v0, 0xa

    const v2, 0x7f0d01f3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 79
    const/16 v0, 0xb

    const v2, 0x7f0d01f4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 80
    const/16 v0, 0xc

    const v2, 0x7f0d01f5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 81
    const/16 v0, 0xd

    const v2, 0x7f0d01f6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 82
    const/16 v0, 0xe

    const v2, 0x7f0d01f7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 83
    const/16 v0, 0xf

    const v2, 0x7f0d01f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 84
    const/16 v0, 0x10

    const v2, 0x7f0d01f9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 85
    const/16 v0, 0x11

    const v2, 0x7f0d01fa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 86
    const/16 v0, 0x12

    const v2, 0x7f0d01fb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 87
    const/16 v0, 0x13

    const v2, 0x7f0d01fc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V

    .line 89
    const v0, 0x7f0d00d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->d:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/al;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 91
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->k:Landroid/widget/Button;

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->k:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/al$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/al$1;-><init>(Lsoftware/simplicial/nebulous/application/al;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 213
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 214
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 199
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->j:[Landroid/widget/Button;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 200
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al;->h:[Landroid/widget/Button;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 202
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 204
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/al;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$m;)V

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    sget-object v1, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/bc$a;->a()Lsoftware/simplicial/nebulous/f/o$b;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/o$b;Lsoftware/simplicial/nebulous/f/al$ad;)V

    .line 208
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 189
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 191
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/al;->a(Landroid/graphics/Bitmap;)V

    .line 192
    return-void
.end method
