.class public Lsoftware/simplicial/nebulous/application/ae;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lsoftware/simplicial/a/ah;
.implements Lsoftware/simplicial/a/an;
.implements Lsoftware/simplicial/a/w;
.implements Lsoftware/simplicial/nebulous/e/a;
.implements Lsoftware/simplicial/nebulous/f/d;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/ImageView;

.field b:Landroid/widget/EditText;

.field c:Landroid/widget/ImageButton;

.field d:Landroid/widget/ImageButton;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/ImageButton;

.field h:Landroid/widget/ImageButton;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/ImageView;

.field k:Landroid/widget/ImageView;

.field l:Landroid/widget/ImageView;

.field m:Landroid/widget/ImageView;

.field n:Landroid/widget/ImageView;

.field o:Landroid/widget/ImageButton;

.field p:Landroid/widget/ImageButton;

.field q:Landroid/widget/CheckBox;

.field r:Landroid/widget/CheckBox;

.field s:Landroid/widget/ImageButton;

.field t:Landroid/widget/RelativeLayout;

.field u:Landroid/widget/ImageView;

.field v:Landroid/widget/ImageButton;

.field w:Landroid/widget/ImageButton;

.field x:Landroid/widget/ImageButton;

.field private y:Ljava/util/Random;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lsoftware/simplicial/nebulous/application/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ae;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 90
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->y:Ljava/util/Random;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ae;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ae;->d()V

    return-void
.end method

.method private d()V
    .locals 14

    .prologue
    const v3, 0x7f0801ad

    const/high16 v13, -0x1000000

    const/16 v12, 0x7e

    const/4 v4, 0x0

    .line 371
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 374
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v10

    .line 378
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_9

    .line 380
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 382
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 385
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 427
    :cond_1
    :goto_0
    return-void

    .line 388
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    .line 390
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-boolean v0, v0, Lsoftware/simplicial/a/t;->t:Z

    if-eqz v0, :cond_3

    .line 391
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, v10, Lsoftware/simplicial/a/u;->aZ:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->g(I)V

    .line 419
    :goto_1
    iget-object v0, v10, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v1, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v0, v1, :cond_1

    .line 421
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->y:Ljava/util/Random;

    invoke-virtual {v0, v12}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x64

    .line 422
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->y:Ljava/util/Random;

    invoke-virtual {v1, v12}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x64

    .line 423
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ae;->y:Ljava/util/Random;

    invoke-virtual {v2, v12}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x64

    .line 424
    shl-int/lit8 v3, v2, 0x10

    or-int/2addr v3, v13

    shl-int/lit8 v4, v1, 0x8

    or-int/2addr v3, v4

    or-int/2addr v3, v0

    sput v3, Lsoftware/simplicial/nebulous/views/GameView;->b:I

    .line 425
    add-int/lit8 v2, v2, 0x1e

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v2, v13

    add-int/lit8 v1, v1, 0x1e

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1e

    or-int/2addr v0, v1

    sput v0, Lsoftware/simplicial/nebulous/views/GameView;->c:I

    goto :goto_0

    .line 393
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 394
    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    :goto_2
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 395
    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    :goto_3
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 396
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 397
    invoke-virtual {v6}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v6

    sget-object v7, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    :goto_4
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 398
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 399
    invoke-virtual {v8}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v8

    sget-object v9, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v8, v9, :cond_7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->c()Ljava/lang/String;

    move-result-object v8

    :goto_5
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 400
    invoke-virtual {v9}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v9

    sget-object v11, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v9, v11, :cond_8

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 393
    :goto_6
    invoke-virtual/range {v0 .. v9}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILsoftware/simplicial/a/bd;ILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_1

    .line 394
    :cond_4
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    goto :goto_2

    .line 395
    :cond_5
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    goto :goto_3

    .line 397
    :cond_6
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    goto :goto_4

    .line 399
    :cond_7
    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 400
    :cond_8
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    goto :goto_6

    .line 405
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-boolean v0, v0, Lsoftware/simplicial/a/t;->t:Z

    if-eqz v0, :cond_a

    .line 406
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, v10, Lsoftware/simplicial/a/u;->aZ:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->g(I)V

    goto/16 :goto_1

    .line 408
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Noob "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ae;->y:Ljava/util/Random;

    const v3, 0x1869f

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [B

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 409
    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v3, v4, :cond_b

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    :goto_7
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 410
    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    :goto_8
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 411
    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 412
    invoke-virtual {v6}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v6

    sget-object v7, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v6, v7, :cond_d

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    :goto_9
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 413
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v7

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 414
    invoke-virtual {v8}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v8

    sget-object v9, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v8, v9, :cond_e

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->c()Ljava/lang/String;

    move-result-object v8

    :goto_a
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 415
    invoke-virtual {v9}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v9

    sget-object v11, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v9, v11, :cond_f

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 408
    :goto_b
    invoke-virtual/range {v0 .. v9}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;ILsoftware/simplicial/a/bd;ILjava/lang/String;Lsoftware/simplicial/a/as;)V

    goto/16 :goto_1

    .line 409
    :cond_b
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    goto :goto_7

    .line 410
    :cond_c
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    goto :goto_8

    .line 412
    :cond_d
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    goto :goto_9

    .line 414
    :cond_e
    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/f/ag;->d()Ljava/lang/String;

    move-result-object v8

    goto :goto_a

    .line 415
    :cond_f
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    goto :goto_b
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 541
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 547
    return-void
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 633
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 634
    if-nez v0, :cond_0

    .line 649
    :goto_0
    return-void

    .line 637
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ae$8;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ae$8;-><init>(Lsoftware/simplicial/nebulous/application/ae;Ljava/lang/String;[B)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 535
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 506
    if-nez v0, :cond_0

    .line 529
    :goto_0
    return-void

    .line 509
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ae$6;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/ae$6;-><init>(Lsoftware/simplicial/nebulous/application/ae;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ZZLjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 628
    :goto_0
    return-void

    .line 624
    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_2

    .line 625
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->u:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 627
    :cond_2
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->c()V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 598
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 599
    if-nez v0, :cond_0

    .line 615
    :goto_0
    return v2

    .line 602
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ae$7;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/ae$7;-><init>(Lsoftware/simplicial/nebulous/application/ae;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->F:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 367
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 553
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 575
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 576
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->c:Landroid/widget/ImageButton;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 577
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->q:Landroid/widget/CheckBox;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0801ac

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 579
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 580
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 581
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 582
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 583
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->m:Landroid/widget/ImageView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 584
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->r:Landroid/widget/CheckBox;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    if-eqz v0, :cond_8

    const v0, 0x7f08003a

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 586
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v3

    .line 588
    iget-boolean v4, v3, Lsoftware/simplicial/a/u;->o:Z

    .line 589
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v5, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v5, :cond_9

    const/4 v0, 0x1

    .line 591
    :goto_9
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->h:Landroid/widget/ImageButton;

    iget-object v3, v3, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v6, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-ne v3, v6, :cond_a

    if-eqz v0, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-boolean v0, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v0, :cond_a

    :goto_a
    invoke-virtual {v5, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 592
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->h:Landroid/widget/ImageButton;

    if-nez v4, :cond_b

    const v0, 0x7f020147

    :goto_b
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 593
    return-void

    .line 575
    :cond_0
    const/4 v0, 0x4

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 576
    goto/16 :goto_1

    .line 577
    :cond_2
    const v0, 0x7f080270

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 579
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 580
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 581
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 582
    goto :goto_6

    :cond_7
    move v0, v2

    .line 583
    goto :goto_7

    .line 584
    :cond_8
    const v0, 0x7f08026b

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    :cond_9
    move v0, v1

    .line 589
    goto :goto_9

    :cond_a
    move v1, v2

    .line 591
    goto :goto_a

    .line 592
    :cond_b
    const v0, 0x7f020148

    goto :goto_b
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 655
    if-nez v0, :cond_0

    .line 669
    :goto_0
    return-void

    .line 658
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ae$9;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ae$9;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 558
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->q:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_1

    .line 560
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    .line 561
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->c()V

    .line 562
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->i()V

    .line 563
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "namesEnabled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 565
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->r:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 567
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    .line 568
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->c()V

    .line 569
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "avatarsEnabled"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 217
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_1

    .line 222
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 223
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08002f

    .line 224
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080233

    .line 225
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080318

    .line 226
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ae$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ae$2;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 237
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ae;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    :catch_0
    move-exception v0

    .line 360
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v2, "Error"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 244
    :cond_2
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->d:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 246
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 247
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08002f

    .line 248
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080177

    .line 249
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080318

    .line 250
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ae$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ae$3;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 261
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 263
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 267
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_5

    .line 269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 271
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->j:Landroid/widget/ImageView;

    if-eq p1, v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    if-eq p1, v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->l:Landroid/widget/ImageView;

    if-eq p1, v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->m:Landroid/widget/ImageView;

    if-eq p1, v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->n:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_7

    .line 273
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->r:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 275
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->v:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_a

    .line 277
    invoke-static {}, Lcom/facebook/share/widget/a;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 279
    new-instance v0, Lcom/facebook/share/b/a$a;

    invoke-direct {v0}, Lcom/facebook/share/b/a$a;-><init>()V

    const-string v1, "https://fb.me/1828792354049175"

    .line 280
    invoke-virtual {v0, v1}, Lcom/facebook/share/b/a$a;->a(Ljava/lang/String;)Lcom/facebook/share/b/a$a;

    move-result-object v0

    const-string v1, "https://www.simplicialsoftware.com/homepage/img/app_icon.png"

    .line 281
    invoke-virtual {v0, v1}, Lcom/facebook/share/b/a$a;->b(Ljava/lang/String;)Lcom/facebook/share/b/a$a;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lcom/facebook/share/b/a$a;->a()Lcom/facebook/share/b/a;

    move-result-object v0

    .line 283
    invoke-static {p0, v0}, Lcom/facebook/share/widget/a;->a(Landroid/app/Fragment;Lcom/facebook/share/b/a;)V

    goto/16 :goto_0

    .line 285
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0, v1}, Landroid/support/v4/b/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/e;->b()V

    .line 288
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/views/GameView;->f:Z

    goto/16 :goto_0

    .line 291
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    const v2, 0x7f0801b7

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 293
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->c:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_c

    .line 295
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 296
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 299
    :cond_b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 300
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08008c

    .line 301
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08008c

    .line 302
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801a6

    .line 303
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ae$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ae$5;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080041

    .line 315
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ae$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ae$4;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 331
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->o:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_d

    .line 333
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->j:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 335
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->p:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_e

    .line 337
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->l:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 339
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->s:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_10

    .line 341
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/u;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 342
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    .line 343
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 345
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_11

    .line 347
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->b()V

    goto/16 :goto_0

    .line 349
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->x:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_12

    .line 351
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 353
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->w:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 97
    const v0, 0x7f040044

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 99
    const v0, 0x7f0d0118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    .line 100
    const v0, 0x7f0d0183

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->c:Landroid/widget/ImageButton;

    .line 101
    const v0, 0x7f0d017c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->d:Landroid/widget/ImageButton;

    .line 102
    const v0, 0x7f0d018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->e:Landroid/widget/Button;

    .line 103
    const v0, 0x7f0d018d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->f:Landroid/widget/Button;

    .line 104
    const v0, 0x7f0d0171

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->g:Landroid/widget/ImageButton;

    .line 105
    const v0, 0x7f0d017a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->h:Landroid/widget/ImageButton;

    .line 106
    const v0, 0x7f0d0179

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->i:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0d0186

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->j:Landroid/widget/ImageView;

    .line 108
    const v0, 0x7f0d0187

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    .line 109
    const v0, 0x7f0d0189

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->l:Landroid/widget/ImageView;

    .line 110
    const v0, 0x7f0d018a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->m:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f0d0188

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->n:Landroid/widget/ImageView;

    .line 112
    const v0, 0x7f0d018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->v:Landroid/widget/ImageButton;

    .line 113
    const v0, 0x7f0d017d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->o:Landroid/widget/ImageButton;

    .line 114
    const v0, 0x7f0d017e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->p:Landroid/widget/ImageButton;

    .line 115
    const v0, 0x7f0d0182

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->q:Landroid/widget/CheckBox;

    .line 116
    const v0, 0x7f0d0185

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->r:Landroid/widget/CheckBox;

    .line 117
    const v0, 0x7f0d0180

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->s:Landroid/widget/ImageButton;

    .line 118
    const v0, 0x7f0d017f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->t:Landroid/widget/RelativeLayout;

    .line 119
    const v0, 0x7f0d007b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->u:Landroid/widget/ImageView;

    .line 120
    const v0, 0x7f0d0190

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->w:Landroid/widget/ImageButton;

    .line 121
    const v0, 0x7f0d018e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->x:Landroid/widget/ImageButton;

    .line 122
    const v0, 0x7f0d00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->z:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f0d0191

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->A:Landroid/widget/ImageView;

    .line 125
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 494
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 496
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 497
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->j:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 498
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 499
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->b:Lsoftware/simplicial/nebulous/e/a;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    .line 500
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v5, 0x3

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 432
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 434
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v0, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    .line 436
    if-eqz v0, :cond_1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v4, Lsoftware/simplicial/a/bp;->d:Lsoftware/simplicial/a/bp;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v3, v4, :cond_1

    .line 438
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v5, v5, v3}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v3

    .line 439
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->z:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0800fa

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f080114

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->z:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 447
    :goto_0
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->A:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v4, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    .line 450
    :goto_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    .line 451
    :goto_3
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 452
    :goto_4
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v5}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v5

    sget-object v6, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    .line 453
    :goto_5
    sget-object v6, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    .line 454
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ae;->j:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v0}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v9, "drawable"

    iget-object v10, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v10}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v0, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 455
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v0, :cond_8

    .line 457
    if-eqz v6, :cond_7

    .line 458
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-direct {v7, v8, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 462
    :goto_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 469
    :goto_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->l:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v3}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "drawable"

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v3, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 470
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->n:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v4}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "drawable"

    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 471
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->m:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v5}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 473
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->e:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 474
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->e:Landroid/widget/Button;

    const v3, 0x7f0801dd

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/u;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 476
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480
    :goto_8
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->c()V

    .line 482
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 483
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->j:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 484
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 485
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    .line 487
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_0

    .line 488
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/d;)V

    .line 489
    :cond_0
    return-void

    .line 444
    :cond_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->z:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 447
    goto/16 :goto_1

    .line 449
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    goto/16 :goto_2

    .line 450
    :cond_4
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    goto/16 :goto_3

    .line 451
    :cond_5
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    goto/16 :goto_4

    .line 452
    :cond_6
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    goto/16 :goto_5

    .line 460
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget-object v7, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    invoke-virtual {v7}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "drawable"

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_6

    .line 466
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 478
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->u:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 132
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 134
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->b:Landroid/widget/EditText;

    new-instance v3, Lsoftware/simplicial/nebulous/application/ae$1;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ae$1;-><init>(Lsoftware/simplicial/nebulous/application/ae;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->p:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->q:Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->r:Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->g:Z

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v3

    .line 183
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->f:Landroid/widget/Button;

    iget-boolean v0, v3, Lsoftware/simplicial/a/u;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 186
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->t:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v5, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v4, :cond_4

    iget-object v0, v3, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v4, Lsoftware/simplicial/a/aq;->b:Lsoftware/simplicial/a/aq;

    if-ne v0, v4, :cond_4

    .line 189
    new-instance v0, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v5, v6}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bn;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 190
    new-instance v4, Landroid/text/SpannableString;

    iget-object v5, v3, Lsoftware/simplicial/a/u;->ba:Ljava/lang/String;

    iget-object v6, v3, Lsoftware/simplicial/a/u;->bb:[B

    invoke-static {v5, v6}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ae;->i:Landroid/widget/TextView;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/CharSequence;

    aput-object v0, v6, v1

    const/4 v0, 0x1

    aput-object v4, v6, v0

    invoke-static {v6}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ae;->o:Landroid/widget/ImageButton;

    iget-object v4, v3, Lsoftware/simplicial/a/u;->aV:Lsoftware/simplicial/a/aq;

    sget-object v5, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    if-eq v4, v5, :cond_0

    iget-boolean v4, v3, Lsoftware/simplicial/a/u;->p:Z

    if-nez v4, :cond_0

    iget-boolean v4, v3, Lsoftware/simplicial/a/u;->r:Z

    if-nez v4, :cond_0

    iget-boolean v4, v3, Lsoftware/simplicial/a/u;->q:Z

    if-nez v4, :cond_0

    iget-boolean v3, v3, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 209
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->c()V

    .line 210
    return-void

    :cond_2
    move v0, v2

    .line 183
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 186
    goto :goto_1

    .line 195
    :cond_4
    const-string v0, ""

    .line 196
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v4, v5, :cond_5

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bn;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_5
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->A()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 199
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v4, 0x7f080044

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, v3, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-boolean v4, v3, Lsoftware/simplicial/a/u;->aR:Z

    if-eqz v4, :cond_6

    .line 204
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v4, 0x7f080186

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ae;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    :cond_6
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->i:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 201
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ae;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v4}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ae;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/bj;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
