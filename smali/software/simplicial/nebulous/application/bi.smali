.class public Lsoftware/simplicial/nebulous/application/bi;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljava/lang/Runnable;
.implements Lsoftware/simplicial/nebulous/f/al$ah;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private A:J

.field private B:I

.field b:Landroid/widget/Button;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/ImageView;

.field f:Landroid/widget/LinearLayout;

.field g:Landroid/widget/ImageView;

.field h:Landroid/widget/TextView;

.field i:Landroid/view/View;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Ljava/lang/Thread;

.field private n:Z

.field private o:Lsoftware/simplicial/nebulous/f/ai;

.field private p:J

.field private q:J

.field private r:Ljava/util/Random;

.field private s:D

.field private t:D

.field private u:D

.field private v:J

.field private w:D

.field private final x:Ljava/lang/Object;

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lsoftware/simplicial/nebulous/application/bi;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bi;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const-wide/16 v6, -0x1

    const-wide/16 v4, 0x0

    .line 38
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 60
    iput-object v9, p0, Lsoftware/simplicial/nebulous/application/bi;->m:Ljava/lang/Thread;

    .line 61
    iput-boolean v8, p0, Lsoftware/simplicial/nebulous/application/bi;->n:Z

    .line 62
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->a:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    .line 63
    iput-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 64
    iput-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 65
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    .line 67
    iput-wide v4, p0, Lsoftware/simplicial/nebulous/application/bi;->t:D

    .line 68
    iput-wide v4, p0, Lsoftware/simplicial/nebulous/application/bi;->u:D

    .line 69
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    .line 70
    iput-wide v4, p0, Lsoftware/simplicial/nebulous/application/bi;->w:D

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->x:Ljava/lang/Object;

    .line 72
    iput-object v9, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    .line 74
    iput-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->A:J

    .line 75
    iput v8, p0, Lsoftware/simplicial/nebulous/application/bi;->B:I

    return-void
.end method

.method private a()D
    .locals 12

    .prologue
    const-wide/16 v0, 0x0

    const-wide/high16 v10, 0x406b000000000000L    # 216.0

    const-wide v8, 0x405f800000000000L    # 126.0

    const-wide/high16 v6, 0x4032000000000000L    # 18.0

    const/4 v4, 0x3

    .line 338
    .line 340
    const-string v2, "NONE"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 342
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 469
    :cond_0
    :goto_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    const-wide/high16 v6, 0x4030000000000000L    # 16.0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 470
    const-wide v2, 0x4076800000000000L    # 360.0

    sub-double v0, v2, v0

    .line 471
    const-wide/high16 v2, 0x400c000000000000L    # 3.5

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 472
    mul-int/lit16 v2, v2, 0x168

    int-to-double v2, v2

    add-double/2addr v0, v2

    .line 473
    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    return-wide v0

    .line 345
    :pswitch_0
    add-double/2addr v0, v0

    .line 346
    goto :goto_0

    .line 348
    :pswitch_1
    const-wide/high16 v2, 0x4062000000000000L    # 144.0

    add-double/2addr v0, v2

    .line 349
    goto :goto_0

    .line 351
    :pswitch_2
    const-wide v2, 0x4074400000000000L    # 324.0

    add-double/2addr v0, v2

    goto :goto_0

    .line 355
    :cond_1
    const-string v2, "SKIN"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 360
    :pswitch_3
    add-double/2addr v0, v6

    .line 361
    goto :goto_0

    .line 363
    :pswitch_4
    add-double/2addr v0, v8

    .line 364
    goto :goto_0

    .line 366
    :pswitch_5
    add-double/2addr v0, v10

    goto :goto_0

    .line 370
    :cond_2
    const-string v2, "EJECT_SKIN"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 372
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_2

    goto :goto_0

    .line 375
    :pswitch_6
    add-double/2addr v0, v6

    .line 376
    goto :goto_0

    .line 378
    :pswitch_7
    add-double/2addr v0, v8

    .line 379
    goto :goto_0

    .line 381
    :pswitch_8
    add-double/2addr v0, v10

    goto :goto_0

    .line 385
    :cond_3
    const-string v2, "HAT"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 387
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->r:Ljava/util/Random;

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_3

    goto :goto_0

    .line 390
    :pswitch_9
    add-double/2addr v0, v6

    .line 391
    goto :goto_0

    .line 393
    :pswitch_a
    add-double/2addr v0, v8

    .line 394
    goto :goto_0

    .line 396
    :pswitch_b
    add-double/2addr v0, v10

    goto :goto_0

    .line 400
    :cond_4
    const-string v2, "XP_2X"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 404
    const-string v2, "XP_3X"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 406
    iget v2, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    packed-switch v2, :pswitch_data_4

    goto/16 :goto_0

    .line 412
    :pswitch_c
    const-wide v2, 0x4066800000000000L    # 180.0

    add-double/2addr v0, v2

    .line 413
    goto/16 :goto_0

    .line 409
    :pswitch_d
    const-wide v2, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v2

    .line 410
    goto/16 :goto_0

    .line 415
    :pswitch_e
    const-wide v2, 0x4073200000000000L    # 306.0

    add-double/2addr v0, v2

    goto/16 :goto_0

    .line 419
    :cond_5
    const-string v2, "AUTO"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 423
    const-string v2, "ULTRA"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 425
    iget v2, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_0

    .line 428
    :sswitch_0
    const-wide/high16 v2, 0x404b000000000000L    # 54.0

    add-double/2addr v0, v2

    .line 429
    goto/16 :goto_0

    .line 431
    :sswitch_1
    const-wide v2, 0x4070e00000000000L    # 270.0

    add-double/2addr v0, v2

    goto/16 :goto_0

    .line 435
    :cond_6
    const-string v2, "PLASMA"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 437
    iget v2, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    sparse-switch v2, :sswitch_data_1

    goto/16 :goto_0

    .line 461
    :sswitch_2
    const-wide/high16 v2, 0x4072000000000000L    # 288.0

    add-double/2addr v0, v2

    .line 462
    goto/16 :goto_0

    .line 440
    :sswitch_3
    const-wide/high16 v2, 0x4042000000000000L    # 36.0

    add-double/2addr v0, v2

    .line 441
    goto/16 :goto_0

    .line 443
    :sswitch_4
    const-wide/high16 v2, 0x4052000000000000L    # 72.0

    add-double/2addr v0, v2

    .line 444
    goto/16 :goto_0

    .line 446
    :sswitch_5
    const-wide/high16 v2, 0x405b000000000000L    # 108.0

    add-double/2addr v0, v2

    .line 447
    goto/16 :goto_0

    .line 449
    :sswitch_6
    const-wide v2, 0x4064400000000000L    # 162.0

    add-double/2addr v0, v2

    .line 450
    goto/16 :goto_0

    .line 452
    :sswitch_7
    const-wide v2, 0x4068c00000000000L    # 198.0

    add-double/2addr v0, v2

    .line 453
    goto/16 :goto_0

    .line 455
    :sswitch_8
    const-wide v2, 0x406d400000000000L    # 234.0

    add-double/2addr v0, v2

    .line 456
    goto/16 :goto_0

    .line 458
    :sswitch_9
    const-wide v2, 0x406f800000000000L    # 252.0

    add-double/2addr v0, v2

    .line 459
    goto/16 :goto_0

    .line 464
    :sswitch_a
    const-wide v2, 0x4075600000000000L    # 342.0

    add-double/2addr v0, v2

    goto/16 :goto_0

    .line 342
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 357
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 372
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 387
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 406
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 425
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x18 -> :sswitch_1
    .end sparse-switch

    .line 437
    :sswitch_data_1
    .sparse-switch
        0x32 -> :sswitch_2
        0x64 -> :sswitch_6
        0x96 -> :sswitch_9
        0xc8 -> :sswitch_3
        0xfa -> :sswitch_8
        0x15e -> :sswitch_5
        0x190 -> :sswitch_7
        0x3e8 -> :sswitch_4
        0x5dc -> :sswitch_a
    .end sparse-switch
.end method

.method private a(J)D
    .locals 7

    .prologue
    .line 304
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 305
    iget-wide p1, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    .line 306
    :cond_0
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->w:D

    neg-double v0, v0

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    .line 307
    long-to-double v2, p1

    mul-double/2addr v0, v2

    long-to-double v2, p1

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->w:D

    long-to-double v4, p1

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->t:D

    add-double/2addr v0, v2

    return-wide v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bi;D)D
    .locals 1

    .prologue
    .line 38
    iput-wide p1, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    return-wide p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->x:Ljava/lang/Object;

    return-object v0
.end method

.method private a(DLandroid/app/Activity;)V
    .locals 1

    .prologue
    .line 132
    if-nez p3, :cond_0

    .line 144
    :goto_0
    return-void

    .line 134
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/bi$1;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bi$1;-><init>(Lsoftware/simplicial/nebulous/application/bi;D)V

    invoke-virtual {p3, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V
    .locals 1

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 603
    :goto_0
    return-void

    .line 486
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/bi$3;

    invoke-direct {v0, p0, p2}, Lsoftware/simplicial/nebulous/application/bi$3;-><init>(Lsoftware/simplicial/nebulous/application/bi;Z)V

    invoke-virtual {p1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bi;DLandroid/app/Activity;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    return-void
.end method

.method private b()J
    .locals 4

    .prologue
    .line 478
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->u:D

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->t:D

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->w:D

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bi;)Lsoftware/simplicial/nebulous/f/ai;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/bi;)J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->A:J

    return-wide v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/bi;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bi;->B:I

    return v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/application/bi;)D
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->u:D

    return-wide v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/application/bi;)D
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bi;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/application/bi;)D
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    return-wide v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/application/bi;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;IJI)V
    .locals 7

    .prologue
    .line 313
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    .line 314
    iput p2, p0, Lsoftware/simplicial/nebulous/application/bi;->z:I

    .line 315
    iput-wide p3, p0, Lsoftware/simplicial/nebulous/application/bi;->A:J

    .line 316
    iput p5, p0, Lsoftware/simplicial/nebulous/application/bi;->B:I

    .line 318
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    sget-object v2, Lsoftware/simplicial/nebulous/f/ai;->d:Lsoftware/simplicial/nebulous/f/ai;

    if-ne v0, v2, :cond_0

    .line 322
    const-wide v2, 0x3f79bc65b68b71c3L    # 0.006283185307179587

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->w:D

    .line 323
    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->t:D

    .line 324
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bi;->a()D

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->u:D

    .line 325
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bi;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    .line 327
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 328
    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/application/bi;->v:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 329
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->e:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    .line 333
    :goto_0
    monitor-exit v1

    .line 334
    return-void

    .line 332
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    sget-object v3, Lsoftware/simplicial/nebulous/f/ai;->e:Lsoftware/simplicial/nebulous/f/ai;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v2, v0}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 332
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 159
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->f:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi;->x:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->b:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    .line 166
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 167
    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    const-wide/16 v4, 0x5dc

    add-long/2addr v2, v4

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 168
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 172
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ai:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 174
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 178
    :cond_2
    return-void

    .line 168
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 80
    const v0, 0x7f040060

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 82
    const v0, 0x7f0d02b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    .line 83
    const v0, 0x7f0d02b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->c:Landroid/widget/Button;

    .line 84
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->d:Landroid/widget/Button;

    .line 85
    const v0, 0x7f0d02b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->e:Landroid/widget/ImageView;

    .line 86
    const v0, 0x7f0d02b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->f:Landroid/widget/LinearLayout;

    .line 87
    const v0, 0x7f0d02b2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->g:Landroid/widget/ImageView;

    .line 88
    const v0, 0x7f0d02b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->h:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0d02b5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->i:Landroid/view/View;

    .line 90
    const v0, 0x7f0d02ae

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->j:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0d02af

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->k:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0d00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    .line 94
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bi;->n:Z

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->m:Ljava/lang/Thread;

    .line 154
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 112
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bi;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {p0, v0, v1, v2}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->k:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {p0, v0, v3}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V

    .line 123
    iput-boolean v3, p0, Lsoftware/simplicial/nebulous/application/bi;->n:Z

    .line 124
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->m:Ljava/lang/Thread;

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$ah;)V

    .line 128
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    .line 183
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bi;->n:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 186
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 187
    if-nez v4, :cond_1

    .line 300
    :cond_0
    return-void

    .line 190
    :cond_1
    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/bi;->x:Ljava/lang/Object;

    monitor-enter v8

    .line 192
    const-wide/16 v0, 0x0

    .line 193
    :try_start_0
    iget-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    iget-wide v10, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    cmp-long v5, v6, v10

    if-lez v5, :cond_2

    .line 195
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    sub-long v0, v2, v0

    .line 196
    iget-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    iget-wide v10, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    sub-long/2addr v6, v10

    .line 197
    long-to-double v0, v0

    long-to-double v6, v6

    div-double/2addr v0, v6

    .line 199
    :cond_2
    const-wide/16 v6, 0x0

    cmpg-double v5, v0, v6

    if-gez v5, :cond_3

    const-wide/16 v0, 0x0

    .line 200
    :cond_3
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v5, v0, v6

    if-lez v5, :cond_4

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 202
    :cond_4
    sget-object v5, Lsoftware/simplicial/nebulous/application/bi$4;->a:[I

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/f/ai;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 289
    :cond_5
    :goto_1
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    const-wide/16 v0, 0x14

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    goto :goto_0

    .line 205
    :pswitch_0
    :try_start_2
    new-instance v0, Lsoftware/simplicial/nebulous/application/bi$2;

    invoke-direct {v0, p0, v2, v3}, Lsoftware/simplicial/nebulous/application/bi$2;-><init>(Lsoftware/simplicial/nebulous/application/bi;J)V

    invoke-virtual {v4, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 239
    :pswitch_1
    :try_start_3
    iget-wide v6, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v10

    const-wide v10, 0x3f947ae147ae147bL    # 0.02

    mul-double/2addr v0, v10

    add-double/2addr v0, v6

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    .line 240
    :goto_2
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v0, v0, v6

    if-lez v0, :cond_6

    .line 241
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v0, v6

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    goto :goto_2

    .line 242
    :cond_6
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    invoke-direct {p0, v0, v1, v4}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    .line 244
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_5

    .line 246
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 247
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 248
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->c:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    goto :goto_1

    .line 252
    :pswitch_2
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v6, 0x3fc015bf9217271aL    # 0.12566370614359174

    add-double/2addr v0, v6

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    .line 253
    :goto_3
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v0, v0, v6

    if-lez v0, :cond_7

    .line 254
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v0, v6

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    goto :goto_3

    .line 255
    :cond_7
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    invoke-direct {p0, v0, v1, v4}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    .line 257
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_5

    .line 259
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 260
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 261
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->d:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    .line 263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$ah;)V

    goto/16 :goto_1

    .line 267
    :pswitch_3
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v2, 0x3fc015bf9217271aL    # 0.12566370614359174

    add-double/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    .line 268
    :goto_4
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v0, v0, v2

    if-lez v0, :cond_8

    .line 269
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    goto :goto_4

    .line 270
    :cond_8
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    invoke-direct {p0, v0, v1, v4}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    goto/16 :goto_1

    .line 273
    :pswitch_4
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    sub-long v0, v2, v0

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/bi;->a(J)D

    move-result-wide v0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    .line 274
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->s:D

    invoke-direct {p0, v0, v1, v4}, Lsoftware/simplicial/nebulous/application/bi;->a(DLandroid/app/Activity;)V

    .line 276
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_5

    .line 278
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->p:J

    .line 279
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bi;->q:J

    .line 280
    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)V

    .line 281
    const-string v0, "NONE"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bi;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    new-instance v1, Lsoftware/simplicial/a/a/c;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lsoftware/simplicial/a/a/c;-><init>(II)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40a00000    # 5.0f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/g/a;->a(Lsoftware/simplicial/a/a/y;FFFFFF)V

    .line 285
    :goto_5
    sget-object v0, Lsoftware/simplicial/nebulous/f/ai;->a:Lsoftware/simplicial/nebulous/f/ai;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->o:Lsoftware/simplicial/nebulous/f/ai;

    goto/16 :goto_1

    .line 284
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    new-instance v1, Lsoftware/simplicial/a/a/ab;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lsoftware/simplicial/a/a/ab;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x40a00000    # 5.0f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/g/a;->a(Lsoftware/simplicial/a/a/y;FFFFFF)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    .line 202
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
