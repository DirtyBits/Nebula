.class Lsoftware/simplicial/nebulous/application/e$7$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/e$7;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lsoftware/simplicial/a/bd;

.field final synthetic c:Lsoftware/simplicial/nebulous/application/e$7;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/e$7;Landroid/widget/EditText;Lsoftware/simplicial/a/bd;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->b:Lsoftware/simplicial/a/bd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 366
    :goto_0
    return-void

    .line 350
    :cond_0
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 353
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 362
    :catch_0
    move-exception v0

    .line 364
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 360
    :cond_1
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->c:Lsoftware/simplicial/nebulous/application/e$7;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$7$1;->b:Lsoftware/simplicial/a/bd;

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/f/ag;->a(Lsoftware/simplicial/a/bd;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
