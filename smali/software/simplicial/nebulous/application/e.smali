.class public Lsoftware/simplicial/nebulous/application/e;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/nebulous/f/al$l;
.implements Lsoftware/simplicial/nebulous/f/al$m;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Z

.field private O:I

.field private P:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private R:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;"
        }
    .end annotation
.end field

.field private T:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;"
        }
    .end annotation
.end field

.field private V:Z

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/Button;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/CheckBox;

.field q:Landroid/widget/RelativeLayout;

.field r:Landroid/widget/LinearLayout;

.field s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

.field t:Landroid/widget/GridView;

.field u:Lb/a/a/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb/a/a/c/a",
            "<",
            "Lsoftware/simplicial/nebulous/f/i;",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field private v:Landroid/widget/TextView;

.field private w:Lsoftware/simplicial/nebulous/a/d;

.field private x:J

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-class v0, Lsoftware/simplicial/nebulous/application/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/e;->a:Ljava/lang/String;

    .line 73
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/e;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    .line 96
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/e;->x:J

    .line 97
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->y:I

    .line 98
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->z:I

    .line 99
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->A:I

    .line 100
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->B:I

    .line 101
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->C:I

    .line 102
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->D:I

    .line 103
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->E:I

    .line 104
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->I:I

    .line 105
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->J:I

    .line 106
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->K:I

    .line 107
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->L:I

    .line 108
    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->M:I

    .line 109
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/e;->N:Z

    .line 111
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->P:Ljava/util/Set;

    .line 112
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    .line 114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    .line 116
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/e;->V:Z

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 789
    if-nez p1, :cond_0

    .line 790
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/e;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020432

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 793
    :goto_0
    return-void

    .line 792
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/af;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 489
    iget v0, p1, Lsoftware/simplicial/a/af;->d:I

    if-ltz v0, :cond_0

    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08031f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iget v2, p1, Lsoftware/simplicial/a/af;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 492
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 493
    const v0, 0x7f080321

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 495
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 496
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 497
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 499
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 500
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lsoftware/simplicial/a/af;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 501
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 503
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 505
    const v1, 0x7f08015d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 506
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v1, v2, :cond_2

    .line 507
    const v1, 0x7f080248

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$10;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$10;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 535
    :cond_2
    :goto_0
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 536
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 537
    return-void

    .line 520
    :cond_3
    iget v1, p1, Lsoftware/simplicial/a/af;->d:I

    if-ltz v1, :cond_2

    .line 522
    const v1, 0x7f0800c6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 523
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$11;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/e$11;-><init>(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/af;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/as;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 541
    iget v0, p1, Lsoftware/simplicial/a/as;->d:I

    if-ltz v0, :cond_0

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08031f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iget v2, p1, Lsoftware/simplicial/a/as;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 544
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 545
    const v0, 0x7f080321

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 547
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 548
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 549
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 551
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 552
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lsoftware/simplicial/a/as;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 553
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 555
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 557
    const v1, 0x7f08015d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 558
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v1, v2, :cond_2

    .line 559
    const v1, 0x7f080248

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$12;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$12;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 587
    :cond_2
    :goto_0
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 588
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 589
    return-void

    .line 572
    :cond_3
    iget v1, p1, Lsoftware/simplicial/a/as;->d:I

    if-ltz v1, :cond_2

    .line 574
    const v1, 0x7f0800c6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 575
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$13;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/e$13;-><init>(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/as;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/bd;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 593
    iget v0, p1, Lsoftware/simplicial/a/bd;->d:I

    if-ltz v0, :cond_0

    .line 594
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08031e

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iget v2, p1, Lsoftware/simplicial/a/bd;->d:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 596
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 597
    const v0, 0x7f080321

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 599
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 600
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 601
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 603
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 604
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lsoftware/simplicial/a/bd;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 605
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 607
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 609
    const v1, 0x7f08015d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 610
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v1, v2, :cond_2

    .line 611
    const v1, 0x7f080248

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$2;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 639
    :cond_2
    :goto_0
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 640
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 641
    return-void

    .line 624
    :cond_3
    iget v1, p1, Lsoftware/simplicial/a/bd;->d:I

    if-ltz v1, :cond_2

    .line 626
    const v1, 0x7f0800c6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 627
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$3;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/e$3;-><init>(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/bd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/a/e;)V
    .locals 18

    .prologue
    .line 387
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/e;->P:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lsoftware/simplicial/nebulous/application/e;->x:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/nebulous/application/e;->y:I

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/nebulous/application/e;->z:I

    move-object/from16 v0, p0

    iget v8, v0, Lsoftware/simplicial/nebulous/application/e;->A:I

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/nebulous/application/e;->B:I

    move-object/from16 v0, p0

    iget v10, v0, Lsoftware/simplicial/nebulous/application/e;->C:I

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/application/e;->D:I

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/nebulous/application/e;->E:I

    move-object/from16 v0, p0

    iget v13, v0, Lsoftware/simplicial/nebulous/application/e;->I:I

    move-object/from16 v0, p0

    iget v14, v0, Lsoftware/simplicial/nebulous/application/e;->J:I

    move-object/from16 v0, p0

    iget v15, v0, Lsoftware/simplicial/nebulous/application/e;->K:I

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/nebulous/application/e;->L:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/nebulous/application/e;->M:I

    move/from16 v17, v0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v17}, Lsoftware/simplicial/a/e;->a(Lsoftware/simplicial/a/e;Ljava/util/Set;Ljava/util/Set;IIIIIIIIIIIII)Z

    move-result v2

    .line 390
    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/e;->lt:Lsoftware/simplicial/a/d;

    if-eqz v3, :cond_0

    if-nez v2, :cond_0

    .line 391
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802c0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lsoftware/simplicial/a/e;->lt:Lsoftware/simplicial/a/d;

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/e;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/d/b;->a(Lsoftware/simplicial/a/d;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    .line 433
    :goto_0
    return-void

    .line 392
    :cond_0
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lm:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    if-nez v2, :cond_1

    .line 393
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802c1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lm:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto :goto_0

    .line 394
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ln:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    if-nez v2, :cond_2

    .line 395
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ln:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080397

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto :goto_0

    .line 397
    :cond_2
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lo:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    if-nez v2, :cond_3

    .line 398
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lo:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08038b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 400
    :cond_3
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lp:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    if-nez v2, :cond_4

    .line 401
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080396

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 403
    :cond_4
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lq:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    if-nez v2, :cond_5

    .line 404
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lq:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0803a0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 406
    :cond_5
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lr:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    if-nez v2, :cond_6

    .line 407
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lr:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080356

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 408
    :cond_6
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ls:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_7

    if-nez v2, :cond_7

    .line 409
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ls:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080385

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 410
    :cond_7
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ly:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_8

    if-nez v2, :cond_8

    .line 411
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->ly:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080398

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 412
    :cond_8
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lz:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_9

    if-nez v2, :cond_9

    .line 413
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lz:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080393

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    :cond_9
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lA:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_a

    if-nez v2, :cond_a

    .line 415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lA:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080357

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 416
    :cond_a
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lB:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_b

    if-nez v2, :cond_b

    .line 417
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lB:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0803a3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 418
    :cond_b
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lC:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_c

    if-nez v2, :cond_c

    .line 419
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lC:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080390

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 420
    :cond_c
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lD:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_d

    if-nez v2, :cond_d

    .line 421
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0802bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lD:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080395

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 422
    :cond_d
    move-object/from16 v0, p1

    iget v3, v0, Lsoftware/simplicial/a/e;->lG:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_e

    if-nez v2, :cond_e

    .line 423
    const-string v2, ""

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 426
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/f/e;->a(Lsoftware/simplicial/a/e;)V

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v3, :cond_f

    .line 428
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v0, p1

    iput-object v0, v2, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    .line 431
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 430
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v0, p1

    iput-object v0, v2, Lsoftware/simplicial/nebulous/f/ag;->m:Lsoftware/simplicial/a/e;

    goto :goto_1
.end method

.method private a(Lsoftware/simplicial/a/e;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 437
    iget v0, p1, Lsoftware/simplicial/a/e;->lG:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 438
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_0

    .line 439
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f08031f

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 441
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 442
    const v1, 0x7f080321

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 444
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 445
    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 446
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 448
    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 449
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 450
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 452
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 454
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    const v0, 0x7f0801c9

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 455
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v2, :cond_2

    .line 456
    const v0, 0x7f080248

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$8;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$8;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 483
    :cond_2
    :goto_1
    const v0, 0x7f08005d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 484
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 485
    return-void

    .line 454
    :cond_3
    const v0, 0x7f08015d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 468
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_2

    .line 470
    const v0, 0x7f0800c6

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 471
    const v0, 0x7f0801e0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$9;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/e$9;-><init>(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/e;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/e;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/e;->c()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/af;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/af;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/as;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/as;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/bd;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/bd;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/e;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/e;)V

    return-void
.end method

.method private a(Lsoftware/simplicial/nebulous/f/i;)V
    .locals 3

    .prologue
    .line 645
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/d;->a(Lsoftware/simplicial/nebulous/f/i;)V

    .line 646
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    invoke-virtual {v0}, Lb/a/a/c/a;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 647
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 649
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    invoke-virtual {v0, p1}, Lb/a/a/c/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f020235

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 650
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 21

    .prologue
    .line 774
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->q:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/application/e;->N:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 775
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->r:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/application/e;->N:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 777
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lsoftware/simplicial/nebulous/application/e;->x:J

    invoke-static {v4, v5}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/e;->P:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v5, v0, Lsoftware/simplicial/nebulous/application/e;->y:I

    move-object/from16 v0, p0

    iget v6, v0, Lsoftware/simplicial/nebulous/application/e;->z:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v9, v0, Lsoftware/simplicial/nebulous/application/e;->A:I

    move-object/from16 v0, p0

    iget v10, v0, Lsoftware/simplicial/nebulous/application/e;->B:I

    move-object/from16 v0, p0

    iget v11, v0, Lsoftware/simplicial/nebulous/application/e;->C:I

    move-object/from16 v0, p0

    iget v12, v0, Lsoftware/simplicial/nebulous/application/e;->D:I

    move-object/from16 v0, p0

    iget v13, v0, Lsoftware/simplicial/nebulous/application/e;->E:I

    move-object/from16 v0, p0

    iget v14, v0, Lsoftware/simplicial/nebulous/application/e;->I:I

    move-object/from16 v0, p0

    iget v15, v0, Lsoftware/simplicial/nebulous/application/e;->J:I

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/nebulous/application/e;->K:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/nebulous/application/e;->L:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lsoftware/simplicial/nebulous/application/e;->M:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    move-object/from16 v20, v0

    invoke-virtual/range {v2 .. v20}, Lsoftware/simplicial/nebulous/a/d;->a(ILjava/util/Set;IILjava/util/Set;Ljava/util/Set;IIIIIIIIIILjava/util/Map;Ljava/util/Set;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3, v2}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setVisibility(I)V

    .line 781
    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lsoftware/simplicial/nebulous/application/e;->V:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    const/16 v4, 0xff

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    :goto_3
    iput v2, v3, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 783
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v2, :cond_0

    .line 784
    sget-object v2, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Landroid/graphics/Bitmap;)V

    .line 785
    :cond_0
    return-void

    .line 774
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 775
    :cond_2
    const/4 v2, 0x4

    goto/16 :goto_1

    .line 780
    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    .line 781
    :cond_4
    const/16 v2, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    goto :goto_3
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 931
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 925
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 937
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 895
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 913
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 901
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/be;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 942
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 980
    :goto_0
    return-void

    .line 945
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/be;

    .line 947
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "AVATAR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 949
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    if-ltz v2, :cond_1

    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 950
    sget-object v2, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    iget v3, v0, Lsoftware/simplicial/a/be;->b:I

    aget-object v2, v2, v3

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, v2, Lsoftware/simplicial/a/e;->lG:I

    goto :goto_1

    .line 952
    :cond_2
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "EJECT_SKIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 954
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    if-ltz v2, :cond_1

    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    sget-object v3, Lsoftware/simplicial/a/af;->b:[Lsoftware/simplicial/a/af;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 955
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    invoke-static {v2}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, v2, Lsoftware/simplicial/a/af;->d:I

    goto :goto_1

    .line 957
    :cond_3
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "HAT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 959
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    if-ltz v2, :cond_1

    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    sget-object v3, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 960
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    invoke-static {v2}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, v2, Lsoftware/simplicial/a/as;->d:I

    goto :goto_1

    .line 962
    :cond_4
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "PET"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 964
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    if-ltz v2, :cond_1

    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    sget-object v3, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 965
    iget v2, v0, Lsoftware/simplicial/a/be;->b:I

    invoke-static {v2}, Lsoftware/simplicial/a/bd;->a(I)Lsoftware/simplicial/a/bd;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, v2, Lsoftware/simplicial/a/bd;->d:I

    goto/16 :goto_1

    .line 967
    :cond_5
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "SKIN_MAP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 969
    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, p0, Lsoftware/simplicial/nebulous/application/e;->O:I

    goto/16 :goto_1

    .line 972
    :cond_6
    invoke-static {}, Lsoftware/simplicial/nebulous/f/i;->a()V

    .line 973
    invoke-static {}, Lsoftware/simplicial/nebulous/f/i;->b()V

    .line 974
    invoke-static {}, Lsoftware/simplicial/nebulous/f/i;->c()V

    .line 975
    invoke-static {}, Lsoftware/simplicial/nebulous/f/i;->d()V

    .line 977
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/d;->a()V

    .line 978
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->v:Landroid/widget/TextView;

    const v1, 0x7f08006d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 979
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 919
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 813
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    iget-object v2, p1, Lsoftware/simplicial/a/az;->a:Lsoftware/simplicial/a/am;

    if-eqz v2, :cond_1

    .line 879
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    if-eqz p11, :cond_3

    .line 818
    if-nez p9, :cond_2

    .line 819
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    .line 820
    :cond_2
    iput-boolean p9, p0, Lsoftware/simplicial/nebulous/application/e;->N:Z

    .line 823
    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/ag;->t:I

    if-eqz v2, :cond_4

    if-eqz p12, :cond_4

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/ag;->t:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/e;->V:Z

    .line 825
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v2, v3, :cond_5

    .line 827
    iget-wide v2, p1, Lsoftware/simplicial/a/az;->b:J

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/e;->x:J

    .line 828
    iget v2, p1, Lsoftware/simplicial/a/az;->d:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->y:I

    .line 829
    iget v2, p1, Lsoftware/simplicial/a/az;->e:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->z:I

    .line 830
    iget v2, p1, Lsoftware/simplicial/a/az;->f:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->A:I

    .line 831
    iget v2, p1, Lsoftware/simplicial/a/az;->g:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->B:I

    .line 832
    iget v2, p1, Lsoftware/simplicial/a/az;->h:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->C:I

    .line 833
    iget v2, p1, Lsoftware/simplicial/a/az;->i:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->D:I

    .line 834
    iget v2, p1, Lsoftware/simplicial/a/az;->j:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->E:I

    .line 835
    iget v2, p1, Lsoftware/simplicial/a/az;->k:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->I:I

    .line 836
    iget v2, p1, Lsoftware/simplicial/a/az;->l:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->J:I

    .line 837
    iget v2, p1, Lsoftware/simplicial/a/az;->m:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->K:I

    .line 838
    iget v2, p1, Lsoftware/simplicial/a/az;->n:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->L:I

    .line 839
    iget v2, p1, Lsoftware/simplicial/a/az;->o:I

    iput v2, p0, Lsoftware/simplicial/nebulous/application/e;->M:I

    .line 840
    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p1, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->P:Ljava/util/Set;

    .line 841
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    .line 842
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p10

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    .line 843
    new-instance v2, Ljava/util/HashMap;

    move-object/from16 v0, p13

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    .line 844
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p14

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    .line 845
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/e;->c()V

    goto/16 :goto_0

    .line 823
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 850
    :cond_5
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/e;

    .line 851
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 852
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v4, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/e;)Z

    goto :goto_2

    .line 853
    :cond_7
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    invoke-interface {v2, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 854
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->t:Ljava/util/Set;

    invoke-interface {v2, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 857
    invoke-interface/range {p10 .. p10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/af;

    .line 858
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 859
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v4, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/af;)Z

    goto :goto_3

    .line 860
    :cond_9
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    move-object/from16 v0, p10

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 861
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->u:Ljava/util/Set;

    move-object/from16 v0, p10

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 864
    invoke-interface/range {p14 .. p14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/as;

    .line 865
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 866
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v4, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/as;)Z

    goto :goto_4

    .line 867
    :cond_b
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    move-object/from16 v0, p14

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 868
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->v:Ljava/util/Set;

    move-object/from16 v0, p14

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 871
    invoke-interface/range {p13 .. p13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_c
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/bd;

    .line 872
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    iget-byte v5, v2, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 873
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v4, v2}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/bd;)Z

    goto :goto_5

    .line 874
    :cond_d
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    move-object/from16 v0, p13

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 875
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->w:Ljava/util/Map;

    move-object/from16 v0, p13

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 877
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/e;->c()V

    goto/16 :goto_0
.end method

.method public a(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 4

    .prologue
    .line 985
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->n:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/e;->O:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 987
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 988
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 807
    return-void
.end method

.method public a(ZLjava/lang/String;I)V
    .locals 3

    .prologue
    .line 993
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1021
    :cond_0
    :goto_0
    return-void

    .line 996
    :cond_1
    if-eqz p1, :cond_0

    .line 998
    const-string v0, "AVATAR"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 999
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    sget-object v1, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    aget-object v1, v1, p3

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1001
    :cond_2
    const-string v0, "EJECT_SKIN"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1002
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    invoke-static {p3}, Lsoftware/simplicial/a/af;->a(I)Lsoftware/simplicial/a/af;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1004
    :cond_3
    const-string v0, "HAT"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1005
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    invoke-static {p3}, Lsoftware/simplicial/a/as;->a(I)Lsoftware/simplicial/a/as;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1007
    :cond_4
    const-string v0, "PET"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1009
    const-wide/16 v0, 0x1

    invoke-static {p3, v0, v1}, Lsoftware/simplicial/a/bd;->a(IJ)Lsoftware/simplicial/a/bd;

    move-result-object v0

    .line 1010
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    iget-byte v2, v0, Lsoftware/simplicial/a/bd;->c:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1015
    :cond_5
    const-string v0, "SKIN_MAP"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1016
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/e;->N:Z

    .line 1018
    :cond_6
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/e;->c()V

    .line 1019
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 907
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 655
    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 657
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    move-object v0, p1

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v1, v0}, Lb/a/a/c/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/i;

    .line 658
    if-eqz v0, :cond_0

    .line 660
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/f/i;)V

    .line 664
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 666
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 667
    const v0, 0x1080027

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 668
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_3

    const v0, 0x7f0801c9

    :goto_0
    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080383

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0800d3

    .line 670
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/nebulous/application/e;->O:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 669
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 672
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 674
    const v0, 0x7f0801e0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$4;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 701
    :cond_1
    :goto_1
    const v0, 0x7f08005d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 702
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 708
    :cond_2
    :goto_2
    return-void

    .line 668
    :cond_3
    const v0, 0x7f0800c6

    goto/16 :goto_0

    .line 686
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v2, :cond_1

    .line 688
    const v0, 0x7f080248

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/e$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/e$5;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 704
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    if-ne p1, v0, :cond_2

    .line 706
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->M:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 121
    const v0, 0x7f04002a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 123
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 125
    const v0, 0x7f0d00c5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->c:Landroid/widget/Button;

    .line 126
    const v0, 0x7f0d00c6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->d:Landroid/widget/Button;

    .line 127
    const v0, 0x7f0d00cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->e:Landroid/widget/Button;

    .line 128
    const v0, 0x7f0d00c7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->f:Landroid/widget/Button;

    .line 129
    const v0, 0x7f0d00c8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->g:Landroid/widget/Button;

    .line 130
    const v0, 0x7f0d00cc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->h:Landroid/widget/Button;

    .line 131
    const v0, 0x7f0d00ca

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->i:Landroid/widget/Button;

    .line 132
    const v0, 0x7f0d00c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->j:Landroid/widget/Button;

    .line 133
    const v0, 0x7f0d00ce

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->k:Landroid/widget/Button;

    .line 134
    const v0, 0x7f0d00cf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->l:Landroid/widget/Button;

    .line 135
    const v0, 0x7f0d00cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->m:Landroid/widget/Button;

    .line 136
    const v0, 0x7f0d00d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->t:Landroid/widget/GridView;

    .line 137
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->v:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f0d00d1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->o:Landroid/widget/Button;

    .line 139
    const v0, 0x7f0d00d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->p:Landroid/widget/CheckBox;

    .line 140
    const v0, 0x7f0d00d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->q:Landroid/widget/RelativeLayout;

    .line 141
    const v0, 0x7f0d00d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->r:Landroid/widget/LinearLayout;

    .line 142
    const v0, 0x7f0d00d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->n:Landroid/widget/TextView;

    .line 143
    const v0, 0x7f0d00d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    .line 145
    new-instance v0, Lb/a/a/c/a;

    invoke-direct {v0}, Lb/a/a/c/a;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->c:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->f:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->g:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->b:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->e:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->d:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->a:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->c:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->k:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->d:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->h:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->i:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->j:Lsoftware/simplicial/nebulous/f/i;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e;->m:Landroid/widget/Button;

    invoke-virtual {v0, v2, v3}, Lb/a/a/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 158
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/e;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x108003f

    invoke-static {v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/e;->a(Landroid/graphics/Bitmap;)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->q:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 798
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 800
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 801
    return-void
.end method

.method public onResume()V
    .locals 18

    .prologue
    .line 713
    invoke-super/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 714
    sget-object v2, Lsoftware/simplicial/nebulous/f/i;->a:Lsoftware/simplicial/nebulous/f/i;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/f/i;)V

    .line 716
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 718
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->n:Landroid/widget/TextView;

    const-string v3, "---"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v3, :cond_1

    .line 722
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget-wide v2, v2, Lsoftware/simplicial/a/az;->b:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lsoftware/simplicial/nebulous/application/e;->x:J

    .line 723
    new-instance v3, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget-object v2, v2, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->P:Ljava/util/Set;

    .line 724
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->y:I

    .line 725
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->e:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->z:I

    .line 726
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->f:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->A:I

    .line 727
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->g:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->B:I

    .line 728
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->h:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->C:I

    .line 729
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->i:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->D:I

    .line 730
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->j:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->E:I

    .line 731
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->k:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->I:I

    .line 732
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->l:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->J:I

    .line 733
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->m:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->K:I

    .line 734
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->n:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->L:I

    .line 735
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsoftware/simplicial/a/az;

    iget v2, v2, Lsoftware/simplicial/a/az;->o:I

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/application/e;->M:I

    .line 736
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag$a;->t:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    .line 737
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag$a;->u:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    .line 738
    new-instance v2, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag$a;->v:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    .line 739
    new-instance v2, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag$a;->w:Ljava/util/Map;

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    .line 740
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsoftware/simplicial/a/az;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    move-object/from16 v17, v0

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v17}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V

    .line 741
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/b;)V

    .line 753
    :goto_0
    sget-object v3, Lsoftware/simplicial/a/e;->a:[Lsoftware/simplicial/a/e;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 754
    iget v6, v5, Lsoftware/simplicial/a/e;->lG:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 755
    const/4 v6, -0x2

    iput v6, v5, Lsoftware/simplicial/a/e;->lG:I

    .line 753
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 745
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->v:Landroid/widget/TextView;

    const v3, 0x7f0800c7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 746
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->Q:Ljava/util/Set;

    .line 747
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->R:Ljava/util/Set;

    .line 748
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->T:Ljava/util/Map;

    .line 749
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->S:Ljava/util/Set;

    .line 750
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/am;ILsoftware/simplicial/a/b;)V

    goto :goto_0

    .line 757
    :cond_2
    sget-object v3, Lsoftware/simplicial/a/af;->b:[Lsoftware/simplicial/a/af;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 758
    iget v6, v5, Lsoftware/simplicial/a/af;->d:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    .line 759
    const/4 v6, -0x2

    iput v6, v5, Lsoftware/simplicial/a/af;->d:I

    .line 757
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 761
    :cond_4
    sget-object v3, Lsoftware/simplicial/a/as;->b:[Lsoftware/simplicial/a/as;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 762
    iget v6, v5, Lsoftware/simplicial/a/as;->d:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    .line 763
    const/4 v6, -0x2

    iput v6, v5, Lsoftware/simplicial/a/as;->d:I

    .line 761
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 765
    :cond_6
    sget-object v3, Lsoftware/simplicial/a/bd;->b:[Lsoftware/simplicial/a/bd;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v4, :cond_8

    aget-object v5, v3, v2

    .line 766
    iget v6, v5, Lsoftware/simplicial/a/bd;->d:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_7

    .line 767
    const/4 v6, -0x2

    iput v6, v5, Lsoftware/simplicial/a/bd;->d:I

    .line 765
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 769
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v0}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$m;)V

    .line 770
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 171
    new-instance v0, Lsoftware/simplicial/nebulous/a/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/d;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    .line 173
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->u:Lb/a/a/c/a;

    invoke-virtual {v0}, Lb/a/a/c/a;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 174
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 176
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->s:Lsoftware/simplicial/nebulous/views/CustomAvatarView;

    const/16 v1, 0xff

    invoke-static {v1, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    iput v1, v0, Lsoftware/simplicial/nebulous/views/CustomAvatarView;->a:I

    .line 180
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->p:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->p:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/e$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/e$1;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->t:Landroid/widget/GridView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->t:Landroid/widget/GridView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/e$6;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/e$6;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 314
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e;->t:Landroid/widget/GridView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/e$7;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/e$7;-><init>(Lsoftware/simplicial/nebulous/application/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 383
    return-void
.end method

.method public p_()V
    .locals 21

    .prologue
    .line 884
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v2, :cond_0

    .line 889
    :goto_0
    return-void

    .line 887
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/e;->w:Lsoftware/simplicial/nebulous/a/d;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v3

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    new-instance v20, Ljava/util/HashSet;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {v2 .. v20}, Lsoftware/simplicial/nebulous/a/d;->a(ILjava/util/Set;IILjava/util/Set;Ljava/util/Set;IIIIIIIIIILjava/util/Map;Ljava/util/Set;)V

    goto :goto_0
.end method
