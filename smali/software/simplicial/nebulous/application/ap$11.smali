.class Lsoftware/simplicial/nebulous/application/ap$11;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ap;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/ap;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ap;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3

    .prologue
    .line 361
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 365
    :goto_0
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 379
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0xa

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    .line 380
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap$11;->a:Lsoftware/simplicial/nebulous/application/ap;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->i()V

    goto :goto_0
.end method
