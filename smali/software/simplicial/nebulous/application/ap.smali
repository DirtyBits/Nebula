.class public Lsoftware/simplicial/nebulous/application/ap;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Z


# instance fields
.field A:Landroid/widget/CheckBox;

.field B:Landroid/widget/CheckBox;

.field C:Landroid/widget/CheckBox;

.field D:Landroid/widget/CheckBox;

.field E:Landroid/widget/CheckBox;

.field F:Landroid/widget/ImageButton;

.field G:Landroid/widget/Spinner;

.field private H:[Ljava/lang/String;

.field c:Landroid/widget/SeekBar;

.field d:Landroid/widget/SeekBar;

.field e:Landroid/widget/SeekBar;

.field f:Landroid/widget/SeekBar;

.field g:Landroid/widget/Spinner;

.field h:Landroid/widget/Spinner;

.field i:Landroid/widget/Spinner;

.field j:Landroid/widget/Spinner;

.field k:Landroid/widget/SeekBar;

.field l:Landroid/widget/SeekBar;

.field m:Landroid/widget/ImageButton;

.field n:Landroid/widget/ImageButton;

.field o:Landroid/widget/ImageButton;

.field p:Landroid/widget/CheckBox;

.field q:Landroid/widget/CheckBox;

.field r:Landroid/widget/CheckBox;

.field s:Landroid/widget/CheckBox;

.field t:Landroid/widget/CheckBox;

.field u:Landroid/widget/CheckBox;

.field v:Landroid/widget/CheckBox;

.field w:Landroid/widget/CheckBox;

.field x:Landroid/widget/CheckBox;

.field y:Landroid/widget/CheckBox;

.field z:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lsoftware/simplicial/nebulous/application/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ap;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 68
    const/16 v0, 0x2f

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "en"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "af"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ar"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ar_IQ"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "bg"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "bn"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "bs"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ca"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "cs"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "da"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "el"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "et"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "fi"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "hi"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "hr"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ja"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ko"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "lt"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "lv"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "mk"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "mr"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "ms"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "nl"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "pt_BR"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "pt_PT"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "si"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "sk"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "sq"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "sr"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "sv"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "th"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "tl"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "uk"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "vi"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "zh_CN"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "zh_TW"

    aput-object v2, v0, v1

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->H:[Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 549
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->m:Landroid/widget/ImageButton;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020069

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 550
    return-void

    .line 549
    :cond_0
    const v0, 0x7f020068

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ap;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->H:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->x:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_1

    .line 557
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->x:Z

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 559
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->y:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_2

    .line 561
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->E:Z

    goto :goto_0

    .line 563
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->C:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_3

    .line 565
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->ab:Z

    goto :goto_0

    .line 567
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->z:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_4

    .line 569
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->ac:Z

    goto :goto_0

    .line 571
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->A:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_5

    .line 573
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    goto :goto_0

    .line 575
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->B:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_6

    .line 577
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->ah:Z

    goto :goto_0

    .line 579
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->q:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_7

    .line 581
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    goto :goto_0

    .line 583
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->r:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_8

    .line 585
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->S:Z

    goto :goto_0

    .line 587
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->p:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_9

    .line 589
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->Q:Z

    goto :goto_0

    .line 591
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->s:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_a

    .line 593
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->T:Z

    .line 595
    if-nez p2, :cond_0

    .line 596
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/p;->a()V

    goto :goto_0

    .line 599
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->t:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_b

    .line 601
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->U:Z

    goto :goto_0

    .line 603
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->D:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_c

    .line 605
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->au:Z

    goto/16 :goto_0

    .line 607
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->E:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_d

    .line 609
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->av:Z

    goto/16 :goto_0

    .line 611
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->u:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_e

    .line 613
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->V:Z

    goto/16 :goto_0

    .line 615
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->v:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_f

    .line 617
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    goto/16 :goto_0

    .line 619
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->w:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->X:Z

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v6, 0x28

    const/16 v5, 0x14

    const/16 v4, 0x32

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 450
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->n:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_0

    .line 452
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 454
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->F:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_2

    .line 456
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getOnItemSelectedListener()Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v2

    if-nez v2, :cond_1

    .line 458
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    new-instance v3, Lsoftware/simplicial/nebulous/application/ap$3;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ap$3;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 496
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->performClick()Z

    .line 498
    :cond_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->o:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_3

    .line 500
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->p:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 501
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->q:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 502
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->r:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 503
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->s:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 504
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->t:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 505
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->u:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 506
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->v:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 507
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->w:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 508
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->x:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 509
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->y:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 510
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->C:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 511
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->z:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 512
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->A:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 513
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->B:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 514
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->D:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 515
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->E:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 516
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    sget-object v3, Lsoftware/simplicial/nebulous/c/b;->a:Lsoftware/simplicial/nebulous/c/b;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/b;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 517
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    sget-object v3, Lsoftware/simplicial/nebulous/c/a;->a:Lsoftware/simplicial/nebulous/c/a;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/a;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 518
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    sget-object v3, Lsoftware/simplicial/nebulous/c/e;->a:Lsoftware/simplicial/nebulous/c/e;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/c/e;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 519
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    sget-object v3, Lsoftware/simplicial/nebulous/f/aj;->a:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/aj;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 520
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->c:Landroid/widget/SeekBar;

    invoke-virtual {v2, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 521
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->f:Landroid/widget/SeekBar;

    invoke-virtual {v2, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 522
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->d:Landroid/widget/SeekBar;

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 523
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->e:Landroid/widget/SeekBar;

    invoke-virtual {v2, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 524
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->k:Landroid/widget/SeekBar;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->k:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 525
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->l:Landroid/widget/SeekBar;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->l:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 526
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v4, v2, Lsoftware/simplicial/nebulous/f/ag;->y:I

    .line 527
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v6, v2, Lsoftware/simplicial/nebulous/f/ag;->z:I

    .line 528
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v4, v2, Lsoftware/simplicial/nebulous/f/ag;->A:I

    .line 529
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v5, v2, Lsoftware/simplicial/nebulous/f/ag;->B:I

    .line 530
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean v0, v2, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    .line 531
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ap;->a()V

    .line 532
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    .line 533
    if-eqz v2, :cond_3

    .line 535
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v3, v3, Lsoftware/simplicial/nebulous/f/ag;->y:I

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/c/d;->a(I)V

    .line 536
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v3, v3, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/c/d;->b(I)V

    .line 537
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/c/d;->c()V

    .line 540
    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->m:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_4

    .line 542
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    if-nez v3, :cond_5

    :goto_0
    iput-boolean v0, v2, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    .line 543
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ap;->a()V

    .line 545
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 542
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    const v0, 0x7f04004e

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 76
    const v0, 0x7f0d021a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->c:Landroid/widget/SeekBar;

    .line 77
    const v0, 0x7f0d021c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->k:Landroid/widget/SeekBar;

    .line 78
    const v0, 0x7f0d021d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->l:Landroid/widget/SeekBar;

    .line 79
    const v0, 0x7f0d021b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->d:Landroid/widget/SeekBar;

    .line 80
    const v0, 0x7f0d0219

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->e:Landroid/widget/SeekBar;

    .line 81
    const v0, 0x7f0d0218

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->f:Landroid/widget/SeekBar;

    .line 82
    const v0, 0x7f0d021e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    .line 83
    const v0, 0x7f0d021f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    .line 84
    const v0, 0x7f0d0220

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    .line 85
    const v0, 0x7f0d0221

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    .line 86
    const v0, 0x7f0d0232

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->m:Landroid/widget/ImageButton;

    .line 87
    const v0, 0x7f0d0235

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->n:Landroid/widget/ImageButton;

    .line 88
    const v0, 0x7f0d0233

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->o:Landroid/widget/ImageButton;

    .line 89
    const v0, 0x7f0d0222

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->p:Landroid/widget/CheckBox;

    .line 90
    const v0, 0x7f0d0224

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->q:Landroid/widget/CheckBox;

    .line 91
    const v0, 0x7f0d0223

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->r:Landroid/widget/CheckBox;

    .line 92
    const v0, 0x7f0d0225

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->s:Landroid/widget/CheckBox;

    .line 93
    const v0, 0x7f0d0226

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->t:Landroid/widget/CheckBox;

    .line 94
    const v0, 0x7f0d0227

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->u:Landroid/widget/CheckBox;

    .line 95
    const v0, 0x7f0d0228

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->v:Landroid/widget/CheckBox;

    .line 96
    const v0, 0x7f0d0229

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->w:Landroid/widget/CheckBox;

    .line 97
    const v0, 0x7f0d022a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->x:Landroid/widget/CheckBox;

    .line 98
    const v0, 0x7f0d022b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->y:Landroid/widget/CheckBox;

    .line 99
    const v0, 0x7f0d022c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->z:Landroid/widget/CheckBox;

    .line 100
    const v0, 0x7f0d022e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->A:Landroid/widget/CheckBox;

    .line 101
    const v0, 0x7f0d0231

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->B:Landroid/widget/CheckBox;

    .line 102
    const v0, 0x7f0d022d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->C:Landroid/widget/CheckBox;

    .line 103
    const v0, 0x7f0d022f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->D:Landroid/widget/CheckBox;

    .line 104
    const v0, 0x7f0d0230

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->E:Landroid/widget/CheckBox;

    .line 105
    const v0, 0x7f0d0234

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->F:Landroid/widget/ImageButton;

    .line 106
    const v0, 0x7f0d0236

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    .line 107
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 108
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f04009e

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->G:Landroid/widget/Spinner;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 113
    return-object v2
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 635
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 636
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/nebulous/application/ap;->b:Z

    .line 637
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 628
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 629
    const/4 v0, 0x1

    sput-boolean v0, Lsoftware/simplicial/nebulous/application/ap;->b:Z

    .line 630
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/high16 v7, 0x42c80000    # 100.0f

    const/high16 v6, 0x41200000    # 10.0f

    const v5, 0x7f04009e

    const/4 v2, 0x0

    .line 119
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 121
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ap;->a()V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->F:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->o:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->m:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->bringToFront()V

    .line 130
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 131
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 133
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 134
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/c/b;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 135
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->g:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$1;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 158
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 159
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 161
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->i:Lsoftware/simplicial/nebulous/c/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/c/a;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->h:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$4;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 184
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 185
    :goto_2
    const/4 v0, 0x2

    if-ge v1, v0, :cond_2

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 187
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/c/e;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->i:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$5;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 210
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 211
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 213
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 214
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->w:Lsoftware/simplicial/nebulous/f/aj;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/aj;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->j:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$6;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$6;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->c:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->c:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/ag;->y:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->c:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$7;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$7;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->d:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 264
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->d:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$8;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$8;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 295
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->e:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->B:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 296
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->e:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$9;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$9;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 325
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->f:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->A:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 326
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->f:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$10;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$10;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->k:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ao:F

    mul-float/2addr v1, v7

    sub-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->k:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$11;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$11;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->l:Landroid/widget/SeekBar;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ap:F

    mul-float/2addr v1, v7

    sub-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 385
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->l:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ap$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ap$2;-><init>(Lsoftware/simplicial/nebulous/application/ap;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 413
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->p:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Q:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 414
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 415
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->r:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->S:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 416
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->r:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 417
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->q:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 418
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 419
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->s:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->T:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 420
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 421
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->t:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->U:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 422
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->t:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 423
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->u:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->V:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 424
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->u:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 425
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->v:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 426
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->v:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 427
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->w:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->X:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 428
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->w:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 429
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->x:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->x:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 430
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->x:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 431
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->y:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->E:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 432
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->y:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 433
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->C:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ab:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 434
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 435
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->z:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ac:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 436
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 437
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->A:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ad:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 438
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 439
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->B:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ah:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 440
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->B:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 441
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->D:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->au:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 442
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 443
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->E:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ap;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->av:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 444
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ap;->E:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 445
    return-void
.end method
