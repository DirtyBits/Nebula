.class public Lsoftware/simplicial/nebulous/application/k;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/al$w;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lsoftware/simplicial/nebulous/application/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 179
    if-eqz p2, :cond_0

    .line 181
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 183
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 184
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 185
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 186
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 187
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0802ee

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f080387

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f0801cf

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lsoftware/simplicial/nebulous/application/k$1;

    invoke-direct {v4, p0, p1, v1}, Lsoftware/simplicial/nebulous/application/k$1;-><init>(Lsoftware/simplicial/nebulous/application/k;Ljava/lang/String;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 206
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f08005d

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 208
    new-instance v3, Landroid/widget/LinearLayout;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 209
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 210
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 211
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 212
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 214
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lsoftware/simplicial/nebulous/f/v;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/ax;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 225
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 228
    :cond_0
    if-eqz p1, :cond_7

    .line 230
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ax;

    .line 232
    iget-object v2, v0, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    const-string v3, "1000_coins"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 233
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->l:Landroid/widget/TextView;

    iget-object v3, v0, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_2
    iget-object v2, v0, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    const-string v3, "2500_coins"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 235
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->k:Landroid/widget/TextView;

    iget-object v3, v0, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    :cond_3
    iget-object v2, v0, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    const-string v3, "10000_coins"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 237
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->j:Landroid/widget/TextView;

    iget-object v3, v0, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    :cond_4
    iget-object v2, v0, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    const-string v3, "30000_coins"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 239
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->i:Landroid/widget/TextView;

    iget-object v3, v0, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :cond_5
    iget-object v2, v0, Lsoftware/simplicial/a/ax;->a:Ljava/lang/String;

    const-string v3, "100000_coins"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 241
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->h:Landroid/widget/TextView;

    iget-object v0, v0, Lsoftware/simplicial/a/ax;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 244
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 245
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 247
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 248
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 250
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 254
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080102

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 157
    const-string v0, "100000_coins"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/k;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 161
    const-string v0, "30000_coins"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/k;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 163
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 165
    const-string v0, "10000_coins"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/k;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 167
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 169
    const-string v0, "2500_coins"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/k;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 171
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 173
    const-string v0, "1000_coins"

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/k;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 56
    const v0, 0x7f04002f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f0d00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->g:Landroid/widget/Button;

    .line 61
    const v0, 0x7f0d00f7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0d00f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0d00fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    .line 64
    const v0, 0x7f0d00fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    .line 65
    const v0, 0x7f0d0100

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0d00f8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->h:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0d00fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->i:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0d00fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->j:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0d00ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->k:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0d0101

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->l:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0d00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->n:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0d0102

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->o:Landroid/widget/CheckBox;

    .line 76
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 146
    return-void
.end method

.method public onResume()V
    .locals 10

    .prologue
    const/4 v3, 0x3

    const v9, 0x7f0c00fb

    const v8, 0x7f0c003f

    const v7, 0x7f0801e6

    const/4 v6, 0x0

    .line 95
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 97
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    .line 99
    if-eqz v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v2, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v3, v3, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 102
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->n:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0800fa

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f080114

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v3, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 111
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v3, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 112
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v3, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 113
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v3, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 114
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v3, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_5
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 116
    if-eqz v1, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v1, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    .line 118
    :goto_6
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    const v4, 0x186a0

    mul-int/2addr v4, v0

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v7}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    mul-int/lit16 v4, v0, 0x7530

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v7}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    mul-int/lit16 v4, v0, 0x2710

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v7}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    mul-int/lit16 v4, v0, 0x9c4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v7}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v7}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 128
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 131
    const-string v1, "1000_coins"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    const-string v1, "2500_coins"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v1, "10000_coins"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v1, "30000_coins"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    const-string v1, "100000_coins"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->m:Landroid/widget/TextView;

    const v2, 0x7f08017d

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/k;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/k;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v1, v0, p0}, Lsoftware/simplicial/nebulous/f/v;->a(Ljava/util/List;Lsoftware/simplicial/nebulous/f/al$w;)V

    .line 140
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->n:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 110
    :cond_1
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_1

    .line 111
    :cond_2
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_2

    .line 112
    :cond_3
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_3

    .line 113
    :cond_4
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_4

    .line 114
    :cond_5
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/k;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto/16 :goto_5

    .line 116
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_6
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/k;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void
.end method
