.class Lsoftware/simplicial/nebulous/application/w$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/w;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/w;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/w;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    if-nez v0, :cond_1

    .line 282
    :cond_0
    :goto_0
    return v2

    .line 247
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/w;->g(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 250
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 251
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 252
    if-eqz v0, :cond_3

    .line 254
    invoke-virtual {v0, v4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v4

    .line 255
    int-to-float v3, v3

    invoke-virtual {v0, v4, v3}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v3

    .line 257
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/w;->d(Lsoftware/simplicial/nebulous/application/w;)[Z

    move-result-object v0

    array-length v0, v0

    if-ge v3, v0, :cond_3

    .line 262
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-le v0, v1, :cond_4

    move v0, v1

    .line 268
    :goto_1
    if-nez v0, :cond_5

    .line 270
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/w;->d(Lsoftware/simplicial/nebulous/application/w;)[Z

    move-result-object v0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/application/w;->d(Lsoftware/simplicial/nebulous/application/w;)[Z

    move-result-object v4

    aget-boolean v4, v4, v3

    if-nez v4, :cond_2

    move v2, v1

    :cond_2
    aput-boolean v2, v0, v3

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/w;->a()V

    :cond_3
    :goto_2
    move v2, v1

    .line 279
    goto :goto_0

    :cond_4
    move v0, v2

    .line 262
    goto :goto_1

    .line 264
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_1

    .line 275
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/w$6;->a:Lsoftware/simplicial/nebulous/application/w;

    const v4, 0x7f080084

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method
