.class Lsoftware/simplicial/nebulous/application/bi$2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/bi;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Lsoftware/simplicial/nebulous/application/bi;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/bi;J)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iput-wide p2, p0, Lsoftware/simplicial/nebulous/application/bi$2;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->a(Lsoftware/simplicial/nebulous/application/bi;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 212
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->b(Lsoftware/simplicial/nebulous/application/bi;)Lsoftware/simplicial/nebulous/f/ai;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/nebulous/f/ai;->a:Lsoftware/simplicial/nebulous/f/ai;

    if-eq v0, v2, :cond_1

    .line 213
    :cond_0
    monitor-exit v1

    .line 235
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->f:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 217
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->c(Lsoftware/simplicial/nebulous/application/bi;)J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-eqz v0, :cond_2

    iget-wide v2, p0, Lsoftware/simplicial/nebulous/application/bi$2;->a:J

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->c(Lsoftware/simplicial/nebulous/application/bi;)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    .line 219
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v2, 0x7f08017d

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 220
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/bi;->c(Lsoftware/simplicial/nebulous/application/bi;)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 221
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/bi;->c(Lsoftware/simplicial/nebulous/application/bi;)J

    move-result-wide v2

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/application/bi$2;->a:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lsoftware/simplicial/nebulous/f/aa;->c(J)Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/bi;->j:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v5, 0x7f0801bb

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->k:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->i:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 234
    :goto_1
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 229
    :cond_4
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->j:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v4, 0x7f0801bb

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v4, 0x7f0801aa

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->k:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    const v4, 0x7f08028b

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/bi;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/bi;->d(Lsoftware/simplicial/nebulous/application/bi;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bi$2;->b:Lsoftware/simplicial/nebulous/application/bi;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bi;->b:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
