.class public Lsoftware/simplicial/nebulous/application/bj;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/EditText;

.field f:Landroid/widget/EditText;

.field g:Landroid/widget/EditText;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/CheckBox;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lsoftware/simplicial/nebulous/application/bj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bj;->a:Ljava/lang/String;

    .line 35
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bj;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bj;->n:Z

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bj;Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/bj;->b(Lsoftware/simplicial/nebulous/application/bc$a;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bj;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/bj;->n:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bj;Lsoftware/simplicial/nebulous/application/bc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/bj;->c(Lsoftware/simplicial/nebulous/application/bc$a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->c:Landroid/widget/TextView;

    const v3, 0x7f08017d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->i:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 148
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->h:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 149
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 150
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    invoke-static {v3, v4, v2, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 153
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->d:Landroid/widget/TextView;

    const-string v2, "---"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v2, Lsoftware/simplicial/nebulous/application/bj$4;

    invoke-direct {v2, p0, p1}, Lsoftware/simplicial/nebulous/application/bj$4;-><init>(Lsoftware/simplicial/nebulous/application/bj;Lsoftware/simplicial/nebulous/application/bc$a;)V

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$m;)V

    .line 177
    return-void

    :cond_1
    move v0, v2

    .line 147
    goto :goto_0

    :cond_2
    move v0, v2

    .line 148
    goto :goto_1

    :cond_3
    move v0, v2

    .line 149
    goto :goto_2

    :cond_4
    move v0, v2

    .line 150
    goto :goto_3
.end method

.method private c(Lsoftware/simplicial/nebulous/application/bc$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne p1, v0, :cond_0

    const-string v0, "WRITE_MAIL"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "WRITE_MAIL_CLAN"

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v4, 0x7f0801cf

    const v5, 0x7f08005d

    const v3, 0x1080027

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 211
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 215
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 225
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 226
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 227
    const v1, 0x7f0800c6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 228
    const v1, 0x7f0802ea

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 229
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 230
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v3, 0x7f080062

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 231
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v3, 0x7f0800d3

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bj;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Landroid/content/res/Resources;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 234
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lsoftware/simplicial/nebulous/application/bj$5;

    invoke-direct {v3, p0, v0}, Lsoftware/simplicial/nebulous/application/bj$5;-><init>(Lsoftware/simplicial/nebulous/application/bj;I)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 278
    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 279
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 318
    :cond_2
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 219
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080102

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08015f

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/bj;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 281
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 283
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0

    .line 285
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 287
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 288
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 289
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080218

    .line 290
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 291
    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bj$6;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bj$6;-><init>(Lsoftware/simplicial/nebulous/application/bj;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 306
    invoke-virtual {v1, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 308
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 310
    sget-object v0, Lsoftware/simplicial/nebulous/application/ba$a;->a:Lsoftware/simplicial/nebulous/application/ba$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->b:Lsoftware/simplicial/nebulous/application/ba$a;

    .line 311
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ac:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 313
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 315
    sget-object v0, Lsoftware/simplicial/nebulous/application/az$a;->b:Lsoftware/simplicial/nebulous/application/az$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/az;->b:Lsoftware/simplicial/nebulous/application/az$a;

    .line 316
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->T:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 54
    const v0, 0x7f040061

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 56
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->c:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f0d0087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->d:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0d02b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    .line 61
    const v0, 0x7f0d02bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->f:Landroid/widget/EditText;

    .line 62
    const v0, 0x7f0d025b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->g:Landroid/widget/EditText;

    .line 63
    const v0, 0x7f0d02bc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->j:Landroid/widget/Button;

    .line 64
    const v0, 0x7f0d014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->k:Landroid/widget/Button;

    .line 65
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->l:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0d02b8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->h:Landroid/widget/Button;

    .line 67
    const v0, 0x7f0d02b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->i:Landroid/widget/Button;

    .line 68
    const v0, 0x7f0d02ba

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    .line 70
    return-object v1
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 189
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bj;->n:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->f:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    .line 194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    .line 203
    :goto_0
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, -0x1

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->m:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bj$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bj$1;-><init>(Lsoftware/simplicial/nebulous/application/bj;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->g:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->g:Landroid/widget/EditText;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bj$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bj$2;-><init>(Lsoftware/simplicial/nebulous/application/bj;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bj;->e:Landroid/widget/EditText;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bj$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bj$3;-><init>(Lsoftware/simplicial/nebulous/application/bj;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 141
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bj;->b(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 142
    return-void
.end method
