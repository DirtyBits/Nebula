.class public Lsoftware/simplicial/nebulous/application/z;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lsoftware/simplicial/nebulous/f/al$ad;
.implements Lsoftware/simplicial/nebulous/f/x;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field A:Ljava/lang/Thread;

.field B:J

.field C:Z

.field D:Z

.field E:Z

.field F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:F

.field private L:F

.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/LinearLayout;

.field n:Lsoftware/simplicial/nebulous/f/q;

.field o:I

.field p:Landroid/graphics/Bitmap;

.field q:Landroid/widget/ImageButton;

.field r:Landroid/widget/ImageButton;

.field s:Landroid/widget/ImageButton;

.field t:Landroid/widget/ImageButton;

.field u:Landroid/widget/ImageButton;

.field v:Landroid/widget/SeekBar;

.field w:F

.field x:F

.field y:F

.field z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lsoftware/simplicial/nebulous/application/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/z;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 57
    sget-object v0, Lsoftware/simplicial/nebulous/f/q;->a:Lsoftware/simplicial/nebulous/f/q;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->n:Lsoftware/simplicial/nebulous/f/q;

    .line 58
    iput v2, p0, Lsoftware/simplicial/nebulous/application/z;->o:I

    .line 59
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    .line 60
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->G:Z

    .line 61
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->H:Z

    .line 62
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->I:Z

    .line 71
    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    .line 72
    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    .line 73
    iput v3, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 74
    iput v3, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 75
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/z;->A:Ljava/lang/Thread;

    .line 76
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->J:Z

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/z;->B:J

    .line 78
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->C:Z

    .line 79
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->D:Z

    .line 80
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->E:Z

    .line 81
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->F:Z

    .line 82
    iput v3, p0, Lsoftware/simplicial/nebulous/application/z;->K:F

    .line 83
    iput v3, p0, Lsoftware/simplicial/nebulous/application/z;->L:F

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 442
    sget-object v0, Lsoftware/simplicial/nebulous/application/z$5;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->n:Lsoftware/simplicial/nebulous/f/q;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 480
    :goto_0
    return-void

    .line 445
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f08010d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 446
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 447
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 448
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 452
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f08015a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 453
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 454
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 455
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 459
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f08022c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 460
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c006c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 461
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 462
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 463
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 466
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f080232

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 467
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 468
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 469
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 470
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 473
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 474
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 475
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 476
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/graphics/Bitmap;Z)V
    .locals 3

    .prologue
    .line 305
    if-nez p1, :cond_1

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    .line 308
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    .line 309
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020432

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 310
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->b()V

    .line 320
    :goto_0
    if-eqz p2, :cond_0

    .line 322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->I:Z

    .line 323
    sget-object v0, Lsoftware/simplicial/nebulous/f/q;->a:Lsoftware/simplicial/nebulous/f/q;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->n:Lsoftware/simplicial/nebulous/f/q;

    .line 324
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->a()V

    .line 326
    :cond_0
    return-void

    .line 314
    :cond_1
    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    .line 315
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    .line 316
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 317
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/z;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/z;Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/z;->a(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/z;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/z;->G:Z

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 485
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->v:Landroid/widget/SeekBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 486
    iput v2, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 487
    iput v2, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 488
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    .line 489
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/z;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->J:Z

    return v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/z;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/z;->H:Z

    return p1
.end method

.method private c()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 493
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 496
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 497
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 498
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 535
    :goto_0
    return-void

    .line 502
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 503
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 505
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 506
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 507
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 508
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-le v3, v4, :cond_5

    .line 509
    int-to-float v1, v1

    iget v3, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 512
    :goto_1
    int-to-float v1, v1

    iget v3, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v1, v3

    int-to-float v3, v2

    sub-float/2addr v1, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v3, v6

    div-float/2addr v1, v3

    invoke-static {v5, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 513
    int-to-float v0, v0

    iget v3, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v0, v3

    int-to-float v2, v2

    sub-float/2addr v0, v2

    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v2, v6

    div-float/2addr v0, v2

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 514
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_1

    .line 515
    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 516
    :cond_1
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    neg-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 517
    neg-float v2, v1

    iput v2, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 518
    :cond_2
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    cmpl-float v2, v2, v0

    if-lez v2, :cond_3

    .line 519
    iput v0, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 520
    :cond_3
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    neg-float v3, v0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 521
    neg-float v2, v0

    iput v2, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 523
    :cond_4
    cmpl-float v2, v1, v5

    if-nez v2, :cond_6

    .line 524
    iput v5, p0, Lsoftware/simplicial/nebulous/application/z;->K:F

    .line 528
    :goto_2
    cmpl-float v1, v0, v5

    if-nez v1, :cond_7

    .line 529
    iput v5, p0, Lsoftware/simplicial/nebulous/application/z;->L:F

    .line 533
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 534
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    goto/16 :goto_0

    .line 511
    :cond_5
    int-to-float v0, v0

    iget v3, p0, Lsoftware/simplicial/nebulous/application/z;->x:F

    mul-float/2addr v0, v3

    float-to-int v0, v0

    goto :goto_1

    .line 526
    :cond_6
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    div-float v1, v2, v1

    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->K:F

    goto :goto_2

    .line 531
    :cond_7
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    div-float v0, v1, v0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/z;->L:F

    goto :goto_3
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/z;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->G:Z

    return v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/16 v6, 0x100

    const/high16 v5, 0x40000000    # 2.0f

    .line 539
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    div-float/2addr v0, v1

    .line 543
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->K:F

    div-float/2addr v1, v5

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    mul-float/2addr v1, v2

    .line 544
    iget v2, p0, Lsoftware/simplicial/nebulous/application/z;->L:F

    div-float/2addr v2, v5

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    mul-float/2addr v2, v3

    .line 545
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 546
    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    .line 547
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    add-float/2addr v4, v0

    div-float/2addr v4, v5

    add-float/2addr v1, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    add-float/2addr v0, v4

    div-float/2addr v0, v5

    add-float/2addr v0, v2

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 548
    iget v0, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->w:F

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 549
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 550
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 551
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 552
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->p:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 553
    const/4 v1, 0x1

    invoke-static {v0, v6, v6, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 555
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v0, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/graphics/Bitmap;Landroid/content/Context;)V

    .line 556
    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    .line 557
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->I:Z

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v1, 0x0

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    goto/16 :goto_0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/z;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->H:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 332
    if-nez v0, :cond_0

    .line 354
    :goto_0
    return-void

    .line 335
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/z$3;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/z$3;-><init>(Lsoftware/simplicial/nebulous/application/z;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 360
    if-nez v0, :cond_0

    .line 393
    :goto_0
    return-void

    .line 363
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/z$4;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/z$4;-><init>(Lsoftware/simplicial/nebulous/application/z;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/q;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 428
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 431
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 432
    if-ltz v0, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 433
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/q;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->n:Lsoftware/simplicial/nebulous/f/q;

    .line 435
    :cond_2
    iget v0, p0, Lsoftware/simplicial/nebulous/application/z;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/z;->o:I

    .line 436
    iget v0, p0, Lsoftware/simplicial/nebulous/application/z;->o:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 437
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->a()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 398
    invoke-super {p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/application/ao;->onActivityResult(IILandroid/content/Intent;)V

    .line 400
    if-nez p1, :cond_0

    .line 402
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 406
    :try_start_0
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 407
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 408
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 409
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 410
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 411
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 412
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 413
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 414
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/z;->a(Landroid/graphics/Bitmap;Z)V

    .line 415
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 423
    :cond_0
    :goto_0
    return-void

    .line 417
    :catch_0
    move-exception v0

    .line 419
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080102

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x108003f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 241
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 243
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->d()V

    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->u:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 248
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->b()V

    goto :goto_0

    .line 250
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0

    .line 254
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 256
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 257
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/z;->startActivityForResult(Landroid/content/Intent;I)V

    .line 261
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080102

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0801bf

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 268
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 270
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->G:Z

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 272
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 273
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 274
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/z;->a(Landroid/graphics/Bitmap;Z)V

    .line 275
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->b(Lsoftware/simplicial/nebulous/f/x;)V

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "profile_facebook"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 281
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->H:Z

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 283
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 285
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/z;->a(Landroid/graphics/Bitmap;Z)V

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/x;)Z

    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    const-string v1, "profile_google"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 291
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->d()V

    .line 292
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->U:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 295
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 297
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->d()V

    .line 298
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/al;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    .line 299
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->U:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 168
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/ao;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 169
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->m:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 173
    :goto_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    .line 174
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->m:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 89
    const v0, 0x7f04003e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 91
    const v0, 0x7f0d00d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->b:Landroid/widget/ImageView;

    .line 92
    const v0, 0x7f0d012b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->c:Landroid/widget/Button;

    .line 93
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->d:Landroid/widget/Button;

    .line 94
    const v0, 0x7f0d015b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0d015c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    .line 96
    const v0, 0x7f0d015d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0d015e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->h:Landroid/widget/Button;

    .line 98
    const v0, 0x7f0d015f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->i:Landroid/widget/Button;

    .line 99
    const v0, 0x7f0d0151

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0d0153

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0d0152

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0d0154

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->m:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0d0156

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->q:Landroid/widget/ImageButton;

    .line 105
    const v0, 0x7f0d0157

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->r:Landroid/widget/ImageButton;

    .line 106
    const v0, 0x7f0d0158

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->s:Landroid/widget/ImageButton;

    .line 107
    const v0, 0x7f0d0159

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->t:Landroid/widget/ImageButton;

    .line 108
    const v0, 0x7f0d0155

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->u:Landroid/widget/ImageButton;

    .line 109
    const v0, 0x7f0d015a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->v:Landroid/widget/SeekBar;

    .line 111
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/z;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 113
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 610
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->J:Z

    .line 613
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->A:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 614
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 180
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 182
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 184
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 186
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/z;->i:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 189
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    .line 191
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/z;->J:Z

    .line 192
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lsoftware/simplicial/nebulous/application/z$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/z$2;-><init>(Lsoftware/simplicial/nebulous/application/z;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->A:Ljava/lang/Thread;

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->A:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 236
    return-void

    :cond_0
    move v0, v2

    .line 186
    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 564
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 603
    :cond_0
    :goto_0
    return v0

    .line 567
    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0xaf

    add-long/2addr v2, v4

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/z;->B:J

    .line 568
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->s:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_1

    .line 570
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    add-float/2addr v1, v6

    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 571
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->C:Z

    .line 573
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->t:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_2

    .line 575
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    sub-float/2addr v1, v6

    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->z:F

    .line 576
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->D:Z

    .line 578
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->q:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_3

    .line 580
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    add-float/2addr v1, v6

    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 581
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->E:Z

    .line 583
    :cond_3
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/z;->r:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_0

    .line 585
    iget v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    sub-float/2addr v1, v6

    iput v1, p0, Lsoftware/simplicial/nebulous/application/z;->y:F

    .line 586
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/z;->F:Z

    goto :goto_0

    .line 591
    :pswitch_2
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/z;->B:J

    .line 592
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->s:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_4

    .line 593
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/z;->C:Z

    .line 594
    :cond_4
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->t:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_5

    .line 595
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/z;->D:Z

    .line 596
    :cond_5
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->q:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_6

    .line 597
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/z;->E:Z

    .line 598
    :cond_6
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/z;->r:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_7

    .line 599
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/z;->F:Z

    .line 600
    :cond_7
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/z;->c()V

    goto :goto_0

    .line 564
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 119
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 131
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 133
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->v:Landroid/widget/SeekBar;

    new-instance v1, Lsoftware/simplicial/nebulous/application/z$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/z$1;-><init>(Lsoftware/simplicial/nebulous/application/z;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 155
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/z;->a(Landroid/graphics/Bitmap;Z)V

    .line 157
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/z;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/z;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$b;->a:Lsoftware/simplicial/nebulous/f/o$b;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/o$b;Lsoftware/simplicial/nebulous/f/al$ad;)V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/z;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    sget-object v1, Lsoftware/simplicial/nebulous/f/o$b;->b:Lsoftware/simplicial/nebulous/f/o$b;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/o$b;Lsoftware/simplicial/nebulous/f/al$ad;)V

    .line 163
    return-void
.end method
