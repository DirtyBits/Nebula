.class Lsoftware/simplicial/nebulous/application/ah$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/ah;->b(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/ah;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/ah;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1024
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/ah$6;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1028
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 1041
    :goto_0
    return-void

    .line 1031
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->b:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_2

    .line 1032
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->z:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1034
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1035
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d/b;

    .line 1036
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->g:Ljava/util/Set;

    iget-object v4, v0, Lsoftware/simplicial/a/d/b;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1037
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1038
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1040
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah$6;->a:Ljava/util/List;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah$6;->b:Lsoftware/simplicial/nebulous/application/ah;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/t;->a(Ljava/util/List;Lsoftware/simplicial/a/d/c;)V

    goto :goto_0
.end method
