.class public Lsoftware/simplicial/nebulous/application/as;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lsoftware/simplicial/a/ah;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/a/b/c;
.implements Lsoftware/simplicial/a/c;
.implements Lsoftware/simplicial/a/c/d;
.implements Lsoftware/simplicial/a/h/e;
.implements Lsoftware/simplicial/a/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/as$a;,
        Lsoftware/simplicial/nebulous/application/as$b;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Z

.field public static c:Z

.field public static d:Lsoftware/simplicial/nebulous/application/as$b;

.field public static e:Lsoftware/simplicial/nebulous/application/as$a;


# instance fields
.field A:Landroid/widget/CheckBox;

.field B:Lsoftware/simplicial/nebulous/a/y;

.field C:Lsoftware/simplicial/nebulous/a/s;

.field D:Lsoftware/simplicial/nebulous/a/l;

.field E:Lsoftware/simplicial/nebulous/a/o;

.field F:Lsoftware/simplicial/nebulous/a/ab;

.field G:Lsoftware/simplicial/nebulous/a/ac;

.field H:Lsoftware/simplicial/nebulous/a/j;

.field I:Landroid/widget/ArrayAdapter;

.field private final J:Ljava/lang/Object;

.field private final K:Ljava/lang/Object;

.field private L:Ljava/util/Timer;

.field private M:Landroid/widget/Toast;

.field private N:I

.field private O:I

.field private P:Ljava/util/Timer;

.field f:Landroid/widget/ListView;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/Button;

.field n:Landroid/widget/Button;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/LinearLayout;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/RadioGroup;

.field x:Landroid/widget/ImageButton;

.field y:Landroid/widget/ImageButton;

.field z:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    const-class v0, Lsoftware/simplicial/nebulous/application/as;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->a:Ljava/lang/String;

    .line 94
    sput-boolean v1, Lsoftware/simplicial/nebulous/application/as;->b:Z

    .line 95
    sput-boolean v1, Lsoftware/simplicial/nebulous/application/as;->c:Z

    .line 97
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$a;->a:Lsoftware/simplicial/nebulous/application/as$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->J:Ljava/lang/Object;

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->K:Ljava/lang/Object;

    .line 129
    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 131
    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    .line 132
    iput v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 133
    iput v1, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    return-void
.end method

.method private a(ILsoftware/simplicial/a/b/b;)V
    .locals 2

    .prologue
    .line 1191
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$a;->a:Lsoftware/simplicial/nebulous/application/as$a;

    if-ne v0, v1, :cond_1

    .line 1193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1194
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    sget-object v0, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    if-eq p2, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    if-ne p2, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1196
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->F:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0, p1, p2}, Lsoftware/simplicial/nebulous/a/ab;->a(ILsoftware/simplicial/a/b/b;)V

    .line 1197
    return-void

    .line 1194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lsoftware/simplicial/nebulous/application/as$b;)V
    .locals 6

    .prologue
    const v1, 0x7f020236

    const v5, 0x7f020235

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 461
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, p1, :cond_0

    .line 462
    iput v2, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 464
    :cond_0
    sput-object p1, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    .line 465
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->h:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 466
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 467
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 468
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 469
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 470
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 471
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 472
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->q:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 473
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 474
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->r:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 480
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 481
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 482
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 485
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 486
    sput-boolean v2, Lsoftware/simplicial/nebulous/application/as;->b:Z

    .line 487
    sput-boolean v2, Lsoftware/simplicial/nebulous/application/as;->c:Z

    .line 488
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$13;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/nebulous/application/as$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 603
    :goto_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->g()V

    .line 604
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 605
    return-void

    .line 491
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->B:Lsoftware/simplicial/nebulous/a/y;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 492
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->h:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 493
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 494
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 495
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 496
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    const v1, 0x7f080287

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/as;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iput v2, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    goto :goto_0

    .line 501
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 502
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 503
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    const-string v1, "1/10"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 506
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 507
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 508
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080217

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 509
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 510
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 514
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 515
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    goto/16 :goto_0

    .line 518
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->H:Lsoftware/simplicial/nebulous/a/j;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 519
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 520
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 521
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080167

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 524
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 525
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 526
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 527
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 528
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 529
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    goto/16 :goto_0

    .line 532
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 533
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 534
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    const-string v1, "1/10"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 538
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080152

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 539
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 540
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 541
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 543
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 544
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 545
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 546
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->r:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 547
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 548
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    goto/16 :goto_0

    .line 551
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 552
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 553
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 555
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 556
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 557
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080073

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 558
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 559
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 560
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 561
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 562
    sput-boolean v4, Lsoftware/simplicial/nebulous/application/as;->b:Z

    .line 563
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    goto/16 :goto_0

    .line 566
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 567
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 568
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 569
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 570
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 572
    sput-boolean v4, Lsoftware/simplicial/nebulous/application/as;->c:Z

    .line 573
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    .line 575
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 576
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$13;->b:[I

    sget-object v1, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/as$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 579
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 580
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->F:Lsoftware/simplicial/nebulous/a/ab;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 582
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 583
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080101

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 584
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 587
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 588
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->G:Lsoftware/simplicial/nebulous/a/ac;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    .line 590
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const v1, 0x7f080101

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 592
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 594
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 595
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    const v1, 0x7f080184

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 596
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 576
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/as;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->g()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/as;I)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/as;->c(I)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/as;ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 0

    .prologue
    .line 90
    invoke-direct/range {p0 .. p5}, Lsoftware/simplicial/nebulous/application/as;->b(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/as;ILsoftware/simplicial/a/b/b;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/as;->a(ILsoftware/simplicial/a/b/b;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/as;Lsoftware/simplicial/nebulous/application/as$b;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/as;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    return v0
.end method

.method private b(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1201
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/as$a;->b:Lsoftware/simplicial/nebulous/application/as$a;

    if-ne v0, v3, :cond_4

    .line 1203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1204
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    sget-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-eq p3, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    if-ne p3, v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1205
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 1206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    sget-object v3, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-eq p3, v3, :cond_2

    sget-object v3, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    if-ne p3, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1208
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->G:Lsoftware/simplicial/nebulous/a/ac;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/a/ac;->a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    .line 1209
    return-void

    :cond_5
    move v0, v1

    .line 1204
    goto :goto_0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/as;)I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 451
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 452
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 453
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 454
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 455
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    if-ge v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 456
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    if-lez v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 457
    return-void

    :cond_0
    move v0, v2

    .line 455
    goto :goto_0

    :cond_1
    move v1, v2

    .line 456
    goto :goto_1
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 1183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1184
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->v:Lsoftware/simplicial/a/c/h;

    sget-object v2, Lsoftware/simplicial/a/c/h;->f:Lsoftware/simplicial/a/c/h;

    if-ne v0, v2, :cond_2

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1185
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/o;->a(I)V

    .line 1186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->notifyDataSetChanged()V

    .line 1187
    return-void

    .line 1184
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->A:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 782
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 786
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$a;->a:Lsoftware/simplicial/nebulous/application/as$a;

    if-ne v0, v1, :cond_0

    .line 787
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->G:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 793
    :goto_0
    return-void

    .line 790
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ay$a;->a:Lsoftware/simplicial/nebulous/application/ay$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    .line 791
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->I:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 797
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 799
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 800
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 801
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 802
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 804
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/as$17;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/as$17;-><init>(Lsoftware/simplicial/nebulous/application/as;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 822
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 824
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 826
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 827
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 828
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 829
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 830
    return-void
.end method

.method private g()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/16 v5, 0x64

    const/16 v4, 0xa

    const/4 v2, 0x0

    .line 834
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 837
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    .line 840
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 842
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 843
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 844
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 845
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 846
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 847
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 848
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 849
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->h()V

    .line 851
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/as$b;->a:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v3, :cond_9

    .line 853
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 854
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v5

    .line 855
    iget-object v0, v5, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v3, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v3, :cond_4

    .line 857
    iget-object v6, v5, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v7, v6

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_4

    aget-object v8, v6, v3

    .line 859
    iget-boolean v0, v8, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v0, :cond_1

    .line 861
    new-instance v9, Lsoftware/simplicial/a/bg;

    invoke-direct {v9}, Lsoftware/simplicial/a/bg;-><init>()V

    .line 862
    iget v0, v8, Lsoftware/simplicial/a/bf;->A:I

    iput v0, v9, Lsoftware/simplicial/a/bg;->b:I

    .line 863
    iget-object v0, v8, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    iput-object v0, v9, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 864
    iget-object v0, v8, Lsoftware/simplicial/a/bf;->I:[B

    iput-object v0, v9, Lsoftware/simplicial/a/bg;->e:[B

    .line 865
    iget-object v0, v8, Lsoftware/simplicial/a/bf;->J:Lsoftware/simplicial/a/q;

    iput-object v0, v9, Lsoftware/simplicial/a/bg;->f:Lsoftware/simplicial/a/q;

    .line 866
    iget v0, v8, Lsoftware/simplicial/a/bf;->A:I

    iget-object v10, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v10}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v10

    if-ne v0, v10, :cond_2

    iget v0, v8, Lsoftware/simplicial/a/bf;->A:I

    const/4 v10, -0x1

    if-eq v0, v10, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v9, Lsoftware/simplicial/a/bg;->a:Z

    .line 867
    iget-boolean v0, v9, Lsoftware/simplicial/a/bg;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v10, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v10}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v10

    invoke-static {v0, v10}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    iput-object v0, v9, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    .line 868
    iget v0, v8, Lsoftware/simplicial/a/bf;->bD:I

    invoke-static {v0}, Lsoftware/simplicial/a/aw;->b(I)J

    move-result-wide v10

    iput-wide v10, v9, Lsoftware/simplicial/a/bg;->g:J

    .line 870
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 857
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 866
    goto :goto_1

    .line 867
    :cond_3
    iget-object v0, v8, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v10, v8, Lsoftware/simplicial/a/bf;->E:[B

    invoke-static {v0, v10}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    .line 874
    :cond_4
    iget v3, v5, Lsoftware/simplicial/a/u;->w:I

    .line 876
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 877
    const-string v0, "%s %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const v6, 0x7f080287

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/as;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 878
    const/16 v1, 0x7f

    if-lt v3, v1, :cond_5

    .line 879
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 880
    :cond_5
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 882
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->B:Lsoftware/simplicial/nebulous/a/y;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/y;->clear()V

    .line 883
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->B:Lsoftware/simplicial/nebulous/a/y;

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/a/y;->addAll(Ljava/util/Collection;)V

    .line 884
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->B:Lsoftware/simplicial/nebulous/a/y;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/y;->notifyDataSetChanged()V

    .line 985
    :cond_6
    :goto_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, v1, :cond_7

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_8

    .line 987
    :cond_7
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->i()V

    .line 989
    :cond_8
    return-void

    .line 886
    :cond_9
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_a

    .line 888
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->clear()V

    .line 889
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1, v5}, Lsoftware/simplicial/nebulous/a/s;->a(II)V

    .line 890
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 891
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v1, v1, 0x64

    new-instance v2, Lsoftware/simplicial/nebulous/application/as$18;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/as$18;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1, v5, v2}, Lsoftware/simplicial/nebulous/f/al;->a(IILsoftware/simplicial/nebulous/f/al$s;)V

    goto :goto_3

    .line 915
    :cond_a
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_b

    .line 917
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/j;->clear()V

    .line 918
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/j;->notifyDataSetChanged()V

    .line 919
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/as$19;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/as$19;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$q;)V

    goto :goto_3

    .line 935
    :cond_b
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_c

    .line 937
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->clear()V

    .line 938
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    .line 939
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v2, v2, 0x64

    new-instance v3, Lsoftware/simplicial/nebulous/application/as$20;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/as$20;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1, v2, v5, v3}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$r;)V

    goto :goto_3

    .line 959
    :cond_c
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_e

    .line 961
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 962
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->F:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ab;->clear()V

    .line 963
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->G:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ac;->clear()V

    .line 964
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->F:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ab;->notifyDataSetChanged()V

    .line 965
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->G:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ac;->notifyDataSetChanged()V

    .line 966
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$a;->a:Lsoftware/simplicial/nebulous/application/as$a;

    if-ne v0, v1, :cond_d

    .line 968
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1, v4}, Lsoftware/simplicial/a/t;->a(II)V

    .line 974
    :goto_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 972
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1, v4}, Lsoftware/simplicial/a/t;->b(II)V

    goto :goto_4

    .line 976
    :cond_e
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_6

    .line 978
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 979
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->clear()V

    .line 980
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->notifyDataSetChanged()V

    .line 981
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    mul-int/lit8 v1, v1, 0xa

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    invoke-virtual {v0, v1, v4, v2}, Lsoftware/simplicial/a/t;->a(IIZ)V

    .line 982
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method private h()V
    .locals 6

    .prologue
    .line 993
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->K:Ljava/lang/Object;

    monitor-enter v1

    .line 995
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->k()V

    .line 997
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->L:Ljava/util/Timer;

    .line 998
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->L:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/nebulous/application/as$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/as$2;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1026
    monitor-exit v1

    .line 1027
    return-void

    .line 1026
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 7

    .prologue
    .line 1031
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/as;->J:Ljava/lang/Object;

    monitor-enter v6

    .line 1033
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->j()V

    .line 1035
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    .line 1036
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    new-instance v1, Lsoftware/simplicial/nebulous/application/as$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/as$3;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1075
    monitor-exit v6

    .line 1076
    return-void

    .line 1075
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1080
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->J:Ljava/lang/Object;

    monitor-enter v1

    .line 1082
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1084
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1085
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->P:Ljava/util/Timer;

    .line 1087
    :cond_0
    monitor-exit v1

    .line 1088
    return-void

    .line 1087
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1092
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->K:Ljava/lang/Object;

    monitor-enter v1

    .line 1094
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->L:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1096
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->L:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1097
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->L:Ljava/util/Timer;

    .line 1099
    :cond_0
    monitor-exit v1

    .line 1100
    return-void

    .line 1099
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private l()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 1310
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1312
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1313
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1314
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1315
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1317
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/as$11;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/as$11;-><init>(Lsoftware/simplicial/nebulous/application/as;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1335
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1337
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1339
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 1340
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1341
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1342
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 1343
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 310
    return-void
.end method

.method public a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 8

    .prologue
    .line 1290
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1291
    if-nez v7, :cond_0

    .line 1305
    :goto_0
    return-void

    .line 1294
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$10;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/as$10;-><init>(Lsoftware/simplicial/nebulous/application/as;ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    invoke-virtual {v7, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;IIZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lsoftware/simplicial/a/h/a$a;",
            "IIZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/h/f;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1261
    iget-object v12, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1262
    if-nez v12, :cond_0

    .line 1284
    :goto_0
    return-void

    .line 1265
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$9;

    move-object v1, p0

    move-object/from16 v2, p3

    move v3, p1

    move-object v4, p2

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p9

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lsoftware/simplicial/nebulous/application/as$9;-><init>(Lsoftware/simplicial/nebulous/application/as;Lsoftware/simplicial/a/h/a$a;ILjava/lang/String;IIZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v12, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ILsoftware/simplicial/a/b/b;Lsoftware/simplicial/a/b/e;)V
    .locals 2

    .prologue
    .line 1241
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1242
    if-nez v0, :cond_0

    .line 1256
    :goto_0
    return-void

    .line 1245
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/as$8;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/as$8;-><init>(Lsoftware/simplicial/nebulous/application/as;ILsoftware/simplicial/a/b/b;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 316
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, v1, :cond_2

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_0

    .line 291
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    :goto_1
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 272
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 298
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/c/i;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1106
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1107
    if-nez v0, :cond_0

    .line 1127
    :goto_0
    return-void

    .line 1110
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/as$4;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/as$4;-><init>(Lsoftware/simplicial/nebulous/application/as;Ljava/util/List;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;ILsoftware/simplicial/a/b/b;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/g;",
            ">;I",
            "Lsoftware/simplicial/a/b/b;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1214
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1215
    if-nez v6, :cond_0

    .line 1236
    :goto_0
    return-void

    .line 1218
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$7;

    move-object v1, p0

    move-object v2, p3

    move v3, p2

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/application/as$7;-><init>(Lsoftware/simplicial/nebulous/application/as;Lsoftware/simplicial/a/b/b;IZLjava/util/List;)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/h/a$a;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1349
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;)V
    .locals 0

    .prologue
    .line 1179
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    return-void
.end method

.method public a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;I)V
    .locals 2

    .prologue
    .line 1132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1133
    if-nez v0, :cond_0

    .line 1147
    :goto_0
    return-void

    .line 1136
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/as$5;

    invoke-direct {v1, p0, p4}, Lsoftware/simplicial/nebulous/application/as$5;-><init>(Lsoftware/simplicial/nebulous/application/as;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/c/h;IIII)V
    .locals 8

    .prologue
    .line 1152
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1153
    if-nez v7, :cond_0

    .line 1172
    :goto_0
    return-void

    .line 1156
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$6;

    move-object v1, p0

    move v2, p5

    move-object v3, p1

    move v4, p4

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/as$6;-><init>(Lsoftware/simplicial/nebulous/application/as;ILsoftware/simplicial/a/c/h;III)V

    invoke-virtual {v7, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 367
    sget-object v0, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne p1, v0, :cond_0

    .line 369
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 370
    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    new-instance v1, Lsoftware/simplicial/nebulous/application/as$15;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/as$15;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->c()V

    .line 241
    return-void
.end method

.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 10

    .prologue
    .line 662
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 663
    if-nez v9, :cond_0

    .line 681
    :goto_0
    return-void

    .line 666
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/as$16;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/as$16;-><init>(Lsoftware/simplicial/nebulous/application/as;[I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 322
    if-nez v0, :cond_0

    .line 341
    :goto_0
    return v2

    .line 325
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/as$14;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/as$14;-><init>(Lsoftware/simplicial/nebulous/application/as;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 282
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 409
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    if-ne p1, v0, :cond_2

    .line 349
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    .line 350
    const v1, 0x7f0d01dd

    if-ne p2, v1, :cond_3

    .line 351
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    .line 357
    :cond_0
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    if-eq v0, v1, :cond_1

    .line 358
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/x;)V

    .line 360
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 362
    :cond_2
    return-void

    .line 352
    :cond_3
    const v1, 0x7f0d01de

    if-ne p2, v1, :cond_4

    .line 353
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->c:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    goto :goto_0

    .line 354
    :cond_4
    const v1, 0x7f0d01df

    if-ne p2, v1, :cond_0

    .line 355
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->b:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 688
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 776
    :cond_0
    :goto_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->c()V

    .line 777
    return-void

    .line 690
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 692
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->a:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 694
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 696
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 697
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto :goto_0

    .line 699
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 701
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_8

    .line 703
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 704
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto :goto_0

    .line 706
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    :goto_1
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    :cond_7
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    goto :goto_1

    .line 708
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_9

    .line 710
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 712
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 714
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 716
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_b

    .line 718
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$a;->a:Lsoftware/simplicial/nebulous/application/as$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    .line 719
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 721
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 723
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$a;->b:Lsoftware/simplicial/nebulous/application/as$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    .line 724
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_0

    .line 726
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 728
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 730
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_e

    .line 732
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ak:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 734
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_10

    .line 736
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_f

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->e:Lsoftware/simplicial/nebulous/application/as$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$a;->b:Lsoftware/simplicial/nebulous/application/as$a;

    if-ne v0, v1, :cond_f

    .line 737
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->J:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 739
    :cond_f
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->g()V

    goto/16 :goto_0

    .line 741
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_16

    .line 743
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->b:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_11

    .line 744
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->f()V

    goto/16 :goto_0

    .line 745
    :cond_11
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_12

    .line 746
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->l()V

    goto/16 :goto_0

    .line 747
    :cond_12
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_13

    .line 748
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->q()V

    goto/16 :goto_0

    .line 749
    :cond_13
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_14

    .line 750
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->d()V

    goto/16 :goto_0

    .line 751
    :cond_14
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->f:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_0

    .line 753
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 754
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->e()V

    goto/16 :goto_0

    .line 756
    :cond_15
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto/16 :goto_0

    .line 759
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_18

    .line 761
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 762
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    if-gez v0, :cond_17

    .line 763
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 765
    :cond_17
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->g()V

    goto/16 :goto_0

    .line 767
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 769
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 770
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    if-le v0, v1, :cond_19

    .line 771
    iget v0, p0, Lsoftware/simplicial/nebulous/application/as;->O:I

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 773
    :cond_19
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->g()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 139
    const v0, 0x7f040051

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 141
    const v0, 0x7f0d01fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->f:Landroid/widget/ListView;

    .line 142
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->g:Landroid/widget/Button;

    .line 143
    const v0, 0x7f0d0243

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->h:Landroid/widget/Button;

    .line 144
    const v0, 0x7f0d01d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    .line 145
    const v0, 0x7f0d00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    .line 146
    const v0, 0x7f0d01e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    .line 147
    const v0, 0x7f0d0142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f0d01d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    .line 149
    const v0, 0x7f0d0246

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->v:Landroid/widget/TextView;

    .line 150
    const v0, 0x7f0d01dc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    .line 151
    const v0, 0x7f0d0240

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    .line 152
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    .line 153
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    .line 154
    const v0, 0x7f0d01d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    .line 155
    const v0, 0x7f0d019e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    .line 156
    const v0, 0x7f0d01da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    .line 157
    const v0, 0x7f0d01db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    .line 158
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->q:Landroid/widget/Button;

    .line 159
    const v0, 0x7f0d01cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->r:Landroid/widget/Button;

    .line 160
    const v0, 0x7f0d01d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->s:Landroid/widget/LinearLayout;

    .line 161
    const v0, 0x7f0d01e0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->z:Landroid/widget/CheckBox;

    .line 162
    const v0, 0x7f0d01e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    .line 164
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 610
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->I:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 612
    instance-of v1, v0, Lsoftware/simplicial/a/bg;

    if-eqz v1, :cond_5

    .line 614
    check-cast v0, Lsoftware/simplicial/a/bg;

    .line 624
    iget v1, v0, Lsoftware/simplicial/a/bg;->b:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_2

    .line 626
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 629
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802b5

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    .line 630
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 657
    :cond_1
    :goto_0
    return-void

    .line 633
    :cond_2
    iget v1, v0, Lsoftware/simplicial/a/bg;->b:I

    const/4 v2, -0x1

    if-gt v1, v2, :cond_4

    .line 635
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    if-eqz v0, :cond_3

    .line 636
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 638
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802b6

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    .line 639
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->M:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 643
    :cond_4
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v0, Lsoftware/simplicial/a/bg;->b:I

    iput v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 644
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/a/bg;->e:[B

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v5, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    .line 645
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v6, v0, Lsoftware/simplicial/a/bg;->d:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 644
    invoke-static {v2, v3, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v2

    iput-object v2, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    .line 646
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/a/bg;->c:Ljava/lang/CharSequence;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    .line 647
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 649
    :cond_5
    instance-of v1, v0, Lsoftware/simplicial/nebulous/f/m;

    if-eqz v1, :cond_1

    .line 651
    check-cast v0, Lsoftware/simplicial/nebulous/f/m;

    .line 653
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/m;->b:[B

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v5, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    .line 654
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/m;->a:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 653
    invoke-static {v2, v3, v4, v0}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    .line 655
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->y:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 434
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 436
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 437
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 438
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 439
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 440
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 441
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 443
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->k()V

    .line 444
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->j()V

    .line 446
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/nebulous/application/as;->b:Z

    .line 447
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 414
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 416
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/as;->N:I

    .line 417
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, v1, :cond_0

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-ne v0, v1, :cond_3

    .line 418
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->c:Lsoftware/simplicial/nebulous/application/as$b;

    :goto_0
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    .line 421
    :goto_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->c()V

    .line 423
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 424
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 426
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 427
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 428
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 429
    return-void

    .line 418
    :cond_2
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    goto :goto_0

    .line 420
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/as;->a(Lsoftware/simplicial/nebulous/application/as$b;)V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 170
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 172
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    new-instance v0, Lsoftware/simplicial/nebulous/a/y;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/y;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->B:Lsoftware/simplicial/nebulous/a/y;

    .line 175
    new-instance v0, Lsoftware/simplicial/nebulous/a/s;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/a/s$a;->a:Lsoftware/simplicial/nebulous/a/s$a;

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/s;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/s$a;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->C:Lsoftware/simplicial/nebulous/a/s;

    .line 176
    new-instance v0, Lsoftware/simplicial/nebulous/a/l;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v2, Lsoftware/simplicial/nebulous/a/l$a;->a:Lsoftware/simplicial/nebulous/a/l$a;

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/a/l;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/l$a;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    .line 177
    new-instance v0, Lsoftware/simplicial/nebulous/a/j;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/j;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->H:Lsoftware/simplicial/nebulous/a/j;

    .line 178
    new-instance v0, Lsoftware/simplicial/nebulous/a/o;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/o;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->E:Lsoftware/simplicial/nebulous/a/o;

    .line 179
    new-instance v0, Lsoftware/simplicial/nebulous/a/ab;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/ab;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->F:Lsoftware/simplicial/nebulous/a/ab;

    .line 180
    new-instance v0, Lsoftware/simplicial/nebulous/a/ac;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/ac;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->G:Lsoftware/simplicial/nebulous/a/ac;

    .line 182
    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-nez v0, :cond_0

    .line 183
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->a:Lsoftware/simplicial/nebulous/application/as$b;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    .line 185
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$13;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    invoke-virtual {v1}, Lsoftware/simplicial/a/x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->j:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->n:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->p:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->r:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->l:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->z:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->z:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/as$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/as$1;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 222
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 223
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->A:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/as$12;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/as$12;-><init>(Lsoftware/simplicial/nebulous/application/as;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 232
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->c()V

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    return-void

    .line 188
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    const v1, 0x7f0d01dd

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    .line 191
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    const v1, 0x7f0d01de

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    .line 194
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->w:Landroid/widget/RadioGroup;

    const v1, 0x7f0d01df

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 185
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public p_()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 258
    :goto_0
    return-void

    .line 257
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/as;->c()V

    goto :goto_0
.end method
