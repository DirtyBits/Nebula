.class public Lsoftware/simplicial/nebulous/application/ax;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/RadioGroup;

.field c:Landroid/widget/EditText;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lsoftware/simplicial/nebulous/application/ax;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ax;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a(I)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v1

    .line 77
    if-eqz v1, :cond_0

    .line 79
    iget-object v2, v1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 81
    iget-boolean v5, v4, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v5, :cond_1

    iget v5, v4, Lsoftware/simplicial/a/bf;->A:I

    if-ne v5, p1, :cond_1

    .line 83
    iget v0, v4, Lsoftware/simplicial/a/bf;->ad:I

    .line 88
    :cond_0
    return v0

    .line 79
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a()Lsoftware/simplicial/nebulous/f/ae;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 106
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->d:Lsoftware/simplicial/nebulous/f/ae;

    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->c:Lsoftware/simplicial/nebulous/f/ae;

    goto :goto_0

    .line 98
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->a:Lsoftware/simplicial/nebulous/f/ae;

    goto :goto_0

    .line 100
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->b:Lsoftware/simplicial/nebulous/f/ae;

    goto :goto_0

    .line 102
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->e:Lsoftware/simplicial/nebulous/f/ae;

    goto :goto_0

    .line 104
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->d:Lsoftware/simplicial/nebulous/f/ae;

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0256
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 55
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 57
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ax;->a()Lsoftware/simplicial/nebulous/f/ae;

    move-result-object v3

    .line 58
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 59
    const-wide/16 v4, 0x0

    .line 60
    sget-object v0, Lsoftware/simplicial/nebulous/f/ae;->e:Lsoftware/simplicial/nebulous/f/ae;

    if-ne v3, v0, :cond_0

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ax;->a(I)I

    move-result v0

    int-to-long v4, v0

    .line 62
    :cond_0
    const-string v7, ""

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 65
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-virtual/range {v1 .. v7}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/nebulous/f/ae;JLjava/lang/String;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 72
    :cond_2
    :goto_0
    return-void

    .line 68
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 32
    const v0, 0x7f040056

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 34
    const v0, 0x7f0d012f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->b:Landroid/widget/RadioGroup;

    .line 35
    const v0, 0x7f0d025b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->c:Landroid/widget/EditText;

    .line 36
    const v0, 0x7f0d023f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->d:Landroid/widget/Button;

    .line 37
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->e:Landroid/widget/Button;

    .line 39
    return-object v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ax;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    return-void
.end method
