.class Lsoftware/simplicial/nebulous/application/n$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/n;->a(ILjava/lang/String;[BLjava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:[B

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Z

.field final synthetic g:Lsoftware/simplicial/nebulous/application/n;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/n;IILjava/lang/String;[BLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iput p2, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    iput p3, p0, Lsoftware/simplicial/nebulous/application/n$5;->b:I

    iput-object p4, p0, Lsoftware/simplicial/nebulous/application/n$5;->c:Ljava/lang/String;

    iput-object p5, p0, Lsoftware/simplicial/nebulous/application/n$5;->d:[B

    iput-object p6, p0, Lsoftware/simplicial/nebulous/application/n$5;->e:Ljava/lang/String;

    iput-boolean p7, p0, Lsoftware/simplicial/nebulous/application/n$5;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v9, 0x12

    const/4 v8, 0x1

    const/4 v4, -0x1

    const/4 v6, 0x0

    .line 494
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/n;->b(Lsoftware/simplicial/nebulous/application/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/n;->isVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 502
    iget-object v11, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    new-instance v0, Lsoftware/simplicial/nebulous/f/z;

    sget-object v1, Lsoftware/simplicial/nebulous/f/z$a;->a:Lsoftware/simplicial/nebulous/f/z$a;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/n$5;->b:I

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/n$5;->c:Ljava/lang/String;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/n$5;->d:[B

    sget-object v7, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v8, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/n$5;->e:Ljava/lang/String;

    iget-boolean v10, p0, Lsoftware/simplicial/nebulous/application/n$5;->f:Z

    invoke-direct/range {v0 .. v10}, Lsoftware/simplicial/nebulous/f/z;-><init>(Lsoftware/simplicial/nebulous/f/z$a;IIILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;Z)V

    invoke-static {v11, v0}, Lsoftware/simplicial/nebulous/application/n;->a(Lsoftware/simplicial/nebulous/application/n;Lsoftware/simplicial/nebulous/f/z;)V

    goto :goto_0

    .line 506
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->e:Ljava/lang/String;

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 507
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->c:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n$5;->d:[B

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v2

    .line 509
    :goto_1
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/16 v3, 0x32

    if-lt v0, v3, :cond_3

    .line 511
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 512
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 513
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 514
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    goto :goto_1

    .line 516
    :cond_3
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->h()Ljava/util/List;

    move-result-object v0

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 520
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c00fb

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 522
    const-string v0, ""

    .line 523
    iget v5, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    if-eq v5, v4, :cond_5

    .line 525
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "["

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 526
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v5, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 527
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    const v5, 0x7f080189

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 528
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "]"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 532
    :cond_5
    new-instance v4, Landroid/text/SpannableString;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/CharSequence;

    aput-object v2, v5, v6

    aput-object v0, v5, v8

    const/4 v6, 0x2

    const-string v7, ": "

    aput-object v7, v5, v6

    const/4 v6, 0x3

    aput-object v1, v5, v6

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 533
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v5

    invoke-virtual {v4, v1, v2, v5, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 534
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 536
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/n$5;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v3, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(IZ)I

    move-result v1

    .line 537
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$5;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/n$5;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {v4, v2, v1, v0, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 539
    :cond_6
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/n;->c(Lsoftware/simplicial/nebulous/application/n;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 540
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 541
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/n;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    if-gt v0, v8, :cond_0

    .line 542
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$5;->g:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/n;->c()V

    goto/16 :goto_0
.end method
