.class public Lsoftware/simplicial/nebulous/application/ab;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:I

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field private g:Landroid/app/ProgressDialog;

.field private h:Z

.field private i:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lsoftware/simplicial/nebulous/application/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 59
    iput v0, p0, Lsoftware/simplicial/nebulous/application/ab;->b:I

    .line 65
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/ab;->h:Z

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ab;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/ab;->h:Z

    return v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ab;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/ab;->h:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/ab;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 7

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    const v4, 0x7f0c00fb

    const v6, 0x7f0c003f

    .line 263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 266
    :cond_0
    iput p1, p0, Lsoftware/simplicial/nebulous/application/ab;->b:I

    .line 267
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 268
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f08023d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 270
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    .line 271
    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v2, Lsoftware/simplicial/a/bp;->b:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v5, v5, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 274
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0800fa

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f080114

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 277
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->c:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 278
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 279
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->e:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 290
    :goto_2
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-super {p0, v0}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 267
    goto/16 :goto_1

    .line 283
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->c:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->e:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_2
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 403
    :cond_0
    invoke-virtual {p0, p2}, Lsoftware/simplicial/nebulous/application/ab;->a(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 410
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 365
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 344
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const v6, 0x7f0801cf

    const v5, 0x7f080102

    .line 308
    invoke-super {p0, p1, p2, p3}, Lsoftware/simplicial/nebulous/application/bc;->onActivityResult(IILandroid/content/Intent;)V

    .line 310
    if-ne p2, v1, :cond_1

    const v2, 0xabcdf0

    if-ne p1, v2, :cond_1

    .line 314
    :try_start_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    const v3, 0x7f08017d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 316
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 318
    const-string v2, "ENGAGEMENT_STATUS"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 319
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 338
    :cond_1
    :goto_1
    return-void

    .line 319
    :sswitch_0
    const-string v3, "CLOSE_FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "ERROR"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v0, "CLOSE_ABORTED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 322
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 333
    :catch_0
    move-exception v0

    .line 335
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 325
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080102

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080102

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 328
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802ee

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aborted"

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 319
    nop

    :sswitch_data_0
    .sparse-switch
        0x3f2d9e8 -> :sswitch_1
        0x4200e448 -> :sswitch_2
        0x75990ef9 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/t$a;->a:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 123
    new-instance v0, Lsoftware/simplicial/nebulous/application/ab$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/ab$2;-><init>(Lsoftware/simplicial/nebulous/application/ab;)V

    invoke-static {v0}, Lcom/fyber/g/b;->a(Lcom/fyber/g/c;)Lcom/fyber/g/b;

    move-result-object v0

    const/4 v1, 0x1

    .line 149
    invoke-virtual {v0, v1}, Lcom/fyber/g/b;->a(Z)Lcom/fyber/g/b;

    move-result-object v0

    const-string v1, "pub0"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    .line 150
    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/fyber/g/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/b;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 151
    invoke-virtual {v0, v1}, Lcom/fyber/g/b;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 155
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ab;->h:Z

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/e;->c()V

    .line 159
    new-instance v0, Lsoftware/simplicial/nebulous/application/ab$3;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/ab$3;-><init>(Lsoftware/simplicial/nebulous/application/ab;)V

    invoke-static {v0}, Lcom/fyber/g/h;->a(Lcom/fyber/g/g;)Lcom/fyber/g/h;

    move-result-object v0

    .line 180
    new-instance v1, Lsoftware/simplicial/nebulous/application/ab$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ab$4;-><init>(Lsoftware/simplicial/nebulous/application/ab;)V

    invoke-static {v1}, Lcom/fyber/g/f;->a(Lcom/fyber/g/c;)Lcom/fyber/g/f;

    move-result-object v1

    .line 215
    invoke-virtual {v1, v0}, Lcom/fyber/g/f;->a(Lcom/fyber/g/h;)Lcom/fyber/g/f;

    move-result-object v0

    .line 216
    invoke-virtual {v0, v2}, Lcom/fyber/g/f;->a(Z)Lcom/fyber/g/f;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 217
    invoke-virtual {v0, v1}, Lcom/fyber/g/f;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 219
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 221
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ah:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 71
    const v0, 0x7f040040

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 72
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f0d0168

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->c:Landroid/widget/Button;

    .line 75
    const v0, 0x7f0d0169

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    .line 76
    const v0, 0x7f0d016a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->e:Landroid/widget/Button;

    .line 77
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->f:Landroid/widget/Button;

    .line 78
    const v0, 0x7f0d00f5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    .line 80
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ab;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 82
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    .line 84
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 296
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 298
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 300
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 302
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lcom/fyber/cache/a;->c(Landroid/content/Context;)V

    .line 303
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 230
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ab;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lcom/fyber/cache/a;->b(Landroid/content/Context;)V

    .line 238
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 242
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    new-instance v2, Lsoftware/simplicial/nebulous/application/ab$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ab$5;-><init>(Lsoftware/simplicial/nebulous/application/ab;)V

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/k;)V

    .line 259
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08017d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ab;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08017e

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ab;->g:Landroid/app/ProgressDialog;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ab$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ab$1;-><init>(Lsoftware/simplicial/nebulous/application/ab;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 109
    return-void
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 359
    return-void
.end method
