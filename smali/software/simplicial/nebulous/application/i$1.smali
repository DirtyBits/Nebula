.class Lsoftware/simplicial/nebulous/application/i$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/i;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/i;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/i;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IILsoftware/simplicial/a/s;I)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/i;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    if-eq p3, v0, :cond_0

    .line 160
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v1, v1, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v1

    .line 161
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 162
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 163
    const/16 v0, 0xd

    invoke-virtual {v2, v0, p4}, Ljava/util/Calendar;->add(II)V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p3, v0}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/s;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 165
    const/4 v3, -0x1

    if-eq p4, v3, :cond_2

    .line 166
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    const v4, 0x7f0803a5

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/i;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/i;->a(Lsoftware/simplicial/nebulous/application/i;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/i;->a(Lsoftware/simplicial/nebulous/application/i;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    sget-object v0, Lsoftware/simplicial/nebulous/application/i$3;->a:[I

    invoke-virtual {p3}, Lsoftware/simplicial/a/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 172
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/i;->a(Lsoftware/simplicial/nebulous/application/i;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 175
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/i;->a(Lsoftware/simplicial/nebulous/application/i;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/i$1;->a:Lsoftware/simplicial/nebulous/application/i;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/i;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
