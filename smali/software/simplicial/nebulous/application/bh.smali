.class public Lsoftware/simplicial/nebulous/application/bh;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/ImageView;

.field c:Landroid/widget/ImageView;

.field d:Landroid/widget/LinearLayout;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/CheckBox;

.field h:Landroid/widget/TextView;

.field private i:Ljava/lang/String;

.field private j:Lsoftware/simplicial/nebulous/f/o$b;

.field private k:I

.field private l:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lsoftware/simplicial/nebulous/application/bh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    .line 53
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$b;->a:Lsoftware/simplicial/nebulous/f/o$b;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->j:Lsoftware/simplicial/nebulous/f/o$b;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/bh;->k:I

    .line 55
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/bh;->l:J

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bh;)Lsoftware/simplicial/nebulous/f/o$b;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->j:Lsoftware/simplicial/nebulous/f/o$b;

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)Z
    .locals 14

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/16 v9, 0x64

    const/4 v8, 0x1

    const/16 v3, 0x80

    const/4 v2, 0x0

    .line 104
    if-nez p1, :cond_0

    .line 106
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->b:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bh;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f020432

    invoke-static {v1, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 185
    :goto_0
    return v2

    .line 111
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v3, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-ne v0, v3, :cond_2

    move v0, v8

    .line 112
    :goto_1
    if-nez v0, :cond_d

    .line 114
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v0

    int-to-float v1, v1

    div-float/2addr v1, v5

    .line 116
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v4, v0

    int-to-float v4, v4

    div-float/2addr v4, v5

    .line 117
    float-to-int v1, v1

    float-to-int v4, v4

    invoke-static {p1, v1, v4, v0, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 118
    invoke-static {v0, v3, v3, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 122
    :goto_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bh;->g:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_5

    .line 126
    const/16 v1, 0x4000

    new-array v1, v1, [I

    move v4, v2

    move v5, v2

    move v6, v3

    move v7, v3

    .line 127
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    move v4, v2

    move v5, v2

    .line 128
    :goto_3
    array-length v6, v1

    if-ge v4, v6, :cond_3

    .line 130
    aget v6, v1, v4

    shr-int/lit8 v6, v6, 0x18

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0xff

    if-eq v6, v7, :cond_1

    .line 132
    aget v5, v1, v4

    const/high16 v6, -0x1000000

    or-int/2addr v5, v6

    aput v5, v1, v4

    move v5, v8

    .line 128
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    move v0, v2

    .line 111
    goto :goto_1

    .line 136
    :cond_3
    if-eqz v5, :cond_4

    .line 137
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v3, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 138
    :cond_4
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bh;->g:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 142
    :cond_5
    const/4 v3, 0x0

    .line 143
    new-array v10, v8, [I

    .line 145
    const/4 v1, -0x1

    move v4, v1

    move-object v6, v3

    move v1, v9

    move v3, v2

    .line 151
    :goto_4
    add-int v5, v3, v1

    div-int/lit8 v5, v5, 0x2

    .line 152
    if-gez v5, :cond_6

    move v5, v2

    .line 153
    :cond_6
    if-le v5, v9, :cond_7

    move v5, v9

    .line 154
    :cond_7
    if-ne v5, v4, :cond_9

    .line 174
    :cond_8
    :goto_5
    if-nez v6, :cond_b

    .line 176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080102

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f0803a7

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801cf

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v3, v4}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 155
    :cond_9
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->WEBP:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {v0, v7, v5, v10}, Lsoftware/simplicial/nebulous/f/o;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;I[I)Ljava/lang/String;

    move-result-object v7

    .line 156
    sget-object v11, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Encoded "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    aget v13, v10, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " bytes @ "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " quality"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 157
    aget v11, v10, v2

    sget v12, Lsoftware/simplicial/nebulous/f/o;->a:I

    if-le v11, v12, :cond_a

    .line 159
    add-int/lit8 v1, v5, -0x1

    .line 160
    sget-object v7, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v11, "Encoding not acceptable"

    invoke-static {v7, v11}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 172
    :goto_6
    if-eq v4, v5, :cond_8

    move v4, v5

    goto :goto_4

    .line 164
    :cond_a
    sget-object v3, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v6, "Encoding acceptable"

    invoke-static {v3, v6}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 166
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bh;->h:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v11, 0x7f0800c4

    invoke-virtual {p0, v11}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, " "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "%"

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    aget v3, v10, v2

    sget v6, Lsoftware/simplicial/nebulous/f/o;->a:I

    if-ge v3, v6, :cond_c

    .line 168
    add-int/lit8 v3, v5, 0x1

    move-object v6, v7

    goto :goto_6

    .line 180
    :cond_b
    iput-object v6, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/o;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bh;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v2, v8

    .line 185
    goto/16 :goto_0

    :cond_c
    move-object v6, v7

    goto/16 :goto_5

    :cond_d
    move-object v0, p1

    goto/16 :goto_2
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bh;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bh;->k:I

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/bh;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->e:Landroid/widget/Button;

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bh;->a(Landroid/graphics/Bitmap;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 255
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0801e6

    .line 191
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 197
    const v0, 0x1080027

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 198
    const v0, 0x7f0800c6

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080030

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0800d3

    .line 200
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/application/bh;->l:J

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->j:Lsoftware/simplicial/nebulous/f/o$b;

    sget-object v3, Lsoftware/simplicial/nebulous/f/o$b;->b:Lsoftware/simplicial/nebulous/f/o$b;

    if-ne v0, v3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f08008d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 204
    :try_start_0
    new-instance v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 205
    const/high16 v2, 0x42c80000    # 100.0f

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v2, v3}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v2

    float-to-int v2, v2

    .line 206
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bh;->i:Ljava/lang/String;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/o;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v2, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 207
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bh;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 208
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    :goto_2
    const v0, 0x7f0801e0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/bh$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bh$1;-><init>(Lsoftware/simplicial/nebulous/application/bh;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 241
    const v0, 0x7f08005d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 242
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 200
    :cond_2
    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/bh;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 210
    :catch_0
    move-exception v0

    .line 212
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 245
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 247
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x0

    .line 60
    const v0, 0x7f04005f

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    const v0, 0x7f0d00d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->b:Landroid/widget/ImageView;

    .line 63
    const v0, 0x7f0d02aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->c:Landroid/widget/ImageView;

    .line 64
    const v0, 0x7f0d02ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->d:Landroid/widget/LinearLayout;

    .line 65
    const v0, 0x7f0d02ad

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->e:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->f:Landroid/widget/Button;

    .line 67
    const v0, 0x7f0d02ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->g:Landroid/widget/CheckBox;

    .line 68
    const v0, 0x7f0d02a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->h:Landroid/widget/TextView;

    .line 70
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bh;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 72
    const-string v2, "price"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/bh;->l:J

    .line 73
    const-string v2, "index"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lsoftware/simplicial/nebulous/application/bh;->k:I

    .line 74
    invoke-static {}, Lsoftware/simplicial/nebulous/f/o$b;->values()[Lsoftware/simplicial/nebulous/f/o$b;

    move-result-object v2

    const-string v3, "MODE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    aget-object v0, v2, v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->j:Lsoftware/simplicial/nebulous/f/o$b;

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ah:Landroid/graphics/Bitmap;

    .line 76
    if-eqz v0, :cond_0

    .line 78
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bh;->c:Landroid/widget/ImageView;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bh;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->c:Landroid/widget/ImageView;

    const/16 v2, 0xaf

    invoke-static {v2, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 87
    :goto_0
    return-object v1

    .line 83
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bh;->e:Landroid/widget/Button;

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bh;->a(Landroid/graphics/Bitmap;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 100
    return-void
.end method
