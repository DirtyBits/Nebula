.class public Lsoftware/simplicial/nebulous/application/w;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;

.field public static c:Lsoftware/simplicial/nebulous/application/bc$a;

.field private static final g:I


# instance fields
.field d:Z

.field e:Ljava/lang/String;

.field f:[B

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/Button;

.field private o:Landroid/widget/Spinner;

.field private p:Landroid/widget/Spinner;

.field private q:Landroid/widget/GridView;

.field private r:Landroid/widget/RelativeLayout;

.field private s:Landroid/widget/Button;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/view/View;

.field private v:[Z

.field private w:I

.field private x:Lsoftware/simplicial/nebulous/a/x;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 45
    const-class v0, Lsoftware/simplicial/nebulous/application/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/w;->a:Ljava/lang/String;

    .line 46
    const/16 v0, 0x95

    const/16 v1, 0xed

    invoke-static {v2, v2, v0, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lsoftware/simplicial/nebulous/application/w;->g:I

    .line 48
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/w;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 49
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/w;I)I
    .locals 0

    .prologue
    .line 43
    iput p1, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->o:Landroid/widget/Spinner;

    return-object v0
.end method

.method private b()Landroid/widget/ArrayAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f04009d

    invoke-direct {v1, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 321
    const v0, 0x7f08005d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 322
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 323
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->aA:[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->aB:[[B

    aget-object v3, v3, v0

    invoke-static {v2, v3}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 324
    :cond_0
    return-object v1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/w;->b()Landroid/widget/ArrayAdapter;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->p:Landroid/widget/Spinner;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 329
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v1, :cond_0

    const-string v0, "COLOR_PLAYER_NAME"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "COLOR_CLAN_NAME"

    goto :goto_0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/w;)[Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->v:[Z

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/application/w;)I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    return v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/application/w;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/w;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/application/w;)Lsoftware/simplicial/nebulous/a/x;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->x:Lsoftware/simplicial/nebulous/a/x;

    return-object v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/application/w;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->t:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 389
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-super {p0, v0}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 392
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v1, :cond_1

    .line 394
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->h:Landroid/widget/TextView;

    const v1, 0x7f0801a6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/w;->f:[B

    invoke-static {v1, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    :goto_0
    move v1, v2

    .line 403
    :goto_1
    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 404
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/w;->v:[Z

    aget-boolean v4, v4, v1

    if-eqz v4, :cond_0

    .line 405
    new-instance v4, Landroid/text/style/BackgroundColorSpan;

    sget v5, Lsoftware/simplicial/nebulous/application/w;->g:I

    invoke-direct {v4, v5}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    add-int/lit8 v5, v1, 0x1

    const/16 v6, 0x12

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 403
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 399
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->h:Landroid/widget/TextView;

    const v1, 0x7f080067

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->f:[B

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    .line 401
    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 400
    invoke-static {v0, v1, v4, v5}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v0

    goto :goto_0

    .line 407
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->j:Landroid/widget/TextView;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    if-ltz v0, :cond_3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget v4, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->s:Landroid/widget/Button;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/w;->w:I

    if-ltz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 412
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->u:Landroid/view/View;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    if-eqz v0, :cond_5

    move v0, v3

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 413
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->r:Landroid/widget/RelativeLayout;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    if-eqz v1, :cond_6

    :goto_5
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->m:Landroid/widget/Button;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 415
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->n:Landroid/widget/Button;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/w;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 416
    return-void

    .line 409
    :cond_3
    const-string v0, "---"

    goto :goto_2

    :cond_4
    move v0, v2

    .line 410
    goto :goto_3

    :cond_5
    move v0, v2

    .line 412
    goto :goto_4

    :cond_6
    move v3, v2

    .line 413
    goto :goto_5
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 77
    const v0, 0x7f04003b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 79
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 81
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->e:Ljava/lang/String;

    .line 82
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ax:[B

    :goto_1
    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->f:[B

    .line 83
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v2, :cond_2

    const/16 v0, 0x10

    :goto_2
    new-array v0, v0, [Z

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->v:[Z

    .line 85
    const v0, 0x7f0d011f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->h:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0d009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->i:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0d0146

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->q:Landroid/widget/GridView;

    .line 88
    const v0, 0x7f0d0087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->j:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->k:Landroid/widget/Button;

    .line 90
    const v0, 0x7f0d014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->l:Landroid/widget/Button;

    .line 91
    const v0, 0x7f0d012b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->m:Landroid/widget/Button;

    .line 92
    const v0, 0x7f0d0148

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->n:Landroid/widget/Button;

    .line 93
    const v0, 0x7f0d0149

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->o:Landroid/widget/Spinner;

    .line 94
    const v0, 0x7f0d0147

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->p:Landroid/widget/Spinner;

    .line 95
    const v0, 0x7f0d0144

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->r:Landroid/widget/RelativeLayout;

    .line 96
    const v0, 0x7f0d0145

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->s:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0d0142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->t:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0d0141

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->u:Landroid/view/View;

    .line 100
    return-object v1

    .line 81
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    goto/16 :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->aa:[B

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto/16 :goto_1

    .line 83
    :cond_2
    const/16 v0, 0xb

    goto/16 :goto_2
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 384
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 385
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 336
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 338
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->t:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/w$8;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/w$8;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$x;)V

    .line 379
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x7

    .line 106
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 108
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->m:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$1;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->n:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$2;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->k:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$3;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->l:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$4;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->s:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$5;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 239
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->i:Landroid/widget/TextView;

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$6;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$6;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 288
    const/4 v0, 0x3

    move v3, v0

    :goto_0
    if-ltz v3, :cond_2

    move v2, v1

    .line 289
    :goto_1
    if-ltz v2, :cond_1

    move v0, v1

    .line 290
    :goto_2
    if-ltz v0, :cond_0

    .line 291
    shl-int/lit8 v5, v2, 0x2

    or-int/2addr v5, v3

    shl-int/lit8 v6, v0, 0x5

    or-int/2addr v5, v6

    int-to-byte v5, v5

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 289
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    .line 288
    :cond_1
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    .line 292
    :cond_2
    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 293
    new-instance v0, Lsoftware/simplicial/nebulous/a/x;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/x;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->x:Lsoftware/simplicial/nebulous/a/x;

    .line 294
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Byte;

    .line 295
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/w;->x:Lsoftware/simplicial/nebulous/a/x;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/a/x;->add(Ljava/lang/Object;)V

    goto :goto_3

    .line 296
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->q:Landroid/widget/GridView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/w;->x:Lsoftware/simplicial/nebulous/a/x;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 297
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w;->q:Landroid/widget/GridView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/w$7;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/w$7;-><init>(Lsoftware/simplicial/nebulous/application/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 315
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/w;->a()V

    .line 316
    return-void
.end method
