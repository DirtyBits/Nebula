.class Lsoftware/simplicial/nebulous/application/a$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/a;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/a;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/a;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 565
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 568
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 589
    :goto_0
    return-void

    .line 573
    :cond_0
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 574
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 575
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 578
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 579
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/a$4;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 585
    :catch_0
    move-exception v0

    .line 587
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 583
    :cond_1
    :try_start_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/a$4;->b:Lsoftware/simplicial/nebulous/application/a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/a;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
