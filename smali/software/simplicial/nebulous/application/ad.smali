.class public Lsoftware/simplicial/nebulous/application/ad;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Spinner;

.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Spinner;

.field g:Landroid/widget/Spinner;

.field h:Landroid/widget/Spinner;

.field i:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lsoftware/simplicial/nebulous/application/ad;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ad;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 238
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 239
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->h:Landroid/widget/Spinner;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->R:I

    add-int/lit8 v2, v2, -0x5

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 241
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->g:Landroid/widget/Spinner;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 242
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->f:Landroid/widget/Spinner;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 243
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 249
    :goto_4
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    invoke-virtual {v0}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 241
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 242
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 245
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_4

    .line 248
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_4
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 257
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 259
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 261
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    .line 262
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    .line 263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    .line 264
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    .line 265
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    .line 266
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/16 v1, 0xa

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->R:I

    .line 267
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ad;->a()V

    .line 269
    :cond_1
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 43
    const v0, 0x7f040043

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 45
    const v0, 0x7f0d012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    .line 46
    const v0, 0x7f0d013a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    .line 47
    const v0, 0x7f0d014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->d:Landroid/widget/Button;

    .line 48
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->e:Landroid/widget/Button;

    .line 49
    const v0, 0x7f0d0119

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->f:Landroid/widget/Spinner;

    .line 50
    const v0, 0x7f0d0177

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->g:Landroid/widget/Spinner;

    .line 51
    const v0, 0x7f0d0175

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    .line 52
    const v0, 0x7f0d0178

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->h:Landroid/widget/Spinner;

    .line 54
    return-object v1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/16 v11, 0x1b

    const/4 v4, 0x1

    const v10, 0x7f0801a9

    const/4 v3, 0x0

    const v9, 0x7f04009e

    .line 60
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 62
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 63
    invoke-virtual {p0, v10}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 64
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 67
    if-ne v2, v4, :cond_5

    .line 69
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 70
    check-cast v0, Landroid/text/SpannableString;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    const/16 v7, 0xff

    const/16 v8, 0xa5

    invoke-static {v7, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const/16 v8, 0x12

    invoke-virtual {v0, v6, v3, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 72
    :goto_1
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2, v9, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->b:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$1;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 96
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 97
    invoke-virtual {p0, v10}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v3

    .line 98
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 100
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v5, v9, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$2;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 123
    :goto_3
    const/16 v1, 0xf

    if-gt v3, v1, :cond_2

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v3, 0x5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 125
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ad;->h:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v9, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->h:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$3;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 145
    invoke-virtual {p0, v10}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 146
    :goto_4
    if-gt v0, v11, :cond_3

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 148
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->g:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v9, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->g:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$4;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 171
    invoke-virtual {p0, v10}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 172
    :goto_5
    if-gt v0, v11, :cond_4

    .line 173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 174
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->f:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v9, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->f:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$5;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-virtual {p0, v10}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    const v1, 0x7f08028c

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const v1, 0x7f080186

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ad;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ad;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v9, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->i:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ad$6;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ad$6;-><init>(Lsoftware/simplicial/nebulous/application/ad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 230
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ad;->a()V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ad;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    return-void

    :cond_5
    move-object v1, v0

    goto/16 :goto_1
.end method
