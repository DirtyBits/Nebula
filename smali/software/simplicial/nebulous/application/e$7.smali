.class Lsoftware/simplicial/nebulous/application/e$7;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/e;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/e;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/e;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v7, 0x8

    .line 319
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v2, :cond_1

    .line 380
    :cond_0
    :goto_0
    return v0

    .line 322
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v2

    iget-object v2, v2, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v3, Lsoftware/simplicial/nebulous/f/i;->j:Lsoftware/simplicial/nebulous/f/i;

    if-ne v2, v3, :cond_0

    .line 324
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v2

    invoke-virtual {v2, p3}, Lsoftware/simplicial/nebulous/a/d;->d(I)Lsoftware/simplicial/a/bd;

    move-result-object v2

    .line 326
    iget-byte v3, v2, Lsoftware/simplicial/a/bd;->c:B

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/application/e;->c(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/a/bd;->a(BLjava/util/Map;)Z

    move-result v3

    .line 328
    if-nez v3, :cond_2

    .line 329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    const-string v3, ""

    invoke-static {v0, v2, v3}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/bd;Ljava/lang/String;)V

    :goto_1
    move v0, v1

    .line 378
    goto :goto_0

    .line 332
    :cond_2
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 334
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f080245

    invoke-virtual {v4, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 335
    new-instance v4, Landroid/widget/EditText;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v5}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 336
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v5, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(Lsoftware/simplicial/a/bd;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 337
    invoke-virtual {v4, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 338
    new-array v5, v1, [Landroid/text/InputFilter;

    new-instance v6, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v6, v7}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v6, v5, v0

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 339
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 341
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f0801cf

    invoke-virtual {v0, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lsoftware/simplicial/nebulous/application/e$7$1;

    invoke-direct {v5, p0, v4, v2}, Lsoftware/simplicial/nebulous/application/e$7$1;-><init>(Lsoftware/simplicial/nebulous/application/e$7;Landroid/widget/EditText;Lsoftware/simplicial/a/bd;)V

    invoke-virtual {v3, v0, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 369
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 371
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 374
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 375
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/e$7;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 376
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_1
.end method
