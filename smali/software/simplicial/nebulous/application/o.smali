.class public Lsoftware/simplicial/nebulous/application/o;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lsoftware/simplicial/a/w;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/EditText;

.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lsoftware/simplicial/nebulous/application/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const v5, 0x7f04009e

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 91
    const/16 v1, 0x14

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    .line 95
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 96
    const/4 v0, 0x2

    :goto_0
    if-gt v0, v1, :cond_1

    .line 97
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 100
    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 101
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->G:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 106
    return-void
.end method

.method private c()Z
    .locals 4

    .prologue
    const v3, 0x7f0801ad

    const/4 v0, 0x0

    .line 111
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-static {v1}, Lsoftware/simplicial/a/ba;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/o;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 117
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/o;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 121
    :goto_0
    return v0

    .line 120
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->L:Ljava/lang/String;

    .line 121
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 166
    if-nez v0, :cond_0

    .line 189
    :goto_0
    return-void

    .line 169
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/o$1;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/o$1;-><init>(Lsoftware/simplicial/nebulous/application/o;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 18

    .prologue
    .line 72
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/o;->d:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 74
    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/o;->c()Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/o;->f:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v4, v4, Lsoftware/simplicial/nebulous/f/ag;->G:I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 78
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v7

    sget-object v8, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    sget-object v9, Lsoftware/simplicial/a/ac;->a:Lsoftware/simplicial/a/ac;

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v11, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    const/4 v12, 0x1

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/o;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lsoftware/simplicial/a/ap;->c:Lsoftware/simplicial/a/ap;

    const v13, 0x417a6666    # 15.65f

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    .line 79
    invoke-static {v14}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/ap;)S

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-short v15, v15, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move/from16 v16, v0

    const/16 v17, 0x0

    .line 77
    invoke-virtual/range {v1 .. v17}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;Ljava/lang/String;Lsoftware/simplicial/a/ap;FSSZ[Z)V

    .line 81
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/o;->e:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 83
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 45
    const v0, 0x7f040032

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    const v0, 0x7f0d0118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    .line 48
    const v0, 0x7f0d011a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    .line 49
    const v0, 0x7f0d011c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->d:Landroid/widget/Button;

    .line 50
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->e:Landroid/widget/Button;

    .line 51
    const v0, 0x7f0d011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->f:Landroid/widget/CheckBox;

    .line 53
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->c:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    add-int/lit8 v1, p3, 0x2

    if-ne v0, v1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    add-int/lit8 v1, p3, 0x2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->G:I

    .line 132
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/o;->b()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 140
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 159
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 145
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/o;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/o;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/o;->b()V

    .line 67
    return-void
.end method
