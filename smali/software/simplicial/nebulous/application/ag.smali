.class public Lsoftware/simplicial/nebulous/application/ag;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/ag$b;,
        Lsoftware/simplicial/nebulous/application/ag$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field A:Landroid/widget/Button;

.field B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field C:Landroid/widget/Button;

.field D:Landroid/widget/Button;

.field E:Landroid/widget/Button;

.field F:Landroid/widget/Button;

.field G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field H:Landroid/widget/Button;

.field I:Landroid/widget/Button;

.field J:Landroid/widget/Button;

.field K:Landroid/widget/Button;

.field L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field M:Landroid/widget/Button;

.field N:Landroid/widget/ImageButton;

.field O:Landroid/widget/ImageButton;

.field P:Landroid/widget/LinearLayout;

.field Q:Landroid/widget/Spinner;

.field R:Landroid/widget/Spinner;

.field S:Landroid/widget/LinearLayout;

.field T:Landroid/widget/CheckBox;

.field private V:Z

.field private W:I

.field private X:I

.field private Y:Lsoftware/simplicial/nebulous/application/ag$a;

.field private Z:Z

.field private aa:Lsoftware/simplicial/a/bn;

.field private ab:Lsoftware/simplicial/a/g/f;

.field private ac:Lsoftware/simplicial/nebulous/application/ag$b;

.field private ad:Lsoftware/simplicial/a/am;

.field private ae:Lsoftware/simplicial/a/b/d;

.field private af:Lsoftware/simplicial/a/h/f;

.field private ag:Lsoftware/simplicial/a/c/g;

.field private ah:I

.field private ai:Landroid/widget/AdapterView$OnItemSelectedListener;

.field c:Landroid/widget/TableLayout;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field m:Landroid/widget/Button;

.field n:Landroid/widget/Button;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/Button;

.field t:Landroid/widget/Button;

.field u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field v:Landroid/widget/Button;

.field w:Landroid/widget/Button;

.field x:Landroid/widget/Button;

.field y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field z:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lsoftware/simplicial/nebulous/application/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag;->a:Ljava/lang/String;

    .line 64
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 109
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/ag;->V:Z

    .line 110
    const/16 v0, 0xa

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    .line 112
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->a:Lsoftware/simplicial/nebulous/application/ag$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    .line 113
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    .line 114
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    .line 115
    sget-object v0, Lsoftware/simplicial/a/g/f;->a:Lsoftware/simplicial/a/g/f;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    .line 116
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$b;->a:Lsoftware/simplicial/nebulous/application/ag$b;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    .line 117
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ad:Lsoftware/simplicial/a/am;

    .line 118
    sget-object v0, Lsoftware/simplicial/a/b/d;->c:Lsoftware/simplicial/a/b/d;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ae:Lsoftware/simplicial/a/b/d;

    .line 119
    sget-object v0, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->af:Lsoftware/simplicial/a/h/f;

    .line 120
    sget-object v0, Lsoftware/simplicial/a/c/g;->d:Lsoftware/simplicial/a/c/g;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ag:Lsoftware/simplicial/a/c/g;

    .line 121
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    .line 123
    new-instance v0, Lsoftware/simplicial/nebulous/application/ag$1;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/ag$1;-><init>(Lsoftware/simplicial/nebulous/application/ag;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ai:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method private a()V
    .locals 13

    .prologue
    const/16 v5, 0x11

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/16 v8, 0xa

    const/4 v1, -0x1

    .line 831
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 832
    new-instance v0, Landroid/widget/TableRow;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 833
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 834
    const v3, 0x7f08017d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 835
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 836
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 837
    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 838
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v2, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    move v0, v9

    .line 839
    :goto_0
    if-ge v0, v8, :cond_0

    .line 841
    new-instance v2, Landroid/widget/TableRow;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 842
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 843
    const-string v4, " "

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 844
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 845
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 846
    const/high16 v4, 0x41200000    # 10.0f

    invoke-virtual {v3, v10, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 847
    invoke-virtual {v2, v3}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 849
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v3, v2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 839
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 852
    :cond_0
    iput v8, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    .line 854
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->h:[I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/ag$a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 898
    :goto_1
    return-void

    .line 857
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_1

    .line 858
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->ad:Lsoftware/simplicial/a/am;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    iget v6, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/a/bn;Lsoftware/simplicial/a/am;ZLsoftware/simplicial/a/g/f;IILsoftware/simplicial/nebulous/f/w;)V

    goto :goto_1

    .line 863
    :cond_1
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->ad:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/am;ZII)Ljava/util/List;

    move-result-object v0

    .line 864
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 865
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/c;

    .line 866
    new-instance v3, Lsoftware/simplicial/a/g/d;

    iget-object v4, v0, Lsoftware/simplicial/a/g/c;->a:Ljava/lang/CharSequence;

    iget v0, v0, Lsoftware/simplicial/a/g/c;->b:I

    invoke-direct {v3, v4, v0}, Lsoftware/simplicial/a/g/d;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 869
    :catch_0
    move-exception v0

    .line 871
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "Error"

    const-string v2, "Failed to retrieve single player high scores"

    const-string v3, "OK"

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 867
    :cond_2
    :try_start_1
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 876
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_3

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/f/al;->a(ILsoftware/simplicial/a/g/f;IILsoftware/simplicial/nebulous/f/w;)V

    goto/16 :goto_1

    .line 879
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_4

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_4
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/f/al;->b(ILsoftware/simplicial/a/g/f;IILsoftware/simplicial/nebulous/f/w;)V

    goto/16 :goto_1

    .line 882
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_5

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_5
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->ad:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->ag:Lsoftware/simplicial/a/c/g;

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    iget v8, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v9, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    move-object v10, p0

    invoke-virtual/range {v0 .. v10}, Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/a/bn;Lsoftware/simplicial/a/am;ZLsoftware/simplicial/a/c/g;IIILsoftware/simplicial/nebulous/f/w;)V

    goto/16 :goto_1

    .line 886
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_6

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_6
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->ae:Lsoftware/simplicial/a/b/d;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    iget v6, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v8, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    iget v11, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    const/4 v12, 0x2

    if-lt v11, v12, :cond_7

    iget v11, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    const/16 v12, 0xd

    if-gt v11, v12, :cond_7

    :goto_3
    move-object v9, p0

    invoke-virtual/range {v0 .. v10}, Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/a/bn;Lsoftware/simplicial/a/b/d;ZIIILsoftware/simplicial/nebulous/f/w;Z)V

    goto/16 :goto_1

    :cond_7
    move v10, v9

    goto :goto_3

    .line 890
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$b;->a:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v2, :cond_8

    move v10, v8

    :cond_8
    iput v10, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    .line 891
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_9

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_9
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->af:Lsoftware/simplicial/a/h/f;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    iget v6, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    move-object v9, p0

    invoke-virtual/range {v0 .. v9}, Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/a/bn;Lsoftware/simplicial/a/h/f;ZIIILsoftware/simplicial/nebulous/f/w;)V

    goto/16 :goto_1

    .line 894
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v2, v3, :cond_a

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    :cond_a
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    iget v4, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v5, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    iget-boolean v7, p0, Lsoftware/simplicial/nebulous/application/ag;->V:Z

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lsoftware/simplicial/nebulous/f/al;->a(IZLsoftware/simplicial/a/bn;IIZZLsoftware/simplicial/nebulous/f/w;)V

    goto/16 :goto_1

    .line 854
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 1627
    iput p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    .line 1628
    return-void
.end method

.method private a(Lsoftware/simplicial/a/am;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 437
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ad:Lsoftware/simplicial/a/am;

    .line 439
    iput v3, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 441
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 442
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 444
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 445
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 447
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 448
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    invoke-static {p1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->ai:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 451
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->d:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne v0, v1, :cond_2

    .line 453
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->e:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/am;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 484
    :cond_2
    :goto_2
    return-void

    .line 456
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 462
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 463
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 464
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 467
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 468
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 469
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 472
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 474
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 477
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 453
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/b/d;)V
    .locals 4

    .prologue
    const v3, 0x7f020235

    .line 378
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ae:Lsoftware/simplicial/a/b/d;

    .line 380
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 382
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 383
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 385
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 394
    :goto_1
    return-void

    .line 388
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 391
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->F:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/bn;Z)V
    .locals 4

    .prologue
    const v2, 0x7f020235

    .line 511
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->aa:Lsoftware/simplicial/a/bn;

    .line 512
    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/application/ag;->Z:Z

    .line 514
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 516
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 517
    if-eqz p2, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    const v1, 0x7f020236

    goto :goto_1

    .line 519
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->g:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/bn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 543
    :goto_2
    return-void

    .line 522
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->o:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 525
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->n:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 528
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->p:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 531
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 534
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->r:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 537
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->s:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 540
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_2

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/c/g;)V
    .locals 4

    .prologue
    const v3, 0x7f020235

    .line 352
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ag:Lsoftware/simplicial/a/c/g;

    .line 354
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 357
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 359
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->b:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/c/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 374
    :goto_1
    return-void

    .line 362
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 365
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 368
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 371
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/g/f;)V
    .locals 4

    .prologue
    const v3, 0x7f020235

    .line 488
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    .line 490
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 492
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 493
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 495
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->f:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/g/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 507
    :goto_1
    return-void

    .line 498
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->v:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 501
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->w:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 504
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->x:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 495
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/a/h/f;)V
    .locals 4

    .prologue
    const v3, 0x7f020235

    .line 417
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->af:Lsoftware/simplicial/a/h/f;

    .line 419
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 421
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 422
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 424
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->d:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/h/f;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 433
    :goto_1
    return-void

    .line 427
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 430
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->E:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 424
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/nebulous/application/ag$a;)V
    .locals 6

    .prologue
    const v5, 0x7f020235

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 547
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    .line 549
    iput v2, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 551
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 552
    const v4, 0x7f020236

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 554
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 555
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 557
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->h:[I

    invoke-virtual {p1}, Lsoftware/simplicial/nebulous/application/ag$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 704
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 705
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$b;->a:Lsoftware/simplicial/nebulous/application/ag$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$b;)V

    .line 707
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne p1, v0, :cond_2d

    .line 708
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/b/d;)V

    .line 715
    :goto_2
    return-void

    .line 560
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->e:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 562
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 563
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v1, v5, :cond_2

    move v1, v2

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    :cond_2
    move v1, v3

    goto :goto_4

    .line 564
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 565
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v5, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v1, v5, :cond_4

    move v1, v2

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    :cond_4
    move v1, v3

    goto :goto_6

    .line 566
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 567
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_7

    .line 568
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 569
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_8

    .line 570
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 572
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_9

    .line 573
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 576
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->f:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 578
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 579
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_a

    .line 580
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 581
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_b

    .line 582
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 583
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_c

    .line 584
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v1, :cond_c

    :goto_d
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 585
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 586
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_e

    :cond_c
    move v2, v3

    .line 584
    goto :goto_d

    .line 587
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 589
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_f

    .line 591
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 594
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->g:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 596
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 597
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_10

    .line 598
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 599
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_11

    .line 600
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 601
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_12

    .line 602
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v1, :cond_12

    :goto_13
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 603
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 604
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_14

    :cond_12
    move v2, v3

    .line 602
    goto :goto_13

    .line 605
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 607
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_15

    .line 609
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 613
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->h:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 615
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 616
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_16

    .line 617
    :cond_15
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 618
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_17

    .line 619
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 620
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_18

    .line 621
    :cond_17
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v2

    :goto_19
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 623
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1a

    :cond_18
    move v0, v3

    .line 621
    goto :goto_19

    .line 624
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 625
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 626
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1b

    .line 628
    :cond_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 631
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 633
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ag:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    goto/16 :goto_1

    .line 636
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->i:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 638
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 639
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1c

    .line 640
    :cond_1b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 641
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1d

    .line 642
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 643
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1e

    .line 644
    :cond_1d
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_1e

    move v0, v2

    :goto_1f
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 645
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 646
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_20

    :cond_1e
    move v0, v3

    .line 644
    goto :goto_1f

    .line 647
    :cond_1f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 648
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 649
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->F:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 650
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 651
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_21

    .line 653
    :cond_20
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 654
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 656
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 659
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 661
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_22
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 662
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_22

    .line 663
    :cond_21
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_23
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 664
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_23

    .line 665
    :cond_22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 666
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_24

    .line 667
    :cond_23
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_24

    move v0, v2

    :goto_25
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 669
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_26

    :cond_24
    move v0, v3

    .line 667
    goto :goto_25

    .line 670
    :cond_25
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->E:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_27
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 674
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_27

    .line 676
    :cond_26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 677
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 679
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 682
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->k:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 684
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_28
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 685
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_28

    .line 686
    :cond_27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_29
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 687
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_29

    .line 688
    :cond_28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 689
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2a

    .line 690
    :cond_29
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_2a

    move v0, v2

    :goto_2b
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 691
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 692
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2c

    :cond_2a
    move v0, v3

    .line 690
    goto :goto_2b

    .line 693
    :cond_2b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 694
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 695
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2d

    .line 697
    :cond_2c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 698
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 709
    :cond_2d
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne p1, v0, :cond_2e

    .line 710
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->al:Lsoftware/simplicial/a/h/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/h/f;)V

    goto/16 :goto_2

    .line 711
    :cond_2e
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne p1, v0, :cond_2f

    .line 712
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/ag;->V:Z

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Z)V

    goto/16 :goto_2

    .line 714
    :cond_2f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/am;)V

    goto/16 :goto_2

    .line 557
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lsoftware/simplicial/nebulous/application/ag$b;)V
    .locals 5

    .prologue
    const v4, 0x7f020235

    const/16 v1, 0xa

    .line 330
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    .line 332
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 334
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 335
    const v3, 0x7f020236

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 337
    :cond_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$4;->a:[I

    invoke-virtual {p1}, Lsoftware/simplicial/nebulous/application/ag$b;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 348
    :goto_1
    return-void

    .line 340
    :pswitch_0
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    .line 341
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->z:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    .line 344
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_2
    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    .line 345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1

    :cond_1
    move v0, v1

    .line 344
    goto :goto_2

    .line 337
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ag;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ag;->a()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ag;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/ag;->a(I)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ag;Lsoftware/simplicial/a/am;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/am;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const v3, 0x7f020235

    .line 398
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/ag;->V:Z

    .line 400
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 402
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 403
    const v2, 0x7f020236

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    .line 405
    :cond_0
    if-eqz p1, :cond_1

    .line 407
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 413
    :goto_1
    return-void

    .line 411
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 903
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 905
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 906
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 907
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 908
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->add(II)V

    .line 909
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 910
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 911
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 912
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 913
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 914
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 916
    const/4 v1, 0x3

    invoke-static {v3, v1}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v1

    .line 917
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 919
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0802b1

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 920
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/16 v4, -0x100

    const/high16 v8, 0x41200000    # 10.0f

    const/4 v2, 0x1

    const/16 v7, 0x11

    .line 971
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1033
    :cond_0
    return-void

    .line 974
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 976
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 980
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 981
    const v3, 0x7f080337

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 982
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 983
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 984
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 986
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v1, :cond_2

    .line 988
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 989
    const v3, 0x7f080335

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 990
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 991
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 992
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 995
    :cond_2
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 996
    const v3, 0x7f080338

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 997
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 998
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 999
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1000
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1003
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/d;

    .line 1005
    new-instance v4, Landroid/widget/TableRow;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v5}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1006
    new-instance v5, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1007
    iget v6, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    add-int/2addr v6, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1008
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1009
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 1010
    invoke-virtual {v5, v2, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1011
    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1013
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v5, v5, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v5, :cond_3

    .line 1015
    new-instance v5, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1016
    iget-object v6, v0, Lsoftware/simplicial/a/g/d;->a:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1017
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1018
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 1019
    invoke-virtual {v5, v2, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1020
    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1023
    :cond_3
    new-instance v5, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1024
    iget v0, v0, Lsoftware/simplicial/a/g/d;->b:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1026
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 1027
    invoke-virtual {v5, v2, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1028
    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1030
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1031
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1032
    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/e;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1537
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1623
    :cond_0
    return-void

    .line 1540
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1542
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1543
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1544
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1545
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1546
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1547
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1549
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v1, :cond_2

    .line 1551
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1552
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0801f0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1553
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1554
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1555
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1558
    :cond_2
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1559
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080220

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0802eb

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1560
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1561
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1562
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1563
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1565
    const/4 v2, -0x1

    .line 1566
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_8

    .line 1569
    if-eqz p2, :cond_8

    .line 1571
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_6

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/e;

    iget-object v0, v0, Lsoftware/simplicial/a/g/e;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1572
    const/4 v1, 0x5

    .line 1587
    :cond_3
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1589
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/e;

    .line 1591
    if-ne v2, v1, :cond_7

    const/16 v3, -0x100

    .line 1593
    :goto_2
    new-instance v5, Landroid/widget/TableRow;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1594
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1595
    iget v6, v0, Lsoftware/simplicial/a/g/e;->b:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1596
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1597
    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1598
    const/4 v6, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1599
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1601
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v4, :cond_5

    .line 1603
    new-instance v6, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1604
    iget-object v4, v0, Lsoftware/simplicial/a/g/e;->a:Ljava/lang/String;

    .line 1605
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xb

    if-le v7, v8, :cond_4

    .line 1606
    const/4 v7, 0x0

    const/16 v8, 0xb

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1607
    :cond_4
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1608
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1609
    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1610
    const/4 v4, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v6, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1611
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1614
    :cond_5
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1615
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, v0, Lsoftware/simplicial/a/g/e;->f:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lsoftware/simplicial/a/g/e;->c:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lsoftware/simplicial/a/g/e;->e:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v0, v0, Lsoftware/simplicial/a/g/e;->d:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ")"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1616
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1617
    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1618
    const/4 v0, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v4, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1619
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1587
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1575
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1577
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/e;

    iget-object v0, v0, Lsoftware/simplicial/a/g/e;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1575
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1591
    :cond_7
    const/4 v3, -0x1

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto/16 :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1038
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1131
    :cond_0
    return-void

    .line 1041
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1043
    new-instance v1, Landroid/widget/TableRow;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1044
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1045
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1046
    const/16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1047
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1048
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1050
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    .line 1052
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1053
    const v2, 0x7f080335

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1054
    const/16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1055
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1056
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1059
    :cond_2
    new-instance v2, Landroid/widget/TextView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1060
    const v0, 0x7f0803d1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1061
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    sget-object v4, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    if-ne v3, v4, :cond_3

    .line 1062
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f08017a

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1063
    :cond_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1064
    const/16 v0, -0x100

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1065
    const/16 v0, 0x11

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1066
    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1067
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1069
    const/4 v2, -0x1

    .line 1070
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_a

    .line 1072
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    .line 1073
    if-eqz v3, :cond_a

    .line 1075
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_8

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    iget-object v0, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1076
    const/4 v1, 0x5

    .line 1091
    :cond_4
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1093
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    .line 1095
    if-ne v2, v1, :cond_9

    const/16 v3, -0x100

    .line 1097
    :goto_2
    new-instance v5, Landroid/widget/TableRow;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1098
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1099
    iget v6, v0, Lsoftware/simplicial/a/g/h;->d:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1100
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1101
    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1102
    const/4 v6, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1103
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1105
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v4, :cond_6

    .line 1107
    new-instance v6, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1108
    iget-object v4, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    .line 1109
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x10

    if-le v7, v8, :cond_5

    .line 1110
    const/4 v7, 0x0

    const/16 v8, 0x10

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1111
    :cond_5
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1112
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1113
    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1114
    const/4 v4, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v6, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1115
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1118
    :cond_6
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1119
    iget-wide v6, v0, Lsoftware/simplicial/a/g/h;->c:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->a(J)I

    move-result v6

    .line 1120
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v7

    iget-wide v8, v0, Lsoftware/simplicial/a/g/h;->c:J

    invoke-virtual {v7, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1121
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    sget-object v8, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    if-ne v7, v8, :cond_7

    .line 1122
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " ("

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ")"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1123
    :cond_7
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1124
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1125
    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1126
    const/4 v0, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v4, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1127
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1091
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1079
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 1081
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    iget-object v0, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1079
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1095
    :cond_9
    const/4 v3, -0x1

    goto/16 :goto_2

    :cond_a
    move v1, v2

    goto/16 :goto_0
.end method

.method public c(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1222
    :cond_0
    return-void

    .line 1139
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1141
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1142
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1143
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1144
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1145
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1146
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1148
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v1, :cond_2

    .line 1150
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1151
    const v2, 0x7f080335

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1152
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1153
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1154
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1157
    :cond_2
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1158
    const v2, 0x7f0801ec

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1159
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1160
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1161
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1162
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1164
    const/4 v2, -0x1

    .line 1165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_8

    .line 1167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    .line 1168
    if-eqz v3, :cond_8

    .line 1170
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_6

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/g;

    iget-object v0, v0, Lsoftware/simplicial/a/g/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1171
    const/4 v1, 0x5

    .line 1186
    :cond_3
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1188
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/g;

    .line 1190
    if-ne v2, v1, :cond_7

    const/16 v3, -0x100

    .line 1192
    :goto_2
    new-instance v5, Landroid/widget/TableRow;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1193
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1194
    iget v6, v0, Lsoftware/simplicial/a/g/g;->c:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1195
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1196
    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1197
    const/4 v6, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1198
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1200
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v4, :cond_5

    .line 1202
    new-instance v6, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1203
    iget-object v4, v0, Lsoftware/simplicial/a/g/g;->a:Ljava/lang/String;

    .line 1204
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x10

    if-le v7, v8, :cond_4

    .line 1205
    const/4 v7, 0x0

    const/16 v8, 0x10

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1206
    :cond_4
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1207
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1208
    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1209
    const/4 v4, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v6, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1210
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1213
    :cond_5
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1214
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v6

    iget v0, v0, Lsoftware/simplicial/a/g/g;->b:I

    int-to-long v8, v0

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1215
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1216
    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1217
    const/4 v0, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v4, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1218
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1186
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1174
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1176
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/g;

    iget-object v0, v0, Lsoftware/simplicial/a/g/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1190
    :cond_7
    const/4 v3, -0x1

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto/16 :goto_0
.end method

.method public d(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/h;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1227
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1321
    :cond_0
    return-void

    .line 1230
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1232
    new-instance v1, Landroid/widget/TableRow;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1233
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1234
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1235
    const/16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1236
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1237
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1239
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    .line 1241
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1242
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08008d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1243
    const/16 v2, -0x100

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1244
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1245
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1248
    :cond_2
    new-instance v2, Landroid/widget/TextView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1249
    const v0, 0x7f0803d1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1250
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    sget-object v4, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    if-ne v3, v4, :cond_3

    .line 1251
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f08017a

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1252
    :cond_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1253
    const/16 v0, -0x100

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1254
    const/16 v0, 0x11

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1255
    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1256
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1258
    const/4 v2, -0x1

    .line 1259
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_a

    .line 1261
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 1262
    if-eqz v3, :cond_a

    .line 1264
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_8

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    iget-object v0, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1265
    const/4 v1, 0x5

    .line 1280
    :cond_4
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1282
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    .line 1284
    if-ne v2, v1, :cond_9

    const/16 v3, -0x100

    .line 1286
    :goto_2
    new-instance v5, Landroid/widget/TableRow;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1287
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1288
    iget v6, v0, Lsoftware/simplicial/a/g/h;->d:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1289
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1290
    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1291
    const/4 v6, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1292
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1294
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v4, :cond_6

    .line 1296
    new-instance v6, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1297
    iget-object v4, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    .line 1298
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xb

    if-le v7, v8, :cond_5

    .line 1299
    const/4 v7, 0x0

    const/16 v8, 0xb

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1300
    :cond_5
    iget-object v7, v0, Lsoftware/simplicial/a/g/h;->b:[B

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    .line 1301
    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    invoke-interface {v9, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 1300
    invoke-static {v4, v7, v8, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1302
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1303
    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1304
    const/4 v4, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v6, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1305
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1308
    :cond_6
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1309
    iget-wide v6, v0, Lsoftware/simplicial/a/g/h;->c:J

    invoke-static {v6, v7}, Lsoftware/simplicial/a/aw;->b(J)I

    move-result v6

    .line 1310
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v7

    iget-wide v8, v0, Lsoftware/simplicial/a/g/h;->c:J

    invoke-virtual {v7, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1311
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ab:Lsoftware/simplicial/a/g/f;

    sget-object v8, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    if-ne v7, v8, :cond_7

    .line 1312
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " ("

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ")"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1313
    :cond_7
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1314
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1315
    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1316
    const/4 v0, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v4, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1317
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1319
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1280
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1268
    :cond_8
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 1270
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/h;

    iget-object v0, v0, Lsoftware/simplicial/a/g/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1268
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1284
    :cond_9
    const/4 v3, -0x1

    goto/16 :goto_2

    :cond_a
    move v1, v2

    goto/16 :goto_0
.end method

.method public e(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1326
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1413
    :cond_0
    return-void

    .line 1329
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1331
    new-instance v0, Landroid/widget/TableRow;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1332
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1333
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1335
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1336
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1338
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v1, :cond_2

    .line 1340
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f08008d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1342
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1343
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1344
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1347
    :cond_2
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080254

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0802eb

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1349
    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1350
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 1351
    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1352
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1354
    const/4 v2, -0x1

    .line 1355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_8

    .line 1357
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    .line 1358
    if-eqz v3, :cond_8

    .line 1360
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_6

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/b;

    iget-object v0, v0, Lsoftware/simplicial/a/g/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1361
    const/4 v1, 0x5

    .line 1376
    :cond_3
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1378
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/b;

    .line 1380
    if-ne v2, v1, :cond_7

    const/16 v3, -0x100

    .line 1382
    :goto_2
    new-instance v5, Landroid/widget/TableRow;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v4}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1383
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1384
    iget v6, v0, Lsoftware/simplicial/a/g/b;->c:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1385
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1386
    const/16 v6, 0x11

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1387
    const/4 v6, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1388
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1390
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v4, :cond_5

    .line 1392
    new-instance v6, Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1393
    iget-object v4, v0, Lsoftware/simplicial/a/g/b;->a:Ljava/lang/String;

    .line 1394
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xb

    if-le v7, v8, :cond_4

    .line 1395
    const/4 v7, 0x0

    const/16 v8, 0xb

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1396
    :cond_4
    iget-object v7, v0, Lsoftware/simplicial/a/g/b;->b:[B

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    .line 1397
    invoke-interface {v8, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    invoke-interface {v9, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 1396
    invoke-static {v4, v7, v8, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1398
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1399
    const/16 v4, 0x11

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 1400
    const/4 v4, 0x1

    const/high16 v7, 0x41200000    # 10.0f

    invoke-virtual {v6, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1401
    invoke-virtual {v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1404
    :cond_5
    new-instance v4, Landroid/widget/TextView;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v4, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1405
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, v0, Lsoftware/simplicial/a/g/b;->d:I

    iget v8, v0, Lsoftware/simplicial/a/g/b;->e:I

    sub-int/2addr v7, v8

    iget v8, v0, Lsoftware/simplicial/a/g/b;->f:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lsoftware/simplicial/a/g/b;->d:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lsoftware/simplicial/a/g/b;->f:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v0, v0, Lsoftware/simplicial/a/g/b;->e:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ")"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1406
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1407
    const/16 v0, 0x11

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1408
    const/4 v0, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v4, v0, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1409
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1411
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1376
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 1364
    :cond_6
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1366
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/b;

    iget-object v0, v0, Lsoftware/simplicial/a/g/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1364
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1380
    :cond_7
    const/4 v3, -0x1

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto/16 :goto_0
.end method

.method public f(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/g/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/high16 v13, 0x41200000    # 10.0f

    const/16 v12, 0xe

    const/16 v4, -0x100

    const/16 v11, 0x11

    const/4 v10, 0x1

    .line 1418
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 1532
    :cond_0
    return-void

    .line 1421
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 1423
    new-instance v1, Landroid/widget/TableRow;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1424
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1425
    const v2, 0x7f080337

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1426
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1427
    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1428
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1430
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    .line 1432
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1433
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0801f0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1435
    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1436
    invoke-virtual {v1, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1439
    :cond_2
    new-instance v2, Landroid/widget/TextView;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1441
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ae:Lsoftware/simplicial/a/b/d;

    sget-object v3, Lsoftware/simplicial/a/b/d;->b:Lsoftware/simplicial/a/b/d;

    if-ne v0, v3, :cond_9

    .line 1443
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-le v0, v10, :cond_3

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-lt v0, v12, :cond_8

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    const/16 v3, 0x16

    if-gt v0, v3, :cond_8

    .line 1444
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f080254

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f080311

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1455
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1456
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1457
    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1458
    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1459
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1461
    const/4 v2, -0x1

    .line 1462
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_12

    .line 1464
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    .line 1465
    if-eqz v3, :cond_12

    .line 1467
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_c

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/a;

    iget-object v0, v0, Lsoftware/simplicial/a/g/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1468
    const/4 v1, 0x5

    .line 1483
    :cond_4
    :goto_1
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1485
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/a;

    .line 1487
    if-ne v2, v1, :cond_d

    move v3, v4

    .line 1489
    :goto_3
    new-instance v6, Landroid/widget/TableRow;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v6, v5}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 1490
    new-instance v5, Landroid/widget/TextView;

    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1491
    iget v7, v0, Lsoftware/simplicial/a/g/a;->b:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1492
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1493
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1494
    invoke-virtual {v5, v10, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1495
    invoke-virtual {v6, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1497
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v5, v5, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v5, :cond_6

    .line 1499
    new-instance v7, Landroid/widget/TextView;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v7, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1500
    iget-object v5, v0, Lsoftware/simplicial/a/g/a;->a:Ljava/lang/String;

    .line 1501
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    const/16 v9, 0x10

    if-le v8, v9, :cond_5

    .line 1502
    const/4 v8, 0x0

    const/16 v9, 0x10

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1503
    :cond_5
    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1504
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1505
    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1506
    invoke-virtual {v7, v10, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1507
    invoke-virtual {v6, v7}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1510
    :cond_6
    new-instance v5, Landroid/widget/TextView;

    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v5, v7}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1511
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ae:Lsoftware/simplicial/a/b/d;

    sget-object v8, Lsoftware/simplicial/a/b/d;->b:Lsoftware/simplicial/a/b/d;

    if-ne v7, v8, :cond_f

    .line 1513
    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-le v7, v10, :cond_7

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-lt v7, v12, :cond_e

    .line 1514
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Lsoftware/simplicial/a/g/a;->c:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1525
    :goto_4
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1526
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 1527
    invoke-virtual {v5, v10, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1528
    invoke-virtual {v6, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 1530
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    invoke-virtual {v0, v6}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 1483
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 1446
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f080220

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f0802eb

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1450
    :cond_9
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-le v0, v10, :cond_a

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-lt v0, v12, :cond_b

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    const/16 v3, 0x16

    if-gt v0, v3, :cond_b

    .line 1451
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f080254

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f0802eb

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1453
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f080220

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v3, 0x7f0802eb

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1471
    :cond_c
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 1473
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/g/a;

    iget-object v0, v0, Lsoftware/simplicial/a/g/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1471
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1487
    :cond_d
    const/4 v3, -0x1

    goto/16 :goto_3

    .line 1516
    :cond_e
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Lsoftware/simplicial/a/g/a;->f:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1520
    :cond_f
    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-le v7, v10, :cond_10

    iget v7, p0, Lsoftware/simplicial/nebulous/application/ag;->ah:I

    if-lt v7, v12, :cond_11

    .line 1521
    :cond_10
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, v0, Lsoftware/simplicial/a/g/a;->c:I

    iget v9, v0, Lsoftware/simplicial/a/g/a;->d:I

    sub-int/2addr v8, v9

    iget v9, v0, Lsoftware/simplicial/a/g/a;->e:I

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lsoftware/simplicial/a/g/a;->c:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lsoftware/simplicial/a/g/a;->e:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Lsoftware/simplicial/a/g/a;->d:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 1523
    :cond_11
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, v0, Lsoftware/simplicial/a/g/a;->f:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lsoftware/simplicial/a/g/a;->c:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lsoftware/simplicial/a/g/a;->e:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Lsoftware/simplicial/a/g/a;->d:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ")"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_12
    move v1, v2

    goto/16 :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 720
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 721
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->a:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 722
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 723
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->b:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 724
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 725
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->c:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 726
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 727
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->d:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 728
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 729
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 730
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 731
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 732
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 733
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 735
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 736
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v3}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 737
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_8

    .line 738
    sget-object v0, Lsoftware/simplicial/a/bn;->a:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 739
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->n:Landroid/widget/Button;

    if-ne p1, v0, :cond_9

    .line 740
    sget-object v0, Lsoftware/simplicial/a/bn;->b:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 741
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 742
    sget-object v0, Lsoftware/simplicial/a/bn;->f:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 743
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_b

    .line 744
    sget-object v0, Lsoftware/simplicial/a/bn;->c:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 745
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 746
    sget-object v0, Lsoftware/simplicial/a/bn;->e:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 747
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->s:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 748
    sget-object v0, Lsoftware/simplicial/a/bn;->d:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 749
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->t:Landroid/widget/Button;

    if-ne p1, v0, :cond_e

    .line 750
    sget-object v0, Lsoftware/simplicial/a/bn;->g:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 752
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->v:Landroid/widget/Button;

    if-ne p1, v0, :cond_f

    .line 753
    sget-object v0, Lsoftware/simplicial/a/g/f;->a:Lsoftware/simplicial/a/g/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/g/f;)V

    .line 754
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->w:Landroid/widget/Button;

    if-ne p1, v0, :cond_10

    .line 755
    sget-object v0, Lsoftware/simplicial/a/g/f;->b:Lsoftware/simplicial/a/g/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/g/f;)V

    .line 756
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->x:Landroid/widget/Button;

    if-ne p1, v0, :cond_11

    .line 757
    sget-object v0, Lsoftware/simplicial/a/g/f;->c:Lsoftware/simplicial/a/g/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/g/f;)V

    .line 759
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->z:Landroid/widget/Button;

    if-ne p1, v0, :cond_12

    .line 760
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$b;->a:Lsoftware/simplicial/nebulous/application/ag$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$b;)V

    .line 761
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    if-ne p1, v0, :cond_13

    .line 762
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$b;)V

    .line 764
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    if-ne p1, v0, :cond_14

    .line 766
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne v0, v1, :cond_22

    .line 767
    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Z)V

    .line 771
    :cond_14
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->F:Landroid/widget/Button;

    if-ne p1, v0, :cond_15

    .line 773
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne v0, v1, :cond_23

    .line 774
    sget-object v0, Lsoftware/simplicial/a/b/d;->b:Lsoftware/simplicial/a/b/d;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/b/d;)V

    .line 779
    :cond_15
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    if-ne p1, v0, :cond_16

    .line 781
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    if-ne v0, v1, :cond_24

    .line 782
    invoke-direct {p0, v3}, Lsoftware/simplicial/nebulous/application/ag;->a(Z)V

    .line 786
    :cond_16
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->E:Landroid/widget/Button;

    if-ne p1, v0, :cond_17

    .line 787
    sget-object v0, Lsoftware/simplicial/a/h/f;->b:Lsoftware/simplicial/a/h/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/h/f;)V

    .line 789
    :cond_17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    if-ne p1, v0, :cond_18

    .line 790
    sget-object v0, Lsoftware/simplicial/a/c/g;->d:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    .line 791
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    if-ne p1, v0, :cond_19

    .line 792
    sget-object v0, Lsoftware/simplicial/a/c/g;->a:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    .line 793
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    if-ne p1, v0, :cond_1a

    .line 794
    sget-object v0, Lsoftware/simplicial/a/c/g;->b:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    .line 795
    :cond_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    if-ne p1, v0, :cond_1b

    .line 796
    sget-object v0, Lsoftware/simplicial/a/c/g;->c:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    .line 798
    :cond_1b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1c

    .line 800
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 801
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    add-int/2addr v0, v1

    if-gt v0, v4, :cond_1c

    .line 802
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    add-int/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 804
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1d

    .line 806
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 807
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    if-lt v0, v1, :cond_1d

    .line 808
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    sub-int/2addr v0, v1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    .line 810
    :cond_1d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->M:Landroid/widget/Button;

    if-ne p1, v0, :cond_1e

    .line 812
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 815
    :cond_1e
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    if-lez v0, :cond_1f

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_25

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    if-eq v0, v1, :cond_25

    .line 816
    :cond_1f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 820
    :goto_3
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ag;->X:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ag;->W:I

    add-int/2addr v0, v1

    if-ge v0, v4, :cond_20

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->ac:Lsoftware/simplicial/nebulous/application/ag$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->b:Lsoftware/simplicial/nebulous/application/ag$b;

    if-ne v0, v1, :cond_26

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Y:Lsoftware/simplicial/nebulous/application/ag$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    if-eq v0, v1, :cond_26

    .line 821
    :cond_20
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 825
    :goto_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->M:Landroid/widget/Button;

    if-eq p1, v0, :cond_21

    .line 826
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ag;->a()V

    .line 827
    :cond_21
    return-void

    .line 769
    :cond_22
    sget-object v0, Lsoftware/simplicial/a/b/d;->c:Lsoftware/simplicial/a/b/d;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/b/d;)V

    goto/16 :goto_0

    .line 776
    :cond_23
    sget-object v0, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/am;)V

    goto/16 :goto_1

    .line 784
    :cond_24
    sget-object v0, Lsoftware/simplicial/a/h/f;->a:Lsoftware/simplicial/a/h/f;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/h/f;)V

    goto/16 :goto_2

    .line 818
    :cond_25
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_3

    .line 823
    :cond_26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_4
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 146
    const v0, 0x7f040046

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 148
    const v0, 0x7f0d01c8

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->c:Landroid/widget/TableLayout;

    .line 150
    const v0, 0x7f0d01a6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->d:Landroid/widget/TextView;

    .line 152
    const v0, 0x7f0d01a8

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->e:Landroid/widget/Button;

    .line 153
    const v0, 0x7f0d01a9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->f:Landroid/widget/Button;

    .line 154
    const v0, 0x7f0d01aa

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->g:Landroid/widget/Button;

    .line 155
    const v0, 0x7f0d01ab

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->h:Landroid/widget/Button;

    .line 156
    const v0, 0x7f0d019e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->i:Landroid/widget/Button;

    .line 157
    const v0, 0x7f0d01ac

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->j:Landroid/widget/Button;

    .line 158
    const v0, 0x7f0d019d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->k:Landroid/widget/Button;

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->e:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->f:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->g:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->h:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->i:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->j:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->k:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    const v0, 0x7f0d00bc

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->P:Landroid/widget/LinearLayout;

    .line 168
    const v0, 0x7f0d00bd

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Q:Landroid/widget/Spinner;

    .line 169
    const v0, 0x7f0d012a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    .line 170
    const v0, 0x7f0d01bb

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->S:Landroid/widget/LinearLayout;

    .line 171
    const v0, 0x7f0d00bb

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    .line 173
    const v0, 0x7f0d01ae

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->m:Landroid/widget/Button;

    .line 174
    const v0, 0x7f0d01af

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->n:Landroid/widget/Button;

    .line 175
    const v0, 0x7f0d01b0

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->o:Landroid/widget/Button;

    .line 176
    const v0, 0x7f0d01b1

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->p:Landroid/widget/Button;

    .line 177
    const v0, 0x7f0d01b2

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->q:Landroid/widget/Button;

    .line 178
    const v0, 0x7f0d01b3

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->r:Landroid/widget/Button;

    .line 179
    const v0, 0x7f0d01b4

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->s:Landroid/widget/Button;

    .line 180
    const v0, 0x7f0d01b5

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->t:Landroid/widget/Button;

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    .line 182
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->m:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->n:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->o:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->p:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->q:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->r:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->s:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->t:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    const v0, 0x7f0d01c2

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->v:Landroid/widget/Button;

    .line 192
    const v0, 0x7f0d01c3

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->w:Landroid/widget/Button;

    .line 193
    const v0, 0x7f0d01c4

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->x:Landroid/widget/Button;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    .line 195
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->v:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->w:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->x:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    const v0, 0x7f0d01c6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->z:Landroid/widget/Button;

    .line 200
    const v0, 0x7f0d01c7

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    .line 202
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->z:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    const v0, 0x7f0d01b7

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    .line 206
    const v0, 0x7f0d01b8

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    .line 207
    const v0, 0x7f0d01b9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->E:Landroid/widget/Button;

    .line 208
    const v0, 0x7f0d01ba

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->F:Landroid/widget/Button;

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->C:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->D:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->E:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->F:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    const v0, 0x7f0d01bd

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    .line 216
    const v0, 0x7f0d01be

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    .line 217
    const v0, 0x7f0d01bf

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    .line 218
    const v0, 0x7f0d01c0

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->H:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->I:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->J:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->K:Landroid/widget/Button;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    const v0, 0x7f0d00b9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->M:Landroid/widget/Button;

    .line 226
    const v0, 0x7f0d012e

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    .line 227
    const v0, 0x7f0d012d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    .line 229
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 230
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v1

    iget-boolean v1, v1, Lsoftware/simplicial/a/u;->aR:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 233
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->T:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ag$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ag$2;-><init>(Lsoftware/simplicial/nebulous/application/ag;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 245
    invoke-static {}, Lsoftware/simplicial/nebulous/f/aa;->b()I

    move-result v1

    .line 247
    new-array v2, v1, [Ljava/lang/String;

    move v0, v3

    .line 248
    :goto_0
    if-ge v0, v1, :cond_1

    .line 250
    add-int/lit8 v5, v1, -0x1

    if-ne v0, v5, :cond_0

    .line 252
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ag;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0800d4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    .line 248
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ag;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08025a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v0

    goto :goto_1

    .line 260
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v5, 0x7f040098

    invoke-direct {v0, v1, v5, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 261
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->Q:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 262
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->Q:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 264
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 265
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 267
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 268
    const/4 v1, 0x1

    if-ne v2, v1, :cond_3

    .line 270
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 271
    check-cast v0, Landroid/text/SpannableString;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    const/16 v7, 0xff

    const/16 v8, 0xa5

    invoke-static {v7, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v7

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const/16 v8, 0x12

    invoke-virtual {v0, v6, v3, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 273
    :goto_3
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 275
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f04009e

    invoke-direct {v1, v2, v3, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 277
    return-object v4

    :cond_3
    move-object v1, v0

    goto :goto_3
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 965
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 966
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 925
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 927
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ag;->b()V

    .line 928
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->f:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 929
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->g:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 930
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->h:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 931
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->i:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 932
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->j:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_5

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 933
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->k:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v4, :cond_6

    move v0, v1

    :goto_5
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 934
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->A:Landroid/widget/Button;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 935
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 936
    iget-boolean v2, v0, Lsoftware/simplicial/a/u;->p:Z

    if-eqz v2, :cond_7

    .line 937
    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->d:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    .line 946
    :goto_6
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->k:Lsoftware/simplicial/a/bn;

    invoke-direct {p0, v2, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/bn;Z)V

    .line 947
    sget-object v1, Lsoftware/simplicial/a/g/f;->a:Lsoftware/simplicial/a/g/f;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/g/f;)V

    .line 948
    sget-object v1, Lsoftware/simplicial/nebulous/application/ag$b;->a:Lsoftware/simplicial/nebulous/application/ag$b;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$b;)V

    .line 949
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->q:Z

    if-eqz v1, :cond_b

    .line 950
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/b/d;)V

    .line 957
    :goto_7
    iget-object v0, v0, Lsoftware/simplicial/a/u;->v:Lsoftware/simplicial/a/c/g;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/c/g;)V

    .line 959
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ag;->a()V

    .line 960
    return-void

    :cond_1
    move v0, v2

    .line 928
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 929
    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 930
    goto/16 :goto_2

    :cond_4
    move v0, v2

    .line 931
    goto :goto_3

    :cond_5
    move v0, v2

    .line 932
    goto :goto_4

    :cond_6
    move v0, v2

    .line 933
    goto :goto_5

    .line 938
    :cond_7
    iget-boolean v2, v0, Lsoftware/simplicial/a/u;->q:Z

    if-eqz v2, :cond_8

    .line 939
    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->e:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    goto :goto_6

    .line 940
    :cond_8
    iget-boolean v2, v0, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v2, :cond_9

    .line 941
    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->g:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    goto :goto_6

    .line 942
    :cond_9
    iget-boolean v2, v0, Lsoftware/simplicial/a/u;->r:Z

    if-eqz v2, :cond_a

    .line 943
    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->f:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    goto :goto_6

    .line 945
    :cond_a
    sget-object v2, Lsoftware/simplicial/nebulous/application/ag$a;->a:Lsoftware/simplicial/nebulous/application/ag$a;

    invoke-direct {p0, v2}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/nebulous/application/ag$a;)V

    goto :goto_6

    .line 951
    :cond_b
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->r:Z

    if-eqz v1, :cond_c

    .line 952
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->al:Lsoftware/simplicial/a/h/f;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/h/f;)V

    goto :goto_7

    .line 953
    :cond_c
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->t:Z

    if-eqz v1, :cond_d

    .line 954
    iget-boolean v1, v0, Lsoftware/simplicial/a/u;->u:Z

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Z)V

    goto :goto_7

    .line 956
    :cond_d
    iget-object v1, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/ag;->a(Lsoftware/simplicial/a/am;)V

    goto :goto_7
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 283
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 286
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 288
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 289
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 291
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 292
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 294
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 295
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 297
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->G:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 298
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 300
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 301
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 303
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->R:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ag;->ai:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 305
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->Q:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ag$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ag$3;-><init>(Lsoftware/simplicial/nebulous/application/ag;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 321
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->N:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->M:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ag;->O:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 326
    return-void
.end method
