.class public Lsoftware/simplicial/nebulous/application/f;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Z

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/RelativeLayout;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Lsoftware/simplicial/nebulous/views/CircleView;

.field private k:Lsoftware/simplicial/nebulous/views/CircleView;

.field private l:Landroid/widget/ImageView;

.field private m:Lcom/flask/colorpicker/ColorPickerView;

.field private n:Lcom/flask/colorpicker/slider/LightnessSlider;

.field private o:Landroid/widget/CheckBox;

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lsoftware/simplicial/nebulous/application/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/f;->a:Ljava/lang/String;

    .line 41
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/f;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/f;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    return v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/f;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/f;)Lsoftware/simplicial/nebulous/views/CircleView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->k:Lsoftware/simplicial/nebulous/views/CircleView;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/f;)Lsoftware/simplicial/nebulous/views/CircleView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->j:Lsoftware/simplicial/nebulous/views/CircleView;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/f;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->h:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 226
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-super {p0, v0}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 228
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->d:Landroid/widget/TextView;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    if-ltz v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iget v4, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->g:Landroid/widget/Button;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/f;->p:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 231
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->m:Lcom/flask/colorpicker/ColorPickerView;

    iget-boolean v3, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    invoke-virtual {v0, v3}, Lcom/flask/colorpicker/ColorPickerView;->setEnabled(Z)V

    .line 232
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->n:Lcom/flask/colorpicker/slider/LightnessSlider;

    iget-boolean v3, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    invoke-virtual {v0, v3}, Lcom/flask/colorpicker/slider/LightnessSlider;->setEnabled(Z)V

    .line 234
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->i:Landroid/view/View;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->f:Landroid/widget/RelativeLayout;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 237
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->o:Landroid/widget/CheckBox;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->o:Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->aC:Z

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 241
    iget-boolean v3, p0, Lsoftware/simplicial/nebulous/application/f;->c:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->aC:Z

    if-eqz v3, :cond_5

    .line 243
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/f;->l:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 244
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f;->k:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lsoftware/simplicial/a/ba;->d(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/views/CircleView;->a(I)V

    .line 245
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f;->k:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v2, v1}, Lsoftware/simplicial/nebulous/views/CircleView;->setVisibility(I)V

    .line 246
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/f;->j:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->d(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/Integer;)I

    move-result v0

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/views/CircleView;->a(I)V

    .line 247
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->j:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/views/CircleView;->setVisibility(I)V

    .line 255
    :goto_5
    return-void

    .line 228
    :cond_0
    const-string v0, "---"

    goto/16 :goto_0

    :cond_1
    move v0, v1

    .line 229
    goto/16 :goto_1

    :cond_2
    move v0, v1

    .line 234
    goto :goto_2

    :cond_3
    move v0, v1

    .line 235
    goto :goto_3

    :cond_4
    move v0, v2

    .line 237
    goto :goto_4

    .line 251
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->k:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/views/CircleView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->j:Lsoftware/simplicial/nebulous/views/CircleView;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/views/CircleView;->setVisibility(I)V

    goto :goto_5
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 62
    const v0, 0x7f04003a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 64
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f0d0087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->d:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->e:Landroid/widget/Button;

    .line 68
    const v0, 0x7f0d0144

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->f:Landroid/widget/RelativeLayout;

    .line 69
    const v0, 0x7f0d0145

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->g:Landroid/widget/Button;

    .line 70
    const v0, 0x7f0d0142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->h:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0d0141

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->i:Landroid/view/View;

    .line 72
    const v0, 0x7f0d013c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/CircleView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->j:Lsoftware/simplicial/nebulous/views/CircleView;

    .line 73
    const v0, 0x7f0d013d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/CircleView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->k:Lsoftware/simplicial/nebulous/views/CircleView;

    .line 74
    const v0, 0x7f0d013e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->l:Landroid/widget/ImageView;

    .line 75
    const v0, 0x7f0d013f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flask/colorpicker/ColorPickerView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->m:Lcom/flask/colorpicker/ColorPickerView;

    .line 76
    const v0, 0x7f0d0140

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/flask/colorpicker/slider/LightnessSlider;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->n:Lcom/flask/colorpicker/slider/LightnessSlider;

    .line 77
    const v0, 0x7f0d0143

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->o:Landroid/widget/CheckBox;

    .line 79
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 221
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 222
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 178
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$6;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$6;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$x;)V

    .line 216
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->e:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$1;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->g:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$2;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->o:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$3;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->m:Lcom/flask/colorpicker/ColorPickerView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$4;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Lcom/flask/colorpicker/ColorPickerView;->a(Lcom/flask/colorpicker/d;)V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/f;->m:Lcom/flask/colorpicker/ColorPickerView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/f$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/f$5;-><init>(Lsoftware/simplicial/nebulous/application/f;)V

    invoke-virtual {v0, v1}, Lcom/flask/colorpicker/ColorPickerView;->a(Lcom/flask/colorpicker/c;)V

    .line 170
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/f;->a()V

    .line 171
    return-void
.end method
