.class Lsoftware/simplicial/nebulous/application/MainActivity$26;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(IIILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;II)V
    .locals 0

    .prologue
    .line 2996
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->a:I

    iput p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3000
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3013
    :cond_0
    return-void

    .line 3003
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->z(Lsoftware/simplicial/nebulous/application/MainActivity;)Lsoftware/simplicial/nebulous/application/GameChatFragment;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->isVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3004
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->n(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3006
    :cond_2
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->b:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 3008
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 3009
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 3010
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 3011
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$26;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method
