.class public abstract Lsoftware/simplicial/nebulous/application/n;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lsoftware/simplicial/a/m;


# static fields
.field private static d:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Landroid/text/SpannableString;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/widget/ImageButton;

.field public b:Landroid/widget/ImageButton;

.field protected c:Z

.field private final h:Ljava/lang/Object;

.field private i:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lsoftware/simplicial/nebulous/f/z;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/widget/ListView;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/Button;

.field private n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

.field private o:Ljava/util/Timer;

.field private p:Ljava/lang/CharSequence;

.field private q:I

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Lsoftware/simplicial/a/m;

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->h:Ljava/lang/Object;

    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    .line 71
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    .line 73
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->r:Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    .line 75
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    .line 76
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/n;->c:Z

    .line 77
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/n;->u:Z

    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 226
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2f

    if-eq v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 243
    :goto_0
    return v0

    .line 230
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 231
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-nez v2, :cond_1

    .line 234
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v2, v0, :cond_4

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_5

    :cond_4
    move v0, v1

    .line 235
    goto :goto_0

    .line 239
    :cond_5
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    move v0, v1

    .line 243
    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/n;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/n;Lsoftware/simplicial/nebulous/f/z;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/n;->a(Lsoftware/simplicial/nebulous/f/z;)V

    return-void
.end method

.method private a(Lsoftware/simplicial/nebulous/f/z;)V
    .locals 2

    .prologue
    .line 447
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    .line 448
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    .line 449
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 450
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/n;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->u:Z

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/n;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/n;)Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/application/n;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    return v0
.end method

.method static synthetic f()Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/application/n;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic g()Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h()Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i()Ljava/util/List;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->g:Ljava/util/List;

    return-object v0
.end method

.method private j()V
    .locals 6

    .prologue
    .line 250
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 252
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/n;->k()V

    .line 254
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->o:Ljava/util/Timer;

    .line 255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->o:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/nebulous/application/n$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/n$3;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 277
    monitor-exit v1

    .line 278
    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 282
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 284
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->o:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->o:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->o:Ljava/util/Timer;

    .line 289
    :cond_0
    monitor-exit v1

    .line 290
    return-void

    .line 289
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 295
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    sget-object v1, Lsoftware/simplicial/a/n;->b:Lsoftware/simplicial/a/n;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_1

    .line 296
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v1, Lsoftware/simplicial/a/n;->c:Lsoftware/simplicial/a/n;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    .line 297
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    sget-object v1, Lsoftware/simplicial/a/n;->c:Lsoftware/simplicial/a/n;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 298
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v1, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    .line 300
    :cond_2
    const-string v0, ""

    .line 301
    sget-object v1, Lsoftware/simplicial/nebulous/application/n$2;->a:[I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    invoke-virtual {v2}, Lsoftware/simplicial/a/n;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 316
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 317
    return-void

    .line 304
    :pswitch_0
    const v0, 0x7f080204

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/n;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 308
    :pswitch_1
    const v0, 0x7f08008d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 309
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/n;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0039

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 312
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    const v2, -0x3f5f01

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 552
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 553
    if-nez v7, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    if-eqz v0, :cond_0

    .line 559
    :cond_2
    new-instance v0, Lsoftware/simplicial/nebulous/application/n$6;

    move-object v1, p0

    move v2, p2

    move v3, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/n$6;-><init>(Lsoftware/simplicial/nebulous/application/n;IIILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 369
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 370
    if-nez v9, :cond_1

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    if-eqz v0, :cond_0

    .line 376
    :cond_2
    new-instance v0, Lsoftware/simplicial/nebulous/application/n$4;

    move-object v1, p0

    move v2, p2

    move v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/n$4;-><init>(Lsoftware/simplicial/nebulous/application/n;IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;[BLjava/lang/String;IZ)V
    .locals 9

    .prologue
    .line 482
    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 483
    if-nez v8, :cond_1

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    if-eqz v0, :cond_0

    .line 489
    :cond_2
    new-instance v0, Lsoftware/simplicial/nebulous/application/n$5;

    move-object v1, p0

    move v2, p5

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lsoftware/simplicial/nebulous/application/n$5;-><init>(Lsoftware/simplicial/nebulous/application/n;IILjava/lang/String;[BLjava/lang/String;Z)V

    invoke-virtual {v8, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 81
    const v0, 0x7f0d016c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    .line 82
    const v0, 0x7f0d016f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    .line 83
    const v0, 0x7f0d016e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    .line 84
    const v0, 0x7f0d0174

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    .line 85
    const v0, 0x7f0d0173

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    .line 86
    const v0, 0x7f0d0171

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->a:Landroid/widget/ImageButton;

    .line 87
    const v0, 0x7f0d0172

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->b:Landroid/widget/ImageButton;

    .line 88
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    .line 454
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->u:Z

    .line 455
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 457
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->i:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/f/z;

    .line 458
    sget-object v1, Lsoftware/simplicial/nebulous/application/n$2;->b:[I

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/z;->a:Lsoftware/simplicial/nebulous/f/z$a;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/z$a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 461
    :pswitch_0
    iget v1, v0, Lsoftware/simplicial/nebulous/f/z;->b:I

    iget-object v2, v0, Lsoftware/simplicial/nebulous/f/z;->e:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/z;->f:[B

    iget-object v4, v0, Lsoftware/simplicial/nebulous/f/z;->i:Ljava/lang/String;

    iget v5, v0, Lsoftware/simplicial/nebulous/f/z;->c:I

    iget-boolean v6, v0, Lsoftware/simplicial/nebulous/f/z;->j:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/n;->a(ILjava/lang/String;[BLjava/lang/String;IZ)V

    goto :goto_0

    .line 464
    :pswitch_1
    iget v1, v0, Lsoftware/simplicial/nebulous/f/z;->b:I

    iget v2, v0, Lsoftware/simplicial/nebulous/f/z;->c:I

    iget-object v3, v0, Lsoftware/simplicial/nebulous/f/z;->e:Ljava/lang/String;

    iget-object v4, v0, Lsoftware/simplicial/nebulous/f/z;->f:[B

    iget-object v5, v0, Lsoftware/simplicial/nebulous/f/z;->g:Lsoftware/simplicial/a/q;

    iget-object v6, v0, Lsoftware/simplicial/nebulous/f/z;->h:Lsoftware/simplicial/a/n;

    iget-object v7, v0, Lsoftware/simplicial/nebulous/f/z;->i:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/application/n;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V

    goto :goto_0

    .line 467
    :pswitch_2
    iget v1, v0, Lsoftware/simplicial/nebulous/f/z;->b:I

    iget v2, v0, Lsoftware/simplicial/nebulous/f/z;->c:I

    iget v3, v0, Lsoftware/simplicial/nebulous/f/z;->d:I

    iget-object v4, v0, Lsoftware/simplicial/nebulous/f/z;->e:Ljava/lang/String;

    iget-object v5, v0, Lsoftware/simplicial/nebulous/f/z;->i:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/application/n;->a(IIILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    :cond_0
    return-void

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 475
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 476
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    sget-object v1, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 477
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 790
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 807
    :goto_0
    return-void

    .line 793
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v1, Lsoftware/simplicial/nebulous/application/n$9;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/n$9;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 811
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 828
    :goto_0
    return-void

    .line 814
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v1, Lsoftware/simplicial/nebulous/application/n$10;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/n$10;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_a

    .line 134
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->A()Z

    move-result v0

    if-nez v0, :cond_2

    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801c9

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 192
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    sget-object v2, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 195
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/n;->j()V

    .line 222
    :cond_1
    :goto_1
    return-void

    .line 138
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 140
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 141
    invoke-direct {p0, v3}, Lsoftware/simplicial/nebulous/application/n;->a(Ljava/lang/String;)I

    move-result v4

    .line 142
    if-eq v4, v2, :cond_5

    move v0, v1

    .line 145
    :goto_2
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 146
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v5, 0x20

    if-ne v2, v5, :cond_4

    .line 148
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 149
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 150
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lsoftware/simplicial/a/t;->b(ILjava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 154
    :cond_5
    sget-object v0, Lsoftware/simplicial/nebulous/application/n$2;->a:[I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    invoke-virtual {v4}, Lsoftware/simplicial/a/n;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 159
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 165
    :goto_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2, v0, v3}, Lsoftware/simplicial/a/t;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 161
    :catch_0
    move-exception v0

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    .line 168
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 169
    const v0, 0x7f080243

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [B

    sget-object v5, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v6, Lsoftware/simplicial/a/n;->b:Lsoftware/simplicial/a/n;

    const v0, 0x7f0801c8

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/application/n;->a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 171
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, v3}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 177
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0801ad

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 181
    :cond_7
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v2

    sget-object v4, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-eq v2, v4, :cond_8

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 182
    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v2

    sget-object v4, Lsoftware/simplicial/a/t$a;->g:Lsoftware/simplicial/a/t$a;

    if-eq v2, v4, :cond_8

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 183
    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v2

    sget-object v4, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-ne v2, v4, :cond_9

    .line 184
    :cond_8
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 186
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, v3}, Lsoftware/simplicial/a/t;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 197
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_c

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    sget-object v2, Lsoftware/simplicial/a/n;->c:Lsoftware/simplicial/a/n;

    if-ne v0, v2, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 201
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    .line 211
    :goto_4
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/n;->a()V

    goto/16 :goto_1

    .line 205
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->T:I

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    invoke-virtual {v0}, Lsoftware/simplicial/a/n;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 207
    sget-object v2, Lsoftware/simplicial/a/n;->d:[Lsoftware/simplicial/a/n;

    array-length v2, v2

    if-lt v0, v2, :cond_e

    .line 209
    :goto_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/n;->d:[Lsoftware/simplicial/a/n;

    aget-object v1, v2, v1

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->ae:Lsoftware/simplicial/a/n;

    goto :goto_4

    .line 213
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->a:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_d

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/n;->u:Z

    .line 216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_1

    .line 218
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->b:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aj:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    :cond_e
    move v1, v0

    goto :goto_5

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 626
    :try_start_0
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->e:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    .line 627
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->f:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    .line 628
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->g:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->r:Ljava/lang/String;

    .line 629
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 631
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 632
    const v2, 0x7f08005d

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 633
    if-eqz v0, :cond_0

    const v0, 0x7f0802d2

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 634
    const v0, 0x7f0800d2

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 635
    const v0, 0x7f0800d1

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    const v0, 0x7f0802e5

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    const v0, 0x7f080263

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    new-instance v2, Lsoftware/simplicial/nebulous/application/n$7;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/n$7;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setSpinnerEventsListener(Lsoftware/simplicial/nebulous/widgets/CloseableSpinner$a;)V

    .line 652
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f04001d

    invoke-direct {v2, v3, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 656
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    .line 657
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setSelection(I)V

    .line 659
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 660
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {p2}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setX(F)V

    .line 661
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setY(F)V

    .line 662
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setVisibility(I)V

    .line 663
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->performClick()Z

    .line 672
    :goto_1
    return-void

    .line 633
    :cond_0
    const v0, 0x7f080042

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 666
    :catch_0
    move-exception v0

    .line 668
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    .line 669
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    .line 670
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->r:Ljava/lang/String;

    goto :goto_1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const v5, 0x7f0800d0

    const v2, 0x7f0802b6

    const/4 v1, -0x1

    const/4 v4, 0x0

    .line 677
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    if-ne p1, v0, :cond_0

    .line 679
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    if-nez v0, :cond_1

    .line 681
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    packed-switch p3, :pswitch_data_0

    .line 767
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setVisibility(I)V

    .line 768
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    goto :goto_0

    .line 691
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 693
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(ILsoftware/simplicial/nebulous/application/MainActivity;)V

    goto :goto_1

    .line 697
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 698
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 699
    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08002f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 700
    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/n;->p:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 701
    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080318

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/n$8;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/n$8;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 712
    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 716
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 717
    const v1, 0x7f0800d2

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/n;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 718
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 720
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 723
    :pswitch_3
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    if-ne v0, v1, :cond_3

    .line 725
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 729
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 730
    const v1, 0x7f080023

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 731
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 733
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 737
    :pswitch_4
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    if-ne v0, v1, :cond_4

    .line 739
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 743
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 744
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, ""

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    .line 745
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    .line 749
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 751
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c9

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 753
    :cond_5
    iget v0, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    if-ne v0, v1, :cond_6

    .line 755
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 759
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 760
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/n;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 761
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 762
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 763
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInputFromWindow(Landroid/os/IBinder;II)V

    goto/16 :goto_1

    .line 686
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 785
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setVisibility(I)V

    .line 786
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 355
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 358
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 362
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/n;->k()V

    .line 363
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 323
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 325
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    iput v2, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    .line 329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setSelection(I)V

    .line 332
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/n;->a()V

    .line 335
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    .line 336
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/m;

    .line 338
    instance-of v2, v0, Lsoftware/simplicial/nebulous/application/n;

    if-eqz v2, :cond_1

    .line 340
    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    .line 345
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 346
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    if-eqz v0, :cond_3

    .line 347
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->t:Lsoftware/simplicial/a/m;

    invoke-interface {v0, v1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 349
    :cond_3
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/n;->c()V

    .line 350
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->k:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    sget-object v0, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f040068

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    .line 101
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/n;->e:Ljava/util/List;

    .line 102
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/n;->f:Ljava/util/List;

    .line 103
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/n;->g:Ljava/util/List;

    .line 106
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    sget-object v1, Lsoftware/simplicial/nebulous/application/n;->d:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->j:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->n:Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/widgets/CloseableSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 113
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n;->m:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/n$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/n$1;-><init>(Lsoftware/simplicial/nebulous/application/n;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/n;->s:I

    .line 126
    return-void
.end method
