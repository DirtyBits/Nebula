.class public Lsoftware/simplicial/nebulous/application/ah;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lsoftware/simplicial/a/ah;
.implements Lsoftware/simplicial/a/ar;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/a/b/c;
.implements Lsoftware/simplicial/a/c;
.implements Lsoftware/simplicial/a/c/d;
.implements Lsoftware/simplicial/a/h/e;
.implements Lsoftware/simplicial/nebulous/e/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/ah$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Z

.field public static c:Lsoftware/simplicial/nebulous/application/ah$a;


# instance fields
.field A:Landroid/widget/TextView;

.field B:Landroid/widget/RadioGroup;

.field C:Landroid/widget/CheckBox;

.field D:Landroid/widget/CheckBox;

.field E:Lsoftware/simplicial/nebulous/a/t;

.field F:Lsoftware/simplicial/nebulous/a/s;

.field G:Lsoftware/simplicial/nebulous/a/l;

.field H:Lsoftware/simplicial/nebulous/a/j;

.field I:Lsoftware/simplicial/nebulous/a/o;

.field J:Lsoftware/simplicial/nebulous/a/ab;

.field K:Lsoftware/simplicial/nebulous/a/ac;

.field private final L:Ljava/lang/Object;

.field private final M:Ljava/lang/Object;

.field private N:I

.field private O:I

.field private P:Ljava/util/Timer;

.field private Q:Ljava/util/Timer;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/Button;

.field j:Landroid/widget/Button;

.field k:Landroid/widget/Button;

.field l:Landroid/widget/Button;

.field m:Landroid/widget/Button;

.field n:Landroid/widget/Button;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/ImageButton;

.field t:Landroid/widget/LinearLayout;

.field u:Landroid/widget/LinearLayout;

.field v:Landroid/widget/EditText;

.field w:Landroid/widget/ImageButton;

.field x:Landroid/widget/ImageButton;

.field y:Landroid/widget/ListView;

.field z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lsoftware/simplicial/nebulous/application/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ah;->a:Ljava/lang/String;

    .line 95
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    .line 96
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 97
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->L:Ljava/lang/Object;

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->M:Ljava/lang/Object;

    .line 134
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 1213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v3, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1214
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    if-ne p1, v4, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1215
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v3, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    if-ne p1, v4, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1217
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/o;->a(I)V

    .line 1218
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->notifyDataSetChanged()V

    .line 1219
    return-void

    :cond_2
    move v0, v2

    .line 1214
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1216
    goto :goto_1
.end method

.method private a(ILsoftware/simplicial/a/b/b;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1223
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    if-ne v0, v3, :cond_4

    .line 1225
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1226
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    sget-object v0, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    if-eq p2, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    if-ne p2, v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1227
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 1228
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    sget-object v3, Lsoftware/simplicial/a/b/b;->a:Lsoftware/simplicial/a/b/b;

    if-eq p2, v3, :cond_2

    sget-object v3, Lsoftware/simplicial/a/b/b;->d:Lsoftware/simplicial/a/b/b;

    if-ne p2, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1230
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0, p1, p2}, Lsoftware/simplicial/nebulous/a/ab;->a(ILsoftware/simplicial/a/b/b;)V

    .line 1231
    return-void

    :cond_5
    move v0, v1

    .line 1226
    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ah;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->f()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ah;I)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/ah;->a(I)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ah;ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 0

    .prologue
    .line 89
    invoke-direct/range {p0 .. p5}, Lsoftware/simplicial/nebulous/application/ah;->b(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ah;ILsoftware/simplicial/a/b/b;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ah;->a(ILsoftware/simplicial/a/b/b;)V

    return-void
.end method

.method private b(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1456
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/ah$a;->b:Lsoftware/simplicial/nebulous/application/ah$a;

    if-ne v0, v3, :cond_4

    .line 1458
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1459
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    sget-object v0, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-eq p3, v0, :cond_0

    sget-object v0, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    if-ne p3, v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1460
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 1461
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    sget-object v3, Lsoftware/simplicial/a/h/a$a;->a:Lsoftware/simplicial/a/h/a$a;

    if-eq p3, v3, :cond_2

    sget-object v3, Lsoftware/simplicial/a/h/a$a;->f:Lsoftware/simplicial/a/h/a$a;

    if-ne p3, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1463
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lsoftware/simplicial/nebulous/a/ac;->a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    .line 1464
    return-void

    :cond_5
    move v0, v1

    .line 1459
    goto :goto_0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/ah;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->d()V

    return-void
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/ah;)I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    return v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const v4, 0x7f020236

    const v6, 0x7f020235

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->f:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 283
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->h:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 284
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->g:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 286
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 288
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->n:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 290
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->o:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 291
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->p:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 293
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 296
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 302
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 304
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->r:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 306
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->q:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v5, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    if-ne v0, v5, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 307
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->q:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020089

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 309
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$18;->c:[I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    invoke-virtual {v4}, Lsoftware/simplicial/a/d/c;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 460
    :goto_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    if-ge v0, v4, :cond_2

    move v0, v3

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 461
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    if-lez v2, :cond_3

    :goto_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 462
    return-void

    :cond_0
    move v0, v2

    .line 306
    goto :goto_0

    .line 307
    :cond_1
    const v0, 0x7f02007a

    goto :goto_1

    .line 312
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->f:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 313
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 314
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/t;->notifyDataSetChanged()V

    .line 315
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 317
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080073

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080165

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto :goto_2

    .line 328
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->g:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 330
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/t;->notifyDataSetChanged()V

    .line 331
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 333
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080073

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080165

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 344
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->h:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 346
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/t;->notifyDataSetChanged()V

    .line 347
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 349
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080073

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 350
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080165

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 361
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 362
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 363
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 364
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1/10"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 371
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080211

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080217

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 373
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 375
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 378
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 379
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 380
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/j;->notifyDataSetChanged()V

    .line 381
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 383
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080211

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080167

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 386
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 388
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 391
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 392
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->G:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 393
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->G:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    .line 394
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 395
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1/10"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 401
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080211

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080152

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 403
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->r:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 406
    const/16 v0, 0x9

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 409
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 410
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 411
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->notifyDataSetChanged()V

    .line 412
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v4, 0x7f080073

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 413
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 414
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 415
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v2, "1"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 420
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 421
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 424
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 425
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    const-string v4, "1"

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 430
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$18;->b:[I

    sget-object v4, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/application/ah$a;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_1

    .line 456
    :goto_5
    const v0, 0x7fffffff

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    goto/16 :goto_2

    .line 433
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->n:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 434
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 435
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ab;->notifyDataSetChanged()V

    .line 437
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v4, 0x7f080101

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 440
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    .line 443
    :pswitch_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->o:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 444
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 445
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ac;->notifyDataSetChanged()V

    .line 447
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    const v2, 0x7f080101

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 448
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 451
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    const v2, 0x7f080184

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 453
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_5

    :cond_2
    move v0, v1

    .line 460
    goto/16 :goto_3

    :cond_3
    move v3, v1

    .line 461
    goto/16 :goto_4

    .line 309
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 430
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private e()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 713
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 715
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 716
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 717
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 718
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 720
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$24;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/ah$24;-><init>(Lsoftware/simplicial/nebulous/application/ah;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 738
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 740
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 742
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 743
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 744
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 745
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 746
    return-void
.end method

.method private f()V
    .locals 10

    .prologue
    const/16 v4, 0x64

    const/16 v3, 0xa

    const/4 v9, 0x0

    .line 750
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 751
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->e:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_4

    .line 753
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 754
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->clear()V

    .line 755
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1, v4}, Lsoftware/simplicial/nebulous/a/s;->a(II)V

    .line 756
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/s;->notifyDataSetChanged()V

    .line 757
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v1, v1, 0x64

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah$25;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ah$25;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1, v4, v2}, Lsoftware/simplicial/nebulous/f/al;->a(IILsoftware/simplicial/nebulous/f/al$s;)V

    .line 853
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->e:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->g:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->f:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_1

    .line 856
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 857
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 858
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 859
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 860
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->g()V

    .line 863
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_3

    .line 865
    :cond_2
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->h()V

    .line 867
    :cond_3
    return-void

    .line 780
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->f:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_5

    .line 782
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 783
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/j;->clear()V

    .line 784
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->H:Lsoftware/simplicial/nebulous/a/j;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/j;->notifyDataSetChanged()V

    .line 785
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ah$2;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$q;)V

    goto/16 :goto_0

    .line 801
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->g:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_6

    .line 803
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 804
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->G:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->clear()V

    .line 805
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->G:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    .line 806
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v2, v2, 0x64

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$3;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ah$3;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1, v2, v4, v3}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$r;)V

    goto/16 :goto_0

    .line 824
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_8

    .line 826
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ab;->clear()V

    .line 827
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ab;->notifyDataSetChanged()V

    .line 828
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ac;->clear()V

    .line 829
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ac;->notifyDataSetChanged()V

    .line 830
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 831
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    if-ne v0, v1, :cond_7

    .line 833
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1, v3}, Lsoftware/simplicial/a/t;->a(II)V

    goto/16 :goto_0

    .line 837
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v1, v1, 0xa

    invoke-virtual {v0, v1, v3}, Lsoftware/simplicial/a/t;->b(II)V

    goto/16 :goto_0

    .line 840
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v1, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    if-ne v0, v1, :cond_9

    .line 842
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->clear()V

    .line 843
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/o;->notifyDataSetChanged()V

    .line 844
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 845
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    mul-int/lit8 v1, v1, 0xa

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    invoke-virtual {v0, v1, v3, v2}, Lsoftware/simplicial/a/t;->a(IIZ)V

    goto/16 :goto_0

    .line 849
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 850
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->R:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->R:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    iget-object v8, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    invoke-virtual/range {v0 .. v8}, Lsoftware/simplicial/a/t;->a(IILsoftware/simplicial/a/d/c;Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_0
.end method

.method private g()V
    .locals 6

    .prologue
    .line 871
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->L:Ljava/lang/Object;

    monitor-enter v1

    .line 873
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->j()V

    .line 875
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->P:Ljava/util/Timer;

    .line 876
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->P:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ah$4;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 901
    monitor-exit v1

    .line 902
    return-void

    .line 901
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private h()V
    .locals 7

    .prologue
    .line 906
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ah;->M:Ljava/lang/Object;

    monitor-enter v6

    .line 908
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->i()V

    .line 910
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->Q:Ljava/util/Timer;

    .line 911
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->Q:Ljava/util/Timer;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ah$5;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 948
    monitor-exit v6

    .line 949
    return-void

    .line 948
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 997
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->M:Ljava/lang/Object;

    monitor-enter v1

    .line 999
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->Q:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->Q:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1002
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->Q:Ljava/util/Timer;

    .line 1004
    :cond_0
    monitor-exit v1

    .line 1005
    return-void

    .line 1004
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 1009
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->P:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1011
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->P:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1012
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->P:Ljava/util/Timer;

    .line 1014
    :cond_0
    return-void
.end method

.method private k()Z
    .locals 6

    .prologue
    const v5, 0x7f0801ad

    const/4 v0, 0x0

    .line 1098
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v1, :cond_0

    .line 1111
    :goto_0
    return v0

    .line 1101
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1102
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v3

    invoke-static {v2, v3}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1103
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1104
    invoke-static {v1}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1106
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1107
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/ah;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1110
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    .line 1111
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 1365
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1367
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080282

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1368
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 1369
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 1370
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1372
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$15;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/ah$15;-><init>(Lsoftware/simplicial/nebulous/application/ah;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1390
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1392
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1394
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 1395
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1396
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 1397
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 1398
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 1349
    return-void
.end method

.method public a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V
    .locals 8

    .prologue
    .line 1437
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1438
    if-nez v7, :cond_0

    .line 1452
    :goto_0
    return-void

    .line 1441
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ah$17;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/ah$17;-><init>(Lsoftware/simplicial/nebulous/application/ah;ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;II)V

    invoke-virtual {v7, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Lsoftware/simplicial/a/h/a$a;IIZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lsoftware/simplicial/a/h/a$a;",
            "IIZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/h/f;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1403
    iget-object v12, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1404
    if-nez v12, :cond_0

    .line 1426
    :goto_0
    return-void

    .line 1407
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ah$16;

    move-object v1, p0

    move-object/from16 v2, p3

    move v3, p1

    move-object v4, p2

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p9

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lsoftware/simplicial/nebulous/application/ah$16;-><init>(Lsoftware/simplicial/nebulous/application/ah;Lsoftware/simplicial/a/h/a$a;ILjava/lang/String;IIZLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v12, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ILsoftware/simplicial/a/b/b;Lsoftware/simplicial/a/b/e;)V
    .locals 2

    .prologue
    .line 1263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1264
    if-nez v0, :cond_0

    .line 1278
    :goto_0
    return-void

    .line 1267
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$14;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ah$14;-><init>(Lsoftware/simplicial/nebulous/application/ah;ILsoftware/simplicial/a/b/b;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1343
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1355
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1312
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 1331
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 2

    .prologue
    .line 1317
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1318
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1337
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/c/i;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1146
    if-nez v0, :cond_0

    .line 1164
    :goto_0
    return-void

    .line 1149
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$9;

    invoke-direct {v1, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ah$9;-><init>(Lsoftware/simplicial/nebulous/application/ah;Ljava/util/List;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;ILsoftware/simplicial/a/b/b;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/g;",
            ">;I",
            "Lsoftware/simplicial/a/b/b;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1236
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1237
    if-nez v6, :cond_0

    .line 1258
    :goto_0
    return-void

    .line 1240
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ah$13;

    move-object v1, p0

    move-object v2, p3

    move v3, p2

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/application/ah$13;-><init>(Lsoftware/simplicial/nebulous/application/ah;Lsoftware/simplicial/a/b/b;IZLjava/util/List;)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/h/a$a;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1432
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 1069
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;)V
    .locals 0

    .prologue
    .line 1285
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1300
    return-void
.end method

.method public a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;I)V
    .locals 2

    .prologue
    .line 1169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1170
    if-nez v0, :cond_0

    .line 1184
    :goto_0
    return-void

    .line 1173
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$10;

    invoke-direct {v1, p0, p4}, Lsoftware/simplicial/nebulous/application/ah$10;-><init>(Lsoftware/simplicial/nebulous/application/ah;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/c/h;IIII)V
    .locals 8

    .prologue
    .line 1189
    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1190
    if-nez v7, :cond_0

    .line 1209
    :goto_0
    return-void

    .line 1193
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ah$11;

    move-object v1, p0

    move v2, p5

    move v3, p4

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/application/ah$11;-><init>(Lsoftware/simplicial/nebulous/application/ah;IILsoftware/simplicial/a/c/h;II)V

    invoke-virtual {v7, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1292
    return-void
.end method

.method public a([I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V
    .locals 10

    .prologue
    .line 1075
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1076
    if-nez v9, :cond_0

    .line 1094
    :goto_0
    return-void

    .line 1079
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ah$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/ah$7;-><init>(Lsoftware/simplicial/nebulous/application/ah;[I[Lsoftware/simplicial/a/bg$b;[Lsoftware/simplicial/a/aq;[Z[Lsoftware/simplicial/a/bn;[Lsoftware/simplicial/a/ak;[S)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1119
    if-nez v0, :cond_0

    .line 1139
    :goto_0
    return v2

    .line 1122
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$8;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/ah$8;-><init>(Lsoftware/simplicial/nebulous/application/ah;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1323
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1325
    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1019
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 1020
    if-nez v0, :cond_0

    .line 1043
    :goto_0
    return-void

    .line 1023
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$6;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/ah$6;-><init>(Lsoftware/simplicial/nebulous/application/ah;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 1360
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->k()Z

    move-result v0

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3

    .prologue
    .line 1048
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    if-ne p1, v0, :cond_2

    .line 1050
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    .line 1051
    const v1, 0x7f0d01dd

    if-ne p2, v1, :cond_3

    .line 1052
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->a:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    .line 1058
    :cond_0
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    if-eq v0, v1, :cond_1

    .line 1059
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/x;)V

    .line 1061
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1063
    :cond_2
    return-void

    .line 1053
    :cond_3
    const v1, 0x7f0d01de

    if-ne p2, v1, :cond_4

    .line 1054
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->c:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    goto :goto_0

    .line 1055
    :cond_4
    const v1, 0x7f0d01df

    if-ne p2, v1, :cond_0

    .line 1056
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    sget-object v2, Lsoftware/simplicial/a/x;->b:Lsoftware/simplicial/a/x;

    iput-object v2, v1, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const v2, 0x7f08008c

    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 467
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 469
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 470
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    iget v1, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    if-le v0, v1, :cond_0

    .line 471
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->O:I

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 707
    :cond_0
    :goto_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->d()V

    .line 708
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->f()V

    .line 709
    :cond_1
    :goto_1
    return-void

    .line 473
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 475
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 476
    iget v0, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    if-gez v0, :cond_0

    .line 477
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    goto :goto_0

    .line 479
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 481
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_1

    .line 484
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 486
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$18;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    invoke-virtual {v1}, Lsoftware/simplicial/a/d/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 495
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->o:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 492
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->p:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 498
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->q:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 501
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->A:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 504
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 506
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    if-ne v0, v1, :cond_5

    .line 507
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->G:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 510
    :cond_5
    sget-object v0, Lsoftware/simplicial/nebulous/application/ay$a;->a:Lsoftware/simplicial/nebulous/application/ay$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->b:Lsoftware/simplicial/nebulous/application/ay$a;

    .line 511
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->I:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 515
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto :goto_1

    .line 519
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_9

    .line 521
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$18;->c:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    invoke-virtual {v1}, Lsoftware/simplicial/a/d/c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 539
    :pswitch_6
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 543
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08017f

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 545
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 547
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 548
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 550
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ah;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$20;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/ah$20;-><init>(Lsoftware/simplicial/nebulous/application/ah;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 574
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/ah;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah$21;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ah$21;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 586
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 587
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 588
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 589
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 590
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_1

    .line 524
    :pswitch_7
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->e()V

    goto/16 :goto_1

    .line 527
    :pswitch_8
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->l()V

    goto/16 :goto_1

    .line 530
    :pswitch_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->q()V

    goto/16 :goto_0

    .line 533
    :pswitch_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 534
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->J:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 536
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto/16 :goto_0

    .line 594
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_a

    .line 596
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 597
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->b:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 599
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_b

    .line 601
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 602
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 604
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_d

    .line 606
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 607
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto/16 :goto_0

    .line 610
    :cond_c
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 611
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 614
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_f

    .line 616
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 617
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto/16 :goto_0

    .line 620
    :cond_e
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 621
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->e:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 624
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    if-ne p1, v0, :cond_13

    .line 626
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 627
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k()V

    goto/16 :goto_0

    .line 630
    :cond_10
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 631
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_11
    sget-object v0, Lsoftware/simplicial/a/d/c;->f:Lsoftware/simplicial/a/d/c;

    :goto_2
    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    :cond_12
    sget-object v0, Lsoftware/simplicial/a/d/c;->g:Lsoftware/simplicial/a/d/c;

    goto :goto_2

    .line 634
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    if-ne p1, v0, :cond_14

    .line 636
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 637
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->h:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 639
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    if-ne p1, v0, :cond_15

    .line 641
    iput v1, p0, Lsoftware/simplicial/nebulous/application/ah;->N:I

    .line 642
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 644
    :cond_15
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->n:Landroid/widget/Button;

    if-ne p1, v0, :cond_16

    .line 646
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$a;->a:Lsoftware/simplicial/nebulous/application/ah$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    .line 647
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 649
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_17

    .line 651
    sget-object v0, Lsoftware/simplicial/nebulous/application/ah$a;->b:Lsoftware/simplicial/nebulous/application/ah$a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ah;->c:Lsoftware/simplicial/nebulous/application/ah$a;

    .line 652
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/a/d/c;->i:Lsoftware/simplicial/a/d/c;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    goto/16 :goto_0

    .line 654
    :cond_17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_18

    .line 656
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    .line 659
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_19

    .line 661
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    .line 664
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_1a

    .line 666
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ak:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    .line 669
    :cond_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->s:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 671
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_1b

    .line 672
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_1

    .line 675
    :cond_1b
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 676
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 677
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 678
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801a6

    .line 679
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah$23;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ah$23;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080041

    .line 691
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah$22;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/ah$22;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 702
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 489
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 521
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_7
        :pswitch_9
        :pswitch_8
        :pswitch_6
        :pswitch_a
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 141
    const v0, 0x7f040048

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 143
    const v0, 0x7f0d01d3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->d:Landroid/widget/Button;

    .line 144
    const v0, 0x7f0d011c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    .line 145
    const v0, 0x7f0d01d5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->f:Landroid/widget/Button;

    .line 146
    const v0, 0x7f0d01d4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->g:Landroid/widget/Button;

    .line 147
    const v0, 0x7f0d01d6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->h:Landroid/widget/Button;

    .line 148
    const v0, 0x7f0d01d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    .line 149
    const v0, 0x7f0d00aa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    .line 150
    const v0, 0x7f0d01d7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    .line 151
    const v0, 0x7f0d019e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    .line 152
    const v0, 0x7f0d01d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    .line 153
    const v0, 0x7f0d0118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    .line 154
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    .line 155
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    .line 156
    const v0, 0x7f0d01cf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->y:Landroid/widget/ListView;

    .line 157
    const v0, 0x7f0d0142

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->z:Landroid/widget/TextView;

    .line 158
    const v0, 0x7f0d01d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->A:Landroid/widget/TextView;

    .line 159
    const v0, 0x7f0d01dc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    .line 160
    const v0, 0x7f0d01e0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->C:Landroid/widget/CheckBox;

    .line 161
    const v0, 0x7f0d01e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    .line 162
    const v0, 0x7f0d01da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->n:Landroid/widget/Button;

    .line 163
    const v0, 0x7f0d01db

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->o:Landroid/widget/Button;

    .line 164
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->p:Landroid/widget/Button;

    .line 165
    const v0, 0x7f0d01e3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->q:Landroid/widget/Button;

    .line 166
    const v0, 0x7f0d01cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->r:Landroid/widget/Button;

    .line 167
    const v0, 0x7f0d0183

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->s:Landroid/widget/ImageButton;

    .line 168
    const v0, 0x7f0d01d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->u:Landroid/widget/LinearLayout;

    .line 169
    const v0, 0x7f0d0181

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->t:Landroid/widget/LinearLayout;

    .line 171
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 981
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 983
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 984
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 985
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 986
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 987
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 988
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 989
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 991
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->i()V

    .line 992
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->j()V

    .line 993
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 954
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 956
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 957
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 958
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 959
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 961
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 962
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 963
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 964
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->i:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 965
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 966
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->h:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 967
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 969
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->f()V

    .line 971
    sget-boolean v0, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    if-eqz v0, :cond_0

    .line 973
    const/4 v0, 0x0

    sput-boolean v0, Lsoftware/simplicial/nebulous/application/ah;->b:Z

    .line 974
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 976
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 177
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 179
    sget-object v2, Lsoftware/simplicial/nebulous/application/ah$18;->a:[I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->Y:Lsoftware/simplicial/a/x;

    invoke-virtual {v3}, Lsoftware/simplicial/a/x;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 192
    :goto_0
    new-instance v2, Lsoftware/simplicial/nebulous/a/t;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v2, v3, v4, p0}, Lsoftware/simplicial/nebulous/a/t;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/util/List;Lsoftware/simplicial/nebulous/e/b;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->E:Lsoftware/simplicial/nebulous/a/t;

    .line 193
    new-instance v2, Lsoftware/simplicial/nebulous/a/s;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v4, Lsoftware/simplicial/nebulous/a/s$a;->a:Lsoftware/simplicial/nebulous/a/s$a;

    invoke-direct {v2, v3, v4}, Lsoftware/simplicial/nebulous/a/s;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/s$a;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->F:Lsoftware/simplicial/nebulous/a/s;

    .line 194
    new-instance v2, Lsoftware/simplicial/nebulous/a/l;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v4, Lsoftware/simplicial/nebulous/a/l$a;->a:Lsoftware/simplicial/nebulous/a/l$a;

    invoke-direct {v2, v3, v4}, Lsoftware/simplicial/nebulous/a/l;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/a/l$a;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->G:Lsoftware/simplicial/nebulous/a/l;

    .line 195
    new-instance v2, Lsoftware/simplicial/nebulous/a/j;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Lsoftware/simplicial/nebulous/a/j;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->H:Lsoftware/simplicial/nebulous/a/j;

    .line 196
    new-instance v2, Lsoftware/simplicial/nebulous/a/o;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Lsoftware/simplicial/nebulous/a/o;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->I:Lsoftware/simplicial/nebulous/a/o;

    .line 197
    new-instance v2, Lsoftware/simplicial/nebulous/a/ab;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Lsoftware/simplicial/nebulous/a/ab;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->J:Lsoftware/simplicial/nebulous/a/ab;

    .line 198
    new-instance v2, Lsoftware/simplicial/nebulous/a/ac;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3}, Lsoftware/simplicial/nebulous/a/ac;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->K:Lsoftware/simplicial/nebulous/a/ac;

    .line 200
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->d:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->e:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->k:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->f:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->h:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->g:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    invoke-virtual {v2, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 213
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->n:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->o:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->p:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->q:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->r:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->s:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->i:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 221
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->j:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->l:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 223
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->m:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 224
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->x:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 225
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->w:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 227
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->z:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->v:Landroid/widget/EditText;

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$1;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ah$1;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 254
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->C:Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->an:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 255
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->C:Landroid/widget/CheckBox;

    new-instance v3, Lsoftware/simplicial/nebulous/application/ah$12;

    invoke-direct {v3, p0}, Lsoftware/simplicial/nebulous/application/ah$12;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 265
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/ah;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    if-nez v3, :cond_0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 266
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ah;->D:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ah$19;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ah$19;-><init>(Lsoftware/simplicial/nebulous/application/ah;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 277
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ah;->d()V

    .line 278
    return-void

    .line 182
    :pswitch_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    const v3, 0x7f0d01dd

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    .line 185
    :pswitch_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    const v3, 0x7f0d01de

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    .line 188
    :pswitch_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ah;->B:Landroid/widget/RadioGroup;

    const v3, 0x7f0d01df

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0

    :cond_0
    move v0, v1

    .line 265
    goto :goto_1

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 1306
    return-void
.end method
