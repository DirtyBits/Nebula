.class Lsoftware/simplicial/nebulous/application/MainActivity$40;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/au;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/a/au;

.field final synthetic b:I

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/au;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 3561
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->a:Lsoftware/simplicial/a/au;

    iput p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->b:I

    iput-object p4, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const v3, 0x7f08015d

    const v2, 0x7f080102

    const v5, 0x7f0801cf

    .line 3565
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->a:Lsoftware/simplicial/a/au;

    sget-object v1, Lsoftware/simplicial/a/au;->b:Lsoftware/simplicial/a/au;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->a:Lsoftware/simplicial/a/au;

    sget-object v1, Lsoftware/simplicial/a/au;->f:Lsoftware/simplicial/a/au;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->av:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->b:I

    .line 3566
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3606
    :cond_2
    :goto_0
    return-void

    .line 3569
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->e:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->a:Lsoftware/simplicial/a/au;

    invoke-virtual {v1}, Lsoftware/simplicial/a/au;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 3572
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->b:I

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Lsoftware/simplicial/nebulous/application/MainActivity;I)I

    .line 3573
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 3574
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->u(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 3577
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080160

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3580
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080161

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3583
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080162

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3586
    :pswitch_4
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->b:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 3587
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080163

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3589
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->u(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    .line 3593
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802ed

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3596
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801c7

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3599
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08015c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3602
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f080138

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f0802dc

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$40;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3569
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
