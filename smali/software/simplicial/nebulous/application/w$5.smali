.class Lsoftware/simplicial/nebulous/application/w$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/w;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/w;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/w;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f0801e6

    .line 203
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/w;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 204
    const v0, 0x1080027

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 205
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v2, 0x7f0800c6

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v3, 0x7f080113

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v3, 0x7f0800d3

    .line 207
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/w;->e(Lsoftware/simplicial/nebulous/application/w;)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->c:Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v3, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    .line 208
    invoke-virtual {v0, v6}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v2, 0x7f0801e0

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lsoftware/simplicial/nebulous/application/w$5$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/w$5$1;-><init>(Lsoftware/simplicial/nebulous/application/w$5;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v2, 0x7f08005d

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 235
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 236
    return-void

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    const v4, 0x7f08008d

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/w$5;->a:Lsoftware/simplicial/nebulous/application/w;

    invoke-virtual {v3, v6}, Lsoftware/simplicial/nebulous/application/w;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
