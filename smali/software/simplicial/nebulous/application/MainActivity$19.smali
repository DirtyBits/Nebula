.class Lsoftware/simplicial/nebulous/application/MainActivity$19;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->b(Lsoftware/simplicial/a/ai;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;ZZZ)V
    .locals 0

    .prologue
    .line 2636
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->a:Z

    iput-boolean p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->b:Z

    iput-boolean p4, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 2640
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 2641
    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->k(Lsoftware/simplicial/nebulous/application/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/b;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, v1, Lsoftware/simplicial/a/t;->u:F

    sget-object v2, Lsoftware/simplicial/a/u;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2644
    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/b;->a(FII)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2646
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/b;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2647
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/b;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Lsoftware/simplicial/nebulous/application/MainActivity;Z)Z

    .line 2652
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-boolean v0, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v0, :cond_1

    .line 2653
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->c:Z

    if-eqz v0, :cond_3

    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->c:Lsoftware/simplicial/nebulous/f/af;

    :goto_1
    invoke-static {v1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/af;)V

    .line 2654
    :cond_1
    return-void

    .line 2649
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$19;->d:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->u()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/b;->a(Landroid/app/Activity;Z)V

    goto :goto_0

    .line 2653
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->a:Lsoftware/simplicial/nebulous/f/af;

    goto :goto_1
.end method
