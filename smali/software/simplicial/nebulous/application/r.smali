.class public Lsoftware/simplicial/nebulous/application/r;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/o;
.implements Lsoftware/simplicial/nebulous/f/al$h;
.implements Lsoftware/simplicial/nebulous/f/al$l;
.implements Lsoftware/simplicial/nebulous/f/al$m;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/application/r$a;
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field private c:Landroid/widget/Button;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/Spinner;

.field private f:Landroid/widget/Button;

.field private g:[Lsoftware/simplicial/nebulous/application/r$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lsoftware/simplicial/nebulous/application/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/r;->a:Ljava/lang/String;

    .line 41
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/r;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 48
    sget-object v0, Lsoftware/simplicial/a/p;->i:[Lsoftware/simplicial/a/p;

    array-length v0, v0

    new-array v0, v0, [Lsoftware/simplicial/nebulous/application/r$a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/r;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    return-object v0
.end method

.method private a()V
    .locals 13

    .prologue
    const v12, 0x7f0201aa

    const v11, 0x7f0201a9

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 193
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/r;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 195
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 197
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v7, 0x0

    iget-object v8, v4, Lsoftware/simplicial/nebulous/application/r$a;->a:Lsoftware/simplicial/a/p;

    invoke-static {v6, v7, v8}, Lsoftware/simplicial/nebulous/f/aa;->a(Landroid/content/Context;Ljava/lang/CharSequence;Lsoftware/simplicial/a/p;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-boolean v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->g:Z

    if-eqz v5, :cond_0

    .line 200
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 201
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->f:Landroid/widget/ImageView;

    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 202
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->c:Landroid/widget/TextView;

    const v6, 0x7f080101

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/r;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->d:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 205
    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    const v5, 0x7f020089

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 195
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_0
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->e:Landroid/widget/ImageView;

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 210
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->f:Landroid/widget/ImageView;

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 211
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->d:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 212
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    const v6, 0x7f02007a

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 213
    iget v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->h:I

    if-ltz v5, :cond_1

    .line 215
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->c:Landroid/widget/TextView;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v6

    iget v7, v4, Lsoftware/simplicial/nebulous/application/r$a;->h:I

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    invoke-virtual {v4, v10}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 220
    :cond_1
    iget-object v5, v4, Lsoftware/simplicial/nebulous/application/r$a;->c:Landroid/widget/TextView;

    const-string v6, "---"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 225
    :cond_2
    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 78
    new-instance v1, Lsoftware/simplicial/nebulous/application/r$a;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/r$a;-><init>(Lsoftware/simplicial/nebulous/application/r;I)V

    .line 79
    const v0, 0x7f0d0086

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    .line 80
    const v0, 0x7f0d0087

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->c:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0d0088

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->d:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f0d0089

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->e:Landroid/widget/ImageView;

    .line 83
    const v0, 0x7f0d008a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->f:Landroid/widget/ImageView;

    .line 84
    iget-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    new-instance v2, Lsoftware/simplicial/nebulous/application/r$1;

    invoke-direct {v2, p0, v1, p1}, Lsoftware/simplicial/nebulous/application/r$1;-><init>(Lsoftware/simplicial/nebulous/application/r;Lsoftware/simplicial/nebulous/application/r$a;I)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, v1, Lsoftware/simplicial/nebulous/application/r$a;->b:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    aput-object v1, v0, p1

    .line 124
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 125
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 128
    const/4 v1, 0x1

    if-ne v2, v1, :cond_1

    .line 130
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 131
    check-cast v0, Landroid/text/SpannableString;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/16 v6, 0xff

    const/16 v7, 0xa5

    invoke-static {v6, v7, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x12

    invoke-virtual {v0, v5, v3, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 133
    :goto_1
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f04009e

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 136
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/r;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/be;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/be;

    .line 231
    iget-object v2, v0, Lsoftware/simplicial/a/be;->a:Ljava/lang/String;

    const-string v3, "CLAN_HOUSE_ROOM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    iget v3, v0, Lsoftware/simplicial/a/be;->b:I

    aget-object v2, v2, v3

    iget v0, v0, Lsoftware/simplicial/a/be;->c:I

    iput v0, v2, Lsoftware/simplicial/nebulous/application/r$a;->h:I

    goto :goto_0

    .line 234
    :cond_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/r;->a()V

    .line 235
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/p;

    .line 242
    if-eqz v0, :cond_0

    .line 245
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    invoke-virtual {v0}, Lsoftware/simplicial/a/p;->ordinal()I

    move-result v0

    aget-object v0, v2, v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lsoftware/simplicial/nebulous/application/r$a;->g:Z

    goto :goto_0

    .line 247
    :cond_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/r;->a()V

    .line 248
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    return-void
.end method

.method public a(Lsoftware/simplicial/a/am;)V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 255
    if-nez v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 258
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/r$2;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/r$2;-><init>(Lsoftware/simplicial/nebulous/application/r;Lsoftware/simplicial/a/am;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(ZLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 185
    if-ltz p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    array-length v0, v0

    if-lt p3, v0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->g:[Lsoftware/simplicial/nebulous/application/r$a;

    aget-object v0, v0, p3

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/r$a;->g:Z

    .line 188
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/r;->a()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 172
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 177
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 178
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lsoftware/simplicial/nebulous/f/aa;->d(J)Lsoftware/simplicial/a/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/am;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 53
    const v0, 0x7f040035

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 55
    invoke-super {p0, v2, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 57
    const v0, 0x7f0d00e9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->c:Landroid/widget/Button;

    .line 58
    const v0, 0x7f0d0129

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->d:Landroid/view/View;

    .line 59
    const v0, 0x7f0d012a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 61
    const v0, 0x7f0d012b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->d:Lsoftware/simplicial/a/q;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->af:Lsoftware/simplicial/a/q;

    sget-object v4, Lsoftware/simplicial/a/q;->f:Lsoftware/simplicial/a/q;

    if-ne v0, v4, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 64
    const v0, 0x7f0d0121

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 65
    const/4 v0, 0x1

    const v1, 0x7f0d0122

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 66
    const/4 v0, 0x2

    const v1, 0x7f0d0123

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 67
    const/4 v0, 0x3

    const v1, 0x7f0d0124

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 68
    const/4 v0, 0x4

    const v1, 0x7f0d0125

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 69
    const/4 v0, 0x5

    const v1, 0x7f0d0126

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 70
    const/4 v0, 0x6

    const v1, 0x7f0d0127

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 71
    const/4 v0, 0x7

    const v1, 0x7f0d0128

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/r;->a(ILandroid/view/View;)V

    .line 73
    return-object v2

    .line 63
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->o:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 165
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 152
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/r;->a()V

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->o:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->l()V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$m;)V

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/r;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/al$h;)V

    .line 157
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/r;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    return-void
.end method
