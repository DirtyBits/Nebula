.class Lsoftware/simplicial/nebulous/application/n$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/n;->a(IIILjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lsoftware/simplicial/nebulous/application/n;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/n;IIILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iput p2, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    iput p3, p0, Lsoftware/simplicial/nebulous/application/n$6;->b:I

    iput p4, p0, Lsoftware/simplicial/nebulous/application/n$6;->c:I

    iput-object p5, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    iput-object p6, p0, Lsoftware/simplicial/nebulous/application/n$6;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v5, 0x12

    const/4 v10, 0x0

    .line 564
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->az:Ljava/util/Set;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 570
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/n;->b(Lsoftware/simplicial/nebulous/application/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/n;->isVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 572
    iget-object v11, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    new-instance v0, Lsoftware/simplicial/nebulous/f/z;

    sget-object v1, Lsoftware/simplicial/nebulous/f/z$a;->c:Lsoftware/simplicial/nebulous/f/z$a;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/n$6;->b:I

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->c:I

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    new-array v6, v10, [B

    sget-object v7, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    sget-object v8, Lsoftware/simplicial/a/n;->a:Lsoftware/simplicial/a/n;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/n$6;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lsoftware/simplicial/nebulous/f/z;-><init>(Lsoftware/simplicial/nebulous/f/z$a;IIILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;Z)V

    invoke-static {v11, v0}, Lsoftware/simplicial/nebulous/application/n;->a(Lsoftware/simplicial/nebulous/application/n;Lsoftware/simplicial/nebulous/f/z;)V

    goto :goto_0

    .line 576
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->e:Ljava/lang/String;

    invoke-static {v0}, Lsoftware/simplicial/a/ba;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578
    :goto_1
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/16 v2, 0x32

    if-lt v0, v2, :cond_3

    .line 580
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 581
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 582
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 583
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    goto :goto_1

    .line 585
    :cond_3
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 587
    const v2, -0x3f5f01

    .line 589
    const-string v0, ""

    .line 590
    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->c:I

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v4

    if-eq v3, v4, :cond_6

    .line 592
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "[->"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->c:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 593
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->h()Ljava/util/List;

    move-result-object v3

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 594
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->g()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 605
    :cond_4
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 606
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 607
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v4, v10, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 608
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 610
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v2, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(IZ)I

    move-result v1

    .line 611
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v2, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v4

    invoke-virtual {v3, v2, v1, v0, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 613
    :cond_5
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/n;->c(Lsoftware/simplicial/nebulous/application/n;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    .line 614
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->f()Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 615
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/n;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 616
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/n;->c()V

    goto/16 :goto_0

    .line 596
    :cond_6
    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    .line 598
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 599
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/n;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 600
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->f:Lsoftware/simplicial/nebulous/application/n;

    const v4, 0x7f080189

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/n;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 601
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 602
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->h()Ljava/util/List;

    move-result-object v3

    iget v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    invoke-static {}, Lsoftware/simplicial/nebulous/application/n;->g()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/n$6;->d:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method
