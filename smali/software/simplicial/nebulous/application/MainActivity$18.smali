.class Lsoftware/simplicial/nebulous/application/MainActivity$18;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/ai;Ljava/util/Collection;Lsoftware/simplicial/a/bf;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/Collection;

.field final synthetic b:Lsoftware/simplicial/a/am;

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/util/Collection;Lsoftware/simplicial/a/am;)V
    .locals 0

    .prologue
    .line 2599
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->a:Ljava/util/Collection;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->b:Lsoftware/simplicial/a/am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2603
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 2604
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/d;)V

    goto :goto_0

    .line 2606
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_2

    .line 2614
    :cond_1
    return-void

    .line 2609
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->a:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2610
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->c:Ljava/util/Map;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->b:Lsoftware/simplicial/a/am;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/az;

    iget-object v0, v0, Lsoftware/simplicial/a/az;->L:Ljava/util/Set;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->a:Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2612
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/d;

    .line 2613
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$18;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v2, v0}, Lsoftware/simplicial/nebulous/d/d;->a(Lsoftware/simplicial/a/d;)Z

    goto :goto_1
.end method
