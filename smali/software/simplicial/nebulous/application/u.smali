.class public Lsoftware/simplicial/nebulous/application/u;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/al$j;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ListView;

.field private e:Landroid/widget/ListView;

.field private f:Lsoftware/simplicial/nebulous/a/g;

.field private g:Lsoftware/simplicial/nebulous/a/h;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lsoftware/simplicial/nebulous/application/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/u;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->c:Landroid/view/View;

    return-object v0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    const v1, 0x7f080283

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/u;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 111
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 112
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 113
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 115
    const v2, 0x7f0801cf

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/u;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/u$1;

    invoke-direct {v3, p0, p1, v1}, Lsoftware/simplicial/nebulous/application/u$1;-><init>(Lsoftware/simplicial/nebulous/application/u;ZLandroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/u;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 138
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 142
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 143
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 144
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 145
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->f:Lsoftware/simplicial/nebulous/a/g;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/g;->clear()V

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->f:Lsoftware/simplicial/nebulous/a/g;

    invoke-virtual {v0, p2}, Lsoftware/simplicial/nebulous/a/g;->addAll(Ljava/util/Collection;)V

    .line 152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->f:Lsoftware/simplicial/nebulous/a/g;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/g;->notifyDataSetChanged()V

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->g:Lsoftware/simplicial/nebulous/a/h;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/h;->clear()V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->g:Lsoftware/simplicial/nebulous/a/h;

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/h;->addAll(Ljava/util/Collection;)V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->g:Lsoftware/simplicial/nebulous/a/h;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/h;->notifyDataSetChanged()V

    .line 156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 94
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 98
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/u;->a(Z)V

    goto :goto_0

    .line 100
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/u;->a(Z)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 43
    const v0, 0x7f040038

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 45
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f0d00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->b:Landroid/widget/Button;

    .line 48
    const v0, 0x7f0d0137

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->h:Landroid/widget/Button;

    .line 49
    const v0, 0x7f0d0139

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->i:Landroid/widget/Button;

    .line 50
    const v0, 0x7f0d0129

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->c:Landroid/view/View;

    .line 51
    const v0, 0x7f0d0136

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->d:Landroid/widget/ListView;

    .line 52
    const v0, 0x7f0d0138

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->e:Landroid/widget/ListView;

    .line 55
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 86
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 87
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/u;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/al$j;)V

    .line 81
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    new-instance v0, Lsoftware/simplicial/nebulous/a/g;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/g;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->f:Lsoftware/simplicial/nebulous/a/g;

    .line 68
    new-instance v0, Lsoftware/simplicial/nebulous/a/h;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/h;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->g:Lsoftware/simplicial/nebulous/a/h;

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->f:Lsoftware/simplicial/nebulous/a/g;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/u;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/u;->g:Lsoftware/simplicial/nebulous/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 71
    return-void
.end method
