.class Lsoftware/simplicial/nebulous/application/aa$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/aa;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/aa;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/aa;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 127
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 128
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 129
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v2, 0x7f0800c6

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v3, 0x7f08021e

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v3, 0x7f0800d3

    .line 131
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/aa;->c(Lsoftware/simplicial/nebulous/application/aa;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v3, 0x7f0801e6

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 133
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v2, 0x7f0801e0

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/aa$3$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/aa$3$1;-><init>(Lsoftware/simplicial/nebulous/application/aa$3;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 148
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa$3;->a:Lsoftware/simplicial/nebulous/application/aa;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 150
    return-void
.end method
