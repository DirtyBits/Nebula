.class Lsoftware/simplicial/nebulous/application/aw$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$aa;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/aw;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/aw;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/aw;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lsoftware/simplicial/nebulous/f/y;)V
    .locals 9

    .prologue
    const v8, 0x7f080189

    const/4 v7, 0x3

    const/4 v2, 0x1

    const/16 v6, 0x12

    const/4 v5, 0x0

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iput-object p1, v0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->n:Lsoftware/simplicial/nebulous/f/y;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/y;->b:I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->j:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->i:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 130
    :cond_0
    const-string v0, ""

    .line 131
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 132
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v3, p1, Lsoftware/simplicial/nebulous/f/y;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lsoftware/simplicial/nebulous/f/y;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lsoftware/simplicial/nebulous/f/y;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 140
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 141
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 142
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v4, p1, Lsoftware/simplicial/nebulous/f/y;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v1, v0}, Lsoftware/simplicial/nebulous/f/aa;->a(IZ)I

    move-result v0

    .line 143
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v1, v5, v2, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 144
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    const-string v0, ""

    .line 147
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0084

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 148
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v3, p1, Lsoftware/simplicial/nebulous/f/y;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lsoftware/simplicial/nebulous/f/y;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 155
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lsoftware/simplicial/nebulous/f/y;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 157
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    iget v4, p1, Lsoftware/simplicial/nebulous/f/y;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v1, v0}, Lsoftware/simplicial/nebulous/f/aa;->a(IZ)I

    move-result v0

    .line 159
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v1, v5, v2, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 160
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v7, v7, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/aw;->f:Landroid/widget/TextView;

    iget-object v2, p1, Lsoftware/simplicial/nebulous/f/y;->h:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->g:Landroid/widget/TextView;

    iget-object v1, p1, Lsoftware/simplicial/nebulous/f/y;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->h:Landroid/widget/TextView;

    iget-object v1, p1, Lsoftware/simplicial/nebulous/f/y;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aw$3;->a:Lsoftware/simplicial/nebulous/application/aw;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/aw;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    return-void

    .line 135
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lsoftware/simplicial/nebulous/f/y;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 151
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lsoftware/simplicial/nebulous/f/y;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method
