.class public Lsoftware/simplicial/nebulous/application/am;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lsoftware/simplicial/nebulous/f/al$r;


# instance fields
.field a:Landroid/widget/Button;

.field b:Landroid/widget/ListView;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ImageButton;

.field e:Landroid/widget/ImageButton;

.field f:I

.field g:Lsoftware/simplicial/nebulous/a/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/am;->d:Landroid/widget/ImageButton;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->e:Landroid/widget/ImageButton;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 79
    return-void

    :cond_0
    move v0, v2

    .line 77
    goto :goto_0

    :cond_1
    move v1, v2

    .line 78
    goto :goto_1
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->c:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/10"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->g:Lsoftware/simplicial/nebulous/a/m;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/m;->clear()V

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->g:Lsoftware/simplicial/nebulous/a/m;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/m;->addAll(Ljava/util/Collection;)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->g:Lsoftware/simplicial/nebulous/a/m;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/m;->notifyDataSetChanged()V

    .line 120
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x64

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->a:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 86
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->e:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 88
    iget v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    mul-int/lit8 v2, v2, 0x64

    invoke-virtual {v0, v1, v2, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$r;)V

    .line 90
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/am;->a()V

    .line 92
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->d:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    iget v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    if-lez v0, :cond_2

    .line 94
    iget v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    mul-int/lit8 v2, v2, 0x64

    invoke-virtual {v0, v1, v2, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$r;)V

    .line 96
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/am;->a()V

    .line 98
    :cond_2
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 41
    const v0, 0x7f04004c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 43
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->a:Landroid/widget/Button;

    .line 44
    const v0, 0x7f0d01fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->b:Landroid/widget/ListView;

    .line 45
    const v0, 0x7f0d01d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->c:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->d:Landroid/widget/ImageButton;

    .line 47
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->e:Landroid/widget/ImageButton;

    .line 49
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->c:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/am;->a()V

    .line 52
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 110
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 71
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/am;->f:I

    mul-int/lit8 v2, v2, 0x64

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$r;)V

    .line 73
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 60
    new-instance v0, Lsoftware/simplicial/nebulous/a/m;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/am;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/m;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->g:Lsoftware/simplicial/nebulous/a/m;

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/am;->g:Lsoftware/simplicial/nebulous/a/m;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->a:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/am;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method
