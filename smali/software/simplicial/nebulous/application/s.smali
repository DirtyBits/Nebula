.class public Lsoftware/simplicial/nebulous/application/s;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/ad;
.implements Lsoftware/simplicial/nebulous/f/al$i;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Button;

.field c:Landroid/widget/ImageButton;

.field d:Landroid/widget/ImageButton;

.field e:Landroid/widget/ListView;

.field f:Landroid/widget/TextView;

.field private g:I

.field private h:Lsoftware/simplicial/nebulous/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lsoftware/simplicial/nebulous/application/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/s;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->f:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/s;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/s;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    mul-int/lit8 v2, v2, 0x64

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;IILsoftware/simplicial/nebulous/f/al$i;)V

    .line 97
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/s;->b()V

    .line 128
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/nebulous/f/n;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->h:Lsoftware/simplicial/nebulous/a/k;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/k;->clear()V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->h:Lsoftware/simplicial/nebulous/a/k;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/k;->addAll(Ljava/util/Collection;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->h:Lsoftware/simplicial/nebulous/a/k;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/k;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 75
    iget v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    .line 76
    iget v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    if-gtz v0, :cond_2

    .line 78
    iput v1, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 82
    :cond_2
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/s;->b()V

    goto :goto_0

    .line 84
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->d:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 86
    iget v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/s;->g:I

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 89
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/s;->b()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 39
    const v0, 0x7f040036

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 41
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->b:Landroid/widget/Button;

    .line 42
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    .line 43
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->d:Landroid/widget/ImageButton;

    .line 44
    const v0, 0x7f0d012c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->e:Landroid/widget/ListView;

    .line 45
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->f:Landroid/widget/TextView;

    .line 47
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 109
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 103
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    new-instance v0, Lsoftware/simplicial/nebulous/a/k;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/s;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1, p0}, Lsoftware/simplicial/nebulous/a/k;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/ad;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->h:Lsoftware/simplicial/nebulous/a/k;

    .line 59
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/s;->h:Lsoftware/simplicial/nebulous/a/k;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/s;->c:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 63
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/s;->b()V

    .line 64
    return-void
.end method
