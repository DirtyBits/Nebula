.class public Lsoftware/simplicial/nebulous/application/bg;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lsoftware/simplicial/a/i/f;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field A:I

.field B:I

.field public C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field E:I

.field private I:Ljava/util/Timer;

.field private final J:Ljava/lang/Object;

.field private K:I

.field c:Landroid/widget/ListView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/CheckBox;

.field k:Landroid/widget/CheckBox;

.field l:Landroid/widget/LinearLayout;

.field m:Landroid/widget/RelativeLayout;

.field n:Landroid/widget/Button;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/Button;

.field q:Landroid/widget/Button;

.field r:Landroid/widget/Button;

.field s:Landroid/widget/ImageButton;

.field t:Landroid/widget/ImageButton;

.field u:Lsoftware/simplicial/nebulous/a/v;

.field v:Lsoftware/simplicial/a/i/e;

.field w:I

.field x:I

.field y:I

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lsoftware/simplicial/nebulous/application/bg;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bg;->a:Ljava/lang/String;

    .line 43
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bg;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->J:Ljava/lang/Object;

    .line 68
    sget-object v0, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->v:Lsoftware/simplicial/a/i/e;

    .line 69
    iput v1, p0, Lsoftware/simplicial/nebulous/application/bg;->w:I

    .line 70
    iput v1, p0, Lsoftware/simplicial/nebulous/application/bg;->x:I

    .line 71
    iput v1, p0, Lsoftware/simplicial/nebulous/application/bg;->y:I

    .line 72
    iput v2, p0, Lsoftware/simplicial/nebulous/application/bg;->z:I

    .line 73
    iput v1, p0, Lsoftware/simplicial/nebulous/application/bg;->A:I

    .line 74
    iput v2, p0, Lsoftware/simplicial/nebulous/application/bg;->B:I

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->C:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->D:Ljava/util/List;

    .line 77
    iput v2, p0, Lsoftware/simplicial/nebulous/application/bg;->E:I

    .line 78
    iput v2, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->A:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->y:I

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f080201

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/bg;->b(Ljava/util/List;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0801e6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;I)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v0, :cond_0

    .line 348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bg;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->c()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bg;Z)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/bg;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 541
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bg;->s:Landroid/widget/ImageButton;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 543
    if-nez p1, :cond_0

    .line 545
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->u()V

    .line 546
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 549
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    mul-int/lit8 v1, v1, 0xa

    const/16 v2, 0xa

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lsoftware/simplicial/a/t;->a(IIZZ)V

    .line 550
    return-void

    :cond_1
    move v0, v1

    .line 541
    goto :goto_0
.end method

.method private b()I
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->B:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->z:I

    goto :goto_0
.end method

.method private b(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 302
    const/4 v0, 0x0

    .line 303
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 304
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 305
    :cond_0
    return v1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bg;)I
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v0

    return v0
.end method

.method private c(Ljava/util/List;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const v3, 0x7f0801e6

    .line 310
    const-string v0, ""

    .line 312
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08012a

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n1. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 321
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 322
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n2. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1, v2}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 323
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 325
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f080262

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n3. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1, v4}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 328
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 329
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n4. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1, v5}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v6, :cond_3

    .line 332
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08020f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n5. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1, v6}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x5

    if-le v1, v2, :cond_4

    .line 336
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n6. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x6

    if-le v1, v2, :cond_5

    .line 338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n7. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x6

    invoke-direct {p0, p1, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    :cond_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x7

    if-le v1, v2, :cond_6

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n8. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x7

    invoke-direct {p0, p1, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    :cond_6
    return-object v0

    .line 319
    :cond_7
    const v0, 0x7f080102

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private c()V
    .locals 10

    .prologue
    const v0, 0x7f0802a5

    const v9, 0x7f08022d

    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 360
    :try_start_0
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bg;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v3, :cond_0

    move v3, v0

    :goto_0
    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->v:Lsoftware/simplicial/a/i/e;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v3, v4}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/i/e;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    sget-object v0, Lsoftware/simplicial/nebulous/application/bg$9;->a:[I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->v:Lsoftware/simplicial/a/i/e;

    invoke-virtual {v3}, Lsoftware/simplicial/a/i/e;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 403
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 404
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {p0, v9}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 407
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 408
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 411
    :goto_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->r:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->v:Lsoftware/simplicial/a/i/e;

    sget-object v4, Lsoftware/simplicial/a/i/e;->d:Lsoftware/simplicial/a/i/e;

    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 412
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->s:Landroid/widget/ImageButton;

    iget v3, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    if-lez v3, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 414
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->D:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 415
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_5

    .line 360
    :cond_0
    const v3, 0x7f0802cc

    goto/16 :goto_0

    .line 362
    :catch_0
    move-exception v3

    .line 364
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->i:Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v4, :cond_1

    :goto_6
    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_1
    const v0, 0x7f0802cc

    goto :goto_6

    .line 371
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 372
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    const v3, 0x7f0802dd

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 375
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 376
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_2

    .line 379
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c007a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 380
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {p0, v9}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v0

    if-ltz v0, :cond_2

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 383
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 384
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_2

    :cond_2
    move v0, v2

    .line 382
    goto :goto_7

    .line 387
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c006c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 388
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    const v3, 0x7f0802dd

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 390
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 391
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 392
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_2

    .line 395
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 396
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 397
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {p0, v9}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 399
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 400
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 411
    goto/16 :goto_3

    :cond_4
    move v1, v2

    .line 412
    goto/16 :goto_4

    .line 416
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f080201

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->f:Landroid/widget/TextView;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->y:I

    if-ltz v0, :cond_7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    const-string v0, ""

    .line 419
    iget v1, p0, Lsoftware/simplicial/nebulous/application/bg;->x:I

    if-ltz v1, :cond_6

    .line 420
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08028e

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lsoftware/simplicial/nebulous/application/bg;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f08025c

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 421
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f080230

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/bg;->E:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 423
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->g:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bg;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->E:I

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->b()I

    move-result v3

    if-lt v0, v3, :cond_8

    const v0, 0x7f0c0056

    :goto_9
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 425
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->w:I

    packed-switch v0, :pswitch_data_1

    .line 440
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08023e

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lsoftware/simplicial/nebulous/application/bg;->w:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    :goto_a
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bg;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 445
    return-void

    .line 417
    :cond_7
    const-string v0, "---"

    goto/16 :goto_8

    .line 424
    :cond_8
    const v0, 0x7f0c007a

    goto :goto_9

    .line 428
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    const v1, 0x7f0802c5

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 431
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    const v1, 0x7f08012a

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 434
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    const v1, 0x7f080262

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 437
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    const v1, 0x7f08020f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    .line 368
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 425
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private d()V
    .locals 7

    .prologue
    .line 509
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/bg;->J:Ljava/lang/Object;

    monitor-enter v6

    .line 511
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->e()V

    .line 513
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->I:Ljava/util/Timer;

    .line 514
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->I:Ljava/util/Timer;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bg$8;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bg$8;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 536
    monitor-exit v6

    .line 537
    return-void

    .line 536
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 554
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->J:Ljava/lang/Object;

    monitor-enter v1

    .line 556
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->I:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->I:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 559
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->I:Ljava/util/Timer;

    .line 561
    :cond_0
    monitor-exit v1

    .line 562
    return-void

    .line 561
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Ljava/util/List;Lsoftware/simplicial/a/i/e;IILjava/util/List;ILjava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/i/b;",
            ">;",
            "Lsoftware/simplicial/a/i/e;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 450
    iget-object v9, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 451
    if-nez v9, :cond_0

    .line 477
    :goto_0
    return-void

    .line 453
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/bg$6;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move v5, p3

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lsoftware/simplicial/nebulous/application/bg$6;-><init>(Lsoftware/simplicial/nebulous/application/bg;Ljava/util/List;Lsoftware/simplicial/a/i/e;IIILjava/util/List;Ljava/util/List;)V

    invoke-virtual {v9, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/i/e;IZZ)V
    .locals 7

    .prologue
    .line 482
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 483
    if-nez v6, :cond_0

    .line 504
    :goto_0
    return-void

    .line 485
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/bg$7;

    move-object v1, p0

    move v2, p3

    move v3, p4

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/application/bg$7;-><init>(Lsoftware/simplicial/nebulous/application/bg;ZZLsoftware/simplicial/a/i/e;I)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 567
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 570
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_2

    .line 572
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    .line 573
    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Z)V

    .line 575
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_0

    .line 577
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-boolean p2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    .line 578
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->c()V

    .line 579
    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f0800d3

    const v3, 0x7f0800c6

    const v2, 0x1080027

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->n:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->p:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->D:Ljava/util/List;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 176
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->D:Ljava/util/List;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 177
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->o:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->C:Ljava/util/List;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->C:Ljava/util/List;

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->c(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 183
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 185
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->r:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v1, Lsoftware/simplicial/a/i/c;->d:Lsoftware/simplicial/a/i/c;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/i/c;ZZI)V

    goto :goto_0

    .line 189
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 191
    sget-object v0, Lsoftware/simplicial/nebulous/application/bg$9;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->v:Lsoftware/simplicial/a/i/e;

    invoke-virtual {v1}, Lsoftware/simplicial/a/i/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 194
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    if-eqz v0, :cond_5

    .line 196
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 197
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 198
    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08022d

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 200
    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 202
    const v1, 0x7f08025f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg$2;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bg$2;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 214
    const v1, 0x7f08012f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bg$3;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 226
    const v1, 0x7f08025e

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bg$4;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 243
    :cond_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 244
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 245
    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08022d

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 247
    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 246
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 249
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg$5;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bg$5;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 262
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 263
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 268
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v1, Lsoftware/simplicial/a/i/c;->c:Lsoftware/simplicial/a/i/c;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/i/c;ZZI)V

    goto/16 :goto_0

    .line 272
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->s:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_8

    .line 274
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    if-lez v0, :cond_7

    .line 275
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    .line 276
    :cond_7
    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Z)V

    goto/16 :goto_0

    .line 278
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->t:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 280
    iget v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/bg;->K:I

    .line 281
    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/application/bg;->a(Z)V

    goto/16 :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 83
    const v0, 0x7f04005e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 85
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    const v0, 0x7f0d02a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->c:Landroid/widget/ListView;

    .line 88
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->d:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0d02a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->e:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0d0087

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->f:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0d01d0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->g:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0d02a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->h:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0d00bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->i:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    .line 95
    const v0, 0x7f0d02a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    .line 96
    const v0, 0x7f0d02a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->l:Landroid/widget/LinearLayout;

    .line 97
    const v0, 0x7f0d0129

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->m:Landroid/widget/RelativeLayout;

    .line 98
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->n:Landroid/widget/Button;

    .line 99
    const v0, 0x7f0d02a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->o:Landroid/widget/Button;

    .line 100
    const v0, 0x7f0d02a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->p:Landroid/widget/Button;

    .line 101
    const v0, 0x7f0d02a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    .line 102
    const v0, 0x7f0d02a2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->r:Landroid/widget/Button;

    .line 103
    const v0, 0x7f0d012d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->s:Landroid/widget/ImageButton;

    .line 104
    const v0, 0x7f0d012e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->t:Landroid/widget/ImageButton;

    .line 106
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 163
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->e()V

    .line 164
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 135
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 137
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->c()V

    .line 139
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bg$1;-><init>(Lsoftware/simplicial/nebulous/application/bg;)V

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/al$ai;)V

    .line 154
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bg;->d()V

    .line 155
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 115
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 117
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->n:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->p:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->r:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->j:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 127
    new-instance v0, Lsoftware/simplicial/nebulous/a/v;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/v;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->u:Lsoftware/simplicial/nebulous/a/v;

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bg;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bg;->u:Lsoftware/simplicial/nebulous/a/v;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    return-void
.end method
