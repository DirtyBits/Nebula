.class public final enum Lsoftware/simplicial/nebulous/application/bc$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/application/bc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/application/bc$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/application/bc$a;

.field public static final enum b:Lsoftware/simplicial/nebulous/application/bc$a;

.field private static final synthetic c:[Lsoftware/simplicial/nebulous/application/bc$a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, Lsoftware/simplicial/nebulous/application/bc$a;

    const-string v1, "ACCOUNT"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/bc$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    new-instance v0, Lsoftware/simplicial/nebulous/application/bc$a;

    const-string v1, "CLAN"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/application/bc$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    .line 86
    const/4 v0, 0x2

    new-array v0, v0, [Lsoftware/simplicial/nebulous/application/bc$a;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->b:Lsoftware/simplicial/nebulous/application/bc$a;

    aput-object v1, v0, v3

    sput-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->c:[Lsoftware/simplicial/nebulous/application/bc$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/application/bc$a;
    .locals 1

    .prologue
    .line 86
    const-class v0, Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/application/bc$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/application/bc$a;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->c:[Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/application/bc$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/application/bc$a;

    return-object v0
.end method


# virtual methods
.method public a()Lsoftware/simplicial/nebulous/f/o$b;
    .locals 2

    .prologue
    .line 92
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$2;->a:[I

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bc$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 95
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$b;->a:Lsoftware/simplicial/nebulous/f/o$b;

    goto :goto_0

    .line 97
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/o$b;->b:Lsoftware/simplicial/nebulous/f/o$b;

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
