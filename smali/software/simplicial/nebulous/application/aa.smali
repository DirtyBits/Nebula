.class public Lsoftware/simplicial/nebulous/application/aa;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/e/a;
.implements Lsoftware/simplicial/nebulous/f/al$n;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Button;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/RelativeLayout;

.field f:Landroid/widget/RelativeLayout;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/TextView;

.field private k:Lsoftware/simplicial/a/aa;

.field private l:Lsoftware/simplicial/a/aa;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:J

.field private s:I

.field private t:I

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lsoftware/simplicial/nebulous/application/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/aa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 40
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->k:Lsoftware/simplicial/a/aa;

    .line 41
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    .line 42
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->m:Z

    .line 43
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->o:Z

    .line 46
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->p:Z

    .line 47
    iput v2, p0, Lsoftware/simplicial/nebulous/application/aa;->q:I

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/application/aa;->r:J

    .line 49
    iput v2, p0, Lsoftware/simplicial/nebulous/application/aa;->s:I

    .line 50
    iput v2, p0, Lsoftware/simplicial/nebulous/application/aa;->t:I

    .line 51
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->u:Z

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/aa;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lsoftware/simplicial/nebulous/application/aa;->s:I

    return v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/aa;Lsoftware/simplicial/a/aa;)Lsoftware/simplicial/a/aa;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/aa;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/aa;->p:Z

    return p1
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 184
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->f:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 190
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->i:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->j:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/aa;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aa;->b()V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/aa;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/aa;->o:Z

    return p1
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/aa;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lsoftware/simplicial/nebulous/application/aa;->t:I

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/aa;Z)Z
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    return p1
.end method


# virtual methods
.method public a(Lsoftware/simplicial/a/aa;ZIJII)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 200
    :cond_0
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->o:Z

    .line 201
    iput p3, p0, Lsoftware/simplicial/nebulous/application/aa;->q:I

    .line 202
    iput-wide p4, p0, Lsoftware/simplicial/nebulous/application/aa;->r:J

    .line 203
    iput p6, p0, Lsoftware/simplicial/nebulous/application/aa;->s:I

    .line 204
    iput p7, p0, Lsoftware/simplicial/nebulous/application/aa;->t:I

    .line 205
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/aa;->k:Lsoftware/simplicial/a/aa;

    .line 206
    iput-boolean p2, p0, Lsoftware/simplicial/nebulous/application/aa;->m:Z

    .line 208
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->p:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->k:Lsoftware/simplicial/a/aa;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->m:Z

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    if-eq v0, v1, :cond_2

    .line 210
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/aa;->a:Lsoftware/simplicial/a/aa;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    .line 211
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    .line 212
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aa;->b()V

    .line 214
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    .line 217
    const-wide/16 v0, 0x32

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 219
    :catch_0
    move-exception v0

    .line 221
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 227
    :cond_2
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/aa;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 233
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/aa;->k:Lsoftware/simplicial/a/aa;

    invoke-virtual {v0, v4}, Lsoftware/simplicial/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/aa;->m:Z

    if-ne v0, v4, :cond_7

    .line 237
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->i:Landroid/widget/TextView;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v4, v5}, Lsoftware/simplicial/nebulous/f/r;->a(Lsoftware/simplicial/a/aa;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/aa;->j:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f080313

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/aa;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    iget v5, v5, Lsoftware/simplicial/a/aa;->t:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    if-eqz v0, :cond_0

    const v0, 0x7f08006c

    :goto_0
    invoke-virtual {v6, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/aa;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0c0022

    :goto_1
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 241
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->o:Z

    if-eqz v0, :cond_2

    .line 261
    :goto_2
    return-void

    .line 238
    :cond_0
    const v0, 0x7f08014f

    goto :goto_0

    .line 239
    :cond_1
    const v0, 0x7f0c001d

    goto :goto_1

    .line 244
    :cond_2
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    if-eqz v0, :cond_5

    .line 246
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/aa;->e:Landroid/widget/RelativeLayout;

    iget v0, p0, Lsoftware/simplicial/nebulous/application/aa;->q:I

    if-lez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->f:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->c:Landroid/widget/Button;

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->u:Z

    if-nez v2, :cond_3

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/application/aa;->r:J

    iget v2, p0, Lsoftware/simplicial/nebulous/application/aa;->s:I

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    move v1, v3

    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 249
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/aa;->s:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 246
    goto :goto_3

    .line 253
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->e:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->f:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->d:Landroid/widget/Button;

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->u:Z

    if-nez v2, :cond_6

    iget-wide v4, p0, Lsoftware/simplicial/nebulous/application/aa;->r:J

    iget v2, p0, Lsoftware/simplicial/nebulous/application/aa;->t:I

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-lez v2, :cond_6

    :goto_4
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 256
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/aa;->t:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    move v3, v1

    .line 255
    goto :goto_4

    .line 260
    :cond_7
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aa;->b()V

    goto/16 :goto_2
.end method

.method public o_()V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 267
    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 270
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/aa$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aa$4;-><init>(Lsoftware/simplicial/nebulous/application/aa;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 56
    const v0, 0x7f04003f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->b:Landroid/widget/Button;

    .line 61
    const v0, 0x7f0d014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->c:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0d0163

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->d:Landroid/widget/Button;

    .line 64
    const v0, 0x7f0d0165

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->e:Landroid/widget/RelativeLayout;

    .line 65
    const v0, 0x7f0d0162

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->f:Landroid/widget/RelativeLayout;

    .line 67
    const v0, 0x7f0d0166

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->g:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0d0164

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->h:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0d0160

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->i:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0d0161

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->j:Landroid/widget/TextView;

    .line 72
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/application/MainActivity;->b:Lsoftware/simplicial/nebulous/e/a;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    .line 180
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 157
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 159
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aa;->b()V

    .line 161
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    sget-object v3, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->u:Z

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 164
    iget-object v3, v0, Lsoftware/simplicial/a/u;->m:Lsoftware/simplicial/a/aa;

    iput-object v3, p0, Lsoftware/simplicial/nebulous/application/aa;->l:Lsoftware/simplicial/a/aa;

    .line 165
    iget-boolean v0, v0, Lsoftware/simplicial/a/u;->o:Z

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aa;->n:Z

    .line 166
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/aa;->o:Z

    .line 167
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/aa;->p:Z

    .line 169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    .line 171
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aa;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/al$n;)V

    .line 172
    return-void

    :cond_0
    move v0, v2

    .line 161
    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->b:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/aa$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aa$1;-><init>(Lsoftware/simplicial/nebulous/application/aa;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->c:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/aa$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aa$2;-><init>(Lsoftware/simplicial/nebulous/application/aa;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aa;->d:Landroid/widget/Button;

    new-instance v1, Lsoftware/simplicial/nebulous/application/aa$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/aa$3;-><init>(Lsoftware/simplicial/nebulous/application/aa;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    return-void
.end method
