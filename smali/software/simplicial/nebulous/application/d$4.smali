.class Lsoftware/simplicial/nebulous/application/d$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/d;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/d;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/d;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 182
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/b/d;ZZ)V

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/d;->b(Lsoftware/simplicial/nebulous/application/d;)I

    move-result v0

    if-lez v0, :cond_0

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ARENA_TOKEN_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d$4;->a:Lsoftware/simplicial/nebulous/application/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/d;->b(Lsoftware/simplicial/nebulous/application/d;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method
