.class Lsoftware/simplicial/nebulous/application/MainActivity$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/f/a;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/a;)V
    .locals 0

    .prologue
    .line 1448
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->a:Lsoftware/simplicial/nebulous/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    const/4 v0, 0x1

    .line 1452
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->j(Lsoftware/simplicial/nebulous/application/MainActivity;)I

    .line 1462
    :try_start_0
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->a:Lsoftware/simplicial/nebulous/f/a;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->k(Lsoftware/simplicial/nebulous/application/MainActivity;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 2041
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->w(Lsoftware/simplicial/nebulous/application/MainActivity;)I

    .line 2043
    :goto_0
    return-void

    .line 1468
    :cond_1
    :try_start_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->u()V

    .line 1470
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    .line 1472
    sget-object v3, Lsoftware/simplicial/nebulous/application/MainActivity$48;->a:[I

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v5}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v5

    aget v3, v3, v5

    sparse-switch v3, :sswitch_data_0

    .line 1495
    :goto_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v5

    .line 1497
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->a:Lsoftware/simplicial/nebulous/f/a;

    sget-object v6, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v3, v6, :cond_6

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v6, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;

    if-ne v3, v6, :cond_6

    .line 1499
    const/16 v3, 0xe

    .line 1502
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x12

    if-ge v6, v7, :cond_2

    .line 1504
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->l(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getRotation()I

    move-result v6

    .line 1505
    packed-switch v6, :pswitch_data_0

    :cond_2
    move v0, v3

    .line 1522
    :goto_2
    :pswitch_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->setRequestedOrientation(I)V

    .line 1523
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->g()V

    .line 1531
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1532
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1533
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    const/4 v1, -0x1

    iput v1, v0, Lsoftware/simplicial/a/t;->s:I

    .line 1536
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->a:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2026
    :cond_3
    :goto_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->a:Lsoftware/simplicial/nebulous/f/a;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    .line 2028
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4

    .line 2029
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->s(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    .line 2030
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_5

    .line 2032
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->r(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2033
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->t(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2034
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->u(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2035
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->v(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2037
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->w(Lsoftware/simplicial/nebulous/application/MainActivity;)I

    goto/16 :goto_0

    .line 1491
    :sswitch_0
    :try_start_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 2041
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->w(Lsoftware/simplicial/nebulous/application/MainActivity;)I

    throw v0

    :pswitch_1
    move v0, v1

    .line 1512
    goto/16 :goto_2

    .line 1514
    :pswitch_2
    const/16 v0, 0x9

    .line 1515
    goto/16 :goto_2

    :pswitch_3
    move v0, v2

    .line 1517
    goto/16 :goto_2

    .line 1527
    :cond_6
    :try_start_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->setRequestedOrientation(I)V

    .line 1528
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->h()V

    goto/16 :goto_3

    .line 1539
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 1540
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->w:Z

    .line 1541
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    .line 1543
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->j()V

    .line 1544
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->p()V

    .line 1545
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1546
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/af;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/af;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1548
    if-eqz v5, :cond_7

    .line 1550
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v1, v5, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/bf;->l:F

    .line 1551
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v1, v5, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/bf;->m:F

    .line 1552
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v1, v5, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/bf;->s:F

    .line 1553
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v1, v5, Lsoftware/simplicial/a/u;->aE:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/a/bf;->t:F

    .line 1555
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->m(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    goto/16 :goto_4

    .line 1558
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0800c7

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1559
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->n(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1561
    if-eqz v5, :cond_3

    .line 1562
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->i()V

    goto/16 :goto_4

    .line 1565
    :pswitch_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0800c7

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1566
    if-eqz v5, :cond_3

    .line 1567
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->i()V

    goto/16 :goto_4

    .line 1570
    :pswitch_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0800c7

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1571
    if-eqz v5, :cond_3

    .line 1572
    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->i()V

    goto/16 :goto_4

    .line 1575
    :pswitch_8
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1576
    new-instance v0, Lsoftware/simplicial/nebulous/application/ae;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/ae;-><init>()V

    .line 1577
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/ae;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ae;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1579
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_8

    invoke-virtual {v5}, Lsoftware/simplicial/a/u;->d()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v0, v0, Lsoftware/simplicial/a/bf;->S:I

    if-lez v0, :cond_8

    .line 1581
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->o(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    .line 1582
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801dc

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1584
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    .line 1586
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->p(Lsoftware/simplicial/nebulous/application/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->c:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_3

    .line 1587
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->q(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    goto/16 :goto_4

    .line 1590
    :pswitch_9
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1591
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ah;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ah;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ah;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ah;->a:Ljava/lang/String;

    .line 1592
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1594
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    .line 1596
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->p(Lsoftware/simplicial/nebulous/application/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->d:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_3

    .line 1597
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->q(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    goto/16 :goto_4

    .line 1601
    :pswitch_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080166

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1602
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->m(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    goto/16 :goto_4

    .line 1605
    :pswitch_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->o()Lsoftware/simplicial/a/bf;

    move-result-object v0

    iget v0, v0, Lsoftware/simplicial/a/bf;->aD:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_9

    .line 1607
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 1608
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080286

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 1609
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 1610
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1612
    :cond_9
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1613
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    .line 1614
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->m(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    goto/16 :goto_4

    .line 1617
    :pswitch_c
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1618
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ai;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v3, v3, Lsoftware/simplicial/a/t;->q:Ljava/lang/String;

    invoke-direct {v2, v3}, Lsoftware/simplicial/nebulous/application/ai;-><init>(Ljava/lang/String;)V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1620
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    goto/16 :goto_4

    .line 1623
    :pswitch_d
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1624
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_c

    .line 1625
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1628
    :cond_b
    :goto_5
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ag;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ag;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ag;->a:Ljava/lang/String;

    .line 1629
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1626
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_b

    .line 1627
    :cond_d
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ag;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_5

    .line 1632
    :pswitch_e
    new-instance v0, Lsoftware/simplicial/nebulous/application/a;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/a;-><init>()V

    .line 1633
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->f:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->r:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_e

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->Q:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_10

    .line 1636
    :cond_e
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/a;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1640
    :cond_f
    :goto_6
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1641
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/a;->a:Ljava/lang/String;

    .line 1642
    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/a;->a:Ljava/lang/String;

    .line 1643
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1644
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1637
    :cond_10
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_11

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_f

    .line 1638
    :cond_11
    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/a;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_6

    .line 1647
    :pswitch_f
    new-instance v0, Lsoftware/simplicial/nebulous/application/bd;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/bd;-><init>()V

    .line 1649
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_12

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_14

    .line 1650
    :cond_12
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/bd;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1654
    :cond_13
    :goto_7
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1655
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/bd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1651
    :cond_14
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_15

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_13

    .line 1652
    :cond_15
    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/bd;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_7

    .line 1658
    :pswitch_10
    new-instance v0, Lsoftware/simplicial/nebulous/application/ab;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/ab;-><init>()V

    .line 1659
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1660
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/ab;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1663
    :pswitch_11
    new-instance v0, Lsoftware/simplicial/nebulous/application/bi;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/bi;-><init>()V

    .line 1664
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1665
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/bi;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bi;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1668
    :pswitch_12
    new-instance v0, Lsoftware/simplicial/nebulous/application/l;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/l;-><init>()V

    .line 1669
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1670
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1673
    :pswitch_13
    new-instance v0, Lsoftware/simplicial/nebulous/application/bb;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/bb;-><init>()V

    .line 1674
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1675
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/bb;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1678
    :pswitch_14
    new-instance v0, Lsoftware/simplicial/nebulous/application/k;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/k;-><init>()V

    .line 1679
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1680
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/k;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1683
    :pswitch_15
    new-instance v0, Lsoftware/simplicial/nebulous/application/m;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/m;-><init>()V

    .line 1684
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1685
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1688
    :pswitch_16
    new-instance v0, Lsoftware/simplicial/nebulous/application/j;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/j;-><init>()V

    .line 1689
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1690
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1693
    :pswitch_17
    new-instance v0, Lsoftware/simplicial/nebulous/application/i;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/i;-><init>()V

    .line 1694
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1695
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1698
    :pswitch_18
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1699
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ap;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ap;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ap;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ap;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1702
    :pswitch_19
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1703
    new-instance v0, Lsoftware/simplicial/nebulous/application/as;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/as;-><init>()V

    .line 1704
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->r(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1705
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/as;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/as;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1708
    :pswitch_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->f:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->N:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->Y:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ab:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_18

    .line 1711
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ar;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1714
    :cond_17
    :goto_8
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1715
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ar;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ar;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ar;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ar;->a:Ljava/lang/String;

    .line 1716
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1712
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_19

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aj:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_17

    .line 1713
    :cond_19
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ar;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_8

    .line 1719
    :pswitch_1b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->j()V

    .line 1721
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1722
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/be;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/be;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/be;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/be;->a:Ljava/lang/String;

    .line 1723
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1726
    :pswitch_1c
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1727
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/h;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/h;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/h;->a:Ljava/lang/String;

    .line 1728
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1731
    :pswitch_1d
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1732
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ad;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ad;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ad;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ad;->a:Ljava/lang/String;

    .line 1733
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1736
    :pswitch_1e
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1737
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/at;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/at;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/at;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/at;->a:Ljava/lang/String;

    .line 1738
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1741
    :pswitch_1f
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1742
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/au;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/au;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/au;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/au;->a:Ljava/lang/String;

    .line 1743
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1746
    :pswitch_20
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1747
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/o;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/o;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/o;->a:Ljava/lang/String;

    .line 1748
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1751
    :pswitch_21
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_1c

    .line 1752
    :cond_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/v;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1755
    :cond_1b
    :goto_9
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1756
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/v;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/v;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/v;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/v;->a:Ljava/lang/String;

    .line 1757
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1753
    :cond_1c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_1b

    .line 1754
    :cond_1d
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/v;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_9

    .line 1760
    :pswitch_22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1e

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_20

    .line 1761
    :cond_1e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/d;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1764
    :cond_1f
    :goto_a
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1765
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/d;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/d;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/d;->a:Ljava/lang/String;

    .line 1766
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1762
    :cond_20
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_21

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_21

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_1f

    .line 1763
    :cond_21
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/d;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_a

    .line 1769
    :pswitch_23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_22

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_24

    .line 1770
    :cond_22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bg;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1773
    :cond_23
    :goto_b
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1774
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/bg;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/bg;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/bg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bg;->a:Ljava/lang/String;

    .line 1775
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1771
    :cond_24
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_25

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_25

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_23

    .line 1772
    :cond_25
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bg;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_b

    .line 1778
    :pswitch_24
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_27

    .line 1779
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/e;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1782
    :cond_26
    :goto_c
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1783
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/e;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/e;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/e;->a:Ljava/lang/String;

    .line 1784
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1780
    :cond_27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_28

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_28

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_26

    .line 1781
    :cond_28
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/e;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_c

    .line 1787
    :pswitch_25
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_29

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_29

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_29

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->y:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_2b

    .line 1789
    :cond_29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bf;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1793
    :cond_2a
    :goto_d
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1794
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/bf;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/bf;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/bf;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bf;->a:Ljava/lang/String;

    .line 1795
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1790
    :cond_2b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_2c

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_2c

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_2a

    .line 1791
    :cond_2c
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bf;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_d

    .line 1798
    :pswitch_26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_2d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_2d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_2d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->y:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_2f

    .line 1800
    :cond_2d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/b;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1804
    :cond_2e
    :goto_e
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1805
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/b;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/b;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/b;->a:Ljava/lang/String;

    .line 1806
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1801
    :cond_2f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_30

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_30

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_2e

    .line 1802
    :cond_30
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/b;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_e

    .line 1809
    :pswitch_27
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1810
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/s;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/s;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/s;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/s;->a:Ljava/lang/String;

    .line 1811
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1814
    :pswitch_28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->p()V

    .line 1815
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->j()V

    .line 1817
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1818
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/af;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/af;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/af;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1821
    :pswitch_29
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_31

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_33

    .line 1822
    :cond_31
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ac;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1826
    :cond_32
    :goto_f
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1827
    new-instance v0, Lsoftware/simplicial/nebulous/application/ac;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/ac;-><init>()V

    .line 1828
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1829
    const-string v2, "friendID"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1830
    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/ac;->setArguments(Landroid/os/Bundle;)V

    .line 1831
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/ac;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ac;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1823
    :cond_33
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_34

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_34

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_32

    .line 1824
    :cond_34
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ac;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_f

    .line 1834
    :pswitch_2a
    new-instance v0, Lsoftware/simplicial/nebulous/application/an;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/an;-><init>()V

    .line 1835
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1836
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/an;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/an;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1840
    :pswitch_2b
    new-instance v0, Lsoftware/simplicial/nebulous/application/t;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/t;-><init>()V

    .line 1841
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1842
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    const-string v3, "ClanPermissionsFragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "ClanPermissionsFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1845
    :pswitch_2c
    new-instance v0, Lsoftware/simplicial/nebulous/application/p;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/p;-><init>()V

    .line 1846
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1847
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/p;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1850
    :pswitch_2d
    new-instance v0, Lsoftware/simplicial/nebulous/application/aj;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/aj;-><init>()V

    .line 1851
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_36

    .line 1852
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/aj;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1856
    :cond_35
    :goto_10
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1857
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/aj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/aj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1853
    :cond_36
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_37

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_37

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_35

    .line 1854
    :cond_37
    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/aj;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_10

    .line 1860
    :pswitch_2e
    new-instance v0, Lsoftware/simplicial/nebulous/application/bj;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/bj;-><init>()V

    .line 1861
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->Z:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_38

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->ab:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_38

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_39

    .line 1862
    :cond_38
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/bj;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1864
    :cond_39
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1865
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/bj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1868
    :pswitch_2f
    new-instance v0, Lsoftware/simplicial/nebulous/application/aw;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/aw;-><init>()V

    .line 1870
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1871
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/aw;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/aw;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1874
    :pswitch_30
    new-instance v0, Lsoftware/simplicial/nebulous/application/am;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/am;-><init>()V

    .line 1876
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1877
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    const-string v3, "MemberPermissionsFragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "MemberPermissionsFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1880
    :pswitch_31
    new-instance v0, Lsoftware/simplicial/nebulous/application/aq;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/aq;-><init>()V

    .line 1881
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_3a

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_3c

    .line 1882
    :cond_3a
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/aq;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1886
    :cond_3b
    :goto_11
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1887
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/aq;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/aq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1883
    :cond_3c
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_3d

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v1, v2, :cond_3d

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v1, v2, :cond_3b

    .line 1884
    :cond_3d
    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v1, Lsoftware/simplicial/nebulous/application/aq;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_11

    .line 1890
    :pswitch_32
    new-instance v0, Lsoftware/simplicial/nebulous/application/y;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/y;-><init>()V

    .line 1892
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1893
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/y;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1896
    :pswitch_33
    new-instance v0, Lsoftware/simplicial/nebulous/application/av;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/av;-><init>()V

    .line 1898
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1899
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/av;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/av;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1902
    :pswitch_34
    new-instance v0, Lsoftware/simplicial/nebulous/application/aa;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/aa;-><init>()V

    .line 1904
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1905
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/aa;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/aa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1908
    :pswitch_35
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_3e

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_3e

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_40

    .line 1909
    :cond_3e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->c:Lsoftware/simplicial/nebulous/f/a;

    .line 1912
    :cond_3f
    :goto_12
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1913
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ay;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ay;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ay;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ay;->a:Ljava/lang/String;

    .line 1914
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1910
    :cond_40
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_41

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_41

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_3f

    .line 1911
    :cond_41
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ay;->c:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_12

    .line 1917
    :pswitch_36
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_42

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_42

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_42

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_44

    .line 1919
    :cond_42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/az;->c:Lsoftware/simplicial/nebulous/f/a;

    .line 1922
    :cond_43
    :goto_13
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1923
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/az;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/az;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/az;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/az;->a:Ljava/lang/String;

    .line 1924
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1920
    :cond_44
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_45

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_45

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_43

    .line 1921
    :cond_45
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/az;->c:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_13

    .line 1927
    :pswitch_37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_46

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_48

    .line 1928
    :cond_46
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->c:Lsoftware/simplicial/nebulous/f/a;

    .line 1931
    :cond_47
    :goto_14
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1932
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ba;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ba;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ba;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ba;->a:Ljava/lang/String;

    .line 1933
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1929
    :cond_48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_49

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_49

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_47

    .line 1930
    :cond_49
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ba;->c:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_14

    .line 1936
    :pswitch_38
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1937
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/al;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/al;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/al;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/al;->a:Ljava/lang/String;

    .line 1938
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1941
    :pswitch_39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_4c

    .line 1942
    :cond_4a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/w;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1946
    :cond_4b
    :goto_15
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1947
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/w;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/w;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/w;->a:Ljava/lang/String;

    .line 1948
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1943
    :cond_4c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_4b

    .line 1944
    :cond_4d
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/w;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_15

    .line 1951
    :pswitch_3a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4e

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_4e

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_50

    .line 1952
    :cond_4e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/f;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1956
    :cond_4f
    :goto_16
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1957
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/f;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/f;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/f;->a:Ljava/lang/String;

    .line 1958
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1953
    :cond_50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_51

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_4f

    .line 1954
    :cond_51
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/f;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_16

    .line 1961
    :pswitch_3b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_52

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_54

    .line 1962
    :cond_52
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ak;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1965
    :cond_53
    :goto_17
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1966
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ak;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ak;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ak;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ak;->a:Ljava/lang/String;

    .line 1967
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1963
    :cond_54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_55

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_55

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_53

    .line 1964
    :cond_55
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/ak;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_17

    .line 1970
    :pswitch_3c
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1971
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/x;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/x;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/x;->a:Ljava/lang/String;

    .line 1972
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1975
    :pswitch_3d
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1976
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/ax;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/ax;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/ax;->a:Ljava/lang/String;

    .line 1977
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1980
    :pswitch_3e
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1981
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/z;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/z;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/z;->a:Ljava/lang/String;

    .line 1982
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1985
    :pswitch_3f
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1986
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/q;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/q;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/q;->a:Ljava/lang/String;

    .line 1987
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1990
    :pswitch_40
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_56

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_57

    .line 1991
    :cond_56
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/c;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 1993
    :cond_57
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1994
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0075

    new-instance v2, Lsoftware/simplicial/nebulous/application/c;

    invoke-direct {v2}, Lsoftware/simplicial/nebulous/application/c;-><init>()V

    sget-object v3, Lsoftware/simplicial/nebulous/application/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/c;->a:Ljava/lang/String;

    .line 1995
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 1998
    :pswitch_41
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1999
    new-instance v0, Lsoftware/simplicial/nebulous/application/bh;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/bh;-><init>()V

    .line 2000
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->ag:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/bh;->setArguments(Landroid/os/Bundle;)V

    .line 2001
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/bh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/bh;->a:Ljava/lang/String;

    .line 2002
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 2005
    :pswitch_42
    new-instance v0, Lsoftware/simplicial/nebulous/application/g;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/g;-><init>()V

    .line 2006
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 2007
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 2010
    :pswitch_43
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_58

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_58

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_58

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_5a

    .line 2011
    :cond_58
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/r;->b:Lsoftware/simplicial/nebulous/f/a;

    .line 2015
    :cond_59
    :goto_18
    new-instance v0, Lsoftware/simplicial/nebulous/application/r;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/r;-><init>()V

    .line 2016
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 2017
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/r;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_4

    .line 2012
    :cond_5a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_5b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$5;->b:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_59

    .line 2013
    :cond_5b
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/r;->b:Lsoftware/simplicial/nebulous/f/a;

    goto :goto_18

    .line 2020
    :pswitch_44
    new-instance v0, Lsoftware/simplicial/nebulous/application/u;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/u;-><init>()V

    .line 2021
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v4, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 2022
    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0075

    sget-object v3, Lsoftware/simplicial/nebulous/application/u;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/nebulous/application/u;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 1472
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0xa -> :sswitch_0
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x18 -> :sswitch_0
        0x1a -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x3a -> :sswitch_0
    .end sparse-switch

    .line 1505
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1536
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_d
        :pswitch_e
        :pswitch_4
        :pswitch_b
        :pswitch_c
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_25
        :pswitch_26
        :pswitch_1b
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_24
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_23
        :pswitch_42
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_14
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_41
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_27
        :pswitch_1c
        :pswitch_1d
        :pswitch_40
        :pswitch_43
        :pswitch_44
        :pswitch_28
    .end packed-switch
.end method
