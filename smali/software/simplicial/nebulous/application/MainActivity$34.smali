.class Lsoftware/simplicial/nebulous/application/MainActivity$34;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 660
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 664
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 666
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->e(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 667
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->e(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/List;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 668
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->e(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/List;

    move-result-object v0

    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 669
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 670
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    .line 671
    iget-object v4, v0, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 673
    iget-boolean v0, v6, Lsoftware/simplicial/a/bf;->aF:Z

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, v6, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    iget-object v7, v6, Lsoftware/simplicial/a/bf;->E:[B

    invoke-static {v0, v7}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->e(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/List;

    move-result-object v0

    iget v6, v6, Lsoftware/simplicial/a/bf;->C:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 675
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v7, v6, Lsoftware/simplicial/a/bf;->C:I

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 680
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 681
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f0801a9

    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 682
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f080040

    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 683
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 684
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$34;->a:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 686
    :cond_3
    return v2
.end method
