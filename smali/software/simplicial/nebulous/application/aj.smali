.class public Lsoftware/simplicial/nebulous/application/aj;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/Button;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/ListView;

.field j:Lsoftware/simplicial/nebulous/a/u;

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lsoftware/simplicial/nebulous/application/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/aj;->a:Ljava/lang/String;

    .line 28
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/aj;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const v2, 0x7f020236

    const v1, 0x7f020235

    .line 88
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->g:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 98
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->j:Lsoftware/simplicial/nebulous/a/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/u;->clear()V

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->j:Lsoftware/simplicial/nebulous/a/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/u;->notifyDataSetChanged()V

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->j:Lsoftware/simplicial/nebulous/a/u;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/a/u;->a(Z)V

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    new-instance v2, Lsoftware/simplicial/nebulous/application/aj$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/aj$1;-><init>(Lsoftware/simplicial/nebulous/application/aj;)V

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/al;->a(ZLsoftware/simplicial/nebulous/f/al$v;)V

    .line 117
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 128
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aj;->a()V

    goto :goto_0

    .line 130
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 132
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0

    .line 134
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    .line 137
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aj;->a()V

    goto :goto_0

    .line 139
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/aj;->k:Z

    .line 142
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aj;->a()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 44
    const v0, 0x7f040049

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 46
    const v0, 0x7f0d01e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->i:Landroid/widget/ListView;

    .line 47
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->h:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0d01e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->e:Landroid/widget/Button;

    .line 49
    const v0, 0x7f0d01e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->d:Landroid/widget/Button;

    .line 50
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->c:Landroid/widget/Button;

    .line 51
    const v0, 0x7f0d01e4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->f:Landroid/widget/Button;

    .line 52
    const v0, 0x7f0d01e5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->g:Landroid/widget/Button;

    .line 54
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 84
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 77
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/aj;->a()V

    .line 78
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    new-instance v0, Lsoftware/simplicial/nebulous/a/u;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aj;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/u;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->j:Lsoftware/simplicial/nebulous/a/u;

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/aj;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/aj;->j:Lsoftware/simplicial/nebulous/a/u;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    return-void
.end method
