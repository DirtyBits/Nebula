.class public Lsoftware/simplicial/nebulous/application/ai;
.super Lsoftware/simplicial/nebulous/application/n;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lsoftware/simplicial/a/ar;
.implements Lsoftware/simplicial/a/m;


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field e:Landroid/widget/TextView;

.field f:Landroid/widget/ListView;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Button;

.field i:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field private k:Ljava/util/Timer;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lsoftware/simplicial/nebulous/application/ai;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/ai;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/n;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->l:Ljava/util/List;

    .line 48
    const-string v0, "Lobby"

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->j:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/n;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->l:Ljava/util/List;

    .line 54
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ai;->j:Ljava/lang/String;

    .line 55
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ai;)Ljava/util/List;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/ai;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/ai;->l:Ljava/util/List;

    return-object p1
.end method

.method private j()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->k:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->k:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->k:Ljava/util/Timer;

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 173
    if-nez v6, :cond_0

    .line 199
    :goto_0
    return-void

    .line 176
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/ai$2;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/nebulous/application/ai$2;-><init>(Lsoftware/simplicial/nebulous/application/ai;Ljava/util/List;Ljava/util/List;Ljava/util/List;F)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/d/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/n;->onClick(Landroid/view/View;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 131
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->m()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    const v0, 0x7f040047

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 62
    invoke-super {p0, v1}, Lsoftware/simplicial/nebulous/application/n;->a(Landroid/view/View;)V

    .line 64
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/ai;->c:Z

    .line 66
    const v0, 0x7f0d01c9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->e:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0d01cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->f:Landroid/widget/ListView;

    .line 68
    const v0, 0x7f0d013b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    .line 69
    const v0, 0x7f0d01cc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->h:Landroid/widget/Button;

    .line 71
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/n;->onPause()V

    .line 157
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 159
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ai;->j()V

    .line 160
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/n;->onResume()V

    .line 149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->l:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/n;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 80
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 82
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->g:Landroid/widget/Button;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    sget-object v3, Lsoftware/simplicial/a/d/c;->d:Lsoftware/simplicial/a/d/c;

    if-ne v2, v3, :cond_0

    const/16 v0, 0x8

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f040078

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/ai;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/ai;->f:Landroid/widget/ListView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/ai$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/ai$1;-><init>(Lsoftware/simplicial/nebulous/application/ai;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    return-void
.end method
