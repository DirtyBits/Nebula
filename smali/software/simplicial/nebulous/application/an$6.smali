.class Lsoftware/simplicial/nebulous/application/an$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/an;->l()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Lsoftware/simplicial/nebulous/application/an;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/an;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 809
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/an;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 848
    :goto_0
    return-void

    .line 814
    :cond_0
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 815
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 816
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 817
    invoke-static {v0}, Lsoftware/simplicial/a/ba;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 819
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/an;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 820
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v2, 0x7f0801ad

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 844
    :catch_0
    move-exception v0

    .line 846
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 824
    :cond_1
    :try_start_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/an;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 825
    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 826
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v3, 0x7f0800c6

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 827
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v4, 0x7f080074

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v4, 0x7f0800d3

    .line 828
    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    invoke-static {v4}, Lsoftware/simplicial/nebulous/application/an;->c(Lsoftware/simplicial/nebulous/application/an;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v4, 0x7f0801e6

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 827
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 830
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v3, 0x7f0801e0

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/an$6$1;

    invoke-direct {v3, p0, v0}, Lsoftware/simplicial/nebulous/application/an$6$1;-><init>(Lsoftware/simplicial/nebulous/application/an$6;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 841
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/an$6;->b:Lsoftware/simplicial/nebulous/application/an;

    const v2, 0x7f08005d

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/an;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 842
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
