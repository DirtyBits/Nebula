.class public Lsoftware/simplicial/nebulous/application/MainActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lsoftware/simplicial/a/ah;
.implements Lsoftware/simplicial/a/al;
.implements Lsoftware/simplicial/a/at;
.implements Lsoftware/simplicial/a/av;
.implements Lsoftware/simplicial/a/b;
.implements Lsoftware/simplicial/a/b/c;
.implements Lsoftware/simplicial/a/bo;
.implements Lsoftware/simplicial/a/c/d;
.implements Lsoftware/simplicial/a/i/f;
.implements Lsoftware/simplicial/a/j;
.implements Lsoftware/simplicial/a/m;
.implements Lsoftware/simplicial/a/w;
.implements Lsoftware/simplicial/nebulous/b/b;
.implements Lsoftware/simplicial/nebulous/f/ag$b;
.implements Lsoftware/simplicial/nebulous/f/b$a;


# static fields
.field public static final a:Lsoftware/simplicial/a/ah;

.field public static final b:Lsoftware/simplicial/nebulous/e/a;


# instance fields
.field public A:Lsoftware/simplicial/a/bs;

.field public B:Lsoftware/simplicial/a/d/c;

.field public C:I

.field public D:Ljava/lang/CharSequence;

.field public E:Ljava/lang/CharSequence;

.field public F:Lsoftware/simplicial/nebulous/f/ak;

.field public G:Z

.field public H:I

.field public I:Landroid/widget/RelativeLayout;

.field public J:Landroid/widget/RelativeLayout;

.field public K:Landroid/widget/RelativeLayout;

.field public L:Landroid/widget/Spinner;

.field public M:Lsoftware/simplicial/a/am;

.field public N:Lsoftware/simplicial/a/ap;

.field public O:Ljava/lang/Boolean;

.field public P:Ljava/lang/Integer;

.field public Q:Ljava/lang/Integer;

.field public R:I

.field public S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public T:I

.field public U:Lsoftware/simplicial/nebulous/f/p;

.field public V:Lsoftware/simplicial/nebulous/f/a;

.field public W:Z

.field public X:I

.field public Y:Lcom/fyber/a$a;

.field public Z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bp;",
            ">;"
        }
    .end annotation
.end field

.field private aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

.field private aB:Landroid/widget/TextView;

.field private aC:Landroid/widget/ImageButton;

.field private aD:Landroid/widget/ImageButton;

.field private aE:Landroid/widget/ImageButton;

.field private aF:Landroid/widget/ImageButton;

.field private aG:Landroid/widget/ImageButton;

.field private aH:Landroid/widget/ImageButton;

.field private aI:Landroid/widget/ImageButton;

.field private aJ:Landroid/widget/LinearLayout;

.field private aK:Landroid/widget/ImageView;

.field private aL:Landroid/widget/ImageView;

.field private volatile aM:Z

.field private volatile aN:Ljava/util/Timer;

.field private aO:Z

.field private aP:Landroid/widget/Toast;

.field private aQ:Landroid/view/Display;

.field private aR:Z

.field private aS:Z

.field private aT:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private aU:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private aV:Lcom/facebook/f;

.field private aW:Ljava/lang/String;

.field private aX:I

.field private aY:Z

.field private aZ:Z

.field public aa:Ljava/util/Date;

.field ab:Ljava/lang/Runnable;

.field ac:Lsoftware/simplicial/nebulous/views/GameView;

.field public ad:Lsoftware/simplicial/a/s;

.field public ae:J

.field public af:Landroid/widget/GridView;

.field public ag:Landroid/os/Bundle;

.field public ah:Landroid/graphics/Bitmap;

.field public ai:J

.field public aj:I

.field public ak:Ljava/lang/String;

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public an:Z

.field public ao:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public ap:Z

.field public aq:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ar:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final as:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final at:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/nebulous/f/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final au:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lsoftware/simplicial/a/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final av:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Ljava/lang/Object;

.field private ax:I

.field private ay:J

.field private az:Lsoftware/simplicial/nebulous/f/af;

.field private ba:Z

.field private bb:Landroid/content/BroadcastReceiver;

.field private bc:Lsoftware/simplicial/a/bo;

.field private bd:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private be:I

.field private bf:Ljava/lang/String;

.field public final c:Lsoftware/simplicial/nebulous/f/ag;

.field public final d:Lsoftware/simplicial/a/t;

.field public final e:Lsoftware/simplicial/nebulous/f/u;

.field public final f:Lsoftware/simplicial/nebulous/d/d;

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lsoftware/simplicial/a/cc;

.field public final i:Ljava/util/concurrent/atomic/AtomicLong;

.field public final j:Ljava/util/concurrent/atomic/AtomicLong;

.field public final k:Lsoftware/simplicial/nebulous/c/d;

.field public final l:Lsoftware/simplicial/nebulous/f/v;

.field public final m:Lsoftware/simplicial/nebulous/g/a;

.field public n:Lsoftware/simplicial/nebulous/f/e;

.field public final o:Lsoftware/simplicial/nebulous/f/al;

.field public final p:Lsoftware/simplicial/nebulous/f/b;

.field public q:Ljava/lang/String;

.field public r:Lsoftware/simplicial/nebulous/b/d;

.field public s:Lsoftware/simplicial/nebulous/b/a;

.field public t:Ljava/util/concurrent/atomic/AtomicInteger;

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public volatile y:Lsoftware/simplicial/a/ah;

.field public volatile z:Lsoftware/simplicial/nebulous/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$1;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/MainActivity$1;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    .line 198
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$12;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/application/MainActivity$12;-><init>()V

    sput-object v0, Lsoftware/simplicial/nebulous/application/MainActivity;->b:Lsoftware/simplicial/nebulous/e/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 520
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 219
    new-instance v0, Lsoftware/simplicial/nebulous/f/ag;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/f/ag;-><init>(Lsoftware/simplicial/nebulous/f/ag$b;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 220
    new-instance v0, Lsoftware/simplicial/a/t;

    invoke-direct {v0, p0}, Lsoftware/simplicial/a/t;-><init>(Lsoftware/simplicial/a/al;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 221
    new-instance v0, Lsoftware/simplicial/nebulous/f/u;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/f/u;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    .line 222
    new-instance v0, Lsoftware/simplicial/nebulous/d/d;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/d/d;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    .line 223
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->g:Ljava/util/Set;

    .line 224
    new-instance v0, Lsoftware/simplicial/a/cc;

    invoke-direct {v0}, Lsoftware/simplicial/a/cc;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    .line 225
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 226
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    .line 227
    new-instance v0, Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-direct {v0, v1, v2, p0}, Lsoftware/simplicial/nebulous/c/d;-><init>(Lsoftware/simplicial/nebulous/f/ag;Lsoftware/simplicial/a/t;Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    .line 228
    new-instance v0, Lsoftware/simplicial/nebulous/f/v;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/f/v;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    .line 229
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->as:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 230
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->at:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 231
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->au:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 232
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->av:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 233
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Lsoftware/simplicial/nebulous/g/b;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/g/b;-><init>()V

    :goto_0
    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    .line 234
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aw:Ljava/lang/Object;

    .line 236
    new-instance v0, Lsoftware/simplicial/nebulous/f/e;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/f/e;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    .line 237
    new-instance v0, Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/f/al;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/e;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    .line 238
    new-instance v0, Lsoftware/simplicial/nebulous/f/b;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-direct {v0, p0, v1}, Lsoftware/simplicial/nebulous/f/b;-><init>(Lsoftware/simplicial/nebulous/f/b$a;Lsoftware/simplicial/nebulous/f/e;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    .line 240
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    .line 241
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    .line 242
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v7}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 243
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    .line 244
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 245
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->w:Z

    .line 246
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->x:Z

    .line 248
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity;->a:Lsoftware/simplicial/a/ah;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    .line 249
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity;->b:Lsoftware/simplicial/nebulous/e/a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    .line 250
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    .line 251
    sget-object v0, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->B:Lsoftware/simplicial/a/d/c;

    .line 252
    iput v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->C:I

    .line 253
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->D:Ljava/lang/CharSequence;

    .line 254
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->E:Ljava/lang/CharSequence;

    .line 255
    sget-object v0, Lsoftware/simplicial/nebulous/f/ak;->a:Lsoftware/simplicial/nebulous/f/ak;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->F:Lsoftware/simplicial/nebulous/f/ak;

    .line 256
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->G:Z

    .line 257
    iput v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->H:I

    .line 264
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    .line 265
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    .line 266
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    .line 267
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    .line 268
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    .line 269
    const/16 v0, 0xa

    iput v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->R:I

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->S:Ljava/util/List;

    .line 276
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->a:Lsoftware/simplicial/nebulous/f/a;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    .line 277
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z

    .line 278
    iput v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->X:I

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    .line 281
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    .line 282
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$23;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$23;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ab:Ljava/lang/Runnable;

    .line 447
    iput v7, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ax:I

    .line 448
    iput-wide v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ay:J

    .line 449
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 450
    iput-wide v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    .line 451
    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->a:Lsoftware/simplicial/nebulous/f/af;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->az:Lsoftware/simplicial/nebulous/f/af;

    .line 465
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aM:Z

    .line 466
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    .line 467
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    .line 470
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    .line 471
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aS:Z

    .line 473
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aU:Ljava/util/List;

    .line 474
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    .line 496
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aW:Ljava/lang/String;

    .line 497
    iput v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    .line 498
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    .line 499
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aZ:Z

    .line 500
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ba:Z

    .line 501
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    .line 502
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bc:Lsoftware/simplicial/a/bo;

    .line 503
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ag:Landroid/os/Bundle;

    .line 504
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ah:Landroid/graphics/Bitmap;

    .line 505
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bd:Ljava/util/HashSet;

    .line 506
    iput-wide v8, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ai:J

    .line 507
    iput v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aj:I

    .line 508
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ak:Ljava/lang/String;

    .line 509
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->al:Ljava/lang/String;

    .line 510
    iput-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->am:Ljava/lang/String;

    .line 511
    iput-boolean v7, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->an:Z

    .line 512
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ao:Ljava/util/Set;

    .line 513
    iput-boolean v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ap:Z

    .line 514
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aq:Ljava/util/Set;

    .line 515
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    .line 516
    iput v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->be:I

    .line 517
    const-string v0, ""

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bf:Ljava/lang/String;

    .line 521
    invoke-static {v5}, Lsoftware/simplicial/a/e/a;->a(Z)V

    .line 522
    return-void

    .line 233
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/g/c;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/g/c;-><init>()V

    goto/16 :goto_0
.end method

.method static synthetic A(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aL:Landroid/widget/ImageView;

    return-object v0
.end method

.method private B()Z
    .locals 2

    .prologue
    .line 780
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aw:Ljava/lang/Object;

    monitor-enter v1

    .line 782
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 785
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    .line 786
    const/4 v0, 0x1

    monitor-exit v1

    .line 788
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 789
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic B(Lsoftware/simplicial/nebulous/application/MainActivity;)Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    return v0
.end method

.method static synthetic C(Lsoftware/simplicial/nebulous/application/MainActivity;)Lsoftware/simplicial/a/bo;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bc:Lsoftware/simplicial/a/bo;

    return-object v0
.end method

.method private C()V
    .locals 3

    .prologue
    .line 1361
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aw:Ljava/lang/Object;

    monitor-enter v1

    .line 1363
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->B()Z

    .line 1364
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->w()V

    .line 1365
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/GameView;->a()V

    .line 1367
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aS:Z

    if-eqz v0, :cond_0

    .line 1369
    invoke-static {}, Lsoftware/simplicial/nebulous/d/c;->b()Z

    .line 1370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aS:Z

    .line 1373
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1374
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/p;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/Context;Ljava/util/List;)V

    .line 1375
    monitor-exit v1

    .line 1376
    return-void

    .line 1375
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private D()V
    .locals 1

    .prologue
    .line 2078
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v0, :cond_1

    .line 2085
    :cond_0
    :goto_0
    return-void

    .line 2081
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->c()V

    .line 2082
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_2

    .line 2083
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bs;->l()V

    .line 2084
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    goto :goto_0
.end method

.method private E()V
    .locals 1

    .prologue
    .line 2089
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v0, :cond_1

    .line 2096
    :cond_0
    :goto_0
    return-void

    .line 2092
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->d()V

    .line 2093
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_2

    .line 2094
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-virtual {v0}, Lsoftware/simplicial/a/bs;->m()V

    .line 2095
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    goto :goto_0
.end method

.method private F()V
    .locals 2

    .prologue
    .line 2231
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2233
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aK:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2236
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2237
    return-void
.end method

.method private G()V
    .locals 3

    .prologue
    .line 2661
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aZ:Z

    .line 2662
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 2663
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080181

    .line 2664
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801f5

    .line 2665
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801cf

    .line 2666
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$20;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$20;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 2687
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2688
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;I)I
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ax:I

    return p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;J)J
    .locals 1

    .prologue
    .line 186
    iput-wide p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ay:J

    return-wide p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aW:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->av:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/af;)Lsoftware/simplicial/nebulous/f/af;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->az:Lsoftware/simplicial/nebulous/f/af;

    return-object p1
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1041
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aw:Ljava/lang/Object;

    monitor-enter v1

    .line 1043
    :try_start_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->B()Z

    .line 1045
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    .line 1046
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$50;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$50;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1060
    monitor-exit v1

    .line 1061
    return-void

    .line 1060
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aP:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 2101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aP:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 2102
    :cond_0
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aP:Landroid/widget/Toast;

    .line 2103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aP:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2104
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Lsoftware/simplicial/nebulous/f/af;)V
    .locals 1

    .prologue
    .line 2579
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$17;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$17;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/af;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2588
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aS:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/MainActivity;I)I
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->be:I

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bf:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->au:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/af;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/af;)V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    return p1
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->as:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ba:Z

    return p1
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->at:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/application/MainActivity;Z)Z
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aZ:Z

    return p1
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aU:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aT:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aw:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/application/MainActivity;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aN:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->C()V

    return-void
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/application/MainActivity;)I
    .locals 2

    .prologue
    .line 186
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    return v0
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/application/MainActivity;)Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    return v0
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/view/Display;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aQ:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic m(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->E()V

    return-void
.end method

.method static synthetic n(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aK:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic o(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->D()V

    return-void
.end method

.method static synthetic p(Lsoftware/simplicial/nebulous/application/MainActivity;)Z
    .locals 1

    .prologue
    .line 186
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aZ:Z

    return v0
.end method

.method static synthetic q(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->G()V

    return-void
.end method

.method static synthetic r(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic s(Lsoftware/simplicial/nebulous/application/MainActivity;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->F()V

    return-void
.end method

.method static synthetic t(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic u(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic v(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic w(Lsoftware/simplicial/nebulous/application/MainActivity;)I
    .locals 2

    .prologue
    .line 186
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    return v0
.end method

.method static synthetic x(Lsoftware/simplicial/nebulous/application/MainActivity;)I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ax:I

    return v0
.end method

.method static synthetic y(Lsoftware/simplicial/nebulous/application/MainActivity;)J
    .locals 2

    .prologue
    .line 186
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ay:J

    return-wide v0
.end method

.method static synthetic z(Lsoftware/simplicial/nebulous/application/MainActivity;)Lsoftware/simplicial/nebulous/application/GameChatFragment;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 2

    .prologue
    .line 3889
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 1381
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$2;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1389
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1394
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$3;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$3;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1430
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 2947
    if-lez p1, :cond_0

    .line 2949
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ac;->c:Lsoftware/simplicial/nebulous/f/ac;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/ac;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;I)V

    .line 2950
    new-instance v0, Lsoftware/simplicial/nebulous/f/ab;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ac;->c:Lsoftware/simplicial/nebulous/f/ac;

    invoke-direct {v0, v1, p1}, Lsoftware/simplicial/nebulous/f/ab;-><init>(Lsoftware/simplicial/nebulous/f/ac;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/ab;)V

    .line 2952
    :cond_0
    return-void
.end method

.method public a(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2995
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$26;

    invoke-direct {v0, p0, p2, p3}, Lsoftware/simplicial/nebulous/application/MainActivity$26;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;II)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3015
    return-void
.end method

.method public a(IIILsoftware/simplicial/a/h/d;ZLsoftware/simplicial/a/h/f;Z)V
    .locals 0

    .prologue
    .line 2706
    return-void
.end method

.method public a(IILjava/lang/String;[BLsoftware/simplicial/a/q;Lsoftware/simplicial/a/n;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2979
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$25;

    invoke-direct {v0, p0, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$25;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2990
    return-void
.end method

.method public a(IIZZZ)V
    .locals 0

    .prologue
    .line 2712
    return-void
.end method

.method public a(ILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 3542
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$39;

    invoke-direct {v0, p0, p3, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$39;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3555
    return-void
.end method

.method public a(ILjava/lang/String;[BLjava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 3020
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$27;

    invoke-direct {v0, p0, p5}, Lsoftware/simplicial/nebulous/application/MainActivity$27;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3031
    return-void
.end method

.method public a(ILjava/util/List;Ljava/util/List;ZLsoftware/simplicial/a/b/d;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/f/bl;",
            ">;Z",
            "Lsoftware/simplicial/a/b/d;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 2700
    return-void
.end method

.method public a(ILsoftware/simplicial/a/b/b;Lsoftware/simplicial/a/b/e;)V
    .locals 1

    .prologue
    .line 3462
    sget-object v0, Lsoftware/simplicial/a/b/e;->b:Lsoftware/simplicial/a/b/e;

    if-ne p3, v0, :cond_0

    .line 3473
    :goto_0
    return-void

    .line 3465
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$36;

    invoke-direct {v0, p0, p3}, Lsoftware/simplicial/nebulous/application/MainActivity$36;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/b/e;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Landroid/bluetooth/BluetoothAdapter;)V
    .locals 0

    .prologue
    .line 3709
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 3710
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2942
    return-void
.end method

.method public a(Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$l;)V
    .locals 2

    .prologue
    .line 3211
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 3212
    const v1, 0x7f08020d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 3213
    const v1, 0x7f0801f4

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3214
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 3215
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 3217
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$30;

    invoke-direct {v1, p0, v0, p3}, Lsoftware/simplicial/nebulous/application/MainActivity$30;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Landroid/app/ProgressDialog;Lsoftware/simplicial/nebulous/f/al$l;)V

    .line 3346
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 3348
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p1, p2, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;ILsoftware/simplicial/nebulous/f/al$l;)V

    .line 3349
    return-void
.end method

.method public a(Ljava/lang/String;Lsoftware/simplicial/a/c/f;Ljava/lang/String;Lsoftware/simplicial/a/c/f;Lsoftware/simplicial/a/c/a;Z)V
    .locals 0

    .prologue
    .line 2694
    return-void
.end method

.method public a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/d;)V
    .locals 2

    .prologue
    .line 3477
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$37;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$37;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/d;)V

    invoke-virtual {v0, p1, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/d;)V

    .line 3516
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2900
    if-eqz p2, :cond_0

    .line 2902
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$24;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$24;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2911
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 2830
    if-eqz p2, :cond_0

    .line 2832
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$22;

    invoke-direct {v0, p0, p1, p3}, Lsoftware/simplicial/nebulous/application/MainActivity$22;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2895
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 2931
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 2

    .prologue
    .line 2917
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/f/e;->a(Ljava/lang/String;)V

    .line 2918
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    .line 2919
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2936
    return-void
.end method

.method public a(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/c/i;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 3037
    return-void
.end method

.method public a(Ljava/util/List;ILsoftware/simplicial/a/b/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/g;",
            ">;I",
            "Lsoftware/simplicial/a/b/b;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 3457
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/Date;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bp;",
            ">;",
            "Ljava/util/Date;",
            ")V"
        }
    .end annotation

    .prologue
    .line 3752
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$44;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$44;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/util/List;Ljava/util/Date;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3764
    return-void
.end method

.method public a(Ljava/util/List;Lsoftware/simplicial/a/i/e;IILjava/util/List;ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/i/b;",
            ">;",
            "Lsoftware/simplicial/a/i/e;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3865
    return-void
.end method

.method public a(Lsoftware/simplicial/a/aa;)V
    .locals 1

    .prologue
    .line 3193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->au:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 3194
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3195
    return-void
.end method

.method public a(Lsoftware/simplicial/a/aa;Lsoftware/simplicial/a/bf;Z)V
    .locals 0

    .prologue
    .line 2621
    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/aa;)V

    .line 2622
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;)V
    .locals 1

    .prologue
    .line 3055
    check-cast p1, Lsoftware/simplicial/a/u;

    .line 3056
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$28;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$28;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/u;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3072
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;II)V
    .locals 1

    .prologue
    .line 2363
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$11;

    invoke-direct {v0, p0, p3, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$11;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;ILsoftware/simplicial/a/ai;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2379
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 2357
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2358
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ai;Ljava/util/Collection;Lsoftware/simplicial/a/bf;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/ai;",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/d;",
            ">;",
            "Lsoftware/simplicial/a/bf;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2593
    instance-of v0, p1, Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_0

    .line 2616
    :goto_0
    return-void

    .line 2596
    :cond_0
    iget-object v0, p1, Lsoftware/simplicial/a/ai;->aU:Lsoftware/simplicial/a/am;

    .line 2598
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$18;

    invoke-direct {v1, p0, p2, v0}, Lsoftware/simplicial/nebulous/application/MainActivity$18;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/util/Collection;Lsoftware/simplicial/a/am;)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/ai;Lsoftware/simplicial/a/a/y;FFFFFF)V
    .locals 8

    .prologue
    .line 2509
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->r:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_2

    move-object v0, p2

    .line 2511
    check-cast v0, Lsoftware/simplicial/a/a/ak;

    .line 2512
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$16;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity$16;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/a/ak;)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2573
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    if-nez v0, :cond_1

    .line 2574
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lsoftware/simplicial/nebulous/g/a;->a(Lsoftware/simplicial/a/a/y;FFFFFF)V

    .line 2575
    :cond_1
    return-void

    .line 2522
    :cond_2
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->y:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_3

    move-object v0, p2

    .line 2524
    check-cast v0, Lsoftware/simplicial/a/a/aa;

    .line 2525
    iget-short v0, v0, Lsoftware/simplicial/a/a/aa;->a:S

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c(I)V

    goto :goto_0

    .line 2527
    :cond_3
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->z:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_4

    move-object v0, p2

    .line 2529
    check-cast v0, Lsoftware/simplicial/a/a/b;

    .line 2530
    iget-byte v0, v0, Lsoftware/simplicial/a/a/b;->a:B

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2532
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ac;->b:Lsoftware/simplicial/nebulous/f/ac;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/ac;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->H:I

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;I)V

    .line 2533
    new-instance v0, Lsoftware/simplicial/nebulous/f/ab;

    sget-object v1, Lsoftware/simplicial/nebulous/f/ac;->b:Lsoftware/simplicial/nebulous/f/ac;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->H:I

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/ab;-><init>(Lsoftware/simplicial/nebulous/f/ac;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/ab;)V

    goto :goto_0

    .line 2536
    :cond_4
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->I:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_7

    move-object v0, p2

    .line 2538
    check-cast v0, Lsoftware/simplicial/a/a/k;

    .line 2539
    iget-short v0, v0, Lsoftware/simplicial/a/a/k;->a:S

    .line 2540
    check-cast p1, Lsoftware/simplicial/a/u;

    .line 2541
    iget-object v2, p1, Lsoftware/simplicial/a/u;->ae:[Lsoftware/simplicial/a/bf;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 2543
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ar:Ljava/util/Set;

    iget-object v4, v4, Lsoftware/simplicial/a/bf;->H:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2545
    mul-int/lit8 v0, v0, 0x2

    .line 2549
    :cond_5
    new-instance v1, Lsoftware/simplicial/nebulous/f/ab;

    sget-object v2, Lsoftware/simplicial/nebulous/f/ac;->d:Lsoftware/simplicial/nebulous/f/ac;

    invoke-direct {v1, v2, v0}, Lsoftware/simplicial/nebulous/f/ab;-><init>(Lsoftware/simplicial/nebulous/f/ac;I)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/ab;)V

    goto :goto_0

    .line 2541
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2551
    :cond_7
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->J:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_8

    move-object v0, p2

    .line 2553
    check-cast v0, Lsoftware/simplicial/a/a/ab;

    .line 2554
    new-instance v1, Lsoftware/simplicial/nebulous/f/ab;

    sget-object v2, Lsoftware/simplicial/nebulous/f/ac;->e:Lsoftware/simplicial/nebulous/f/ac;

    iget v3, v0, Lsoftware/simplicial/a/a/ab;->a:I

    invoke-direct {v1, v2, v3}, Lsoftware/simplicial/nebulous/f/ab;-><init>(Lsoftware/simplicial/nebulous/f/ac;I)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/ab;)V

    .line 2555
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    sget-object v2, Lsoftware/simplicial/nebulous/f/ac;->e:Lsoftware/simplicial/nebulous/f/ac;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/ac;->toString()Ljava/lang/String;

    move-result-object v2

    iget v0, v0, Lsoftware/simplicial/a/a/ab;->a:I

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/f/e;->b(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2557
    :cond_8
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->s:Lsoftware/simplicial/a/a/y$a;

    if-eq v0, v1, :cond_9

    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->t:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_a

    .line 2559
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->z:Lsoftware/simplicial/nebulous/e/a;

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/e/a;->o_()V

    goto/16 :goto_0

    .line 2561
    :cond_a
    iget-object v0, p2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->E:Lsoftware/simplicial/a/a/y$a;

    if-ne v0, v1, :cond_0

    instance-of v0, p1, Lsoftware/simplicial/a/u;

    if-eqz v0, :cond_0

    .line 2563
    check-cast p1, Lsoftware/simplicial/a/u;

    .line 2564
    iget-boolean v0, p1, Lsoftware/simplicial/a/u;->r:Z

    if-nez v0, :cond_b

    iget-boolean v0, p1, Lsoftware/simplicial/a/u;->q:Z

    if-eqz v0, :cond_0

    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-boolean v0, v0, Lsoftware/simplicial/a/t;->t:Z

    if-nez v0, :cond_0

    .line 2566
    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->c:Lsoftware/simplicial/nebulous/f/af;

    .line 2567
    iget-boolean v1, p1, Lsoftware/simplicial/a/u;->s:Z

    if-nez v1, :cond_c

    .line 2568
    iget-boolean v0, p1, Lsoftware/simplicial/a/u;->q:Z

    if-eqz v0, :cond_d

    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->a:Lsoftware/simplicial/nebulous/f/af;

    .line 2569
    :cond_c
    :goto_2
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/af;)V

    goto/16 :goto_0

    .line 2568
    :cond_d
    sget-object v0, Lsoftware/simplicial/nebulous/f/af;->b:Lsoftware/simplicial/nebulous/f/af;

    goto :goto_2
.end method

.method public a(Lsoftware/simplicial/a/au;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 3560
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$40;

    invoke-direct {v0, p0, p1, p3, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$40;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/au;ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3608
    return-void
.end method

.method public a(Lsoftware/simplicial/a/ay;)V
    .locals 2

    .prologue
    .line 2462
    new-instance v0, Lsoftware/simplicial/a/ay;

    invoke-direct {v0, p1}, Lsoftware/simplicial/a/ay;-><init>(Lsoftware/simplicial/a/ay;)V

    .line 2464
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$14;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity$14;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/ay;)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2489
    return-void
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2960
    return-void
.end method

.method public a(Lsoftware/simplicial/a/bf;Z)V
    .locals 7

    .prologue
    .line 2421
    new-instance v0, Lsoftware/simplicial/a/ay;

    iget-object v1, p1, Lsoftware/simplicial/a/bf;->aB:Lsoftware/simplicial/a/ay;

    invoke-direct {v0, v1}, Lsoftware/simplicial/a/ay;-><init>(Lsoftware/simplicial/a/ay;)V

    .line 2422
    invoke-virtual {p1}, Lsoftware/simplicial/a/bf;->k()I

    move-result v1

    .line 2424
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-eqz v2, :cond_0

    .line 2426
    new-instance v2, Lsoftware/simplicial/a/f/br;

    const/4 v3, -0x1

    new-instance v4, Lsoftware/simplicial/a/ay;

    invoke-direct {v4, v0}, Lsoftware/simplicial/a/ay;-><init>(Lsoftware/simplicial/a/ay;)V

    const-string v5, ""

    iget-object v6, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    iget-short v6, v6, Lsoftware/simplicial/a/f/bl;->d:S

    invoke-direct {v2, v3, v4, v5, v6}, Lsoftware/simplicial/a/f/br;-><init>(ILsoftware/simplicial/a/ay;Ljava/lang/String;S)V

    .line 2427
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    new-instance v4, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-virtual {v2, v4}, Lsoftware/simplicial/a/f/br;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    iget-object v4, p1, Lsoftware/simplicial/a/bf;->bC:Lsoftware/simplicial/a/f/bl;

    invoke-virtual {v3, v2, v4}, Lsoftware/simplicial/nebulous/b/d;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 2430
    :cond_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->r()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 2456
    :cond_1
    :goto_0
    return-void

    .line 2433
    :cond_2
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$13;

    invoke-direct {v1, p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity$13;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/ay;)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/bo;)V
    .locals 1

    .prologue
    .line 3768
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bc:Lsoftware/simplicial/a/bo;

    .line 3769
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/a/bo;)V

    .line 3770
    return-void
.end method

.method public a(Lsoftware/simplicial/a/c/b;Lsoftware/simplicial/a/c/e;Lsoftware/simplicial/a/c/g;I)V
    .locals 0

    .prologue
    .line 3044
    return-void
.end method

.method public a(Lsoftware/simplicial/a/c/h;IIII)V
    .locals 0

    .prologue
    .line 3050
    return-void
.end method

.method public a(Lsoftware/simplicial/a/d;)V
    .locals 1

    .prologue
    .line 3181
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->as:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 3182
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3183
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/ba;JI)V
    .locals 8

    .prologue
    .line 3077
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$29;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lsoftware/simplicial/nebulous/application/MainActivity$29;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/f/ba;JI)V

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3171
    return-void
.end method

.method public a(Lsoftware/simplicial/a/f/bl;Ljava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V
    .locals 0

    .prologue
    .line 2720
    return-void
.end method

.method public a(Lsoftware/simplicial/a/i/e;IZZ)V
    .locals 1

    .prologue
    .line 3870
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$47;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/application/MainActivity$47;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/i/e;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3884
    return-void
.end method

.method public a(Lsoftware/simplicial/a/k;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 3613
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$41;

    invoke-direct {v0, p0, p2, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$41;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;Lsoftware/simplicial/a/k;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3647
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 1

    .prologue
    .line 2725
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$21;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$21;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2744
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 2298
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->c:[I

    invoke-virtual {p1}, Lsoftware/simplicial/a/t$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2352
    :cond_0
    :goto_0
    return-void

    .line 2301
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 2304
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->c:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 2307
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->d:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 2310
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->O:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 2313
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->r:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->M:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->U:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->X:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ad:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->an:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->s:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->t:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->H:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->R:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->W:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->Y:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->Z:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aa:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ab:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->N:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->y:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->l:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->j:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->F:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->C:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->D:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->am:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ag:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->E:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->G:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->A:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->K:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->J:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->I:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ae:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->af:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->aj:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ak:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->al:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v1, :cond_1

    .line 2330
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 2332
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/t$a;->d:Lsoftware/simplicial/a/t$a;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_0

    .line 2333
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->d()V

    goto/16 :goto_0

    .line 2336
    :pswitch_5
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 2339
    :pswitch_6
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 2342
    :pswitch_7
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->f:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 2343
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->e()V

    goto/16 :goto_0

    .line 2346
    :pswitch_8
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 2349
    :pswitch_9
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->h:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 2298
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/nebulous/f/a;)V
    .locals 3

    .prologue
    .line 1447
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$5;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$5;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Lsoftware/simplicial/nebulous/f/a;)V

    .line 2046
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aX:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2047
    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2050
    :goto_0
    return-void

    .line 2049
    :cond_0
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/nebulous/f/ab;)V
    .locals 1

    .prologue
    .line 3187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->at:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 3188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3189
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 2792
    if-eqz p1, :cond_0

    .line 2794
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/u;->a()V

    .line 2795
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->o()V

    .line 2796
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    invoke-virtual {v0}, Lsoftware/simplicial/a/u;->h()V

    .line 2797
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    .line 2799
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;Lsoftware/simplicial/nebulous/f/d;)V

    .line 2801
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/e;->a(I)V

    .line 2803
    :cond_0
    return-void
.end method

.method public a(ZLsoftware/simplicial/a/az;ZLjava/util/Set;ZIJLjava/util/Set;ZLsoftware/simplicial/a/s;JLjava/util/Map;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lsoftware/simplicial/a/az;",
            "Z",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;ZIJ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Lsoftware/simplicial/a/s;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2385
    new-instance v2, Lsoftware/simplicial/a/f/cx;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/cx;-><init>()V

    .line 2387
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v3}, Lsoftware/simplicial/a/t;->r()I

    move-result v3

    iput v3, v2, Lsoftware/simplicial/a/f/cx;->ar:I

    .line 2389
    iput-boolean p1, v2, Lsoftware/simplicial/a/f/cx;->B:Z

    .line 2390
    iput-object p2, v2, Lsoftware/simplicial/a/f/cx;->C:Lsoftware/simplicial/a/az;

    .line 2392
    iput-boolean p3, v2, Lsoftware/simplicial/a/f/cx;->p:Z

    .line 2393
    iput-object p4, v2, Lsoftware/simplicial/a/f/cx;->q:Ljava/util/Set;

    .line 2394
    iput-object p9, v2, Lsoftware/simplicial/a/f/cx;->r:Ljava/util/Set;

    .line 2395
    move-object/from16 v0, p14

    iput-object v0, v2, Lsoftware/simplicial/a/f/cx;->t:Ljava/util/Map;

    .line 2396
    move-object/from16 v0, p15

    iput-object v0, v2, Lsoftware/simplicial/a/f/cx;->s:Ljava/util/Set;

    .line 2398
    iput-boolean p5, v2, Lsoftware/simplicial/a/f/cx;->j:Z

    .line 2399
    iput p6, v2, Lsoftware/simplicial/a/f/cx;->k:I

    .line 2400
    iput-wide p7, v2, Lsoftware/simplicial/a/f/cx;->l:J

    .line 2402
    iput-boolean p10, v2, Lsoftware/simplicial/a/f/cx;->m:Z

    .line 2403
    move-object/from16 v0, p11

    iput-object v0, v2, Lsoftware/simplicial/a/f/cx;->n:Lsoftware/simplicial/a/s;

    .line 2404
    move-wide/from16 v0, p12

    iput-wide v0, v2, Lsoftware/simplicial/a/f/cx;->o:J

    .line 2406
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/f/cx;->b:Z

    .line 2407
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/f/cx;->z:Z

    .line 2408
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/f/cx;->g:Z

    .line 2409
    const/4 v3, 0x0

    iput-boolean v3, v2, Lsoftware/simplicial/a/f/cx;->x:Z

    .line 2411
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    .line 2412
    if-eqz v3, :cond_1

    .line 2413
    invoke-virtual {v3, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V

    .line 2416
    :cond_0
    :goto_0
    return-void

    .line 2414
    :cond_1
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v3, :cond_0

    .line 2415
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    new-instance v4, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v4}, Lsoftware/simplicial/a/f/bn;-><init>()V

    invoke-virtual {v2, v4}, Lsoftware/simplicial/a/f/cx;->a(Lsoftware/simplicial/a/f/bn;)[B

    move-result-object v2

    invoke-virtual {v3, v2}, Lsoftware/simplicial/nebulous/b/a;->a([B)V

    goto :goto_0
.end method

.method public a(IILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 2965
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v0

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 2966
    const/4 v0, 0x0

    .line 2973
    :goto_0
    return v0

    .line 2968
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0, p1, p3}, Lsoftware/simplicial/nebulous/f/u;->a(ILjava/lang/String;)V

    .line 2970
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->y:Lsoftware/simplicial/a/ah;

    invoke-interface {v0, p1, p2, p3}, Lsoftware/simplicial/a/ah;->a(IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2971
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/f/u;->a(I)V

    .line 2973
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3805
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bd:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3843
    :goto_0
    return v0

    .line 3808
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p0, p1}, Landroid/support/v4/a/a;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3810
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3811
    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 3812
    const v2, 0x7f080141

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3813
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3815
    const v2, 0x7f0801cf

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/MainActivity$46;

    invoke-direct {v3, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$46;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3830
    const v2, 0x7f08005d

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3831
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 3836
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-ne v2, v3, :cond_2

    .line 3837
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->G:Z

    .line 3838
    :cond_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/a;->n:Lsoftware/simplicial/nebulous/f/a;

    if-ne v2, v3, :cond_3

    .line 3839
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    .line 3840
    :cond_3
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v3, Lsoftware/simplicial/nebulous/f/a;->S:Lsoftware/simplicial/nebulous/f/a;

    if-ne v2, v3, :cond_4

    .line 3841
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 3842
    :cond_4
    new-array v2, v1, [Ljava/lang/String;

    aput-object p1, v2, v0

    invoke-static {p0, v2, v0}, Landroid/support/v4/a/a;->a(Landroid/app/Activity;[Ljava/lang/String;I)V

    move v0, v1

    .line 3843
    goto :goto_0
.end method

.method public b(Landroid/bluetooth/BluetoothAdapter;)Lsoftware/simplicial/nebulous/b/d;
    .locals 3

    .prologue
    .line 3721
    new-instance v0, Lsoftware/simplicial/nebulous/b/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-direct {v0, p1, v1, v2}, Lsoftware/simplicial/nebulous/b/d;-><init>(Landroid/bluetooth/BluetoothAdapter;Ljava/util/concurrent/atomic/AtomicInteger;Lsoftware/simplicial/a/bs;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    .line 3722
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    sget-object v1, Lsoftware/simplicial/nebulous/f/aa;->a:Ljava/util/UUID;

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/b/d;->a(Ljava/util/UUID;Landroid/content/Context;)V

    .line 3724
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 3725
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$43;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$43;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    .line 3744
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3746
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2924
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/t;->e(Ljava/lang/String;)V

    .line 2925
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1435
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$4;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/application/MainActivity$4;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;I)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1443
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 774
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aB:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 775
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aB:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 776
    return-void

    .line 774
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lsoftware/simplicial/a/ai;)V
    .locals 4

    .prologue
    .line 2627
    check-cast p1, Lsoftware/simplicial/a/u;

    .line 2629
    iget-boolean v0, p1, Lsoftware/simplicial/a/u;->q:Z

    .line 2630
    iget-boolean v1, p1, Lsoftware/simplicial/a/u;->s:Z

    .line 2631
    iget-boolean v2, p1, Lsoftware/simplicial/a/u;->t:Z

    .line 2633
    iget-boolean v3, p1, Lsoftware/simplicial/a/u;->h:Z

    if-eqz v3, :cond_0

    .line 2635
    new-instance v3, Lsoftware/simplicial/nebulous/application/MainActivity$19;

    invoke-direct {v3, p0, v2, v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity$19;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;ZZZ)V

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2657
    :cond_0
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/16 v2, 0x13

    .line 530
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    if-eqz v0, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 537
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 547
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_2

    or-int/lit16 v0, v0, 0x200

    .line 548
    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_3

    or-int/lit8 v0, v0, 0x2

    .line 549
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x4

    .line 550
    :cond_4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_5

    or-int/lit16 v0, v0, 0x1000

    .line 551
    :cond_5
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 553
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 555
    :catch_0
    move-exception v0

    .line 557
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 3199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->av:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 3200
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ab:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3201
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2765
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/f/al;->c(Ljava/lang/String;)V

    .line 2766
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 745
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 748
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 749
    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ar:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :goto_0
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 754
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    .line 755
    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 756
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 763
    :cond_0
    :goto_1
    return-void

    .line 751
    :cond_1
    new-instance v0, Ljava/util/Locale;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->aq:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 759
    :catch_0
    move-exception v0

    .line 761
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 3372
    return-void
.end method

.method public e()Lcom/facebook/f;
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    if-nez v0, :cond_0

    .line 768
    invoke-static {}, Lcom/facebook/f$a;->a()Lcom/facebook/f;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    .line 769
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    return-object v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$52;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$52;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$ae;)V

    .line 1191
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 1271
    :try_start_0
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1272
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Sensor;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1278
    :goto_0
    return-void

    .line 1274
    :catch_0
    move-exception v0

    .line 1276
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 1284
    :try_start_0
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 1285
    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1291
    :goto_0
    return-void

    .line 1287
    :catch_0
    move-exception v0

    .line 1289
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public i()V
    .locals 9

    .prologue
    const/16 v4, 0x8

    const/16 v8, 0xb

    const/16 v7, 0x9

    const/4 v3, 0x0

    .line 1295
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ap:F

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1297
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1298
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/GameChatFragment;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1299
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/GameChatFragment;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1300
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1301
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/f/ag;->j:Lsoftware/simplicial/nebulous/c/e;

    sget-object v6, Lsoftware/simplicial/nebulous/c/e;->a:Lsoftware/simplicial/nebulous/c/e;

    if-ne v5, v6, :cond_3

    .line 1303
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1304
    invoke-virtual {v0, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1305
    const/high16 v5, 0x41c00000    # 24.0f

    invoke-static {v5, p0}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v3, v5, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1306
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 1308
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->J:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1309
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->K:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1310
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1311
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1312
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1313
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1314
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1316
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1317
    invoke-virtual {v1, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1319
    invoke-virtual {v2, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1320
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1344
    :goto_0
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/GameChatFragment;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1346
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/GameChatFragment;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1348
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->A()Lsoftware/simplicial/a/u;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/a/u;->aU:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    .line 1350
    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/bj;->c:Lsoftware/simplicial/a/bj;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->k:Lsoftware/simplicial/nebulous/f/a;

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    sget-object v2, Lsoftware/simplicial/nebulous/f/a;->i:Lsoftware/simplicial/nebulous/f/a;

    if-ne v0, v2, :cond_4

    :cond_1
    move v0, v3

    .line 1348
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1352
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1353
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->F()V

    .line 1354
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->J:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->W:Z

    if-eqz v0, :cond_5

    move v0, v3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1355
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->K:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->X:Z

    if-eqz v0, :cond_6

    move v0, v3

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->R:Z

    if-eqz v1, :cond_7

    :goto_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1357
    return-void

    .line 1324
    :cond_3
    invoke-virtual {v0, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1325
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1326
    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static {v5, p0}, Lb/a/a/a/b;->a(FLandroid/content/Context;)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v3, v5, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1327
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 1329
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1330
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1331
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1332
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1333
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1334
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->K:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1335
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->J:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1338
    invoke-virtual {v1, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1339
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1341
    invoke-virtual {v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1342
    invoke-virtual {v2, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_0

    :cond_4
    move v0, v4

    .line 1350
    goto/16 :goto_1

    :cond_5
    move v0, v4

    .line 1354
    goto :goto_2

    :cond_6
    move v0, v4

    .line 1355
    goto :goto_3

    :cond_7
    move v3, v4

    .line 1356
    goto :goto_4
.end method

.method public j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2054
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    if-eqz v0, :cond_0

    .line 2056
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-virtual {v0, v2, v2}, Lsoftware/simplicial/a/bs;->a(ZZ)Z

    .line 2057
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    .line 2059
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 2061
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2062
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bb:Landroid/content/BroadcastReceiver;

    .line 2064
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    if-eqz v0, :cond_2

    .line 2066
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/d;->a()V

    .line 2067
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->r:Lsoftware/simplicial/nebulous/b/d;

    .line 2069
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    if-eqz v0, :cond_3

    .line 2071
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/a;->a()V

    .line 2072
    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->s:Lsoftware/simplicial/nebulous/b/a;

    .line 2074
    :cond_3
    return-void
.end method

.method public k()V
    .locals 3

    .prologue
    .line 2211
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2212
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 2213
    const v1, 0x7f08015d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2214
    const v1, 0x7f0801c9

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2216
    const v1, 0x7f080248

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$10;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$10;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2225
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2226
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2227
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 2494
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 2495
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$15;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$15;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2503
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 2760
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->a()V

    .line 2761
    return-void
.end method

.method public n()V
    .locals 2

    .prologue
    .line 3175
    invoke-static {}, Lsoftware/simplicial/nebulous/d/c;->a()Z

    .line 3176
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v0, p0, v1}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/Context;Lsoftware/simplicial/nebulous/d/d;)V

    .line 3177
    return-void
.end method

.method public o()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 3205
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 3206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 3207
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 2282
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2771
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2773
    sget v0, Lsoftware/simplicial/nebulous/f/al;->a:I

    if-ne p1, v0, :cond_2

    .line 2775
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/f/al;->a(Landroid/content/Intent;)V

    .line 2784
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    if-eqz v0, :cond_1

    .line 2785
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aV:Lcom/facebook/f;

    invoke-interface {v0, p1, p2, p3}, Lcom/facebook/f;->a(IILandroid/content/Intent;)Z

    .line 2786
    :cond_1
    return-void

    .line 2777
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/f/v;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2779
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0, p1, p2, p3}, Lsoftware/simplicial/nebulous/f/v;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 813
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 815
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->F()V

    .line 1037
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1034
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 822
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/t$a;->h:Lsoftware/simplicial/a/t$a;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/t$a;->f:Lsoftware/simplicial/a/t$a;

    if-ne v0, v1, :cond_3

    .line 824
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 825
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08002f

    .line 826
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080177

    .line 827
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080318

    .line 828
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$49;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$49;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 836
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 840
    :cond_3
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 844
    :pswitch_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 847
    :pswitch_2
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 850
    :pswitch_3
    sget-object v0, Lsoftware/simplicial/nebulous/application/ar;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 853
    :pswitch_4
    sget-object v0, Lsoftware/simplicial/nebulous/application/ag;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 856
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-nez v0, :cond_0

    .line 857
    sget-object v0, Lsoftware/simplicial/nebulous/application/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 860
    :pswitch_6
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 861
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto/16 :goto_0

    .line 864
    :pswitch_7
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 867
    :pswitch_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->n()V

    goto/16 :goto_0

    .line 870
    :pswitch_9
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 873
    :pswitch_a
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 876
    :pswitch_b
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 879
    :pswitch_c
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 882
    :pswitch_d
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 885
    :pswitch_e
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 888
    :pswitch_f
    sget-object v0, Lsoftware/simplicial/nebulous/application/bf;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 891
    :pswitch_10
    sget-object v0, Lsoftware/simplicial/nebulous/application/b;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 894
    :pswitch_11
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 899
    :pswitch_12
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 902
    :pswitch_13
    sget-object v0, Lsoftware/simplicial/nebulous/application/v;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 905
    :pswitch_14
    sget-object v0, Lsoftware/simplicial/nebulous/application/d;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 908
    :pswitch_15
    sget-object v0, Lsoftware/simplicial/nebulous/application/e;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 911
    :pswitch_16
    sget-object v0, Lsoftware/simplicial/nebulous/application/ac;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 914
    :pswitch_17
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 917
    :pswitch_18
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 920
    :pswitch_19
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 923
    :pswitch_1a
    sget-object v0, Lsoftware/simplicial/nebulous/application/aj;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 926
    :pswitch_1b
    sget-object v0, Lsoftware/simplicial/nebulous/application/bg;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 929
    :pswitch_1c
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 932
    :pswitch_1d
    sget-object v0, Lsoftware/simplicial/nebulous/application/bj;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 935
    :pswitch_1e
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->Z:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 938
    :pswitch_1f
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->H:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 941
    :pswitch_20
    sget-object v0, Lsoftware/simplicial/nebulous/application/aq;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 944
    :pswitch_21
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 947
    :pswitch_22
    sget-object v0, Lsoftware/simplicial/nebulous/application/bd;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 950
    :pswitch_23
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 953
    :pswitch_24
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->Q:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 956
    :pswitch_25
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->ah:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 959
    :pswitch_26
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 962
    :pswitch_27
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 965
    :pswitch_28
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 968
    :pswitch_29
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 971
    :pswitch_2a
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 974
    :pswitch_2b
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 977
    :pswitch_2c
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 980
    :pswitch_2d
    sget-object v0, Lsoftware/simplicial/nebulous/application/ay;->c:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 983
    :pswitch_2e
    sget-object v0, Lsoftware/simplicial/nebulous/application/az;->c:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 986
    :pswitch_2f
    sget-object v0, Lsoftware/simplicial/nebulous/application/ba;->c:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 989
    :pswitch_30
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->M:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 992
    :pswitch_31
    sget-object v0, Lsoftware/simplicial/nebulous/application/w;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 995
    :pswitch_32
    sget-object v0, Lsoftware/simplicial/nebulous/application/f;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 998
    :pswitch_33
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->U:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1001
    :pswitch_34
    sget-object v0, Lsoftware/simplicial/nebulous/application/ak;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1004
    :pswitch_35
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->J:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1007
    :pswitch_36
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->v:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1010
    :pswitch_37
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->r:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1013
    :pswitch_38
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1016
    :pswitch_39
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1019
    :pswitch_3a
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->n:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1022
    :pswitch_3b
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->e:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1025
    :pswitch_3c
    sget-object v0, Lsoftware/simplicial/nebulous/application/c;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1028
    :pswitch_3d
    sget-object v0, Lsoftware/simplicial/nebulous/application/r;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1031
    :pswitch_3e
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->x:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 819
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const v9, 0x7f080022

    const v8, 0x1080027

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 2109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_7

    .line 2111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_6

    .line 2112
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 2119
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aC:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 2121
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->b()V

    .line 2123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->c()V

    .line 2124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/GameChatFragment;->a()V

    .line 2125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aK:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2128
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aD:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 2130
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->Z:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 2132
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 2134
    sget-object v0, Lsoftware/simplicial/nebulous/application/as$b;->e:Lsoftware/simplicial/nebulous/application/as$b;

    sput-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    .line 2135
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->u:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 2137
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 2139
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2140
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080330

    .line 2141
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08033a

    .line 2142
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aW:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f080023

    .line 2143
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->X:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2142
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2144
    invoke-virtual {p0, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$7;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$7;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0800e5

    .line 2153
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$6;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$6;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2162
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2164
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 2166
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2167
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080138

    .line 2168
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08033b

    .line 2169
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bf:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lsoftware/simplicial/a/ba;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f080023

    .line 2170
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->be:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2169
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2171
    invoke-virtual {p0, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$9;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$9;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0800e5

    .line 2180
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$8;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$8;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2189
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2191
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_5

    .line 2193
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->b:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->az:Lsoftware/simplicial/nebulous/f/af;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/af;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2205
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2207
    :cond_5
    return-void

    .line 2114
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2117
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2196
    :pswitch_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->G:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_1

    .line 2199
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->z()V

    goto :goto_1

    .line 2202
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    sget-object v1, Lsoftware/simplicial/a/l;->b:Lsoftware/simplicial/a/l;

    iget v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->X:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/l;IZ)V

    goto :goto_1

    .line 2193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 2287
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2289
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    if-eqz v0, :cond_0

    .line 2290
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/views/GameView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/views/GameView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/c/d;->a(FF)V

    .line 2292
    :cond_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->i()V

    .line 2293
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 592
    new-instance v0, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->permitNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 593
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 595
    invoke-static {}, Lcom/facebook/p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    invoke-static {v4}, Lcom/facebook/p;->a(Z)V

    .line 598
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/e;->a(Landroid/content/Context;)V

    .line 600
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/SharedPreferences;Landroid/app/Activity;)V

    .line 602
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->d()V

    .line 604
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {p0, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    .line 606
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bd:Ljava/util/HashSet;

    .line 607
    new-instance v0, Lsoftware/simplicial/nebulous/f/p;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    invoke-virtual {v3, p0}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/nebulous/f/p;-><init>(Lsoftware/simplicial/nebulous/f/al;Landroid/content/res/Resources;Ljava/util/List;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->U:Lsoftware/simplicial/nebulous/f/p;

    .line 608
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Landroid/content/Context;)V

    .line 609
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/c/d;->a(Landroid/content/Context;)V

    .line 610
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/g/a;->a(Landroid/content/Context;)V

    .line 611
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/v;->a(Landroid/content/Context;)V

    .line 612
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/b;->a(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    .line 614
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 615
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aQ:Landroid/view/Display;

    .line 618
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 620
    const v0, 0x7f04001b

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->setContentView(I)V

    .line 623
    const v0, 0x7f0d0073

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/GameView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    .line 624
    const v0, 0x7f0d0085

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aB:Landroid/widget/TextView;

    .line 625
    const v0, 0x7f0d007a

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aC:Landroid/widget/ImageButton;

    .line 626
    const v0, 0x7f0d007f

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    .line 627
    const v0, 0x7f0d0084

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    .line 628
    const v0, 0x7f0d007d

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aD:Landroid/widget/ImageButton;

    .line 629
    const v0, 0x7f0d0081

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    .line 630
    const v0, 0x7f0d0082

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    .line 631
    const v0, 0x7f0d0083

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    .line 632
    const v0, 0x7f0d0080

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    .line 633
    const v0, 0x7f0d0076

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->I:Landroid/widget/RelativeLayout;

    .line 634
    const v0, 0x7f0d0079

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->J:Landroid/widget/RelativeLayout;

    .line 635
    const v0, 0x7f0d007c

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->K:Landroid/widget/RelativeLayout;

    .line 636
    const v0, 0x7f0d0078

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aJ:Landroid/widget/LinearLayout;

    .line 637
    const v0, 0x7f0d007b

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aK:Landroid/widget/ImageView;

    .line 638
    const v0, 0x7f0d007e

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aL:Landroid/widget/ImageView;

    .line 639
    const v0, 0x7f0d0074

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    .line 641
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aC:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 642
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aE:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 643
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aD:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 644
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/views/GameView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 645
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 646
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 647
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 648
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 650
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->af:Landroid/widget/GridView;

    new-instance v1, Lsoftware/simplicial/nebulous/a/r;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/a/r;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 652
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0d0077

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/application/GameChatFragment;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aA:Lsoftware/simplicial/nebulous/application/GameChatFragment;

    .line 654
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f04009e

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aT:Landroid/widget/ArrayAdapter;

    .line 655
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aU:Ljava/util/List;

    .line 656
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aT:Landroid/widget/ArrayAdapter;

    const v1, 0x7f080286

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 657
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aT:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 658
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 659
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$34;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$34;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 689
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->L:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$45;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$45;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 712
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aF:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aG:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 714
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aH:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 715
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aI:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 717
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->y:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/c/d;->a(I)V

    .line 718
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->z:I

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/c/d;->b(I)V

    .line 719
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/views/GameView;->setGameController(Lsoftware/simplicial/nebulous/c/d;)V

    .line 721
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->setVolumeControlStream(I)V

    .line 723
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->F()V

    .line 727
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 728
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->q:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 736
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v0

    .line 737
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 738
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/f/e;->a(I)V

    .line 739
    :cond_1
    return-void

    .line 730
    :catch_0
    move-exception v0

    .line 732
    const-string v1, "1.0.0.0"

    iput-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->q:Ljava/lang/String;

    .line 733
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 795
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 797
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->C()V

    .line 799
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->v()V

    .line 800
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/u;->c()V

    .line 801
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/b;->d()V

    .line 802
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->h()V

    .line 803
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/c/d;->b()V

    .line 804
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->m:Lsoftware/simplicial/nebulous/g/a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/g/a;->b()V

    .line 805
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->b()V

    .line 807
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/d/d;->close()V

    .line 808
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2242
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/c/d;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2243
    const/4 v0, 0x1

    .line 2245
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2260
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, p1, p2}, Lsoftware/simplicial/nebulous/c/d;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261
    const/4 v0, 0x1

    .line 2262
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2268
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, p1, p2}, Lsoftware/simplicial/nebulous/c/d;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2269
    const/4 v0, 0x1

    .line 2270
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const-wide/32 v0, 0xea60

    .line 1196
    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v3, "OnPause"

    invoke-static {v2, v3}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1200
    :try_start_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1201
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1202
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1203
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1204
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->m:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1205
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1206
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->f:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1207
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->g:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1208
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v2, v2, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v2, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 1209
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v2, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 1210
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v3, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;

    if-ne v2, v3, :cond_0

    .line 1211
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->h()V

    .line 1213
    :cond_0
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->w:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    if-eqz v2, :cond_2

    .line 1252
    :cond_1
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/GameView;->onPause()V

    .line 1254
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/b;->f()V

    .line 1256
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1259
    invoke-static {}, Lsoftware/simplicial/a/e/a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1263
    iput-boolean v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    .line 1265
    return-void

    .line 1217
    :cond_2
    :try_start_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v2, v3, :cond_3

    .line 1218
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->D()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1263
    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    throw v0

    .line 1221
    :cond_3
    :try_start_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v2}, Lsoftware/simplicial/a/t;->a()Lsoftware/simplicial/a/f/aa;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v2, v2, Lsoftware/simplicial/nebulous/f/ag;->E:Z

    if-nez v2, :cond_5

    :cond_4
    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->G:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/v;->d()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1224
    :cond_5
    const-wide/16 v2, 0x3a98

    .line 1225
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/v;->d()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1242
    :cond_6
    :goto_1
    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(J)V

    goto :goto_0

    .line 1227
    :cond_7
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v4, v4, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-nez v4, :cond_6

    .line 1229
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/b;->a()Lsoftware/simplicial/nebulous/f/c;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/nebulous/f/c;->c:Lsoftware/simplicial/nebulous/f/c;

    if-ne v4, v5, :cond_8

    .line 1230
    const-wide/32 v0, 0xafc8

    goto :goto_1

    .line 1231
    :cond_8
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/b;->a()Lsoftware/simplicial/nebulous/f/c;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/nebulous/f/c;->b:Lsoftware/simplicial/nebulous/f/c;

    if-ne v4, v5, :cond_9

    .line 1232
    const-wide/32 v0, 0x88b8

    goto :goto_1

    .line 1233
    :cond_9
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aY:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v4}, Lsoftware/simplicial/nebulous/f/b;->a()Lsoftware/simplicial/nebulous/f/c;

    move-result-object v4

    sget-object v5, Lsoftware/simplicial/nebulous/f/c;->a:Lsoftware/simplicial/nebulous/f/c;

    if-ne v4, v5, :cond_a

    .line 1234
    const-wide/16 v0, 0x61a8

    goto :goto_1

    .line 1235
    :cond_a
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ba:Z

    if-eqz v4, :cond_b

    .line 1236
    const-wide/16 v0, 0x7530

    goto :goto_1

    .line 1237
    :cond_b
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->G:Z

    if-nez v4, :cond_6

    .line 1239
    iget-boolean v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z

    if-nez v4, :cond_6

    move-wide v0, v2

    goto :goto_1

    .line 1247
    :cond_c
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    .line 1248
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->C()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3850
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 3852
    array-length v0, p2

    if-lez v0, :cond_0

    aget v0, p3, v1

    if-eqz v0, :cond_0

    .line 3854
    aget-object v0, p2, v1

    invoke-static {p0, v0}, Landroid/support/v4/a/a;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3856
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->bd:Ljava/util/HashSet;

    aget-object v1, p2, v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 3859
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1066
    sget-object v0, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v1, "OnResume"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 1068
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aO:Z

    .line 1070
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1072
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->B()Z

    .line 1073
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ac:Lsoftware/simplicial/nebulous/views/GameView;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/views/GameView;->onResume()V

    .line 1075
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/b;->o:Z

    if-nez v0, :cond_0

    .line 1076
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/b;->e()V

    .line 1078
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/c/d;->c()V

    .line 1080
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1082
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->e()V

    .line 1083
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->a:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->V:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/f/a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 1113
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    if-eqz v0, :cond_1

    .line 1114
    const v0, 0x7f0801dc

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->b(Ljava/lang/String;)V

    .line 1116
    :cond_1
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->F()V

    .line 1117
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->i()V

    .line 1121
    :try_start_0
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 1122
    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$51;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$51;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1136
    :goto_1
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    .line 1138
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->x:Z

    .line 1139
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->G:Z

    .line 1140
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z

    .line 1141
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ba:Z

    .line 1142
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 1143
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    .line 1144
    iput-boolean v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->w:Z

    .line 1147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->c:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1149
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->d:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1150
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->k:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->m:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1152
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->f:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->g:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->n:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 1156
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 1157
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->h:Lsoftware/simplicial/nebulous/c/b;

    sget-object v1, Lsoftware/simplicial/nebulous/c/b;->f:Lsoftware/simplicial/nebulous/c/b;

    if-ne v0, v1, :cond_2

    .line 1158
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->g()V

    .line 1161
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->g()V

    .line 1162
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->f()Lsoftware/simplicial/a/bm;

    move-result-object v0

    .line 1163
    if-eqz v0, :cond_3

    .line 1164
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v2, v0, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    iget-object v3, v0, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    iget-object v4, v0, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    iget v0, v0, Lsoftware/simplicial/a/bm;->g:I

    invoke-virtual {v1, v2, v3, v4, v0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1167
    :cond_3
    const-string v0, "45520"

    invoke-static {v0, p0}, Lcom/fyber/a;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/fyber/a;

    move-result-object v0

    const-string v1, "17da4fd5783821c1a64e4aa2d98cdf97"

    invoke-virtual {v0, v1}, Lcom/fyber/a;->b(Ljava/lang/String;)Lcom/fyber/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a;->b()Lcom/fyber/a$a;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Y:Lcom/fyber/a$a;

    .line 1168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Y:Lcom/fyber/a$a;

    invoke-virtual {v0, v5}, Lcom/fyber/a$a;->a(Z)Lcom/fyber/a$a;

    .line 1169
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Y:Lcom/fyber/a$a;

    invoke-virtual {v0, v5}, Lcom/fyber/a$a;->b(Z)Lcom/fyber/a$a;

    .line 1172
    return-void

    .line 1086
    :sswitch_0
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->ah:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1089
    :sswitch_1
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->B:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1093
    :cond_4
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->W:Z

    if-eqz v0, :cond_5

    .line 1094
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->M:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1095
    :cond_5
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    if-eqz v0, :cond_6

    .line 1096
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->S:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1097
    :cond_6
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->u:Z

    if-eqz v0, :cond_7

    .line 1098
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->n:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1099
    :cond_7
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->w:Z

    if-eqz v0, :cond_8

    .line 1100
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->Q:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1101
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/al;->c:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->x:Z

    if-eqz v0, :cond_a

    .line 1102
    :cond_9
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1103
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->a()Lsoftware/simplicial/a/f/aa;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-eq v0, v1, :cond_b

    .line 1104
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1105
    :cond_b
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aR:Z

    if-eqz v0, :cond_c

    .line 1106
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->g:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 1109
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->f()Lsoftware/simplicial/a/t$a;

    move-result-object v0

    .line 1110
    invoke-virtual {p0, v0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V

    goto/16 :goto_0

    .line 1132
    :catch_0
    move-exception v0

    goto/16 :goto_1

    .line 1083
    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_0
        0x2d -> :sswitch_1
    .end sparse-switch
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    .prologue
    .line 2276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->aQ:Landroid/view/Display;

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lsoftware/simplicial/nebulous/c/d;->a(Landroid/hardware/SensorEvent;I)V

    .line 2277
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 2749
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 2750
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 2755
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 2756
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2251
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->k:Lsoftware/simplicial/nebulous/c/d;

    invoke-virtual {v0, p2}, Lsoftware/simplicial/nebulous/c/d;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252
    const/4 v0, 0x1

    .line 2254
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 564
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 565
    if-eqz p1, :cond_0

    .line 566
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->c()V

    .line 567
    :cond_0
    return-void
.end method

.method public p()V
    .locals 2

    .prologue
    .line 3353
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/MainActivity$31;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$31;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$b;)V

    .line 3366
    return-void
.end method

.method public p_()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 2808
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->e:Lsoftware/simplicial/nebulous/f/u;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/u;->a()V

    .line 2809
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->o()V

    .line 2810
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->p()V

    .line 2814
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    .line 2815
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    .line 2816
    const/4 v0, 0x0

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView;->a:Landroid/graphics/Bitmap;

    .line 2818
    sget-object v0, Lsoftware/simplicial/a/s;->a:Lsoftware/simplicial/a/s;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    .line 2819
    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    .line 2820
    const/4 v0, 0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ax:I

    .line 2821
    iput-wide v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->ay:J

    .line 2822
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->p:Lsoftware/simplicial/nebulous/f/b;

    iput-boolean v1, v0, Lsoftware/simplicial/nebulous/f/b;->o:Z

    .line 2824
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/e;->a()V

    .line 2825
    return-void
.end method

.method public q()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 3376
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3378
    const v1, 0x7f080283

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3379
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 3380
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 3381
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 3383
    const v2, 0x7f0801cf

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/MainActivity$32;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity$32;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3398
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3400
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 3403
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 3404
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 3405
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 3406
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 3407
    return-void
.end method

.method public r()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 3411
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3413
    const v1, 0x7f0802de

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3414
    const v1, 0x7f0800e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$33;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$33;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3424
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3426
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 3429
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 3430
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 3431
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 3432
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 3433
    return-void
.end method

.method public s()V
    .locals 3

    .prologue
    .line 3438
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 3439
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08002f

    .line 3440
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080176

    .line 3441
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->Z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080318

    .line 3442
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/MainActivity$35;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$35;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 3450
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3451
    return-void
.end method

.method public t()V
    .locals 1

    .prologue
    .line 3521
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$38;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$38;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3532
    return-void
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 3536
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v0

    sget-object v1, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()V
    .locals 1

    .prologue
    .line 3652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 3653
    new-instance v0, Lsoftware/simplicial/nebulous/application/MainActivity$42;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/MainActivity$42;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3667
    return-void
.end method

.method public w()V
    .locals 17

    .prologue
    .line 3672
    const/4 v2, 0x1

    new-instance v3, Lsoftware/simplicial/a/az;

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    invoke-direct {v3, v1}, Lsoftware/simplicial/a/az;-><init>(Lsoftware/simplicial/a/az;)V

    const/4 v4, 0x1

    new-instance v5, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->t:Ljava/util/Set;

    invoke-direct {v5, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ax:I

    move-object/from16 v0, p0

    iget-wide v8, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ay:J

    new-instance v10, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->u:Ljava/util/Set;

    invoke-direct {v10, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ad:Lsoftware/simplicial/a/s;

    move-object/from16 v0, p0

    iget-wide v13, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->ae:J

    new-instance v15, Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->w:Ljava/util/Map;

    invoke-direct {v15, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    new-instance v16, Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag$a;->v:Ljava/util/Set;

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v16}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(ZLsoftware/simplicial/a/az;ZLjava/util/Set;ZIJLjava/util/Set;ZLsoftware/simplicial/a/s;JLjava/util/Map;Ljava/util/Set;)V

    .line 3676
    return-void
.end method

.method public x()V
    .locals 2

    .prologue
    .line 3714
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->v:Z

    .line 3715
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3716
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3717
    return-void
.end method

.method public y()Z
    .locals 1

    .prologue
    .line 3774
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->M:Lsoftware/simplicial/a/am;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->N:Lsoftware/simplicial/a/ap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->O:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->P:Ljava/lang/Integer;

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->Q:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z()I
    .locals 1

    .prologue
    .line 3783
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->as:Z

    if-eqz v0, :cond_0

    .line 3784
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->t:I

    .line 3785
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
