.class public Lsoftware/simplicial/nebulous/application/p;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/ListView;

.field c:Landroid/widget/Button;

.field d:Landroid/widget/TextView;

.field e:Lsoftware/simplicial/nebulous/a/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lsoftware/simplicial/nebulous/application/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/p;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->e:Lsoftware/simplicial/nebulous/a/ad;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ad;->clear()V

    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->e:Lsoftware/simplicial/nebulous/a/ad;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/ad;->notifyDataSetChanged()V

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    new-instance v1, Lsoftware/simplicial/nebulous/application/p$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/p$1;-><init>(Lsoftware/simplicial/nebulous/application/p;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$p;)V

    .line 87
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 96
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 33
    const v0, 0x7f040033

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    const v0, 0x7f0d011d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->b:Landroid/widget/ListView;

    .line 36
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->d:Landroid/widget/TextView;

    .line 37
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->c:Landroid/widget/Button;

    .line 39
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 66
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 58
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/p;->a()V

    .line 60
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    new-instance v0, Lsoftware/simplicial/nebulous/a/ad;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/p;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/ad;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->e:Lsoftware/simplicial/nebulous/a/ad;

    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/p;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/p;->e:Lsoftware/simplicial/nebulous/a/ad;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    return-void
.end method
