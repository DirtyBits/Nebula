.class Lsoftware/simplicial/nebulous/application/e$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/e;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/e;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/e;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->j:Lsoftware/simplicial/nebulous/f/i;

    if-ne v0, v1, :cond_3

    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/d;->d(I)Lsoftware/simplicial/a/bd;

    move-result-object v0

    .line 206
    iget-byte v1, v0, Lsoftware/simplicial/a/bd;->c:B

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/e;->c(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/a/bd;->a(BLjava/util/Map;)Z

    move-result v1

    .line 208
    if-nez v1, :cond_1

    .line 209
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const-string v2, ""

    invoke-static {v1, v0, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/bd;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/f/e;->a(Lsoftware/simplicial/a/bd;)V

    .line 213
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_2

    .line 214
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    .line 217
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto :goto_0

    .line 216
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->s:Lsoftware/simplicial/a/bd;

    goto :goto_1

    .line 220
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->h:Lsoftware/simplicial/nebulous/f/i;

    if-ne v0, v1, :cond_6

    .line 222
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/d;->b(I)Lsoftware/simplicial/a/af;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/e;->d(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/af;->a(Lsoftware/simplicial/a/af;Ljava/util/Set;)Z

    move-result v1

    .line 226
    if-nez v1, :cond_4

    .line 227
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const-string v2, ""

    invoke-static {v1, v0, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/af;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 230
    :cond_4
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/f/e;->a(Lsoftware/simplicial/a/af;)V

    .line 231
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_5

    .line 232
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    .line 235
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 234
    :cond_5
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->o:Lsoftware/simplicial/a/af;

    goto :goto_2

    .line 238
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/d;->a:Lsoftware/simplicial/nebulous/f/i;

    sget-object v1, Lsoftware/simplicial/nebulous/f/i;->i:Lsoftware/simplicial/nebulous/f/i;

    if-ne v0, v1, :cond_9

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/d;->c(I)Lsoftware/simplicial/a/as;

    move-result-object v0

    .line 242
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/application/e;->e(Lsoftware/simplicial/nebulous/application/e;)Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/a/as;->a(Lsoftware/simplicial/a/as;Ljava/util/Set;)Z

    move-result v1

    .line 244
    if-nez v1, :cond_7

    .line 245
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const-string v2, ""

    invoke-static {v1, v0, v2}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/as;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    :cond_7
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->n:Lsoftware/simplicial/nebulous/f/e;

    invoke-virtual {v1, v0}, Lsoftware/simplicial/nebulous/f/e;->a(Lsoftware/simplicial/a/as;)V

    .line 249
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v1}, Lsoftware/simplicial/a/t;->t()Lsoftware/simplicial/a/bj;

    move-result-object v1

    sget-object v2, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    if-ne v1, v2, :cond_8

    .line 250
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    .line 253
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 252
    :cond_8
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v0, v1, Lsoftware/simplicial/nebulous/f/ag;->q:Lsoftware/simplicial/a/as;

    goto :goto_3

    .line 258
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/e;->b(Lsoftware/simplicial/nebulous/application/e;)Lsoftware/simplicial/nebulous/a/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lsoftware/simplicial/nebulous/a/d;->a(I)Lsoftware/simplicial/a/e;

    move-result-object v0

    .line 260
    iget-object v1, v0, Lsoftware/simplicial/a/e;->lE:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 262
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/e;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 263
    iget-object v2, v0, Lsoftware/simplicial/a/e;->lE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 264
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const v3, 0x7f0803b3

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 265
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const v3, 0x7f08025d

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/e;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/e$6$1;

    invoke-direct {v3, p0, v0}, Lsoftware/simplicial/nebulous/application/e$6$1;-><init>(Lsoftware/simplicial/nebulous/application/e$6;Lsoftware/simplicial/a/e;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    const v3, 0x7f080318

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/e;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/e$6$2;

    invoke-direct {v3, p0, v0}, Lsoftware/simplicial/nebulous/application/e$6$2;-><init>(Lsoftware/simplicial/nebulous/application/e$6;Lsoftware/simplicial/a/e;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 305
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 309
    :cond_a
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/e$6;->a:Lsoftware/simplicial/nebulous/application/e;

    invoke-static {v1, v0}, Lsoftware/simplicial/nebulous/application/e;->a(Lsoftware/simplicial/nebulous/application/e;Lsoftware/simplicial/a/e;)V

    goto/16 :goto_0
.end method
