.class public Lsoftware/simplicial/nebulous/application/bd;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/RelativeLayout;

.field private m:Landroid/widget/RelativeLayout;

.field private n:Landroid/widget/ListView;

.field private o:Landroid/widget/TextView;

.field private p:Lsoftware/simplicial/nebulous/a/z;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/ImageView;

.field private s:Landroid/widget/ImageView;

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lsoftware/simplicial/nebulous/application/bd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/bd;->a:Ljava/lang/String;

    .line 57
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/bd;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 77
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    .line 78
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->u:Z

    .line 79
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->v:Z

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bd;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    return-object v0
.end method

.method private a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 283
    iget-object v2, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iget-object v3, p1, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    :goto_0
    return-object v0

    .line 286
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, p1

    .line 287
    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bd;Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 136
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->q:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->q:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bd;Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 0

    .prologue
    .line 54
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/bd;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/bd;->u:Z

    return p1
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bd;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/bd;Z)Z
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lsoftware/simplicial/nebulous/application/bd;->v:Z

    return p1
.end method

.method private c()Z
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 249
    const-string v2, "remove_ads"

    iget-object v3, v0, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v0, v0, Lsoftware/simplicial/a/bm;->e:Z

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/application/bd;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    return v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    new-instance v1, Lsoftware/simplicial/nebulous/application/bd$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bd$1;-><init>(Lsoftware/simplicial/nebulous/application/bd;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 278
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    iput-boolean v1, p0, Lsoftware/simplicial/nebulous/application/bd;->u:Z

    .line 294
    new-instance v0, Lsoftware/simplicial/nebulous/application/bd$2;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/application/bd$2;-><init>(Lsoftware/simplicial/nebulous/application/bd;)V

    .line 332
    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 333
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    .line 338
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/al;->f()V

    .line 339
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    .prologue
    .line 537
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 531
    :goto_0
    return-void

    .line 530
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->f()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 482
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-static {v0, p1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 485
    if-nez v0, :cond_0

    .line 504
    :goto_0
    return-void

    .line 488
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/bd$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/bd$5;-><init>(Lsoftware/simplicial/nebulous/application/bd;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;ZLsoftware/simplicial/a/q;ILsoftware/simplicial/a/q;Lsoftware/simplicial/a/q;JIJI)V
    .locals 0

    .prologue
    .line 522
    return-void
.end method

.method public a(Ljava/lang/String;[BLsoftware/simplicial/a/q;I)V
    .locals 0

    .prologue
    .line 510
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 363
    :goto_0
    return-void

    .line 350
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 352
    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v2

    .line 354
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 355
    iget v3, v0, Lsoftware/simplicial/a/bm;->g:I

    iput v3, v2, Lsoftware/simplicial/a/bm;->g:I

    .line 356
    iput-boolean v4, v2, Lsoftware/simplicial/a/bm;->e:Z

    .line 357
    iget-object v0, v0, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    iput-object v0, v2, Lsoftware/simplicial/a/bm;->f:Ljava/util/Date;

    goto :goto_1

    .line 360
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->v:Z

    .line 361
    iput-boolean v4, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    .line 362
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/a/az;Lsoftware/simplicial/a/r;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;IJZLjava/util/Set;ZLjava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsoftware/simplicial/a/az;",
            "Lsoftware/simplicial/a/r;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/e;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJZ",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/af;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Lsoftware/simplicial/a/bd;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lsoftware/simplicial/a/as;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 471
    return-void
.end method

.method public a(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 152
    invoke-static {p0, p1}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bd;Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->clear()V

    .line 155
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->notifyDataSetChanged()V

    .line 157
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    const v1, 0x7f0801c9

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->v:Z

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    const v3, 0x7f08020c

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 174
    iget-boolean v4, v0, Lsoftware/simplicial/a/bm;->e:Z

    if-eqz v4, :cond_1

    iget-object v4, v0, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 175
    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Lsoftware/simplicial/nebulous/f/v;->a(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;)V

    goto :goto_2

    .line 165
    :cond_2
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->u:Z

    if-nez v0, :cond_4

    .line 166
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    const v3, 0x7f08017d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 168
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    const v3, 0x7f08006d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 177
    :cond_5
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->d()V

    .line 178
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 179
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 181
    iget-object v5, v0, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-boolean v5, v0, Lsoftware/simplicial/a/bm;->e:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    iget-object v6, v0, Lsoftware/simplicial/a/bm;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/f/v;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 182
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 185
    :cond_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 187
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/a/z;->addAll(Ljava/util/Collection;)V

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/z;->notifyDataSetChanged()V

    .line 194
    :cond_8
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v4

    .line 196
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->c()Z

    move-result v5

    .line 197
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    if-eqz v0, :cond_9

    if-nez v5, :cond_9

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    move v3, v1

    .line 198
    :goto_4
    if-nez v4, :cond_a

    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/application/bd;->t:Z

    if-eqz v0, :cond_a

    if-nez v5, :cond_a

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->c()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    .line 200
    :goto_5
    if-eqz v5, :cond_b

    .line 202
    new-instance v0, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f080234

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v5, 0x7f080193

    invoke-virtual {p0, v5}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 203
    new-instance v3, Landroid/text/style/RelativeSizeSpan;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-direct {v3, v5}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v0, v3, v5, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 204
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setAlpha(F)V

    .line 205
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 206
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const v3, 0x7f02007a

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 208
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->j:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->k:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 232
    :goto_6
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bd;->d:Landroid/widget/Button;

    if-nez v4, :cond_d

    move v0, v1

    :goto_7
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 234
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->l:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->m:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 239
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 241
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->l:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->m:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    :cond_9
    move v3, v2

    .line 197
    goto/16 :goto_4

    :cond_a
    move v0, v2

    .line 198
    goto/16 :goto_5

    .line 211
    :cond_b
    if-eqz v3, :cond_c

    .line 213
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setAlpha(F)V

    .line 214
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const v3, 0x7f08002d

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const v3, 0x7f020089

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 217
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->k:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_6

    .line 222
    :cond_c
    new-instance v3, Landroid/text/SpannableString;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f080234

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f080193

    invoke-virtual {p0, v6}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 223
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct {v5, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v3}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 224
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setAlpha(F)V

    .line 225
    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v5, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 227
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const v3, 0x7f02007a

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 228
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->j:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->k:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_6

    :cond_d
    move v0, v2

    .line 232
    goto/16 :goto_7
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 461
    :cond_0
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->e()V

    .line 462
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->f()V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 516
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 398
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 400
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->i:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 402
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->E:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0

    .line 404
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 406
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 407
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 409
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 410
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08002f

    .line 411
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08002d

    .line 412
    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080234

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/f/al;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801cf

    .line 413
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bd$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bd$4;-><init>(Lsoftware/simplicial/nebulous/application/bd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    .line 426
    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/bd$3;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/bd$3;-><init>(Lsoftware/simplicial/nebulous/application/bd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 436
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    const-string v1, "remove_ads"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/v;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 438
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 440
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->m:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 444
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 445
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->C:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 446
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 447
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->r:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 448
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->f:Landroid/widget/Button;

    if-ne p1, v0, :cond_8

    .line 449
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->D:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0

    .line 450
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->g:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 451
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v1, Lsoftware/simplicial/nebulous/f/a;->ag:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/bc;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 147
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(I)V

    .line 148
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 86
    const v0, 0x7f04005b

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 88
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f0d0268

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->n:Landroid/widget/ListView;

    .line 91
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0d00e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->c:Landroid/widget/Button;

    .line 93
    const v0, 0x7f0d0269

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->i:Landroid/widget/Button;

    .line 94
    const v0, 0x7f0d025f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->d:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0d0261

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->e:Landroid/widget/Button;

    .line 96
    const v0, 0x7f0d0262

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->f:Landroid/widget/Button;

    .line 97
    const v0, 0x7f0d0263

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->g:Landroid/widget/Button;

    .line 98
    const v0, 0x7f0d0265

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    .line 99
    const v0, 0x7f0d0154

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->q:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0d0264

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->l:Landroid/widget/RelativeLayout;

    .line 101
    const v0, 0x7f0d025e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->m:Landroid/widget/RelativeLayout;

    .line 102
    const v0, 0x7f0d0191

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->r:Landroid/widget/ImageView;

    .line 103
    const v0, 0x7f0d0260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->s:Landroid/widget/ImageView;

    .line 104
    const v0, 0x7f0d0266

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->j:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f0d0267

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->k:Landroid/widget/ImageView;

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 108
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    const v2, 0x7f02007a

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 109
    new-instance v0, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f080234

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f080193

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 110
    new-instance v2, Landroid/text/style/RelativeSizeSpan;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v2, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 111
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 388
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 390
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->b(Lsoftware/simplicial/a/b;)V

    .line 391
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 368
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 370
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->Z:Ljava/util/List;

    sget-object v2, Lsoftware/simplicial/a/bp;->c:Lsoftware/simplicial/a/bp;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->aa:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 371
    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->r:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 372
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd;->s:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 374
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 375
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/application/bd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(I)V

    .line 377
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->h:Lsoftware/simplicial/a/cc;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/a/cc;->a(Lsoftware/simplicial/a/b;)V

    .line 379
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->o:Landroid/widget/TextView;

    const v1, 0x7f0800c7

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/bd;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->f()V

    .line 382
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bd;->e()V

    .line 383
    return-void

    :cond_0
    move v0, v1

    .line 370
    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    new-instance v0, Lsoftware/simplicial/nebulous/a/z;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/z;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd;->n:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/bd;->p:Lsoftware/simplicial/nebulous/a/z;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 131
    sget-object v0, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 132
    return-void
.end method

.method public p_()V
    .locals 0

    .prologue
    .line 477
    return-void
.end method
