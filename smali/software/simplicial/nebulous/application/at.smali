.class public Lsoftware/simplicial/nebulous/application/at;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lsoftware/simplicial/a/w;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/EditText;

.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Spinner;

.field e:Landroid/widget/Spinner;

.field f:Landroid/widget/Button;

.field g:Landroid/widget/Button;

.field h:Landroid/widget/Spinner;

.field i:Landroid/widget/Spinner;

.field j:Landroid/widget/Spinner;

.field k:Landroid/widget/CheckBox;

.field l:Landroid/widget/CheckBox;

.field m:Landroid/widget/GridView;

.field n:Landroid/widget/LinearLayout;

.field o:Landroid/widget/ScrollView;

.field private p:Lsoftware/simplicial/nebulous/a/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lsoftware/simplicial/nebulous/application/at;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/at;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/at;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/at;->b()V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v7, 0x7f04009e

    const/4 v1, 0x2

    .line 239
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 242
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->n:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v0}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;)S

    move-result v0

    const/16 v3, 0x7fff

    if-eq v0, v3, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    invoke-static {v0, v2}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v3

    .line 245
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    if-le v0, v3, :cond_0

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v3, v0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    .line 248
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 249
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v4}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v4

    invoke-static {v2, v4}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v2

    if-le v0, v2, :cond_1

    .line 250
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v2}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/am;)Lsoftware/simplicial/a/ap;

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v0

    .line 251
    :cond_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v2, v2, Lsoftware/simplicial/nebulous/f/ag;->I:I

    if-le v2, v0, :cond_2

    .line 252
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v0, v2, Lsoftware/simplicial/nebulous/f/ag;->I:I

    .line 254
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    .line 255
    :goto_1
    if-gt v2, v3, :cond_4

    .line 256
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 242
    :cond_3
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 258
    :cond_4
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v2, v3, v7, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 259
    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 260
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v3, v3, Lsoftware/simplicial/nebulous/f/ag;->H:I

    add-int/lit8 v3, v3, -0x2

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 262
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 263
    :goto_2
    if-gt v1, v0, :cond_5

    .line 264
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 266
    :cond_5
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 267
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 268
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->I:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 270
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 272
    return-void
.end method

.method private c()Z
    .locals 4

    .prologue
    const v3, 0x7f0801ad

    const/4 v0, 0x0

    .line 302
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lsoftware/simplicial/a/ba;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 305
    invoke-static {v1}, Lsoftware/simplicial/a/ba;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 307
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 308
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 312
    :goto_0
    return v0

    .line 311
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput-object v1, v0, Lsoftware/simplicial/nebulous/f/ag;->K:Ljava/lang/String;

    .line 312
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 399
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 405
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;)V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method public a(Lsoftware/simplicial/a/t$a;Lsoftware/simplicial/a/t$a;)V
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 364
    if-nez v0, :cond_0

    .line 387
    :goto_0
    return-void

    .line 367
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/at$8;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/application/at$8;-><init>(Lsoftware/simplicial/nebulous/application/at;Lsoftware/simplicial/a/t$a;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 411
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 18

    .prologue
    .line 278
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->g:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 280
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    sget-object v2, Lsoftware/simplicial/a/am;->h:Lsoftware/simplicial/a/am;

    if-ne v1, v2, :cond_1

    .line 282
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0802ee

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f080320

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f080120

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801cf

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/at;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    invoke-direct/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/at;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/at;->k:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v4, v4, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v4, v4, Lsoftware/simplicial/nebulous/f/ag;->H:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v5, v5, Lsoftware/simplicial/nebulous/f/ag;->I:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v6, v6, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 291
    invoke-virtual {v7}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v8, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    sget-object v10, Lsoftware/simplicial/a/d/c;->c:Lsoftware/simplicial/a/d/c;

    const-string v11, ""

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    const v13, 0x417a6666    # 15.65f

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v14, v14, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    .line 292
    invoke-static {v14}, Lsoftware/simplicial/a/ai;->c(Lsoftware/simplicial/a/ap;)S

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-short v15, v15, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/at;->p:Lsoftware/simplicial/nebulous/a/w;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/a/w;->a:[Z

    move-object/from16 v17, v0

    .line 290
    invoke-virtual/range {v1 .. v17}, Lsoftware/simplicial/a/t;->a(Ljava/lang/String;ZIILjava/lang/String;[BLsoftware/simplicial/a/am;Lsoftware/simplicial/a/ac;Lsoftware/simplicial/a/d/c;Ljava/lang/String;Lsoftware/simplicial/a/ap;FSSZ[Z)V

    .line 294
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->f:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 296
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 67
    const v0, 0x7f040052

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 69
    const v0, 0x7f0d0118

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    .line 70
    const v0, 0x7f0d012a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    .line 71
    const v0, 0x7f0d0249

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    .line 72
    const v0, 0x7f0d013a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    .line 73
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->f:Landroid/widget/Button;

    .line 74
    const v0, 0x7f0d013b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->g:Landroid/widget/Button;

    .line 75
    const v0, 0x7f0d0119

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    .line 76
    const v0, 0x7f0d0177

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    .line 77
    const v0, 0x7f0d024c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    .line 78
    const v0, 0x7f0d011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->k:Landroid/widget/CheckBox;

    .line 79
    const v0, 0x7f0d024a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->n:Landroid/widget/LinearLayout;

    .line 80
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->l:Landroid/widget/CheckBox;

    .line 81
    const v0, 0x7f0d0248

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->m:Landroid/widget/GridView;

    .line 82
    const v0, 0x7f0d0247

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->o:Landroid/widget/ScrollView;

    .line 84
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 318
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->h:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_2

    .line 320
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    add-int/lit8 v1, p3, 0x2

    if-ne v0, v1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    add-int/lit8 v1, p3, 0x2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->H:I

    .line 323
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/at;->b()V

    .line 325
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->i:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    .line 327
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->I:I

    add-int/lit8 v1, p3, 0x2

    if-eq v0, v1, :cond_0

    .line 329
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    add-int/lit8 v1, p3, 0x2

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->I:I

    .line 330
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/at;->b()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 338
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 354
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 356
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 357
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 343
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 345
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 346
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 348
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->b:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 349
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const v8, 0x7f04009e

    const/4 v3, 0x0

    .line 90
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->K:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 94
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v2, v3

    .line 95
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 97
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 98
    const/4 v1, 0x1

    if-ne v2, v1, :cond_5

    .line 100
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 101
    check-cast v0, Landroid/text/SpannableString;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const/16 v6, 0xff

    const/16 v7, 0xa5

    invoke-static {v6, v7, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v6

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    const/16 v7, 0x12

    invoke-virtual {v0, v5, v3, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 103
    :goto_1
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2, v8, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 106
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$1;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    .line 127
    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 128
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 129
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    invoke-virtual {v1}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 131
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->d:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$2;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 149
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    .line 150
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 151
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 152
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 153
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    invoke-virtual {v1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 154
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->e:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$3;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 173
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    .line 174
    :goto_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 176
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 177
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-short v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 178
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->j:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$4;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->l:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->l:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$5;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$5;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 211
    new-instance v0, Lsoftware/simplicial/nebulous/a/w;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/w;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->p:Lsoftware/simplicial/nebulous/a/w;

    .line 212
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->p:Lsoftware/simplicial/nebulous/a/w;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 213
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->m:Landroid/widget/GridView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/at;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    if-eqz v1, :cond_4

    :goto_5
    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->o:Landroid/widget/ScrollView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$6;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$6;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 226
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/at;->m:Landroid/widget/GridView;

    new-instance v1, Lsoftware/simplicial/nebulous/application/at$7;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/at$7;-><init>(Lsoftware/simplicial/nebulous/application/at;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 234
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/at;->b()V

    .line 235
    return-void

    .line 213
    :cond_4
    const/16 v3, 0x8

    goto :goto_5

    :cond_5
    move-object v1, v0

    goto/16 :goto_1
.end method
