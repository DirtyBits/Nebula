.class Lsoftware/simplicial/nebulous/application/as$20;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/nebulous/f/al$r;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/as;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/as;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/as;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/a/bg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 944
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lsoftware/simplicial/nebulous/application/as;->d:Lsoftware/simplicial/nebulous/application/as$b;

    sget-object v1, Lsoftware/simplicial/nebulous/application/as$b;->d:Lsoftware/simplicial/nebulous/application/as$b;

    if-eq v0, v1, :cond_1

    .line 956
    :cond_0
    :goto_0
    return-void

    .line 947
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->clear()V

    .line 948
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/l;->addAll(Ljava/util/Collection;)V

    .line 949
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->D:Lsoftware/simplicial/nebulous/a/l;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/l;->notifyDataSetChanged()V

    .line 951
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->t:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 953
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 954
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/a/t;->a(Ljava/util/List;)V

    .line 955
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/as;->u:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/as;->b(Lsoftware/simplicial/nebulous/application/as;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/as$20;->a:Lsoftware/simplicial/nebulous/application/as;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/application/as;->c(Lsoftware/simplicial/nebulous/application/as;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
