.class Lsoftware/simplicial/nebulous/application/bd$2;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/bd;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lsoftware/simplicial/a/bm;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/application/bd;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/bd;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 300
    if-nez v0, :cond_0

    .line 301
    const/4 v0, 0x0

    .line 303
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->l:Lsoftware/simplicial/nebulous/f/v;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/f/v;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/bm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/bd;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 329
    :goto_0
    return-void

    .line 312
    :cond_0
    if-eqz p1, :cond_1

    .line 316
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bm;

    .line 318
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bd;Lsoftware/simplicial/a/bm;)Lsoftware/simplicial/a/bm;

    move-result-object v2

    .line 320
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->b:Ljava/lang/String;

    .line 321
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->d:Ljava/lang/String;

    .line 322
    iget-object v3, v0, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    iput-object v3, v2, Lsoftware/simplicial/a/bm;->c:Ljava/lang/String;

    .line 323
    iget v0, v0, Lsoftware/simplicial/a/bm;->g:I

    iput v0, v2, Lsoftware/simplicial/a/bm;->g:I

    goto :goto_1

    .line 327
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bd;Z)Z

    .line 328
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/bd$2;->a:Lsoftware/simplicial/nebulous/application/bd;

    sget-object v1, Lsoftware/simplicial/nebulous/application/bc$a;->a:Lsoftware/simplicial/nebulous/application/bc$a;

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/bd;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 295
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/application/bd$2;->a([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 295
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/application/bd$2;->a(Ljava/util/List;)V

    return-void
.end method
