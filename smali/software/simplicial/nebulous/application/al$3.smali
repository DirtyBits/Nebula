.class Lsoftware/simplicial/nebulous/application/al$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/al;->a(ILandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lsoftware/simplicial/nebulous/application/al;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/al;I)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iput p2, p0, Lsoftware/simplicial/nebulous/application/al$3;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 134
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 136
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f0801ba

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 137
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lsoftware/simplicial/nebulous/application/al$3;->a:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 140
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 142
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801cf

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lsoftware/simplicial/nebulous/application/al$3$1;

    invoke-direct {v3, p0, v1}, Lsoftware/simplicial/nebulous/application/al$3$1;-><init>(Lsoftware/simplicial/nebulous/application/al$3;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 161
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f08005d

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 166
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 167
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/al$3;->b:Lsoftware/simplicial/nebulous/application/al;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/al;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 168
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 169
    return-void
.end method
