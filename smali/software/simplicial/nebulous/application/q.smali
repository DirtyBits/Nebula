.class public Lsoftware/simplicial/nebulous/application/q;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/nebulous/f/al$g;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Button;

.field c:Landroid/widget/ListView;

.field d:Landroid/widget/TextView;

.field private e:Lsoftware/simplicial/nebulous/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lsoftware/simplicial/nebulous/application/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/q;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lsoftware/simplicial/nebulous/f/l;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->d:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->e:Lsoftware/simplicial/nebulous/a/i;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/i;->clear()V

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->e:Lsoftware/simplicial/nebulous/a/i;

    invoke-virtual {v0, p1}, Lsoftware/simplicial/nebulous/a/i;->addAll(Ljava/util/Collection;)V

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->e:Lsoftware/simplicial/nebulous/a/i;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/a/i;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->b:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 62
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 34
    const v0, 0x7f040034

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 36
    const v0, 0x7f0d00b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->b:Landroid/widget/Button;

    .line 37
    const v0, 0x7f0d011e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->c:Landroid/widget/ListView;

    .line 38
    const v0, 0x7f0d0085

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->d:Landroid/widget/TextView;

    .line 40
    return-object v1
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 74
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 68
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    new-instance v0, Lsoftware/simplicial/nebulous/a/i;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/q;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Lsoftware/simplicial/nebulous/a/i;-><init>(Lsoftware/simplicial/nebulous/application/MainActivity;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->e:Lsoftware/simplicial/nebulous/a/i;

    .line 50
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/q;->e:Lsoftware/simplicial/nebulous/a/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 52
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/q;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    invoke-virtual {v0, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Lsoftware/simplicial/nebulous/f/al$g;)V

    .line 53
    return-void
.end method
