.class Lsoftware/simplicial/nebulous/application/MainActivity$22;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2833
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const v5, 0x7f0801cf

    const v4, 0x7f080102

    .line 2837
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    .line 2838
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "No such user found."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2839
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c4

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2887
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2888
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2892
    :goto_1
    return-void

    .line 2840
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "No open public clans exist."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2841
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2842
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You aren\'t invited to that clan."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2843
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f08031d

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2844
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "No such clan exists."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2845
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c3

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2846
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "That clan is full."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2847
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802b0

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2848
    :cond_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Other user is not a member of your clan."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2849
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801d9

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2850
    :cond_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You can\'t be your own friend."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2851
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080339

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2852
    :cond_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "A clan with that name already exists."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2853
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2854
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Cannot apply new XP boost until current boost expires."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2855
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080082

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2856
    :cond_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "This purchase has already been redeemed."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2857
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802b8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2858
    :cond_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You can\'t remove yourself. Leave clan instead."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2859
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080322

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2860
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You aren\'t in a clan."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2861
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801c8

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2862
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Not currently supported."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2863
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802af

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2864
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Name is invalid."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2865
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801ad

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2866
    :cond_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "That player does not meet the minimum level requirement to join this clan:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2867
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "That player does not meet the minimum level requirement to join this clan:"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0801f2

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2868
    :cond_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You don\'t own the team:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2869
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You don\'t own the team:"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080323

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2870
    :cond_11
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "User has already been invited."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2871
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802e1

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2872
    :cond_12
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Maximum players allowed on the team:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2873
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Maximum players allowed on the team:"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080192

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2874
    :cond_13
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Name is already in use."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2875
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801af

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2876
    :cond_14
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "User is not in the team."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2877
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0802e2

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2878
    :cond_15
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You are not invited to team:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2879
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "You are not invited to team:"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08031c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2880
    :cond_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Team does not exist:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2881
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Team does not exist:"

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802ab

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2882
    :cond_17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Please wait a minute before sending another report."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2883
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f0801f6

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2884
    :cond_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->a:Ljava/lang/String;

    const-string v1, "Only leader or diamond can send clan mail."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2885
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v1, 0x7f080386

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2890
    :cond_19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v5}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->b:Ljava/lang/String;

    iget-object v5, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v6, 0x7f080382

    .line 2891
    invoke-virtual {v5, v6}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lsoftware/simplicial/nebulous/application/MainActivity$22;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v7, 0x7f080381

    invoke-virtual {v6, v7}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2890
    invoke-static/range {v0 .. v6}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method
