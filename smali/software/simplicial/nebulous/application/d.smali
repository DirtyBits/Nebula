.class public Lsoftware/simplicial/nebulous/application/d;
.super Lsoftware/simplicial/nebulous/application/bc;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lsoftware/simplicial/a/b/c;
.implements Lsoftware/simplicial/nebulous/f/al$d;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:Lsoftware/simplicial/nebulous/f/a;


# instance fields
.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/CheckBox;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/CheckBox;

.field j:Z

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lsoftware/simplicial/nebulous/application/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/d;->a:Ljava/lang/String;

    .line 36
    sget-object v0, Lsoftware/simplicial/nebulous/f/a;->b:Lsoftware/simplicial/nebulous/f/a;

    sput-object v0, Lsoftware/simplicial/nebulous/application/d;->b:Lsoftware/simplicial/nebulous/f/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/bc;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/application/d;->j:Z

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/d;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    return p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->g:Landroid/widget/TextView;

    const-string v1, "---"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->h:Landroid/widget/TextView;

    const v1, 0x7f08017d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->e:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->o:Lsoftware/simplicial/nebulous/f/al;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->M:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v0, v1, v2, p0}, Lsoftware/simplicial/nebulous/f/al;->a(Ljava/lang/String;Lsoftware/simplicial/a/b/d;Lsoftware/simplicial/nebulous/f/al$d;)V

    .line 139
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/d;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/d;->a()V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/application/d;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    return v0
.end method


# virtual methods
.method public a(ILsoftware/simplicial/a/b/b;Lsoftware/simplicial/a/b/e;)V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 251
    if-nez v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 254
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/d$6;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/d$6;-><init>(Lsoftware/simplicial/nebulous/application/d;Lsoftware/simplicial/a/b/b;)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ljava/util/List;ILsoftware/simplicial/a/b/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/b/g;",
            ">;I",
            "Lsoftware/simplicial/a/b/b;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method public a(Lsoftware/simplicial/a/b/d;I)V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 205
    if-nez v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 208
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/application/d$5;

    invoke-direct {v1, p0, p2}, Lsoftware/simplicial/nebulous/application/d$5;-><init>(Lsoftware/simplicial/nebulous/application/d;I)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/application/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/nebulous/application/bc$a;)V
    .locals 9

    .prologue
    const v8, 0x7f080032

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 227
    invoke-super {p0, p1}, Lsoftware/simplicial/nebulous/application/bc;->a(Lsoftware/simplicial/nebulous/application/bc$a;)V

    .line 229
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->g:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :try_start_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->h:Landroid/widget/TextView;

    const v3, 0x7f080032

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    iget-object v7, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-static {v7}, Lsoftware/simplicial/a/b/a;->a(Lsoftware/simplicial/a/b/d;)I

    move-result v7

    mul-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->e:Landroid/widget/Button;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    iget v3, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 239
    return-void

    .line 234
    :catch_0
    move-exception v2

    .line 236
    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v8}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 238
    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->e:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget v1, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-static {v2}, Lsoftware/simplicial/a/b/a;->a(Lsoftware/simplicial/a/b/d;)I

    move-result v2

    mul-int/2addr v1, v2

    iput v1, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->H:I

    .line 167
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    const/4 v2, 0x1

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v3, v3, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1, v2, v3}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/b/d;ZZ)V

    .line 195
    :cond_0
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 197
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 199
    :cond_1
    return-void

    .line 171
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 172
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 173
    const v1, 0x7f0800c6

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080117

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0800d3

    .line 175
    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lsoftware/simplicial/nebulous/application/d;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0801e6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 177
    const v1, 0x7f0801e0

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/d$4;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/d$4;-><init>(Lsoftware/simplicial/nebulous/application/d;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 191
    const v1, 0x7f08005d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 52
    const v0, 0x7f040029

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 54
    invoke-super {p0, v1, p3}, Lsoftware/simplicial/nebulous/application/bc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f0d00c1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->c:Landroid/widget/Spinner;

    .line 57
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->d:Landroid/widget/Button;

    .line 58
    const v0, 0x7f0d00c3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->e:Landroid/widget/Button;

    .line 59
    const v0, 0x7f0d00c2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    .line 60
    const v0, 0x7f0d00c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->g:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0d00bf

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->h:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0d00bb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->i:Landroid/widget/CheckBox;

    .line 64
    return-object v1
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onPause()V

    .line 158
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 159
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/bc;->onResume()V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 148
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iget-object v0, v0, Lsoftware/simplicial/a/t;->e:Ljava/util/Collection;

    invoke-interface {v0, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/d;->a()V

    .line 151
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 70
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/bc;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 77
    const v1, 0x7f08011d

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    const v1, 0x7f08032f

    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/d;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->c:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v4, 0x7f04009e

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ak:Lsoftware/simplicial/a/b/d;

    invoke-virtual {v1}, Lsoftware/simplicial/a/b/d;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/d$1;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/d$1;-><init>(Lsoftware/simplicial/nebulous/application/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 98
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->f:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/d$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/d$2;-><init>(Lsoftware/simplicial/nebulous/application/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/d;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v1, v1, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/d;->i:Landroid/widget/CheckBox;

    new-instance v1, Lsoftware/simplicial/nebulous/application/d$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/d$3;-><init>(Lsoftware/simplicial/nebulous/application/d;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 130
    return-void
.end method
