.class public Lsoftware/simplicial/nebulous/application/be;
.super Lsoftware/simplicial/nebulous/application/ao;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Landroid/widget/Spinner;

.field c:Landroid/widget/Spinner;

.field d:Landroid/widget/Button;

.field e:Landroid/widget/Button;

.field f:Landroid/widget/Spinner;

.field g:Landroid/widget/Spinner;

.field h:Landroid/widget/LinearLayout;

.field i:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lsoftware/simplicial/nebulous/application/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/application/be;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/ao;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 97
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/d/c;->a(Landroid/content/Context;)J

    move-result-wide v0

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag;->b:Lsoftware/simplicial/nebulous/f/ag$a;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/f/ag$a;->a:Lsoftware/simplicial/a/az;

    iget-wide v2, v2, Lsoftware/simplicial/a/az;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 99
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    .line 100
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0802ee

    .line 101
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f08012e

    .line 102
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f080318

    .line 103
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lsoftware/simplicial/nebulous/application/be$1;

    invoke-direct {v2, p0}, Lsoftware/simplicial/nebulous/application/be$1;-><init>(Lsoftware/simplicial/nebulous/application/be;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0801bd

    .line 113
    invoke-virtual {p0, v1}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 118
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Specified directory or database file does not exist"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v2, 0x7f080118

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0801cf

    invoke-virtual {p0, v3}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/application/be;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/be;->b()V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 207
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    invoke-static {v0, v1}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/ap;)I

    move-result v1

    .line 210
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    if-le v0, v1, :cond_0

    .line 211
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    .line 213
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 214
    const/4 v0, 0x1

    :goto_0
    if-gt v0, v1, :cond_1

    .line 215
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f04009e

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 218
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 219
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v1, v1, Lsoftware/simplicial/nebulous/f/ag;->D:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 221
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 27

    .prologue
    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->d:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 246
    new-instance v4, Lsoftware/simplicial/nebulous/f/ah;

    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4, v2}, Lsoftware/simplicial/nebulous/f/ah;-><init>(Ljava/util/concurrent/atomic/AtomicInteger;)V

    .line 247
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->j()V

    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v8, v2, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    .line 249
    sget-object v2, Lsoftware/simplicial/a/am;->n:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_0

    sget-object v2, Lsoftware/simplicial/a/am;->o:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_0

    sget-object v2, Lsoftware/simplicial/a/am;->p:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_0

    sget-object v2, Lsoftware/simplicial/a/am;->q:Lsoftware/simplicial/a/am;

    if-eq v8, v2, :cond_0

    sget-object v2, Lsoftware/simplicial/a/am;->r:Lsoftware/simplicial/a/am;

    if-ne v8, v2, :cond_1

    .line 250
    :cond_0
    invoke-static {}, Lsoftware/simplicial/a/ai;->n()Lsoftware/simplicial/a/am;

    move-result-object v8

    .line 251
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v26, v0

    new-instance v2, Lsoftware/simplicial/a/bs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v9, 0x7f08024b

    invoke-virtual {v7, v9}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/be;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v7, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [B

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v9, v9, Lsoftware/simplicial/nebulous/f/ag;->D:I

    .line 253
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v13, v13, Lsoftware/simplicial/nebulous/f/ag;->D:I

    sget-object v14, Lsoftware/simplicial/a/aq;->a:Lsoftware/simplicial/a/aq;

    sget-object v15, Lsoftware/simplicial/a/bj;->a:Lsoftware/simplicial/a/bj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-short v0, v0, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-direct/range {v2 .. v25}, Lsoftware/simplicial/a/bs;-><init>(Lsoftware/simplicial/a/al;Lsoftware/simplicial/a/f/bf;ILjava/lang/String;[BLsoftware/simplicial/a/am;IJLsoftware/simplicial/a/ac;ILsoftware/simplicial/a/aq;Lsoftware/simplicial/a/bj;Lsoftware/simplicial/a/ap;SLsoftware/simplicial/a/c/a;Lsoftware/simplicial/a/b/a;Lsoftware/simplicial/a/h/c;Lsoftware/simplicial/a/i/a;Z[ZZZ)V

    move-object/from16 v0, v26

    iput-object v2, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bs;->k()Z

    .line 257
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->A:Lsoftware/simplicial/a/bs;

    iput-object v2, v4, Lsoftware/simplicial/nebulous/f/ah;->a:Lsoftware/simplicial/a/bs;

    .line 258
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    iput-object v2, v4, Lsoftware/simplicial/nebulous/f/ah;->b:Lsoftware/simplicial/a/t;

    .line 259
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->d:Lsoftware/simplicial/a/t;

    move-object/from16 v0, p0

    iget-object v3, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v3, v3, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v5, v5, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v5, v5, Lsoftware/simplicial/nebulous/f/ag;->D:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v8, 0x7f08024b

    .line 260
    invoke-virtual {v7, v8}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    const/4 v8, 0x0

    invoke-virtual/range {p0 .. p0}, Lsoftware/simplicial/nebulous/application/be;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;ZLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v7, v7, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v9, v9, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v9, v9, Lsoftware/simplicial/nebulous/f/ag;->at:Z

    move-object/from16 v0, p0

    iget-object v10, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v10, v10, Lsoftware/simplicial/nebulous/f/ag;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v11, v11, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    .line 261
    invoke-virtual {v11}, Lsoftware/simplicial/nebulous/f/ag;->a()[B

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v12, v12, Lsoftware/simplicial/nebulous/f/ag;->l:Lsoftware/simplicial/a/e;

    move-object/from16 v0, p0

    iget-object v13, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v13, v13, Lsoftware/simplicial/nebulous/f/ag;->n:Lsoftware/simplicial/a/af;

    move-object/from16 v0, p0

    iget-object v14, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    .line 262
    invoke-virtual {v14}, Lsoftware/simplicial/nebulous/application/MainActivity;->z()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v15, v15, Lsoftware/simplicial/nebulous/f/ag;->r:Lsoftware/simplicial/a/bd;

    iget-byte v15, v15, Lsoftware/simplicial/a/bd;->c:B

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v16, v0

    .line 263
    invoke-virtual/range {v16 .. v16}, Lsoftware/simplicial/nebulous/f/ag;->b()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v17, v0

    .line 264
    invoke-virtual/range {v17 .. v17}, Lsoftware/simplicial/nebulous/f/ag;->c()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->p:Lsoftware/simplicial/a/as;

    move-object/from16 v18, v0

    .line 259
    invoke-virtual/range {v2 .. v18}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/am;Lsoftware/simplicial/a/f/bg;ILjava/lang/String;Lsoftware/simplicial/a/ap;IZLjava/lang/String;[BLsoftware/simplicial/a/e;Lsoftware/simplicial/a/af;IBILjava/lang/String;Lsoftware/simplicial/a/as;)V

    .line 266
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->e:Landroid/widget/Button;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_3

    .line 268
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->onBackPressed()V

    .line 270
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->i:Landroid/widget/ImageButton;

    move-object/from16 v0, p1

    if-ne v0, v2, :cond_4

    .line 272
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    .line 274
    if-nez v2, :cond_5

    .line 276
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080102

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0802b3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801cf

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 289
    :cond_4
    :goto_0
    return-void

    .line 280
    :cond_5
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_6

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->x()V

    goto :goto_0

    .line 286
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    sget-object v3, Lsoftware/simplicial/nebulous/f/a;->S:Lsoftware/simplicial/nebulous/f/a;

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/nebulous/f/a;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 56
    const v0, 0x7f04005c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f0d0249

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    .line 59
    const v0, 0x7f0d013a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    .line 60
    const v0, 0x7f0d018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->d:Landroid/widget/Button;

    .line 61
    const v0, 0x7f0d00a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->e:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0d026a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->i:Landroid/widget/ImageButton;

    .line 63
    const v0, 0x7f0d0119

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    .line 64
    const v0, 0x7f0d024c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    .line 65
    const v0, 0x7f0d024a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->h:Landroid/widget/LinearLayout;

    .line 67
    return-object v1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->f:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_0

    .line 228
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget v0, v0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    add-int/lit8 v1, p3, 0x1

    if-ne v0, v1, :cond_1

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    add-int/lit8 v1, p3, 0x1

    iput v1, v0, Lsoftware/simplicial/nebulous/f/ag;->D:I

    .line 231
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/be;->b()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 239
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 127
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onPause()V

    .line 128
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Lsoftware/simplicial/nebulous/application/ao;->onResume()V

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v2, v2, Lsoftware/simplicial/nebulous/application/MainActivity;->f:Lsoftware/simplicial/nebulous/d/d;

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/f/ag;->a(Landroid/content/Context;Lsoftware/simplicial/nebulous/d/d;)V

    .line 77
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->p()V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->d:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0, v1}, Landroid/support/v4/b/b;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 82
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/be;->a()V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    const v2, 0x7f0801b6

    invoke-virtual {p0, v2}, Lsoftware/simplicial/nebulous/application/be;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->d:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const v5, 0x7f04009e

    const/4 v2, 0x0

    .line 133
    invoke-super {p0, p1, p2}, Lsoftware/simplicial/nebulous/application/ao;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 135
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/application/be;->b()V

    .line 141
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 142
    :goto_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 144
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->C:Lsoftware/simplicial/a/ac;

    invoke-virtual {v1}, Lsoftware/simplicial/a/ac;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->b:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/be$2;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/be$2;-><init>(Lsoftware/simplicial/nebulous/application/be;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 161
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 162
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 163
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 164
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v1, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 165
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->v:Lsoftware/simplicial/a/ap;

    invoke-virtual {v1}, Lsoftware/simplicial/a/ap;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 166
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->c:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/be$3;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/be$3;-><init>(Lsoftware/simplicial/nebulous/application/be;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 183
    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->h:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/f/ag;->u:Lsoftware/simplicial/a/am;

    invoke-static {v0}, Lsoftware/simplicial/a/ai;->b(Lsoftware/simplicial/a/am;)S

    move-result v0

    const/16 v3, 0x7fff

    if-eq v0, v3, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 186
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 183
    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    .line 187
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-direct {v2, v3, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 188
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/be;->U:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-short v1, v1, Lsoftware/simplicial/nebulous/f/ag;->ag:S

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 189
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/be;->g:Landroid/widget/Spinner;

    new-instance v1, Lsoftware/simplicial/nebulous/application/be$4;

    invoke-direct {v1, p0}, Lsoftware/simplicial/nebulous/application/be$4;-><init>(Lsoftware/simplicial/nebulous/application/be;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 203
    return-void
.end method
