.class Lsoftware/simplicial/nebulous/application/MainActivity$41;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/application/MainActivity;->a(Lsoftware/simplicial/a/k;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lsoftware/simplicial/a/k;

.field final synthetic c:Lsoftware/simplicial/nebulous/application/MainActivity;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/application/MainActivity;Ljava/lang/String;Lsoftware/simplicial/a/k;)V
    .locals 0

    .prologue
    .line 3614
    iput-object p1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iput-object p3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->b:Lsoftware/simplicial/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const v2, 0x7f080102

    const v4, 0x7f0801cf

    .line 3618
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/f/ag;->au:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3645
    :cond_0
    :goto_0
    return-void

    .line 3620
    :cond_1
    sget-object v0, Lsoftware/simplicial/nebulous/application/MainActivity$48;->f:[I

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->b:Lsoftware/simplicial/a/k;

    invoke-virtual {v1}, Lsoftware/simplicial/a/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 3623
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080088

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3626
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080086

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3629
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/application/MainActivity;->c:Lsoftware/simplicial/nebulous/f/ag;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/f/ag;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3630
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->a:Ljava/lang/String;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f080087

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3632
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/application/MainActivity;->t(Lsoftware/simplicial/nebulous/application/MainActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 3635
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802b6

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3639
    :pswitch_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f0802ec

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3642
    :pswitch_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    const v3, 0x7f08015c

    invoke-virtual {v2, v3}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/application/MainActivity$41;->c:Lsoftware/simplicial/nebulous/application/MainActivity;

    invoke-virtual {v3, v4}, Lsoftware/simplicial/nebulous/application/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 3620
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
