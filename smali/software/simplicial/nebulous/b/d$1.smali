.class Lsoftware/simplicial/nebulous/b/d$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsoftware/simplicial/nebulous/b/d;->a(Landroid/bluetooth/BluetoothSocket;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field a:Lsoftware/simplicial/nebulous/b/e;

.field final synthetic b:Landroid/bluetooth/BluetoothSocket;

.field final synthetic c:Lsoftware/simplicial/nebulous/b/d;


# direct methods
.method constructor <init>(Lsoftware/simplicial/nebulous/b/d;Landroid/bluetooth/BluetoothSocket;)V
    .locals 1

    .prologue
    .line 107
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    iput-object p2, p0, Lsoftware/simplicial/nebulous/b/d$1;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Lsoftware/simplicial/nebulous/b/e;

    invoke-direct {v0}, Lsoftware/simplicial/nebulous/b/e;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d$1;->a:Lsoftware/simplicial/nebulous/b/e;

    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/w;)V
    .locals 7

    .prologue
    .line 146
    iget-object v0, p1, Lsoftware/simplicial/a/f/w;->s:Ljava/lang/Object;

    check-cast v0, Lsoftware/simplicial/a/v;

    .line 147
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->a:I

    .line 148
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->b:I

    .line 149
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, p1, Lsoftware/simplicial/a/f/w;->a:I

    iput v2, v1, Lsoftware/simplicial/a/f/bl;->c:I

    .line 150
    iget-object v1, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget-short v2, p1, Lsoftware/simplicial/a/f/w;->g:S

    iput-short v2, v1, Lsoftware/simplicial/a/f/bl;->d:S

    .line 151
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/b/d;->b(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v2, v2, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/b/d$1;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    new-instance v2, Lsoftware/simplicial/a/f/bn;

    invoke-direct {v2}, Lsoftware/simplicial/a/f/bn;-><init>()V

    iget-object v3, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v3, v3, Lsoftware/simplicial/a/f/bl;->c:I

    iget-object v4, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v4, v4, Lsoftware/simplicial/a/f/bl;->b:I

    iget-object v5, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    iget v5, v5, Lsoftware/simplicial/a/f/bl;->a:I

    const-string v6, ""

    invoke-static {v2, v3, v4, v5, v6}, Lsoftware/simplicial/a/f/y;->a(Lsoftware/simplicial/a/f/bn;IIILjava/lang/String;)[B

    move-result-object v2

    iget-object v0, v0, Lsoftware/simplicial/a/v;->a:Lsoftware/simplicial/a/f/bl;

    invoke-virtual {v1, v2, v0}, Lsoftware/simplicial/nebulous/b/d;->a([BLsoftware/simplicial/a/f/bl;)V

    .line 154
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 113
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BTS RECV "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/b/d$1;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 115
    :cond_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$1;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    :try_start_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/d$1;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/b/d$1;->a:Lsoftware/simplicial/nebulous/b/e;

    invoke-virtual {v1, v2, v3}, Lsoftware/simplicial/nebulous/b/d;->b(Ljava/io/InputStream;Lsoftware/simplicial/nebulous/b/e;)Lsoftware/simplicial/a/f/be;

    move-result-object v2

    .line 121
    if-eqz v2, :cond_0

    .line 124
    iget-object v1, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    if-ne v1, v3, :cond_3

    .line 125
    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/w;

    move-object v1, v0

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/b/d$1;->a(Lsoftware/simplicial/a/f/w;)V

    .line 128
    :cond_1
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    iget-object v1, v1, Lsoftware/simplicial/nebulous/b/d;->a:Lsoftware/simplicial/a/bs;

    invoke-virtual {v1, v2}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    const/4 v1, 0x0

    .line 136
    :goto_1
    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 138
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v2, "GIVING UP AFTER 5 CONSECUTIVE ERRORS"

    invoke-static {v1, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 142
    :cond_2
    return-void

    .line 126
    :cond_3
    :try_start_1
    iget-object v1, v2, Lsoftware/simplicial/a/f/be;->aq:Lsoftware/simplicial/a/f/bh;

    sget-object v3, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    if-ne v1, v3, :cond_1

    .line 127
    iget-object v3, p0, Lsoftware/simplicial/nebulous/b/d$1;->c:Lsoftware/simplicial/nebulous/b/d;

    move-object v0, v2

    check-cast v0, Lsoftware/simplicial/a/f/ac;

    move-object v1, v0

    invoke-static {v3, v1}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/nebulous/b/d;Lsoftware/simplicial/a/f/ac;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v1

    .line 133
    const/4 v1, 0x1

    goto :goto_1
.end method
