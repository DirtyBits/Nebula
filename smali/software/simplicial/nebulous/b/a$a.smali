.class Lsoftware/simplicial/nebulous/b/a$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/b/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/b/a;

.field private b:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/b/a;Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;)V
    .locals 1

    .prologue
    .line 217
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    .line 218
    invoke-virtual {p2, p3}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    .line 219
    return-void
.end method

.method private a(Landroid/bluetooth/BluetoothSocket;)V
    .locals 1

    .prologue
    .line 381
    :try_start_0
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    :goto_0
    return-void

    .line 383
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 225
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 237
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v2, "BTC"

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 241
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 271
    :goto_0
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    .line 272
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v4

    .line 281
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    sget-object v2, Lsoftware/simplicial/a/f/aa;->c:Lsoftware/simplicial/a/f/aa;

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/aa;)Lsoftware/simplicial/a/f/aa;

    .line 282
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/b/a;->b(Lsoftware/simplicial/nebulous/b/a;)Lsoftware/simplicial/nebulous/b/b;

    move-result-object v0

    invoke-interface {v0}, Lsoftware/simplicial/nebulous/b/b;->v()V

    .line 284
    :cond_0
    :goto_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    iget-boolean v0, v0, Lsoftware/simplicial/nebulous/b/a;->b:Z

    if-nez v0, :cond_2

    .line 289
    :try_start_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v2}, Lsoftware/simplicial/nebulous/b/a;->c(Lsoftware/simplicial/nebulous/b/a;)Lsoftware/simplicial/nebulous/b/e;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Lsoftware/simplicial/nebulous/b/a;->a(Ljava/io/InputStream;Lsoftware/simplicial/nebulous/b/e;)[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v5

    .line 300
    :try_start_3
    array-length v6, v5

    .line 302
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 304
    invoke-virtual {v0}, Ljava/io/DataInputStream;->read()I

    move-result v7

    .line 308
    sget-object v0, Lsoftware/simplicial/a/f/bh;->j:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    .line 309
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    .line 310
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    .line 311
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->U:Lsoftware/simplicial/a/f/bh;

    .line 312
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->l:Lsoftware/simplicial/a/f/bh;

    .line 313
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->p:Lsoftware/simplicial/a/f/bh;

    .line 314
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->K:Lsoftware/simplicial/a/f/bh;

    .line 315
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->z:Lsoftware/simplicial/a/f/bh;

    .line 316
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v7, v0, :cond_1

    sget-object v0, Lsoftware/simplicial/a/f/bh;->i:Lsoftware/simplicial/a/f/bh;

    .line 317
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v7, v0, :cond_3

    .line 320
    :cond_1
    const/4 v2, 0x5

    .line 321
    const/4 v0, 0x1

    aget-byte v0, v5, v0

    shl-int/lit8 v0, v0, 0x18

    const/high16 v8, -0x1000000

    and-int/2addr v0, v8

    const/4 v8, 0x2

    aget-byte v8, v5, v8

    shl-int/lit8 v8, v8, 0x10

    const/high16 v9, 0xff0000

    and-int/2addr v8, v9

    add-int/2addr v0, v8

    const/4 v8, 0x3

    aget-byte v8, v5, v8

    shl-int/lit8 v8, v8, 0x8

    const v9, 0xff00

    and-int/2addr v8, v9

    add-int/2addr v0, v8

    const/4 v8, 0x4

    aget-byte v8, v5, v8

    shl-int/lit8 v8, v8, 0x0

    and-int/lit16 v8, v8, 0xff

    add-int/2addr v0, v8

    .line 328
    :goto_2
    sub-int v8, v6, v2

    new-array v8, v8, [B

    .line 329
    const/4 v9, 0x0

    sub-int/2addr v6, v2

    invoke-static {v5, v2, v8, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 331
    new-instance v2, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v2, v7, v0, v8}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    .line 333
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aD:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v7, v0, :cond_4

    .line 335
    new-instance v0, Lsoftware/simplicial/a/f/y;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/y;-><init>()V

    .line 336
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/y;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 337
    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/y;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 370
    :catch_0
    move-exception v0

    .line 372
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 243
    :catch_1
    move-exception v0

    .line 245
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/b/a$a;->a(Landroid/bluetooth/BluetoothSocket;)V

    .line 250
    const-wide/16 v4, 0x1f4

    :try_start_4
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_6

    .line 259
    :goto_3
    :try_start_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 261
    :catch_2
    move-exception v0

    .line 263
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/b/a$a;->a(Landroid/bluetooth/BluetoothSocket;)V

    .line 375
    :cond_2
    :goto_4
    return-void

    .line 274
    :catch_3
    move-exception v0

    .line 276
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 277
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->b:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/b/a$a;->a(Landroid/bluetooth/BluetoothSocket;)V

    goto :goto_4

    .line 291
    :catch_4
    move-exception v0

    .line 293
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 294
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;)V

    goto :goto_4

    :cond_3
    move v0, v1

    move v2, v3

    .line 325
    goto :goto_2

    .line 339
    :cond_4
    :try_start_6
    sget-object v0, Lsoftware/simplicial/a/f/bh;->b:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v7, v0, :cond_5

    .line 341
    new-instance v0, Lsoftware/simplicial/a/f/x;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/x;-><init>()V

    .line 342
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/x;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/x;)V

    goto/16 :goto_1

    .line 345
    :cond_5
    sget-object v0, Lsoftware/simplicial/a/f/bh;->h:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v7, v0, :cond_6

    .line 347
    new-instance v0, Lsoftware/simplicial/a/f/ac;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/ac;-><init>()V

    .line 348
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/ac;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 349
    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/ac;)V

    goto/16 :goto_1

    .line 351
    :cond_6
    sget-object v0, Lsoftware/simplicial/a/f/bh;->Q:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v7, v0, :cond_7

    .line 353
    new-instance v0, Lsoftware/simplicial/a/f/br;

    invoke-direct {v0}, Lsoftware/simplicial/a/f/br;-><init>()V

    .line 354
    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/f/br;->b(Lsoftware/simplicial/a/f/bi;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 355
    iget-object v2, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    invoke-static {v2, v0}, Lsoftware/simplicial/nebulous/b/a;->a(Lsoftware/simplicial/nebulous/b/a;Lsoftware/simplicial/a/f/br;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_1

    .line 361
    :cond_7
    :try_start_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/a$a;->a:Lsoftware/simplicial/nebulous/b/a;

    iget-object v0, v0, Lsoftware/simplicial/nebulous/b/a;->a:Lsoftware/simplicial/a/t;

    invoke-virtual {v0, v2}, Lsoftware/simplicial/a/t;->a(Lsoftware/simplicial/a/f/bi;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_1

    .line 363
    :catch_5
    move-exception v0

    .line 365
    :try_start_8
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error routing message "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto/16 :goto_1

    .line 252
    :catch_6
    move-exception v0

    goto/16 :goto_3
.end method
