.class public Lsoftware/simplicial/nebulous/b/d;
.super Lsoftware/simplicial/nebulous/b/c;
.source "SourceFile"

# interfaces
.implements Lsoftware/simplicial/a/f/bf;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/b/d$a;
    }
.end annotation


# instance fields
.field public final a:Lsoftware/simplicial/a/bs;

.field private final c:Landroid/bluetooth/BluetoothAdapter;

.field private final d:Ljava/util/concurrent/atomic/AtomicInteger;

.field private e:Lsoftware/simplicial/nebulous/b/d$a;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/bluetooth/BluetoothSocket;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothSocket;",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothAdapter;Ljava/util/concurrent/atomic/AtomicInteger;Lsoftware/simplicial/a/bs;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/b/c;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->f:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    .line 52
    iput-object p1, p0, Lsoftware/simplicial/nebulous/b/d;->c:Landroid/bluetooth/BluetoothAdapter;

    .line 53
    iput-object p2, p0, Lsoftware/simplicial/nebulous/b/d;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 54
    iput-object p3, p0, Lsoftware/simplicial/nebulous/b/d;->a:Lsoftware/simplicial/a/bs;

    .line 55
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private a(Landroid/bluetooth/BluetoothSocket;)V
    .locals 2

    .prologue
    .line 106
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lsoftware/simplicial/nebulous/b/d$1;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/b/d$1;-><init>(Lsoftware/simplicial/nebulous/b/d;Landroid/bluetooth/BluetoothSocket;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 156
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 158
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/ac;)V
    .locals 4

    .prologue
    .line 162
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 163
    new-instance v1, Lsoftware/simplicial/nebulous/b/d$2;

    invoke-direct {v1, p0, p1}, Lsoftware/simplicial/nebulous/b/d$2;-><init>(Lsoftware/simplicial/nebulous/b/d;Lsoftware/simplicial/a/f/ac;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 196
    return-void
.end method

.method private a(Lsoftware/simplicial/a/f/bl;)V
    .locals 5

    .prologue
    .line 270
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->a:Lsoftware/simplicial/a/bs;

    new-instance v1, Lsoftware/simplicial/a/f/ac;

    iget v2, p1, Lsoftware/simplicial/a/f/bl;->a:I

    iget v3, p1, Lsoftware/simplicial/a/f/bl;->b:I

    iget v4, p1, Lsoftware/simplicial/a/f/bl;->c:I

    invoke-direct {v1, v2, v3, v4}, Lsoftware/simplicial/a/f/ac;-><init>(III)V

    invoke-virtual {v0, v1}, Lsoftware/simplicial/a/bs;->a(Lsoftware/simplicial/a/f/be;)V

    .line 271
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->f:Ljava/util/Map;

    iget v1, p1, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;

    .line 272
    if-eqz v0, :cond_0

    .line 276
    :try_start_0
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 284
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 287
    const-wide/16 v2, 0xfa

    :try_start_1
    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 294
    :cond_0
    :goto_1
    return-void

    .line 278
    :catch_0
    move-exception v1

    .line 280
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 289
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/d;Landroid/bluetooth/BluetoothSocket;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/d;->a(Landroid/bluetooth/BluetoothSocket;)V

    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/b/d;Lsoftware/simplicial/a/f/ac;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/a/f/ac;)V

    return-void
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/b/d;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/b/d;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->c:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 4

    .prologue
    .line 78
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/b/d;->b:Z

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/d$a;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :try_start_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lsoftware/simplicial/nebulous/b/d$a;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    :try_start_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v0

    .line 99
    :try_start_4
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_1
    monitor-exit p0

    return-void

    .line 86
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized a(Ljava/util/UUID;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->c:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :try_start_1
    new-instance v0, Lsoftware/simplicial/nebulous/b/d$a;

    invoke-direct {v0, p0, p1}, Lsoftware/simplicial/nebulous/b/d$a;-><init>(Lsoftware/simplicial/nebulous/b/d;Ljava/util/UUID;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    .line 66
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->e:Lsoftware/simplicial/nebulous/b/d$a;

    invoke-virtual {v0}, Lsoftware/simplicial/nebulous/b/d$a;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    :goto_0
    monitor-exit p0

    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 70
    :try_start_2
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 71
    const v1, 0x7f080102

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0801cf

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v1, v0, v2}, Lb/a/a/a/a;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a([BLjava/lang/Iterable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/Iterable",
            "<",
            "Lsoftware/simplicial/a/f/bl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 243
    monitor-enter p0

    :try_start_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/f/bl;

    .line 245
    invoke-virtual {p0, p1, v0}, Lsoftware/simplicial/nebulous/b/d;->a([BLsoftware/simplicial/a/f/bl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 247
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a([BLsoftware/simplicial/a/f/bl;)V
    .locals 2

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/b/d;->f:Ljava/util/Map;

    iget v1, p2, Lsoftware/simplicial/a/f/bl;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    if-eqz v0, :cond_0

    .line 257
    :try_start_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lsoftware/simplicial/nebulous/b/d;->a([BLjava/io/OutputStream;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    invoke-direct {p0, p2}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/a/f/bl;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 265
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 262
    :try_start_2
    invoke-direct {p0, p2}, Lsoftware/simplicial/nebulous/b/d;->a(Lsoftware/simplicial/a/f/bl;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/io/InputStream;Lsoftware/simplicial/nebulous/b/e;)Lsoftware/simplicial/a/f/be;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 200
    invoke-virtual {p0, p1, p2}, Lsoftware/simplicial/nebulous/b/d;->a(Ljava/io/InputStream;Lsoftware/simplicial/nebulous/b/e;)[B

    move-result-object v0

    .line 201
    array-length v5, v0

    .line 203
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 205
    invoke-virtual {v1}, Ljava/io/DataInputStream;->read()I

    move-result v1

    .line 206
    const/4 v6, 0x5

    .line 207
    const/4 v2, 0x1

    aget-byte v2, v0, v2

    shl-int/lit8 v2, v2, 0x18

    const/high16 v3, -0x1000000

    and-int/2addr v2, v3

    const/4 v3, 0x2

    aget-byte v3, v0, v3

    shl-int/lit8 v3, v3, 0x10

    const/high16 v7, 0xff0000

    and-int/2addr v3, v7

    add-int/2addr v2, v3

    const/4 v3, 0x3

    aget-byte v3, v0, v3

    shl-int/lit8 v3, v3, 0x8

    const v7, 0xff00

    and-int/2addr v3, v7

    add-int/2addr v2, v3

    const/4 v3, 0x4

    aget-byte v3, v0, v3

    shl-int/lit8 v3, v3, 0x0

    and-int/lit16 v3, v3, 0xff

    add-int/2addr v2, v3

    .line 209
    sub-int v3, v5, v6

    new-array v3, v3, [B

    .line 210
    const/4 v7, 0x0

    sub-int/2addr v5, v6

    invoke-static {v0, v6, v3, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 212
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aS:[Lsoftware/simplicial/a/f/bh;

    aget-object v0, v0, v1

    invoke-static {v0}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bh;)Lsoftware/simplicial/a/f/be;

    move-result-object v6

    .line 213
    if-nez v6, :cond_1

    .line 215
    sget-object v0, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v1, "UNEXPECTED NULL MESSAGE IN BTML.Read()"

    invoke-static {v0, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 237
    :cond_0
    :goto_0
    return-object v4

    .line 220
    :cond_1
    sget-object v0, Lsoftware/simplicial/a/f/bh;->g:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-eq v1, v0, :cond_2

    sget-object v0, Lsoftware/simplicial/a/f/bh;->R:Lsoftware/simplicial/a/f/bh;

    .line 221
    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 223
    :cond_2
    new-instance v0, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/a/f/be;->b(Lsoftware/simplicial/a/f/bi;)Z

    move-result v0

    .line 234
    :goto_1
    if-eqz v0, :cond_0

    move-object v4, v6

    .line 235
    goto :goto_0

    .line 225
    :cond_3
    sget-object v0, Lsoftware/simplicial/a/f/bh;->aC:Lsoftware/simplicial/a/f/bh;

    invoke-virtual {v0}, Lsoftware/simplicial/a/f/bh;->ordinal()I

    move-result v0

    if-ne v1, v0, :cond_4

    .line 227
    new-instance v0, Lsoftware/simplicial/a/f/bj;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lsoftware/simplicial/a/f/bj;-><init>(II[BLjava/net/InetSocketAddress;Ljava/net/DatagramSocket;)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v0

    goto :goto_1

    .line 231
    :cond_4
    new-instance v0, Lsoftware/simplicial/a/f/bi;

    invoke-direct {v0, v1, v2, v3}, Lsoftware/simplicial/a/f/bi;-><init>(II[B)V

    invoke-virtual {v6, v0}, Lsoftware/simplicial/a/f/be;->a(Lsoftware/simplicial/a/f/bi;)Z

    move-result v0

    goto :goto_1
.end method
