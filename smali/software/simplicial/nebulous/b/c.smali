.class public Lsoftware/simplicial/nebulous/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/b/c;->b:Z

    return-void
.end method


# virtual methods
.method protected declared-synchronized a([BLjava/io/OutputStream;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 20
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/b/c;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 36
    :goto_0
    monitor-exit p0

    return v0

    .line 25
    :cond_0
    :try_start_1
    array-length v1, p1

    ushr-int/lit8 v1, v1, 0x0

    and-int/lit16 v1, v1, 0xff

    .line 26
    array-length v2, p1

    ushr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    .line 27
    invoke-virtual {p2, v1}, Ljava/io/OutputStream;->write(I)V

    .line 28
    invoke-virtual {p2, v2}, Ljava/io/OutputStream;->write(I)V

    .line 29
    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {p2, p1, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 30
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 31
    const/4 v0, 0x1

    goto :goto_0

    .line 33
    :catch_0
    move-exception v1

    .line 35
    :try_start_2
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Ljava/io/InputStream;Lsoftware/simplicial/nebulous/b/e;)[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 42
    iput v3, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    .line 45
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 46
    iget v1, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    if-nez v1, :cond_1

    .line 47
    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    iput-short v0, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    .line 58
    :goto_0
    iget v0, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    .line 59
    iget v0, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    add-int/lit8 v0, v0, -0x2

    iget-short v1, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    if-ne v0, v1, :cond_0

    .line 61
    iput v3, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    .line 62
    iget-object v0, p2, Lsoftware/simplicial/nebulous/b/e;->b:[B

    return-object v0

    .line 48
    :cond_1
    iget v1, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 50
    iget-short v1, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    const v2, 0xff00

    shl-int/lit8 v0, v0, 0x8

    and-int/2addr v0, v2

    int-to-short v0, v0

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    .line 51
    iget-short v0, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    if-lez v0, :cond_2

    iget-short v0, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    const/16 v1, 0x2000

    if-le v0, v1, :cond_3

    .line 52
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid message size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_3
    iget-short v0, p2, Lsoftware/simplicial/nebulous/b/e;->a:S

    new-array v0, v0, [B

    iput-object v0, p2, Lsoftware/simplicial/nebulous/b/e;->b:[B

    goto :goto_0

    .line 56
    :cond_4
    iget-object v1, p2, Lsoftware/simplicial/nebulous/b/e;->b:[B

    iget v2, p2, Lsoftware/simplicial/nebulous/b/e;->c:I

    add-int/lit8 v2, v2, -0x2

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    goto :goto_0
.end method
