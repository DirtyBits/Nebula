.class public Lsoftware/simplicial/nebulous/views/a/e;
.super Lsoftware/simplicial/nebulous/views/a/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 57
    const-string v0, "attribute vec2 aPos;attribute vec2 aTex;varying vec2 vTex;uniform float uAspect;uniform vec4 uTranslateScale;uniform vec2 uAngleDepth;uniform vec2 uTexRevolution;uniform float uTexRotation;uniform vec2 uTexZoom;uniform vec2 uTexOffset;void main() {float xScaled = aPos.x * uTranslateScale.z;float yScaled = aPos.y * uTranslateScale.w;float xRot = xScaled * cos(uAngleDepth.x) - yScaled * sin(uAngleDepth.x);float yRot = yScaled * cos(uAngleDepth.x) + xScaled * sin(uAngleDepth.x);gl_Position.x = (xRot + uTranslateScale.x) * uAspect;gl_Position.y = yRot + uTranslateScale.y;gl_Position.z = uAngleDepth.y;gl_Position.w = 1.0;vTex.x = (aTex.x - 0.5 ) * cos(-uTexRotation) - (aTex.y - 0.5 ) * sin(-uTexRotation) + 0.5;vTex.y = (aTex.y - 0.5 ) * cos(-uTexRotation) + (aTex.x - 0.5 ) * sin(-uTexRotation) + 0.5;vTex += uTexOffset;vTex.x += uTexRevolution.x * cos(uTexRevolution.y);vTex.y += uTexRevolution.x * sin(uTexRevolution.y);vTex.x = (vTex.x - 0.5 ) * uTexZoom.x + 0.5;vTex.y = (vTex.y - 0.5 ) * uTexZoom.y + 0.5;}"

    const-string v1, "precision mediump float;uniform vec4 uColor;varying lowp vec2 vTex;uniform sampler2D uTex;void main() {gl_FragColor = texture2D(uTex, vTex) *  uColor;}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->b:I

    const-string v1, "uTexRevolution"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->a:I

    .line 60
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->b:I

    const-string v1, "uTexRotation"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->c:I

    .line 61
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->b:I

    const-string v1, "uTexZoom"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->d:I

    .line 62
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->b:I

    const-string v1, "uTexOffset"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->e:I

    .line 64
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/views/a/e;->c()V

    .line 65
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->c:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 85
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->a:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 70
    return-void
.end method

.method public b(FF)V
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->d:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 75
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 90
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->a:I

    invoke-static {v0, v1, v1}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 91
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->c:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 92
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->d:I

    invoke-static {v0, v2, v2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 93
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->e:I

    invoke-static {v0, v1, v1}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 94
    return-void
.end method

.method public c(FF)V
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/e;->e:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 80
    return-void
.end method
