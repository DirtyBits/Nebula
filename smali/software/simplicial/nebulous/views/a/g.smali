.class public Lsoftware/simplicial/nebulous/views/a/g;
.super Lsoftware/simplicial/nebulous/views/a/d;
.source "SourceFile"


# instance fields
.field a:[I

.field c:Ljava/nio/IntBuffer;

.field private final d:[I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:F

.field private i:F

.field private j:F

.field private k:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    const-string v0, "attribute vec2 aPos;attribute vec4 aColor;uniform vec4 uTranslateScaleAspect;varying vec4 vColor;void main() {float xScaled = aPos.x * uTranslateScaleAspect.z;float yScaled = aPos.y * uTranslateScaleAspect.z;gl_Position.x = (xScaled + uTranslateScaleAspect.x) * uTranslateScaleAspect.w;gl_Position.y = yScaled + uTranslateScaleAspect.y;gl_Position.z = 0.935;gl_Position.w = 1.0;vColor = aColor; }"

    const-string v1, "precision mediump float;varying vec4 vColor;void main() {gl_FragColor = vColor;}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    .line 64
    iput-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->a:[I

    .line 65
    iput-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    .line 70
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->e:I

    .line 71
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->b:I

    const-string v1, "aColor"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->f:I

    .line 73
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->b:I

    const-string v1, "uTranslateScaleAspect"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->g:I

    .line 75
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->b:I

    const-string v1, "uTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 76
    return-void
.end method

.method private a(Lsoftware/simplicial/a/bx;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 232
    if-nez p1, :cond_0

    .line 245
    :goto_0
    return v0

    .line 234
    :cond_0
    iget v1, p1, Lsoftware/simplicial/a/bx;->c:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 237
    :pswitch_0
    const v0, -0xffff80

    goto :goto_0

    .line 239
    :pswitch_1
    const v0, -0xff8000

    goto :goto_0

    .line 241
    :pswitch_2
    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_0

    .line 243
    :pswitch_3
    const v0, -0xff7f80

    goto :goto_0

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const v6, 0x8892

    const/4 v9, 0x4

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 94
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 96
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    aget v0, v0, v3

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 97
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 98
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->e:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    aget v0, v0, v7

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 100
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->f:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 101
    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/g;->f:I

    const/16 v6, 0x1401

    move v5, v9

    move v8, v9

    move v9, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 102
    return-void
.end method

.method public a(FFF)V
    .locals 5

    .prologue
    .line 80
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->g:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/g;->i:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->j:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/g;->k:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/g;->j:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/g;->j:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/g;->h:F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 81
    return-void
.end method

.method public a(FFFF)V
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lsoftware/simplicial/nebulous/views/a/g;->h:F

    .line 86
    iput p2, p0, Lsoftware/simplicial/nebulous/views/a/g;->i:F

    .line 87
    iput p3, p0, Lsoftware/simplicial/nebulous/views/a/g;->k:F

    .line 88
    iput p4, p0, Lsoftware/simplicial/nebulous/views/a/g;->j:F

    .line 89
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x4

    const/4 v1, 0x0

    mul-int/lit8 v2, p1, 0x6

    mul-int/2addr v2, p1

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 118
    return-void
.end method

.method public a(Ljava/util/Collection;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lsoftware/simplicial/a/bz;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const v5, 0x8892

    const/4 v4, 0x0

    .line 203
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    if-nez v0, :cond_1

    .line 208
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->a:[I

    .line 209
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 210
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 211
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    .line 214
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v5, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 215
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/bz;

    .line 217
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->a:[I

    iget-object v3, v0, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    invoke-direct {p0, v3}, Lsoftware/simplicial/nebulous/views/a/g;->a(Lsoftware/simplicial/a/bx;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 218
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    invoke-virtual {v2, v4}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 219
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/a/g;->a:[I

    invoke-virtual {v2, v3}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 220
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    invoke-virtual {v2, v4}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 221
    iget v2, v0, Lsoftware/simplicial/a/bz;->a:I

    mul-int/2addr v2, p2

    iget v0, v0, Lsoftware/simplicial/a/bz;->b:I

    add-int/2addr v0, v2

    .line 222
    mul-int/lit8 v0, v0, 0x18

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    .line 224
    invoke-virtual {v2}, Ljava/nio/IntBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/a/g;->c:Ljava/nio/IntBuffer;

    .line 222
    invoke-static {v5, v0, v2, v3}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto :goto_1

    .line 227
    :cond_2
    invoke-static {v5, v4}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public a([[Lsoftware/simplicial/a/bz;I)V
    .locals 11

    .prologue
    const/4 v1, 0x2

    const/high16 v8, 0x3f800000    # 1.0f

    const v10, 0x8892

    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    invoke-static {v1, v0, v3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 125
    add-int/lit8 v0, p2, 0x1

    add-int/lit8 v1, p2, 0x1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    .line 126
    add-int/lit8 v1, p2, 0x1

    add-int/lit8 v2, p2, 0x1

    filled-new-array {v1, v2}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[F

    move v4, v3

    .line 127
    :goto_0
    add-int/lit8 v2, p2, 0x1

    if-ge v4, v2, :cond_1

    move v2, v3

    .line 129
    :goto_1
    add-int/lit8 v5, p2, 0x1

    if-ge v2, v5, :cond_0

    .line 131
    aget-object v5, v0, v2

    mul-int/lit8 v6, v2, 0x2

    int-to-float v6, v6

    int-to-float v7, p2

    div-float/2addr v6, v7

    sub-float/2addr v6, v8

    aput v6, v5, v4

    .line 132
    aget-object v5, v1, v2

    mul-int/lit8 v6, v4, 0x2

    int-to-float v6, v6

    int-to-float v7, p2

    div-float/2addr v6, v7

    sub-float/2addr v6, v8

    aput v6, v5, v4

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 127
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 135
    :cond_1
    mul-int/lit8 v2, p2, 0xc

    mul-int/2addr v2, p2

    new-array v6, v2, [F

    move v5, v3

    move v2, v3

    .line 137
    :goto_2
    if-ge v5, p2, :cond_3

    move v4, v2

    move v2, v3

    .line 139
    :goto_3
    if-ge v2, p2, :cond_2

    .line 144
    add-int/lit8 v7, v4, 0x1

    aget-object v8, v0, v2

    aget v8, v8, v5

    aput v8, v6, v4

    .line 145
    add-int/lit8 v4, v7, 0x1

    aget-object v8, v1, v2

    aget v8, v8, v5

    aput v8, v6, v7

    .line 148
    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v0, v8

    aget v8, v8, v5

    aput v8, v6, v4

    .line 149
    add-int/lit8 v4, v7, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v1, v8

    aget v8, v8, v5

    aput v8, v6, v7

    .line 152
    add-int/lit8 v7, v4, 0x1

    aget-object v8, v0, v2

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v4

    .line 153
    add-int/lit8 v4, v7, 0x1

    aget-object v8, v1, v2

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v7

    .line 159
    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v0, v8

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v4

    .line 160
    add-int/lit8 v4, v7, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v1, v8

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v7

    .line 163
    add-int/lit8 v7, v4, 0x1

    aget-object v8, v0, v2

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v4

    .line 164
    add-int/lit8 v4, v7, 0x1

    aget-object v8, v1, v2

    add-int/lit8 v9, v5, 0x1

    aget v8, v8, v9

    aput v8, v6, v7

    .line 167
    add-int/lit8 v7, v4, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v0, v8

    aget v8, v8, v5

    aput v8, v6, v4

    .line 168
    add-int/lit8 v4, v7, 0x1

    add-int/lit8 v8, v2, 0x1

    aget-object v8, v1, v8

    aget v8, v8, v5

    aput v8, v6, v7

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 137
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v4

    goto/16 :goto_2

    .line 172
    :cond_3
    invoke-static {v6}, Lsoftware/simplicial/nebulous/f/aa;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    aget v1, v1, v3

    invoke-static {v10, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 174
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    const v2, 0x88e4

    invoke-static {v10, v1, v0, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 175
    invoke-static {v10, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 177
    invoke-virtual {p0, p1, p2}, Lsoftware/simplicial/nebulous/views/a/g;->b([[Lsoftware/simplicial/a/bz;I)V

    .line 178
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 107
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 109
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 110
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->f:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 112
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 113
    return-void
.end method

.method public b([[Lsoftware/simplicial/a/bz;I)V
    .locals 7

    .prologue
    const v6, 0x8892

    const/4 v1, 0x0

    .line 182
    const/4 v0, 0x6

    new-array v3, v0, [I

    .line 183
    mul-int/lit8 v0, p2, 0x18

    mul-int/2addr v0, p2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 184
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 185
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v4

    move v2, v1

    .line 187
    :goto_0
    if-ge v2, p2, :cond_1

    move v0, v1

    .line 189
    :goto_1
    if-ge v0, p2, :cond_0

    .line 191
    aget-object v5, p1, v0

    aget-object v5, v5, v2

    iget-object v5, v5, Lsoftware/simplicial/a/bz;->g:Lsoftware/simplicial/a/bx;

    invoke-direct {p0, v5}, Lsoftware/simplicial/nebulous/views/a/g;->a(Lsoftware/simplicial/a/bx;)I

    move-result v5

    invoke-static {v3, v5}, Ljava/util/Arrays;->fill([II)V

    .line 192
    invoke-virtual {v4, v3}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 187
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 195
    :cond_1
    invoke-virtual {v4, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/g;->d:[I

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 197
    invoke-virtual {v4}, Ljava/nio/IntBuffer;->capacity()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    const v2, 0x88e8

    invoke-static {v6, v0, v4, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 198
    invoke-static {v6, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 199
    return-void
.end method
