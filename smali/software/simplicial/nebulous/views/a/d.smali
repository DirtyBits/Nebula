.class public abstract Lsoftware/simplicial/nebulous/views/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    .line 21
    const v0, 0x8b31

    invoke-direct {p0, v0, p1}, Lsoftware/simplicial/nebulous/views/a/d;->a(ILjava/lang/String;)I

    move-result v0

    .line 22
    const v1, 0x8b30

    invoke-direct {p0, v1, p2}, Lsoftware/simplicial/nebulous/views/a/d;->a(ILjava/lang/String;)I

    move-result v1

    .line 24
    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 25
    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 26
    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 28
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 29
    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    const v4, 0x8b82

    invoke-static {v3, v4, v2, v5}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 32
    aget v2, v2, v5

    if-nez v2, :cond_0

    .line 33
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 35
    :cond_0
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 36
    return-void
.end method

.method private a(ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 40
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 41
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 42
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 43
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 45
    :cond_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/d;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 51
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 55
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 56
    return-void
.end method
