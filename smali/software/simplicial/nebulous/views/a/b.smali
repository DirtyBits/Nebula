.class public Lsoftware/simplicial/nebulous/views/a/b;
.super Lsoftware/simplicial/nebulous/views/a/d;
.source "SourceFile"


# instance fields
.field a:Ljava/nio/FloatBuffer;

.field private final c:[I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:F

.field private i:F

.field private j:F

.field private k:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 71
    const-string v0, "attribute vec2 aPos;attribute vec2 aTex;attribute vec4 aColor;uniform vec4 uTranslateScaleAspect;varying vec2 vTex;varying vec4 vColor;void main() {float xScaled = aPos.x * uTranslateScaleAspect.z;float yScaled = aPos.y * uTranslateScaleAspect.z;gl_Position.x = (xScaled + uTranslateScaleAspect.x) * uTranslateScaleAspect.w;gl_Position.y = yScaled + uTranslateScaleAspect.y;gl_Position.z = 0.85;gl_Position.w = 1.0;vTex = aTex;vColor = aColor; }"

    const-string v1, "precision mediump float;uniform sampler2D uTex;varying vec2 vTex;varying vec4 vColor;void main() {gl_FragColor = vec4(vColor.rgb, texture2D(uTex, vTex).a);}"

    invoke-direct {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    .line 72
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->b:I

    const-string v1, "aPos"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->d:I

    .line 73
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->b:I

    const-string v1, "aTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->e:I

    .line 74
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->b:I

    const-string v1, "aColor"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->f:I

    .line 76
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->b:I

    const-string v1, "uTranslateScaleAspect"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->g:I

    .line 78
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->b:I

    const-string v1, "uTex"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 79
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v7, 0x1

    const v6, 0x8892

    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 97
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->a()V

    .line 99
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    aget v0, v0, v3

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 100
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 101
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->d:I

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 102
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    aget v0, v0, v7

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 103
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 104
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->e:I

    const/16 v2, 0x1406

    const/16 v4, 0x8

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 105
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    aget v0, v0, v1

    invoke-static {v6, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 106
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->f:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 107
    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/b;->f:I

    const/16 v6, 0x1401

    move v5, v9

    move v8, v9

    move v9, v3

    invoke-static/range {v4 .. v9}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 108
    return-void
.end method

.method public a(FFF)V
    .locals 5

    .prologue
    .line 83
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->g:I

    iget v1, p0, Lsoftware/simplicial/nebulous/views/a/b;->i:F

    sub-float v1, p1, v1

    iget v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->j:F

    mul-float/2addr v1, v2

    neg-float v2, p2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/b;->k:F

    add-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/b;->j:F

    mul-float/2addr v2, v3

    iget v3, p0, Lsoftware/simplicial/nebulous/views/a/b;->j:F

    mul-float/2addr v3, p3

    iget v4, p0, Lsoftware/simplicial/nebulous/views/a/b;->h:F

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glUniform4f(IFFFF)V

    .line 84
    return-void
.end method

.method public a(FFFF)V
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lsoftware/simplicial/nebulous/views/a/b;->h:F

    .line 89
    iput p2, p0, Lsoftware/simplicial/nebulous/views/a/b;->i:F

    .line 90
    iput p3, p0, Lsoftware/simplicial/nebulous/views/a/b;->k:F

    .line 91
    iput p4, p0, Lsoftware/simplicial/nebulous/views/a/b;->j:F

    .line 92
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x4

    const/4 v1, 0x0

    mul-int/lit8 v2, p1, 0x6

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 125
    return-void
.end method

.method public a(Ljava/util/List;F)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lsoftware/simplicial/a/ae;",
            ">;F)V"
        }
    .end annotation

    .prologue
    const v8, 0x8892

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 234
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    if-nez v0, :cond_1

    .line 239
    const/16 v0, 0x30

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 240
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 241
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    .line 244
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    aget v0, v0, v7

    invoke-static {v8, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 245
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/a/ae;

    .line 247
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v7}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 251
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 252
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 255
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 256
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 259
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 260
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 266
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 267
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 270
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 271
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 274
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->s:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 275
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    iget v3, v0, Lsoftware/simplicial/a/ae;->t:F

    sget v4, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v6

    div-float/2addr v3, p2

    sub-float/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 277
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v7}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 278
    iget-short v0, v0, Lsoftware/simplicial/a/ae;->b:S

    mul-int/lit8 v0, v0, 0x30

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    .line 280
    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/a/b;->a:Ljava/nio/FloatBuffer;

    .line 278
    invoke-static {v8, v0, v2, v3}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto/16 :goto_1

    .line 283
    :cond_2
    invoke-static {v8, v7}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    goto/16 :goto_0
.end method

.method public a([Lsoftware/simplicial/a/ae;F)V
    .locals 10

    .prologue
    const v9, 0x8892

    const/4 v1, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 190
    array-length v0, p1

    mul-int/lit8 v0, v0, 0xc

    new-array v3, v0, [F

    move v0, v1

    move v2, v1

    .line 192
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_0

    .line 197
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 198
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 201
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 202
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 205
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 206
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 212
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 213
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 216
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 217
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 220
    add-int/lit8 v4, v2, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->s:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    add-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v2

    .line 221
    add-int/lit8 v2, v4, 0x1

    aget-object v5, p1, v0

    iget v5, v5, Lsoftware/simplicial/a/ae;->t:F

    sget v6, Lsoftware/simplicial/a/ae;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v8

    div-float/2addr v5, p2

    sub-float/2addr v5, v7

    aput v5, v3, v4

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 224
    :cond_0
    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/aa;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 225
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    aget v2, v2, v1

    invoke-static {v9, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 226
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e8

    invoke-static {v9, v2, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 227
    invoke-static {v9, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 228
    return-void
.end method

.method public a([Lsoftware/simplicial/a/ae;[IF)V
    .locals 8

    .prologue
    const/4 v2, 0x3

    const v7, 0x8892

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 130
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 132
    invoke-virtual {p0, p1, p3}, Lsoftware/simplicial/nebulous/views/a/b;->a([Lsoftware/simplicial/a/ae;F)V

    .line 134
    array-length v0, p1

    mul-int/lit8 v0, v0, 0xc

    new-array v3, v0, [F

    move v0, v1

    move v2, v1

    .line 136
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_0

    .line 141
    add-int/lit8 v4, v2, 0x1

    aput v5, v3, v2

    .line 142
    add-int/lit8 v2, v4, 0x1

    aput v5, v3, v4

    .line 145
    add-int/lit8 v4, v2, 0x1

    aput v6, v3, v2

    .line 146
    add-int/lit8 v2, v4, 0x1

    aput v5, v3, v4

    .line 149
    add-int/lit8 v4, v2, 0x1

    aput v5, v3, v2

    .line 150
    add-int/lit8 v2, v4, 0x1

    aput v6, v3, v4

    .line 156
    add-int/lit8 v4, v2, 0x1

    aput v6, v3, v2

    .line 157
    add-int/lit8 v2, v4, 0x1

    aput v6, v3, v4

    .line 160
    add-int/lit8 v4, v2, 0x1

    aput v5, v3, v2

    .line 161
    add-int/lit8 v2, v4, 0x1

    aput v6, v3, v4

    .line 164
    add-int/lit8 v4, v2, 0x1

    aput v6, v3, v2

    .line 165
    add-int/lit8 v2, v4, 0x1

    aput v5, v3, v4

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_0
    invoke-static {v3}, Lsoftware/simplicial/nebulous/f/aa;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 169
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v7, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 170
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    const v3, 0x88e4

    invoke-static {v7, v2, v0, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 171
    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 173
    const/4 v0, 0x6

    new-array v2, v0, [I

    .line 174
    array-length v0, p1

    mul-int/lit8 v0, v0, 0x18

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 175
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 176
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    move v0, v1

    .line 177
    :goto_1
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 179
    aget v4, p2, v0

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 180
    invoke-virtual {v3, v2}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 182
    :cond_1
    invoke-virtual {v3, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 183
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->c:[I

    const/4 v2, 0x2

    aget v0, v0, v2

    invoke-static {v7, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 184
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->capacity()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    const v2, 0x88e4

    invoke-static {v7, v0, v3, v2}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 185
    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 186
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Lsoftware/simplicial/nebulous/views/a/d;->b()V

    .line 115
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 116
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 117
    iget v0, p0, Lsoftware/simplicial/nebulous/views/a/b;->f:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 119
    const v0, 0x8892

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 120
    return-void
.end method
