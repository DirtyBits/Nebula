.class Lsoftware/simplicial/nebulous/views/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[[Lsoftware/simplicial/nebulous/views/e;

.field private b:Ljava/util/Random;

.field private c:[J


# direct methods
.method constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v1, 0x0

    const/16 v5, 0x1b

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->b:Ljava/util/Random;

    .line 20
    filled-new-array {v5, v6}, [I

    move-result-object v0

    const-class v2, Lsoftware/simplicial/nebulous/views/e;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lsoftware/simplicial/nebulous/views/e;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->a:[[Lsoftware/simplicial/nebulous/views/e;

    .line 21
    new-array v0, v5, [J

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->c:[J

    move v2, v1

    .line 25
    :goto_0
    if-ge v2, v5, :cond_1

    move v0, v1

    .line 27
    :goto_1
    if-ge v0, v6, :cond_0

    .line 29
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/f;->a:[[Lsoftware/simplicial/nebulous/views/e;

    aget-object v3, v3, v2

    new-instance v4, Lsoftware/simplicial/nebulous/views/e;

    invoke-direct {v4}, Lsoftware/simplicial/nebulous/views/e;-><init>()V

    aput-object v4, v3, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 25
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 32
    :cond_1
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 36
    move v2, v1

    :goto_0
    const/16 v0, 0x1b

    if-ge v2, v0, :cond_3

    move v0, v1

    .line 38
    :goto_1
    const/4 v3, 0x5

    if-ge v0, v3, :cond_2

    .line 40
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/f;->a:[[Lsoftware/simplicial/nebulous/views/e;

    aget-object v3, v3, v2

    aget-object v3, v3, v0

    .line 41
    const-wide/16 v4, 0x1f4

    iget-wide v6, v3, Lsoftware/simplicial/nebulous/views/e;->d:J

    sub-long v6, p1, v6

    sub-long/2addr v4, v6

    .line 42
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gtz v6, :cond_1

    .line 38
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 45
    :cond_1
    iget v6, v3, Lsoftware/simplicial/nebulous/views/e;->e:F

    iget v7, v3, Lsoftware/simplicial/nebulous/views/e;->a:F

    const v8, 0x3ca3d70b    # 0.020000001f

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iput v6, v3, Lsoftware/simplicial/nebulous/views/e;->e:F

    .line 47
    const-wide/16 v6, 0xc8

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    .line 48
    iget-object v3, v3, Lsoftware/simplicial/nebulous/views/e;->c:[F

    const/4 v6, 0x3

    long-to-float v4, v4

    const/high16 v5, 0x43480000    # 200.0f

    div-float/2addr v4, v5

    aput v4, v3, v6

    goto :goto_2

    .line 36
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 51
    :cond_3
    return-void
.end method

.method a(JLsoftware/simplicial/a/bq;)V
    .locals 9

    .prologue
    .line 55
    iget v0, p3, Lsoftware/simplicial/a/bq;->C:I

    if-ltz v0, :cond_0

    iget v0, p3, Lsoftware/simplicial/a/bq;->C:I

    const/16 v1, 0x1b

    if-lt v0, v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->c:[J

    iget v1, p3, Lsoftware/simplicial/a/bq;->C:I

    aget-wide v0, v0, v1

    sub-long v0, p1, v0

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 61
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 63
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/f;->a:[[Lsoftware/simplicial/nebulous/views/e;

    iget v2, p3, Lsoftware/simplicial/a/bq;->C:I

    aget-object v1, v1, v2

    aget-object v1, v1, v0

    .line 64
    iget-wide v2, v1, Lsoftware/simplicial/nebulous/views/e;->d:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 66
    iput-wide p1, v1, Lsoftware/simplicial/nebulous/views/e;->d:J

    .line 67
    iput-object p3, v1, Lsoftware/simplicial/nebulous/views/e;->f:Lsoftware/simplicial/a/bq;

    .line 68
    const-wide v2, 0x402921fb54442d18L    # 12.566370614359172

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v6

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iput v0, v1, Lsoftware/simplicial/nebulous/views/e;->a:F

    .line 69
    iget-object v0, v1, Lsoftware/simplicial/nebulous/views/e;->c:[F

    iget v2, p3, Lsoftware/simplicial/a/bq;->J:I

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/views/GameView;->a([FI)V

    .line 70
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p3}, Lsoftware/simplicial/a/bq;->q_()F

    move-result v2

    mul-float/2addr v0, v2

    iput v0, v1, Lsoftware/simplicial/nebulous/views/e;->b:F

    .line 71
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->b:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v0, v2

    iput v0, v1, Lsoftware/simplicial/nebulous/views/e;->e:F

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/f;->c:[J

    iget v1, p3, Lsoftware/simplicial/a/bq;->C:I

    aput-wide p1, v0, v1

    goto :goto_0

    .line 61
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
