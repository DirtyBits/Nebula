.class public Lsoftware/simplicial/nebulous/views/b/q;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/q$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/q$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 33
    const v0, 0x41e66666    # 28.8f

    const/4 v1, -0x1

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 29
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/q$a;

    sget-object v1, Lsoftware/simplicial/a/i/e;->a:Lsoftware/simplicial/a/i/e;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/q$a;-><init>(Lsoftware/simplicial/nebulous/views/b/q;Lsoftware/simplicial/a/i/e;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->b:Lsoftware/simplicial/nebulous/views/b/q$a;

    .line 34
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 16
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/q$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/q;->a(Lsoftware/simplicial/nebulous/views/b/q$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/q$a;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 48
    if-nez p1, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 80
    :goto_0
    return-object v0

    .line 51
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 53
    const/16 v0, 0x200

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    .line 54
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->f:Landroid/graphics/Canvas;

    .line 57
    :cond_1
    const-string v1, "NULL"

    .line 58
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/q$a;->b(Lsoftware/simplicial/nebulous/views/b/q$a;)I

    move-result v0

    .line 59
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/q$1;->a:[I

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/q$a;->a(Lsoftware/simplicial/nebulous/views/b/q$a;)Lsoftware/simplicial/a/i/e;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/i/e;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 76
    :goto_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 77
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/q;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/q;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 62
    :pswitch_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->c:Landroid/content/res/Resources;

    const v1, 0x7f0802c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {v5, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 66
    :pswitch_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->c:Landroid/content/res/Resources;

    const v1, 0x7f0802ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 70
    :pswitch_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->c:Landroid/content/res/Resources;

    const v1, 0x7f0802c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 71
    const/16 v0, 0xa5

    invoke-static {v5, v0, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/i/e;I)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->b:Lsoftware/simplicial/nebulous/views/b/q$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/q$a;->a(Lsoftware/simplicial/nebulous/views/b/q$a;)Lsoftware/simplicial/a/i/e;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->b:Lsoftware/simplicial/nebulous/views/b/q$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/q$a;->b(Lsoftware/simplicial/nebulous/views/b/q$a;)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/q$a;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/views/b/q$a;-><init>(Lsoftware/simplicial/nebulous/views/b/q;Lsoftware/simplicial/a/i/e;I)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->b:Lsoftware/simplicial/nebulous/views/b/q$a;

    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/q;->b:Lsoftware/simplicial/nebulous/views/b/q$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/q;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
