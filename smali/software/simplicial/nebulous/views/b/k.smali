.class public Lsoftware/simplicial/nebulous/views/b/k;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/k$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/k$a;

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 36
    const v0, 0x41e66666    # 28.8f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v3, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 30
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/k$a;

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-direct {v0, p0, v1, v3, v2}, Lsoftware/simplicial/nebulous/views/b/k$a;-><init>(Lsoftware/simplicial/nebulous/views/b/k;IIF)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    .line 32
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->h:J

    .line 37
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/k$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/k;->a(Lsoftware/simplicial/nebulous/views/b/k$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/k$a;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 57
    const/16 v0, 0x100

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    .line 58
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->f:Landroid/graphics/Canvas;

    .line 60
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/k;->c:Landroid/content/res/Resources;

    const v2, 0x7f080258

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/k$a;->a(Lsoftware/simplicial/nebulous/views/b/k$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/k$a;->b(Lsoftware/simplicial/nebulous/views/b/k$a;)F

    move-result v1

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/k$a;->b(Lsoftware/simplicial/nebulous/views/b/k$a;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    :cond_2
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 64
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/k;->e:Landroid/text/TextPaint;

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/k$a;->c(Lsoftware/simplicial/nebulous/views/b/k$a;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 65
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/k;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/k;->e:Landroid/text/TextPaint;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 67
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->g:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public a(IFIJ)V
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->h:J

    cmp-long v0, p4, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/k$a;->a(Lsoftware/simplicial/nebulous/views/b/k$a;)I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/k$a;->b(Lsoftware/simplicial/nebulous/views/b/k$a;)F

    move-result v0

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/k$a;->c(Lsoftware/simplicial/nebulous/views/b/k$a;)I

    move-result v0

    if-ne v0, p3, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const-wide/16 v0, 0x1f4

    add-long/2addr v0, p4

    iput-wide v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->h:J

    .line 45
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/k$a;

    invoke-direct {v0, p0, p1, p3, p2}, Lsoftware/simplicial/nebulous/views/b/k$a;-><init>(Lsoftware/simplicial/nebulous/views/b/k;IIF)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    .line 46
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/k;->b:Lsoftware/simplicial/nebulous/views/b/k$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/k;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
