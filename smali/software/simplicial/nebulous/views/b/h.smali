.class public Lsoftware/simplicial/nebulous/views/b/h;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/h$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Landroid/graphics/Bitmap;

.field private static h:Landroid/graphics/Bitmap;

.field private static i:Landroid/graphics/Bitmap;


# instance fields
.field private j:Lsoftware/simplicial/nebulous/views/b/h$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    array-length v0, v0

    new-array v0, v0, [Landroid/graphics/Bitmap;

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/h;->b:[Landroid/graphics/Bitmap;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x0

    .line 83
    const/high16 v0, 0x41c00000    # 24.0f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v4, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 79
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/h$a;

    const-string v2, ""

    new-array v3, v5, [B

    sget-object v7, Lsoftware/simplicial/a/q;->a:Lsoftware/simplicial/a/q;

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8, v5}, Ljava/util/HashSet;-><init>(I)V

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9, v5}, Ljava/util/HashSet;-><init>(I)V

    move-object v1, p0

    move v6, v5

    invoke-direct/range {v0 .. v9}, Lsoftware/simplicial/nebulous/views/b/h$a;-><init>(Lsoftware/simplicial/nebulous/views/b/h;Ljava/lang/String;[BIZZLsoftware/simplicial/a/q;Ljava/util/Set;Ljava/util/Set;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    .line 84
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 34
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 35
    const/4 v2, 0x1

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 36
    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 37
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 38
    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 42
    :goto_0
    sget-object v2, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 44
    sget-object v2, Lsoftware/simplicial/a/q;->g:[Lsoftware/simplicial/a/q;

    aget-object v2, v2, v0

    invoke-static {v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/q;)I

    move-result v2

    .line 45
    if-eqz v2, :cond_0

    .line 46
    sget-object v3, Lsoftware/simplicial/nebulous/views/b/h;->b:[Landroid/graphics/Bitmap;

    invoke-static {p0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v3, v0

    .line 42
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/h;->b:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    goto :goto_1

    .line 50
    :cond_1
    const v0, 0x7f0200a5

    invoke-static {p0, v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/h;->h:Landroid/graphics/Bitmap;

    .line 51
    const v0, 0x7f0200a6

    invoke-static {p0, v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/h;->i:Landroid/graphics/Bitmap;

    .line 52
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/h;->a(Lsoftware/simplicial/nebulous/views/b/h$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/h$a;)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    .line 100
    if-nez p1, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 174
    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->c(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/lang/String;

    move-result-object v2

    .line 103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/h$a;->g(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    .line 104
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/h$a;->h(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    .line 106
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    if-nez v0, :cond_2

    .line 108
    :cond_1
    const/16 v0, 0x100

    const/16 v1, 0x40

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    .line 109
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    .line 110
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    const/16 v1, 0x64

    const/16 v3, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v1, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 112
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 114
    const/4 v0, 0x0

    .line 115
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->a(Lsoftware/simplicial/nebulous/views/b/h$a;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 117
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    add-float v8, v0, v1

    .line 118
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->d(Lsoftware/simplicial/nebulous/views/b/h$a;)[B

    move-result-object v0

    invoke-static {v2, v0, v9, v10}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v1

    .line 119
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->d(Lsoftware/simplicial/nebulous/views/b/h$a;)[B

    move-result-object v0

    invoke-static {v2, v0, v9, v10}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[BZZ)Landroid/text/SpannableString;

    move-result-object v11

    .line 121
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 123
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 124
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    const/16 v2, 0xff

    const/16 v3, 0x7f

    const/16 v4, 0x7f

    const/16 v5, 0x7f

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 125
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 126
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 128
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    const/16 v1, 0xff

    const/16 v2, 0xff

    const/16 v3, 0xff

    const/16 v4, 0xff

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 130
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v11

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 131
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move v0, v8

    .line 134
    :cond_3
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->b(Lsoftware/simplicial/nebulous/views/b/h$a;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->e(Lsoftware/simplicial/nebulous/views/b/h$a;)I

    move-result v1

    if-lez v1, :cond_4

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->e(Lsoftware/simplicial/nebulous/views/b/h$a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 140
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v2

    add-float/2addr v2, v0

    .line 141
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 142
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    const/16 v3, 0xff

    const/16 v4, 0x7f

    const/16 v5, 0x7f

    const/16 v6, 0x7f

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 143
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 144
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 145
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    const/16 v3, 0xff

    const/16 v4, 0x64

    const/16 v5, 0xc8

    const/16 v6, 0xff

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 146
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 147
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 150
    if-eqz v9, :cond_5

    .line 151
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/h;->h:Landroid/graphics/Bitmap;

    .line 156
    :goto_1
    if-eqz v0, :cond_4

    .line 158
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/h;->e:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v3, v4

    .line 161
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    .line 162
    iget-object v5, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v6, v1

    sub-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v3, v6

    add-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v6, v3

    sub-float/2addr v5, v6

    .line 163
    sub-float v6, v2, v3

    const v7, 0x3d4ccccd    # 0.05f

    mul-float/2addr v7, v3

    add-float/2addr v6, v7

    .line 164
    const v7, 0x3d4ccccd    # 0.05f

    mul-float/2addr v7, v3

    add-float/2addr v2, v7

    .line 165
    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, v4, v6, v5, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 166
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v4, v0, v5, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 167
    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    sub-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v5, v3

    add-float/2addr v4, v5

    .line 168
    iget-object v5, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float/2addr v1, v8

    add-float/2addr v1, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v3, v5

    add-float/2addr v1, v5

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    .line 169
    invoke-virtual {v7, v4, v6, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 170
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 174
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h;->g:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 152
    :cond_5
    if-eqz v10, :cond_6

    .line 153
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/h;->i:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 155
    :cond_6
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/h;->b:[Landroid/graphics/Bitmap;

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/h$a;->f(Lsoftware/simplicial/nebulous/views/b/h$a;)Lsoftware/simplicial/a/q;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/q;->ordinal()I

    move-result v3

    aget-object v0, v0, v3

    goto/16 :goto_1
.end method

.method public a(ZZLjava/lang/String;[BILsoftware/simplicial/a/q;Ljava/util/Set;Ljava/util/Set;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/lang/String;",
            "[BI",
            "Lsoftware/simplicial/a/q;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->a(Lsoftware/simplicial/nebulous/views/b/h$a;)Z

    move-result v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->b(Lsoftware/simplicial/nebulous/views/b/h$a;)Z

    move-result v1

    if-ne v1, p2, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->c(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    .line 89
    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->d(Lsoftware/simplicial/nebulous/views/b/h$a;)[B

    move-result-object v1

    invoke-static {v1, p4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->e(Lsoftware/simplicial/nebulous/views/b/h$a;)I

    move-result v1

    move/from16 v0, p5

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->f(Lsoftware/simplicial/nebulous/views/b/h$a;)Lsoftware/simplicial/a/q;

    move-result-object v1

    move-object/from16 v0, p6

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    .line 90
    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->g(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;

    move-result-object v1

    move-object/from16 v0, p7

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/h$a;->h(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;

    move-result-object v1

    move-object/from16 v0, p8

    if-ne v1, v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/views/b/h$a;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move v6, p1

    move v7, p2

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lsoftware/simplicial/nebulous/views/b/h$a;-><init>(Lsoftware/simplicial/nebulous/views/b/h;Ljava/lang/String;[BIZZLsoftware/simplicial/a/q;Ljava/util/Set;Ljava/util/Set;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    .line 94
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/h;->j:Lsoftware/simplicial/nebulous/views/b/h$a;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/h;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
