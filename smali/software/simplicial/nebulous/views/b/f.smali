.class public Lsoftware/simplicial/nebulous/views/b/f;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/f$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/f$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/f$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 8

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 41
    const v0, 0x41b33333    # 22.4f

    const v1, -0xaf3f01

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v1, v4}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 37
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/f$a;

    move-object v1, p0

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lsoftware/simplicial/nebulous/views/b/f$a;-><init>(Lsoftware/simplicial/nebulous/views/b/f;IZIZZI)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    .line 42
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->e:Landroid/text/TextPaint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 43
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/f;->a(Lsoftware/simplicial/nebulous/views/b/f$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/f$a;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 111
    :goto_0
    return-object v0

    .line 61
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 63
    const/16 v0, 0xa0

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    .line 64
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->f:Landroid/graphics/Canvas;

    .line 66
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 70
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->b(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->c(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->f(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    .line 88
    :goto_1
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->d(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/f;->c:Landroid/content/res/Resources;

    const v3, 0x7f08017a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->a(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    :goto_2
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/f;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 109
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->f:Landroid/graphics/Canvas;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lsoftware/simplicial/nebulous/views/b/f;->d:F

    const v4, 0x3f866666    # 1.05f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/f;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 111
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 75
    :pswitch_0
    const v0, -0xaf3f01

    .line 76
    goto :goto_1

    .line 78
    :pswitch_1
    const/16 v0, -0x100

    .line 79
    goto :goto_1

    .line 81
    :pswitch_2
    const/high16 v0, -0x10000

    .line 82
    goto :goto_1

    .line 94
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->c:Landroid/content/res/Resources;

    const v1, 0x7f08017d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->f(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    goto :goto_2

    .line 98
    :cond_3
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->e(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->c:Landroid/content/res/Resources;

    const v1, 0x7f080286

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->f(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    goto :goto_2

    .line 105
    :cond_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->c:Landroid/content/res/Resources;

    const v1, 0x7f0801c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/f$a;->f(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    goto :goto_2

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(IZIZZI)V
    .locals 8

    .prologue
    .line 47
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->a(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->b(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->c(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->d(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v0

    if-ne v0, p4, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    .line 48
    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->e(Lsoftware/simplicial/nebulous/views/b/f$a;)Z

    move-result v0

    if-ne v0, p5, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/f$a;->f(Lsoftware/simplicial/nebulous/views/b/f$a;)I

    move-result v0

    if-ne v0, p6, :cond_0

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/f$a;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lsoftware/simplicial/nebulous/views/b/f$a;-><init>(Lsoftware/simplicial/nebulous/views/b/f;IZIZZI)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    .line 52
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/f;->b:Lsoftware/simplicial/nebulous/views/b/f$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/f;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
