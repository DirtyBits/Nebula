.class public Lsoftware/simplicial/nebulous/views/b/c;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/c$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/c$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 46
    const v0, 0x41e66666    # 28.8f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v4, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 42
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/c$a;

    sget-object v2, Lsoftware/simplicial/a/c/h;->a:Lsoftware/simplicial/a/c/h;

    sget-object v7, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    const/4 v9, 0x0

    move-object v1, p0

    move v5, v4

    move v6, v3

    move v8, v4

    invoke-direct/range {v0 .. v9}, Lsoftware/simplicial/nebulous/views/b/c$a;-><init>(Lsoftware/simplicial/nebulous/views/b/c;Lsoftware/simplicial/a/c/h;IIIZLsoftware/simplicial/a/am;ILsoftware/simplicial/nebulous/views/b/c$1;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    .line 47
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 18
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/c;->a(Lsoftware/simplicial/nebulous/views/b/c$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/c$a;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 62
    if-nez p1, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 116
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 67
    const/16 v0, 0x100

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    .line 68
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->f:Landroid/graphics/Canvas;

    .line 71
    :cond_1
    const-string v1, "NULL"

    .line 72
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->f(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v0

    .line 73
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->d(Lsoftware/simplicial/nebulous/views/b/c$a;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/c$1;->a:[I

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->a(Lsoftware/simplicial/nebulous/views/b/c$a;)Lsoftware/simplicial/a/c/h;

    move-result-object v3

    invoke-virtual {v3}, Lsoftware/simplicial/a/c/h;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 112
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 113
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/c;->e:Landroid/text/TextPaint;

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 114
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/c;->e:Landroid/text/TextPaint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 116
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 78
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    const v2, 0x7f08012d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->b(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->c(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto :goto_1

    .line 82
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    const v2, 0x7f080259

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->b(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->c(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-static {v5, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 86
    :pswitch_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    const v1, 0x7f08006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-static {v4, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 90
    :pswitch_4
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->g(Lsoftware/simplicial/nebulous/views/b/c$a;)Lsoftware/simplicial/a/am;

    move-result-object v0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Lsoftware/simplicial/a/am;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 101
    :cond_2
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/c$a;->e(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v0

    if-nez v0, :cond_3

    .line 103
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    const v1, 0x7f08032c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-static {v5, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 108
    :cond_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/c;->c:Landroid/content/res/Resources;

    const v1, 0x7f080253

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v4, v5, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    goto/16 :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lsoftware/simplicial/a/c/h;IIIZLsoftware/simplicial/a/am;I)V
    .locals 11

    .prologue
    .line 51
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->a(Lsoftware/simplicial/nebulous/views/b/c$a;)Lsoftware/simplicial/a/c/h;

    move-result-object v1

    if-ne v1, p1, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->b(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    if-ne v1, p3, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->c(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    if-ne v1, p4, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->d(Lsoftware/simplicial/nebulous/views/b/c$a;)Z

    move-result v1

    move/from16 v0, p5

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    .line 52
    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->e(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    move/from16 v0, p7

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->f(Lsoftware/simplicial/nebulous/views/b/c$a;)I

    move-result v1

    if-ne v1, p2, :cond_0

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/c$a;->g(Lsoftware/simplicial/nebulous/views/b/c$a;)Lsoftware/simplicial/a/am;

    move-result-object v1

    move-object/from16 v0, p6

    if-ne v1, v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 55
    :cond_0
    new-instance v1, Lsoftware/simplicial/nebulous/views/b/c$a;

    const/4 v10, 0x0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v10}, Lsoftware/simplicial/nebulous/views/b/c$a;-><init>(Lsoftware/simplicial/nebulous/views/b/c;Lsoftware/simplicial/a/c/h;IIIZLsoftware/simplicial/a/am;ILsoftware/simplicial/nebulous/views/b/c$1;)V

    iput-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    .line 56
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/c;->b:Lsoftware/simplicial/nebulous/views/b/c$a;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/c;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
