.class public Lsoftware/simplicial/nebulous/views/b/j;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/j$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/j$a;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Lsoftware/simplicial/nebulous/views/b/j$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 32
    const v0, 0x41e66666    # 28.8f

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v2, v1}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 28
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/j$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/j$a;-><init>(Lsoftware/simplicial/nebulous/views/b/j;FI)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->b:Lsoftware/simplicial/nebulous/views/b/j$a;

    .line 33
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/j$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/j;->a(Lsoftware/simplicial/nebulous/views/b/j$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/j$a;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 47
    if-nez p1, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 52
    const/16 v0, 0x200

    const/16 v1, 0x20

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    .line 53
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->f:Landroid/graphics/Canvas;

    .line 55
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/j;->c:Landroid/content/res/Resources;

    const v2, 0x7f08022a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/j$a;->a(Lsoftware/simplicial/nebulous/views/b/j$a;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 57
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/j;->e:Landroid/text/TextPaint;

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/j$a;->b(Lsoftware/simplicial/nebulous/views/b/j$a;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 58
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/j;->f:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/j;->e:Landroid/text/TextPaint;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 60
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->g:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public a(FI)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->b:Lsoftware/simplicial/nebulous/views/b/j$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/j$a;->a(Lsoftware/simplicial/nebulous/views/b/j$a;)F

    move-result v0

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->b:Lsoftware/simplicial/nebulous/views/b/j$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/j$a;->b(Lsoftware/simplicial/nebulous/views/b/j$a;)I

    move-result v0

    if-ne v0, p2, :cond_0

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/j$a;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/views/b/j$a;-><init>(Lsoftware/simplicial/nebulous/views/b/j;FI)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->b:Lsoftware/simplicial/nebulous/views/b/j$a;

    .line 41
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/j;->b:Lsoftware/simplicial/nebulous/views/b/j$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/j;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
