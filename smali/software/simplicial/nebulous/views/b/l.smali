.class public Lsoftware/simplicial/nebulous/views/b/l;
.super Lsoftware/simplicial/nebulous/views/b/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/b",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/l$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/util/concurrent/Executor;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:Lsoftware/simplicial/nebulous/f/p;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:Landroid/graphics/BitmapFactory$Options;

.field private h:Lsoftware/simplicial/nebulous/views/b/l$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/16 v0, 0x36

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/l;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lsoftware/simplicial/nebulous/f/p;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 54
    sget-object v0, Lsoftware/simplicial/nebulous/views/b/l;->b:Ljava/util/concurrent/Executor;

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/views/b/b;-><init>(Ljava/util/concurrent/Executor;)V

    .line 50
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/l$a;

    sget-object v4, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    const/4 v5, 0x0

    move-object v1, p0

    move v3, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/b/l$a;-><init>(Lsoftware/simplicial/nebulous/views/b/l;ZILsoftware/simplicial/a/e;Landroid/graphics/Bitmap;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    .line 55
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/l;->c:Landroid/content/res/Resources;

    .line 56
    iput-object p2, p0, Lsoftware/simplicial/nebulous/views/b/l;->d:Lsoftware/simplicial/nebulous/f/p;

    .line 57
    iput-object p3, p0, Lsoftware/simplicial/nebulous/views/b/l;->e:Ljava/lang/String;

    .line 58
    iput-boolean p4, p0, Lsoftware/simplicial/nebulous/views/b/l;->f:Z

    .line 60
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 62
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 63
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 64
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 65
    return-void
.end method

.method private a(Lsoftware/simplicial/a/e;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 122
    const v0, 0x7f020299

    .line 123
    iget-boolean v1, p0, Lsoftware/simplicial/nebulous/views/b/l;->f:Z

    if-eqz v1, :cond_0

    .line 125
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->c:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lsoftware/simplicial/a/e;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawable"

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/l;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 149
    :goto_0
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/l;->c:Landroid/content/res/Resources;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/l;->g:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, v0, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 129
    :cond_0
    sget-object v1, Lsoftware/simplicial/nebulous/views/b/l$1;->a:[I

    iget-object v2, p1, Lsoftware/simplicial/a/e;->ll:Lsoftware/simplicial/a/f;

    invoke-virtual {v2}, Lsoftware/simplicial/a/f;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 132
    :pswitch_0
    const v0, 0x7f02025e

    .line 133
    goto :goto_0

    .line 135
    :pswitch_1
    const v0, 0x7f020376

    .line 136
    goto :goto_0

    .line 138
    :pswitch_2
    const v0, 0x7f020332

    .line 139
    goto :goto_0

    .line 141
    :pswitch_3
    const v0, 0x7f0201db

    .line 142
    goto :goto_0

    .line 144
    :pswitch_4
    const v0, 0x7f0201d0

    goto :goto_0

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/l$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/l;->a(Lsoftware/simplicial/nebulous/views/b/l$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/l$a;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 81
    if-nez p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    :try_start_0
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->e(Lsoftware/simplicial/nebulous/views/b/l$a;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->d(Lsoftware/simplicial/nebulous/views/b/l$a;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 89
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->d(Lsoftware/simplicial/nebulous/views/b/l$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_2
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->b(Lsoftware/simplicial/nebulous/views/b/l$a;)I

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->a(Lsoftware/simplicial/nebulous/views/b/l$a;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 93
    :cond_3
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->c(Lsoftware/simplicial/nebulous/views/b/l$a;)Lsoftware/simplicial/a/e;

    move-result-object v1

    invoke-direct {p0, v1}, Lsoftware/simplicial/nebulous/views/b/l;->a(Lsoftware/simplicial/a/e;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v1, v0

    .line 99
    :cond_5
    :try_start_1
    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/l;->d:Lsoftware/simplicial/nebulous/f/p;

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->b(Lsoftware/simplicial/nebulous/views/b/l$a;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lsoftware/simplicial/nebulous/f/p;->a(ILsoftware/simplicial/nebulous/f/al$ac;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 100
    if-nez v1, :cond_6

    .line 101
    const-wide/16 v2, 0xf

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 102
    :cond_6
    if-eqz v1, :cond_5

    .line 103
    sget-object v2, Lsoftware/simplicial/nebulous/f/p;->a:Landroid/graphics/Bitmap;

    if-ne v1, v2, :cond_7

    .line 105
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->c(Lsoftware/simplicial/nebulous/views/b/l$a;)Lsoftware/simplicial/a/e;

    move-result-object v2

    sget-object v3, Lsoftware/simplicial/a/e;->b:Lsoftware/simplicial/a/e;

    if-eq v2, v3, :cond_0

    .line 106
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/l$a;->c(Lsoftware/simplicial/nebulous/views/b/l$a;)Lsoftware/simplicial/a/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lsoftware/simplicial/nebulous/views/b/l;->a(Lsoftware/simplicial/a/e;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v1

    .line 114
    :goto_1
    sget-object v2, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lsoftware/simplicial/a/e/a;->a(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 112
    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_0
.end method

.method public a(ZILsoftware/simplicial/a/e;Landroid/graphics/Bitmap;Z)V
    .locals 7

    .prologue
    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/l$a;->a(Lsoftware/simplicial/nebulous/views/b/l$a;)Z

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/l$a;->b(Lsoftware/simplicial/nebulous/views/b/l$a;)I

    move-result v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/l$a;->c(Lsoftware/simplicial/nebulous/views/b/l$a;)Lsoftware/simplicial/a/e;

    move-result-object v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/l$a;->d(Lsoftware/simplicial/nebulous/views/b/l$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-ne v0, p4, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    .line 70
    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/l$a;->e(Lsoftware/simplicial/nebulous/views/b/l$a;)Z

    move-result v0

    if-ne v0, p5, :cond_0

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lsoftware/simplicial/nebulous/views/b/l;->a(Z)V

    .line 74
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/l$a;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lsoftware/simplicial/nebulous/views/b/l$a;-><init>(Lsoftware/simplicial/nebulous/views/b/l;ZILsoftware/simplicial/a/e;Landroid/graphics/Bitmap;Z)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/l;->h:Lsoftware/simplicial/nebulous/views/b/l$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/l;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
