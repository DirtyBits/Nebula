.class Lsoftware/simplicial/nebulous/views/b/h$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/views/b/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/views/b/h;

.field private final b:Ljava/lang/String;

.field private final c:[B

.field private final d:I

.field private final e:Z

.field private final f:Z

.field private final g:Lsoftware/simplicial/a/q;

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/views/b/h;Ljava/lang/String;[BIZZLsoftware/simplicial/a/q;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[BIZZ",
            "Lsoftware/simplicial/a/q;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->a:Lsoftware/simplicial/nebulous/views/b/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p2, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->b:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->c:[B

    .line 70
    iput p4, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->d:I

    .line 71
    iput-boolean p5, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->e:Z

    .line 72
    iput-boolean p6, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->f:Z

    .line 73
    iput-object p7, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->g:Lsoftware/simplicial/a/q;

    .line 74
    iput-object p8, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->h:Ljava/util/Set;

    .line 75
    iput-object p9, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->i:Ljava/util/Set;

    .line 76
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/b/h$a;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->e:Z

    return v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/b/h$a;)Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->f:Z

    return v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/views/b/h$a;)[B
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->c:[B

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/views/b/h$a;)I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->d:I

    return v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/views/b/h$a;)Lsoftware/simplicial/a/q;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->g:Lsoftware/simplicial/a/q;

    return-object v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->h:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/views/b/h$a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/h$a;->i:Ljava/util/Set;

    return-object v0
.end method
