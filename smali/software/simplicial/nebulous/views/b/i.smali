.class public Lsoftware/simplicial/nebulous/views/b/i;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/i$a;",
        ">;"
    }
.end annotation


# instance fields
.field b:Lsoftware/simplicial/nebulous/views/b/i$a;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .prologue
    .line 39
    const/high16 v0, 0x42400000    # 48.0f

    const/4 v1, -0x1

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-direct {p0, p1, v0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 35
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/i$a;

    const-string v1, ""

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-direct {v0, p0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/i$a;-><init>(Lsoftware/simplicial/nebulous/views/b/i;Ljava/lang/String;[B)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->b:Lsoftware/simplicial/nebulous/views/b/i$a;

    .line 40
    return-void
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/i$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/i;->a(Lsoftware/simplicial/nebulous/views/b/i$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/i$a;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/16 v3, 0x7f

    const/4 v7, 0x0

    const/16 v9, 0xff

    .line 54
    if-nez p1, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 79
    :goto_0
    return-object v0

    .line 57
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 59
    const/16 v0, 0x200

    const/16 v1, 0x40

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->g:Landroid/graphics/Bitmap;

    .line 60
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/i;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    .line 61
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    const/16 v1, 0x64

    invoke-static {v1, v9, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 63
    :cond_1
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 65
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/i$a;->a(Lsoftware/simplicial/nebulous/views/b/i$a;)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/i$a;->a(Lsoftware/simplicial/nebulous/views/b/i$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/i$a;->b(Lsoftware/simplicial/nebulous/views/b/i$a;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v8

    .line 67
    instance-of v0, v8, Landroid/text/SpannableString;

    if-eqz v0, :cond_2

    .line 68
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/i$a;->a(Lsoftware/simplicial/nebulous/views/b/i$a;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/i$a;->b(Lsoftware/simplicial/nebulous/views/b/i$a;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v1

    .line 70
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    invoke-static {v9, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 72
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 73
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 74
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    invoke-static {v9, v9, v9, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 76
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/i;->e:Landroid/text/TextPaint;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    move-object v1, v8

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 77
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/i;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->g:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->b:Lsoftware/simplicial/nebulous/views/b/i$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/i$a;->a(Lsoftware/simplicial/nebulous/views/b/i$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->b:Lsoftware/simplicial/nebulous/views/b/i$a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/views/b/i$a;->b(Lsoftware/simplicial/nebulous/views/b/i$a;)[B

    move-result-object v0

    invoke-static {v0, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v0, Lsoftware/simplicial/nebulous/views/b/i$a;

    invoke-direct {v0, p0, p1, p2}, Lsoftware/simplicial/nebulous/views/b/i$a;-><init>(Lsoftware/simplicial/nebulous/views/b/i;Ljava/lang/String;[B)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->b:Lsoftware/simplicial/nebulous/views/b/i$a;

    .line 48
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/i;->b:Lsoftware/simplicial/nebulous/views/b/i$a;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/i;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
