.class public Lsoftware/simplicial/nebulous/views/b/p;
.super Lsoftware/simplicial/nebulous/views/b/o;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/views/b/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/o",
        "<",
        "Lsoftware/simplicial/nebulous/views/b/p$a;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/Byte;


# instance fields
.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private final i:F

.field private j:Lsoftware/simplicial/nebulous/views/b/p$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Byte;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lsoftware/simplicial/nebulous/views/b/p;->b:[Ljava/lang/Byte;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 17

    .prologue
    .line 71
    const v2, 0x429d89d9

    const/4 v3, -0x1

    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lsoftware/simplicial/nebulous/views/b/o;-><init>(Landroid/content/res/Resources;FILandroid/graphics/Paint$Align;)V

    .line 66
    new-instance v2, Lsoftware/simplicial/nebulous/views/b/p$a;

    const/4 v4, -0x1

    const/4 v3, 0x5

    new-array v5, v3, [I

    fill-array-data v5, :array_0

    const/4 v3, 0x5

    new-array v6, v3, [I

    fill-array-data v6, :array_1

    const/4 v3, 0x5

    new-array v7, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v8, 0x0

    aput-object v8, v7, v3

    const/4 v3, 0x1

    const/4 v8, 0x0

    aput-object v8, v7, v3

    const/4 v3, 0x2

    const/4 v8, 0x0

    aput-object v8, v7, v3

    const/4 v3, 0x3

    const/4 v8, 0x0

    aput-object v8, v7, v3

    const/4 v3, 0x4

    const/4 v8, 0x0

    aput-object v8, v7, v3

    const/4 v3, 0x5

    new-array v8, v3, [[B

    const/4 v3, 0x5

    new-array v9, v3, [B

    fill-array-data v9, :array_2

    const/4 v3, 0x5

    new-array v10, v3, [B

    fill-array-data v10, :array_3

    sget-object v11, Lsoftware/simplicial/a/am;->a:Lsoftware/simplicial/a/am;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v16}, Lsoftware/simplicial/nebulous/views/b/p$a;-><init>(Lsoftware/simplicial/nebulous/views/b/p;I[I[I[Ljava/lang/String;[[B[B[BLsoftware/simplicial/a/am;ZZZZI)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/views/b/p;->j:Lsoftware/simplicial/nebulous/views/b/p$a;

    .line 72
    sget-object v2, Lsoftware/simplicial/nebulous/views/b/p;->b:[Ljava/lang/Byte;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lsoftware/simplicial/nebulous/views/b/p;->h:Ljava/util/List;

    .line 73
    move-object/from16 v0, p0

    iget v2, v0, Lsoftware/simplicial/nebulous/views/b/p;->d:F

    const v3, 0x3f91eb85    # 1.14f

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lsoftware/simplicial/nebulous/views/b/p;->i:F

    .line 75
    move-object/from16 v0, p0

    iget-object v2, v0, Lsoftware/simplicial/nebulous/views/b/p;->e:Landroid/text/TextPaint;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 76
    return-void

    .line 66
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method bridge synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 26
    check-cast p1, Lsoftware/simplicial/nebulous/views/b/p$a;

    invoke-virtual {p0, p1}, Lsoftware/simplicial/nebulous/views/b/p;->a(Lsoftware/simplicial/nebulous/views/b/p$a;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method a(Lsoftware/simplicial/nebulous/views/b/p$a;)Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    const/4 v5, 0x5

    const/4 v12, 0x1

    const/4 v6, 0x0

    const/16 v8, 0xff

    const/4 v7, 0x0

    .line 130
    if-nez p1, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 220
    :goto_0
    return-object v0

    .line 134
    :cond_0
    new-array v10, v5, [Ljava/lang/CharSequence;

    .line 135
    new-array v11, v5, [I

    .line 137
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->c(Lsoftware/simplicial/nebulous/views/b/p$a;)I

    move-result v0

    if-lez v0, :cond_8

    move v0, v7

    move v1, v7

    .line 139
    :goto_1
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->c(Lsoftware/simplicial/nebulous/views/b/p$a;)I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 141
    const-string v2, ""

    .line 142
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->d(Lsoftware/simplicial/nebulous/views/b/p$a;)Lsoftware/simplicial/a/am;

    move-result-object v3

    sget-object v4, Lsoftware/simplicial/a/am;->m:Lsoftware/simplicial/a/am;

    if-ne v3, v4, :cond_1

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->e(Lsoftware/simplicial/nebulous/views/b/p$a;)[B

    move-result-object v3

    aget-byte v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 145
    :cond_1
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->a(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v3

    aget v3, v3, v0

    packed-switch v3, :pswitch_data_0

    .line 164
    const-string v2, "NULL"

    aput-object v2, v10, v0

    .line 165
    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v11, v0

    .line 168
    :goto_2
    add-int/lit8 v1, v1, 0x1

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 148
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->d(Lsoftware/simplicial/nebulous/views/b/p$a;)Lsoftware/simplicial/a/am;

    move-result-object v3

    sget-object v5, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_2

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f08032e

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    .line 149
    invoke-static {v8, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v11, v0

    goto :goto_2

    .line 148
    :cond_2
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f08022b

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 152
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->d(Lsoftware/simplicial/nebulous/views/b/p$a;)Lsoftware/simplicial/a/am;

    move-result-object v3

    sget-object v5, Lsoftware/simplicial/a/am;->k:Lsoftware/simplicial/a/am;

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f08029d

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_4
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    .line 153
    invoke-static {v7, v8, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v11, v0

    goto/16 :goto_2

    .line 152
    :cond_3
    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f080142

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 156
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f080057

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    .line 157
    invoke-static {v7, v7, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v11, v0

    goto/16 :goto_2

    .line 160
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lsoftware/simplicial/nebulous/views/b/p;->c:Landroid/content/res/Resources;

    const v5, 0x7f080317

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v3

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v0

    .line 161
    invoke-static {v8, v8, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v11, v0

    goto/16 :goto_2

    :cond_4
    move v8, v1

    .line 189
    :goto_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5

    .line 191
    const/16 v0, 0x400

    const/16 v1, 0x200

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->g:Landroid/graphics/Bitmap;

    .line 192
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/p;->g:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    .line 194
    :cond_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v7}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 196
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    move v9, v7

    .line 197
    :goto_6
    if-ge v9, v8, :cond_d

    .line 199
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->e:Landroid/text/TextPaint;

    aget v1, v11, v9

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 200
    aget-object v1, v10, v9

    .line 201
    if-nez v1, :cond_6

    .line 202
    const-string v1, ""

    .line 204
    :cond_6
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    .line 205
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_7

    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->h:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v2

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 206
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 207
    :cond_7
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/p;->e:Landroid/text/TextPaint;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 208
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    move v13, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v13

    .line 209
    :goto_7
    if-le v0, v12, :cond_c

    .line 211
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {v2, v12, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 212
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lsoftware/simplicial/nebulous/views/b/p;->e:Landroid/text/TextPaint;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 213
    invoke-virtual {v0}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    move v13, v2

    move-object v2, v1

    move-object v1, v0

    move v0, v13

    goto :goto_7

    :cond_8
    move v3, v7

    move v2, v7

    .line 173
    :goto_8
    if-ge v3, v5, :cond_e

    .line 175
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->f(Lsoftware/simplicial/nebulous/views/b/p$a;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v3

    if-nez v0, :cond_9

    move v0, v2

    .line 173
    :goto_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_8

    .line 178
    :cond_9
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->g(Lsoftware/simplicial/nebulous/views/b/p$a;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->f(Lsoftware/simplicial/nebulous/views/b/p$a;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->h(Lsoftware/simplicial/nebulous/views/b/p$a;)[[B

    move-result-object v1

    aget-object v1, v1, v3

    invoke-static {v0, v1}, Lsoftware/simplicial/nebulous/f/aa;->a(Ljava/lang/String;[B)Ljava/lang/CharSequence;

    move-result-object v0

    .line 179
    :goto_a
    const-string v1, ""

    .line 180
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->i(Lsoftware/simplicial/nebulous/views/b/p$a;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->j(Lsoftware/simplicial/nebulous/views/b/p$a;)Z

    move-result v4

    if-nez v4, :cond_a

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->k(Lsoftware/simplicial/nebulous/views/b/p$a;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 181
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->l(Lsoftware/simplicial/nebulous/views/b/p$a;)[B

    move-result-object v4

    aget-byte v4, v4, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 182
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v4

    aget v4, v4, v3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 183
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/CharSequence;

    aput-object v0, v4, v7

    const-string v0, ": "

    aput-object v0, v4, v12

    const/4 v0, 0x2

    aput-object v1, v4, v0

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v10, v3

    .line 184
    invoke-static {p1}, Lsoftware/simplicial/nebulous/views/b/p$a;->m(Lsoftware/simplicial/nebulous/views/b/p$a;)I

    move-result v0

    aput v0, v11, v3

    .line 185
    add-int/lit8 v0, v2, 0x1

    goto/16 :goto_9

    .line 178
    :cond_b
    add-int/lit8 v0, v3, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    .line 215
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 216
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    iget v1, p0, Lsoftware/simplicial/nebulous/views/b/p;->i:F

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 197
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_6

    .line 218
    :cond_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->f:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 220
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p;->g:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_e
    move v8, v2

    goto/16 :goto_5

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a([I[I[Lsoftware/simplicial/a/bf;[Lsoftware/simplicial/a/bx;ILsoftware/simplicial/a/am;ZZZZI)V
    .locals 16

    .prologue
    .line 81
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/views/b/p;->j:Lsoftware/simplicial/nebulous/views/b/p$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/p$a;->a(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/views/b/p;->j:Lsoftware/simplicial/nebulous/views/b/p$a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/views/b/p$a;->b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I

    move-result-object v1

    move-object/from16 v0, p2

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 84
    :cond_0
    const/4 v1, 0x5

    new-array v9, v1, [B

    .line 85
    const/4 v1, 0x5

    new-array v6, v1, [Ljava/lang/String;

    .line 86
    const/4 v1, 0x5

    new-array v7, v1, [[B

    .line 87
    const/4 v1, 0x5

    new-array v8, v1, [B

    .line 89
    if-lez p5, :cond_3

    .line 91
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_6

    .line 93
    aget v2, p1, v1

    .line 94
    if-ltz v2, :cond_1

    add-int/lit8 v3, p5, -0x1

    if-le v2, v3, :cond_2

    .line 91
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 97
    :cond_2
    aget-object v2, p4, v2

    .line 98
    iget v2, v2, Lsoftware/simplicial/a/bx;->d:I

    int-to-byte v2, v2

    aput-byte v2, v9, v1

    goto :goto_2

    .line 103
    :cond_3
    const/4 v1, 0x0

    :goto_3
    const/4 v2, 0x5

    if-ge v1, v2, :cond_6

    .line 105
    aget v2, p1, v1

    .line 107
    if-ltz v2, :cond_4

    move-object/from16 v0, p3

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-le v2, v3, :cond_5

    .line 109
    :cond_4
    const/4 v2, 0x0

    aput-object v2, v6, v1

    .line 103
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 114
    :cond_5
    aget-object v2, p3, v2

    .line 115
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->D:Ljava/lang/String;

    aput-object v3, v6, v1

    .line 116
    iget-object v3, v2, Lsoftware/simplicial/a/bf;->E:[B

    aput-object v3, v7, v1

    .line 117
    iget-byte v2, v2, Lsoftware/simplicial/a/bf;->R:B

    aput-byte v2, v8, v1

    goto :goto_4

    .line 122
    :cond_6
    new-instance v1, Lsoftware/simplicial/nebulous/views/b/p$a;

    move-object/from16 v0, p1

    array-length v2, v0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v4

    move-object/from16 v0, p2

    array-length v2, v0

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v5

    move-object/from16 v2, p0

    move/from16 v3, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    move/from16 v15, p11

    invoke-direct/range {v1 .. v15}, Lsoftware/simplicial/nebulous/views/b/p$a;-><init>(Lsoftware/simplicial/nebulous/views/b/p;I[I[I[Ljava/lang/String;[[B[B[BLsoftware/simplicial/a/am;ZZZZI)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lsoftware/simplicial/nebulous/views/b/p;->j:Lsoftware/simplicial/nebulous/views/b/p$a;

    .line 124
    move-object/from16 v0, p0

    iget-object v1, v0, Lsoftware/simplicial/nebulous/views/b/p;->j:Lsoftware/simplicial/nebulous/views/b/p$a;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/b/p;->a(Ljava/lang/Object;Z)V

    goto :goto_0
.end method
