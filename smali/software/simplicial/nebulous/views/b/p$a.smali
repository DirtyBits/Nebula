.class Lsoftware/simplicial/nebulous/views/b/p$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/views/b/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/views/b/p;

.field private final b:[I

.field private final c:[I

.field private final d:I

.field private final e:[Ljava/lang/String;

.field private final f:[[B

.field private final g:[B

.field private final h:[B

.field private final i:Lsoftware/simplicial/a/am;

.field private final j:Z

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:I


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/views/b/p;I[I[I[Ljava/lang/String;[[B[B[BLsoftware/simplicial/a/am;ZZZZI)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->a:Lsoftware/simplicial/nebulous/views/b/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p2, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->d:I

    .line 51
    iput-object p3, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->b:[I

    .line 52
    iput-object p4, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->c:[I

    .line 53
    iput-object p5, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->e:[Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->f:[[B

    .line 55
    iput-object p7, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->g:[B

    .line 56
    iput-object p8, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->h:[B

    .line 57
    iput-object p9, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->i:Lsoftware/simplicial/a/am;

    .line 58
    iput-boolean p10, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->j:Z

    .line 59
    iput-boolean p11, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->k:Z

    .line 60
    iput-boolean p12, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->l:Z

    .line 61
    iput-boolean p13, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->m:Z

    .line 62
    iput p14, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->n:I

    .line 63
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/views/b/p$a;)[I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->b:[I

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/views/b/p$a;)[I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->c:[I

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/views/b/p$a;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->d:I

    return v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/views/b/p$a;)Lsoftware/simplicial/a/am;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->i:Lsoftware/simplicial/a/am;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/views/b/p$a;)[B
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->h:[B

    return-object v0
.end method

.method static synthetic f(Lsoftware/simplicial/nebulous/views/b/p$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->e:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lsoftware/simplicial/nebulous/views/b/p$a;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->j:Z

    return v0
.end method

.method static synthetic h(Lsoftware/simplicial/nebulous/views/b/p$a;)[[B
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->f:[[B

    return-object v0
.end method

.method static synthetic i(Lsoftware/simplicial/nebulous/views/b/p$a;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->k:Z

    return v0
.end method

.method static synthetic j(Lsoftware/simplicial/nebulous/views/b/p$a;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->l:Z

    return v0
.end method

.method static synthetic k(Lsoftware/simplicial/nebulous/views/b/p$a;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->m:Z

    return v0
.end method

.method static synthetic l(Lsoftware/simplicial/nebulous/views/b/p$a;)[B
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->g:[B

    return-object v0
.end method

.method static synthetic m(Lsoftware/simplicial/nebulous/views/b/p$a;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lsoftware/simplicial/nebulous/views/b/p$a;->n:I

    return v0
.end method
