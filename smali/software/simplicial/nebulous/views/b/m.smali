.class public Lsoftware/simplicial/nebulous/views/b/m;
.super Lsoftware/simplicial/nebulous/views/b/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lsoftware/simplicial/nebulous/views/b/b",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(ILandroid/content/res/Resources;Z)V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Lsoftware/simplicial/nebulous/views/b/b;-><init>()V

    .line 14
    iput p1, p0, Lsoftware/simplicial/nebulous/views/b/m;->b:I

    .line 15
    iput-object p2, p0, Lsoftware/simplicial/nebulous/views/b/m;->c:Landroid/content/res/Resources;

    .line 17
    if-eqz p3, :cond_0

    .line 18
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lsoftware/simplicial/nebulous/views/b/m;->a(Ljava/lang/Object;Z)V

    .line 19
    :cond_0
    return-void
.end method


# virtual methods
.method protected declared-synchronized a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 24
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 25
    const/4 v1, 0x1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 26
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 27
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 28
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 30
    iget-object v1, p0, Lsoftware/simplicial/nebulous/views/b/m;->c:Landroid/content/res/Resources;

    iget v2, p0, Lsoftware/simplicial/nebulous/views/b/m;->b:I

    invoke-static {v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
