.class final enum Lsoftware/simplicial/nebulous/views/GameView$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/views/GameView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lsoftware/simplicial/nebulous/views/GameView$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lsoftware/simplicial/nebulous/views/GameView$a;

.field public static final enum b:Lsoftware/simplicial/nebulous/views/GameView$a;

.field public static final enum c:Lsoftware/simplicial/nebulous/views/GameView$a;

.field public static final enum d:Lsoftware/simplicial/nebulous/views/GameView$a;

.field private static final synthetic e:[Lsoftware/simplicial/nebulous/views/GameView$a;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 742
    new-instance v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lsoftware/simplicial/nebulous/views/GameView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->a:Lsoftware/simplicial/nebulous/views/GameView$a;

    new-instance v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    const-string v1, "ZOOMING_TO_WINNER"

    invoke-direct {v0, v1, v3}, Lsoftware/simplicial/nebulous/views/GameView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->b:Lsoftware/simplicial/nebulous/views/GameView$a;

    new-instance v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    const-string v1, "SITTING_AT_WINNER"

    invoke-direct {v0, v1, v4}, Lsoftware/simplicial/nebulous/views/GameView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->c:Lsoftware/simplicial/nebulous/views/GameView$a;

    new-instance v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    const-string v1, "ZOOMING_OUT"

    invoke-direct {v0, v1, v5}, Lsoftware/simplicial/nebulous/views/GameView$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->d:Lsoftware/simplicial/nebulous/views/GameView$a;

    .line 740
    const/4 v0, 0x4

    new-array v0, v0, [Lsoftware/simplicial/nebulous/views/GameView$a;

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView$a;->a:Lsoftware/simplicial/nebulous/views/GameView$a;

    aput-object v1, v0, v2

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView$a;->b:Lsoftware/simplicial/nebulous/views/GameView$a;

    aput-object v1, v0, v3

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView$a;->c:Lsoftware/simplicial/nebulous/views/GameView$a;

    aput-object v1, v0, v4

    sget-object v1, Lsoftware/simplicial/nebulous/views/GameView$a;->d:Lsoftware/simplicial/nebulous/views/GameView$a;

    aput-object v1, v0, v5

    sput-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->e:[Lsoftware/simplicial/nebulous/views/GameView$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 740
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lsoftware/simplicial/nebulous/views/GameView$a;
    .locals 1

    .prologue
    .line 740
    const-class v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lsoftware/simplicial/nebulous/views/GameView$a;

    return-object v0
.end method

.method public static values()[Lsoftware/simplicial/nebulous/views/GameView$a;
    .locals 1

    .prologue
    .line 740
    sget-object v0, Lsoftware/simplicial/nebulous/views/GameView$a;->e:[Lsoftware/simplicial/nebulous/views/GameView$a;

    invoke-virtual {v0}, [Lsoftware/simplicial/nebulous/views/GameView$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsoftware/simplicial/nebulous/views/GameView$a;

    return-object v0
.end method
