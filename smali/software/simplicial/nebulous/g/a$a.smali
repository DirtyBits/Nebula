.class Lsoftware/simplicial/nebulous/g/a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsoftware/simplicial/nebulous/g/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lsoftware/simplicial/nebulous/g/a;

.field private b:Z

.field private c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lsoftware/simplicial/nebulous/g/d;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lsoftware/simplicial/nebulous/g/a;)V
    .locals 1

    .prologue
    .line 238
    iput-object p1, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    .line 234
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->c:Ljava/util/Queue;

    .line 239
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->d:Ljava/lang/Thread;

    .line 240
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 241
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    .line 245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    .line 246
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 247
    if-lez p1, :cond_0

    .line 251
    :try_start_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->d:Ljava/lang/Thread;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->d:Ljava/lang/Thread;

    .line 259
    return-void

    .line 253
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lsoftware/simplicial/nebulous/g/d;)V
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    if-eqz v0, :cond_0

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public run()V
    .locals 13

    .prologue
    const/high16 v12, 0x40a00000    # 5.0f

    const v8, 0x3eaaaaab

    const/4 v7, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    .line 272
    :goto_0
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    if-nez v0, :cond_1b

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 274
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 276
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lsoftware/simplicial/nebulous/g/d;

    .line 277
    if-nez v4, :cond_2

    .line 494
    :cond_1
    const-wide/16 v0, 0x10

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 496
    :catch_0
    move-exception v0

    goto :goto_0

    .line 280
    :cond_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/g/a;->a(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    iget-object v1, v1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 281
    iget-object v1, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    iget-object v1, v1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    if-ne v1, v2, :cond_a

    .line 283
    iget-object v1, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    check-cast v1, Lsoftware/simplicial/a/a/v;

    .line 284
    iget-byte v2, v1, Lsoftware/simplicial/a/a/v;->a:B

    sget-object v3, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    invoke-virtual {v3}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v2, v2, Lsoftware/simplicial/nebulous/g/a;->a:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v3, v3, Lsoftware/simplicial/nebulous/g/a;->b:I

    if-ne v2, v3, :cond_3

    .line 285
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 286
    :cond_3
    iget-byte v1, v1, Lsoftware/simplicial/a/a/v;->a:B

    sget-object v2, Lsoftware/simplicial/a/bv;->o:Lsoftware/simplicial/a/bv;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_6

    .line 288
    iget-object v1, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v1}, Lsoftware/simplicial/nebulous/g/a;->b(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Random;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 289
    if-nez v1, :cond_4

    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v2, v2, Lsoftware/simplicial/nebulous/g/a;->u:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v3, v3, Lsoftware/simplicial/nebulous/g/a;->v:I

    if-ne v2, v3, :cond_4

    .line 290
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->v:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 291
    :cond_4
    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v2, v2, Lsoftware/simplicial/nebulous/g/a;->w:I

    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v3, v3, Lsoftware/simplicial/nebulous/g/a;->x:I

    if-ne v2, v3, :cond_5

    .line 292
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->x:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 293
    :cond_5
    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v1, v1, Lsoftware/simplicial/nebulous/g/a;->y:I

    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v2, v2, Lsoftware/simplicial/nebulous/g/a;->z:I

    if-ne v1, v2, :cond_6

    .line 294
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->z:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_6
    move-object v5, v0

    .line 321
    :goto_2
    if-eqz v5, :cond_0

    .line 324
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/g/a;->c(Lsoftware/simplicial/nebulous/g/a;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v3, v0

    .line 325
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/g/a;->c(Lsoftware/simplicial/nebulous/g/a;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    int-to-float v9, v0

    .line 330
    iget v0, v4, Lsoftware/simplicial/nebulous/g/d;->b:F

    iget v1, v4, Lsoftware/simplicial/nebulous/g/d;->d:F

    sub-float/2addr v0, v1

    iget v1, v4, Lsoftware/simplicial/nebulous/g/d;->g:F

    div-float/2addr v0, v1

    .line 333
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 336
    const/high16 v1, -0x40800000    # -1.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_14

    move v1, v7

    .line 342
    :goto_3
    sget-object v0, Lsoftware/simplicial/nebulous/g/a$1;->a:[I

    iget-object v2, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    iget-object v2, v2, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-virtual {v2}, Lsoftware/simplicial/a/a/y$a;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_7
    move v0, v7

    move v2, v7

    .line 479
    :cond_8
    :goto_4
    const/high16 v10, 0x3f000000    # 0.5f

    sub-float v1, v10, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v12

    .line 480
    add-float v10, v1, v6

    div-float v10, v6, v10

    mul-float/2addr v10, v3

    div-float/2addr v10, v9

    mul-float/2addr v2, v10

    .line 481
    add-float/2addr v1, v6

    div-float v1, v6, v1

    mul-float/2addr v1, v3

    div-float/2addr v1, v9

    mul-float v3, v0, v1

    .line 486
    cmpl-float v0, v2, v7

    if-nez v0, :cond_9

    cmpl-float v0, v3, v7

    if-eqz v0, :cond_0

    .line 489
    :cond_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/g/a;->e(Lsoftware/simplicial/nebulous/g/a;)Landroid/media/SoundPool;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v5, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v5}, Lsoftware/simplicial/nebulous/g/a;->d(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Map;

    move-result-object v5

    iget-object v4, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    iget-object v4, v4, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/SoundPool;->play(IFFIIF)I

    goto/16 :goto_1

    .line 297
    :cond_a
    iget-object v1, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    iget-object v1, v1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    if-ne v1, v2, :cond_1c

    .line 299
    iget-object v1, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    check-cast v1, Lsoftware/simplicial/a/a/e;

    .line 300
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_b

    .line 301
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 302
    :cond_b
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_c

    .line 303
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 304
    :cond_c
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_d

    .line 305
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 306
    :cond_d
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_e

    .line 307
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 308
    :cond_e
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_f

    .line 309
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->l:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 310
    :cond_f
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_10

    .line 311
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->n:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 312
    :cond_10
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_11

    .line 313
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 314
    :cond_11
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_12

    .line 315
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 316
    :cond_12
    iget-short v2, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_13

    .line 317
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    invoke-static {v0}, Lsoftware/simplicial/nebulous/g/a;->a(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Map;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->G:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 318
    :cond_13
    iget-short v1, v1, Lsoftware/simplicial/a/a/e;->c:S

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_1c

    .line 319
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->a:Lsoftware/simplicial/nebulous/g/a;

    iget v0, v0, Lsoftware/simplicial/nebulous/g/a;->t:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v5, v0

    goto/16 :goto_2

    .line 338
    :cond_14
    cmpl-float v1, v0, v6

    if-lez v1, :cond_15

    move v1, v6

    .line 339
    goto/16 :goto_3

    .line 341
    :cond_15
    add-float/2addr v0, v6

    div-float/2addr v0, v11

    move v1, v0

    goto/16 :goto_3

    .line 345
    :pswitch_0
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 346
    mul-float v0, v11, v1

    .line 347
    goto/16 :goto_4

    .line 349
    :pswitch_1
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 350
    mul-float v0, v11, v1

    .line 351
    goto/16 :goto_4

    .line 353
    :pswitch_2
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 354
    mul-float v0, v11, v1

    .line 355
    goto/16 :goto_4

    .line 357
    :pswitch_3
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 358
    mul-float v0, v11, v1

    .line 359
    goto/16 :goto_4

    .line 361
    :pswitch_4
    const/high16 v0, 0x447a0000    # 1000.0f

    iget v2, v4, Lsoftware/simplicial/nebulous/g/d;->f:F

    sub-float/2addr v2, v12

    add-float/2addr v2, v6

    div-float v2, v0, v2

    .line 362
    const/high16 v0, 0x447a0000    # 1000.0f

    iget v10, v4, Lsoftware/simplicial/nebulous/g/d;->f:F

    sub-float/2addr v10, v12

    add-float/2addr v10, v6

    div-float/2addr v0, v10

    .line 363
    cmpl-float v10, v2, v6

    if-lez v10, :cond_16

    move v2, v6

    .line 364
    :cond_16
    cmpl-float v10, v0, v6

    if-lez v10, :cond_17

    move v0, v6

    .line 365
    :cond_17
    cmpg-float v10, v2, v8

    if-gez v10, :cond_18

    move v2, v8

    .line 366
    :cond_18
    cmpg-float v10, v0, v8

    if-gez v10, :cond_8

    move v0, v8

    goto/16 :goto_4

    .line 369
    :pswitch_5
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 370
    mul-float v0, v11, v1

    .line 371
    goto/16 :goto_4

    .line 373
    :pswitch_6
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 374
    mul-float v0, v11, v1

    .line 375
    goto/16 :goto_4

    .line 377
    :pswitch_7
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 378
    mul-float v0, v11, v1

    .line 379
    goto/16 :goto_4

    .line 381
    :pswitch_8
    sub-float v0, v6, v1

    mul-float v2, v11, v0

    .line 382
    mul-float v0, v11, v1

    .line 383
    goto/16 :goto_4

    :pswitch_9
    move v0, v6

    move v2, v6

    .line 387
    goto/16 :goto_4

    :pswitch_a
    move v0, v6

    move v2, v6

    .line 391
    goto/16 :goto_4

    :pswitch_b
    move v0, v6

    move v2, v6

    .line 395
    goto/16 :goto_4

    :pswitch_c
    move v0, v6

    move v2, v6

    .line 399
    goto/16 :goto_4

    :pswitch_d
    move v0, v6

    move v2, v6

    .line 403
    goto/16 :goto_4

    :pswitch_e
    move v0, v6

    move v2, v6

    .line 407
    goto/16 :goto_4

    .line 409
    :pswitch_f
    const/high16 v2, 0x3f400000    # 0.75f

    .line 410
    const/high16 v0, 0x3f400000    # 0.75f

    .line 411
    goto/16 :goto_4

    .line 413
    :pswitch_10
    iget-object v0, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    check-cast v0, Lsoftware/simplicial/a/a/v;

    .line 414
    iget-byte v2, v0, Lsoftware/simplicial/a/a/v;->a:B

    sget-object v10, Lsoftware/simplicial/a/bv;->f:Lsoftware/simplicial/a/bv;

    invoke-virtual {v10}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v10

    if-ne v2, v10, :cond_19

    .line 416
    const v2, 0x3f0ccccd    # 0.55f

    .line 417
    const v0, 0x3f0ccccd    # 0.55f

    goto/16 :goto_4

    .line 419
    :cond_19
    iget-byte v0, v0, Lsoftware/simplicial/a/a/v;->a:B

    sget-object v2, Lsoftware/simplicial/a/bv;->o:Lsoftware/simplicial/a/bv;

    invoke-virtual {v2}, Lsoftware/simplicial/a/bv;->ordinal()I

    move-result v2

    if-ne v0, v2, :cond_1a

    .line 421
    const v2, 0x3eb33333    # 0.35f

    .line 422
    const v0, 0x3eb33333    # 0.35f

    goto/16 :goto_4

    :cond_1a
    move v0, v6

    move v2, v6

    .line 429
    goto/16 :goto_4

    :pswitch_11
    move v0, v6

    move v2, v6

    .line 433
    goto/16 :goto_4

    :pswitch_12
    move v0, v6

    move v2, v6

    .line 437
    goto/16 :goto_4

    :pswitch_13
    move v0, v6

    move v2, v6

    .line 441
    goto/16 :goto_4

    :pswitch_14
    move v0, v6

    move v2, v6

    .line 445
    goto/16 :goto_4

    :pswitch_15
    move v0, v6

    move v2, v6

    .line 449
    goto/16 :goto_4

    :pswitch_16
    move v0, v6

    move v2, v6

    .line 453
    goto/16 :goto_4

    :pswitch_17
    move v0, v6

    move v2, v6

    .line 457
    goto/16 :goto_4

    :pswitch_18
    move v0, v6

    move v2, v6

    .line 461
    goto/16 :goto_4

    .line 463
    :pswitch_19
    iget-object v0, v4, Lsoftware/simplicial/nebulous/g/d;->a:Lsoftware/simplicial/a/a/y;

    check-cast v0, Lsoftware/simplicial/a/a/b;

    .line 464
    iget-byte v0, v0, Lsoftware/simplicial/a/a/b;->a:B

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    move v0, v6

    move v2, v6

    .line 467
    goto/16 :goto_4

    :pswitch_1a
    move v0, v6

    move v2, v6

    .line 473
    goto/16 :goto_4

    :pswitch_1b
    move v0, v6

    move v2, v6

    .line 476
    goto/16 :goto_4

    .line 502
    :cond_1b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->b:Z

    .line 503
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a$a;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 504
    return-void

    :cond_1c
    move-object v5, v0

    goto/16 :goto_2

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method
