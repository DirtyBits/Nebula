.class public abstract Lsoftware/simplicial/nebulous/g/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/SoundPool$OnLoadCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsoftware/simplicial/nebulous/g/a$a;
    }
.end annotation


# instance fields
.field private final A:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lsoftware/simplicial/a/a/y$a;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/a/y$a;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lsoftware/simplicial/a/a/y$a;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Ljava/util/Random;

.field private E:Landroid/media/SoundPool;

.field private F:Landroid/media/AudioManager;

.field private G:Lsoftware/simplicial/nebulous/g/a$a;

.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:I

.field q:I

.field r:I

.field s:I

.field t:I

.field u:I

.field v:I

.field w:I

.field x:I

.field y:I

.field z:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v1, -0x1

    const/16 v4, 0x8

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->B:Ljava/util/Map;

    .line 33
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    .line 34
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->D:Ljava/util/Random;

    .line 35
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->a:I

    .line 36
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->b:I

    .line 37
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->c:I

    .line 38
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->d:I

    .line 39
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->e:I

    .line 40
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->f:I

    .line 41
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->g:I

    .line 42
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->h:I

    .line 43
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->i:I

    .line 44
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->j:I

    .line 45
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->k:I

    .line 46
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->l:I

    .line 47
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->m:I

    .line 48
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->n:I

    .line 49
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->o:I

    .line 50
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->p:I

    .line 51
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->q:I

    .line 52
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->r:I

    .line 53
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->s:I

    .line 54
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->t:I

    .line 55
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->u:I

    .line 56
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->v:I

    .line 57
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->w:I

    .line 58
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->x:I

    .line 59
    iput v5, p0, Lsoftware/simplicial/nebulous/g/a;->y:I

    .line 60
    iput v1, p0, Lsoftware/simplicial/nebulous/g/a;->z:I

    .line 68
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->b:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->v:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->g:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->d:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->i:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->G:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->c:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->f:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->h:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->H:Lsoftware/simplicial/a/a/y$a;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->n:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->l:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->m:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->k:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->D:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->B:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->A:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->C:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->o:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->t:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->y:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->z:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->I:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->J:Lsoftware/simplicial/a/a/y$a;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v1, Lsoftware/simplicial/a/a/y$a;->j:Lsoftware/simplicial/a/a/y$a;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method static synthetic a(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->B:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Random;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->D:Ljava/util/Random;

    return-object v0
.end method

.method static synthetic c(Lsoftware/simplicial/nebulous/g/a;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->F:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic d(Lsoftware/simplicial/nebulous/g/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Lsoftware/simplicial/nebulous/g/a;)Landroid/media/SoundPool;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    return-object v0
.end method


# virtual methods
.method protected abstract a()Landroid/media/SoundPool;
.end method

.method public a(Landroid/content/Context;)V
    .locals 11

    .prologue
    const v3, 0x7f060014

    const v4, 0x7f060003

    const v5, 0x7f060002

    const v6, 0x7f060001

    const/high16 v7, 0x7f060000

    .line 100
    invoke-virtual {p0}, Lsoftware/simplicial/nebulous/g/a;->a()Landroid/media/SoundPool;

    move-result-object v0

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    .line 101
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    invoke-virtual {v0, p0}, Landroid/media/SoundPool;->setOnLoadCompleteListener(Landroid/media/SoundPool$OnLoadCompleteListener;)V

    .line 102
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->F:Landroid/media/AudioManager;

    .line 104
    const/4 v0, 0x0

    .line 105
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_29

    .line 107
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->F:Landroid/media/AudioManager;

    const-string v1, "android.media.property.OUTPUT_SAMPLE_RATE"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 108
    if-eqz v0, :cond_1

    const-string v1, "48"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 111
    :goto_1
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_2

    const v0, 0x7f060013

    move v2, v0

    :goto_2
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->b:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->b:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_3

    const v0, 0x7f060011

    move v2, v0

    :goto_3
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->c:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->c:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_4

    const v0, 0x7f060011

    move v2, v0

    :goto_4
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->v:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->v:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_5

    const v0, 0x7f060017

    move v2, v0

    :goto_5
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_6

    const v0, 0x7f060007

    move v2, v0

    :goto_6
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_7

    const v0, 0x7f060009

    move v2, v0

    :goto_7
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->f:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->f:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_8

    const v0, 0x7f060027

    move v2, v0

    :goto_8
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->i:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->i:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_9

    const v0, 0x7f060015

    move v2, v0

    :goto_9
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->d:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->d:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_a

    const v0, 0x7f060019

    move v2, v0

    :goto_a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->g:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->g:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_b

    const v0, 0x7f060031

    move v2, v0

    :goto_b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->h:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->h:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_c

    const v0, 0x7f06001d

    move v2, v0

    :goto_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->l:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->l:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_d

    move v2, v4

    :goto_d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->n:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->n:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_e

    move v2, v4

    :goto_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->m:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->m:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_f

    const v0, 0x7f060015

    move v2, v0

    :goto_f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->k:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->k:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_10

    const v0, 0x7f06001d

    move v2, v0

    :goto_10
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->D:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->D:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v9, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_11

    const v0, 0x7f06001d

    move v2, v0

    :goto_11
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v10, Lsoftware/simplicial/a/a/y$a;->B:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v9, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->B:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v8, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_12

    :goto_12
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->A:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v8, p1, v4, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->A:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_0

    const v3, 0x7f060015

    :cond_0
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->C:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v3, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lsoftware/simplicial/a/a/y$a;->C:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_13

    move v2, v6

    :goto_13
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->o:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->o:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_14

    move v2, v6

    :goto_14
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->t:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->t:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_15

    move v2, v6

    :goto_15
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->y:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->y:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_16

    move v2, v6

    :goto_16
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->z:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->z:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_17

    move v2, v6

    :goto_17
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->I:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->I:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_18

    :goto_18
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->J:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v6, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lsoftware/simplicial/a/a/y$a;->J:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_19

    const v0, 0x7f060035

    move v2, v0

    :goto_19
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->j:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->j:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1a

    const v0, 0x7f060033

    move v2, v0

    :goto_1a
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->G:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->G:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    iget-object v4, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1b

    const v0, 0x7f06002d

    move v2, v0

    :goto_1b
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v5, Lsoftware/simplicial/a/a/y$a;->H:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lsoftware/simplicial/a/a/y$a;->H:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1c

    const v0, 0x7f06000f

    move v2, v0

    :goto_1c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->a:I

    .line 141
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->b:I

    .line 142
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1d

    const v0, 0x7f06000b

    move v2, v0

    :goto_1d
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->c:I

    .line 143
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->d:I

    .line 144
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1e

    const v0, 0x7f060037

    move v2, v0

    :goto_1e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->i:I

    .line 145
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->j:I

    .line 146
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_1f

    const v0, 0x7f060025

    move v2, v0

    :goto_1f
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->e:I

    .line 147
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->f:I

    .line 148
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_20

    const v0, 0x7f06001b

    move v2, v0

    :goto_20
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->g:I

    .line 149
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->h:I

    .line 150
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_21

    const v0, 0x7f06002f

    move v2, v0

    :goto_21
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->k:I

    .line 151
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->l:I

    .line 152
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_22

    const v0, 0x7f06002b

    move v2, v0

    :goto_22
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->m:I

    .line 153
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->n:I

    .line 154
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_23

    const v0, 0x7f06000d

    move v2, v0

    :goto_23
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->o:I

    .line 155
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->o:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->p:I

    .line 156
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_24

    const v0, 0x7f060029

    move v2, v0

    :goto_24
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->q:I

    .line 157
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->r:I

    .line 158
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_25

    const v0, 0x7f060005

    move v2, v0

    :goto_25
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->F:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->s:I

    .line 159
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->s:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->t:I

    .line 161
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_26

    const v0, 0x7f06001f

    move v2, v0

    :goto_26
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->u:I

    .line 162
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->v:I

    .line 163
    iget-object v3, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_27

    const v0, 0x7f060021

    move v2, v0

    :goto_27
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v4, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, p1, v2, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->w:I

    .line 164
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->w:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->x:I

    .line 165
    iget-object v2, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    if-eqz v1, :cond_28

    const v0, 0x7f060023

    move v1, v0

    :goto_28
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    sget-object v3, Lsoftware/simplicial/a/a/y$a;->w:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, p1, v1, v0}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->y:I

    .line 166
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->y:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lsoftware/simplicial/nebulous/g/a;->z:I

    .line 168
    new-instance v0, Lsoftware/simplicial/nebulous/g/a$a;

    invoke-direct {v0, p0}, Lsoftware/simplicial/nebulous/g/a$a;-><init>(Lsoftware/simplicial/nebulous/g/a;)V

    iput-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->G:Lsoftware/simplicial/nebulous/g/a$a;

    .line 169
    return-void

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 111
    :cond_2
    const v0, 0x7f060012

    move v2, v0

    goto/16 :goto_2

    .line 112
    :cond_3
    const v0, 0x7f060010

    move v2, v0

    goto/16 :goto_3

    .line 113
    :cond_4
    const v0, 0x7f060010

    move v2, v0

    goto/16 :goto_4

    .line 114
    :cond_5
    const v0, 0x7f060016

    move v2, v0

    goto/16 :goto_5

    .line 115
    :cond_6
    const v0, 0x7f060006

    move v2, v0

    goto/16 :goto_6

    .line 116
    :cond_7
    const v0, 0x7f060008

    move v2, v0

    goto/16 :goto_7

    .line 117
    :cond_8
    const v0, 0x7f060026

    move v2, v0

    goto/16 :goto_8

    :cond_9
    move v2, v3

    .line 118
    goto/16 :goto_9

    .line 119
    :cond_a
    const v0, 0x7f060018

    move v2, v0

    goto/16 :goto_a

    .line 120
    :cond_b
    const v0, 0x7f060030

    move v2, v0

    goto/16 :goto_b

    .line 121
    :cond_c
    const v0, 0x7f06001c

    move v2, v0

    goto/16 :goto_c

    :cond_d
    move v2, v5

    .line 122
    goto/16 :goto_d

    :cond_e
    move v2, v5

    .line 123
    goto/16 :goto_e

    :cond_f
    move v2, v3

    .line 124
    goto/16 :goto_f

    .line 125
    :cond_10
    const v0, 0x7f06001c

    move v2, v0

    goto/16 :goto_10

    .line 126
    :cond_11
    const v0, 0x7f06001c

    move v2, v0

    goto/16 :goto_11

    :cond_12
    move v4, v5

    .line 127
    goto/16 :goto_12

    :cond_13
    move v2, v7

    .line 129
    goto/16 :goto_13

    :cond_14
    move v2, v7

    .line 130
    goto/16 :goto_14

    :cond_15
    move v2, v7

    .line 131
    goto/16 :goto_15

    :cond_16
    move v2, v7

    .line 132
    goto/16 :goto_16

    :cond_17
    move v2, v7

    .line 133
    goto/16 :goto_17

    :cond_18
    move v6, v7

    .line 134
    goto/16 :goto_18

    .line 135
    :cond_19
    const v0, 0x7f060034

    move v2, v0

    goto/16 :goto_19

    .line 137
    :cond_1a
    const v0, 0x7f060032

    move v2, v0

    goto/16 :goto_1a

    .line 138
    :cond_1b
    const v0, 0x7f06002c

    move v2, v0

    goto/16 :goto_1b

    .line 140
    :cond_1c
    const v0, 0x7f06000e

    move v2, v0

    goto/16 :goto_1c

    .line 142
    :cond_1d
    const v0, 0x7f06000a

    move v2, v0

    goto/16 :goto_1d

    .line 144
    :cond_1e
    const v0, 0x7f060036

    move v2, v0

    goto/16 :goto_1e

    .line 146
    :cond_1f
    const v0, 0x7f060024

    move v2, v0

    goto/16 :goto_1f

    .line 148
    :cond_20
    const v0, 0x7f06001a

    move v2, v0

    goto/16 :goto_20

    .line 150
    :cond_21
    const v0, 0x7f06002e

    move v2, v0

    goto/16 :goto_21

    .line 152
    :cond_22
    const v0, 0x7f06002a

    move v2, v0

    goto/16 :goto_22

    .line 154
    :cond_23
    const v0, 0x7f06000c

    move v2, v0

    goto/16 :goto_23

    .line 156
    :cond_24
    const v0, 0x7f060028

    move v2, v0

    goto/16 :goto_24

    .line 158
    :cond_25
    const v0, 0x7f060004

    move v2, v0

    goto/16 :goto_25

    .line 161
    :cond_26
    const v0, 0x7f06001e

    move v2, v0

    goto/16 :goto_26

    .line 163
    :cond_27
    const v0, 0x7f060020

    move v2, v0

    goto/16 :goto_27

    .line 165
    :cond_28
    const v0, 0x7f060022

    move v1, v0

    goto/16 :goto_28

    :cond_29
    move v1, v0

    goto/16 :goto_1
.end method

.method public a(Lsoftware/simplicial/a/a/y;FFFFFF)V
    .locals 9

    .prologue
    .line 175
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->C:Ljava/util/Map;

    iget-object v1, p1, Lsoftware/simplicial/a/a/y;->e:Lsoftware/simplicial/a/a/y$a;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v8, p0, Lsoftware/simplicial/nebulous/g/a;->G:Lsoftware/simplicial/nebulous/g/a$a;

    new-instance v0, Lsoftware/simplicial/nebulous/g/d;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lsoftware/simplicial/nebulous/g/d;-><init>(Lsoftware/simplicial/a/a/y;FFFFFF)V

    invoke-virtual {v8, v0}, Lsoftware/simplicial/nebulous/g/a$a;->a(Lsoftware/simplicial/nebulous/g/d;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->G:Lsoftware/simplicial/nebulous/g/a$a;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lsoftware/simplicial/nebulous/g/a$a;->a(I)V

    .line 193
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->E:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 194
    return-void
.end method

.method public onLoadComplete(Landroid/media/SoundPool;II)V
    .locals 3

    .prologue
    .line 199
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->a:I

    if-ne p2, v0, :cond_0

    .line 200
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->b:I

    .line 201
    :cond_0
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->c:I

    if-ne p2, v0, :cond_1

    .line 202
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->d:I

    .line 203
    :cond_1
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->i:I

    if-ne p2, v0, :cond_2

    .line 204
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->j:I

    .line 205
    :cond_2
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->g:I

    if-ne p2, v0, :cond_3

    .line 206
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->h:I

    .line 207
    :cond_3
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->e:I

    if-ne p2, v0, :cond_4

    .line 208
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->f:I

    .line 209
    :cond_4
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->k:I

    if-ne p2, v0, :cond_5

    .line 210
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->l:I

    .line 211
    :cond_5
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->m:I

    if-ne p2, v0, :cond_6

    .line 212
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->n:I

    .line 213
    :cond_6
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->o:I

    if-ne p2, v0, :cond_7

    .line 214
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->p:I

    .line 215
    :cond_7
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->q:I

    if-ne p2, v0, :cond_8

    .line 216
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->r:I

    .line 217
    :cond_8
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->u:I

    if-ne p2, v0, :cond_9

    .line 218
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->v:I

    .line 219
    :cond_9
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->w:I

    if-ne p2, v0, :cond_a

    .line 220
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->x:I

    .line 221
    :cond_a
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->y:I

    if-ne p2, v0, :cond_b

    .line 222
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->z:I

    .line 223
    :cond_b
    iget v0, p0, Lsoftware/simplicial/nebulous/g/a;->s:I

    if-ne p2, v0, :cond_c

    .line 224
    iput p2, p0, Lsoftware/simplicial/nebulous/g/a;->t:I

    .line 226
    :cond_c
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->B:Ljava/util/Map;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 229
    :cond_d
    :goto_0
    return-void

    .line 228
    :cond_e
    iget-object v0, p0, Lsoftware/simplicial/nebulous/g/a;->B:Ljava/util/Map;

    iget-object v1, p0, Lsoftware/simplicial/nebulous/g/a;->A:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
