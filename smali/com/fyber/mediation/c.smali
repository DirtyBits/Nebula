.class public abstract Lcom/fyber/mediation/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/fyber/g/a/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/g/a/a/j",
            "<",
            "Lcom/fyber/ads/videos/a;",
            "Lcom/fyber/e/b;",
            "Lcom/fyber/ads/videos/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 183
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "TT;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 203
    invoke-static {p0, p1, p3}, Lcom/fyber/mediation/c;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 204
    if-nez v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    move-object p2, v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final a(Landroid/content/Context;Lcom/fyber/ads/videos/a/a;)Ljava/util/concurrent/Future;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/fyber/ads/videos/a/a;",
            ")",
            "Ljava/util/concurrent/Future",
            "<",
            "Lcom/fyber/ads/videos/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/fyber/mediation/c;->a:Lcom/fyber/g/a/a/j;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/fyber/g/a/a/j;

    invoke-virtual {p0}, Lcom/fyber/mediation/c;->c()Lcom/fyber/ads/videos/b/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/fyber/g/a/a/j;-><init>(Lcom/fyber/mediation/e;)V

    iput-object v0, p0, Lcom/fyber/mediation/c;->a:Lcom/fyber/g/a/a/j;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/fyber/mediation/c;->a:Lcom/fyber/g/a/a/j;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/g/a/a/j;->a(Landroid/content/Context;Lcom/fyber/ads/b/a;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->d()Lcom/fyber/ads/interstitials/c/a;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0, p1}, Lcom/fyber/ads/interstitials/c/a;->b(Landroid/app/Activity;)V

    .line 134
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/fyber/ads/videos/b/d;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/fyber/ads/videos/b/d;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->c()Lcom/fyber/ads/videos/b/a;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->c()Lcom/fyber/ads/videos/b/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/fyber/ads/videos/b/a;->a(Landroid/app/Activity;Lcom/fyber/ads/videos/b/d;Ljava/util/Map;)V

    .line 113
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/app/Activity;Ljava/util/Map;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation
.end method

.method public final a(Lcom/fyber/ads/b;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 82
    sget-object v2, Lcom/fyber/mediation/c$1;->a:[I

    invoke-virtual {p1}, Lcom/fyber/ads/b;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 84
    :pswitch_0
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->c()Lcom/fyber/ads/videos/b/a;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->d()Lcom/fyber/ads/interstitials/c/a;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 88
    :pswitch_2
    invoke-virtual {p0}, Lcom/fyber/mediation/c;->e()Lcom/fyber/ads/a/b/a;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method protected abstract c()Lcom/fyber/ads/videos/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/videos/b/a",
            "<+",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract d()Lcom/fyber/ads/interstitials/c/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/interstitials/c/a",
            "<+",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract e()Lcom/fyber/ads/a/b/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fyber/ads/a/b/a",
            "<+",
            "Lcom/fyber/mediation/c;",
            ">;"
        }
    .end annotation
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->g()Lcom/fyber/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a/a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
