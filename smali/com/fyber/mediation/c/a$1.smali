.class Lcom/fyber/mediation/c/a$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/fyber/mediation/c/a;->a(Landroid/app/Activity;Ljava/util/Map;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/fyber/mediation/c/a;


# direct methods
.method constructor <init>(Lcom/fyber/mediation/c/a;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    iput-object p2, p0, Lcom/fyber/mediation/c/a$1;->a:Landroid/app/Activity;

    iput-object p3, p0, Lcom/fyber/mediation/c/a$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 78
    invoke-static {}, Lcom/vungle/publisher/VunglePub;->getInstance()Lcom/vungle/publisher/VunglePub;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/fyber/mediation/c/a$1;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/fyber/mediation/c/a$1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/vungle/publisher/VunglePub;->init(Landroid/content/Context;Ljava/lang/String;)Z

    .line 81
    iget-object v1, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    new-instance v2, Lcom/fyber/mediation/c/b/a;

    iget-object v3, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    invoke-static {v3}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/a;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/fyber/mediation/c/b/a;-><init>(Lcom/fyber/mediation/c/a;)V

    invoke-static {v1, v2}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;Lcom/fyber/mediation/c/b/a;)Lcom/fyber/mediation/c/b/a;

    .line 82
    iget-object v1, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    new-instance v2, Lcom/fyber/mediation/c/a/a;

    iget-object v3, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    invoke-static {v3}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/a;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/fyber/mediation/c/a/a;-><init>(Lcom/fyber/mediation/c/a;)V

    invoke-static {v1, v2}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;Lcom/fyber/mediation/c/a/a;)Lcom/fyber/mediation/c/a/a;

    .line 84
    const/4 v1, 0x2

    new-array v1, v1, [Lcom/vungle/publisher/EventListener;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    invoke-static {v3}, Lcom/fyber/mediation/c/a;->b(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/b/a;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    invoke-static {v3}, Lcom/fyber/mediation/c/a;->c(Lcom/fyber/mediation/c/a;)Lcom/fyber/mediation/c/a/a;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/vungle/publisher/VunglePub;->setEventListeners([Lcom/vungle/publisher/EventListener;)V

    .line 86
    iget-object v1, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    invoke-virtual {v0}, Lcom/vungle/publisher/VunglePub;->getGlobalAdConfig()Lcom/vungle/publisher/AdConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;Lcom/vungle/publisher/AdConfig;)V

    .line 88
    invoke-virtual {v0}, Lcom/vungle/publisher/VunglePub;->onResume()V

    .line 89
    iget-object v0, p0, Lcom/fyber/mediation/c/a$1;->c:Lcom/fyber/mediation/c/a;

    iget-object v1, p0, Lcom/fyber/mediation/c/a$1;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/mediation/c/a;->a(Lcom/fyber/mediation/c/a;Landroid/app/Application;)V

    .line 90
    return-void
.end method
