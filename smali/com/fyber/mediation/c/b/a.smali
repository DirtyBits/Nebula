.class public Lcom/fyber/mediation/c/b/a;
.super Lcom/fyber/ads/videos/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/vungle/publisher/EventListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/ads/videos/b/a",
        "<",
        "Lcom/fyber/mediation/c/a;",
        ">;",
        "Lcom/vungle/publisher/EventListener;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/fyber/mediation/c/a;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/b/a;-><init>(Lcom/fyber/mediation/c;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/vungle/publisher/VunglePub;->getInstance()Lcom/vungle/publisher/VunglePub;

    move-result-object v1

    .line 34
    invoke-virtual {v1}, Lcom/vungle/publisher/VunglePub;->isAdPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/fyber/mediation/c/b/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->a(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {v1}, Lcom/vungle/publisher/VunglePub;->playAd()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/mediation/c/b/a;->e()V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/vungle/publisher/VunglePub;->getInstance()Lcom/vungle/publisher/VunglePub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vungle/publisher/VunglePub;->isAdPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fyber/ads/videos/b/c;->g:Lcom/fyber/ads/videos/b/c;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/fyber/mediation/c/b/a;->a(Lcom/fyber/ads/videos/b/c;)V

    .line 29
    return-void

    .line 27
    :cond_0
    sget-object v0, Lcom/fyber/ads/videos/b/c;->b:Lcom/fyber/ads/videos/b/c;

    goto :goto_0
.end method

.method public onAdEnd(ZZ)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/fyber/mediation/c/b/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    if-eqz p1, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/fyber/mediation/c/b/a;->b()V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/fyber/mediation/c/b/a;->d()V

    goto :goto_0
.end method

.method public onAdPlayableChanged(Z)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public onAdStart()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/fyber/mediation/c/b/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/fyber/mediation/c/b/a;->c()V

    .line 74
    :cond_0
    return-void
.end method

.method public onAdUnavailable(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public onVideoView(ZII)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
