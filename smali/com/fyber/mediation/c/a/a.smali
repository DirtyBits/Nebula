.class public Lcom/fyber/mediation/c/a/a;
.super Lcom/fyber/ads/interstitials/c/a;
.source "SourceFile"

# interfaces
.implements Lcom/vungle/publisher/EventListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/ads/interstitials/c/a",
        "<",
        "Lcom/fyber/mediation/c/a;",
        ">;",
        "Lcom/vungle/publisher/EventListener;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/fyber/mediation/c/a;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/fyber/ads/interstitials/c/a;-><init>(Lcom/fyber/mediation/c;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 19
    invoke-static {}, Lcom/vungle/publisher/VunglePub;->getInstance()Lcom/vungle/publisher/VunglePub;

    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lcom/vungle/publisher/VunglePub;->isAdPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/fyber/mediation/c/a/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->a(Ljava/lang/Object;)V

    .line 24
    new-instance v0, Lcom/vungle/publisher/AdConfig;

    invoke-direct {v0}, Lcom/vungle/publisher/AdConfig;-><init>()V

    .line 25
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/vungle/publisher/AdConfig;->setIncentivized(Z)V

    .line 27
    invoke-virtual {v1, v0}, Lcom/vungle/publisher/VunglePub;->playAd(Lcom/vungle/publisher/AdConfig;)V

    .line 31
    :goto_0
    return-void

    .line 29
    :cond_0
    const-string v0, "Interstitial is not ready"

    invoke-virtual {p0, v0}, Lcom/fyber/mediation/c/a/a;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/vungle/publisher/VunglePub;->getInstance()Lcom/vungle/publisher/VunglePub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vungle/publisher/VunglePub;->isAdPlayable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a/a;->c()V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a/a;->d()V

    goto :goto_0
.end method

.method public onAdEnd(ZZ)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/fyber/mediation/c/a/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a/a;->f()V

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a/a;->g()V

    .line 59
    :cond_1
    return-void
.end method

.method public onAdPlayableChanged(Z)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public onAdStart()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/fyber/mediation/c/a/a;->a:Lcom/fyber/mediation/c;

    check-cast v0, Lcom/fyber/mediation/c/a;

    invoke-virtual {v0, p0}, Lcom/fyber/mediation/c/a;->b(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/fyber/mediation/c/a/a;->e()V

    .line 69
    :cond_0
    return-void
.end method

.method public onAdUnavailable(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public onVideoView(ZII)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
