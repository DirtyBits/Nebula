.class public Lcom/fyber/mediation/b/b/a;
.super Lcom/fyber/ads/videos/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/unity3d/ads/IUnityAdsListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/ads/videos/b/a",
        "<",
        "Lcom/fyber/mediation/b/a;",
        ">;",
        "Lcom/unity3d/ads/IUnityAdsListener;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/fyber/mediation/b/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/fyber/mediation/b/b/a;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/fyber/mediation/b/a;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/b/a;-><init>(Lcom/fyber/mediation/c;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/unity3d/ads/UnityAds;->isReady(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/unity3d/ads/UnityAds;->show(Landroid/app/Activity;Ljava/lang/String;)V

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->e()V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/unity3d/ads/UnityAds;->isReady(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    sget-object v0, Lcom/fyber/ads/videos/b/c;->g:Lcom/fyber/ads/videos/b/c;

    invoke-virtual {p0, v0}, Lcom/fyber/mediation/b/b/a;->a(Lcom/fyber/ads/videos/b/c;)V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    sget-object v0, Lcom/fyber/ads/videos/b/c;->b:Lcom/fyber/ads/videos/b/c;

    invoke-virtual {p0, v0}, Lcom/fyber/mediation/b/b/a;->a(Lcom/fyber/ads/videos/b/c;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public onUnityAdsError(Lcom/unity3d/ads/UnityAds$UnityAdsError;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    sget-object v0, Lcom/fyber/mediation/b/b/a;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UnityAds error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void
.end method

.method public onUnityAdsFinish(Ljava/lang/String;Lcom/unity3d/ads/UnityAds$FinishState;)V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    sget-object v0, Lcom/fyber/mediation/b/b/a$1;->a:[I

    invoke-virtual {p2}, Lcom/unity3d/ads/UnityAds$FinishState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 74
    :pswitch_0
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->b()V

    .line 75
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->d()V

    goto :goto_0

    .line 78
    :pswitch_1
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->d()V

    goto :goto_0

    .line 81
    :pswitch_2
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->e()V

    goto :goto_0

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onUnityAdsReady(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onUnityAdsStart(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/fyber/mediation/b/b/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/fyber/mediation/b/b/a;->c()V

    .line 67
    :cond_0
    return-void
.end method
