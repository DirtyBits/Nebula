.class public final Lcom/fyber/a$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# static fields
.field public static a:Lcom/fyber/a$b;

.field static final b:Landroid/os/Handler;


# instance fields
.field private final c:Lcom/fyber/a$a;

.field private final d:Lcom/fyber/utils/m;

.field private final e:Lcom/fyber/b/c;

.field private f:Lcom/fyber/a/a;

.field private g:Lcom/fyber/a/a$a;

.field private h:Lcom/fyber/g/a/a/l;

.field private i:Lcom/fyber/g/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 533
    new-instance v0, Lcom/fyber/a$b;

    invoke-direct {v0}, Lcom/fyber/a$b;-><init>()V

    sput-object v0, Lcom/fyber/a$b;->a:Lcom/fyber/a$b;

    .line 535
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/fyber/a$b;->b:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 548
    sget-object v0, Lcom/fyber/a$a;->a:Lcom/fyber/a$a;

    iput-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    .line 549
    iput-object v1, p0, Lcom/fyber/a$b;->d:Lcom/fyber/utils/m;

    .line 550
    iput-object v1, p0, Lcom/fyber/a$b;->e:Lcom/fyber/b/c;

    .line 551
    sget-object v0, Lcom/fyber/a/a;->a:Lcom/fyber/a/a;

    iput-object v0, p0, Lcom/fyber/a$b;->f:Lcom/fyber/a/a;

    .line 552
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 555
    invoke-static {}, Lcom/fyber/utils/m;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    invoke-static {p2}, Lcom/fyber/utils/k;->a(Landroid/content/Context;)V

    .line 559
    new-instance v0, Lcom/fyber/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/fyber/a$a;-><init>(B)V

    iput-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    .line 560
    new-instance v0, Lcom/fyber/b/c;

    invoke-direct {v0}, Lcom/fyber/b/c;-><init>()V

    iput-object v0, p0, Lcom/fyber/a$b;->e:Lcom/fyber/b/c;

    .line 563
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/fyber/utils/r;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    invoke-static {p2}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 566
    :cond_0
    new-instance v0, Lcom/fyber/utils/a/b;

    invoke-direct {v0, p2}, Lcom/fyber/utils/a/b;-><init>(Landroid/content/Context;)V

    .line 567
    invoke-static {v0}, Lcom/fyber/utils/n;->a(Ljava/net/CookieStore;)V

    .line 568
    new-instance v0, Lcom/fyber/g/a/a/l$a;

    invoke-direct {v0}, Lcom/fyber/g/a/a/l$a;-><init>()V

    invoke-virtual {v0}, Lcom/fyber/g/a/a/l$a;->a()Lcom/fyber/g/a/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/a$b;->h:Lcom/fyber/g/a/a/l;

    .line 569
    new-instance v0, Lcom/fyber/g/a/k;

    invoke-direct {v0, p2}, Lcom/fyber/g/a/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/fyber/a$b;->i:Lcom/fyber/g/a/k;

    .line 580
    :goto_0
    sget-object v0, Lcom/fyber/a/a;->a:Lcom/fyber/a/a;

    iput-object v0, p0, Lcom/fyber/a$b;->f:Lcom/fyber/a/a;

    .line 581
    new-instance v0, Lcom/fyber/a/a$a;

    invoke-direct {v0, p1}, Lcom/fyber/a/a$a;-><init>(Ljava/lang/String;)V

    .line 582
    invoke-static {p2}, Lcom/fyber/utils/ab;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/a/a$a;->b(Ljava/lang/String;)Lcom/fyber/a/a$a;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/a$b;->g:Lcom/fyber/a/a$a;

    .line 583
    invoke-static {p2}, Lcom/fyber/utils/m;->a(Landroid/content/Context;)Lcom/fyber/utils/m;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/a$b;->d:Lcom/fyber/utils/m;

    .line 585
    return-void

    .line 572
    :cond_1
    invoke-static {}, Lcom/fyber/utils/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 573
    const-string v0, "Fyber"

    sget-object v1, Lcom/fyber/g/d;->a:Lcom/fyber/g/d;

    invoke-virtual {v1}, Lcom/fyber/g/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :goto_1
    sget-object v0, Lcom/fyber/a$a;->a:Lcom/fyber/a$a;

    iput-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    .line 578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/a$b;->e:Lcom/fyber/b/c;

    goto :goto_0

    .line 575
    :cond_2
    const-string v0, "Fyber"

    sget-object v1, Lcom/fyber/g/d;->a:Lcom/fyber/g/d;

    invoke-virtual {v1}, Lcom/fyber/g/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 529
    invoke-direct {p0, p1, p2}, Lcom/fyber/a$b;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/fyber/a$b;)Lcom/fyber/a/a$a;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/fyber/a$b;->g:Lcom/fyber/a/a$a;

    return-object v0
.end method

.method static synthetic a(Lcom/fyber/a$b;Lcom/fyber/a/a;)Lcom/fyber/a/a;
    .locals 0

    .prologue
    .line 529
    iput-object p1, p0, Lcom/fyber/a$b;->f:Lcom/fyber/a/a;

    return-object p1
.end method

.method static synthetic b(Lcom/fyber/a$b;)Lcom/fyber/a$a;
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    return-object v0
.end method

.method public static b(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 638
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 639
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 643
    :goto_0
    return-void

    .line 641
    :cond_0
    sget-object v0, Lcom/fyber/a$b;->b:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/fyber/utils/m;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/fyber/a$b;->d:Lcom/fyber/utils/m;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 624
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 634
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 624
    :sswitch_0
    const-string v1, "CLOSE_ON_REDIRECT"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "NOTIFY_USER_ON_REWARD"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "SHOULD_NOTIFY_ON_USER_ENGAGED"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 626
    :pswitch_0
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    invoke-static {v0}, Lcom/fyber/a$a;->b(Lcom/fyber/a$a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 628
    :pswitch_1
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    invoke-static {v0}, Lcom/fyber/a$a;->c(Lcom/fyber/a$a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 630
    :pswitch_2
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    invoke-static {v0}, Lcom/fyber/a$a;->d(Lcom/fyber/a$a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 624
    :sswitch_data_0
    .sparse-switch
        -0x6ba605ab -> :sswitch_0
        -0x44c2e5fb -> :sswitch_2
        0x531d1d1 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/Future",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 608
    iget-object v0, p0, Lcom/fyber/a$b;->e:Lcom/fyber/b/c;

    invoke-virtual {v0, p1}, Lcom/fyber/b/c;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/fyber/a$b;->e:Lcom/fyber/b/c;

    invoke-virtual {v0, p1}, Lcom/fyber/b/c;->execute(Ljava/lang/Runnable;)V

    .line 613
    return-void
.end method

.method public final b()Lcom/fyber/a$a;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    return-object v0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 596
    iget-object v0, p0, Lcom/fyber/a$b;->c:Lcom/fyber/a$a;

    invoke-static {v0}, Lcom/fyber/a$a;->a(Lcom/fyber/a$a;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/fyber/g/a/a/l;
    .locals 1

    .prologue
    .line 600
    iget-object v0, p0, Lcom/fyber/a$b;->h:Lcom/fyber/g/a/a/l;

    return-object v0
.end method

.method public final e()Lcom/fyber/g/a/k;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/fyber/a$b;->i:Lcom/fyber/g/a/k;

    return-object v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/fyber/a$b;->f:Lcom/fyber/a/a;

    sget-object v1, Lcom/fyber/a/a;->a:Lcom/fyber/a/a;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/fyber/a/a;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/fyber/a$b;->f:Lcom/fyber/a/a;

    return-object v0
.end method
