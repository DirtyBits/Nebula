.class public final Lcom/fyber/b/a/a$a;
.super Lcom/fyber/b/b$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/b/a/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/b/b$a",
        "<",
        "Lcom/fyber/b/a/a;",
        "Lcom/fyber/b/a/a$a;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/fyber/ads/b/d;Lcom/fyber/g/a/a/d;)V
    .locals 6

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/fyber/ads/b/d;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v1

    const-string v2, "TRACKING_URL_KEY"

    invoke-virtual {v1, v2}, Lcom/fyber/g/a/j;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/fyber/b/b$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->d()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string v0, "no_fill"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/fyber/b/a/a$a;->b(Ljava/lang/String;)Lcom/fyber/b/b$a;

    .line 35
    iget-object v0, p0, Lcom/fyber/b/a/a$a;->c:Lcom/fyber/utils/y;

    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/g/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/utils/y;->a(Ljava/lang/String;)Lcom/fyber/utils/y;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 37
    const-string v1, "cache_age"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string v1, "cache_result"

    const-string v2, "hit"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string v1, "ttl"

    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v2

    const-string v3, "CACHE_TTL"

    invoke-virtual {v2, v3}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v1, "hits"

    invoke-virtual {p2}, Lcom/fyber/g/a/a/d;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {p0, v0}, Lcom/fyber/b/a/a$a;->a(Ljava/util/Map;)Lcom/fyber/b/b$a;

    .line 43
    return-void

    .line 34
    :cond_0
    const-string v0, "fill"

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/fyber/b/a/a;
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/fyber/b/b$a;->n_()Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/a/a;

    return-object v0
.end method

.method protected final bridge synthetic d()Lcom/fyber/b/b$a;
    .locals 0

    .prologue
    .line 30
    return-object p0
.end method

.method protected final synthetic m_()Lcom/fyber/b/b;
    .locals 2

    .prologue
    .line 30
    .line 3047
    new-instance v0, Lcom/fyber/b/a/a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/fyber/b/a/a;-><init>(Lcom/fyber/b/b$a;B)V

    .line 30
    return-object v0
.end method

.method public final bridge synthetic n_()Lcom/fyber/b/b;
    .locals 1

    .prologue
    .line 30
    .line 1058
    invoke-super {p0}, Lcom/fyber/b/b$a;->n_()Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/a/a;

    .line 30
    return-object v0
.end method
