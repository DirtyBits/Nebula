.class public final Lcom/fyber/b/i;
.super Lcom/fyber/b/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fyber/b/i$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/b/h",
        "<",
        "Lcom/fyber/b/i$a;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Lcom/fyber/d/a/a;


# instance fields
.field private e:Lcom/fyber/g/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/g/a/e",
            "<",
            "Lcom/fyber/d/b;",
            "Lcom/fyber/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/fyber/g/a/j;

.field private g:Landroid/content/Context;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/fyber/d/a/a;

    invoke-direct {v0}, Lcom/fyber/d/a/a;-><init>()V

    sput-object v0, Lcom/fyber/b/i;->d:Lcom/fyber/d/a/a;

    .line 43
    return-void
.end method

.method private constructor <init>(Lcom/fyber/b/i;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p1, Lcom/fyber/b/i;->c:Lcom/fyber/utils/y;

    iget-object v1, p1, Lcom/fyber/b/i;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/fyber/b/h;-><init>(Lcom/fyber/utils/y;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/b/i;->h:Z

    .line 69
    iget-object v0, p1, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    iput-object v0, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    .line 70
    iget-object v0, p1, Lcom/fyber/b/i;->g:Landroid/content/Context;

    iput-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    .line 72
    new-instance v0, Lcom/fyber/g/a/j;

    iget-object v1, p1, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    invoke-direct {v0, v1}, Lcom/fyber/g/a/j;-><init>(Lcom/fyber/g/a/j;)V

    iget-object v1, p1, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    invoke-virtual {v1}, Lcom/fyber/g/a/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->c(Ljava/lang/String;)Lcom/fyber/g/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fyber/b/i;->h:Z

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/fyber/g/a/j;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/fyber/g/a/j;->d()Lcom/fyber/g/a/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/g/a/l;->c()Lcom/fyber/utils/y;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/fyber/b/h;-><init>(Lcom/fyber/utils/y;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/b/i;->h:Z

    .line 78
    iput-object p1, p0, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    .line 79
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    .line 80
    return-void
.end method

.method static synthetic a(Lcom/fyber/b/i;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Lcom/fyber/b/i$a;
    .locals 8

    .prologue
    .line 141
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 142
    const-string v1, "delta_of_coins"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 143
    const-string v1, "latest_transaction_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 144
    const-string v1, "currency_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 145
    const-string v1, "currency_name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 146
    const-string v1, "is_default"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    .line 148
    new-instance v1, Lcom/fyber/d/b;

    invoke-direct/range {v1 .. v7}, Lcom/fyber/d/b;-><init>(DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-object v1

    .line 149
    :catch_0
    move-exception v0

    .line 150
    sget-object v2, Lcom/fyber/d/a$a;->a:Lcom/fyber/d/a$a;

    .line 151
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 153
    new-instance v1, Lcom/fyber/d/a;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lcom/fyber/d/a;-><init>(Lcom/fyber/d/a$a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    const-string v1, "CURRENCY_ID"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Lcom/fyber/d/a;
    .locals 4

    .prologue
    .line 161
    const/4 v1, 0x0

    .line 163
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 164
    const-string v0, "code"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 165
    :try_start_1
    const-string v1, "message"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 166
    sget-object v1, Lcom/fyber/d/a$a;->c:Lcom/fyber/d/a$a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 173
    :goto_0
    new-instance v3, Lcom/fyber/d/a;

    invoke-direct {v3, v1, v0, v2}, Lcom/fyber/d/a;-><init>(Lcom/fyber/d/a$a;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    .line 167
    :catch_0
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 168
    :goto_1
    const-string v1, "VirtualCurrencyNetworkOperation"

    const-string v3, "An exception was triggered while parsing error response"

    invoke-static {v1, v3, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 170
    sget-object v1, Lcom/fyber/d/a$a;->d:Lcom/fyber/d/a$a;

    .line 171
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 167
    :catch_1
    move-exception v1

    move-object v2, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/e;)Lcom/fyber/b/i;
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    .line 84
    return-object p0
.end method

.method protected final synthetic a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 32
    .line 2126
    invoke-static {p1}, Lcom/fyber/b/i;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2127
    invoke-static {p2}, Lcom/fyber/b/i;->c(Ljava/lang/String;)Lcom/fyber/d/a;

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    .line 2129
    :cond_0
    new-instance v0, Lcom/fyber/d/a;

    sget-object v1, Lcom/fyber/d/a$a;->b:Lcom/fyber/d/a$a;

    const/4 v2, 0x0

    const-string v3, "The signature received in the request did not match the expected one"

    invoke-direct {v0, v1, v2, v3}, Lcom/fyber/d/a;-><init>(Lcom/fyber/d/a$a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 32
    check-cast p1, Lcom/fyber/b/i$a;

    .line 3096
    instance-of v0, p1, Lcom/fyber/d/b;

    if-eqz v0, :cond_5

    .line 3098
    check-cast p1, Lcom/fyber/d/b;

    .line 3229
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/fyber/d/a/b;->a(Landroid/content/Context;)Lcom/fyber/d/a/b;

    move-result-object v1

    .line 3230
    invoke-virtual {v1}, Lcom/fyber/d/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 3231
    invoke-virtual {p1}, Lcom/fyber/d/b;->c()Ljava/lang/String;

    move-result-object v4

    .line 3255
    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3257
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3259
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v9

    .line 3234
    :goto_0
    if-eqz v0, :cond_3

    .line 3236
    invoke-virtual {v1, v4}, Lcom/fyber/d/a/b;->a(Ljava/lang/String;)V

    .line 3238
    invoke-virtual {v1, v4}, Lcom/fyber/d/a/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3241
    new-instance v1, Lcom/fyber/b/i;

    invoke-direct {v1, p0}, Lcom/fyber/b/i;-><init>(Lcom/fyber/b/i;)V

    .line 3242
    iget-object v4, v1, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    const-string v5, "TRANSACTION_ID"

    invoke-virtual {v4, v5, v0}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    move-result-object v0

    const-string v4, "currency_id"

    .line 3243
    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    move-result-object v0

    .line 3244
    invoke-virtual {v0}, Lcom/fyber/g/a/j;->e()Lcom/fyber/g/a/j;

    .line 3246
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/fyber/a$b;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move v0, v8

    .line 3099
    :goto_1
    if-eqz v0, :cond_1

    .line 3269
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/fyber/d/a/b;->a(Landroid/content/Context;)Lcom/fyber/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/d/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 3270
    invoke-virtual {p1}, Lcom/fyber/d/b;->c()Ljava/lang/String;

    move-result-object v5

    .line 3271
    new-instance v1, Lcom/fyber/d/b;

    .line 3272
    invoke-virtual {p1}, Lcom/fyber/d/b;->b()Ljava/lang/String;

    move-result-object v4

    .line 3273
    invoke-virtual {p1}, Lcom/fyber/d/b;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/fyber/d/b;->e()Z

    move-result v7

    invoke-direct/range {v1 .. v7}, Lcom/fyber/d/b;-><init>(DLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 3275
    sget-object v4, Lcom/fyber/b/i;->d:Lcom/fyber/d/a/a;

    invoke-virtual {v4, v1, v5, v0}, Lcom/fyber/d/a/a;->a(Lcom/fyber/b/i$a;Ljava/lang/String;Ljava/lang/String;)V

    .line 3103
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/fyber/d/a/b;->a(Landroid/content/Context;)Lcom/fyber/d/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fyber/d/a/b;->a(Lcom/fyber/d/b;)V

    .line 3105
    iget-object v0, p0, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    const-string v1, "NOTIFY_USER_ON_REWARD"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 3106
    invoke-virtual {p1}, Lcom/fyber/d/b;->a()D

    move-result-wide v4

    cmpl-double v1, v4, v2

    if-lez v1, :cond_0

    if-eqz v0, :cond_0

    .line 4214
    invoke-virtual {p1}, Lcom/fyber/d/b;->d()Ljava/lang/String;

    move-result-object v0

    .line 4215
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4218
    :goto_2
    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    sget-object v2, Lcom/fyber/a$a$a;->j:Lcom/fyber/a$a$a;

    invoke-static {v2}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 4219
    invoke-virtual {p1}, Lcom/fyber/d/b;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v8

    aput-object v0, v3, v9

    .line 4218
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 4220
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    new-instance v1, Lcom/fyber/b/i$1;

    invoke-direct {v1, p0, v0}, Lcom/fyber/b/i$1;-><init>(Lcom/fyber/b/i;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/fyber/a$b;->b(Ljava/lang/Runnable;)V

    .line 3110
    :cond_0
    iget-object v0, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    invoke-virtual {v0, p1}, Lcom/fyber/g/a/e;->c(Ljava/lang/Object;)V

    .line 3120
    :cond_1
    :goto_3
    const/4 v0, 0x0

    .line 32
    return-object v0

    :cond_2
    move v0, v8

    .line 3259
    goto/16 :goto_0

    :cond_3
    move v0, v9

    .line 3249
    goto/16 :goto_1

    .line 4215
    :cond_4
    sget-object v0, Lcom/fyber/a$a$a;->k:Lcom/fyber/a$a$a;

    .line 4216
    invoke-static {v0}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 3116
    :cond_5
    check-cast p1, Lcom/fyber/d/a;

    .line 4263
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/fyber/d/a/b;->a(Landroid/content/Context;)Lcom/fyber/d/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/d/a/b;->a()Ljava/lang/String;

    move-result-object v1

    .line 4264
    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    .line 4265
    :goto_4
    sget-object v2, Lcom/fyber/b/i;->d:Lcom/fyber/d/a/a;

    invoke-virtual {v2, p1, v0, v1}, Lcom/fyber/d/a/a;->a(Lcom/fyber/b/i$a;Ljava/lang/String;Ljava/lang/String;)V

    .line 3118
    iget-object v0, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    invoke-virtual {v0, p1}, Lcom/fyber/g/a/e;->d(Ljava/lang/Object;)V

    goto :goto_3

    .line 4264
    :cond_6
    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method protected final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    invoke-static {p1}, Lcom/fyber/b/i;->b(Ljava/lang/String;)Lcom/fyber/b/i$a;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Z
    .locals 4

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/fyber/b/i;->h:Z

    if-eqz v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/fyber/b/i;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/fyber/d/a/b;->a(Landroid/content/Context;)Lcom/fyber/d/a/b;

    move-result-object v1

    .line 180
    invoke-virtual {v1}, Lcom/fyber/d/a/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 182
    sget-object v2, Lcom/fyber/b/i;->d:Lcom/fyber/d/a/a;

    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/fyber/d/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/b/i$a;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_1

    sget-object v2, Lcom/fyber/d/a/a;->a:Lcom/fyber/d/a;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 184
    instance-of v1, v0, Lcom/fyber/d/b;

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    check-cast v0, Lcom/fyber/d/b;

    invoke-virtual {v1, v0}, Lcom/fyber/g/a/e;->c(Ljava/lang/Object;)V

    .line 189
    :goto_0
    const/4 v0, 0x0

    .line 200
    :goto_1
    return v0

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    check-cast v0, Lcom/fyber/d/a;

    invoke-virtual {v1, v0}, Lcom/fyber/g/a/e;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 1284
    :cond_1
    iget-object v0, p0, Lcom/fyber/b/i;->f:Lcom/fyber/g/a/j;

    const-string v2, "TRANSACTION_ID"

    invoke-virtual {v0, v2}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 193
    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 194
    invoke-direct {p0}, Lcom/fyber/b/i;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/fyber/d/a/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_2
    iget-object v1, p0, Lcom/fyber/b/i;->c:Lcom/fyber/utils/y;

    const-string v2, "ltid"

    invoke-virtual {v1, v2, v0}, Lcom/fyber/utils/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/utils/y;

    .line 200
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected final synthetic b(Ljava/io/IOException;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 32
    .line 5089
    iget-object v0, p0, Lcom/fyber/b/i;->e:Lcom/fyber/g/a/e;

    sget-object v1, Lcom/fyber/g/d;->b:Lcom/fyber/g/d;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    .line 5090
    const/4 v0, 0x0

    .line 32
    return-object v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string v0, "VirtualCurrencyNetworkOperation"

    return-object v0
.end method
