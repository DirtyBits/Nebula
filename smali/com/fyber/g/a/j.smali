.class public final Lcom/fyber/g/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field protected c:Z

.field protected d:[I

.field protected e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Lcom/fyber/g/a/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/fyber/g/a/j;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iget-object v0, p1, Lcom/fyber/g/a/j;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/fyber/g/a/j;->a:Ljava/lang/String;

    .line 37
    iget-object v0, p1, Lcom/fyber/g/a/j;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/fyber/g/a/j;->b:Ljava/lang/String;

    .line 38
    iget-boolean v0, p1, Lcom/fyber/g/a/j;->c:Z

    iput-boolean v0, p0, Lcom/fyber/g/a/j;->c:Z

    .line 39
    iget-object v0, p1, Lcom/fyber/g/a/j;->d:[I

    iput-object v0, p0, Lcom/fyber/g/a/j;->d:[I

    .line 42
    iget-object v0, p1, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    invoke-static {v0}, Lcom/fyber/utils/q;->b(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    .line 45
    :cond_0
    return-void
.end method

.method private h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/fyber/g/a/j;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/fyber/g/a/j;->b:Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;
    .locals 1

    .prologue
    .line 123
    invoke-static {p1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/fyber/g/a/j;->h()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/fyber/g/a/j;
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/fyber/g/a/j;->f()Ljava/util/HashMap;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 100
    invoke-direct {p0}, Lcom/fyber/g/a/j;->h()Ljava/util/Map;

    move-result-object v1

    const-string v2, "CUSTOM_PARAMS_KEY"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-object p0
.end method

.method public final a(Z)Lcom/fyber/g/a/j;
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/fyber/g/a/j;->c:Z

    .line 54
    return-object p0
.end method

.method public final varargs a([I)Lcom/fyber/g/a/j;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/fyber/g/a/j;->d:[I

    .line 64
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/fyber/g/a/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/fyber/g/a/j;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/fyber/g/a/j;->a:Ljava/lang/String;

    .line 59
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/fyber/g/a/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/fyber/g/a/j;
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/fyber/g/a/j;->f:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/fyber/g/a/j;->c:Z

    return v0
.end method

.method public final d(Ljava/lang/String;)Lcom/fyber/g/a/j;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/fyber/g/a/j;->f()Ljava/util/HashMap;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_0

    .line 117
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    :cond_0
    return-object p0
.end method

.method public final d()Lcom/fyber/g/a/l;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/fyber/g/a/j;->g:Lcom/fyber/g/a/l;

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/fyber/g/a/j;->e()Lcom/fyber/g/a/j;

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/fyber/g/a/j;->g:Lcom/fyber/g/a/l;

    return-object v0
.end method

.method public final e()Lcom/fyber/g/a/j;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/fyber/g/a/j;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/fyber/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/a$b;->g()Lcom/fyber/a/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/y;->a(Ljava/lang/String;Lcom/fyber/a/a;)Lcom/fyber/utils/y;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/fyber/g/a/l;

    invoke-direct {v1, v0}, Lcom/fyber/g/a/l;-><init>(Lcom/fyber/utils/y;)V

    iput-object v1, p0, Lcom/fyber/g/a/j;->g:Lcom/fyber/g/a/l;

    .line 140
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->e()Lcom/fyber/g/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/g/a/j;->g:Lcom/fyber/g/a/l;

    invoke-virtual {v0, p0, v1}, Lcom/fyber/g/a/k;->a(Lcom/fyber/g/a/j;Lcom/fyber/g/a/l;)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/fyber/g/a/j;->g:Lcom/fyber/g/a/l;

    invoke-virtual {v0}, Lcom/fyber/g/a/l;->b()Lcom/fyber/g/a/l;

    .line 144
    return-object p0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 166
    :cond_0
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fyber/a$b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 174
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method protected final f()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/fyber/g/a/j;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 152
    invoke-direct {p0}, Lcom/fyber/g/a/j;->h()Ljava/util/Map;

    move-result-object v0

    const-string v1, "CUSTOM_PARAMS_KEY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 154
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/fyber/g/a/j;->a:Ljava/lang/String;

    return-object v0
.end method
