.class public final Lcom/fyber/g/a/a/l;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fyber/g/a/a/l$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/fyber/g/a/a/d",
            "<*>;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/fyber/g/a/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/fyber/g/a/a/m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/fyber/g/a/a/l$a;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/fyber/g/a/a/l;->a:Ljava/util/Map;

    .line 32
    iget-object v0, p1, Lcom/fyber/g/a/a/l$a;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/g/a/a/l;->b:Ljava/util/List;

    .line 33
    iget-object v0, p1, Lcom/fyber/g/a/a/l$a;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/g/a/a/l;->c:Ljava/util/List;

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Lcom/fyber/g/a/a/l$a;B)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/fyber/g/a/a/l;-><init>(Lcom/fyber/g/a/a/l$a;)V

    return-void
.end method

.method private a(Ljava/lang/Object;Lcom/fyber/g/a/j;I)Lcom/fyber/g/a/a/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/fyber/g/a/j;",
            "I)",
            "Lcom/fyber/g/a/a/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/fyber/g/a/a/d;

    invoke-direct {v0, p1}, Lcom/fyber/g/a/a/d;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Lcom/fyber/g/a/a/d;->a(Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;

    move-result-object v0

    .line 47
    invoke-virtual {v0, p3}, Lcom/fyber/g/a/a/d;->a(I)Lcom/fyber/g/a/a/d;

    move-result-object v1

    .line 48
    invoke-virtual {p2}, Lcom/fyber/g/a/j;->b()Ljava/lang/String;

    move-result-object v2

    .line 49
    iget-object v0, p0, Lcom/fyber/g/a/a/l;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/a/d;

    .line 50
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->d()I

    move-result v3

    if-ne v3, p3, :cond_0

    .line 51
    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->g()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/fyber/g/a/a/d;->b(I)Lcom/fyber/g/a/a/d;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/fyber/g/a/a/l;->a:Ljava/util/Map;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/fyber/g/a/j;",
            ")",
            "Lcom/fyber/g/a/a/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-virtual {p1}, Lcom/fyber/g/a/j;->b()Ljava/lang/String;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/fyber/g/a/a/l;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/a/d;

    .line 79
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->i()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 80
    const-string v1, "RequestAgent"

    const-string v3, "There\'s a cached response, checking its validity..."

    invoke-static {v1, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    iget-object v1, p0, Lcom/fyber/g/a/a/l;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    .line 1068
    :goto_0
    if-ge v3, v4, :cond_1

    .line 1069
    iget-object v1, p0, Lcom/fyber/g/a/a/l;->b:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/fyber/g/a/a/c;

    invoke-interface {v1, v0, p1}, Lcom/fyber/g/a/a/c;->a(Lcom/fyber/g/a/a/d;Lcom/fyber/g/a/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 81
    :goto_1
    if-eqz v1, :cond_2

    .line 82
    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->f()Lcom/fyber/g/a/a/d;

    .line 83
    const-string v1, "RequestAgent"

    const-string v2, "The response is valid, proceeding..."

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_2
    return-object v0

    .line 1068
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 1073
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    .line 86
    :cond_2
    const-string v1, "RequestAgent"

    const-string v2, "The cached response is not valid anymore"

    invoke-static {v1, v2}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->e()I

    move-result v1

    if-lez v1, :cond_3

    .line 89
    new-instance v1, Lcom/fyber/b/a/a$a;

    sget-object v2, Lcom/fyber/ads/b/d;->a:Lcom/fyber/ads/b/d;

    invoke-direct {v1, v2, v0}, Lcom/fyber/b/a/a$a;-><init>(Lcom/fyber/ads/b/d;Lcom/fyber/g/a/a/d;)V

    invoke-virtual {v1}, Lcom/fyber/b/a/a$a;->a()Lcom/fyber/b/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/b/a/a;->b()V

    .line 91
    :cond_3
    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->h()Lcom/fyber/g/a/a/d;

    .line 94
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/fyber/g/a/j;",
            ")",
            "Lcom/fyber/g/a/a/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 37
    .line 1058
    iget-object v0, p0, Lcom/fyber/g/a/a/l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    .line 1059
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1060
    iget-object v0, p0, Lcom/fyber/g/a/a/l;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/a/m;

    invoke-interface {v0, p1, p2}, Lcom/fyber/g/a/a/m;->a(Ljava/lang/Object;Lcom/fyber/g/a/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1059
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 38
    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lcom/fyber/g/a/a/l;->a(Ljava/lang/Object;Lcom/fyber/g/a/j;I)Lcom/fyber/g/a/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/fyber/g/a/a/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/fyber/g/a/a/d",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "RequestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing entry for cacheKey = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/fyber/g/a/a/l;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/fyber/g/a/a/d;

    .line 104
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fyber/g/a/a/d;->e()I

    move-result v1

    if-lez v1, :cond_0

    .line 105
    new-instance v1, Lcom/fyber/b/a/a$a;

    sget-object v2, Lcom/fyber/ads/b/d;->a:Lcom/fyber/ads/b/d;

    invoke-direct {v1, v2, v0}, Lcom/fyber/b/a/a$a;-><init>(Lcom/fyber/ads/b/d;Lcom/fyber/g/a/a/d;)V

    invoke-virtual {v1}, Lcom/fyber/b/a/a$a;->a()Lcom/fyber/b/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/fyber/b/a/a;->b()V

    .line 107
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/Object;Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/fyber/g/a/j;",
            ")",
            "Lcom/fyber/g/a/a/d",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/fyber/g/a/a/l;->a(Ljava/lang/Object;Lcom/fyber/g/a/j;I)Lcom/fyber/g/a/a/d;

    move-result-object v0

    return-object v0
.end method
