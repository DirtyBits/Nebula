.class public final Lcom/fyber/g/a/a/e$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/g/a/a/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:[I

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/fyber/g/a/a/e$a;->a:I

    .line 53
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/fyber/g/a/a/e$a;->b:[I

    .line 54
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/fyber/g/a/a/e$a;->c:[Ljava/lang/String;

    .line 55
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/fyber/g/a/a/e$a;->d:[Ljava/lang/String;

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/fyber/g/a/a/e$a;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/fyber/g/a/a/e$a;->a:I

    return v0
.end method

.method public static a(Ljava/lang/String;)Lcom/fyber/g/a/a/e$a;
    .locals 3

    .prologue
    .line 61
    invoke-static {p0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/fyber/g/a/a/e$a;->a(Lorg/json/JSONObject;)Lcom/fyber/g/a/a/e$a;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "ContainerCacheConfig"

    const-string v2, "Couldn\'t parse json to retrieve container cache configuration"

    invoke-static {v1, v2, v0}, Lcom/fyber/utils/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 68
    :cond_0
    new-instance v0, Lcom/fyber/g/a/a/e$a;

    invoke-direct {v0}, Lcom/fyber/g/a/a/e$a;-><init>()V

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/fyber/g/a/a/e$a;
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/4 v1, 0x0

    .line 72
    new-instance v4, Lcom/fyber/g/a/a/e$a;

    invoke-direct {v4}, Lcom/fyber/g/a/a/e$a;-><init>()V

    .line 73
    const-string v0, "container_cache_configuration"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 74
    if-eqz v5, :cond_0

    .line 75
    const-string v0, "fill_ttl"

    .line 1086
    invoke-virtual {v5, v0, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 75
    iput v0, v4, Lcom/fyber/g/a/a/e$a;->a:I

    .line 76
    const-string v0, "no_fill_ttl"

    .line 1090
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 1091
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 1092
    :goto_0
    new-array v2, v0, [I

    move v3, v1

    .line 1094
    :goto_1
    if-ge v3, v0, :cond_3

    .line 1095
    invoke-virtual {v6, v3, v8}, Lorg/json/JSONArray;->optInt(II)I

    move-result v7

    .line 1096
    if-gez v7, :cond_2

    .line 1097
    new-array v0, v1, [I

    .line 76
    :goto_2
    iput-object v0, v4, Lcom/fyber/g/a/a/e$a;->b:[I

    .line 77
    const-string v0, "query_white_list"

    invoke-static {v5, v0}, Lcom/fyber/g/a/a/e$a;->a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/fyber/g/a/a/e$a;->c:[Ljava/lang/String;

    .line 78
    const-string v0, "user_data_white_list"

    invoke-static {v5, v0}, Lcom/fyber/g/a/a/e$a;->a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/fyber/g/a/a/e$a;->d:[Ljava/lang/String;

    .line 80
    :cond_0
    return-object v4

    :cond_1
    move v0, v1

    .line 1091
    goto :goto_0

    .line 1100
    :cond_2
    aput v7, v2, v3

    .line 1094
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 108
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 109
    :goto_0
    new-array v2, v0, [Ljava/lang/String;

    move v3, v1

    .line 111
    :goto_1
    if-ge v3, v0, :cond_2

    .line 112
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 113
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 114
    new-array v0, v1, [Ljava/lang/String;

    .line 120
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 108
    goto :goto_0

    .line 117
    :cond_1
    aput-object v5, v2, v3

    .line 111
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/fyber/g/a/a/e$a;)[I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/fyber/g/a/a/e$a;->b:[I

    return-object v0
.end method

.method static synthetic c(Lcom/fyber/g/a/a/e$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/fyber/g/a/a/e$a;->c:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/fyber/g/a/a/e$a;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/fyber/g/a/a/e$a;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/fyber/g/a/a/e;
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/fyber/g/a/a/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/fyber/g/a/a/e;-><init>(Lcom/fyber/g/a/a/e$a;B)V

    return-object v0
.end method
