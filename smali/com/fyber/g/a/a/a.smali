.class public final Lcom/fyber/g/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/g/a/a/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/fyber/g/a/j;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a/j;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    const-string v0, "BANNER_SIZES"

    invoke-virtual {p0, v0}, Lcom/fyber/g/a/j;->e(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 46
    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 49
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/a/d;Lcom/fyber/g/a/j;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->a()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/fyber/ads/a/a/b;

    if-nez v0, :cond_0

    move v0, v1

    .line 41
    :goto_0
    return v0

    .line 28
    :cond_0
    const-string v0, "BannerSizeCacheValidator"

    const-string v3, "Checking banner sizes..."

    invoke-static {v0, v3}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p1}, Lcom/fyber/g/a/a/d;->c()Lcom/fyber/g/a/j;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lcom/fyber/g/a/a/a;->a(Lcom/fyber/g/a/j;)Ljava/util/List;

    move-result-object v0

    .line 32
    invoke-static {p2}, Lcom/fyber/g/a/a/a;->a(Lcom/fyber/g/a/j;)Ljava/util/List;

    move-result-object v3

    .line 34
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_1

    .line 35
    const-string v0, "BannerSizeCacheValidator"

    const-string v1, "The amount of sizes don\'t match for both requests - invalid"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 36
    goto :goto_0

    .line 39
    :cond_1
    invoke-interface {v3, v0}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v3

    .line 40
    const-string v4, "BannerSizeCacheValidator"

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v6, "Banner sizes %smatch for both requests - %s"

    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/Object;

    if-eqz v3, :cond_2

    const-string v0, ""

    :goto_1
    aput-object v0, v7, v2

    if-eqz v3, :cond_3

    const-string v0, "valid. Proceeding..."

    :goto_2
    aput-object v0, v7, v1

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 41
    goto :goto_0

    .line 40
    :cond_2
    const-string v0, "don\'t "

    goto :goto_1

    :cond_3
    const-string v0, "invalid"

    goto :goto_2
.end method
