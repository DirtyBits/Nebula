.class public abstract Lcom/fyber/g/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<U:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lcom/fyber/g/a/j;

.field protected final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fyber/g/a;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Lcom/fyber/g/a;

.field protected d:Landroid/os/Handler;


# direct methods
.method public varargs constructor <init>([Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<+",
            "Lcom/fyber/g/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/fyber/g/a/e;->b:[Ljava/lang/Class;

    .line 27
    return-void
.end method

.method private a(Lcom/fyber/utils/i;)V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/fyber/g/a/e;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/fyber/g/a/e;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 127
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    invoke-static {p1}, Lcom/fyber/a$b;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/fyber/g/a/e;)Lcom/fyber/g/a/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a/e;",
            ")",
            "Lcom/fyber/g/a/e",
            "<TU;TV;>;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p1, Lcom/fyber/g/a/e;->c:Lcom/fyber/g/a;

    iput-object v0, p0, Lcom/fyber/g/a/e;->c:Lcom/fyber/g/a;

    .line 43
    return-object p0
.end method

.method public final a(Lcom/fyber/g/a/j;)Lcom/fyber/g/a/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a/j;",
            ")",
            "Lcom/fyber/g/a/e",
            "<TU;TV;>;"
        }
    .end annotation

    .prologue
    .line 47
    iput-object p1, p0, Lcom/fyber/g/a/e;->a:Lcom/fyber/g/a/j;

    .line 48
    return-object p0
.end method

.method public final a(Lcom/fyber/g/a;)Lcom/fyber/g/a/e;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a;",
            ")",
            "Lcom/fyber/g/a/e",
            "<TU;TV;>;"
        }
    .end annotation

    .prologue
    .line 36
    iput-object p1, p0, Lcom/fyber/g/a/e;->c:Lcom/fyber/g/a;

    .line 37
    return-object p0
.end method

.method public final a(Lcom/fyber/g/a/a/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/g/a/a/d",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Lcom/fyber/g/a/e$4;

    invoke-direct {v0, p0, p1}, Lcom/fyber/g/a/e$4;-><init>(Lcom/fyber/g/a/e;Lcom/fyber/g/a/a/d;)V

    invoke-direct {p0, v0}, Lcom/fyber/g/a/e;->a(Lcom/fyber/utils/i;)V

    .line 103
    return-void
.end method

.method public final a(Lcom/fyber/g/d;)V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/fyber/g/a/e$1;

    invoke-direct {v0, p0, p1}, Lcom/fyber/g/a/e$1;-><init>(Lcom/fyber/g/a/e;Lcom/fyber/g/d;)V

    invoke-direct {p0, v0}, Lcom/fyber/g/a/e;->a(Lcom/fyber/utils/i;)V

    .line 59
    return-void
.end method

.method protected abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Lcom/fyber/g/a/e;->c:Lcom/fyber/g/a;

    if-eqz v1, :cond_0

    .line 107
    iget-object v2, p0, Lcom/fyber/g/a/e;->b:[Ljava/lang/Class;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 108
    iget-object v5, p0, Lcom/fyber/g/a/e;->c:Lcom/fyber/g/a;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 109
    const/4 v0, 0x1

    .line 113
    :cond_0
    return v0

    .line 107
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/fyber/g/a/e;->a:Lcom/fyber/g/a/j;

    invoke-virtual {v0}, Lcom/fyber/g/a/j;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->d()Lcom/fyber/g/a/a/l;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/g/a/e;->a:Lcom/fyber/g/a/j;

    invoke-virtual {v0, p1, v1}, Lcom/fyber/g/a/a/l;->a(Ljava/lang/Object;Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;

    .line 65
    :cond_0
    new-instance v0, Lcom/fyber/g/a/e$2;

    invoke-direct {v0, p0, p1}, Lcom/fyber/g/a/e$2;-><init>(Lcom/fyber/g/a/e;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/fyber/g/a/e;->a(Lcom/fyber/utils/i;)V

    .line 71
    return-void
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/fyber/g/a/e;->a:Lcom/fyber/g/a/j;

    invoke-virtual {v0}, Lcom/fyber/g/a/j;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->d()Lcom/fyber/g/a/a/l;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/g/a/e;->a:Lcom/fyber/g/a/j;

    invoke-virtual {v0, p1, v1}, Lcom/fyber/g/a/a/l;->b(Ljava/lang/Object;Lcom/fyber/g/a/j;)Lcom/fyber/g/a/a/d;

    .line 77
    :cond_0
    new-instance v0, Lcom/fyber/g/a/e$3;

    invoke-direct {v0, p0, p1}, Lcom/fyber/g/a/e$3;-><init>(Lcom/fyber/g/a/e;Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/fyber/g/a/e;->a(Lcom/fyber/utils/i;)V

    .line 83
    return-void
.end method
