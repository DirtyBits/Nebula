.class public Lcom/fyber/g/h;
.super Lcom/fyber/g/e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/g/e",
        "<",
        "Lcom/fyber/g/h;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/fyber/g/e;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/fyber/g/e;-><init>(Lcom/fyber/g/e;)V

    .line 135
    return-void
.end method

.method private constructor <init>(Lcom/fyber/g/g;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/fyber/g/e;-><init>(Lcom/fyber/g/a;)V

    .line 139
    return-void
.end method

.method public static a(Lcom/fyber/g/e;)Lcom/fyber/g/h;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/fyber/g/h;

    invoke-direct {v0, p0}, Lcom/fyber/g/h;-><init>(Lcom/fyber/g/e;)V

    return-object v0
.end method

.method public static a(Lcom/fyber/g/g;)Lcom/fyber/g/h;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/fyber/g/h;

    invoke-direct {v0, p0}, Lcom/fyber/g/h;-><init>(Lcom/fyber/g/g;)V

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/fyber/g/a/e;
    .locals 4

    .prologue
    .line 112
    new-instance v0, Lcom/fyber/g/h$1;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/fyber/g/g;

    aput-object v3, v1, v2

    invoke-direct {v0, p0, v1}, Lcom/fyber/g/h$1;-><init>(Lcom/fyber/g/h;[Ljava/lang/Class;)V

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/fyber/g/a/j;)V
    .locals 2

    .prologue
    .line 96
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->g()Lcom/fyber/a/a;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/fyber/a/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v0, p0, Lcom/fyber/g/h;->a:Lcom/fyber/g/a/e;

    sget-object v1, Lcom/fyber/g/d;->g:Lcom/fyber/g/d;

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/e;->a(Lcom/fyber/g/d;)V

    .line 108
    :goto_0
    return-void

    .line 104
    :cond_0
    new-instance v1, Lcom/fyber/b/i;

    invoke-direct {v1, p2, v0, p1}, Lcom/fyber/b/i;-><init>(Lcom/fyber/g/a/j;Ljava/lang/String;Landroid/content/Context;)V

    iget-object v0, p0, Lcom/fyber/g/h;->a:Lcom/fyber/g/a/e;

    .line 105
    invoke-virtual {v1, v0}, Lcom/fyber/b/i;->a(Lcom/fyber/g/a/e;)Lcom/fyber/b/i;

    move-result-object v0

    .line 107
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/fyber/a$b;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/fyber/g/h;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/fyber/g/h;->b:Lcom/fyber/g/a/j;

    const-string v1, "CURRENCY_ID"

    invoke-virtual {v0, v1, p1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/fyber/g/a/j;

    .line 74
    return-object p0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/fyber/g/h;->b:Lcom/fyber/g/a/j;

    const-string v1, "vcs"

    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Ljava/lang/String;)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x0

    .line 129
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a(Z)Lcom/fyber/g/a/j;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    .line 130
    invoke-virtual {v0, v1}, Lcom/fyber/g/a/j;->a([I)Lcom/fyber/g/a/j;

    .line 131
    return-void

    .line 129
    nop

    :array_0
    .array-data 4
        0x6
        0x5
        0x0
    .end array-data
.end method

.method protected final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 29
    return-object p0
.end method
