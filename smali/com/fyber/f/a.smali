.class public abstract Lcom/fyber/f/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/lang/String;

.field protected b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-static {p1}, Lcom/fyber/utils/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "App id cannot be null nor empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/fyber/f/a;->a:Ljava/lang/String;

    .line 111
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/fyber/utils/y;)Lcom/fyber/utils/y;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 83
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/fyber/utils/r;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    const-string v0, "Only devices running Android API level 10 and above are able to report"

    .line 1124
    invoke-static {}, Lcom/fyber/utils/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1125
    invoke-virtual {p0}, Lcom/fyber/f/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :goto_0
    const/4 v0, 0x0

    .line 103
    :goto_1
    return v0

    .line 1127
    :cond_0
    invoke-virtual {p0}, Lcom/fyber/f/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {p1}, Lcom/fyber/utils/m;->a(Landroid/content/Context;)Lcom/fyber/utils/m;

    .line 91
    invoke-virtual {p0}, Lcom/fyber/f/a;->b()Lcom/fyber/a/a;

    move-result-object v0

    .line 93
    invoke-virtual {p0}, Lcom/fyber/f/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/fyber/utils/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/fyber/utils/y;->a(Ljava/lang/String;Lcom/fyber/a/a;)Lcom/fyber/utils/y;

    move-result-object v0

    iget-object v1, p0, Lcom/fyber/f/a;->b:Ljava/util/Map;

    .line 94
    invoke-virtual {v0, v1}, Lcom/fyber/utils/y;->a(Ljava/util/Map;)Lcom/fyber/utils/y;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/fyber/utils/y;->a()Lcom/fyber/utils/y;

    move-result-object v0

    .line 97
    invoke-virtual {p0, v0}, Lcom/fyber/f/a;->a(Lcom/fyber/utils/y;)Lcom/fyber/utils/y;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/fyber/b/g;

    invoke-virtual {p0}, Lcom/fyber/f/a;->d()Lcom/fyber/f/a/b;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/fyber/b/g;-><init>(Lcom/fyber/utils/y;Lcom/fyber/f/a/b;)V

    .line 101
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 103
    const/4 v0, 0x1

    goto :goto_1
.end method

.method protected abstract b()Lcom/fyber/a/a;
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method protected abstract d()Lcom/fyber/f/a/b;
.end method
