.class public Lcom/fyber/ads/interstitials/InterstitialActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/ads/interstitials/c;


# instance fields
.field private a:Lcom/fyber/mediation/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/fyber/ads/interstitials/a;)V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public a(Lcom/fyber/ads/interstitials/a;Lcom/fyber/ads/interstitials/b;)V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->a(Lcom/fyber/ads/interstitials/b;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public a(Lcom/fyber/ads/interstitials/a;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/fyber/ads/interstitials/b;->e:Lcom/fyber/ads/interstitials/b;

    invoke-virtual {p0, v0, p2}, Lcom/fyber/ads/interstitials/InterstitialActivity;->a(Lcom/fyber/ads/interstitials/b;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method protected a(Lcom/fyber/ads/interstitials/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    const-string v1, "AD_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 133
    invoke-static {p2}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->setResult(ILandroid/content/Intent;)V

    .line 137
    invoke-virtual {p0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->finish()V

    .line 138
    return-void
.end method

.method public a(Lcom/fyber/mediation/d;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    if-nez v0, :cond_0

    .line 66
    iput-object p1, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    .line 68
    :cond_0
    return-void
.end method

.method public b(Lcom/fyber/ads/interstitials/a;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    invoke-interface {v0}, Lcom/fyber/mediation/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->requestWindowFeature(I)Z

    .line 39
    invoke-static {}, Lcom/fyber/ads/interstitials/b/b;->a()Lcom/fyber/ads/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/ads/b/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    invoke-static {}, Lcom/fyber/ads/interstitials/b/b;->b()Lcom/fyber/ads/interstitials/b/a;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0, p0}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/ads/interstitials/c;)V

    .line 43
    invoke-virtual {v0, p0}, Lcom/fyber/ads/interstitials/b/a;->a(Landroid/app/Activity;)V

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    sget-object v0, Lcom/fyber/ads/b/e;->a:Lcom/fyber/ads/b/e;

    invoke-static {v0}, Lcom/fyber/ads/interstitials/b/b;->a(Lcom/fyber/ads/b/e;)Z

    .line 46
    const-string v0, "Unknown internal issue. Please try again later."

    invoke-virtual {p0, v1, v0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->a(Lcom/fyber/ads/interstitials/a;Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_1
    const-string v0, "There\'s no Ad available to be shown currently."

    invoke-virtual {p0, v1, v0}, Lcom/fyber/ads/interstitials/InterstitialActivity;->a(Lcom/fyber/ads/interstitials/a;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    invoke-interface {v0}, Lcom/fyber/mediation/d;->b()V

    .line 58
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 59
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/fyber/ads/interstitials/InterstitialActivity;->a:Lcom/fyber/mediation/d;

    invoke-interface {v0}, Lcom/fyber/mediation/d;->b()V

    .line 78
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 79
    return-void
.end method
