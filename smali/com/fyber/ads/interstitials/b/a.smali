.class public final Lcom/fyber/ads/interstitials/b/a;
.super Lcom/fyber/ads/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/ads/b/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fyber/ads/b/a",
        "<",
        "Lcom/fyber/ads/interstitials/b/a;",
        "Lcom/fyber/ads/interstitials/a;",
        ">;",
        "Lcom/fyber/ads/b/c",
        "<",
        "Lcom/fyber/ads/interstitials/c;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Lcom/fyber/ads/interstitials/c;

.field private d:Lcom/fyber/ads/interstitials/c;

.field private e:Z

.field private f:Z

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private a(Lcom/fyber/ads/b/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 162
    new-instance v0, Lcom/fyber/b/c/a$a;

    invoke-direct {v0, p1}, Lcom/fyber/b/c/a$a;-><init>(Lcom/fyber/ads/b/b;)V

    invoke-virtual {v0, p2}, Lcom/fyber/b/c/a$a;->b(Ljava/lang/String;)Lcom/fyber/b/b$a;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/c/a$a;

    .line 163
    invoke-virtual {v0, p0}, Lcom/fyber/b/c/a$a;->a(Lcom/fyber/ads/b/a;)Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/c/a;

    invoke-virtual {v0}, Lcom/fyber/b/c/a;->b()V

    .line 164
    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/fyber/ads/a;
    .locals 2

    .prologue
    .line 40
    .line 2158
    new-instance v0, Lcom/fyber/ads/interstitials/a;

    invoke-virtual {p0}, Lcom/fyber/ads/interstitials/b/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/fyber/ads/interstitials/a;-><init>(Ljava/lang/String;Lcom/fyber/ads/b/c;)V

    .line 40
    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 139
    sget-object v0, Lcom/fyber/ads/b/e;->d:Lcom/fyber/ads/b/e;

    invoke-static {v0}, Lcom/fyber/ads/interstitials/b/b;->a(Lcom/fyber/ads/b/e;)Z

    .line 140
    invoke-static {v1}, Lcom/fyber/ads/interstitials/b/b;->a(Lcom/fyber/ads/interstitials/b/a;)V

    .line 142
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->b:Lcom/fyber/g/a/j;

    if-eqz v0, :cond_2

    .line 143
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->d()Lcom/fyber/g/a/a/l;

    move-result-object v0

    iget-object v2, p0, Lcom/fyber/ads/interstitials/b/a;->b:Lcom/fyber/g/a/j;

    invoke-virtual {v2}, Lcom/fyber/g/a/j;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/fyber/g/a/a/l;->a(Ljava/lang/String;)Lcom/fyber/g/a/a/d;

    move-result-object v0

    .line 145
    :goto_0
    iget-boolean v2, p0, Lcom/fyber/ads/interstitials/b/a;->f:Z

    if-eqz v2, :cond_1

    .line 146
    const-string v0, "The Ad was already shown."

    .line 1105
    invoke-virtual {p0, v0, v1}, Lcom/fyber/ads/interstitials/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    :goto_1
    return-void

    .line 149
    :cond_1
    invoke-static {v0}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/g/a/a/d;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->g:Ljava/util/Map;

    .line 150
    sget-object v0, Lcom/fyber/mediation/g;->a:Lcom/fyber/mediation/g;

    invoke-virtual {v0, p1, p0}, Lcom/fyber/mediation/g;->a(Landroid/app/Activity;Lcom/fyber/ads/interstitials/b/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    const-string v0, "The current network is not available"

    .line 2105
    invoke-virtual {p0, v0, v1}, Lcom/fyber/ads/interstitials/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Lcom/fyber/ads/b/b;)V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/fyber/b/c/a$a;

    invoke-direct {v0, p1}, Lcom/fyber/b/c/a$a;-><init>(Lcom/fyber/ads/b/b;)V

    .line 169
    invoke-virtual {v0, p0}, Lcom/fyber/b/c/a$a;->a(Lcom/fyber/ads/b/a;)Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/c/a;

    invoke-virtual {v0}, Lcom/fyber/b/c/a;->b()V

    .line 170
    return-void
.end method

.method protected final a(Lcom/fyber/ads/b/b;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/ads/b/b;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    new-instance v0, Lcom/fyber/b/c/a$a;

    invoke-direct {v0, p1}, Lcom/fyber/b/c/a$a;-><init>(Lcom/fyber/ads/b/b;)V

    invoke-virtual {v0, p2}, Lcom/fyber/b/c/a$a;->a(Ljava/util/Map;)Lcom/fyber/b/b$a;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/c/a$a;

    .line 175
    invoke-virtual {v0, p0}, Lcom/fyber/b/c/a$a;->a(Lcom/fyber/ads/b/a;)Lcom/fyber/b/b;

    move-result-object v0

    check-cast v0, Lcom/fyber/b/c/a;

    invoke-virtual {v0}, Lcom/fyber/b/c/a;->b()V

    .line 176
    return-void
.end method

.method public final a(Lcom/fyber/ads/interstitials/c;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/fyber/ads/interstitials/b/a;->c:Lcom/fyber/ads/interstitials/c;

    .line 63
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/fyber/ads/interstitials/b;)V
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/fyber/ads/b/e;->a:Lcom/fyber/ads/b/e;

    invoke-static {v0}, Lcom/fyber/ads/interstitials/b/b;->a(Lcom/fyber/ads/b/e;)Z

    .line 83
    if-nez p2, :cond_0

    .line 84
    iget-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->e:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/fyber/ads/interstitials/b;->b:Lcom/fyber/ads/interstitials/b;

    .line 85
    :goto_0
    iget-boolean v1, p0, Lcom/fyber/ads/interstitials/b/a;->f:Z

    if-nez v1, :cond_5

    .line 87
    sget-object v0, Lcom/fyber/ads/interstitials/b;->a:Lcom/fyber/ads/interstitials/b;

    move-object p2, v0

    .line 91
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->f:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->e:Z

    if-nez v0, :cond_1

    .line 92
    sget-object v0, Lcom/fyber/ads/b/b;->i:Lcom/fyber/ads/b/b;

    invoke-direct {p0, v0, p1}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/ads/b/b;Ljava/lang/String;)V

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->c:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_2

    .line 96
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->c:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0, p2}, Lcom/fyber/ads/interstitials/c;->a(Lcom/fyber/ads/interstitials/a;Lcom/fyber/ads/interstitials/b;)V

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_3

    .line 100
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0, p2}, Lcom/fyber/ads/interstitials/c;->a(Lcom/fyber/ads/interstitials/a;Lcom/fyber/ads/interstitials/b;)V

    .line 102
    :cond_3
    return-void

    .line 84
    :cond_4
    sget-object v0, Lcom/fyber/ads/interstitials/b;->c:Lcom/fyber/ads/interstitials/b;

    goto :goto_0

    :cond_5
    move-object p2, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 109
    sget-object v0, Lcom/fyber/ads/b/e;->a:Lcom/fyber/ads/b/e;

    invoke-static {v0}, Lcom/fyber/ads/interstitials/b/b;->a(Lcom/fyber/ads/b/e;)Z

    .line 110
    sget-object v0, Lcom/fyber/ads/b/b;->j:Lcom/fyber/ads/b/b;

    invoke-direct {p0, v0, p2}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/ads/b/b;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->c:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->c:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0, p1}, Lcom/fyber/ads/interstitials/c;->a(Lcom/fyber/ads/interstitials/a;Ljava/lang/String;)V

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_1

    .line 117
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0, p1}, Lcom/fyber/ads/interstitials/c;->a(Lcom/fyber/ads/interstitials/a;Ljava/lang/String;)V

    .line 119
    :cond_1
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->f:Z

    if-nez v0, :cond_0

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->f:Z

    .line 73
    sget-object v0, Lcom/fyber/ads/b/b;->f:Lcom/fyber/ads/b/b;

    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->g:Ljava/util/Map;

    invoke-virtual {p0, v0, v1}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/ads/b/b;Ljava/util/Map;)V

    .line 74
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_0

    .line 75
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0}, Lcom/fyber/ads/interstitials/c;->a(Lcom/fyber/ads/interstitials/a;)V

    .line 78
    :cond_0
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->e:Z

    if-nez v0, :cond_0

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/interstitials/b/a;->e:Z

    .line 124
    sget-object v0, Lcom/fyber/ads/b/b;->h:Lcom/fyber/ads/b/b;

    invoke-virtual {p0, v0}, Lcom/fyber/ads/interstitials/b/a;->a(Lcom/fyber/ads/b/b;)V

    .line 125
    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/fyber/ads/interstitials/b/a;->d:Lcom/fyber/ads/interstitials/c;

    iget-object v0, p0, Lcom/fyber/ads/interstitials/b/a;->a:Lcom/fyber/ads/a;

    check-cast v0, Lcom/fyber/ads/interstitials/a;

    invoke-interface {v1, v0}, Lcom/fyber/ads/interstitials/c;->b(Lcom/fyber/ads/interstitials/a;)V

    .line 129
    :cond_0
    return-void
.end method
