.class public abstract Lcom/fyber/ads/interstitials/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/mediation/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/fyber/mediation/c;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/fyber/mediation/e",
        "<",
        "Lcom/fyber/ads/interstitials/a;",
        "Lcom/fyber/e/b;",
        "Lcom/fyber/ads/interstitials/b/a;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lcom/fyber/mediation/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field protected b:Lcom/fyber/ads/interstitials/b/a;

.field private c:Lcom/fyber/mediation/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/mediation/f",
            "<",
            "Lcom/fyber/ads/interstitials/a;",
            "Lcom/fyber/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/fyber/mediation/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/fyber/ads/interstitials/c/a;->a:Lcom/fyber/mediation/c;

    .line 58
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/app/Activity;)V
.end method

.method protected abstract a(Landroid/content/Context;)V
.end method

.method public a(Landroid/content/Context;Lcom/fyber/ads/interstitials/b/a;)V
    .locals 0

    .prologue
    .line 211
    iput-object p2, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    .line 212
    invoke-virtual {p0, p1}, Lcom/fyber/ads/interstitials/c/a;->a(Landroid/content/Context;)V

    .line 213
    return-void
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p2, Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {p0, p1, p2}, Lcom/fyber/ads/interstitials/c/a;->a(Landroid/content/Context;Lcom/fyber/ads/interstitials/b/a;)V

    return-void
.end method

.method public a(Lcom/fyber/mediation/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/mediation/f",
            "<",
            "Lcom/fyber/ads/interstitials/a;",
            "Lcom/fyber/e/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    iput-object p1, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    .line 218
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 107
    invoke-virtual {p0, p1, p1}, Lcom/fyber/ads/interstitials/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method protected a(Ljava/lang/String;Lcom/fyber/ads/interstitials/b;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/ads/interstitials/b/a;->a(Ljava/lang/String;Lcom/fyber/ads/interstitials/b;)V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    .line 169
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    new-instance v1, Lcom/fyber/e/b;

    invoke-direct {v1, p1, p2}, Lcom/fyber/e/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/fyber/mediation/f;->b(Ljava/lang/Object;)V

    .line 122
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    .line 123
    return-void

    .line 120
    :cond_0
    const-string v0, "InterstitialMediationAdapter"

    const-string v1, "No provider request listener"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/fyber/ads/interstitials/c/a;->a(Landroid/app/Activity;)V

    .line 132
    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fyber/ads/interstitials/c/a;->a(Ljava/lang/String;Lcom/fyber/ads/interstitials/b;)V

    .line 162
    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0, p1, p2}, Lcom/fyber/ads/interstitials/b/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    .line 183
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    iget-object v1, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v1}, Lcom/fyber/ads/interstitials/b/a;->h()Lcom/fyber/ads/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/fyber/mediation/f;->a(Ljava/lang/Object;)V

    .line 87
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    const-string v0, "InterstitialMediationAdapter"

    const-string v1, "No provider request listener"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fyber/ads/interstitials/c/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->c:Lcom/fyber/mediation/f;

    invoke-interface {v0}, Lcom/fyber/mediation/f;->a()V

    .line 98
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    .line 99
    return-void

    .line 96
    :cond_0
    const-string v0, "InterstitialMediationAdapter"

    const-string v1, "No provider request listener"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0}, Lcom/fyber/ads/interstitials/b/a;->i()V

    .line 141
    :cond_0
    return-void
.end method

.method protected f()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0}, Lcom/fyber/ads/interstitials/b/a;->j()V

    .line 150
    :cond_0
    return-void
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/fyber/ads/interstitials/c/a;->b(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method protected h()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0}, Lcom/fyber/ads/interstitials/b/a;->d()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method protected i()Lcom/fyber/ads/interstitials/a/a;
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fyber/ads/interstitials/c/a;->b:Lcom/fyber/ads/interstitials/b/a;

    invoke-virtual {v0}, Lcom/fyber/ads/interstitials/b/a;->d()Ljava/util/Map;

    move-result-object v0

    const-string v1, "creative_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 196
    :goto_0
    if-nez v1, :cond_1

    .line 197
    sget-object v0, Lcom/fyber/ads/interstitials/a/a;->c:Lcom/fyber/ads/interstitials/a/a;

    .line 205
    :goto_1
    return-object v0

    .line 195
    :cond_0
    const-string v0, ""

    move-object v1, v0

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 205
    sget-object v0, Lcom/fyber/ads/interstitials/a/a;->c:Lcom/fyber/ads/interstitials/a/a;

    goto :goto_1

    .line 199
    :sswitch_0
    const-string v2, "video"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v2, "static"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    .line 201
    :pswitch_0
    sget-object v0, Lcom/fyber/ads/interstitials/a/a;->a:Lcom/fyber/ads/interstitials/a/a;

    goto :goto_1

    .line 203
    :pswitch_1
    sget-object v0, Lcom/fyber/ads/interstitials/a/a;->b:Lcom/fyber/ads/interstitials/a/a;

    goto :goto_1

    .line 199
    :sswitch_data_0
    .sparse-switch
        -0x35323192 -> :sswitch_1
        0x6b0147b -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
