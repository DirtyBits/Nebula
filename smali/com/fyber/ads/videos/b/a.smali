.class public abstract Lcom/fyber/ads/videos/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/mediation/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/fyber/mediation/c;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/fyber/mediation/e",
        "<",
        "Lcom/fyber/ads/videos/a;",
        "Lcom/fyber/e/b;",
        "Lcom/fyber/ads/videos/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Lcom/fyber/mediation/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field protected b:Lcom/fyber/ads/videos/a/a;

.field private c:Lcom/fyber/ads/videos/b/d;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Landroid/os/Handler;

.field private g:Lcom/fyber/mediation/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/fyber/mediation/f",
            "<",
            "Lcom/fyber/ads/videos/a;",
            "Lcom/fyber/e/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/fyber/mediation/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fyber/ads/videos/b/a;->e:Z

    .line 90
    iput-object p1, p0, Lcom/fyber/ads/videos/b/a;->a:Lcom/fyber/mediation/c;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/fyber/ads/videos/b/a$1;

    invoke-direct {v2, p0}, Lcom/fyber/ads/videos/b/a$1;-><init>(Lcom/fyber/ads/videos/b/a;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/b/a;->f:Landroid/os/Handler;

    .line 103
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 201
    iput-object v0, p0, Lcom/fyber/ads/videos/b/a;->c:Lcom/fyber/ads/videos/b/d;

    .line 202
    iput-object v0, p0, Lcom/fyber/ads/videos/b/a;->d:Ljava/util/Map;

    .line 203
    return-void
.end method

.method public abstract a(Landroid/app/Activity;)V
.end method

.method public final a(Landroid/app/Activity;Lcom/fyber/ads/videos/b/d;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/fyber/ads/videos/b/d;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fyber/ads/videos/b/a;->e:Z

    .line 140
    iput-object p2, p0, Lcom/fyber/ads/videos/b/a;->c:Lcom/fyber/ads/videos/b/d;

    .line 141
    iput-object p3, p0, Lcom/fyber/ads/videos/b/a;->d:Ljava/util/Map;

    .line 142
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x1194

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 143
    invoke-virtual {p0, p1}, Lcom/fyber/ads/videos/b/a;->a(Landroid/app/Activity;)V

    .line 144
    return-void
.end method

.method public abstract a(Landroid/content/Context;)V
.end method

.method public a(Landroid/content/Context;Lcom/fyber/ads/videos/a/a;)V
    .locals 0

    .prologue
    .line 248
    iput-object p2, p0, Lcom/fyber/ads/videos/b/a;->b:Lcom/fyber/ads/videos/a/a;

    .line 249
    invoke-virtual {p0, p1}, Lcom/fyber/ads/videos/b/a;->a(Landroid/content/Context;)V

    .line 250
    return-void
.end method

.method public bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p2, Lcom/fyber/ads/videos/a/a;

    invoke-virtual {p0, p1, p2}, Lcom/fyber/ads/videos/b/a;->a(Landroid/content/Context;Lcom/fyber/ads/videos/a/a;)V

    return-void
.end method

.method protected a(Lcom/fyber/ads/videos/b/b;)V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->c:Lcom/fyber/ads/videos/b/d;

    if-eqz v0, :cond_1

    .line 186
    sget-object v0, Lcom/fyber/ads/videos/b/b;->a:Lcom/fyber/ads/videos/b/b;

    invoke-virtual {p1, v0}, Lcom/fyber/ads/videos/b/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->f:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->c:Lcom/fyber/ads/videos/b/d;

    invoke-virtual {p0}, Lcom/fyber/ads/videos/b/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/fyber/ads/videos/b/a;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/fyber/ads/videos/b/a;->d:Ljava/util/Map;

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/fyber/ads/videos/b/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/fyber/ads/videos/b/b;Ljava/util/Map;)V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_1
    const-string v0, "RewardedVideoMediationAdapter"

    const-string v1, "No video event listener"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/fyber/ads/videos/b/c;)V
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->g:Lcom/fyber/mediation/f;

    if-eqz v0, :cond_3

    .line 165
    sget-object v0, Lcom/fyber/ads/videos/b/c;->g:Lcom/fyber/ads/videos/b/c;

    if-ne p1, v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->g:Lcom/fyber/mediation/f;

    iget-object v1, p0, Lcom/fyber/ads/videos/b/a;->b:Lcom/fyber/ads/videos/a/a;

    invoke-virtual {v1}, Lcom/fyber/ads/videos/a/a;->h()Lcom/fyber/ads/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/fyber/mediation/f;->a(Ljava/lang/Object;)V

    .line 175
    :goto_0
    return-void

    .line 167
    :cond_0
    sget-object v0, Lcom/fyber/ads/videos/b/c;->b:Lcom/fyber/ads/videos/b/c;

    if-ne p1, v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->g:Lcom/fyber/mediation/f;

    invoke-interface {v0}, Lcom/fyber/mediation/f;->a()V

    goto :goto_0

    .line 170
    :cond_1
    iget-object v1, p0, Lcom/fyber/ads/videos/b/a;->g:Lcom/fyber/mediation/f;

    new-instance v2, Lcom/fyber/e/b;

    const-string v3, "validation"

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/fyber/ads/videos/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v3, v0}, Lcom/fyber/e/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/fyber/mediation/f;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const-string v0, "Validation"

    goto :goto_1

    .line 173
    :cond_3
    const-string v0, "RewardedVideoMediationAdapter"

    const-string v1, "No provider request listener"

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/fyber/mediation/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/fyber/mediation/f",
            "<",
            "Lcom/fyber/ads/videos/a;",
            "Lcom/fyber/e/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    iput-object p1, p0, Lcom/fyber/ads/videos/b/a;->g:Lcom/fyber/mediation/f;

    .line 255
    return-void
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/fyber/ads/videos/b/b;->c:Lcom/fyber/ads/videos/b/b;

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/b/a;->a(Lcom/fyber/ads/videos/b/b;)V

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/videos/b/a;->e:Z

    .line 212
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 218
    sget-object v0, Lcom/fyber/ads/videos/b/b;->a:Lcom/fyber/ads/videos/b/b;

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/b/a;->a(Lcom/fyber/ads/videos/b/b;)V

    .line 219
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/fyber/ads/videos/b/a;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fyber/ads/videos/b/b;->d:Lcom/fyber/ads/videos/b/b;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/b/a;->a(Lcom/fyber/ads/videos/b/b;)V

    .line 227
    invoke-virtual {p0}, Lcom/fyber/ads/videos/b/a;->a()V

    .line 228
    return-void

    .line 225
    :cond_0
    sget-object v0, Lcom/fyber/ads/videos/b/b;->b:Lcom/fyber/ads/videos/b/b;

    goto :goto_0
.end method

.method protected e()V
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/fyber/ads/videos/b/b;->g:Lcom/fyber/ads/videos/b/b;

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/b/a;->a(Lcom/fyber/ads/videos/b/b;)V

    .line 235
    invoke-virtual {p0}, Lcom/fyber/ads/videos/b/a;->a()V

    .line 236
    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->a:Lcom/fyber/mediation/c;

    invoke-virtual {v0}, Lcom/fyber/mediation/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/fyber/ads/videos/b/a;->a:Lcom/fyber/mediation/c;

    invoke-virtual {v0}, Lcom/fyber/mediation/c;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
