.class public Lcom/fyber/ads/videos/RewardedVideoActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/fyber/ads/videos/a/d;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Lcom/fyber/mediation/d;

.field private e:Z

.field private f:Landroid/content/IntentFilter;

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 70
    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    .line 73
    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->b:Z

    .line 75
    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    .line 78
    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->e:Z

    .line 80
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->f:Landroid/content/IntentFilter;

    .line 81
    new-instance v0, Lcom/fyber/ads/videos/RewardedVideoActivity$1;

    invoke-direct {v0, p0}, Lcom/fyber/ads/videos/RewardedVideoActivity$1;-><init>(Lcom/fyber/ads/videos/RewardedVideoActivity;)V

    iput-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->g:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/RewardedVideoActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/fyber/ads/videos/RewardedVideoActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/fyber/ads/videos/RewardedVideoActivity;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    return p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    invoke-static {p1}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    invoke-static {}, Lcom/fyber/a;->c()Lcom/fyber/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fyber/a$b;->d()Lcom/fyber/g/a/a/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fyber/g/a/a/l;->a(Ljava/lang/String;)Lcom/fyber/g/a/a/d;

    move-result-object v0

    .line 133
    :cond_0
    iget-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    if-nez v1, :cond_1

    .line 134
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/fyber/ads/videos/RewardedVideoActivity;->setRequestedOrientation(I)V

    .line 136
    :cond_1
    sget-object v1, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v1, p0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d;)Z

    .line 138
    sget-object v1, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    iget-boolean v2, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    invoke-virtual {v1, p0, v2, v0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/RewardedVideoActivity;ZLcom/fyber/g/a/a/d;)Z

    .line 139
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->b:Z

    .line 182
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d;)Z

    .line 183
    invoke-virtual {p0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->finish()V

    .line 184
    return-void
.end method

.method public a(Lcom/fyber/ads/videos/a/d$a;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 193
    sget-object v0, Lcom/fyber/ads/videos/RewardedVideoActivity$3;->a:[I

    invoke-virtual {p1}, Lcom/fyber/ads/videos/a/d$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 212
    :goto_0
    return-void

    .line 195
    :pswitch_0
    const-string v0, "CLOSE_FINISHED"

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :pswitch_1
    const-string v0, "CLOSE_ABORTED"

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    :pswitch_2
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :pswitch_3
    iput-boolean v2, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    goto :goto_0

    .line 207
    :pswitch_4
    iput-boolean v2, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->e:Z

    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lcom/fyber/mediation/d;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    if-nez v0, :cond_0

    .line 226
    iput-object p1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    .line 228
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 215
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 216
    const-string v1, "ENGAGEMENT_STATUS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->setResult(ILandroid/content/Intent;)V

    .line 218
    invoke-virtual {p0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a()V

    .line 219
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->e:Z

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    invoke-interface {v0}, Lcom/fyber/mediation/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    if-nez v0, :cond_2

    .line 253
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->a()V

    goto :goto_0

    .line 255
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 96
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 97
    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 98
    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 100
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->g:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->f:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/fyber/ads/videos/RewardedVideoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1042
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1043
    if-eqz v0, :cond_1

    .line 1044
    const-string v1, "REQUEST_AGENT_CACHE_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1045
    invoke-static {v0}, Lcom/fyber/utils/c;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    :goto_0
    sget-object v1, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v1}, Lcom/fyber/ads/videos/a/c;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    if-eqz p1, :cond_2

    .line 104
    const-string v1, "PENDING_CLOSE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    .line 105
    const-string v1, "ENGAGEMENT_ALREADY_CLOSE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->b:Z

    .line 106
    const-string v1, "PLAY_THROUGH_MEDIATION"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    .line 107
    invoke-direct {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->b(Ljava/lang/String;)V

    .line 121
    :goto_1
    return-void

    .line 1045
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 1047
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 109
    :cond_2
    sget-object v1, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    new-instance v2, Lcom/fyber/ads/videos/RewardedVideoActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity$2;-><init>(Lcom/fyber/ads/videos/RewardedVideoActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/fyber/ads/videos/a/c;->a(Landroid/webkit/ValueCallback;)V

    goto :goto_1

    .line 118
    :cond_3
    const-string v0, "RewardedVideoActivity"

    const-string v1, "Currently it is not possible to show offers. Make sure you have requested offers."

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "ERROR"

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/fyber/ads/videos/RewardedVideoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    .line 177
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 178
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 157
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Z)V

    .line 158
    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->b:Z

    if-nez v0, :cond_0

    .line 159
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->d()V

    .line 160
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->a()V

    .line 161
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d;)Z

    .line 163
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 144
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/fyber/ads/videos/a/c;->a(Z)V

    .line 146
    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    if-eqz v0, :cond_1

    .line 147
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->a()V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    if-nez v0, :cond_0

    .line 149
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0, p0}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/d;)Z

    .line 150
    sget-object v0, Lcom/fyber/ads/videos/a/c;->a:Lcom/fyber/ads/videos/a/c;

    invoke-virtual {v0}, Lcom/fyber/ads/videos/a/c;->e()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 168
    const-string v0, "PENDING_CLOSE"

    iget-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169
    const-string v0, "ENGAGEMENT_ALREADY_CLOSE"

    iget-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 170
    const-string v0, "PLAY_THROUGH_MEDIATION"

    iget-boolean v1, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 171
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->d:Lcom/fyber/mediation/d;

    invoke-interface {v0}, Lcom/fyber/mediation/d;->b()V

    .line 238
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fyber/ads/videos/RewardedVideoActivity;->e:Z

    .line 239
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 240
    return-void
.end method
