.class final Lcom/fyber/ads/videos/a/c$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/ads/videos/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/webkit/WebView;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/fyber/ads/videos/a/c;


# direct methods
.method constructor <init>(Lcom/fyber/ads/videos/a/c;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Landroid/webkit/WebView;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 514
    new-instance v0, Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->j(Lcom/fyber/ads/videos/a/c;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 515
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 517
    invoke-static {v0}, Lcom/fyber/utils/f;->b(Landroid/webkit/WebView;)V

    .line 518
    invoke-static {v1}, Lcom/fyber/utils/f;->a(Landroid/webkit/WebSettings;)V

    .line 519
    invoke-static {v0}, Lcom/fyber/utils/f;->a(Landroid/webkit/WebView;)V

    .line 1058
    const/16 v2, 0x11

    invoke-static {v2}, Lcom/fyber/utils/r;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1059
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 522
    :cond_0
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 524
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 526
    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 528
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->k(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebChromeClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 530
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->l(Lcom/fyber/ads/videos/a/c;)Landroid/webkit/WebViewClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 532
    iget-object v1, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v1}, Lcom/fyber/ads/videos/a/c;->m(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/b/e;

    move-result-object v1

    iget-object v2, p0, Lcom/fyber/ads/videos/a/c$4;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v2}, Lcom/fyber/ads/videos/a/c;->m(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/b/e;

    move-result-object v2

    .line 1096
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "SynchJS"

    .line 532
    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 534
    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/fyber/ads/videos/a/c$4;->a()Landroid/webkit/WebView;

    move-result-object v0

    return-object v0
.end method
