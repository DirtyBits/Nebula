.class final Lcom/fyber/ads/videos/a/c$8;
.super Landroid/webkit/WebChromeClient;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fyber/ads/videos/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/fyber/ads/videos/a/c;


# direct methods
.method constructor <init>(Lcom/fyber/ads/videos/a/c;)V
    .locals 0

    .prologue
    .line 790
    iput-object p1, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 793
    const-string v0, "RewardedVideoClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "js alert - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800
    const-string v0, "RewardedVideoClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "js alert - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/fyber/utils/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->v(Lcom/fyber/ads/videos/a/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1802
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0, v4}, Lcom/fyber/ads/videos/a/c;->a(Lcom/fyber/ads/videos/a/c;Z)Z

    .line 1803
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->j(Lcom/fyber/ads/videos/a/c;)Landroid/content/Context;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1804
    sget-object v0, Lcom/fyber/a$a$a;->p:Lcom/fyber/a$a$a;

    invoke-static {v0}, Lcom/fyber/utils/x;->a(Lcom/fyber/a$a$a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "OK"

    new-instance v3, Lcom/fyber/ads/videos/a/c$8$3;

    invoke-direct {v3, p0}, Lcom/fyber/ads/videos/a/c$8$3;-><init>(Lcom/fyber/ads/videos/a/c$8;)V

    .line 1805
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "Cancel"

    new-instance v3, Lcom/fyber/ads/videos/a/c$8$2;

    invoke-direct {v3, p0}, Lcom/fyber/ads/videos/a/c$8$2;-><init>(Lcom/fyber/ads/videos/a/c$8;)V

    .line 1811
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/fyber/ads/videos/a/c$8$1;

    invoke-direct {v2, p0}, Lcom/fyber/ads/videos/a/c$8$1;-><init>(Lcom/fyber/ads/videos/a/c$8;)V

    .line 1816
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1822
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 795
    :cond_0
    invoke-virtual {p4}, Landroid/webkit/JsResult;->cancel()V

    .line 796
    return v4

    .line 1803
    :cond_1
    iget-object v0, p0, Lcom/fyber/ads/videos/a/c$8;->a:Lcom/fyber/ads/videos/a/c;

    invoke-static {v0}, Lcom/fyber/ads/videos/a/c;->p(Lcom/fyber/ads/videos/a/c;)Lcom/fyber/ads/videos/RewardedVideoActivity;

    move-result-object v0

    goto :goto_0
.end method
