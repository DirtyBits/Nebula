.class public final Lcom/google/android/a/c/c/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/e;
.implements Lcom/google/android/a/c/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/c/e$a;
    }
.end annotation


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcom/google/android/a/f/j;

.field private final c:Lcom/google/android/a/f/j;

.field private final d:Lcom/google/android/a/f/j;

.field private final e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/a/c/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:J

.field private j:I

.field private k:Lcom/google/android/a/f/j;

.field private l:I

.field private m:I

.field private n:I

.field private o:Lcom/google/android/a/c/g;

.field private p:[Lcom/google/android/a/c/c/e$a;

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "qt  "

    invoke-static {v0}, Lcom/google/android/a/f/n;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/a/c/c/e;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lcom/google/android/a/f/j;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    .line 81
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    .line 82
    new-instance v0, Lcom/google/android/a/f/j;

    sget-object v1, Lcom/google/android/a/f/h;->a:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->b:Lcom/google/android/a/f/j;

    .line 83
    new-instance v0, Lcom/google/android/a/f/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->c:Lcom/google/android/a/f/j;

    .line 84
    invoke-direct {p0}, Lcom/google/android/a/c/c/e;->c()V

    .line 85
    return-void
.end method

.method private a(Lcom/google/android/a/c/c/a$a;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 277
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 278
    const-wide v4, 0x7fffffffffffffffL

    .line 279
    const/4 v0, 0x0

    .line 280
    sget v1, Lcom/google/android/a/c/c/a;->as:I

    invoke-virtual {p1, v1}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v1

    .line 281
    if-eqz v1, :cond_4

    .line 282
    invoke-static {v1}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/c/c/a$a;)Lcom/google/android/a/c/i;

    move-result-object v0

    move-object v1, v0

    :goto_0
    move v2, v3

    .line 284
    :goto_1
    iget-object v0, p1, Lcom/google/android/a/c/c/a$a;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 285
    iget-object v0, p1, Lcom/google/android/a/c/c/a$a;->aC:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    .line 286
    iget v6, v0, Lcom/google/android/a/c/c/a$a;->az:I

    sget v7, Lcom/google/android/a/c/c/a;->A:I

    if-eq v6, v7, :cond_1

    .line 284
    :cond_0
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 290
    :cond_1
    sget v6, Lcom/google/android/a/c/c/a;->z:I

    invoke-virtual {p1, v6}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/android/a/c/c/e;->q:Z

    invoke-static {v0, v6, v7}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/a$b;Z)Lcom/google/android/a/c/c/h;

    move-result-object v6

    .line 292
    if-eqz v6, :cond_0

    .line 296
    sget v7, Lcom/google/android/a/c/c/a;->B:I

    invoke-virtual {v0, v7}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v0

    sget v7, Lcom/google/android/a/c/c/a;->C:I

    invoke-virtual {v0, v7}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v0

    sget v7, Lcom/google/android/a/c/c/a;->D:I

    invoke-virtual {v0, v7}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v0

    .line 298
    invoke-static {v6, v0}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/a$a;)Lcom/google/android/a/c/c/k;

    move-result-object v7

    .line 299
    iget v0, v7, Lcom/google/android/a/c/c/k;->a:I

    if-eqz v0, :cond_0

    .line 303
    new-instance v9, Lcom/google/android/a/c/c/e$a;

    iget-object v0, p0, Lcom/google/android/a/c/c/e;->o:Lcom/google/android/a/c/g;

    invoke-interface {v0, v2}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v0

    invoke-direct {v9, v6, v7, v0}, Lcom/google/android/a/c/c/e$a;-><init>(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/k;Lcom/google/android/a/c/m;)V

    .line 306
    iget v0, v7, Lcom/google/android/a/c/c/k;->d:I

    add-int/lit8 v0, v0, 0x1e

    .line 307
    iget-object v6, v6, Lcom/google/android/a/c/c/h;->k:Lcom/google/android/a/o;

    invoke-virtual {v6, v0}, Lcom/google/android/a/o;->a(I)Lcom/google/android/a/o;

    move-result-object v0

    .line 308
    if-eqz v1, :cond_2

    .line 309
    iget v6, v1, Lcom/google/android/a/c/i;->a:I

    iget v10, v1, Lcom/google/android/a/c/i;->b:I

    invoke-virtual {v0, v6, v10}, Lcom/google/android/a/o;->a(II)Lcom/google/android/a/o;

    move-result-object v0

    .line 312
    :cond_2
    iget-object v6, v9, Lcom/google/android/a/c/c/e$a;->c:Lcom/google/android/a/c/m;

    invoke-interface {v6, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 313
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    iget-object v0, v7, Lcom/google/android/a/c/c/k;->b:[J

    aget-wide v6, v0, v3

    .line 316
    cmp-long v0, v6, v4

    if-gez v0, :cond_0

    move-wide v4, v6

    .line 317
    goto :goto_2

    .line 320
    :cond_3
    new-array v0, v3, [Lcom/google/android/a/c/c/e$a;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/a/c/c/e$a;

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    .line 321
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->o:Lcom/google/android/a/c/g;

    invoke-interface {v0}, Lcom/google/android/a/c/g;->f()V

    .line 322
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->o:Lcom/google/android/a/c/g;

    invoke-interface {v0, p0}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 323
    return-void

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 431
    sget v0, Lcom/google/android/a/c/c/a;->N:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->z:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->O:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->P:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->ag:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->ah:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->ai:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->M:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->aj:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->ak:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->al:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->am:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->K:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->a:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->at:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/f/j;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 259
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    .line 261
    sget v2, Lcom/google/android/a/c/c/e;->a:I

    if-ne v1, v2, :cond_0

    .line 270
    :goto_0
    return v0

    .line 264
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 265
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->b()I

    move-result v1

    if-lez v1, :cond_2

    .line 266
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    sget v2, Lcom/google/android/a/c/c/e;->a:I

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 270
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 442
    sget v0, Lcom/google/android/a/c/c/a;->y:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->A:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->B:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->C:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->D:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->L:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->as:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/c/f;)Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 169
    iget v0, p0, Lcom/google/android/a/c/c/e;->j:I

    if-nez v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v0, v2, v8, v1}, Lcom/google/android/a/c/f;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    :goto_0
    return v2

    .line 174
    :cond_0
    iput v8, p0, Lcom/google/android/a/c/c/e;->j:I

    .line 175
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    .line 177
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/c/e;->h:I

    .line 180
    :cond_1
    iget-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v0, v8, v8}, Lcom/google/android/a/c/f;->b([BII)V

    .line 184
    iget v0, p0, Lcom/google/android/a/c/c/e;->j:I

    add-int/2addr v0, v8

    iput v0, p0, Lcom/google/android/a/c/c/e;->j:I

    .line 185
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    .line 188
    :cond_2
    iget v0, p0, Lcom/google/android/a/c/c/e;->h:I

    invoke-static {v0}, Lcom/google/android/a/c/c/e;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 189
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    add-long/2addr v2, v4

    iget v0, p0, Lcom/google/android/a/c/c/e;->j:I

    int-to-long v4, v0

    sub-long/2addr v2, v4

    .line 190
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    new-instance v4, Lcom/google/android/a/c/c/a$a;

    iget v5, p0, Lcom/google/android/a/c/c/e;->h:I

    invoke-direct {v4, v5, v2, v3}, Lcom/google/android/a/c/c/a$a;-><init>(IJ)V

    invoke-virtual {v0, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-direct {p0}, Lcom/google/android/a/c/c/e;->c()V

    :goto_1
    move v2, v1

    .line 205
    goto :goto_0

    .line 192
    :cond_3
    iget v0, p0, Lcom/google/android/a/c/c/e;->h:I

    invoke-static {v0}, Lcom/google/android/a/c/c/e;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 195
    iget v0, p0, Lcom/google/android/a/c/c/e;->j:I

    if-ne v0, v8, :cond_4

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/a/f/b;->b(Z)V

    .line 196
    iget-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-gtz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/a/f/b;->b(Z)V

    .line 197
    new-instance v0, Lcom/google/android/a/f/j;

    iget-wide v4, p0, Lcom/google/android/a/c/c/e;->i:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    .line 198
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->d:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    iget-object v3, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    invoke-static {v0, v2, v3, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    iput v9, p0, Lcom/google/android/a/c/c/e;->g:I

    goto :goto_1

    :cond_4
    move v0, v2

    .line 195
    goto :goto_2

    :cond_5
    move v0, v2

    .line 196
    goto :goto_3

    .line 201
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    .line 202
    iput v9, p0, Lcom/google/android/a/c/c/e;->g:I

    goto :goto_1
.end method

.method private b(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 215
    iget-wide v0, p0, Lcom/google/android/a/c/c/e;->i:J

    iget v2, p0, Lcom/google/android/a/c/c/e;->j:I

    int-to-long v4, v2

    sub-long/2addr v0, v4

    .line 216
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 218
    iget-object v2, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    if-eqz v2, :cond_2

    .line 219
    iget-object v2, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    iget v6, p0, Lcom/google/android/a/c/c/e;->j:I

    long-to-int v0, v0

    invoke-interface {p1, v2, v6, v0}, Lcom/google/android/a/c/f;->b([BII)V

    .line 220
    iget v0, p0, Lcom/google/android/a/c/c/e;->h:I

    sget v1, Lcom/google/android/a/c/c/a;->a:I

    if-ne v0, v1, :cond_1

    .line 221
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    invoke-static {v0}, Lcom/google/android/a/c/c/e;->a(Lcom/google/android/a/f/j;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/a/c/c/e;->q:Z

    move v2, v3

    .line 235
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    iget-wide v0, v0, Lcom/google/android/a/c/c/a$a;->aA:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    .line 236
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    .line 237
    iget v1, v0, Lcom/google/android/a/c/c/a$a;->az:I

    sget v6, Lcom/google/android/a/c/c/a;->y:I

    if-ne v1, v6, :cond_4

    .line 239
    invoke-direct {p0, v0}, Lcom/google/android/a/c/c/e;->a(Lcom/google/android/a/c/c/a$a;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 241
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/c/e;->g:I

    .line 249
    :goto_1
    return v3

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 223
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    new-instance v1, Lcom/google/android/a/c/c/a$b;

    iget v2, p0, Lcom/google/android/a/c/c/e;->h:I

    iget-object v6, p0, Lcom/google/android/a/c/c/e;->k:Lcom/google/android/a/f/j;

    invoke-direct {v1, v2, v6}, Lcom/google/android/a/c/c/a$b;-><init>(ILcom/google/android/a/f/j;)V

    invoke-virtual {v0, v1}, Lcom/google/android/a/c/c/a$a;->a(Lcom/google/android/a/c/c/a$b;)V

    move v2, v3

    goto :goto_0

    .line 227
    :cond_2
    const-wide/32 v6, 0x40000

    cmp-long v2, v0, v6

    if-gez v2, :cond_3

    .line 228
    long-to-int v0, v0

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    move v2, v3

    goto :goto_0

    .line 230
    :cond_3
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v6

    add-long/2addr v0, v6

    iput-wide v0, p2, Lcom/google/android/a/c/j;->a:J

    .line 231
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 243
    :cond_4
    iget-object v1, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    iget-object v1, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/a/c/c/a$a;

    invoke-virtual {v1, v0}, Lcom/google/android/a/c/c/a$a;->a(Lcom/google/android/a/c/c/a$a;)V

    goto :goto_0

    .line 248
    :cond_5
    invoke-direct {p0}, Lcom/google/android/a/c/c/e;->c()V

    move v3, v2

    .line 249
    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_0
.end method

.method private c(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 10

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/google/android/a/c/c/e;->d()I

    move-result v0

    .line 344
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 345
    const/4 v0, -0x1

    .line 400
    :goto_0
    return v0

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    aget-object v0, v1, v0

    .line 348
    iget-object v1, v0, Lcom/google/android/a/c/c/e$a;->c:Lcom/google/android/a/c/m;

    .line 349
    iget v4, v0, Lcom/google/android/a/c/c/e$a;->d:I

    .line 350
    iget-object v2, v0, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget-object v2, v2, Lcom/google/android/a/c/c/k;->b:[J

    aget-wide v2, v2, v4

    .line 351
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v6

    sub-long v6, v2, v6

    iget v5, p0, Lcom/google/android/a/c/c/e;->m:I

    int-to-long v8, v5

    add-long/2addr v6, v8

    .line 352
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_1

    const-wide/32 v8, 0x40000

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    .line 353
    :cond_1
    iput-wide v2, p2, Lcom/google/android/a/c/j;->a:J

    .line 354
    const/4 v0, 0x1

    goto :goto_0

    .line 356
    :cond_2
    long-to-int v2, v6

    invoke-interface {p1, v2}, Lcom/google/android/a/c/f;->b(I)V

    .line 357
    iget-object v2, v0, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget-object v2, v2, Lcom/google/android/a/c/c/k;->c:[I

    aget v2, v2, v4

    iput v2, p0, Lcom/google/android/a/c/c/e;->l:I

    .line 358
    iget-object v2, v0, Lcom/google/android/a/c/c/e$a;->a:Lcom/google/android/a/c/c/h;

    iget v2, v2, Lcom/google/android/a/c/c/h;->o:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    .line 361
    iget-object v2, p0, Lcom/google/android/a/c/c/e;->c:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    .line 362
    const/4 v3, 0x0

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 363
    const/4 v3, 0x1

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 364
    const/4 v3, 0x2

    const/4 v5, 0x0

    aput-byte v5, v2, v3

    .line 365
    iget-object v2, v0, Lcom/google/android/a/c/c/e$a;->a:Lcom/google/android/a/c/c/h;

    iget v2, v2, Lcom/google/android/a/c/c/h;->o:I

    .line 366
    iget-object v3, v0, Lcom/google/android/a/c/c/e$a;->a:Lcom/google/android/a/c/c/h;

    iget v3, v3, Lcom/google/android/a/c/c/h;->o:I

    rsub-int/lit8 v3, v3, 0x4

    .line 370
    :goto_1
    iget v5, p0, Lcom/google/android/a/c/c/e;->m:I

    iget v6, p0, Lcom/google/android/a/c/c/e;->l:I

    if-ge v5, v6, :cond_5

    .line 371
    iget v5, p0, Lcom/google/android/a/c/c/e;->n:I

    if-nez v5, :cond_3

    .line 373
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->c:Lcom/google/android/a/f/j;

    iget-object v5, v5, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v5, v3, v2}, Lcom/google/android/a/c/f;->b([BII)V

    .line 374
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->c:Lcom/google/android/a/f/j;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 375
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v5}, Lcom/google/android/a/f/j;->o()I

    move-result v5

    iput v5, p0, Lcom/google/android/a/c/c/e;->n:I

    .line 377
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->b:Lcom/google/android/a/f/j;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 378
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->b:Lcom/google/android/a/f/j;

    const/4 v6, 0x4

    invoke-interface {v1, v5, v6}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 379
    iget v5, p0, Lcom/google/android/a/c/c/e;->m:I

    add-int/lit8 v5, v5, 0x4

    iput v5, p0, Lcom/google/android/a/c/c/e;->m:I

    .line 380
    iget v5, p0, Lcom/google/android/a/c/c/e;->l:I

    add-int/2addr v5, v3

    iput v5, p0, Lcom/google/android/a/c/c/e;->l:I

    goto :goto_1

    .line 383
    :cond_3
    iget v5, p0, Lcom/google/android/a/c/c/e;->n:I

    const/4 v6, 0x0

    invoke-interface {v1, p1, v5, v6}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v5

    .line 384
    iget v6, p0, Lcom/google/android/a/c/c/e;->m:I

    add-int/2addr v6, v5

    iput v6, p0, Lcom/google/android/a/c/c/e;->m:I

    .line 385
    iget v6, p0, Lcom/google/android/a/c/c/e;->n:I

    sub-int v5, v6, v5

    iput v5, p0, Lcom/google/android/a/c/c/e;->n:I

    goto :goto_1

    .line 389
    :cond_4
    :goto_2
    iget v2, p0, Lcom/google/android/a/c/c/e;->m:I

    iget v3, p0, Lcom/google/android/a/c/c/e;->l:I

    if-ge v2, v3, :cond_5

    .line 390
    iget v2, p0, Lcom/google/android/a/c/c/e;->l:I

    iget v3, p0, Lcom/google/android/a/c/c/e;->m:I

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v2

    .line 391
    iget v3, p0, Lcom/google/android/a/c/c/e;->m:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/a/c/c/e;->m:I

    .line 392
    iget v3, p0, Lcom/google/android/a/c/c/e;->n:I

    sub-int v2, v3, v2

    iput v2, p0, Lcom/google/android/a/c/c/e;->n:I

    goto :goto_2

    .line 395
    :cond_5
    iget-object v2, v0, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget-object v2, v2, Lcom/google/android/a/c/c/k;->e:[J

    aget-wide v2, v2, v4

    iget-object v5, v0, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget-object v5, v5, Lcom/google/android/a/c/c/k;->f:[I

    aget v4, v5, v4

    iget v5, p0, Lcom/google/android/a/c/c/e;->l:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 397
    iget v1, v0, Lcom/google/android/a/c/c/e$a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/c/c/e$a;->d:I

    .line 398
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/c/e;->m:I

    .line 399
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/c/e;->n:I

    .line 400
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/c/e;->g:I

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/c/e;->j:I

    .line 166
    return-void
.end method

.method private d()I
    .locals 7

    .prologue
    .line 408
    const/4 v1, -0x1

    .line 409
    const-wide v2, 0x7fffffffffffffffL

    .line 410
    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 411
    iget-object v4, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    aget-object v4, v4, v0

    .line 412
    iget v5, v4, Lcom/google/android/a/c/c/e$a;->d:I

    .line 413
    iget-object v6, v4, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget v6, v6, Lcom/google/android/a/c/c/k;->a:I

    if-ne v5, v6, :cond_1

    .line 410
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 417
    :cond_1
    iget-object v4, v4, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    iget-object v4, v4, Lcom/google/android/a/c/c/k;->b:[J

    aget-wide v4, v4, v5

    .line 418
    cmp-long v6, v4, v2

    if-gez v6, :cond_0

    move-wide v2, v4

    move v1, v0

    .line 420
    goto :goto_1

    .line 424
    :cond_2
    return v1
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 4

    .prologue
    .line 110
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/a/c/c/e;->g:I

    packed-switch v0, :pswitch_data_0

    .line 129
    invoke-direct {p0, p1, p2}, Lcom/google/android/a/c/c/e;->c(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I

    move-result v0

    :goto_1
    return v0

    .line 112
    :pswitch_0
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 113
    invoke-direct {p0}, Lcom/google/android/a/c/c/e;->c()V

    goto :goto_0

    .line 115
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/c/e;->g:I

    goto :goto_0

    .line 119
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/e;->b(Lcom/google/android/a/c/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const/4 v0, -0x1

    goto :goto_1

    .line 124
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/a/c/c/e;->b(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    goto :goto_1

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/google/android/a/c/g;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/a/c/c/e;->o:Lcom/google/android/a/c/g;

    .line 95
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/android/a/c/f;)Z
    .locals 1

    .prologue
    .line 89
    invoke-static {p1}, Lcom/google/android/a/c/c/g;->b(Lcom/google/android/a/c/f;)Z

    move-result v0

    return v0
.end method

.method public b(J)J
    .locals 7

    .prologue
    .line 143
    const-wide v2, 0x7fffffffffffffffL

    .line 144
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 145
    iget-object v1, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    aget-object v1, v1, v0

    iget-object v4, v1, Lcom/google/android/a/c/c/e$a;->b:Lcom/google/android/a/c/c/k;

    .line 146
    invoke-virtual {v4, p1, p2}, Lcom/google/android/a/c/c/k;->a(J)I

    move-result v1

    .line 147
    const/4 v5, -0x1

    if-ne v1, v5, :cond_0

    .line 149
    invoke-virtual {v4, p1, p2}, Lcom/google/android/a/c/c/k;->b(J)I

    move-result v1

    .line 151
    :cond_0
    iget-object v5, p0, Lcom/google/android/a/c/c/e;->p:[Lcom/google/android/a/c/c/e$a;

    aget-object v5, v5, v0

    iput v1, v5, Lcom/google/android/a/c/c/e$a;->d:I

    .line 153
    iget-object v4, v4, Lcom/google/android/a/c/c/k;->b:[J

    aget-wide v4, v4, v1

    .line 154
    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    move-wide v2, v4

    .line 144
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 158
    :cond_2
    return-wide v2
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/a/c/c/e;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 100
    iput v1, p0, Lcom/google/android/a/c/c/e;->j:I

    .line 101
    iput v1, p0, Lcom/google/android/a/c/c/e;->m:I

    .line 102
    iput v1, p0, Lcom/google/android/a/c/c/e;->n:I

    .line 103
    iput v1, p0, Lcom/google/android/a/c/c/e;->g:I

    .line 104
    return-void
.end method
