.class final Lcom/google/android/a/c/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/c/b$a;,
        Lcom/google/android/a/c/c/b$b;,
        Lcom/google/android/a/c/c/b$c;
    }
.end annotation


# direct methods
.method private static a(Lcom/google/android/a/f/j;I)Lcom/google/android/a/c/c/b$a;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 624
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 626
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v3, v0, 0x1

    .line 627
    const/4 v0, 0x3

    if-ne v3, v0, :cond_0

    .line 628
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 630
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 631
    const/high16 v0, 0x3f800000    # 1.0f

    .line 632
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    and-int/lit8 v5, v1, 0x1f

    move v1, v2

    .line 633
    :goto_0
    if-ge v1, v5, :cond_1

    .line 634
    invoke-static {p0}, Lcom/google/android/a/f/h;->a(Lcom/google/android/a/f/j;)[B

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 633
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 636
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v6

    move v1, v2

    .line 637
    :goto_1
    if-ge v1, v6, :cond_2

    .line 638
    invoke-static {p0}, Lcom/google/android/a/f/h;->a(Lcom/google/android/a/f/j;)[B

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 637
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 641
    :cond_2
    if-lez v5, :cond_3

    .line 643
    new-instance v1, Lcom/google/android/a/f/i;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, Lcom/google/android/a/f/i;-><init>([B)V

    .line 645
    add-int/lit8 v0, v3, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/i;->a(I)V

    .line 646
    invoke-static {v1}, Lcom/google/android/a/f/c;->a(Lcom/google/android/a/f/i;)Lcom/google/android/a/f/c$a;

    move-result-object v0

    iget v0, v0, Lcom/google/android/a/f/c$a;->c:F

    .line 650
    :cond_3
    new-instance v1, Lcom/google/android/a/c/c/b$a;

    invoke-direct {v1, v4, v3, v0}, Lcom/google/android/a/c/c/b$a;-><init>(Ljava/util/List;IF)V

    return-object v1
.end method

.method private static a(Lcom/google/android/a/f/j;IJILjava/lang/String;Z)Lcom/google/android/a/c/c/b$b;
    .locals 24

    .prologue
    .line 515
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 516
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->k()I

    move-result v22

    .line 517
    new-instance v9, Lcom/google/android/a/c/c/b$b;

    move/from16 v0, v22

    invoke-direct {v9, v0}, Lcom/google/android/a/c/c/b$b;-><init>(I)V

    .line 518
    const/4 v10, 0x0

    :goto_0
    move/from16 v0, v22

    if-ge v10, v0, :cond_9

    .line 519
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->d()I

    move-result v3

    .line 520
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->k()I

    move-result v4

    .line 521
    if-lez v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v5, "childAtomSize should be positive"

    invoke-static {v2, v5}, Lcom/google/android/a/f/b;->a(ZLjava/lang/Object;)V

    .line 522
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->k()I

    move-result v12

    .line 523
    sget v2, Lcom/google/android/a/c/c/a;->b:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->c:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->V:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->af:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->d:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->e:I

    if-eq v12, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/a;->f:I

    if-ne v12, v2, :cond_3

    :cond_0
    move-object/from16 v2, p0

    move/from16 v5, p1

    move-wide/from16 v6, p2

    move/from16 v8, p4

    .line 527
    invoke-static/range {v2 .. v10}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;IIIJILcom/google/android/a/c/c/b$b;I)V

    .line 550
    :cond_1
    :goto_2
    add-int v2, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 518
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 521
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 529
    :cond_3
    sget v2, Lcom/google/android/a/c/c/a;->i:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->W:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->k:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->m:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->o:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->r:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->p:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->q:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->aq:I

    if-eq v12, v2, :cond_4

    sget v2, Lcom/google/android/a/c/c/a;->ar:I

    if-ne v12, v2, :cond_5

    :cond_4
    move-object/from16 v11, p0

    move v13, v3

    move v14, v4

    move/from16 v15, p1

    move-wide/from16 v16, p2

    move-object/from16 v18, p5

    move/from16 v19, p6

    move-object/from16 v20, v9

    move/from16 v21, v10

    .line 534
    invoke-static/range {v11 .. v21}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;IIIIJLjava/lang/String;ZLcom/google/android/a/c/c/b$b;I)V

    goto :goto_2

    .line 536
    :cond_5
    sget v2, Lcom/google/android/a/c/c/a;->ad:I

    if-ne v12, v2, :cond_6

    .line 537
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/ttml+xml"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto :goto_2

    .line 539
    :cond_6
    sget v2, Lcom/google/android/a/c/c/a;->an:I

    if-ne v12, v2, :cond_7

    .line 540
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/x-quicktime-tx3g"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto :goto_2

    .line 542
    :cond_7
    sget v2, Lcom/google/android/a/c/c/a;->ao:I

    if-ne v12, v2, :cond_8

    .line 543
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/x-mp4vtt"

    const/4 v13, -0x1

    move-wide/from16 v14, p2

    move-object/from16 v16, p5

    invoke-static/range {v11 .. v16}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto/16 :goto_2

    .line 545
    :cond_8
    sget v2, Lcom/google/android/a/c/c/a;->ap:I

    if-ne v12, v2, :cond_1

    .line 546
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "application/ttml+xml"

    const/4 v14, -0x1

    const-wide/16 v18, 0x0

    move-wide/from16 v15, p2

    move-object/from16 v17, p5

    invoke-static/range {v12 .. v19}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)Lcom/google/android/a/o;

    move-result-object v2

    iput-object v2, v9, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto/16 :goto_2

    .line 552
    :cond_9
    return-object v9
.end method

.method public static a(Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/a$b;Z)Lcom/google/android/a/c/c/h;
    .locals 27

    .prologue
    .line 50
    sget v2, Lcom/google/android/a/c/c/a;->B:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v8

    .line 51
    sget v2, Lcom/google/android/a/c/c/a;->O:I

    invoke-virtual {v8, v2}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v2}, Lcom/google/android/a/c/c/b;->d(Lcom/google/android/a/f/j;)I

    move-result v15

    .line 52
    sget v2, Lcom/google/android/a/c/c/h;->b:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/h;->a:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/h;->c:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/h;->d:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/android/a/c/c/h;->e:I

    if-eq v15, v2, :cond_0

    .line 54
    const/4 v13, 0x0

    .line 73
    :goto_0
    return-object v13

    .line 57
    :cond_0
    sget v2, Lcom/google/android/a/c/c/a;->K:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v2}, Lcom/google/android/a/c/c/b;->c(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/c/b$c;

    move-result-object v16

    .line 58
    invoke-static/range {v16 .. v16}, Lcom/google/android/a/c/c/b$c;->a(Lcom/google/android/a/c/c/b$c;)J

    move-result-wide v2

    .line 59
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v4}, Lcom/google/android/a/c/c/b;->b(Lcom/google/android/a/f/j;)J

    move-result-wide v6

    .line 61
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 62
    const-wide/16 v10, -0x1

    .line 66
    :goto_1
    sget v2, Lcom/google/android/a/c/c/a;->C:I

    invoke-virtual {v8, v2}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v2

    sget v3, Lcom/google/android/a/c/c/a;->D:I

    invoke-virtual {v2, v3}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v2

    .line 69
    sget v3, Lcom/google/android/a/c/c/a;->N:I

    invoke-virtual {v8, v3}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v3}, Lcom/google/android/a/c/c/b;->e(Lcom/google/android/a/f/j;)Landroid/util/Pair;

    move-result-object v3

    .line 70
    sget v4, Lcom/google/android/a/c/c/a;->P:I

    invoke-virtual {v2, v4}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    iget-object v8, v2, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static/range {v16 .. v16}, Lcom/google/android/a/c/c/b$c;->b(Lcom/google/android/a/c/c/b$c;)I

    move-result v9

    invoke-static/range {v16 .. v16}, Lcom/google/android/a/c/c/b$c;->c(Lcom/google/android/a/c/c/b$c;)I

    move-result v12

    iget-object v13, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v13, Ljava/lang/String;

    move/from16 v14, p2

    invoke-static/range {v8 .. v14}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;IJILjava/lang/String;Z)Lcom/google/android/a/c/c/b$b;

    move-result-object v4

    .line 72
    sget v2, Lcom/google/android/a/c/c/a;->L:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/a/c/c/b;->b(Lcom/google/android/a/c/c/a$a;)Landroid/util/Pair;

    move-result-object v5

    .line 73
    iget-object v2, v4, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    if-nez v2, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    .line 64
    :cond_1
    const-wide/32 v4, 0xf4240

    invoke-static/range {v2 .. v7}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v10

    goto :goto_1

    .line 73
    :cond_2
    new-instance v13, Lcom/google/android/a/c/c/h;

    invoke-static/range {v16 .. v16}, Lcom/google/android/a/c/c/b$c;->b(Lcom/google/android/a/c/c/b$c;)I

    move-result v14

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    iget-object v0, v4, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    move-object/from16 v22, v0

    iget-object v0, v4, Lcom/google/android/a/c/c/b$b;->a:[Lcom/google/android/a/c/c/i;

    move-object/from16 v23, v0

    iget v0, v4, Lcom/google/android/a/c/c/b$b;->c:I

    move/from16 v24, v0

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, [J

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v26, v0

    check-cast v26, [J

    move-wide/from16 v18, v6

    move-wide/from16 v20, v10

    invoke-direct/range {v13 .. v26}, Lcom/google/android/a/c/c/h;-><init>(IIJJJLcom/google/android/a/o;[Lcom/google/android/a/c/c/i;I[J[J)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/a/f/j;II)Lcom/google/android/a/c/c/i;
    .locals 5

    .prologue
    .line 730
    add-int/lit8 v1, p1, 0x8

    .line 732
    const/4 v0, 0x0

    .line 733
    :goto_0
    sub-int v2, v1, p1

    if-ge v2, p2, :cond_3

    .line 734
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 735
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 736
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v3

    .line 737
    sget v4, Lcom/google/android/a/c/c/a;->X:I

    if-ne v3, v4, :cond_1

    .line 738
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    .line 746
    :cond_0
    :goto_1
    add-int/2addr v1, v2

    .line 747
    goto :goto_0

    .line 739
    :cond_1
    sget v4, Lcom/google/android/a/c/c/a;->S:I

    if-ne v3, v4, :cond_2

    .line 740
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 741
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    .line 742
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    goto :goto_1

    .line 743
    :cond_2
    sget v4, Lcom/google/android/a/c/c/a;->T:I

    if-ne v3, v4, :cond_0

    .line 744
    invoke-static {p0, v1, v2}, Lcom/google/android/a/c/c/b;->b(Lcom/google/android/a/f/j;II)Lcom/google/android/a/c/c/i;

    move-result-object v0

    goto :goto_1

    .line 749
    :cond_3
    return-object v0
.end method

.method public static a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/a$a;)Lcom/google/android/a/c/c/k;
    .locals 43

    .prologue
    .line 88
    sget v2, Lcom/google/android/a/c/c/a;->ak:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object/from16 v33, v0

    .line 92
    sget v2, Lcom/google/android/a/c/c/a;->al:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    .line 93
    if-nez v2, :cond_0

    .line 94
    sget v2, Lcom/google/android/a/c/c/a;->am:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v2

    .line 96
    :cond_0
    iget-object v0, v2, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object/from16 v34, v0

    .line 98
    sget v3, Lcom/google/android/a/c/c/a;->aj:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object/from16 v35, v0

    .line 100
    sget v3, Lcom/google/android/a/c/c/a;->ag:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object/from16 v36, v0

    .line 102
    sget v3, Lcom/google/android/a/c/c/a;->ah:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v3

    .line 103
    if-eqz v3, :cond_1

    iget-object v3, v3, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object v8, v3

    .line 105
    :goto_0
    sget v3, Lcom/google/android/a/c/c/a;->ai:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v3

    .line 106
    if-eqz v3, :cond_2

    iget-object v3, v3, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object v9, v3

    .line 109
    :goto_1
    const/16 v3, 0xc

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Lcom/google/android/a/f/j;->b(I)V

    .line 110
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/a/f/j;->o()I

    move-result v13

    .line 111
    invoke-virtual/range {v33 .. v33}, Lcom/google/android/a/f/j;->o()I

    move-result v37

    .line 113
    move/from16 v0, v37

    new-array v3, v0, [J

    .line 114
    move/from16 v0, v37

    new-array v4, v0, [I

    .line 115
    const/4 v5, 0x0

    .line 116
    move/from16 v0, v37

    new-array v6, v0, [J

    .line 117
    move/from16 v0, v37

    new-array v7, v0, [I

    .line 118
    if-nez v37, :cond_3

    .line 119
    new-instance v2, Lcom/google/android/a/c/c/k;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/a/c/c/k;-><init>([J[II[J[I)V

    .line 330
    :goto_2
    return-object v2

    .line 103
    :cond_1
    const/4 v3, 0x0

    move-object v8, v3

    goto :goto_0

    .line 106
    :cond_2
    const/4 v3, 0x0

    move-object v9, v3

    goto :goto_1

    .line 123
    :cond_3
    const/16 v10, 0xc

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Lcom/google/android/a/f/j;->b(I)V

    .line 124
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/a/f/j;->o()I

    move-result v38

    .line 126
    const/16 v10, 0xc

    move-object/from16 v0, v35

    invoke-virtual {v0, v10}, Lcom/google/android/a/f/j;->b(I)V

    .line 127
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->o()I

    move-result v10

    add-int/lit8 v27, v10, -0x1

    .line 128
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->k()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_9

    const/4 v10, 0x1

    :goto_3
    const-string v11, "stsc first chunk must be 1"

    invoke-static {v10, v11}, Lcom/google/android/a/f/b;->b(ZLjava/lang/Object;)V

    .line 129
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->o()I

    move-result v20

    .line 130
    const/4 v10, 0x4

    move-object/from16 v0, v35

    invoke-virtual {v0, v10}, Lcom/google/android/a/f/j;->c(I)V

    .line 131
    const/4 v10, -0x1

    .line 132
    if-lez v27, :cond_4

    .line 134
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->o()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    .line 137
    :cond_4
    const/16 v21, 0x0

    .line 141
    const/16 v11, 0xc

    move-object/from16 v0, v36

    invoke-virtual {v0, v11}, Lcom/google/android/a/f/j;->b(I)V

    .line 142
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/a/f/j;->o()I

    move-result v11

    add-int/lit8 v26, v11, -0x1

    .line 143
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/a/f/j;->o()I

    move-result v25

    .line 144
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/a/f/j;->o()I

    move-result v24

    .line 147
    const/16 v23, 0x0

    .line 148
    const/16 v22, 0x0

    .line 149
    const/16 v17, 0x0

    .line 150
    if-eqz v9, :cond_5

    .line 151
    const/16 v11, 0xc

    invoke-virtual {v9, v11}, Lcom/google/android/a/f/j;->b(I)V

    .line 152
    invoke-virtual {v9}, Lcom/google/android/a/f/j;->o()I

    move-result v11

    add-int/lit8 v22, v11, -0x1

    .line 153
    invoke-virtual {v9}, Lcom/google/android/a/f/j;->o()I

    move-result v23

    .line 159
    invoke-virtual {v9}, Lcom/google/android/a/f/j;->k()I

    move-result v17

    .line 162
    :cond_5
    const/4 v12, -0x1

    .line 163
    const/4 v11, 0x0

    .line 164
    if-eqz v8, :cond_6

    .line 165
    const/16 v11, 0xc

    invoke-virtual {v8, v11}, Lcom/google/android/a/f/j;->b(I)V

    .line 166
    invoke-virtual {v8}, Lcom/google/android/a/f/j;->o()I

    move-result v11

    .line 167
    invoke-virtual {v8}, Lcom/google/android/a/f/j;->o()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    .line 172
    :cond_6
    iget v14, v2, Lcom/google/android/a/c/c/a$b;->az:I

    sget v15, Lcom/google/android/a/c/c/a;->al:I

    if-ne v14, v15, :cond_a

    .line 173
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v14

    .line 178
    :goto_4
    const-wide/16 v18, 0x0

    .line 179
    const/16 v16, 0x0

    move/from16 v29, v16

    move-wide/from16 v30, v18

    move/from16 v32, v20

    move/from16 v19, v24

    move/from16 v16, v10

    move/from16 v18, v27

    move/from16 v24, v12

    move/from16 v12, v21

    move/from16 v42, v11

    move/from16 v11, v17

    move/from16 v17, v20

    move-wide/from16 v20, v14

    move/from16 v15, v23

    move/from16 v14, v22

    move/from16 v22, v26

    move/from16 v23, v42

    :goto_5
    move/from16 v0, v29

    move/from16 v1, v37

    if-ge v0, v1, :cond_f

    .line 180
    aput-wide v20, v3, v29

    .line 181
    if-nez v13, :cond_b

    invoke-virtual/range {v33 .. v33}, Lcom/google/android/a/f/j;->o()I

    move-result v10

    :goto_6
    aput v10, v4, v29

    .line 182
    aget v10, v4, v29

    if-le v10, v5, :cond_7

    .line 183
    aget v5, v4, v29

    .line 185
    :cond_7
    int-to-long v0, v11

    move-wide/from16 v26, v0

    add-long v26, v26, v30

    aput-wide v26, v6, v29

    .line 188
    if-nez v8, :cond_c

    const/4 v10, 0x1

    :goto_7
    aput v10, v7, v29

    .line 189
    move/from16 v0, v29

    move/from16 v1, v24

    if-ne v0, v1, :cond_2c

    .line 190
    const/4 v10, 0x1

    aput v10, v7, v29

    .line 191
    add-int/lit8 v10, v23, -0x1

    .line 192
    if-lez v10, :cond_2b

    .line 193
    invoke-virtual {v8}, Lcom/google/android/a/f/j;->o()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v27, v10

    move/from16 v28, v23

    .line 198
    :goto_8
    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v40, v0

    add-long v30, v30, v40

    .line 199
    add-int/lit8 v10, v25, -0x1

    .line 200
    if-nez v10, :cond_2a

    if-lez v22, :cond_2a

    .line 201
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/a/f/j;->o()I

    move-result v19

    .line 202
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/a/f/j;->o()I

    move-result v10

    .line 203
    add-int/lit8 v22, v22, -0x1

    move/from16 v24, v10

    move/from16 v25, v19

    move/from16 v26, v22

    .line 207
    :goto_9
    if-eqz v9, :cond_29

    .line 208
    add-int/lit8 v10, v15, -0x1

    .line 209
    if-nez v10, :cond_28

    if-lez v14, :cond_28

    .line 210
    invoke-virtual {v9}, Lcom/google/android/a/f/j;->o()I

    move-result v15

    .line 212
    invoke-virtual {v9}, Lcom/google/android/a/f/j;->k()I

    move-result v10

    .line 213
    add-int/lit8 v11, v14, -0x1

    move/from16 v19, v10

    move/from16 v22, v11

    move/from16 v23, v15

    .line 218
    :goto_a
    add-int/lit8 v14, v32, -0x1

    .line 219
    if-nez v14, :cond_e

    .line 220
    add-int/lit8 v15, v12, 0x1

    .line 221
    move/from16 v0, v38

    if-ge v15, v0, :cond_27

    .line 222
    iget v10, v2, Lcom/google/android/a/c/c/a$b;->az:I

    sget v11, Lcom/google/android/a/c/c/a;->al:I

    if-ne v10, v11, :cond_d

    .line 223
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v10

    .line 230
    :goto_b
    move/from16 v0, v16

    if-ne v15, v0, :cond_26

    .line 231
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->o()I

    move-result v12

    .line 232
    const/16 v17, 0x4

    move-object/from16 v0, v35

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 233
    add-int/lit8 v17, v18, -0x1

    .line 234
    if-lez v17, :cond_8

    .line 235
    invoke-virtual/range {v35 .. v35}, Lcom/google/android/a/f/j;->o()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    .line 240
    :cond_8
    :goto_c
    move/from16 v0, v38

    if-ge v15, v0, :cond_25

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v12

    .line 179
    :goto_d
    add-int/lit8 v18, v29, 0x1

    move/from16 v29, v18

    move-wide/from16 v20, v10

    move/from16 v32, v12

    move/from16 v11, v19

    move v12, v14

    move/from16 v18, v17

    move/from16 v19, v24

    move/from16 v17, v16

    move/from16 v14, v22

    move/from16 v24, v28

    move/from16 v16, v15

    move/from16 v22, v26

    move/from16 v15, v23

    move/from16 v23, v27

    goto/16 :goto_5

    .line 128
    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_3

    .line 175
    :cond_a
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v14

    goto/16 :goto_4

    :cond_b
    move v10, v13

    .line 181
    goto/16 :goto_6

    .line 188
    :cond_c
    const/4 v10, 0x0

    goto/16 :goto_7

    .line 225
    :cond_d
    invoke-virtual/range {v34 .. v34}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v10

    goto :goto_b

    .line 245
    :cond_e
    aget v10, v4, v29

    int-to-long v10, v10

    add-long v10, v10, v20

    move/from16 v15, v16

    move/from16 v16, v17

    move/from16 v17, v18

    move/from16 v42, v12

    move v12, v14

    move/from16 v14, v42

    goto :goto_d

    .line 250
    :cond_f
    if-nez v23, :cond_10

    const/4 v2, 0x1

    :goto_e
    invoke-static {v2}, Lcom/google/android/a/f/b;->a(Z)V

    .line 251
    if-nez v25, :cond_11

    const/4 v2, 0x1

    :goto_f
    invoke-static {v2}, Lcom/google/android/a/f/b;->a(Z)V

    .line 252
    if-nez v32, :cond_12

    const/4 v2, 0x1

    :goto_10
    invoke-static {v2}, Lcom/google/android/a/f/b;->a(Z)V

    .line 253
    if-nez v22, :cond_13

    const/4 v2, 0x1

    :goto_11
    invoke-static {v2}, Lcom/google/android/a/f/b;->a(Z)V

    .line 254
    if-nez v14, :cond_14

    const/4 v2, 0x1

    :goto_12
    invoke-static {v2}, Lcom/google/android/a/f/b;->a(Z)V

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/a/c/c/h;->m:[J

    if-nez v2, :cond_15

    .line 257
    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/a/c/c/h;->h:J

    invoke-static {v6, v8, v9, v10, v11}, Lcom/google/android/a/f/n;->a([JJJ)V

    .line 258
    new-instance v2, Lcom/google/android/a/c/c/k;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/a/c/c/k;-><init>([J[II[J[I)V

    goto/16 :goto_2

    .line 250
    :cond_10
    const/4 v2, 0x0

    goto :goto_e

    .line 251
    :cond_11
    const/4 v2, 0x0

    goto :goto_f

    .line 252
    :cond_12
    const/4 v2, 0x0

    goto :goto_10

    .line 253
    :cond_13
    const/4 v2, 0x0

    goto :goto_11

    .line 254
    :cond_14
    const/4 v2, 0x0

    goto :goto_12

    .line 266
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/a/c/c/h;->m:[J

    array-length v2, v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/a/c/c/h;->m:[J

    const/4 v8, 0x0

    aget-wide v8, v2, v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_17

    .line 270
    const/4 v2, 0x0

    :goto_13
    array-length v8, v6

    if-ge v2, v8, :cond_16

    .line 271
    aget-wide v8, v6, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/a/c/c/h;->n:[J

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/a/c/c/h;->h:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 270
    add-int/lit8 v2, v2, 0x1

    goto :goto_13

    .line 274
    :cond_16
    new-instance v2, Lcom/google/android/a/c/c/k;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/a/c/c/k;-><init>([J[II[J[I)V

    goto/16 :goto_2

    .line 278
    :cond_17
    const/4 v10, 0x0

    .line 279
    const/4 v9, 0x0

    .line 280
    const/4 v8, 0x0

    .line 281
    const/4 v2, 0x0

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    :goto_14
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->m:[J

    array-length v8, v8

    if-ge v2, v8, :cond_19

    .line 282
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->n:[J

    aget-wide v18, v8, v2

    .line 283
    const-wide/16 v8, -0x1

    cmp-long v8, v18, v8

    if-eqz v8, :cond_24

    .line 284
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->m:[J

    aget-wide v8, v8, v2

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/a/c/c/h;->h:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/a/c/c/h;->i:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v8

    .line 286
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v18

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/android/a/f/n;->b([JJZZ)I

    move-result v11

    .line 287
    add-long v8, v8, v18

    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-static {v6, v8, v9, v10, v12}, Lcom/google/android/a/f/n;->b([JJZZ)I

    move-result v9

    .line 288
    sub-int v8, v9, v11

    add-int v10, v16, v8

    .line 289
    if-eq v15, v11, :cond_18

    const/4 v8, 0x1

    :goto_15
    or-int/2addr v8, v14

    .line 281
    :goto_16
    add-int/lit8 v2, v2, 0x1

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    goto :goto_14

    .line 289
    :cond_18
    const/4 v8, 0x0

    goto :goto_15

    .line 293
    :cond_19
    move/from16 v0, v16

    move/from16 v1, v37

    if-eq v0, v1, :cond_1c

    const/4 v2, 0x1

    :goto_17
    or-int v23, v14, v2

    .line 296
    if-eqz v23, :cond_1d

    move/from16 v0, v16

    new-array v2, v0, [J

    move-object/from16 v22, v2

    .line 297
    :goto_18
    if-eqz v23, :cond_1e

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v21, v2

    .line 298
    :goto_19
    if-eqz v23, :cond_1f

    const/4 v10, 0x0

    .line 299
    :goto_1a
    if-eqz v23, :cond_20

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v17, v2

    .line 300
    :goto_1b
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v24, v0

    .line 301
    const-wide/16 v8, 0x0

    .line 302
    const/4 v5, 0x0

    .line 303
    const/4 v2, 0x0

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    :goto_1c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->m:[J

    array-length v8, v8

    if-ge v2, v8, :cond_22

    .line 304
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->n:[J

    aget-wide v26, v8, v2

    .line 305
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/a/c/c/h;->m:[J

    aget-wide v8, v8, v2

    .line 306
    const-wide/16 v10, -0x1

    cmp-long v10, v26, v10

    if-eqz v10, :cond_23

    .line 307
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/a/c/c/h;->h:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/a/c/c/h;->i:J

    invoke-static/range {v8 .. v13}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v10

    add-long v12, v26, v10

    .line 309
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v26

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/android/a/f/n;->b([JJZZ)I

    move-result v10

    .line 310
    const/4 v11, 0x1

    const/4 v15, 0x0

    invoke-static {v6, v12, v13, v11, v15}, Lcom/google/android/a/f/n;->b([JJZZ)I

    move-result v25

    .line 311
    if-eqz v23, :cond_1a

    .line 312
    sub-int v11, v25, v10

    .line 313
    move-object/from16 v0, v22

    invoke-static {v3, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 314
    move-object/from16 v0, v21

    invoke-static {v4, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 315
    move-object/from16 v0, v17

    invoke-static {v7, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1a
    move/from16 v20, v10

    move/from16 v16, v14

    .line 317
    :goto_1d
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_21

    .line 318
    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/a/c/c/h;->i:J

    move-wide/from16 v10, v18

    invoke-static/range {v10 .. v15}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v28

    .line 319
    aget-wide v10, v6, v20

    sub-long v10, v10, v26

    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/a/c/c/h;->h:J

    invoke-static/range {v10 .. v15}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v10

    .line 321
    add-long v10, v10, v28

    aput-wide v10, v24, v16

    .line 322
    if-eqz v23, :cond_1b

    aget v10, v21, v16

    if-le v10, v5, :cond_1b

    .line 323
    aget v5, v4, v20

    .line 325
    :cond_1b
    add-int/lit8 v16, v16, 0x1

    .line 317
    add-int/lit8 v10, v20, 0x1

    move/from16 v20, v10

    goto :goto_1d

    .line 293
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_17

    :cond_1d
    move-object/from16 v22, v3

    .line 296
    goto/16 :goto_18

    :cond_1e
    move-object/from16 v21, v4

    .line 297
    goto/16 :goto_19

    :cond_1f
    move v10, v5

    .line 298
    goto/16 :goto_1a

    :cond_20
    move-object/from16 v17, v7

    .line 299
    goto/16 :goto_1b

    :cond_21
    move v10, v5

    move/from16 v5, v16

    .line 328
    :goto_1e
    add-long v8, v8, v18

    .line 303
    add-int/lit8 v2, v2, 0x1

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    goto/16 :goto_1c

    .line 330
    :cond_22
    new-instance v2, Lcom/google/android/a/c/c/k;

    move-object/from16 v3, v22

    move-object/from16 v4, v21

    move-object/from16 v6, v24

    move-object/from16 v7, v17

    invoke-direct/range {v2 .. v7}, Lcom/google/android/a/c/c/k;-><init>([J[II[J[I)V

    goto/16 :goto_2

    :cond_23
    move v10, v5

    move v5, v14

    goto :goto_1e

    :cond_24
    move v8, v14

    move v9, v15

    move/from16 v10, v16

    goto/16 :goto_16

    :cond_25
    move/from16 v42, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v12

    move/from16 v12, v42

    goto/16 :goto_d

    :cond_26
    move/from16 v12, v17

    move/from16 v17, v18

    goto/16 :goto_c

    :cond_27
    move-wide/from16 v10, v20

    goto/16 :goto_b

    :cond_28
    move/from16 v19, v11

    move/from16 v22, v14

    move/from16 v23, v10

    goto/16 :goto_a

    :cond_29
    move/from16 v19, v11

    move/from16 v22, v14

    move/from16 v23, v15

    goto/16 :goto_a

    :cond_2a
    move/from16 v24, v19

    move/from16 v25, v10

    move/from16 v26, v22

    goto/16 :goto_9

    :cond_2b
    move/from16 v27, v10

    move/from16 v28, v24

    goto/16 :goto_8

    :cond_2c
    move/from16 v27, v23

    move/from16 v28, v24

    goto/16 :goto_8
.end method

.method public static a(Lcom/google/android/a/c/c/a$a;)Lcom/google/android/a/c/i;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 341
    sget v1, Lcom/google/android/a/c/c/a;->at:I

    invoke-virtual {p0, v1}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v1

    .line 342
    if-nez v1, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-object v0

    .line 345
    :cond_1
    iget-object v2, v1, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    .line 346
    const/16 v1, 0xc

    invoke-virtual {v2, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 347
    new-instance v3, Lcom/google/android/a/f/j;

    invoke-direct {v3}, Lcom/google/android/a/f/j;-><init>()V

    .line 348
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/a/f/j;->b()I

    move-result v1

    if-lez v1, :cond_0

    .line 349
    invoke-virtual {v2}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    add-int/lit8 v4, v1, -0x8

    .line 350
    invoke-virtual {v2}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    .line 351
    sget v5, Lcom/google/android/a/c/c/a;->au:I

    if-ne v1, v5, :cond_2

    .line 352
    iget-object v1, v2, Lcom/google/android/a/f/j;->a:[B

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->d()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {v3, v1, v5}, Lcom/google/android/a/f/j;->a([BI)V

    .line 353
    invoke-virtual {v2}, Lcom/google/android/a/f/j;->d()I

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 354
    invoke-static {v3}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/i;

    move-result-object v1

    .line 355
    if-eqz v1, :cond_2

    move-object v0, v1

    .line 356
    goto :goto_0

    .line 359
    :cond_2
    invoke-virtual {v2, v4}, Lcom/google/android/a/f/j;->c(I)V

    goto :goto_1
.end method

.method private static a(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/i;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x0

    .line 365
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    if-lez v0, :cond_5

    .line 366
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v0

    .line 367
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    add-int v4, v0, v1

    .line 368
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 369
    sget v1, Lcom/google/android/a/c/c/a;->ay:I

    if-ne v0, v1, :cond_6

    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    .line 373
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v5

    if-ge v5, v4, :cond_4

    .line 374
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    .line 375
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v6

    .line 376
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/j;->c(I)V

    .line 377
    sget v7, Lcom/google/android/a/c/c/a;->av:I

    if-ne v6, v7, :cond_1

    .line 378
    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->d(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 379
    :cond_1
    sget v7, Lcom/google/android/a/c/c/a;->aw:I

    if-ne v6, v7, :cond_2

    .line 380
    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->d(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 381
    :cond_2
    sget v7, Lcom/google/android/a/c/c/a;->ax:I

    if-ne v6, v7, :cond_3

    .line 382
    invoke-virtual {p0, v8}, Lcom/google/android/a/f/j;->c(I)V

    .line 383
    add-int/lit8 v0, v5, -0x4

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->d(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 385
    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->c(I)V

    goto :goto_1

    .line 388
    :cond_4
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v4, "com.apple.iTunes"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    invoke-static {v1, v0}, Lcom/google/android/a/c/i;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/a/c/i;

    move-result-object v3

    .line 396
    :cond_5
    return-object v3

    .line 393
    :cond_6
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->b(I)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/f/j;IIIIJLjava/lang/String;ZLcom/google/android/a/c/c/b$b;I)V
    .locals 15

    .prologue
    .line 783
    add-int/lit8 v4, p2, 0x8

    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->b(I)V

    .line 785
    const/4 v4, 0x0

    .line 786
    if-eqz p8, :cond_6

    .line 787
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 788
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v4

    .line 789
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->c(I)V

    .line 794
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v10

    .line 795
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v7

    .line 796
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->c(I)V

    .line 797
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->m()I

    move-result v11

    .line 799
    if-lez v4, :cond_0

    .line 800
    const/16 v5, 0x10

    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->c(I)V

    .line 801
    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 802
    const/16 v4, 0x14

    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 807
    :cond_0
    const/4 v4, 0x0

    .line 808
    sget v5, Lcom/google/android/a/c/c/a;->k:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_7

    .line 809
    const-string v4, "audio/ac3"

    .line 824
    :cond_1
    :goto_1
    const/4 v8, 0x0

    .line 825
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v9

    move-object v5, v4

    .line 826
    :goto_2
    sub-int v4, v9, p2

    move/from16 v0, p3

    if-ge v4, v0, :cond_17

    .line 827
    invoke-virtual {p0, v9}, Lcom/google/android/a/f/j;->b(I)V

    .line 828
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v12

    .line 829
    if-lez v12, :cond_e

    const/4 v4, 0x1

    :goto_3
    const-string v6, "childAtomSize should be positive"

    invoke-static {v4, v6}, Lcom/google/android/a/f/b;->a(ZLjava/lang/Object;)V

    .line 830
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v6

    .line 831
    sget v4, Lcom/google/android/a/c/c/a;->i:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_2

    sget v4, Lcom/google/android/a/c/c/a;->W:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_12

    .line 832
    :cond_2
    const/4 v4, -0x1

    .line 833
    sget v13, Lcom/google/android/a/c/c/a;->G:I

    if-ne v6, v13, :cond_f

    move v4, v9

    .line 838
    :cond_3
    :goto_4
    const/4 v13, -0x1

    if-eq v4, v13, :cond_10

    .line 839
    invoke-static {p0, v4}, Lcom/google/android/a/c/c/b;->d(Lcom/google/android/a/f/j;I)Landroid/util/Pair;

    move-result-object v6

    .line 841
    iget-object v4, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v5, v4

    check-cast v5, Ljava/lang/String;

    .line 842
    iget-object v4, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v6, v4

    check-cast v6, [B

    .line 843
    const-string v4, "audio/mp4a-latm"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 846
    invoke-static {v6}, Lcom/google/android/a/f/c;->a([B)Landroid/util/Pair;

    move-result-object v8

    .line 848
    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 849
    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    :cond_4
    :goto_5
    move-object v8, v6

    .line 875
    :cond_5
    add-int/2addr v9, v12

    .line 876
    goto :goto_2

    .line 791
    :cond_6
    const/16 v5, 0x10

    invoke-virtual {p0, v5}, Lcom/google/android/a/f/j;->c(I)V

    goto/16 :goto_0

    .line 810
    :cond_7
    sget v5, Lcom/google/android/a/c/c/a;->m:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_8

    .line 811
    const-string v4, "audio/eac3"

    goto :goto_1

    .line 812
    :cond_8
    sget v5, Lcom/google/android/a/c/c/a;->o:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_9

    .line 813
    const-string v4, "audio/vnd.dts"

    goto :goto_1

    .line 814
    :cond_9
    sget v5, Lcom/google/android/a/c/c/a;->p:I

    move/from16 v0, p1

    if-eq v0, v5, :cond_a

    sget v5, Lcom/google/android/a/c/c/a;->q:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_b

    .line 815
    :cond_a
    const-string v4, "audio/vnd.dts.hd"

    goto/16 :goto_1

    .line 816
    :cond_b
    sget v5, Lcom/google/android/a/c/c/a;->r:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_c

    .line 817
    const-string v4, "audio/vnd.dts.hd;profile=lbr"

    goto/16 :goto_1

    .line 818
    :cond_c
    sget v5, Lcom/google/android/a/c/c/a;->aq:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_d

    .line 819
    const-string v4, "audio/3gpp"

    goto/16 :goto_1

    .line 820
    :cond_d
    sget v5, Lcom/google/android/a/c/c/a;->ar:I

    move/from16 v0, p1

    if-ne v0, v5, :cond_1

    .line 821
    const-string v4, "audio/amr-wb"

    goto/16 :goto_1

    .line 829
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 835
    :cond_f
    if-eqz p8, :cond_3

    sget v13, Lcom/google/android/a/c/c/a;->j:I

    if-ne v6, v13, :cond_3

    .line 836
    invoke-static {p0, v9, v12}, Lcom/google/android/a/c/c/b;->c(Lcom/google/android/a/f/j;II)I

    move-result v4

    goto/16 :goto_4

    .line 851
    :cond_10
    sget v4, Lcom/google/android/a/c/c/a;->R:I

    if-ne v6, v4, :cond_11

    .line 852
    move-object/from16 v0, p9

    iget-object v4, v0, Lcom/google/android/a/c/c/b$b;->a:[Lcom/google/android/a/c/c/i;

    invoke-static {p0, v9, v12}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;II)Lcom/google/android/a/c/c/i;

    move-result-object v6

    aput-object v6, v4, p10

    :cond_11
    move-object v6, v8

    goto :goto_5

    .line 855
    :cond_12
    sget v4, Lcom/google/android/a/c/c/a;->k:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_14

    sget v4, Lcom/google/android/a/c/c/a;->l:I

    if-ne v6, v4, :cond_14

    .line 858
    add-int/lit8 v4, v9, 0x8

    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->b(I)V

    .line 859
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-wide/from16 v0, p5

    move-object/from16 v2, p7

    invoke-static {p0, v4, v0, v1, v2}, Lcom/google/android/a/f/a;->a(Lcom/google/android/a/f/j;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    .line 887
    :cond_13
    :goto_6
    return-void

    .line 862
    :cond_14
    sget v4, Lcom/google/android/a/c/c/a;->m:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_15

    sget v4, Lcom/google/android/a/c/c/a;->n:I

    if-ne v6, v4, :cond_15

    .line 863
    add-int/lit8 v4, v9, 0x8

    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->b(I)V

    .line 864
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-wide/from16 v0, p5

    move-object/from16 v2, p7

    invoke-static {p0, v4, v0, v1, v2}, Lcom/google/android/a/f/a;->b(Lcom/google/android/a/f/j;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto :goto_6

    .line 867
    :cond_15
    sget v4, Lcom/google/android/a/c/c/a;->o:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_16

    sget v4, Lcom/google/android/a/c/c/a;->r:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_16

    sget v4, Lcom/google/android/a/c/c/a;->p:I

    move/from16 v0, p1

    if-eq v0, v4, :cond_16

    sget v4, Lcom/google/android/a/c/c/a;->q:I

    move/from16 v0, p1

    if-ne v0, v4, :cond_5

    :cond_16
    sget v4, Lcom/google/android/a/c/c/a;->s:I

    if-ne v6, v4, :cond_5

    .line 870
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v12, 0x0

    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    invoke-static/range {v4 .. v13}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/a/o;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto :goto_6

    .line 879
    :cond_17
    if-eqz v5, :cond_13

    .line 883
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, -0x1

    if-nez v8, :cond_18

    const/4 v12, 0x0

    :goto_7
    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    invoke-static/range {v4 .. v13}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/a/o;

    move-result-object v4

    move-object/from16 v0, p9

    iput-object v4, v0, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto :goto_6

    :cond_18
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    goto :goto_7
.end method

.method private static a(Lcom/google/android/a/f/j;IIIJILcom/google/android/a/c/c/b$b;I)V
    .locals 14

    .prologue
    .line 557
    add-int/lit8 v2, p1, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 559
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 560
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v8

    .line 561
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v9

    .line 562
    const/4 v5, 0x0

    .line 563
    const/high16 v12, 0x3f800000    # 1.0f

    .line 564
    const/16 v2, 0x32

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 566
    const/4 v10, 0x0

    .line 567
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v2

    .line 568
    const/4 v3, 0x0

    move v6, v2

    .line 569
    :goto_0
    sub-int v2, v6, p1

    move/from16 v0, p2

    if-ge v2, v0, :cond_0

    .line 570
    invoke-virtual {p0, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 571
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v7

    .line 572
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v11

    .line 573
    if-nez v11, :cond_1

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v2

    sub-int/2addr v2, p1

    move/from16 v0, p2

    if-ne v2, v0, :cond_1

    .line 614
    :cond_0
    if-nez v3, :cond_d

    .line 621
    :goto_1
    return-void

    .line 577
    :cond_1
    if-lez v11, :cond_3

    const/4 v2, 0x1

    :goto_2
    const-string v4, "childAtomSize should be positive"

    invoke-static {v2, v4}, Lcom/google/android/a/f/b;->a(ZLjava/lang/Object;)V

    .line 578
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 579
    sget v4, Lcom/google/android/a/c/c/a;->E:I

    if-ne v2, v4, :cond_5

    .line 580
    if-nez v3, :cond_4

    const/4 v2, 0x1

    :goto_3
    invoke-static {v2}, Lcom/google/android/a/f/b;->b(Z)V

    .line 581
    const-string v3, "video/avc"

    .line 582
    invoke-static {p0, v7}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;I)Lcom/google/android/a/c/c/b$a;

    move-result-object v2

    .line 583
    iget-object v10, v2, Lcom/google/android/a/c/c/b$a;->a:Ljava/util/List;

    .line 584
    iget v4, v2, Lcom/google/android/a/c/c/b$a;->b:I

    move-object/from16 v0, p7

    iput v4, v0, Lcom/google/android/a/c/c/b$b;->c:I

    .line 585
    if-nez v5, :cond_2

    .line 586
    iget v12, v2, Lcom/google/android/a/c/c/b$a;->c:F

    :cond_2
    move v2, v5

    .line 610
    :goto_4
    add-int v4, v6, v11

    move v6, v4

    move v5, v2

    .line 611
    goto :goto_0

    .line 577
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 580
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 588
    :cond_5
    sget v4, Lcom/google/android/a/c/c/a;->F:I

    if-ne v2, v4, :cond_7

    .line 589
    if-nez v3, :cond_6

    const/4 v2, 0x1

    :goto_5
    invoke-static {v2}, Lcom/google/android/a/f/b;->b(Z)V

    .line 590
    const-string v4, "video/hevc"

    .line 591
    invoke-static {p0, v7}, Lcom/google/android/a/c/c/b;->b(Lcom/google/android/a/f/j;I)Landroid/util/Pair;

    move-result-object v7

    .line 592
    iget-object v2, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v3, v2

    check-cast v3, Ljava/util/List;

    .line 593
    iget-object v2, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p7

    iput v2, v0, Lcom/google/android/a/c/c/b$b;->c:I

    move-object v10, v3

    move v2, v5

    move-object v3, v4

    .line 594
    goto :goto_4

    .line 589
    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    .line 594
    :cond_7
    sget v4, Lcom/google/android/a/c/c/a;->g:I

    if-ne v2, v4, :cond_9

    .line 595
    if-nez v3, :cond_8

    const/4 v2, 0x1

    :goto_6
    invoke-static {v2}, Lcom/google/android/a/f/b;->b(Z)V

    .line 596
    const-string v3, "video/3gpp"

    move v2, v5

    goto :goto_4

    .line 595
    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    .line 597
    :cond_9
    sget v4, Lcom/google/android/a/c/c/a;->G:I

    if-ne v2, v4, :cond_b

    .line 598
    if-nez v3, :cond_a

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, Lcom/google/android/a/f/b;->b(Z)V

    .line 599
    invoke-static {p0, v7}, Lcom/google/android/a/c/c/b;->d(Lcom/google/android/a/f/j;I)Landroid/util/Pair;

    move-result-object v3

    .line 601
    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 602
    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    move-object v3, v2

    move v2, v5

    .line 603
    goto :goto_4

    .line 598
    :cond_a
    const/4 v2, 0x0

    goto :goto_7

    .line 603
    :cond_b
    sget v4, Lcom/google/android/a/c/c/a;->R:I

    if-ne v2, v4, :cond_c

    .line 604
    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/google/android/a/c/c/b$b;->a:[Lcom/google/android/a/c/c/i;

    invoke-static {p0, v7, v11}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/f/j;II)Lcom/google/android/a/c/c/i;

    move-result-object v4

    aput-object v4, v2, p8

    move v2, v5

    goto :goto_4

    .line 606
    :cond_c
    sget v4, Lcom/google/android/a/c/c/a;->ac:I

    if-ne v2, v4, :cond_e

    .line 607
    invoke-static {p0, v7}, Lcom/google/android/a/c/c/b;->c(Lcom/google/android/a/f/j;I)F

    move-result v12

    .line 608
    const/4 v2, 0x1

    goto :goto_4

    .line 618
    :cond_d
    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    const/4 v5, -0x1

    move-wide/from16 v6, p4

    move/from16 v11, p6

    invoke-static/range {v2 .. v12}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/android/a/o;

    move-result-object v2

    move-object/from16 v0, p7

    iput-object v2, v0, Lcom/google/android/a/c/c/b$b;->b:Lcom/google/android/a/o;

    goto/16 :goto_1

    :cond_e
    move v2, v5

    goto/16 :goto_4
.end method

.method private static b(Lcom/google/android/a/f/j;)J
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 406
    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 408
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    .line 409
    invoke-static {v1}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v1

    .line 411
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v0

    return-wide v0

    .line 411
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static b(Lcom/google/android/a/c/c/a$a;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/c/c/a$a;",
            ")",
            "Landroid/util/Pair",
            "<[J[J>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 704
    if-eqz p0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->M:I

    invoke-virtual {p0, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 705
    :cond_0
    invoke-static {v1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 725
    :goto_0
    return-object v0

    .line 707
    :cond_1
    iget-object v3, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    .line 708
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 709
    invoke-virtual {v3}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 710
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v4

    .line 711
    invoke-virtual {v3}, Lcom/google/android/a/f/j;->o()I

    move-result v5

    .line 712
    new-array v6, v5, [J

    .line 713
    new-array v7, v5, [J

    .line 714
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    .line 715
    if-ne v4, v8, :cond_2

    invoke-virtual {v3}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v0

    :goto_2
    aput-wide v0, v6, v2

    .line 717
    if-ne v4, v8, :cond_3

    invoke-virtual {v3}, Lcom/google/android/a/f/j;->l()J

    move-result-wide v0

    :goto_3
    aput-wide v0, v7, v2

    .line 718
    invoke-virtual {v3}, Lcom/google/android/a/f/j;->h()S

    move-result v0

    .line 719
    if-eq v0, v8, :cond_4

    .line 721
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported media rate."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v0

    goto :goto_2

    .line 717
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    int-to-long v0, v0

    goto :goto_3

    .line 723
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 714
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 725
    :cond_5
    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/a/f/j;I)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/f/j;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 656
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x15

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 657
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 660
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v6

    .line 662
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v7

    move v3, v1

    move v4, v1

    .line 663
    :goto_0
    if-ge v3, v6, :cond_1

    .line 664
    invoke-virtual {p0, v12}, Lcom/google/android/a/f/j;->c(I)V

    .line 665
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 666
    :goto_1
    if-ge v0, v8, :cond_0

    .line 667
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v4

    .line 668
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 669
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 666
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 663
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 674
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 675
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 677
    :goto_2
    if-ge v3, v6, :cond_3

    .line 678
    invoke-virtual {p0, v12}, Lcom/google/android/a/f/j;->c(I)V

    .line 679
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 680
    :goto_3
    if-ge v0, v8, :cond_2

    .line 681
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v9

    .line 682
    sget-object v10, Lcom/google/android/a/f/h;->a:[B

    sget-object v11, Lcom/google/android/a/f/h;->a:[B

    array-length v11, v11

    invoke-static {v10, v1, v7, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 684
    sget-object v10, Lcom/google/android/a/f/h;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 685
    iget-object v10, p0, Lcom/google/android/a/f/j;->a:[B

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v11

    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 686
    add-int/2addr v2, v9

    .line 687
    invoke-virtual {p0, v9}, Lcom/google/android/a/f/j;->c(I)V

    .line 680
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 677
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 691
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 692
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 691
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_4
.end method

.method private static b(Lcom/google/android/a/f/j;II)Lcom/google/android/a/c/c/i;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 761
    add-int/lit8 v2, p1, 0x8

    .line 762
    :goto_0
    sub-int v3, v2, p1

    if-ge v3, p2, :cond_2

    .line 763
    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 764
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v3

    .line 765
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v4

    .line 766
    sget v5, Lcom/google/android/a/c/c/a;->U:I

    if-ne v4, v5, :cond_1

    .line 767
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 768
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 769
    shr-int/lit8 v3, v2, 0x8

    if-ne v3, v0, :cond_0

    .line 770
    :goto_1
    and-int/lit16 v2, v2, 0xff

    .line 771
    const/16 v3, 0x10

    new-array v3, v3, [B

    .line 772
    array-length v4, v3

    invoke-virtual {p0, v3, v1, v4}, Lcom/google/android/a/f/j;->a([BII)V

    .line 773
    new-instance v1, Lcom/google/android/a/c/c/i;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/a/c/c/i;-><init>(ZI[B)V

    move-object v0, v1

    .line 777
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 769
    goto :goto_1

    .line 775
    :cond_1
    add-int/2addr v2, v3

    .line 776
    goto :goto_0

    .line 777
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static c(Lcom/google/android/a/f/j;I)F
    .locals 2

    .prologue
    .line 753
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 754
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    .line 755
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v1

    .line 756
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private static c(Lcom/google/android/a/f/j;II)I
    .locals 4

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v1

    .line 892
    :goto_0
    sub-int v0, v1, p1

    if-ge v0, p2, :cond_2

    .line 893
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 894
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 895
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v0, v3}, Lcom/google/android/a/f/b;->a(ZLjava/lang/Object;)V

    .line 896
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 897
    sget v3, Lcom/google/android/a/c/c/a;->G:I

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 902
    :goto_2
    return v0

    .line 895
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 900
    :cond_1
    add-int/2addr v1, v2

    .line 901
    goto :goto_0

    .line 902
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private static c(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/c/b$c;
    .locals 11

    .prologue
    const/16 v2, 0x10

    const/16 v1, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 422
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 423
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 424
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v6

    .line 426
    if-nez v6, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 427
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v7

    .line 429
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 430
    const/4 v0, 0x1

    .line 431
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v8

    .line 432
    if-nez v6, :cond_0

    move v1, v3

    :cond_0
    move v5, v4

    .line 433
    :goto_1
    if-ge v5, v1, :cond_1

    .line 434
    iget-object v9, p0, Lcom/google/android/a/f/j;->a:[B

    add-int v10, v8, v5

    aget-byte v9, v9, v10

    const/4 v10, -0x1

    if-eq v9, v10, :cond_3

    move v0, v4

    .line 440
    :cond_1
    if-eqz v0, :cond_4

    .line 441
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 442
    const-wide/16 v0, -0x1

    .line 447
    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 449
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v5

    .line 450
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 451
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v3

    .line 452
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v6

    .line 455
    const/high16 v8, 0x10000

    .line 456
    if-nez v2, :cond_6

    if-ne v5, v8, :cond_6

    neg-int v9, v8

    if-ne v3, v9, :cond_6

    if-nez v6, :cond_6

    .line 457
    const/16 v2, 0x5a

    .line 467
    :goto_3
    new-instance v3, Lcom/google/android/a/c/c/b$c;

    invoke-direct {v3, v7, v0, v1, v2}, Lcom/google/android/a/c/c/b$c;-><init>(IJI)V

    return-object v3

    :cond_2
    move v0, v2

    .line 426
    goto :goto_0

    .line 433
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 444
    :cond_4
    if-nez v6, :cond_5

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v0

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v0

    goto :goto_2

    .line 458
    :cond_6
    if-nez v2, :cond_7

    neg-int v9, v8

    if-ne v5, v9, :cond_7

    if-ne v3, v8, :cond_7

    if-nez v6, :cond_7

    .line 459
    const/16 v2, 0x10e

    goto :goto_3

    .line 460
    :cond_7
    neg-int v9, v8

    if-ne v2, v9, :cond_8

    if-nez v5, :cond_8

    if-nez v3, :cond_8

    neg-int v2, v8

    if-ne v6, v2, :cond_8

    .line 461
    const/16 v2, 0xb4

    goto :goto_3

    :cond_8
    move v2, v4

    .line 464
    goto :goto_3
.end method

.method private static d(Lcom/google/android/a/f/j;)I
    .locals 1

    .prologue
    .line 477
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    return v0
.end method

.method private static d(Lcom/google/android/a/f/j;I)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/f/j;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x7f

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 907
    add-int/lit8 v1, p1, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 909
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 910
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    .line 911
    :goto_0
    if-le v1, v5, :cond_0

    .line 912
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    goto :goto_0

    .line 914
    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 916
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    .line 917
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 918
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 920
    :cond_1
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_2

    .line 921
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 923
    :cond_2
    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_3

    .line 924
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 928
    :cond_3
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 929
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    .line 930
    :goto_1
    if-le v1, v5, :cond_4

    .line 931
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    goto :goto_1

    .line 935
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    .line 937
    sparse-switch v1, :sswitch_data_0

    .line 975
    :goto_2
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 978
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 979
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v2

    .line 980
    and-int/lit8 v1, v2, 0x7f

    .line 981
    :goto_3
    if-le v2, v5, :cond_5

    .line 982
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v2

    .line 983
    shl-int/lit8 v1, v1, 0x8

    .line 984
    and-int/lit8 v3, v2, 0x7f

    or-int/2addr v1, v3

    goto :goto_3

    .line 939
    :sswitch_0
    const-string v1, "audio/mpeg"

    .line 940
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 988
    :goto_4
    return-object v0

    .line 942
    :sswitch_1
    const-string v0, "video/mp4v-es"

    goto :goto_2

    .line 945
    :sswitch_2
    const-string v0, "video/avc"

    goto :goto_2

    .line 948
    :sswitch_3
    const-string v0, "video/hevc"

    goto :goto_2

    .line 954
    :sswitch_4
    const-string v0, "audio/mp4a-latm"

    goto :goto_2

    .line 957
    :sswitch_5
    const-string v0, "audio/ac3"

    goto :goto_2

    .line 960
    :sswitch_6
    const-string v0, "audio/eac3"

    goto :goto_2

    .line 964
    :sswitch_7
    const-string v1, "audio/vnd.dts"

    .line 965
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_4

    .line 968
    :sswitch_8
    const-string v1, "audio/vnd.dts.hd"

    .line 969
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_4

    .line 986
    :cond_5
    new-array v2, v1, [B

    .line 987
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/a/f/j;->a([BII)V

    .line 988
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_4

    .line 937
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x21 -> :sswitch_2
        0x23 -> :sswitch_3
        0x40 -> :sswitch_4
        0x66 -> :sswitch_4
        0x67 -> :sswitch_4
        0x68 -> :sswitch_4
        0x6b -> :sswitch_0
        0xa5 -> :sswitch_5
        0xa6 -> :sswitch_6
        0xa9 -> :sswitch_7
        0xaa -> :sswitch_8
        0xab -> :sswitch_8
        0xac -> :sswitch_7
    .end sparse-switch
.end method

.method private static e(Lcom/google/android/a/f/j;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/f/j;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 489
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 490
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 491
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v2

    .line 492
    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 493
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v4

    .line 494
    if-nez v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 495
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v0

    .line 496
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v0, 0xa

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v0, 0x5

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x60

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 499
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 492
    :cond_1
    const/16 v0, 0x10

    goto :goto_0
.end method
