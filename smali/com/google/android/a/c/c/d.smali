.class public final Lcom/google/android/a/c/c/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/e;


# static fields
.field private static final a:[B


# instance fields
.field private final b:I

.field private final c:Lcom/google/android/a/f/j;

.field private final d:Lcom/google/android/a/f/j;

.field private final e:Lcom/google/android/a/f/j;

.field private final f:Lcom/google/android/a/f/j;

.field private final g:[B

.field private final h:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/a/c/c/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/google/android/a/c/c/j;

.field private j:I

.field private k:I

.field private l:J

.field private m:I

.field private n:Lcom/google/android/a/f/j;

.field private o:J

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Lcom/google/android/a/c/c/h;

.field private u:Lcom/google/android/a/c/c/c;

.field private v:Lcom/google/android/a/c/g;

.field private w:Lcom/google/android/a/c/m;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/a/c/c/d;->a:[B

    return-void

    :array_0
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/a/c/c/d;-><init>(I)V

    .line 115
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/16 v2, 0x10

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput p1, p0, Lcom/google/android/a/c/c/d;->b:I

    .line 123
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0, v2}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    .line 124
    new-instance v0, Lcom/google/android/a/f/j;

    sget-object v1, Lcom/google/android/a/f/h;->a:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->c:Lcom/google/android/a/f/j;

    .line 125
    new-instance v0, Lcom/google/android/a/f/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->d:Lcom/google/android/a/f/j;

    .line 126
    new-instance v0, Lcom/google/android/a/f/j;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->e:Lcom/google/android/a/f/j;

    .line 127
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->g:[B

    .line 128
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    .line 129
    new-instance v0, Lcom/google/android/a/c/c/j;

    invoke-direct {v0}, Lcom/google/android/a/c/c/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    .line 130
    invoke-direct {p0}, Lcom/google/android/a/c/c/d;->a()V

    .line 131
    return-void
.end method

.method private static a(Lcom/google/android/a/f/j;J)Lcom/google/android/a/c/a;
    .locals 23

    .prologue
    .line 638
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/j;->b(I)V

    .line 639
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->k()I

    move-result v4

    .line 640
    invoke-static {v4}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v4

    .line 642
    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/a/f/j;->c(I)V

    .line 643
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v8

    .line 646
    if-nez v4, :cond_0

    .line 647
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v6

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v4

    add-long v4, v4, p1

    move-wide v10, v4

    move-wide v4, v6

    .line 654
    :goto_0
    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/a/f/j;->c(I)V

    .line 656
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->g()I

    move-result v16

    .line 657
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 658
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v18, v0

    .line 659
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v19, v0

    .line 660
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v20, v0

    .line 663
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v12

    .line 664
    const/4 v6, 0x0

    move-wide v14, v10

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    :goto_1
    move/from16 v0, v16

    if-ge v10, v0, :cond_2

    .line 665
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->k()I

    move-result v11

    .line 667
    const/high16 v12, -0x80000000

    and-int/2addr v12, v11

    .line 668
    if-eqz v12, :cond_1

    .line 669
    new-instance v4, Lcom/google/android/a/q;

    const-string v5, "Unhandled indirect reference"

    invoke-direct {v4, v5}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v4

    .line 650
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v6

    .line 651
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v4

    add-long v4, v4, p1

    move-wide v10, v4

    move-wide v4, v6

    goto :goto_0

    .line 671
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v12

    .line 673
    const v21, 0x7fffffff

    and-int v11, v11, v21

    aput v11, v17, v10

    .line 674
    aput-wide v14, v18, v10

    .line 678
    aput-wide v4, v20, v10

    .line 679
    add-long v4, v6, v12

    .line 680
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v12

    .line 681
    aget-wide v6, v20, v10

    sub-long v6, v12, v6

    aput-wide v6, v19, v10

    .line 683
    const/4 v6, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/a/f/j;->c(I)V

    .line 684
    aget v6, v17, v10

    int-to-long v6, v6

    add-long/2addr v14, v6

    .line 664
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    goto :goto_1

    .line 687
    :cond_2
    new-instance v4, Lcom/google/android/a/c/a;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/a/c/a;-><init>([I[J[J[J)V

    return-object v4
.end method

.method private static a(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/c/c;
    .locals 5

    .prologue
    .line 340
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 342
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v1

    .line 343
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v2

    .line 344
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v3

    .line 345
    new-instance v4, Lcom/google/android/a/c/c/c;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/a/c/c/c;-><init>(IIII)V

    return-object v4
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 190
    iput v0, p0, Lcom/google/android/a/c/c/d;->j:I

    .line 191
    iput v0, p0, Lcom/google/android/a/c/c/d;->m:I

    .line 192
    return-void
.end method

.method private a(Lcom/google/android/a/c/c/a$a;)V
    .locals 2

    .prologue
    .line 286
    iget v0, p1, Lcom/google/android/a/c/c/a$a;->az:I

    sget v1, Lcom/google/android/a/c/c/a;->y:I

    if-ne v0, v1, :cond_1

    .line 287
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->b(Lcom/google/android/a/c/c/a$a;)V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget v0, p1, Lcom/google/android/a/c/c/a$a;->az:I

    sget v1, Lcom/google/android/a/c/c/a;->H:I

    if-ne v0, v1, :cond_2

    .line 289
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->c(Lcom/google/android/a/c/c/a$a;)V

    goto :goto_0

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/c/a$a;->a(Lcom/google/android/a/c/c/a$a;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/a/c/c/a$b;J)V
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/c/a$a;->a(Lcom/google/android/a/c/c/a$b;)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget v0, p1, Lcom/google/android/a/c/c/a$b;->az:I

    sget v1, Lcom/google/android/a/c/c/a;->x:I

    if-ne v0, v1, :cond_0

    .line 279
    iget-object v0, p1, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0, p2, p3}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;J)Lcom/google/android/a/c/a;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->v:Lcom/google/android/a/c/g;

    invoke-interface {v1, v0}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/c/c/d;->x:Z

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/c/c/c;Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V
    .locals 5

    .prologue
    .line 474
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 475
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 476
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->b(I)I

    move-result v4

    .line 478
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 479
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v0

    .line 481
    iput-wide v0, p2, Lcom/google/android/a/c/c/j;->b:J

    .line 482
    iput-wide v0, p2, Lcom/google/android/a/c/c/j;->c:J

    .line 485
    :cond_0
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 488
    :goto_0
    and-int/lit8 v0, v4, 0x8

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    move v2, v0

    .line 490
    :goto_1
    and-int/lit8 v0, v4, 0x10

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    move v1, v0

    .line 492
    :goto_2
    and-int/lit8 v0, v4, 0x20

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/a/f/j;->o()I

    move-result v0

    .line 494
    :goto_3
    new-instance v4, Lcom/google/android/a/c/c/c;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/google/android/a/c/c/c;-><init>(IIII)V

    iput-object v4, p2, Lcom/google/android/a/c/c/j;->a:Lcom/google/android/a/c/c/c;

    .line 496
    return-void

    .line 485
    :cond_1
    iget v0, p0, Lcom/google/android/a/c/c/c;->a:I

    move v3, v0

    goto :goto_0

    .line 488
    :cond_2
    iget v0, p0, Lcom/google/android/a/c/c/c;->b:I

    move v2, v0

    goto :goto_1

    .line 490
    :cond_3
    iget v0, p0, Lcom/google/android/a/c/c/c;->c:I

    move v1, v0

    goto :goto_2

    .line 492
    :cond_4
    iget v0, p0, Lcom/google/android/a/c/c/c;->d:I

    goto :goto_3
.end method

.method private static a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;JILcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V
    .locals 26

    .prologue
    .line 522
    const/16 v2, 0x8

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 523
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 524
    invoke-static {v2}, Lcom/google/android/a/c/c/a;->b(I)I

    move-result v3

    .line 526
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->o()I

    move-result v21

    .line 527
    and-int/lit8 v2, v3, 0x1

    if-eqz v2, :cond_0

    .line 528
    move-object/from16 v0, p6

    iget-wide v4, v0, Lcom/google/android/a/c/c/j;->b:J

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    int-to-long v6, v2

    add-long/2addr v4, v6

    move-object/from16 v0, p6

    iput-wide v4, v0, Lcom/google/android/a/c/c/j;->b:J

    .line 531
    :cond_0
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v8, v2

    .line 532
    :goto_0
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/a/c/c/c;->d:I

    .line 533
    if-eqz v8, :cond_1

    .line 534
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->o()I

    move-result v14

    .line 537
    :cond_1
    and-int/lit16 v2, v3, 0x100

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move/from16 v20, v2

    .line 538
    :goto_1
    and-int/lit16 v2, v3, 0x200

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 539
    :goto_2
    and-int/lit16 v2, v3, 0x400

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move/from16 v18, v2

    .line 540
    :goto_3
    and-int/lit16 v2, v3, 0x800

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move v9, v2

    .line 545
    :goto_4
    const-wide/16 v2, 0x0

    .line 549
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/a/c/c/h;->m:[J

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/a/c/c/h;->m:[J

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/a/c/c/h;->m:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_10

    .line 551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/a/c/c/h;->n:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x3e8

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/a/c/c/h;->h:J

    invoke-static/range {v2 .. v7}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v2

    move-wide v10, v2

    .line 554
    :goto_5
    move-object/from16 v0, p6

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/a/c/c/j;->a(I)V

    .line 555
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->e:[I

    move-object/from16 v22, v0

    .line 556
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->f:[I

    move-object/from16 v23, v0

    .line 557
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->g:[J

    move-object/from16 v24, v0

    .line 558
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->h:[Z

    move-object/from16 v25, v0

    .line 560
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/a/c/c/h;->h:J

    .line 562
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/a/c/c/h;->g:I

    sget v3, Lcom/google/android/a/c/c/h;->a:I

    if-ne v2, v3, :cond_8

    and-int/lit8 v2, p4, 0x1

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v12, v2

    .line 564
    :goto_6
    const/4 v2, 0x0

    move/from16 v17, v2

    move-wide/from16 v2, p2

    :goto_7
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_f

    .line 566
    if-eqz v20, :cond_9

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->o()I

    move-result v4

    move/from16 v16, v4

    .line 568
    :goto_8
    if-eqz v19, :cond_a

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->o()I

    move-result v4

    move v15, v4

    .line 569
    :goto_9
    if-nez v17, :cond_b

    if-eqz v8, :cond_b

    move v13, v14

    .line 571
    :goto_a
    if-eqz v9, :cond_d

    .line 577
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->k()I

    move-result v4

    .line 578
    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v23, v17

    .line 582
    :goto_b
    const-wide/16 v4, 0x3e8

    invoke-static/range {v2 .. v7}, Lcom/google/android/a/f/n;->a(JJJ)J

    move-result-wide v4

    sub-long/2addr v4, v10

    aput-wide v4, v24, v17

    .line 584
    aput v15, v22, v17

    .line 585
    shr-int/lit8 v4, v13, 0x10

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_e

    if-eqz v12, :cond_2

    if-nez v17, :cond_e

    :cond_2
    const/4 v4, 0x1

    :goto_c
    aput-boolean v4, v25, v17

    .line 587
    move/from16 v0, v16

    int-to-long v4, v0

    add-long p2, v2, v4

    .line 564
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    move-wide/from16 v2, p2

    goto :goto_7

    .line 531
    :cond_3
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_0

    .line 537
    :cond_4
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_1

    .line 538
    :cond_5
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_2

    .line 539
    :cond_6
    const/4 v2, 0x0

    move/from16 v18, v2

    goto/16 :goto_3

    .line 540
    :cond_7
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_4

    .line 562
    :cond_8
    const/4 v2, 0x0

    move v12, v2

    goto :goto_6

    .line 566
    :cond_9
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/a/c/c/c;->b:I

    move/from16 v16, v4

    goto :goto_8

    .line 568
    :cond_a
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/a/c/c/c;->c:I

    move v15, v4

    goto :goto_9

    .line 569
    :cond_b
    if-eqz v18, :cond_c

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/a/f/j;->k()I

    move-result v4

    move v13, v4

    goto :goto_a

    :cond_c
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/a/c/c/c;->d:I

    move v13, v4

    goto :goto_a

    .line 580
    :cond_d
    const/4 v4, 0x0

    aput v4, v23, v17

    goto :goto_b

    .line 585
    :cond_e
    const/4 v4, 0x0

    goto :goto_c

    .line 589
    :cond_f
    return-void

    :cond_10
    move-wide v10, v2

    goto/16 :goto_5
.end method

.method private static a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/j;I[B)V
    .locals 6

    .prologue
    .line 352
    sget v0, Lcom/google/android/a/c/c/a;->I:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->f(I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 353
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Traf count in moof != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :cond_0
    sget v0, Lcom/google/android/a/c/c/a;->I:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/c/c/d;->b(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/j;I[B)V

    .line 357
    return-void
.end method

.method private static a(Lcom/google/android/a/c/c/i;Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V
    .locals 9

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 410
    iget v5, p0, Lcom/google/android/a/c/c/i;->b:I

    .line 411
    invoke-virtual {p1, v3}, Lcom/google/android/a/f/j;->b(I)V

    .line 412
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 413
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->b(I)I

    move-result v0

    .line 414
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 415
    invoke-virtual {p1, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 417
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    .line 419
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->o()I

    move-result v6

    .line 420
    iget v3, p2, Lcom/google/android/a/c/c/j;->d:I

    if-eq v6, v3, :cond_1

    .line 421
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Length mismatch: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/google/android/a/c/c/j;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_1
    if-nez v0, :cond_3

    .line 426
    iget-object v7, p2, Lcom/google/android/a/c/c/j;->j:[Z

    move v3, v2

    move v0, v2

    .line 427
    :goto_0
    if-ge v3, v6, :cond_4

    .line 428
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->f()I

    move-result v8

    .line 429
    add-int v4, v0, v8

    .line 430
    if-le v8, v5, :cond_2

    move v0, v1

    :goto_1
    aput-boolean v0, v7, v3

    .line 427
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_2
    move v0, v2

    .line 430
    goto :goto_1

    .line 433
    :cond_3
    if-le v0, v5, :cond_5

    .line 434
    :goto_2
    mul-int/2addr v0, v6

    add-int/2addr v0, v2

    .line 435
    iget-object v3, p2, Lcom/google/android/a/c/c/j;->j:[Z

    invoke-static {v3, v2, v6, v1}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 437
    :cond_4
    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/j;->b(I)V

    .line 438
    return-void

    :cond_5
    move v1, v2

    .line 433
    goto :goto_2
.end method

.method private static a(Lcom/google/android/a/f/j;ILcom/google/android/a/c/c/j;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 613
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 614
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 615
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->b(I)I

    move-result v0

    .line 617
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    .line 619
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Overriding TrackEncryptionBox parameters is unsupported."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 622
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 623
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v2

    .line 624
    iget v3, p2, Lcom/google/android/a/c/c/j;->d:I

    if-eq v2, v3, :cond_2

    .line 625
    new-instance v0, Lcom/google/android/a/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Length mismatch: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lcom/google/android/a/c/c/j;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 622
    goto :goto_0

    .line 628
    :cond_2
    iget-object v3, p2, Lcom/google/android/a/c/c/j;->j:[Z

    invoke-static {v3, v1, v2, v0}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 629
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/j;->b(I)V

    .line 630
    invoke-virtual {p2, p0}, Lcom/google/android/a/c/c/j;->a(Lcom/google/android/a/f/j;)V

    .line 631
    return-void
.end method

.method private static a(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 447
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->b(I)V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 449
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->b(I)I

    move-result v1

    .line 450
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 451
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 454
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->o()I

    move-result v1

    .line 455
    if-eq v1, v2, :cond_1

    .line 457
    new-instance v0, Lcom/google/android/a/q;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected saio entry count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 460
    :cond_1
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v0

    .line 461
    iget-wide v2, p1, Lcom/google/android/a/c/c/j;->c:J

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v0

    :goto_0
    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/google/android/a/c/c/j;->c:J

    .line 463
    return-void

    .line 461
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;[B)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 593
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 594
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lcom/google/android/a/f/j;->a([BII)V

    .line 597
    sget-object v0, Lcom/google/android/a/c/c/d;->a:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 605
    :goto_0
    return-void

    .line 604
    :cond_0
    invoke-static {p0, v1, p1}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;ILcom/google/android/a/c/c/j;)V

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 823
    sget v0, Lcom/google/android/a/c/c/a;->O:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->N:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->z:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->x:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->P:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->t:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->u:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->K:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->v:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->w:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->Y:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->Z:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->ab:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->aa:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->M:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/a/f/j;)J
    .locals 2

    .prologue
    .line 505
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 506
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 507
    invoke-static {v0}, Lcom/google/android/a/c/c/a;->a(I)I

    move-result v0

    .line 508
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/c/c/a$a;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 296
    iget-object v4, p1, Lcom/google/android/a/c/c/a$a;->aB:Ljava/util/List;

    .line 297
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 299
    const/4 v1, 0x0

    move v2, v3

    .line 300
    :goto_0
    if-ge v2, v5, :cond_3

    .line 301
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$b;

    .line 302
    iget v6, v0, Lcom/google/android/a/c/c/a$b;->az:I

    sget v7, Lcom/google/android/a/c/c/a;->Q:I

    if-ne v6, v7, :cond_1

    .line 303
    if-nez v1, :cond_0

    .line 304
    new-instance v1, Lcom/google/android/a/b/a$a;

    invoke-direct {v1}, Lcom/google/android/a/b/a$a;-><init>()V

    .line 306
    :cond_0
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    .line 307
    invoke-static {v0}, Lcom/google/android/a/c/c/f;->a([B)Ljava/util/UUID;

    move-result-object v6

    .line 308
    if-nez v6, :cond_2

    .line 309
    const-string v0, "FragmentedMp4Extractor"

    const-string v6, "Skipped pssh atom (failed to extract uuid)"

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 311
    :cond_2
    invoke-static {v0}, Lcom/google/android/a/c/c/f;->a([B)Ljava/util/UUID;

    move-result-object v6

    new-instance v7, Lcom/google/android/a/b/a$b;

    const-string v8, "video/mp4"

    invoke-direct {v7, v8, v0}, Lcom/google/android/a/b/a$b;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v1, v6, v7}, Lcom/google/android/a/b/a$a;->a(Ljava/util/UUID;Lcom/google/android/a/b/a$b;)V

    goto :goto_1

    .line 316
    :cond_3
    if-eqz v1, :cond_4

    .line 317
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->v:Lcom/google/android/a/c/g;

    invoke-interface {v0, v1}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/b/a;)V

    .line 320
    :cond_4
    sget v0, Lcom/google/android/a/c/c/a;->J:I

    invoke-virtual {p1, v0}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v0

    .line 321
    sget v1, Lcom/google/android/a/c/c/a;->v:I

    invoke-virtual {v0, v1}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;)Lcom/google/android/a/c/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->u:Lcom/google/android/a/c/c/c;

    .line 322
    sget v0, Lcom/google/android/a/c/c/a;->A:I

    invoke-virtual {p1, v0}, Lcom/google/android/a/c/c/a$a;->e(I)Lcom/google/android/a/c/c/a$a;

    move-result-object v0

    sget v1, Lcom/google/android/a/c/c/a;->z:I

    invoke-virtual {p1, v1}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/google/android/a/c/c/b;->a(Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/a$b;Z)Lcom/google/android/a/c/c/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    .line 324
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    if-nez v0, :cond_5

    .line 325
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Track type not supported."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_5
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget-object v1, v1, Lcom/google/android/a/c/c/h;->k:Lcom/google/android/a/o;

    invoke-interface {v0, v1}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 328
    return-void
.end method

.method private static b(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/j;I[B)V
    .locals 7

    .prologue
    .line 365
    sget v0, Lcom/google/android/a/c/c/a;->w:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->f(I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 366
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Trun count in traf != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_0
    sget v0, Lcom/google/android/a/c/c/a;->t:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 370
    if-eqz v0, :cond_1

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_6

    .line 371
    :cond_1
    const-wide/16 v2, 0x0

    .line 376
    :goto_0
    sget v0, Lcom/google/android/a/c/c/a;->u:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 377
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {p1, v0, p3}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/c;Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V

    .line 379
    sget v0, Lcom/google/android/a/c/c/a;->w:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 380
    iget-object v1, p3, Lcom/google/android/a/c/c/j;->a:Lcom/google/android/a/c/c/c;

    iget-object v5, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    move-object v0, p0

    move v4, p4

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;JILcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V

    .line 382
    sget v0, Lcom/google/android/a/c/c/a;->Y:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_2

    .line 384
    iget-object v1, p0, Lcom/google/android/a/c/c/h;->l:[Lcom/google/android/a/c/c/i;

    iget-object v2, p3, Lcom/google/android/a/c/c/j;->a:Lcom/google/android/a/c/c/c;

    iget v2, v2, Lcom/google/android/a/c/c/c;->a:I

    aget-object v1, v1, v2

    .line 386
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v1, v0, p3}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/i;Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V

    .line 389
    :cond_2
    sget v0, Lcom/google/android/a/c/c/a;->Z:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 390
    if-eqz v0, :cond_3

    .line 391
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0, p3}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V

    .line 394
    :cond_3
    sget v0, Lcom/google/android/a/c/c/a;->ab:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    .line 395
    if-eqz v0, :cond_4

    .line 396
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0, p3}, Lcom/google/android/a/c/c/d;->b(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V

    .line 399
    :cond_4
    iget-object v0, p2, Lcom/google/android/a/c/c/a$a;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 400
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_7

    .line 401
    iget-object v0, p2, Lcom/google/android/a/c/c/a$a;->aB:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$b;

    .line 402
    iget v3, v0, Lcom/google/android/a/c/c/a$b;->az:I

    sget v4, Lcom/google/android/a/c/c/a;->aa:I

    if-ne v3, v4, :cond_5

    .line 403
    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0, p3, p5}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;[B)V

    .line 400
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 373
    :cond_6
    sget v0, Lcom/google/android/a/c/c/a;->t:I

    invoke-virtual {p2, v0}, Lcom/google/android/a/c/c/a$a;->d(I)Lcom/google/android/a/c/c/a$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/a/c/c/a$b;->aA:Lcom/google/android/a/f/j;

    invoke-static {v0}, Lcom/google/android/a/c/c/d;->b(Lcom/google/android/a/f/j;)J

    move-result-wide v2

    goto :goto_0

    .line 406
    :cond_7
    return-void
.end method

.method private static b(Lcom/google/android/a/f/j;Lcom/google/android/a/c/c/j;)V
    .locals 1

    .prologue
    .line 608
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/f/j;ILcom/google/android/a/c/c/j;)V

    .line 609
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 833
    sget v0, Lcom/google/android/a/c/c/a;->y:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->A:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->B:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->C:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->D:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->H:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->I:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->J:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/android/a/c/c/a;->L:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/c/f;)Z
    .locals 10

    .prologue
    const-wide/32 v8, 0x7fffffff

    const/4 v0, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x1

    .line 195
    iget v2, p0, Lcom/google/android/a/c/c/d;->m:I

    if-nez v2, :cond_1

    .line 197
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v2, v0, v6, v1}, Lcom/google/android/a/c/f;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 257
    :goto_0
    return v0

    .line 200
    :cond_0
    iput v6, p0, Lcom/google/android/a/c/c/d;->m:I

    .line 201
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    invoke-virtual {v2, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 202
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/a/c/c/d;->l:J

    .line 203
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    iput v2, p0, Lcom/google/android/a/c/c/d;->k:I

    .line 206
    :cond_1
    iget-wide v2, p0, Lcom/google/android/a/c/c/d;->l:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 209
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v2, v6, v6}, Lcom/google/android/a/c/f;->b([BII)V

    .line 210
    iget v2, p0, Lcom/google/android/a/c/c/d;->m:I

    add-int/2addr v2, v6

    iput v2, p0, Lcom/google/android/a/c/c/d;->m:I

    .line 211
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->p()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/a/c/c/d;->l:J

    .line 214
    :cond_2
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/a/c/c/d;->m:I

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 215
    iget v4, p0, Lcom/google/android/a/c/c/d;->k:I

    sget v5, Lcom/google/android/a/c/c/a;->H:I

    if-ne v4, v5, :cond_3

    .line 217
    iget-object v4, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iput-wide v2, v4, Lcom/google/android/a/c/c/j;->c:J

    .line 218
    iget-object v4, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iput-wide v2, v4, Lcom/google/android/a/c/c/j;->b:J

    .line 221
    :cond_3
    iget v4, p0, Lcom/google/android/a/c/c/d;->k:I

    sget v5, Lcom/google/android/a/c/c/a;->h:I

    if-ne v4, v5, :cond_6

    .line 222
    iget-wide v4, p0, Lcom/google/android/a/c/c/d;->l:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/c/c/d;->o:J

    .line 223
    iget-boolean v0, p0, Lcom/google/android/a/c/c/d;->x:Z

    if-nez v0, :cond_4

    .line 224
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->v:Lcom/google/android/a/c/g;

    sget-object v2, Lcom/google/android/a/c/l;->f:Lcom/google/android/a/c/l;

    invoke-interface {v0, v2}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 225
    iput-boolean v1, p0, Lcom/google/android/a/c/c/d;->x:Z

    .line 227
    :cond_4
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-boolean v0, v0, Lcom/google/android/a/c/c/j;->m:Z

    if-eqz v0, :cond_5

    .line 228
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/a/c/c/d;->j:I

    :goto_1
    move v0, v1

    .line 232
    goto :goto_0

    .line 230
    :cond_5
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/c/d;->j:I

    goto :goto_1

    .line 235
    :cond_6
    iget v2, p0, Lcom/google/android/a/c/c/d;->k:I

    invoke-static {v2}, Lcom/google/android/a/c/c/d;->b(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 236
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/a/c/c/d;->l:J

    add-long/2addr v2, v4

    const-wide/16 v4, 0x8

    sub-long/2addr v2, v4

    .line 237
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    new-instance v4, Lcom/google/android/a/c/c/a$a;

    iget v5, p0, Lcom/google/android/a/c/c/d;->k:I

    invoke-direct {v4, v5, v2, v3}, Lcom/google/android/a/c/c/a$a;-><init>(IJ)V

    invoke-virtual {v0, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 238
    invoke-direct {p0}, Lcom/google/android/a/c/c/d;->a()V

    :goto_2
    move v0, v1

    .line 257
    goto/16 :goto_0

    .line 239
    :cond_7
    iget v2, p0, Lcom/google/android/a/c/c/d;->k:I

    invoke-static {v2}, Lcom/google/android/a/c/c/d;->a(I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 240
    iget v2, p0, Lcom/google/android/a/c/c/d;->m:I

    if-eq v2, v6, :cond_8

    .line 241
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Leaf atom defines extended atom size (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 243
    :cond_8
    iget-wide v2, p0, Lcom/google/android/a/c/c/d;->l:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_9

    .line 244
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Leaf atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_9
    new-instance v2, Lcom/google/android/a/f/j;

    iget-wide v4, p0, Lcom/google/android/a/c/c/d;->l:J

    long-to-int v3, v4

    invoke-direct {v2, v3}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    .line 247
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->f:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    iget-object v3, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    invoke-static {v2, v0, v3, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 248
    iput v1, p0, Lcom/google/android/a/c/c/d;->j:I

    goto :goto_2

    .line 250
    :cond_a
    iget-wide v2, p0, Lcom/google/android/a/c/c/d;->l:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_b

    .line 251
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Skipping atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    .line 254
    iput v1, p0, Lcom/google/android/a/c/c/d;->j:I

    goto :goto_2
.end method

.method private c(Lcom/google/android/a/f/j;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 797
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->a:Lcom/google/android/a/c/c/c;

    iget v0, v0, Lcom/google/android/a/c/c/c;->a:I

    .line 798
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget-object v2, v2, Lcom/google/android/a/c/c/h;->l:[Lcom/google/android/a/c/c/i;

    aget-object v0, v2, v0

    .line 800
    iget v2, v0, Lcom/google/android/a/c/c/i;->b:I

    .line 801
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->j:[Z

    iget v3, p0, Lcom/google/android/a/c/c/d;->p:I

    aget-boolean v3, v0, v3

    .line 804
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->e:Lcom/google/android/a/f/j;

    iget-object v4, v0, Lcom/google/android/a/f/j;->a:[B

    if-eqz v3, :cond_0

    const/16 v0, 0x80

    :goto_0
    or-int/2addr v0, v2

    int-to-byte v0, v0

    aput-byte v0, v4, v1

    .line 805
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 806
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/c/d;->e:Lcom/google/android/a/f/j;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v4}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 808
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    invoke-interface {v0, p1, v2}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 810
    if-nez v3, :cond_1

    .line 811
    add-int/lit8 v0, v2, 0x1

    .line 818
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 804
    goto :goto_0

    .line 814
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->g()I

    move-result v0

    .line 815
    const/4 v1, -0x2

    invoke-virtual {p1, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 816
    mul-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, 0x2

    .line 817
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    invoke-interface {v1, p1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 818
    add-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    goto :goto_1
.end method

.method private c(Lcom/google/android/a/c/c/a$a;)V
    .locals 6

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    invoke-virtual {v0}, Lcom/google/android/a/c/c/j;->a()V

    .line 332
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget-object v1, p0, Lcom/google/android/a/c/c/d;->u:Lcom/google/android/a/c/c/c;

    iget-object v3, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget v4, p0, Lcom/google/android/a/c/c/d;->b:I

    iget-object v5, p0, Lcom/google/android/a/c/c/d;->g:[B

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/h;Lcom/google/android/a/c/c/c;Lcom/google/android/a/c/c/a$a;Lcom/google/android/a/c/c/j;I[B)V

    .line 333
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/c/d;->p:I

    .line 334
    return-void
.end method

.method private c(Lcom/google/android/a/c/f;)V
    .locals 4

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/google/android/a/c/c/d;->l:J

    long-to-int v0, v0

    iget v1, p0, Lcom/google/android/a/c/c/d;->m:I

    sub-int/2addr v0, v1

    .line 262
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    const/16 v2, 0x8

    invoke-interface {p1, v1, v2, v0}, Lcom/google/android/a/c/f;->b([BII)V

    .line 264
    new-instance v0, Lcom/google/android/a/c/c/a$b;

    iget v1, p0, Lcom/google/android/a/c/c/d;->k:I

    iget-object v2, p0, Lcom/google/android/a/c/c/d;->n:Lcom/google/android/a/f/j;

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/c/c/a$b;-><init>(ILcom/google/android/a/f/j;)V

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/a$b;J)V

    .line 268
    :goto_0
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    .line 269
    :goto_1
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    iget-wide v0, v0, Lcom/google/android/a/c/c/a$a;->aA:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 270
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/c/c/a$a;

    invoke-direct {p0, v0}, Lcom/google/android/a/c/c/d;->a(Lcom/google/android/a/c/c/a$a;)V

    goto :goto_1

    .line 266
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    goto :goto_0

    .line 272
    :cond_1
    invoke-direct {p0}, Lcom/google/android/a/c/c/d;->a()V

    .line 273
    return-void
.end method

.method private d(Lcom/google/android/a/c/f;)V
    .locals 4

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-wide v0, v0, Lcom/google/android/a/c/c/j;->c:J

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 692
    if-gez v0, :cond_0

    .line 693
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Offset to encryption data was negative."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 695
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 696
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/c/j;->a(Lcom/google/android/a/c/f;)V

    .line 697
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/c/d;->j:I

    .line 698
    return-void
.end method

.method private e(Lcom/google/android/a/c/f;)Z
    .locals 10

    .prologue
    const/4 v7, 0x4

    const/4 v9, 0x3

    const/4 v0, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 715
    iget v1, p0, Lcom/google/android/a/c/c/d;->j:I

    if-ne v1, v9, :cond_4

    .line 716
    iget v1, p0, Lcom/google/android/a/c/c/d;->p:I

    iget-object v2, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget v2, v2, Lcom/google/android/a/c/c/j;->d:I

    if-ne v1, v2, :cond_1

    .line 719
    iget-wide v0, p0, Lcom/google/android/a/c/c/d;->o:J

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 720
    if-gez v0, :cond_0

    .line 721
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Offset to end of mdat was negative."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 723
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 724
    invoke-direct {p0}, Lcom/google/android/a/c/c/d;->a()V

    .line 793
    :goto_0
    return v6

    .line 727
    :cond_1
    iget v1, p0, Lcom/google/android/a/c/c/d;->p:I

    if-nez v1, :cond_3

    .line 729
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-wide v2, v1, Lcom/google/android/a/c/c/j;->b:J

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v1, v2

    .line 730
    if-gez v1, :cond_2

    .line 731
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Offset to sample data was negative."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 733
    :cond_2
    invoke-interface {p1, v1}, Lcom/google/android/a/c/f;->b(I)V

    .line 735
    :cond_3
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v1, v1, Lcom/google/android/a/c/c/j;->e:[I

    iget v2, p0, Lcom/google/android/a/c/c/d;->p:I

    aget v1, v1, v2

    iput v1, p0, Lcom/google/android/a/c/c/d;->q:I

    .line 736
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-boolean v1, v1, Lcom/google/android/a/c/c/j;->i:Z

    if-eqz v1, :cond_5

    .line 737
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v1, v1, Lcom/google/android/a/c/c/j;->l:Lcom/google/android/a/f/j;

    invoke-direct {p0, v1}, Lcom/google/android/a/c/c/d;->c(Lcom/google/android/a/f/j;)I

    move-result v1

    iput v1, p0, Lcom/google/android/a/c/c/d;->r:I

    .line 738
    iget v1, p0, Lcom/google/android/a/c/c/d;->q:I

    iget v2, p0, Lcom/google/android/a/c/c/d;->r:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/a/c/c/d;->q:I

    .line 742
    :goto_1
    iput v6, p0, Lcom/google/android/a/c/c/d;->s:I

    .line 743
    iput v7, p0, Lcom/google/android/a/c/c/d;->j:I

    .line 746
    :cond_4
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget v1, v1, Lcom/google/android/a/c/c/h;->o:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 749
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->d:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    .line 750
    aput-byte v6, v1, v6

    .line 751
    aput-byte v6, v1, v8

    .line 752
    aput-byte v6, v1, v0

    .line 753
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget v1, v1, Lcom/google/android/a/c/c/h;->o:I

    .line 754
    iget-object v2, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget v2, v2, Lcom/google/android/a/c/c/h;->o:I

    rsub-int/lit8 v2, v2, 0x4

    .line 758
    :goto_2
    iget v3, p0, Lcom/google/android/a/c/c/d;->r:I

    iget v4, p0, Lcom/google/android/a/c/c/d;->q:I

    if-ge v3, v4, :cond_8

    .line 759
    iget v3, p0, Lcom/google/android/a/c/c/d;->s:I

    if-nez v3, :cond_6

    .line 761
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->d:Lcom/google/android/a/f/j;

    iget-object v3, v3, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v3, v2, v1}, Lcom/google/android/a/c/f;->b([BII)V

    .line 762
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v3, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 763
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v3}, Lcom/google/android/a/f/j;->o()I

    move-result v3

    iput v3, p0, Lcom/google/android/a/c/c/d;->s:I

    .line 765
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v3, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 766
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget-object v4, p0, Lcom/google/android/a/c/c/d;->c:Lcom/google/android/a/f/j;

    invoke-interface {v3, v4, v7}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 767
    iget v3, p0, Lcom/google/android/a/c/c/d;->r:I

    add-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/a/c/c/d;->r:I

    .line 768
    iget v3, p0, Lcom/google/android/a/c/c/d;->q:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/android/a/c/c/d;->q:I

    goto :goto_2

    .line 740
    :cond_5
    iput v6, p0, Lcom/google/android/a/c/c/d;->r:I

    goto :goto_1

    .line 771
    :cond_6
    iget-object v3, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget v4, p0, Lcom/google/android/a/c/c/d;->s:I

    invoke-interface {v3, p1, v4, v6}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v3

    .line 772
    iget v4, p0, Lcom/google/android/a/c/c/d;->r:I

    add-int/2addr v4, v3

    iput v4, p0, Lcom/google/android/a/c/c/d;->r:I

    .line 773
    iget v4, p0, Lcom/google/android/a/c/c/d;->s:I

    sub-int v3, v4, v3

    iput v3, p0, Lcom/google/android/a/c/c/d;->s:I

    goto :goto_2

    .line 777
    :cond_7
    :goto_3
    iget v1, p0, Lcom/google/android/a/c/c/d;->r:I

    iget v2, p0, Lcom/google/android/a/c/c/d;->q:I

    if-ge v1, v2, :cond_8

    .line 778
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget v2, p0, Lcom/google/android/a/c/c/d;->q:I

    iget v3, p0, Lcom/google/android/a/c/c/d;->r:I

    sub-int/2addr v2, v3

    invoke-interface {v1, p1, v2, v6}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v1

    .line 779
    iget v2, p0, Lcom/google/android/a/c/c/d;->r:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/a/c/c/d;->r:I

    goto :goto_3

    .line 783
    :cond_8
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget v2, p0, Lcom/google/android/a/c/c/d;->p:I

    invoke-virtual {v1, v2}, Lcom/google/android/a/c/c/j;->c(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    .line 784
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-boolean v1, v1, Lcom/google/android/a/c/c/j;->i:Z

    if-eqz v1, :cond_9

    :goto_4
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v1, v1, Lcom/google/android/a/c/c/j;->h:[Z

    iget v4, p0, Lcom/google/android/a/c/c/d;->p:I

    aget-boolean v1, v1, v4

    if-eqz v1, :cond_a

    move v1, v8

    :goto_5
    or-int v4, v0, v1

    .line 786
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-object v0, v0, Lcom/google/android/a/c/c/j;->a:Lcom/google/android/a/c/c/c;

    iget v0, v0, Lcom/google/android/a/c/c/c;->a:I

    .line 787
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->i:Lcom/google/android/a/c/c/j;

    iget-boolean v1, v1, Lcom/google/android/a/c/c/j;->i:Z

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/a/c/c/d;->t:Lcom/google/android/a/c/c/h;

    iget-object v1, v1, Lcom/google/android/a/c/c/h;->l:[Lcom/google/android/a/c/c/i;

    aget-object v0, v1, v0

    iget-object v7, v0, Lcom/google/android/a/c/c/i;->c:[B

    .line 789
    :goto_6
    iget-object v1, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    iget v5, p0, Lcom/google/android/a/c/c/d;->q:I

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 791
    iget v0, p0, Lcom/google/android/a/c/c/d;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/c/d;->p:I

    .line 792
    iput v9, p0, Lcom/google/android/a/c/c/d;->j:I

    move v6, v8

    .line 793
    goto/16 :goto_0

    :cond_9
    move v0, v6

    .line 784
    goto :goto_4

    :cond_a
    move v1, v6

    goto :goto_5

    .line 787
    :cond_b
    const/4 v7, 0x0

    goto :goto_6
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 1

    .prologue
    .line 169
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/a/c/c/d;->j:I

    packed-switch v0, :pswitch_data_0

    .line 182
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->e(Lcom/google/android/a/c/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 171
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->b(Lcom/google/android/a/c/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 172
    const/4 v0, -0x1

    goto :goto_1

    .line 176
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->c(Lcom/google/android/a/c/f;)V

    goto :goto_0

    .line 179
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/a/c/c/d;->d(Lcom/google/android/a/c/f;)V

    goto :goto_0

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/google/android/a/c/g;)V
    .locals 1

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/a/c/c/d;->v:Lcom/google/android/a/c/g;

    .line 155
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/c/d;->w:Lcom/google/android/a/c/m;

    .line 156
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->v:Lcom/google/android/a/c/g;

    invoke-interface {v0}, Lcom/google/android/a/c/g;->f()V

    .line 157
    return-void
.end method

.method public a(Lcom/google/android/a/c/f;)Z
    .locals 1

    .prologue
    .line 135
    invoke-static {p1}, Lcom/google/android/a/c/c/g;->a(Lcom/google/android/a/c/f;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/a/c/c/d;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 162
    invoke-direct {p0}, Lcom/google/android/a/c/c/d;->a()V

    .line 163
    return-void
.end method
