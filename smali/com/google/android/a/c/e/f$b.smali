.class final Lcom/google/android/a/c/e/f$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/a/c/e/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:[B

.field public g:[B

.field public h:[B

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:J

.field public n:J

.field public o:Lcom/google/android/a/c/m;

.field public p:I

.field private q:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, -0x1

    .line 1121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1134
    iput v0, p0, Lcom/google/android/a/c/e/f$b;->i:I

    .line 1135
    iput v0, p0, Lcom/google/android/a/c/e/f$b;->j:I

    .line 1138
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/e/f$b;->k:I

    .line 1139
    const/16 v0, 0x1f40

    iput v0, p0, Lcom/google/android/a/c/e/f$b;->l:I

    .line 1140
    iput-wide v2, p0, Lcom/google/android/a/c/e/f$b;->m:J

    .line 1141
    iput-wide v2, p0, Lcom/google/android/a/c/e/f$b;->n:J

    .line 1144
    const-string v0, "eng"

    iput-object v0, p0, Lcom/google/android/a/c/e/f$b;->q:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/a/c/e/f$1;)V
    .locals 0

    .prologue
    .line 1121
    invoke-direct {p0}, Lcom/google/android/a/c/e/f$b;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/a/f/j;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/f/j;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1266
    const/4 v1, 0x4

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 1267
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    add-int/lit8 v2, v1, 0x1

    .line 1268
    const/4 v1, 0x3

    if-ne v2, v1, :cond_0

    .line 1269
    new-instance v0, Lcom/google/android/a/q;

    invoke-direct {v0}, Lcom/google/android/a/q;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1281
    :catch_0
    move-exception v0

    .line 1282
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing AVC codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1271
    :cond_0
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1272
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    and-int/lit8 v4, v1, 0x1f

    move v1, v0

    .line 1273
    :goto_0
    if-ge v1, v4, :cond_1

    .line 1274
    invoke-static {p0}, Lcom/google/android/a/f/h;->a(Lcom/google/android/a/f/j;)[B

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1276
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v1

    .line 1277
    :goto_1
    if-ge v0, v1, :cond_2

    .line 1278
    invoke-static {p0}, Lcom/google/android/a/f/h;->a(Lcom/google/android/a/f/j;)[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1277
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1280
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/a/c/e/f$b;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 1121
    iput-object p1, p0, Lcom/google/android/a/c/e/f$b;->q:Ljava/lang/String;

    return-object p1
.end method

.method private static a([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v0, 0x0

    .line 1348
    const/4 v1, 0x0

    :try_start_0
    aget-byte v1, p0, v1

    if-eq v1, v2, :cond_0

    .line 1349
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1385
    :catch_0
    move-exception v0

    .line 1386
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v3, v4

    .line 1353
    :goto_0
    :try_start_1
    aget-byte v1, p0, v3

    if-ne v1, v5, :cond_1

    .line 1354
    add-int/lit16 v1, v2, 0xff

    .line 1355
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 1357
    :cond_1
    add-int/lit8 v1, v3, 0x1

    aget-byte v3, p0, v3

    add-int/2addr v2, v3

    .line 1360
    :goto_1
    aget-byte v3, p0, v1

    if-ne v3, v5, :cond_2

    .line 1361
    add-int/lit16 v0, v0, 0xff

    .line 1362
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1364
    :cond_2
    add-int/lit8 v3, v1, 0x1

    aget-byte v1, p0, v1

    add-int/2addr v0, v1

    .line 1366
    aget-byte v1, p0, v3

    if-eq v1, v4, :cond_3

    .line 1367
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1369
    :cond_3
    new-array v1, v2, [B

    .line 1370
    const/4 v4, 0x0

    invoke-static {p0, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1371
    add-int/2addr v2, v3

    .line 1372
    aget-byte v3, p0, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    .line 1373
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1375
    :cond_4
    add-int/2addr v0, v2

    .line 1376
    aget-byte v2, p0, v0

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    .line 1377
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1379
    :cond_5
    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    .line 1380
    const/4 v3, 0x0

    array-length v4, p0

    sub-int/2addr v4, v0

    invoke-static {p0, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1381
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1382
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1383
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1384
    return-object v0
.end method

.method private static b(Lcom/google/android/a/f/j;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/a/f/j;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1296
    const/16 v0, 0x15

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 1297
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 1300
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->f()I

    move-result v6

    .line 1302
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v7

    move v3, v1

    move v4, v1

    .line 1303
    :goto_0
    if-ge v3, v6, :cond_1

    .line 1304
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 1305
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 1306
    :goto_1
    if-ge v0, v8, :cond_0

    .line 1307
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v4

    .line 1308
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 1309
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/j;->c(I)V

    .line 1306
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1303
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 1314
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/android/a/f/j;->b(I)V

    .line 1315
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 1317
    :goto_2
    if-ge v3, v6, :cond_3

    .line 1318
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 1319
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 1320
    :goto_3
    if-ge v0, v8, :cond_2

    .line 1321
    invoke-virtual {p0}, Lcom/google/android/a/f/j;->g()I

    move-result v9

    .line 1322
    sget-object v10, Lcom/google/android/a/f/h;->a:[B

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/a/f/h;->a:[B

    array-length v12, v12

    invoke-static {v10, v11, v7, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1324
    sget-object v10, Lcom/google/android/a/f/h;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 1325
    iget-object v10, p0, Lcom/google/android/a/f/j;->a:[B

    invoke-virtual {p0}, Lcom/google/android/a/f/j;->d()I

    move-result v11

    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1327
    add-int/2addr v2, v9

    .line 1328
    invoke-virtual {p0, v9}, Lcom/google/android/a/f/j;->c(I)V

    .line 1320
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1317
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 1332
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 1333
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 1332
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_4

    .line 1334
    :catch_0
    move-exception v0

    .line 1335
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Error parsing HEVC codec private"

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/g;IJ)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x3

    const/16 v5, 0x8

    const/4 v2, -0x1

    .line 1156
    .line 1158
    iget-object v1, p0, Lcom/google/android/a/c/e/f$b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    move v1, v2

    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 1232
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Unrecognized codec identifier."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158
    :sswitch_0
    const-string v3, "V_VP8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "V_VP9"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "V_MPEG2"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "V_MPEG4/ISO/SP"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_4
    const-string v3, "V_MPEG4/ISO/ASP"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "V_MPEG4/ISO/AP"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "V_MPEG4/ISO/AVC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "V_MPEGH/ISO/HEVC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "A_VORBIS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    goto :goto_0

    :sswitch_9
    const-string v3, "A_OPUS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v3, "A_AAC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v3, "A_MPEG/L3"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v3, "A_AC3"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v3, "A_EAC3"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v3, "A_TRUEHD"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v3, "A_DTS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v3, "A_DTS/EXPRESS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v3, "A_DTS/LOSSLESS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v3, "S_TEXT/UTF8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    .line 1160
    :pswitch_0
    const-string v1, "video/x-vnd.on2.vp8"

    move-object v8, v0

    move v3, v2

    .line 1238
    :goto_1
    invoke-static {v1}, Lcom/google/android/a/f/f;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1239
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget v6, p0, Lcom/google/android/a/c/e/f$b;->k:I

    iget v7, p0, Lcom/google/android/a/c/e/f$b;->l:I

    iget-object v9, p0, Lcom/google/android/a/c/e/f$b;->q:Ljava/lang/String;

    move-wide v4, p3

    invoke-static/range {v0 .. v9}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/a/o;

    move-result-object v0

    .line 1252
    :goto_2
    iget v1, p0, Lcom/google/android/a/c/e/f$b;->b:I

    invoke-interface {p1, v1}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/a/c/e/f$b;->o:Lcom/google/android/a/c/m;

    .line 1253
    iget-object v1, p0, Lcom/google/android/a/c/e/f$b;->o:Lcom/google/android/a/c/m;

    invoke-interface {v1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 1254
    return-void

    .line 1163
    :pswitch_1
    const-string v1, "video/x-vnd.on2.vp9"

    move-object v8, v0

    move v3, v2

    .line 1164
    goto :goto_1

    .line 1166
    :pswitch_2
    const-string v1, "video/mpeg2"

    move-object v8, v0

    move v3, v2

    .line 1167
    goto :goto_1

    .line 1171
    :pswitch_3
    const-string v1, "video/mp4v-es"

    .line 1172
    iget-object v3, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    if-nez v3, :cond_1

    :goto_3
    move-object v8, v0

    move v3, v2

    .line 1174
    goto :goto_1

    .line 1172
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 1176
    :pswitch_4
    const-string v3, "video/avc"

    .line 1177
    new-instance v0, Lcom/google/android/a/f/j;

    iget-object v1, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    invoke-static {v0}, Lcom/google/android/a/c/e/f$b;->a(Lcom/google/android/a/f/j;)Landroid/util/Pair;

    move-result-object v1

    .line 1179
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1180
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/android/a/c/e/f$b;->p:I

    move-object v8, v0

    move-object v1, v3

    move v3, v2

    .line 1181
    goto :goto_1

    .line 1183
    :pswitch_5
    const-string v3, "video/hevc"

    .line 1184
    new-instance v0, Lcom/google/android/a/f/j;

    iget-object v1, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    invoke-static {v0}, Lcom/google/android/a/c/e/f$b;->b(Lcom/google/android/a/f/j;)Landroid/util/Pair;

    move-result-object v1

    .line 1186
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1187
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/google/android/a/c/e/f$b;->p:I

    move-object v8, v0

    move-object v1, v3

    move v3, v2

    .line 1188
    goto :goto_1

    .line 1190
    :pswitch_6
    const-string v1, "audio/vorbis"

    .line 1191
    const/16 v3, 0x2000

    .line 1192
    iget-object v0, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-static {v0}, Lcom/google/android/a/c/e/f$b;->a([B)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    .line 1193
    goto/16 :goto_1

    .line 1195
    :pswitch_7
    const-string v1, "audio/opus"

    .line 1196
    const/16 v3, 0x1680

    .line 1197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1198
    iget-object v4, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1199
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-wide v6, p0, Lcom/google/android/a/c/e/f$b;->m:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1201
    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    iget-wide v6, p0, Lcom/google/android/a/c/e/f$b;->n:J

    invoke-virtual {v4, v6, v7}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v8, v0

    .line 1203
    goto/16 :goto_1

    .line 1205
    :pswitch_8
    const-string v1, "audio/mp4a-latm"

    .line 1206
    iget-object v0, p0, Lcom/google/android/a/c/e/f$b;->h:[B

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    move-object v8, v0

    move v3, v2

    .line 1207
    goto/16 :goto_1

    .line 1209
    :pswitch_9
    const-string v1, "audio/mpeg"

    .line 1210
    const/16 v3, 0x1000

    move-object v8, v0

    .line 1211
    goto/16 :goto_1

    .line 1213
    :pswitch_a
    const-string v1, "audio/ac3"

    move-object v8, v0

    move v3, v2

    .line 1214
    goto/16 :goto_1

    .line 1216
    :pswitch_b
    const-string v1, "audio/eac3"

    move-object v8, v0

    move v3, v2

    .line 1217
    goto/16 :goto_1

    .line 1219
    :pswitch_c
    const-string v1, "audio/true-hd"

    move-object v8, v0

    move v3, v2

    .line 1220
    goto/16 :goto_1

    .line 1223
    :pswitch_d
    const-string v1, "audio/vnd.dts"

    move-object v8, v0

    move v3, v2

    .line 1224
    goto/16 :goto_1

    .line 1226
    :pswitch_e
    const-string v1, "audio/vnd.dts.hd"

    move-object v8, v0

    move v3, v2

    .line 1227
    goto/16 :goto_1

    .line 1229
    :pswitch_f
    const-string v1, "application/x-subrip"

    move-object v8, v0

    move v3, v2

    .line 1230
    goto/16 :goto_1

    .line 1242
    :cond_2
    invoke-static {v1}, Lcom/google/android/a/f/f;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1243
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget v6, p0, Lcom/google/android/a/c/e/f$b;->i:I

    iget v7, p0, Lcom/google/android/a/c/e/f$b;->j:I

    move-wide v4, p3

    invoke-static/range {v0 .. v8}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;)Lcom/google/android/a/o;

    move-result-object v0

    goto/16 :goto_2

    .line 1245
    :cond_3
    const-string v0, "application/x-subrip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1246
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/a/c/e/f$b;->q:Ljava/lang/String;

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/android/a/o;

    move-result-object v0

    goto/16 :goto_2

    .line 1249
    :cond_4
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Unexpected MIME type."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158
    :sswitch_data_0
    .sparse-switch
        -0x7ce7f5de -> :sswitch_5
        -0x7ce7f3b0 -> :sswitch_3
        -0x6a615338 -> :sswitch_e
        -0x672350af -> :sswitch_8
        -0x585f4fcd -> :sswitch_b
        -0x2016c535 -> :sswitch_4
        -0x2016c4e5 -> :sswitch_6
        -0x1538b2ba -> :sswitch_11
        0x3c02325 -> :sswitch_a
        0x3c02353 -> :sswitch_c
        0x3c030c5 -> :sswitch_f
        0x4e86155 -> :sswitch_0
        0x4e86156 -> :sswitch_1
        0x2056f406 -> :sswitch_10
        0x32fdf009 -> :sswitch_7
        0x54c61e47 -> :sswitch_12
        0x6bd6c624 -> :sswitch_2
        0x7446132a -> :sswitch_d
        0x744ad97d -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
