.class public final Lcom/google/android/a/c/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/e;
.implements Lcom/google/android/a/c/l;


# static fields
.field private static final d:I


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field private final e:Lcom/google/android/a/f/j;

.field private final g:Lcom/google/android/a/f/j;

.field private final h:Lcom/google/android/a/f/j;

.field private final i:Lcom/google/android/a/f/j;

.field private j:Lcom/google/android/a/c/g;

.field private k:I

.field private l:I

.field private m:Lcom/google/android/a/c/a/a;

.field private n:Lcom/google/android/a/c/a/e;

.field private o:Lcom/google/android/a/c/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "FLV"

    invoke-static {v0}, Lcom/google/android/a/f/n;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/a/c/a/b;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lcom/google/android/a/f/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    .line 75
    new-instance v0, Lcom/google/android/a/f/j;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    .line 76
    new-instance v0, Lcom/google/android/a/f/j;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    .line 77
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    .line 78
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/a/b;->k:I

    .line 79
    return-void
.end method

.method private b(Lcom/google/android/a/c/f;)Z
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 159
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v2, v0, v5, v1}, Lcom/google/android/a/c/f;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    :goto_0
    return v0

    .line 164
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    invoke-virtual {v2, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 165
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/a/f/j;->c(I)V

    .line 166
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->f()I

    move-result v3

    .line 167
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_5

    move v2, v1

    .line 168
    :goto_1
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    move v0, v1

    .line 169
    :cond_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    if-nez v2, :cond_2

    .line 170
    new-instance v2, Lcom/google/android/a/c/a/a;

    iget-object v3, p0, Lcom/google/android/a/c/a/b;->j:Lcom/google/android/a/c/g;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/a/c/a/a;-><init>(Lcom/google/android/a/c/m;)V

    iput-object v2, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    .line 172
    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    if-nez v0, :cond_3

    .line 173
    new-instance v0, Lcom/google/android/a/c/a/e;

    iget-object v2, p0, Lcom/google/android/a/c/a/b;->j:Lcom/google/android/a/c/g;

    invoke-interface {v2, v5}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/a/c/a/e;-><init>(Lcom/google/android/a/c/m;)V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    if-nez v0, :cond_4

    .line 176
    new-instance v0, Lcom/google/android/a/c/a/c;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/a/c/a/c;-><init>(Lcom/google/android/a/c/m;)V

    iput-object v0, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    .line 178
    :cond_4
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->j:Lcom/google/android/a/c/g;

    invoke-interface {v0}, Lcom/google/android/a/c/g;->f()V

    .line 179
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->j:Lcom/google/android/a/c/g;

    invoke-interface {v0, p0}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->g:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x9

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/a/c/a/b;->l:I

    .line 183
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/a/c/a/b;->k:I

    move v0, v1

    .line 184
    goto :goto_0

    :cond_5
    move v2, v0

    .line 167
    goto :goto_1
.end method

.method private c(Lcom/google/android/a/c/f;)V
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/google/android/a/c/a/b;->l:I

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 196
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/a/b;->l:I

    .line 197
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/a/b;->k:I

    .line 198
    return-void
.end method

.method private d(Lcom/google/android/a/c/f;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 209
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    const/16 v3, 0xb

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/google/android/a/c/f;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 221
    :goto_0
    return v0

    .line 214
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v2, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/a/b;->a:I

    .line 216
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/a/b;->b:I

    .line 217
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->i()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/android/a/c/a/b;->c:J

    .line 218
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/google/android/a/c/a/b;->c:J

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/c/a/b;->c:J

    .line 219
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->h:Lcom/google/android/a/f/j;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->c(I)V

    .line 220
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/a/c/a/b;->k:I

    move v0, v1

    .line 221
    goto :goto_0
.end method

.method private e(Lcom/google/android/a/c/f;)Z
    .locals 6

    .prologue
    .line 233
    const/4 v0, 0x1

    .line 234
    iget v1, p0, Lcom/google/android/a/c/a/b;->a:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    if-eqz v1, :cond_1

    .line 235
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->f(Lcom/google/android/a/c/f;)Lcom/google/android/a/f/j;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/a/c/a/b;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/a/c/a/a;->b(Lcom/google/android/a/f/j;J)V

    .line 252
    :cond_0
    :goto_0
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/a/c/a/b;->l:I

    .line 253
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/a/c/a/b;->k:I

    .line 254
    return v0

    .line 236
    :cond_1
    iget v1, p0, Lcom/google/android/a/c/a/b;->a:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    if-eqz v1, :cond_2

    .line 237
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->f(Lcom/google/android/a/c/f;)Lcom/google/android/a/f/j;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/a/c/a/b;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/a/c/a/e;->b(Lcom/google/android/a/f/j;J)V

    goto :goto_0

    .line 238
    :cond_2
    iget v1, p0, Lcom/google/android/a/c/a/b;->a:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    if-eqz v1, :cond_4

    .line 239
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->f(Lcom/google/android/a/c/f;)Lcom/google/android/a/f/j;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/a/c/a/b;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/a/c/a/c;->b(Lcom/google/android/a/f/j;J)V

    .line 240
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    invoke-virtual {v1}, Lcom/google/android/a/c/a/c;->a()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    if-eqz v1, :cond_3

    .line 242
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->m:Lcom/google/android/a/c/a/a;

    iget-object v2, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    invoke-virtual {v2}, Lcom/google/android/a/c/a/c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/a/c/a/a;->a(J)V

    .line 244
    :cond_3
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->n:Lcom/google/android/a/c/a/e;

    iget-object v2, p0, Lcom/google/android/a/c/a/b;->o:Lcom/google/android/a/c/a/c;

    invoke-virtual {v2}, Lcom/google/android/a/c/a/c;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/a/c/a/e;->a(J)V

    goto :goto_0

    .line 249
    :cond_4
    iget v0, p0, Lcom/google/android/a/c/a/b;->b:I

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 250
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/google/android/a/c/f;)Lcom/google/android/a/f/j;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 259
    iget v0, p0, Lcom/google/android/a/c/a/b;->b:I

    iget-object v1, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->e()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    iget-object v1, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->e()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/android/a/c/a/b;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0, v1, v3}, Lcom/google/android/a/f/j;->a([BI)V

    .line 264
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    iget v1, p0, Lcom/google/android/a/c/a/b;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->a(I)V

    .line 265
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    iget v1, p0, Lcom/google/android/a/c/a/b;->b:I

    invoke-interface {p1, v0, v3, v1}, Lcom/google/android/a/c/f;->b([BII)V

    .line 266
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    return-object v0

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/c/a/b;->i:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v3}, Lcom/google/android/a/f/j;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 127
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/android/a/c/a/b;->k:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 129
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->b(Lcom/google/android/a/c/f;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    :goto_1
    return v0

    .line 134
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->c(Lcom/google/android/a/c/f;)V

    goto :goto_0

    .line 137
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->d(Lcom/google/android/a/c/f;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 142
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/a/c/a/b;->e(Lcom/google/android/a/c/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    const/4 v0, 0x0

    goto :goto_1

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/android/a/c/g;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/a/c/a/b;->j:Lcom/google/android/a/c/g;

    .line 115
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/android/a/c/f;)Z
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x0

    .line 84
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    const/4 v2, 0x3

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/a/c/f;->c([BII)V

    .line 85
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 86
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->i()I

    move-result v1

    sget v2, Lcom/google/android/a/c/a/b;->d:I

    if-eq v1, v2, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    const/4 v2, 0x2

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/a/c/f;->c([BII)V

    .line 92
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 93
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->g()I

    move-result v1

    and-int/lit16 v1, v1, 0xfa

    if-nez v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v1, v0, v3}, Lcom/google/android/a/c/f;->c([BII)V

    .line 99
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 100
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    .line 102
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 103
    invoke-interface {p1, v1}, Lcom/google/android/a/c/f;->c(I)V

    .line 106
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v1, v0, v3}, Lcom/google/android/a/c/f;->c([BII)V

    .line 107
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 109
    iget-object v1, p0, Lcom/google/android/a/c/a/b;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v1}, Lcom/google/android/a/f/j;->k()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(J)J
    .locals 2

    .prologue
    .line 278
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/a/b;->k:I

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/a/b;->l:I

    .line 121
    return-void
.end method
