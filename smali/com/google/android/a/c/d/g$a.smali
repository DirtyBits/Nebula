.class final Lcom/google/android/a/c/d/g$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/a/c/d/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/a/f/i;

.field private b:[B

.field private c:I

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    .line 241
    new-instance v0, Lcom/google/android/a/f/i;

    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    .line 242
    invoke-virtual {p0}, Lcom/google/android/a/c/d/g$a;->a()V

    .line 243
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 249
    iput-boolean v0, p0, Lcom/google/android/a/c/d/g$a;->d:Z

    .line 250
    iput v0, p0, Lcom/google/android/a/c/d/g$a;->c:I

    .line 251
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/a/c/d/g$a;->e:I

    .line 252
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 266
    if-ne p1, v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/a/c/d/g$a;->a()V

    .line 268
    iput-boolean v0, p0, Lcom/google/android/a/c/d/g$a;->d:Z

    .line 270
    :cond_0
    return-void
.end method

.method public a([BII)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 280
    iget-boolean v0, p0, Lcom/google/android/a/c/d/g$a;->d:Z

    if-nez v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    sub-int v0, p3, p2

    .line 284
    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    array-length v1, v1

    iget v2, p0, Lcom/google/android/a/c/d/g$a;->c:I

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_2

    .line 285
    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    iget v2, p0, Lcom/google/android/a/c/d/g$a;->c:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    .line 287
    :cond_2
    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    iget v2, p0, Lcom/google/android/a/c/d/g$a;->c:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 288
    iget v1, p0, Lcom/google/android/a/c/d/g$a;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/d/g$a;->c:I

    .line 290
    iget-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->b:[B

    iget v2, p0, Lcom/google/android/a/c/d/g$a;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/a/f/i;->a([BI)V

    .line 291
    iget-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->b(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v0}, Lcom/google/android/a/f/i;->c()I

    move-result v0

    .line 294
    if-eq v0, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v1}, Lcom/google/android/a/f/i;->a()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/i;->b(I)V

    .line 301
    iget-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v0}, Lcom/google/android/a/f/i;->c()I

    move-result v0

    .line 302
    if-eq v0, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v1}, Lcom/google/android/a/f/i;->a()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/a/c/d/g$a;->a:Lcom/google/android/a/f/i;

    invoke-virtual {v0}, Lcom/google/android/a/f/i;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/d/g$a;->e:I

    .line 308
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/g$a;->d:Z

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 258
    iget v0, p0, Lcom/google/android/a/c/d/g$a;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/a/c/d/g$a;->e:I

    return v0
.end method
