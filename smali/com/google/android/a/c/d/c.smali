.class final Lcom/google/android/a/c/d/c;
.super Lcom/google/android/a/c/d/e;
.source "SourceFile"


# static fields
.field private static final b:[B


# instance fields
.field private final c:Lcom/google/android/a/f/i;

.field private final d:Lcom/google/android/a/f/j;

.field private final e:Lcom/google/android/a/c/m;

.field private f:I

.field private g:I

.field private h:I

.field private i:Z

.field private j:Z

.field private k:J

.field private l:I

.field private m:J

.field private n:Lcom/google/android/a/c/m;

.field private o:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/a/c/d/c;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x49t
        0x44t
        0x33t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/m;)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/a/c/d/e;-><init>(Lcom/google/android/a/c/m;)V

    .line 83
    iput-object p2, p0, Lcom/google/android/a/c/d/c;->e:Lcom/google/android/a/c/m;

    .line 84
    invoke-static {}, Lcom/google/android/a/o;->a()Lcom/google/android/a/o;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 85
    new-instance v0, Lcom/google/android/a/f/i;

    const/4 v1, 0x7

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    .line 86
    new-instance v0, Lcom/google/android/a/f/j;

    sget-object v1, Lcom/google/android/a/c/d/c;->b:[B

    const/16 v2, 0xa

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    .line 87
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->c()V

    .line 88
    return-void
.end method

.method private a(Lcom/google/android/a/c/m;JII)V
    .locals 2

    .prologue
    .line 176
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/a/c/d/c;->f:I

    .line 177
    iput p4, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 178
    iput-object p1, p0, Lcom/google/android/a/c/d/c;->n:Lcom/google/android/a/c/m;

    .line 179
    iput-wide p2, p0, Lcom/google/android/a/c/d/c;->o:J

    .line 180
    iput p5, p0, Lcom/google/android/a/c/d/c;->l:I

    .line 181
    return-void
.end method

.method private a(Lcom/google/android/a/f/j;[BI)Z
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/a/c/d/c;->g:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 141
    iget v1, p0, Lcom/google/android/a/c/d/c;->g:I

    invoke-virtual {p1, p2, v1, v0}, Lcom/google/android/a/f/j;->a([BII)V

    .line 142
    iget v1, p0, Lcom/google/android/a/c/d/c;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 143
    iget v0, p0, Lcom/google/android/a/c/d/c;->g:I

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/f/j;)V
    .locals 7

    .prologue
    const/16 v6, 0x200

    const/16 v5, 0x100

    .line 198
    iget-object v2, p1, Lcom/google/android/a/f/j;->a:[B

    .line 199
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v0

    .line 200
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->c()I

    move-result v3

    .line 201
    :goto_0
    if-ge v0, v3, :cond_2

    .line 202
    add-int/lit8 v1, v0, 0x1

    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    .line 203
    iget v4, p0, Lcom/google/android/a/c/d/c;->h:I

    if-ne v4, v6, :cond_1

    const/16 v4, 0xf0

    if-lt v0, v4, :cond_1

    const/16 v4, 0xff

    if-eq v0, v4, :cond_1

    .line 204
    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/a/c/d/c;->i:Z

    .line 205
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->e()V

    .line 206
    invoke-virtual {p1, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 234
    :goto_2
    return-void

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 209
    :cond_1
    iget v4, p0, Lcom/google/android/a/c/d/c;->h:I

    or-int/2addr v0, v4

    sparse-switch v0, :sswitch_data_0

    .line 224
    iget v0, p0, Lcom/google/android/a/c/d/c;->h:I

    if-eq v0, v5, :cond_3

    .line 227
    iput v5, p0, Lcom/google/android/a/c/d/c;->h:I

    .line 228
    add-int/lit8 v0, v1, -0x1

    goto :goto_0

    .line 211
    :sswitch_0
    iput v6, p0, Lcom/google/android/a/c/d/c;->h:I

    move v0, v1

    .line 212
    goto :goto_0

    .line 214
    :sswitch_1
    const/16 v0, 0x300

    iput v0, p0, Lcom/google/android/a/c/d/c;->h:I

    move v0, v1

    .line 215
    goto :goto_0

    .line 217
    :sswitch_2
    const/16 v0, 0x400

    iput v0, p0, Lcom/google/android/a/c/d/c;->h:I

    move v0, v1

    .line 218
    goto :goto_0

    .line 220
    :sswitch_3
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->d()V

    .line 221
    invoke-virtual {p1, v1}, Lcom/google/android/a/f/j;->b(I)V

    goto :goto_2

    .line 233
    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/android/a/f/j;->b(I)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0

    .line 209
    :sswitch_data_0
    .sparse-switch
        0x149 -> :sswitch_1
        0x1ff -> :sswitch_0
        0x344 -> :sswitch_2
        0x433 -> :sswitch_3
    .end sparse-switch
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 150
    iput v0, p0, Lcom/google/android/a/c/d/c;->f:I

    .line 151
    iput v0, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 152
    const/16 v0, 0x100

    iput v0, p0, Lcom/google/android/a/c/d/c;->h:I

    .line 153
    return-void
.end method

.method private c(Lcom/google/android/a/f/j;)V
    .locals 8

    .prologue
    .line 288
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/a/c/d/c;->l:I

    iget v2, p0, Lcom/google/android/a/c/d/c;->g:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 289
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->n:Lcom/google/android/a/c/m;

    invoke-interface {v1, p1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 290
    iget v1, p0, Lcom/google/android/a/c/d/c;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 291
    iget v0, p0, Lcom/google/android/a/c/d/c;->g:I

    iget v1, p0, Lcom/google/android/a/c/d/c;->l:I

    if-ne v0, v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->n:Lcom/google/android/a/c/m;

    iget-wide v2, p0, Lcom/google/android/a/c/d/c;->m:J

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/a/c/d/c;->l:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 293
    iget-wide v0, p0, Lcom/google/android/a/c/d/c;->m:J

    iget-wide v2, p0, Lcom/google/android/a/c/d/c;->o:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/c/d/c;->m:J

    .line 294
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->c()V

    .line 296
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 160
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/c/d/c;->f:I

    .line 161
    sget-object v0, Lcom/google/android/a/c/d/c;->b:[B

    array-length v0, v0

    iput v0, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 162
    iput v1, p0, Lcom/google/android/a/c/d/c;->l:I

    .line 163
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 164
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/a/c/d/c;->f:I

    .line 188
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/c/d/c;->g:I

    .line 189
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/16 v4, 0xa

    .line 240
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->e:Lcom/google/android/a/c/m;

    iget-object v1, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    invoke-interface {v0, v1, v4}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/f/j;I)V

    .line 241
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->b(I)V

    .line 242
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->e:Lcom/google/android/a/c/m;

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->n()I

    move-result v0

    add-int/lit8 v5, v0, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/c/d/c;->a(Lcom/google/android/a/c/m;JII)V

    .line 244
    return-void
.end method

.method private g()V
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, -0x1

    .line 250
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v1, v10}, Lcom/google/android/a/f/i;->a(I)V

    .line 252
    iget-boolean v1, p0, Lcom/google/android/a/c/d/c;->j:Z

    if-nez v1, :cond_1

    .line 253
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 254
    iget-object v3, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v3, v12}, Lcom/google/android/a/f/i;->c(I)I

    move-result v3

    .line 255
    iget-object v4, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v11}, Lcom/google/android/a/f/i;->b(I)V

    .line 256
    iget-object v4, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    .line 258
    invoke-static {v1, v3, v4}, Lcom/google/android/a/f/c;->a(III)[B

    move-result-object v8

    .line 260
    invoke-static {v8}, Lcom/google/android/a/f/c;->a([B)Landroid/util/Pair;

    move-result-object v7

    .line 263
    const-string v1, "audio/mp4a-latm"

    const-wide/16 v4, -0x1

    iget-object v3, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    move v3, v2

    move-object v9, v0

    invoke-static/range {v0 .. v9}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/a/o;

    move-result-object v0

    .line 268
    const-wide/32 v2, 0x3d090000

    iget v1, v0, Lcom/google/android/a/o;->o:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/a/c/d/c;->k:J

    .line 269
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->a:Lcom/google/android/a/c/m;

    invoke-interface {v1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 270
    iput-boolean v11, p0, Lcom/google/android/a/c/d/c;->j:Z

    .line 275
    :goto_0
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v12}, Lcom/google/android/a/f/i;->b(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    add-int/lit8 v5, v0, -0x5

    .line 277
    iget-boolean v0, p0, Lcom/google/android/a/c/d/c;->i:Z

    if-eqz v0, :cond_0

    .line 278
    add-int/lit8 v5, v5, -0x2

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->a:Lcom/google/android/a/c/m;

    iget-wide v2, p0, Lcom/google/android/a/c/d/c;->k:J

    move-object v0, p0

    move v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/c/d/c;->a(Lcom/google/android/a/c/m;JII)V

    .line 282
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->c()V

    .line 93
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/google/android/a/c/d/c;->m:J

    .line 98
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;)V
    .locals 2

    .prologue
    .line 102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    if-lez v0, :cond_2

    .line 103
    iget v0, p0, Lcom/google/android/a/c/d/c;->f:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 105
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/a/c/d/c;->b(Lcom/google/android/a/f/j;)V

    goto :goto_0

    .line 108
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/a/c/d/c;->d:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    const/16 v1, 0xa

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/a/c/d/c;->a(Lcom/google/android/a/f/j;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->f()V

    goto :goto_0

    .line 113
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/a/c/d/c;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    .line 114
    :goto_1
    iget-object v1, p0, Lcom/google/android/a/c/d/c;->c:Lcom/google/android/a/f/i;

    iget-object v1, v1, Lcom/google/android/a/f/i;->a:[B

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/a/c/d/c;->a(Lcom/google/android/a/f/j;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/google/android/a/c/d/c;->g()V

    goto :goto_0

    .line 113
    :cond_1
    const/4 v0, 0x5

    goto :goto_1

    .line 119
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/a/c/d/c;->c(Lcom/google/android/a/f/j;)V

    goto :goto_0

    .line 123
    :cond_2
    return-void

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 128
    return-void
.end method
