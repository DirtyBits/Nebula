.class final Lcom/google/android/a/c/d/l$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/a/c/d/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/android/a/c/d/e;

.field private final b:Lcom/google/android/a/c/d/m;

.field private final c:Lcom/google/android/a/f/i;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Lcom/google/android/a/c/d/e;Lcom/google/android/a/c/d/m;)V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object p1, p0, Lcom/google/android/a/c/d/l$a;->a:Lcom/google/android/a/c/d/e;

    .line 242
    iput-object p2, p0, Lcom/google/android/a/c/d/l$a;->b:Lcom/google/android/a/c/d/m;

    .line 243
    new-instance v0, Lcom/google/android/a/f/i;

    const/16 v1, 0x40

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    .line 244
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 282
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/i;->b(I)V

    .line 283
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/l$a;->d:Z

    .line 284
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/l$a;->e:Z

    .line 287
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/i;->b(I)V

    .line 288
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/d/l$a;->g:I

    .line 289
    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x1e

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/16 v7, 0xf

    const/4 v6, 0x1

    .line 292
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/a/c/d/l$a;->h:J

    .line 293
    iget-boolean v0, p0, Lcom/google/android/a/c/d/l$a;->d:Z

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v5}, Lcom/google/android/a/f/i;->b(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    int-to-long v0, v0

    shl-long/2addr v0, v8

    .line 296
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 297
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0xf

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 298
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 299
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 300
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 301
    iget-boolean v2, p0, Lcom/google/android/a/c/d/l$a;->f:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/a/c/d/l$a;->e:Z

    if-eqz v2, :cond_0

    .line 302
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v5}, Lcom/google/android/a/f/i;->b(I)V

    .line 303
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v2, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v2

    int-to-long v2, v2

    shl-long/2addr v2, v8

    .line 304
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 305
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    shl-int/lit8 v4, v4, 0xf

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 306
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 307
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 308
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v6}, Lcom/google/android/a/f/i;->b(I)V

    .line 314
    iget-object v4, p0, Lcom/google/android/a/c/d/l$a;->b:Lcom/google/android/a/c/d/m;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/a/c/d/m;->a(J)J

    .line 315
    iput-boolean v6, p0, Lcom/google/android/a/c/d/l$a;->f:Z

    .line 317
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/d/l$a;->b:Lcom/google/android/a/c/d/m;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/a/c/d/m;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/c/d/l$a;->h:J

    .line 319
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/c/d/l$a;->f:Z

    .line 255
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/e;->a()V

    .line 256
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;Lcom/google/android/a/c/g;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    iget-object v0, v0, Lcom/google/android/a/f/i;->a:[B

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/android/a/f/j;->a([BII)V

    .line 266
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/i;->a(I)V

    .line 267
    invoke-direct {p0}, Lcom/google/android/a/c/d/l$a;->b()V

    .line 268
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    iget-object v0, v0, Lcom/google/android/a/f/i;->a:[B

    iget v1, p0, Lcom/google/android/a/c/d/l$a;->g:I

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/android/a/f/j;->a([BII)V

    .line 269
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->c:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/i;->a(I)V

    .line 270
    invoke-direct {p0}, Lcom/google/android/a/c/d/l$a;->c()V

    .line 271
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->a:Lcom/google/android/a/c/d/e;

    iget-wide v2, p0, Lcom/google/android/a/c/d/l$a;->h:J

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/a/c/d/e;->a(JZ)V

    .line 272
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0, p1}, Lcom/google/android/a/c/d/e;->a(Lcom/google/android/a/f/j;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/a/c/d/l$a;->a:Lcom/google/android/a/c/d/e;

    invoke-virtual {v0}, Lcom/google/android/a/c/d/e;->b()V

    .line 275
    return-void
.end method
