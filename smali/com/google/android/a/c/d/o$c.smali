.class Lcom/google/android/a/c/d/o$c;
.super Lcom/google/android/a/c/d/o$d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/a/c/d/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/a/c/d/o;

.field private final b:Lcom/google/android/a/f/i;

.field private final c:Lcom/google/android/a/f/j;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/a/c/d/o;)V
    .locals 2

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/a/c/d/o$d;-><init>(Lcom/google/android/a/c/d/o$1;)V

    .line 257
    new-instance v0, Lcom/google/android/a/f/i;

    const/4 v1, 0x5

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/android/a/f/i;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    .line 258
    new-instance v0, Lcom/google/android/a/f/j;

    invoke-direct {v0}, Lcom/google/android/a/f/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    .line 259
    return-void
.end method

.method private a(Lcom/google/android/a/f/j;I)I
    .locals 8

    .prologue
    const/16 v2, 0x87

    const/16 v1, 0x81

    .line 398
    const/4 v0, -0x1

    .line 399
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v3

    add-int/2addr v3, p2

    .line 400
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->d()I

    move-result v4

    if-ge v4, v3, :cond_0

    .line 401
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->f()I

    move-result v4

    .line 402
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->f()I

    move-result v5

    .line 403
    const/4 v6, 0x5

    if-ne v4, v6, :cond_3

    .line 404
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->j()J

    move-result-wide v4

    .line 405
    invoke-static {}, Lcom/google/android/a/c/d/o;->a()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-nez v6, :cond_1

    move v0, v1

    .line 423
    :cond_0
    :goto_1
    invoke-virtual {p1, v3}, Lcom/google/android/a/f/j;->b(I)V

    .line 424
    return v0

    .line 407
    :cond_1
    invoke-static {}, Lcom/google/android/a/c/d/o;->c()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_2

    move v0, v2

    .line 408
    goto :goto_1

    .line 409
    :cond_2
    invoke-static {}, Lcom/google/android/a/c/d/o;->d()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 410
    const/16 v0, 0x24

    goto :goto_1

    .line 413
    :cond_3
    const/16 v6, 0x6a

    if-ne v4, v6, :cond_5

    move v0, v1

    .line 421
    :cond_4
    :goto_2
    invoke-virtual {p1, v5}, Lcom/google/android/a/f/j;->c(I)V

    goto :goto_0

    .line 415
    :cond_5
    const/16 v6, 0x7a

    if-ne v4, v6, :cond_6

    move v0, v2

    .line 416
    goto :goto_2

    .line 417
    :cond_6
    const/16 v6, 0x7b

    if-ne v4, v6, :cond_4

    .line 418
    const/16 v0, 0x8a

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method public a(Lcom/google/android/a/f/j;ZLcom/google/android/a/c/g;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v3, 0x1

    const/16 v9, 0xc

    .line 269
    if-eqz p2, :cond_0

    .line 271
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->f()I

    move-result v0

    .line 272
    invoke-virtual {p1, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 276
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {p1, v0, v10}, Lcom/google/android/a/f/j;->a(Lcom/google/android/a/f/i;I)V

    .line 277
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v9}, Lcom/google/android/a/f/i;->b(I)V

    .line 278
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v9}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/a/c/d/o$c;->d:I

    .line 280
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->e()I

    move-result v0

    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    if-ge v0, v1, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    new-array v1, v1, [B

    iget v4, p0, Lcom/google/android/a/c/d/o$c;->d:I

    invoke-virtual {v0, v1, v4}, Lcom/google/android/a/f/j;->a([BI)V

    .line 288
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/a/f/j;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    iget v4, p0, Lcom/google/android/a/c/d/o$c;->e:I

    sub-int/2addr v1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 289
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    iget-object v1, v1, Lcom/google/android/a/f/j;->a:[B

    iget v4, p0, Lcom/google/android/a/c/d/o$c;->e:I

    invoke-virtual {p1, v1, v4, v0}, Lcom/google/android/a/f/j;->a([BII)V

    .line 290
    iget v1, p0, Lcom/google/android/a/c/d/o$c;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/d/o$c;->e:I

    .line 291
    iget v0, p0, Lcom/google/android/a/c/d/o$c;->e:I

    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    if-ge v0, v1, :cond_2

    .line 386
    :goto_1
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->a()V

    .line 284
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->a(I)V

    goto :goto_0

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/a/f/j;->c(I)V

    .line 302
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v4}, Lcom/google/android/a/f/j;->a(Lcom/google/android/a/f/i;I)V

    .line 303
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v11}, Lcom/google/android/a/f/i;->b(I)V

    .line 304
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v0, v9}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 307
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->c(I)V

    .line 309
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    iget-object v1, v1, Lcom/google/android/a/c/d/o;->c:Lcom/google/android/a/c/d/i;

    if-nez v1, :cond_3

    .line 312
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    new-instance v4, Lcom/google/android/a/c/d/i;

    const/16 v5, 0x15

    invoke-interface {p3, v5}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/a/c/d/i;-><init>(Lcom/google/android/a/c/m;)V

    iput-object v4, v1, Lcom/google/android/a/c/d/o;->c:Lcom/google/android/a/c/d/i;

    .line 315
    :cond_3
    iget v1, p0, Lcom/google/android/a/c/d/o$c;->d:I

    add-int/lit8 v1, v1, -0x9

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0x4

    move v1, v0

    .line 317
    :goto_2
    if-lez v1, :cond_a

    .line 318
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    const/4 v5, 0x5

    invoke-virtual {v0, v4, v5}, Lcom/google/android/a/f/j;->a(Lcom/google/android/a/f/i;I)V

    .line 319
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 320
    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v10}, Lcom/google/android/a/f/i;->b(I)V

    .line 321
    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Lcom/google/android/a/f/i;->c(I)I

    move-result v6

    .line 322
    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v11}, Lcom/google/android/a/f/i;->b(I)V

    .line 323
    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->b:Lcom/google/android/a/f/i;

    invoke-virtual {v4, v9}, Lcom/google/android/a/f/i;->c(I)I

    move-result v4

    .line 324
    const/4 v5, 0x6

    if-ne v0, v5, :cond_4

    .line 326
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    invoke-direct {p0, v0, v4}, Lcom/google/android/a/c/d/o$c;->a(Lcom/google/android/a/f/j;I)I

    move-result v0

    .line 330
    :goto_3
    add-int/lit8 v4, v4, 0x5

    sub-int v5, v1, v4

    .line 331
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    iget-object v1, v1, Lcom/google/android/a/c/d/o;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v5

    .line 332
    goto :goto_2

    .line 328
    :cond_4
    iget-object v5, p0, Lcom/google/android/a/c/d/o$c;->c:Lcom/google/android/a/f/j;

    invoke-virtual {v5, v4}, Lcom/google/android/a/f/j;->c(I)V

    goto :goto_3

    .line 336
    :cond_5
    sparse-switch v0, :sswitch_data_0

    move-object v1, v2

    .line 378
    :goto_4
    if-eqz v1, :cond_6

    .line 379
    iget-object v4, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    iget-object v4, v4, Lcom/google/android/a/c/d/o;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v0, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 380
    iget-object v0, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    iget-object v0, v0, Lcom/google/android/a/c/d/o;->a:Landroid/util/SparseArray;

    new-instance v4, Lcom/google/android/a/c/d/o$b;

    iget-object v7, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    invoke-static {v7}, Lcom/google/android/a/c/d/o;->b(Lcom/google/android/a/c/d/o;)Lcom/google/android/a/c/d/m;

    move-result-object v7

    invoke-direct {v4, v1, v7}, Lcom/google/android/a/c/d/o$b;-><init>(Lcom/google/android/a/c/d/e;Lcom/google/android/a/c/d/m;)V

    invoke-virtual {v0, v6, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_6
    move v1, v5

    .line 383
    goto :goto_2

    .line 338
    :sswitch_0
    new-instance v1, Lcom/google/android/a/c/d/j;

    invoke-interface {p3, v10}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/a/c/d/j;-><init>(Lcom/google/android/a/c/m;)V

    goto :goto_4

    .line 341
    :sswitch_1
    new-instance v1, Lcom/google/android/a/c/d/j;

    invoke-interface {p3, v11}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/a/c/d/j;-><init>(Lcom/google/android/a/c/m;)V

    goto :goto_4

    .line 344
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    invoke-static {v1}, Lcom/google/android/a/c/d/o;->a(Lcom/google/android/a/c/d/o;)I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    move-object v1, v2

    goto :goto_4

    :cond_7
    new-instance v1, Lcom/google/android/a/c/d/c;

    const/16 v4, 0xf

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    new-instance v7, Lcom/google/android/a/c/d;

    invoke-direct {v7}, Lcom/google/android/a/c/d;-><init>()V

    invoke-direct {v1, v4, v7}, Lcom/google/android/a/c/d/c;-><init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/m;)V

    goto :goto_4

    .line 348
    :sswitch_3
    new-instance v1, Lcom/google/android/a/c/d/a;

    const/16 v4, 0x81

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    const/4 v7, 0x0

    invoke-direct {v1, v4, v7}, Lcom/google/android/a/c/d/a;-><init>(Lcom/google/android/a/c/m;Z)V

    goto :goto_4

    .line 351
    :sswitch_4
    new-instance v1, Lcom/google/android/a/c/d/a;

    const/16 v4, 0x87

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Lcom/google/android/a/c/d/a;-><init>(Lcom/google/android/a/c/m;Z)V

    goto :goto_4

    .line 355
    :sswitch_5
    new-instance v1, Lcom/google/android/a/c/d/d;

    const/16 v4, 0x8a

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/a/c/d/d;-><init>(Lcom/google/android/a/c/m;)V

    goto :goto_4

    .line 358
    :sswitch_6
    new-instance v1, Lcom/google/android/a/c/d/f;

    const/4 v4, 0x2

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/a/c/d/f;-><init>(Lcom/google/android/a/c/m;)V

    goto :goto_4

    .line 361
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    invoke-static {v1}, Lcom/google/android/a/c/d/o;->a(Lcom/google/android/a/c/d/o;)I

    move-result v1

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    move-object v1, v2

    goto/16 :goto_4

    :cond_8
    new-instance v4, Lcom/google/android/a/c/d/g;

    const/16 v1, 0x1b

    invoke-interface {p3, v1}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v7

    new-instance v8, Lcom/google/android/a/c/d/n;

    const/16 v1, 0x100

    invoke-interface {p3, v1}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v1

    invoke-direct {v8, v1}, Lcom/google/android/a/c/d/n;-><init>(Lcom/google/android/a/c/m;)V

    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    invoke-static {v1}, Lcom/google/android/a/c/d/o;->a(Lcom/google/android/a/c/d/o;)I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_9

    move v1, v3

    :goto_5
    invoke-direct {v4, v7, v8, v1}, Lcom/google/android/a/c/d/g;-><init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/d/n;Z)V

    move-object v1, v4

    goto/16 :goto_4

    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 367
    :sswitch_8
    new-instance v1, Lcom/google/android/a/c/d/h;

    const/16 v4, 0x24

    invoke-interface {p3, v4}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v4

    new-instance v7, Lcom/google/android/a/c/d/n;

    const/16 v8, 0x100

    invoke-interface {p3, v8}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/a/c/d/n;-><init>(Lcom/google/android/a/c/m;)V

    invoke-direct {v1, v4, v7}, Lcom/google/android/a/c/d/h;-><init>(Lcom/google/android/a/c/m;Lcom/google/android/a/c/d/n;)V

    goto/16 :goto_4

    .line 371
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/a/c/d/o$c;->a:Lcom/google/android/a/c/d/o;

    iget-object v1, v1, Lcom/google/android/a/c/d/o;->c:Lcom/google/android/a/c/d/i;

    goto/16 :goto_4

    .line 385
    :cond_a
    invoke-interface {p3}, Lcom/google/android/a/c/g;->f()V

    goto/16 :goto_1

    .line 336
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_6
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0xf -> :sswitch_2
        0x15 -> :sswitch_9
        0x1b -> :sswitch_7
        0x24 -> :sswitch_8
        0x81 -> :sswitch_3
        0x82 -> :sswitch_5
        0x87 -> :sswitch_4
        0x8a -> :sswitch_5
    .end sparse-switch
.end method
