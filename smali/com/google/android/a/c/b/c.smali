.class public final Lcom/google/android/a/c/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/c/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/c/b/c$a;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:J

.field private final e:Lcom/google/android/a/f/j;

.field private final f:Lcom/google/android/a/f/g;

.field private g:Lcom/google/android/a/c/g;

.field private h:Lcom/google/android/a/c/m;

.field private i:I

.field private j:Lcom/google/android/a/c/i;

.field private k:Lcom/google/android/a/c/b/c$a;

.field private l:J

.field private m:I

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "Xing"

    invoke-static {v0}, Lcom/google/android/a/f/n;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/a/c/b/c;->a:I

    .line 54
    const-string v0, "Info"

    invoke-static {v0}, Lcom/google/android/a/f/n;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/a/c/b/c;->b:I

    .line 55
    const-string v0, "VBRI"

    invoke-static {v0}, Lcom/google/android/a/f/n;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/a/c/b/c;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 77
    const-wide/16 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/c/b/c;-><init>(J)V

    .line 78
    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-wide p1, p0, Lcom/google/android/a/c/b/c;->d:J

    .line 88
    new-instance v0, Lcom/google/android/a/f/j;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/android/a/f/j;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    .line 89
    new-instance v0, Lcom/google/android/a/f/g;

    invoke-direct {v0}, Lcom/google/android/a/f/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    .line 90
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/c/b/c;->l:J

    .line 91
    return-void
.end method

.method private a(Lcom/google/android/a/c/f;Z)Z
    .locals 11

    .prologue
    const/4 v10, 0x4

    const v9, -0x1f400

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 202
    .line 206
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 207
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_b

    .line 208
    invoke-static {p1}, Lcom/google/android/a/c/b/b;->a(Lcom/google/android/a/c/f;)Lcom/google/android/a/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    .line 209
    invoke-interface {p1}, Lcom/google/android/a/c/f;->b()J

    move-result-wide v0

    long-to-int v0, v0

    .line 210
    if-nez p2, :cond_0

    .line 211
    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    :cond_0
    move v6, v0

    move v1, v2

    move v3, v2

    move v4, v2

    .line 215
    :goto_0
    if-eqz p2, :cond_2

    const/16 v0, 0x1000

    if-ne v4, v0, :cond_2

    .line 259
    :cond_1
    :goto_1
    return v2

    .line 218
    :cond_2
    if-nez p2, :cond_3

    const/high16 v0, 0x20000

    if-ne v4, v0, :cond_3

    .line 219
    new-instance v0, Lcom/google/android/a/q;

    const-string v1, "Searched too many bytes."

    invoke-direct {v0, v1}, Lcom/google/android/a/q;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_3
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    invoke-interface {p1, v0, v2, v10, v5}, Lcom/google/android/a/c/f;->b([BIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v2}, Lcom/google/android/a/f/j;->b(I)V

    .line 225
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 227
    if-eqz v1, :cond_4

    and-int v7, v0, v9

    and-int v8, v1, v9

    if-ne v7, v8, :cond_5

    :cond_4
    invoke-static {v0}, Lcom/google/android/a/f/g;->a(I)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_7

    .line 233
    :cond_5
    add-int/lit8 v0, v4, 0x1

    .line 234
    if-eqz p2, :cond_6

    .line 235
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 236
    add-int v1, v6, v0

    invoke-interface {p1, v1}, Lcom/google/android/a/c/f;->c(I)V

    move v1, v2

    move v3, v0

    move v0, v2

    :goto_2
    move v4, v3

    move v3, v1

    move v1, v0

    .line 251
    goto :goto_0

    .line 238
    :cond_6
    invoke-interface {p1, v5}, Lcom/google/android/a/c/f;->b(I)V

    move v1, v2

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 242
    :cond_7
    add-int/lit8 v3, v3, 0x1

    .line 243
    if-ne v3, v5, :cond_8

    .line 244
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    invoke-static {v0, v1}, Lcom/google/android/a/f/g;->a(ILcom/google/android/a/f/g;)Z

    .line 249
    :goto_3
    add-int/lit8 v1, v7, -0x4

    invoke-interface {p1, v1}, Lcom/google/android/a/c/f;->c(I)V

    move v1, v3

    move v3, v4

    goto :goto_2

    .line 246
    :cond_8
    if-ne v3, v10, :cond_a

    .line 253
    if-eqz p2, :cond_9

    .line 254
    add-int v0, v6, v4

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 258
    :goto_4
    iput v1, p0, Lcom/google/android/a/c/b/c;->i:I

    move v2, v5

    .line 259
    goto :goto_1

    .line 256
    :cond_9
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_3

    :cond_b
    move v6, v2

    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/c/f;)I
    .locals 12

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 135
    iget v1, p0, Lcom/google/android/a/c/b/c;->n:I

    if-nez v1, :cond_3

    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/a/c/b/c;->c(Lcom/google/android/a/c/f;)Z

    move-result v1

    if-nez v1, :cond_1

    move v6, v0

    .line 160
    :cond_0
    :goto_0
    return v6

    .line 139
    :cond_1
    iget-wide v2, p0, Lcom/google/android/a/c/b/c;->l:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_2

    .line 140
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/a/c/b/c$a;->a(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/a/c/b/c;->l:J

    .line 141
    iget-wide v2, p0, Lcom/google/android/a/c/b/c;->d:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 142
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    const-wide/16 v2, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/a/c/b/c$a;->a(J)J

    move-result-wide v2

    .line 143
    iget-wide v8, p0, Lcom/google/android/a/c/b/c;->l:J

    iget-wide v10, p0, Lcom/google/android/a/c/b/c;->d:J

    sub-long v2, v10, v2

    add-long/2addr v2, v8

    iput-wide v2, p0, Lcom/google/android/a/c/b/c;->l:J

    .line 146
    :cond_2
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v1, v1, Lcom/google/android/a/f/g;->c:I

    iput v1, p0, Lcom/google/android/a/c/b/c;->n:I

    .line 148
    :cond_3
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->h:Lcom/google/android/a/c/m;

    iget v2, p0, Lcom/google/android/a/c/b/c;->n:I

    invoke-interface {v1, p1, v2, v4}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/c/f;IZ)I

    move-result v1

    .line 149
    if-ne v1, v0, :cond_4

    move v6, v0

    .line 150
    goto :goto_0

    .line 152
    :cond_4
    iget v0, p0, Lcom/google/android/a/c/b/c;->n:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/b/c;->n:I

    .line 153
    iget v0, p0, Lcom/google/android/a/c/b/c;->n:I

    if-gtz v0, :cond_0

    .line 156
    iget-wide v0, p0, Lcom/google/android/a/c/b/c;->l:J

    iget v2, p0, Lcom/google/android/a/c/b/c;->m:I

    int-to-long v2, v2

    const-wide/32 v8, 0xf4240

    mul-long/2addr v2, v8

    iget-object v5, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v5, v5, Lcom/google/android/a/f/g;->d:I

    int-to-long v8, v5

    div-long/2addr v2, v8

    add-long/2addr v2, v0

    .line 157
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->h:Lcom/google/android/a/c/m;

    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v5, v0, Lcom/google/android/a/f/g;->c:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/android/a/c/m;->a(JIII[B)V

    .line 158
    iget v0, p0, Lcom/google/android/a/c/b/c;->m:I

    iget-object v1, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v1, v1, Lcom/google/android/a/f/g;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/c/b/c;->m:I

    .line 159
    iput v6, p0, Lcom/google/android/a/c/b/c;->n:I

    goto :goto_0
.end method

.method private c(Lcom/google/android/a/c/f;)Z
    .locals 6

    .prologue
    const v5, -0x1f400

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 168
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 169
    iget-object v2, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    iget-object v2, v2, Lcom/google/android/a/f/j;->a:[B

    const/4 v3, 0x4

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/google/android/a/c/f;->b([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 185
    :goto_0
    return v0

    .line 173
    :cond_0
    iget-object v2, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v2, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 174
    iget-object v2, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v2}, Lcom/google/android/a/f/j;->k()I

    move-result v2

    .line 175
    and-int v3, v2, v5

    iget v4, p0, Lcom/google/android/a/c/b/c;->i:I

    and-int/2addr v4, v5

    if-ne v3, v4, :cond_1

    .line 176
    invoke-static {v2}, Lcom/google/android/a/f/g;->a(I)I

    move-result v3

    .line 177
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    invoke-static {v2, v0}, Lcom/google/android/a/f/g;->a(ILcom/google/android/a/f/g;)Z

    move v0, v1

    .line 179
    goto :goto_0

    .line 183
    :cond_1
    iput v0, p0, Lcom/google/android/a/c/b/c;->i:I

    .line 184
    invoke-interface {p1, v1}, Lcom/google/android/a/c/f;->b(I)V

    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/a/c/b/c;->d(Lcom/google/android/a/c/f;)Z

    move-result v0

    goto :goto_0
.end method

.method private d(Lcom/google/android/a/c/f;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 194
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/a/c/b/c;->a(Lcom/google/android/a/c/f;Z)Z
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 196
    :goto_0
    return v0

    .line 195
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private e(Lcom/google/android/a/c/f;)V
    .locals 10

    .prologue
    const/16 v0, 0x24

    const/16 v6, 0x15

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 275
    new-instance v1, Lcom/google/android/a/f/j;

    iget-object v2, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v2, v2, Lcom/google/android/a/f/g;->c:I

    invoke-direct {v1, v2}, Lcom/google/android/a/f/j;-><init>(I)V

    .line 276
    iget-object v2, v1, Lcom/google/android/a/f/j;->a:[B

    iget-object v3, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v3, v3, Lcom/google/android/a/f/g;->c:I

    invoke-interface {p1, v2, v9, v3}, Lcom/google/android/a/c/f;->c([BII)V

    .line 278
    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v2

    .line 279
    invoke-interface {p1}, Lcom/google/android/a/c/f;->d()J

    move-result-wide v4

    .line 282
    iget-object v7, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v7, v7, Lcom/google/android/a/f/g;->a:I

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v7, v7, Lcom/google/android/a/f/g;->e:I

    if-eq v7, v8, :cond_0

    move v6, v0

    .line 285
    :cond_0
    :goto_0
    invoke-virtual {v1, v6}, Lcom/google/android/a/f/j;->b(I)V

    .line 286
    invoke-virtual {v1}, Lcom/google/android/a/f/j;->k()I

    move-result v7

    .line 287
    sget v8, Lcom/google/android/a/c/b/c;->a:I

    if-eq v7, v8, :cond_1

    sget v8, Lcom/google/android/a/c/b/c;->b:I

    if-ne v7, v8, :cond_6

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/c/b/e;->a(Lcom/google/android/a/f/g;Lcom/google/android/a/f/j;JJ)Lcom/google/android/a/c/b/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    .line 289
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    if-nez v0, :cond_2

    .line 291
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 292
    add-int/lit16 v0, v6, 0x8d

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->c(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    const/4 v1, 0x3

    invoke-interface {p1, v0, v9, v1}, Lcom/google/android/a/c/f;->c([BII)V

    .line 294
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v9}, Lcom/google/android/a/f/j;->b(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->i()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/a/c/i;->a(I)Lcom/google/android/a/c/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v0, v0, Lcom/google/android/a/f/g;->c:I

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    .line 308
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    if-nez v0, :cond_4

    .line 311
    invoke-interface {p1}, Lcom/google/android/a/c/f;->a()V

    .line 312
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    iget-object v0, v0, Lcom/google/android/a/f/j;->a:[B

    const/4 v1, 0x4

    invoke-interface {p1, v0, v9, v1}, Lcom/google/android/a/c/f;->c([BII)V

    .line 313
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0, v9}, Lcom/google/android/a/f/j;->b(I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->e:Lcom/google/android/a/f/j;

    invoke-virtual {v0}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    invoke-static {v0, v1}, Lcom/google/android/a/f/g;->a(ILcom/google/android/a/f/g;)Z

    .line 315
    new-instance v0, Lcom/google/android/a/c/b/a;

    invoke-interface {p1}, Lcom/google/android/a/c/f;->c()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v3, v3, Lcom/google/android/a/f/g;->f:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/c/b/a;-><init>(JIJ)V

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    .line 317
    :cond_4
    return-void

    .line 282
    :cond_5
    iget-object v7, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v7, v7, Lcom/google/android/a/f/g;->e:I

    if-ne v7, v8, :cond_0

    const/16 v6, 0xd

    goto :goto_0

    .line 300
    :cond_6
    invoke-virtual {v1, v0}, Lcom/google/android/a/f/j;->b(I)V

    .line 301
    invoke-virtual {v1}, Lcom/google/android/a/f/j;->k()I

    move-result v0

    .line 302
    sget v6, Lcom/google/android/a/c/b/c;->c:I

    if-ne v0, v6, :cond_3

    .line 303
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    invoke-static/range {v0 .. v5}, Lcom/google/android/a/c/b/d;->a(Lcom/google/android/a/f/g;Lcom/google/android/a/f/j;JJ)Lcom/google/android/a/c/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    .line 304
    iget-object v0, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v0, v0, Lcom/google/android/a/f/g;->c:I

    invoke-interface {p1, v0}, Lcom/google/android/a/c/f;->b(I)V

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/google/android/a/c/f;Lcom/google/android/a/c/j;)I
    .locals 10

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 116
    iget v1, p0, Lcom/google/android/a/c/b/c;->i:I

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/a/c/b/c;->d(Lcom/google/android/a/c/f;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    :goto_0
    return v2

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    if-nez v1, :cond_2

    .line 120
    invoke-direct {p0, p1}, Lcom/google/android/a/c/b/c;->e(Lcom/google/android/a/c/f;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->g:Lcom/google/android/a/c/g;

    iget-object v3, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    invoke-interface {v1, v3}, Lcom/google/android/a/c/g;->a(Lcom/google/android/a/c/l;)V

    .line 122
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget-object v1, v1, Lcom/google/android/a/f/g;->b:Ljava/lang/String;

    const/16 v3, 0x1000

    iget-object v4, p0, Lcom/google/android/a/c/b/c;->k:Lcom/google/android/a/c/b/c$a;

    invoke-interface {v4}, Lcom/google/android/a/c/b/c$a;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v6, v6, Lcom/google/android/a/f/g;->e:I

    iget-object v7, p0, Lcom/google/android/a/c/b/c;->f:Lcom/google/android/a/f/g;

    iget v7, v7, Lcom/google/android/a/f/g;->d:I

    move-object v8, v0

    move-object v9, v0

    invoke-static/range {v0 .. v9}, Lcom/google/android/a/o;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/android/a/o;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    if-eqz v1, :cond_1

    .line 126
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    iget v1, v1, Lcom/google/android/a/c/i;->a:I

    iget-object v2, p0, Lcom/google/android/a/c/b/c;->j:Lcom/google/android/a/c/i;

    iget v2, v2, Lcom/google/android/a/c/i;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/a/o;->a(II)Lcom/google/android/a/o;

    move-result-object v0

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/google/android/a/c/b/c;->h:Lcom/google/android/a/c/m;

    invoke-interface {v1, v0}, Lcom/google/android/a/c/m;->a(Lcom/google/android/a/o;)V

    .line 131
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/a/c/b/c;->b(Lcom/google/android/a/c/f;)I

    move-result v2

    goto :goto_0
.end method

.method public a(Lcom/google/android/a/c/g;)V
    .locals 1

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/a/c/b/c;->g:Lcom/google/android/a/c/g;

    .line 101
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/a/c/g;->d(I)Lcom/google/android/a/c/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/c/b/c;->h:Lcom/google/android/a/c/m;

    .line 102
    invoke-interface {p1}, Lcom/google/android/a/c/g;->f()V

    .line 103
    return-void
.end method

.method public a(Lcom/google/android/a/c/f;)Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/a/c/b/c;->a(Lcom/google/android/a/c/f;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    iput v2, p0, Lcom/google/android/a/c/b/c;->i:I

    .line 108
    iput v2, p0, Lcom/google/android/a/c/b/c;->m:I

    .line 109
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/c/b/c;->l:J

    .line 110
    iput v2, p0, Lcom/google/android/a/c/b/c;->n:I

    .line 111
    return-void
.end method
