.class final Lcom/google/android/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/f;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/a/h;

.field private final c:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/android/a/f$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:[[Lcom/google/android/a/o;

.field private final e:[I

.field private f:Z

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(III)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, "ExoPlayerImpl"

    const-string v1, "Init 1.5.6"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/g;->f:Z

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/a/g;->g:I

    .line 60
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 61
    new-array v0, p1, [[Lcom/google/android/a/o;

    iput-object v0, p0, Lcom/google/android/a/g;->d:[[Lcom/google/android/a/o;

    .line 62
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/android/a/g;->e:[I

    .line 63
    new-instance v0, Lcom/google/android/a/g$1;

    invoke-direct {v0, p0}, Lcom/google/android/a/g$1;-><init>(Lcom/google/android/a/g;)V

    iput-object v0, p0, Lcom/google/android/a/g;->a:Landroid/os/Handler;

    .line 69
    new-instance v0, Lcom/google/android/a/h;

    iget-object v1, p0, Lcom/google/android/a/g;->a:Landroid/os/Handler;

    iget-boolean v2, p0, Lcom/google/android/a/g;->f:Z

    iget-object v3, p0, Lcom/google/android/a/g;->e:[I

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/h;-><init>(Landroid/os/Handler;Z[III)V

    iput-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    .line 71
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/a/g;->g:I

    return v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/a/h;->a(J)V

    .line 147
    return-void
.end method

.method a(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 228
    :cond_0
    return-void

    .line 197
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/a/g;->d:[[Lcom/google/android/a/o;

    iget-object v2, p0, Lcom/google/android/a/g;->d:[[Lcom/google/android/a/o;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/a/g;->g:I

    .line 199
    iget-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/f$c;

    .line 200
    iget-boolean v2, p0, Lcom/google/android/a/g;->f:Z

    iget v3, p0, Lcom/google/android/a/g;->g:I

    invoke-interface {v0, v2, v3}, Lcom/google/android/a/f$c;->onPlayerStateChanged(ZI)V

    goto :goto_0

    .line 205
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/a/g;->g:I

    .line 206
    iget-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/f$c;

    .line 207
    iget-boolean v2, p0, Lcom/google/android/a/g;->f:Z

    iget v3, p0, Lcom/google/android/a/g;->g:I

    invoke-interface {v0, v2, v3}, Lcom/google/android/a/f$c;->onPlayerStateChanged(ZI)V

    goto :goto_1

    .line 212
    :pswitch_2
    iget v0, p0, Lcom/google/android/a/g;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/a/g;->h:I

    .line 213
    iget v0, p0, Lcom/google/android/a/g;->h:I

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/f$c;

    .line 215
    invoke-interface {v0}, Lcom/google/android/a/f$c;->onPlayWhenReadyCommitted()V

    goto :goto_2

    .line 221
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/a/e;

    .line 222
    iget-object v1, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/a/f$c;

    .line 223
    invoke-interface {v1, v0}, Lcom/google/android/a/f$c;->onPlayerError(Lcom/google/android/a/e;)V

    goto :goto_3

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/android/a/f$a;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/a/h;->a(Lcom/google/android/a/f$a;ILjava/lang/Object;)V

    .line 163
    return-void
.end method

.method public a(Lcom/google/android/a/f$c;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/a/g;->f:Z

    if-eq v0, p1, :cond_0

    .line 125
    iput-boolean p1, p0, Lcom/google/android/a/g;->f:Z

    .line 126
    iget v0, p0, Lcom/google/android/a/g;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/g;->h:I

    .line 127
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0, p1}, Lcom/google/android/a/h;->a(Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/a/g;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/f$c;

    .line 129
    iget v2, p0, Lcom/google/android/a/g;->g:I

    invoke-interface {v0, p1, v2}, Lcom/google/android/a/f$c;->onPlayerStateChanged(ZI)V

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public varargs a([Lcom/google/android/a/v;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/a/g;->d:[[Lcom/google/android/a/o;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0, p1}, Lcom/google/android/a/h;->a([Lcom/google/android/a/v;)V

    .line 97
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/google/android/a/g;->f:Z

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0}, Lcom/google/android/a/h;->c()V

    .line 152
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0}, Lcom/google/android/a/h;->d()V

    .line 157
    iget-object v0, p0, Lcom/google/android/a/g;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 158
    return-void
.end method

.method public e()J
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0}, Lcom/google/android/a/h;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/a/g;->b:Lcom/google/android/a/h;

    invoke-virtual {v0}, Lcom/google/android/a/h;->a()J

    move-result-wide v0

    return-wide v0
.end method
