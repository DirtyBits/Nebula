.class public abstract Lcom/google/android/a/l;
.super Lcom/google/android/a/t;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/l$a;,
        Lcom/google/android/a/l$b;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:Z

.field private E:Z

.field private F:I

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field public final a:Lcom/google/android/a/b;

.field protected final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/a/k;

.field private final d:Lcom/google/android/a/b/b;

.field private final e:Z

.field private final f:Lcom/google/android/a/r;

.field private final g:Lcom/google/android/a/p;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Landroid/media/MediaCodec$BufferInfo;

.field private final j:Lcom/google/android/a/l$b;

.field private final k:Z

.field private l:Lcom/google/android/a/o;

.field private m:Lcom/google/android/a/b/a;

.field private n:Landroid/media/MediaCodec;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:[Ljava/nio/ByteBuffer;

.field private v:[Ljava/nio/ByteBuffer;

.field private w:J

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/l$b;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 253
    new-array v2, v0, [Lcom/google/android/a/s;

    aput-object p1, v2, v1

    invoke-direct {p0, v2}, Lcom/google/android/a/t;-><init>([Lcom/google/android/a/s;)V

    .line 254
    sget v2, Lcom/google/android/a/f/n;->a:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/a/f/b;->b(Z)V

    .line 255
    invoke-static {p2}, Lcom/google/android/a/f/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/a/k;

    iput-object v0, p0, Lcom/google/android/a/l;->c:Lcom/google/android/a/k;

    .line 256
    iput-object p3, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    .line 257
    iput-boolean p4, p0, Lcom/google/android/a/l;->e:Z

    .line 258
    iput-object p5, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    .line 259
    iput-object p6, p0, Lcom/google/android/a/l;->j:Lcom/google/android/a/l$b;

    .line 260
    invoke-static {}, Lcom/google/android/a/l;->B()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/a/l;->k:Z

    .line 261
    new-instance v0, Lcom/google/android/a/b;

    invoke-direct {v0}, Lcom/google/android/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    .line 262
    new-instance v0, Lcom/google/android/a/r;

    invoke-direct {v0, v1}, Lcom/google/android/a/r;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    .line 263
    new-instance v0, Lcom/google/android/a/p;

    invoke-direct {v0}, Lcom/google/android/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/l;->g:Lcom/google/android/a/p;

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    .line 265
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    .line 266
    iput v1, p0, Lcom/google/android/a/l;->B:I

    .line 267
    iput v1, p0, Lcom/google/android/a/l;->C:I

    .line 268
    return-void

    :cond_0
    move v0, v1

    .line 254
    goto :goto_0
.end method

.method private A()V
    .locals 2

    .prologue
    .line 930
    iget v0, p0, Lcom/google/android/a/l;->C:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 932
    invoke-virtual {p0}, Lcom/google/android/a/l;->m()V

    .line 933
    invoke-virtual {p0}, Lcom/google/android/a/l;->j()V

    .line 938
    :goto_0
    return-void

    .line 935
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/l;->H:Z

    .line 936
    invoke-virtual {p0}, Lcom/google/android/a/l;->h()V

    goto :goto_0
.end method

.method private static B()Z
    .locals 2

    .prologue
    .line 1081
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x16

    if-gt v0, v1, :cond_0

    const-string v0, "foster"

    sget-object v1, Lcom/google/android/a/f/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "NVIDIA"

    sget-object v1, Lcom/google/android/a/f/n;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/a/r;I)Landroid/media/MediaCodec$CryptoInfo;
    .locals 4

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/a/r;->a:Lcom/google/android/a/c;

    invoke-virtual {v0}, Lcom/google/android/a/c;->a()Landroid/media/MediaCodec$CryptoInfo;

    move-result-object v0

    .line 688
    if-nez p1, :cond_0

    .line 698
    :goto_0
    return-object v0

    .line 694
    :cond_0
    iget-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    if-nez v1, :cond_1

    .line 695
    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    .line 697
    :cond_1
    iget-object v1, v0, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    const/4 v2, 0x0

    aget v3, v1, v2

    add-int/2addr v3, p1

    aput v3, v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/a/l;)Lcom/google/android/a/l$b;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/a/l;->j:Lcom/google/android/a/l$b;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 508
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/l;->w:J

    .line 509
    iput v2, p0, Lcom/google/android/a/l;->x:I

    .line 510
    iput v2, p0, Lcom/google/android/a/l;->y:I

    .line 511
    iput-boolean v4, p0, Lcom/google/android/a/l;->J:Z

    .line 512
    iput-boolean v3, p0, Lcom/google/android/a/l;->I:Z

    .line 513
    iget-object v0, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 514
    iget-boolean v0, p0, Lcom/google/android/a/l;->q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/a/l;->s:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/a/l;->E:Z

    if-eqz v0, :cond_2

    .line 516
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/a/l;->m()V

    .line 517
    invoke-virtual {p0}, Lcom/google/android/a/l;->j()V

    .line 528
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/a/l;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    if-eqz v0, :cond_1

    .line 531
    iput v4, p0, Lcom/google/android/a/l;->B:I

    .line 533
    :cond_1
    return-void

    .line 518
    :cond_2
    iget v0, p0, Lcom/google/android/a/l;->C:I

    if-eqz v0, :cond_3

    .line 521
    invoke-virtual {p0}, Lcom/google/android/a/l;->m()V

    .line 522
    invoke-virtual {p0}, Lcom/google/android/a/l;->j()V

    goto :goto_0

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 526
    iput-boolean v3, p0, Lcom/google/android/a/l;->D:Z

    goto :goto_0
.end method

.method private a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/l;->j:Lcom/google/android/a/l$b;

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/l$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/a/l$2;-><init>(Lcom/google/android/a/l;Landroid/media/MediaCodec$CryptoException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 960
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/a/l$a;)V
    .locals 1

    .prologue
    .line 392
    invoke-direct {p0, p1}, Lcom/google/android/a/l;->b(Lcom/google/android/a/l$a;)V

    .line 393
    new-instance v0, Lcom/google/android/a/e;

    invoke-direct {v0, p1}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljava/lang/String;JJ)V
    .locals 8

    .prologue
    .line 964
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/l;->j:Lcom/google/android/a/l$b;

    if-eqz v0, :cond_0

    .line 965
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/l$3;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/a/l$3;-><init>(Lcom/google/android/a/l;Ljava/lang/String;JJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 973
    :cond_0
    return-void
.end method

.method private a(JZ)Z
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 544
    iget-boolean v0, p0, Lcom/google/android/a/l;->G:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/a/l;->C:I

    if-ne v0, v8, :cond_1

    .line 682
    :cond_0
    :goto_0
    return v2

    .line 551
    :cond_1
    iget v0, p0, Lcom/google/android/a/l;->x:I

    if-gez v0, :cond_2

    .line 552
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/a/l;->x:I

    .line 553
    iget v0, p0, Lcom/google/android/a/l;->x:I

    if-ltz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-object v1, p0, Lcom/google/android/a/l;->u:[Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/google/android/a/l;->x:I

    aget-object v1, v1, v3

    iput-object v1, v0, Lcom/google/android/a/r;->b:Ljava/nio/ByteBuffer;

    .line 557
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->d()V

    .line 560
    :cond_2
    iget v0, p0, Lcom/google/android/a/l;->C:I

    if-ne v0, v7, :cond_4

    .line 563
    iget-boolean v0, p0, Lcom/google/android/a/l;->r:Z

    if-eqz v0, :cond_3

    .line 570
    :goto_1
    iput v8, p0, Lcom/google/android/a/l;->C:I

    goto :goto_0

    .line 566
    :cond_3
    iput-boolean v7, p0, Lcom/google/android/a/l;->E:Z

    .line 567
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/a/l;->x:I

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 568
    iput v9, p0, Lcom/google/android/a/l;->x:I

    goto :goto_1

    .line 575
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/a/l;->I:Z

    if-eqz v0, :cond_7

    .line 577
    const/4 v0, -0x3

    .line 594
    :cond_5
    :goto_2
    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 597
    const/4 v1, -0x4

    if-ne v0, v1, :cond_a

    .line 598
    iget v0, p0, Lcom/google/android/a/l;->B:I

    if-ne v0, v8, :cond_6

    .line 601
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->d()V

    .line 602
    iput v7, p0, Lcom/google/android/a/l;->B:I

    .line 604
    :cond_6
    iget-object v0, p0, Lcom/google/android/a/l;->g:Lcom/google/android/a/p;

    invoke-virtual {p0, v0}, Lcom/google/android/a/l;->a(Lcom/google/android/a/p;)V

    move v2, v7

    .line 605
    goto :goto_0

    .line 581
    :cond_7
    iget v0, p0, Lcom/google/android/a/l;->B:I

    if-ne v0, v7, :cond_9

    move v1, v2

    .line 582
    :goto_3
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    iget-object v0, v0, Lcom/google/android/a/o;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 583
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    iget-object v0, v0, Lcom/google/android/a/o;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 584
    iget-object v3, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-object v3, v3, Lcom/google/android/a/r;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 582
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 586
    :cond_8
    iput v8, p0, Lcom/google/android/a/l;->B:I

    .line 588
    :cond_9
    iget-object v0, p0, Lcom/google/android/a/l;->g:Lcom/google/android/a/p;

    iget-object v1, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/a/l;->a(JLcom/google/android/a/p;Lcom/google/android/a/r;)I

    move-result v0

    .line 589
    if-eqz p3, :cond_5

    iget v1, p0, Lcom/google/android/a/l;->F:I

    if-ne v1, v7, :cond_5

    const/4 v1, -0x2

    if-ne v0, v1, :cond_5

    .line 590
    iput v8, p0, Lcom/google/android/a/l;->F:I

    goto :goto_2

    .line 607
    :cond_a
    if-ne v0, v9, :cond_d

    .line 608
    iget v0, p0, Lcom/google/android/a/l;->B:I

    if-ne v0, v8, :cond_b

    .line 612
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->d()V

    .line 613
    iput v7, p0, Lcom/google/android/a/l;->B:I

    .line 615
    :cond_b
    iput-boolean v7, p0, Lcom/google/android/a/l;->G:Z

    .line 616
    iget-boolean v0, p0, Lcom/google/android/a/l;->D:Z

    if-nez v0, :cond_c

    .line 617
    invoke-direct {p0}, Lcom/google/android/a/l;->A()V

    goto/16 :goto_0

    .line 621
    :cond_c
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/a/l;->r:Z

    if-nez v0, :cond_0

    .line 624
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/l;->E:Z

    .line 625
    iget-object v4, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget v5, p0, Lcom/google/android/a/l;->x:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x4

    invoke-virtual/range {v4 .. v10}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 626
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/a/l;->x:I
    :try_end_0
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 628
    :catch_0
    move-exception v0

    .line 629
    invoke-direct {p0, v0}, Lcom/google/android/a/l;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 630
    new-instance v1, Lcom/google/android/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 634
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/a/l;->J:Z

    if-eqz v0, :cond_10

    .line 637
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->c()Z

    move-result v0

    if-nez v0, :cond_f

    .line 638
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->d()V

    .line 639
    iget v0, p0, Lcom/google/android/a/l;->B:I

    if-ne v0, v8, :cond_e

    .line 642
    iput v7, p0, Lcom/google/android/a/l;->B:I

    :cond_e
    move v2, v7

    .line 644
    goto/16 :goto_0

    .line 646
    :cond_f
    iput-boolean v2, p0, Lcom/google/android/a/l;->J:Z

    .line 648
    :cond_10
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v0}, Lcom/google/android/a/r;->a()Z

    move-result v0

    .line 649
    invoke-direct {p0, v0}, Lcom/google/android/a/l;->a(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/a/l;->I:Z

    .line 650
    iget-boolean v1, p0, Lcom/google/android/a/l;->I:Z

    if-nez v1, :cond_0

    .line 653
    iget-boolean v1, p0, Lcom/google/android/a/l;->p:Z

    if-eqz v1, :cond_12

    if-nez v0, :cond_12

    .line 654
    iget-object v1, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-object v1, v1, Lcom/google/android/a/r;->b:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/google/android/a/f/h;->a(Ljava/nio/ByteBuffer;)V

    .line 655
    iget-object v1, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-object v1, v1, Lcom/google/android/a/r;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    if-nez v1, :cond_11

    move v2, v7

    .line 656
    goto/16 :goto_0

    .line 658
    :cond_11
    iput-boolean v2, p0, Lcom/google/android/a/l;->p:Z

    .line 661
    :cond_12
    :try_start_1
    iget-object v1, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-object v1, v1, Lcom/google/android/a/r;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 662
    iget-object v1, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget v1, v1, Lcom/google/android/a/r;->c:I

    sub-int v1, v3, v1

    .line 663
    iget-object v2, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    iget-wide v4, v2, Lcom/google/android/a/r;->e:J

    .line 664
    iget-object v2, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-virtual {v2}, Lcom/google/android/a/r;->b()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 665
    iget-object v2, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 667
    :cond_13
    if-eqz v0, :cond_14

    .line 668
    iget-object v0, p0, Lcom/google/android/a/l;->f:Lcom/google/android/a/r;

    invoke-static {v0, v1}, Lcom/google/android/a/l;->a(Lcom/google/android/a/r;I)Landroid/media/MediaCodec$CryptoInfo;

    move-result-object v3

    .line 670
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/a/l;->x:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V

    .line 674
    :goto_4
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/a/l;->x:I

    .line 675
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/l;->D:Z

    .line 676
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/l;->B:I

    .line 677
    invoke-virtual {p0, v4, v5}, Lcom/google/android/a/l;->b(J)V

    move v2, v7

    .line 682
    goto/16 :goto_0

    .line 672
    :cond_14
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/a/l;->x:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 678
    :catch_1
    move-exception v0

    .line 679
    invoke-direct {p0, v0}, Lcom/google/android/a/l;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 680
    new-instance v1, Lcom/google/android/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/16 v1, 0x12

    .line 995
    sget v0, Lcom/google/android/a/f/n;->a:I

    if-lt v0, v1, :cond_1

    sget v0, Lcom/google/android/a/f/n;->a:I

    if-ne v0, v1, :cond_0

    const-string v0, "OMX.SEC.avc.dec"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "OMX.SEC.avc.dec.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/a/f/n;->d:Ljava/lang/String;

    const-string v1, "SM-G800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "OMX.Exynos.avc.dec"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "OMX.Exynos.avc.dec.secure"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/a/o;)Z
    .locals 2

    .prologue
    .line 1013
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/a/o;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "OMX.MTK.VIDEO.DECODER.AVC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 710
    iget-boolean v1, p0, Lcom/google/android/a/l;->z:Z

    if-nez v1, :cond_1

    .line 721
    :cond_0
    :goto_0
    return v0

    .line 713
    :cond_1
    iget-object v1, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v1}, Lcom/google/android/a/b/b;->b()I

    move-result v1

    .line 714
    if-nez v1, :cond_2

    .line 715
    new-instance v0, Lcom/google/android/a/e;

    iget-object v1, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v1}, Lcom/google/android/a/b/b;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 717
    :cond_2
    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    if-nez p1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/a/l;->e:Z

    if-nez v1, :cond_0

    .line 719
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/android/a/o;)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 702
    invoke-virtual {p1}, Lcom/google/android/a/o;->b()Landroid/media/MediaFormat;

    move-result-object v0

    .line 703
    iget-boolean v1, p0, Lcom/google/android/a/l;->k:Z

    if-eqz v1, :cond_0

    .line 704
    const-string v1, "auto-frc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 706
    :cond_0
    return-object v0
.end method

.method private b(Lcom/google/android/a/l$a;)V
    .locals 2

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/l;->j:Lcom/google/android/a/l$b;

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/google/android/a/l;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/l$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/a/l$1;-><init>(Lcom/google/android/a/l;Lcom/google/android/a/l$a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 949
    :cond_0
    return-void
.end method

.method private b(JJ)Z
    .locals 11

    .prologue
    .line 856
    iget-boolean v0, p0, Lcom/google/android/a/l;->H:Z

    if-eqz v0, :cond_0

    .line 857
    const/4 v0, 0x0

    .line 896
    :goto_0
    return v0

    .line 860
    :cond_0
    iget v0, p0, Lcom/google/android/a/l;->y:I

    if-gez v0, :cond_1

    .line 861
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    invoke-virtual {p0}, Lcom/google/android/a/l;->o()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, Lcom/google/android/a/l;->y:I

    .line 864
    :cond_1
    iget v0, p0, Lcom/google/android/a/l;->y:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_2

    .line 865
    invoke-direct {p0}, Lcom/google/android/a/l;->z()V

    .line 866
    const/4 v0, 0x1

    goto :goto_0

    .line 867
    :cond_2
    iget v0, p0, Lcom/google/android/a/l;->y:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_3

    .line 868
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/l;->v:[Ljava/nio/ByteBuffer;

    .line 869
    iget-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->d:I

    .line 870
    const/4 v0, 0x1

    goto :goto_0

    .line 871
    :cond_3
    iget v0, p0, Lcom/google/android/a/l;->y:I

    if-gez v0, :cond_6

    .line 872
    iget-boolean v0, p0, Lcom/google/android/a/l;->r:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/a/l;->G:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/a/l;->C:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 874
    :cond_4
    invoke-direct {p0}, Lcom/google/android/a/l;->A()V

    .line 875
    const/4 v0, 0x1

    goto :goto_0

    .line 877
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 880
    :cond_6
    iget-object v0, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    .line 881
    invoke-direct {p0}, Lcom/google/android/a/l;->A()V

    .line 882
    const/4 v0, 0x0

    goto :goto_0

    .line 885
    :cond_7
    iget-object v0, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/a/l;->i(J)I

    move-result v0

    .line 886
    iget-object v6, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/android/a/l;->v:[Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/google/android/a/l;->y:I

    aget-object v7, v1, v2

    iget-object v8, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    iget v9, p0, Lcom/google/android/a/l;->y:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    const/4 v10, 0x1

    :goto_1
    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/a/l;->a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 888
    iget-object v1, p0, Lcom/google/android/a/l;->i:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {p0, v2, v3}, Lcom/google/android/a/l;->c(J)V

    .line 889
    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 890
    iget-object v1, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 892
    :cond_8
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/a/l;->y:I

    .line 893
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 886
    :cond_9
    const/4 v10, 0x0

    goto :goto_1

    .line 896
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1030
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x11

    if-gt v0, v1, :cond_0

    const-string v0, "OMX.rk.video_decoder.avc"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lcom/google/android/a/o;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1062
    sget v1, Lcom/google/android/a/f/n;->a:I

    const/16 v2, 0x12

    if-gt v1, v2, :cond_0

    iget v1, p1, Lcom/google/android/a/o;->n:I

    if-ne v1, v0, :cond_0

    const-string v1, "OMX.MTK.AUDIO.DECODER.MP3"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1045
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x17

    if-gt v0, v1, :cond_0

    const-string v0, "OMX.google.vorbis.decoder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(J)V
    .locals 3

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/a/l;->g:Lcom/google/android/a/p;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/a/l;->a(JLcom/google/android/a/p;Lcom/google/android/a/r;)I

    move-result v0

    .line 502
    const/4 v1, -0x4

    if-ne v0, v1, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/a/l;->g:Lcom/google/android/a/p;

    invoke-virtual {p0, v0}, Lcom/google/android/a/l;->a(Lcom/google/android/a/p;)V

    .line 505
    :cond_0
    return-void
.end method

.method private i(J)I
    .locals 7

    .prologue
    .line 976
    iget-object v0, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 977
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 978
    iget-object v0, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    move v0, v1

    .line 982
    :goto_1
    return v0

    .line 977
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 982
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private i()Z
    .locals 6

    .prologue
    .line 837
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/a/l;->w:J

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 3

    .prologue
    .line 905
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 906
    iget-boolean v1, p0, Lcom/google/android/a/l;->t:Z

    if-eqz v1, :cond_0

    .line 907
    const-string v1, "channel-count"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 909
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/a/l;->a(Landroid/media/MediaFormat;)V

    .line 910
    iget-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->c:I

    .line 911
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/a/k;Ljava/lang/String;Z)Lcom/google/android/a/d;
    .locals 1

    .prologue
    .line 298
    invoke-interface {p1, p2, p3}, Lcom/google/android/a/k;->a(Ljava/lang/String;Z)Lcom/google/android/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected a(J)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 461
    iput v0, p0, Lcom/google/android/a/l;->F:I

    .line 462
    iput-boolean v0, p0, Lcom/google/android/a/l;->G:Z

    .line 463
    iput-boolean v0, p0, Lcom/google/android/a/l;->H:Z

    .line 464
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 465
    invoke-direct {p0}, Lcom/google/android/a/l;->a()V

    .line 467
    :cond_0
    return-void
.end method

.method protected a(JJZ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 482
    if-eqz p5, :cond_6

    iget v0, p0, Lcom/google/android/a/l;->F:I

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/a/l;->F:I

    .line 485
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    if-nez v0, :cond_0

    .line 486
    invoke-direct {p0, p1, p2}, Lcom/google/android/a/l;->h(J)V

    .line 488
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/a/l;->j()V

    .line 489
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_4

    .line 490
    const-string v0, "drainAndFeed"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 491
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/a/l;->b(JJ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 492
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/a/l;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    :cond_2
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/a/l;->a(JZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 495
    :cond_3
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 497
    :cond_4
    iget-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/b;->a()V

    .line 498
    return-void

    .line 482
    :cond_5
    iget v0, p0, Lcom/google/android/a/l;->F:I

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method protected abstract a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
.end method

.method protected a(Landroid/media/MediaFormat;)V
    .locals 0

    .prologue
    .line 760
    return-void
.end method

.method protected a(Lcom/google/android/a/p;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 731
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    .line 732
    iget-object v1, p1, Lcom/google/android/a/p;->a:Lcom/google/android/a/o;

    iput-object v1, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    .line 733
    iget-object v1, p1, Lcom/google/android/a/p;->b:Lcom/google/android/a/b/a;

    iput-object v1, p0, Lcom/google/android/a/l;->m:Lcom/google/android/a/b/a;

    .line 734
    iget-object v1, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget-boolean v2, p0, Lcom/google/android/a/l;->o:Z

    iget-object v3, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/google/android/a/l;->a(Landroid/media/MediaCodec;ZLcom/google/android/a/o;Lcom/google/android/a/o;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 735
    iput-boolean v4, p0, Lcom/google/android/a/l;->A:Z

    .line 736
    iput v4, p0, Lcom/google/android/a/l;->B:I

    .line 747
    :goto_0
    return-void

    .line 738
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/a/l;->D:Z

    if-eqz v0, :cond_1

    .line 740
    iput v4, p0, Lcom/google/android/a/l;->C:I

    goto :goto_0

    .line 743
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/l;->m()V

    .line 744
    invoke-virtual {p0}, Lcom/google/android/a/l;->j()V

    goto :goto_0
.end method

.method protected abstract a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
.end method

.method protected a(Landroid/media/MediaCodec;ZLcom/google/android/a/o;Lcom/google/android/a/o;)Z
    .locals 1

    .prologue
    .line 812
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract a(Lcom/google/android/a/k;Lcom/google/android/a/o;)Z
.end method

.method protected final a(Lcom/google/android/a/o;)Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/a/l;->c:Lcom/google/android/a/k;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/a/l;->a(Lcom/google/android/a/k;Lcom/google/android/a/o;)Z

    move-result v0

    return v0
.end method

.method protected b(J)V
    .locals 0

    .prologue
    .line 782
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 472
    return-void
.end method

.method protected c(J)V
    .locals 0

    .prologue
    .line 793
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 477
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/android/a/l;->H:Z

    return v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/a/l;->I:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/a/l;->F:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/a/l;->y:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/a/l;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 410
    iput-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    .line 411
    iput-object v0, p0, Lcom/google/android/a/l;->m:Lcom/google/android/a/b/a;

    .line 413
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/a/l;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 416
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/a/l;->z:Z

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v0}, Lcom/google/android/a/b/b;->a()V

    .line 418
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/l;->z:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    :cond_0
    invoke-super {p0}, Lcom/google/android/a/t;->g()V

    .line 424
    return-void

    .line 421
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/a/t;->g()V

    throw v0

    .line 415
    :catchall_1
    move-exception v0

    .line 416
    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/a/l;->z:Z

    if-eqz v1, :cond_1

    .line 417
    iget-object v1, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v1}, Lcom/google/android/a/b/b;->a()V

    .line 418
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/a/l;->z:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 421
    :cond_1
    invoke-super {p0}, Lcom/google/android/a/t;->g()V

    throw v0

    :catchall_2
    move-exception v0

    invoke-super {p0}, Lcom/google/android/a/t;->g()V

    throw v0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 771
    return-void
.end method

.method protected final j()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v2, 0x0

    .line 314
    invoke-virtual {p0}, Lcom/google/android/a/l;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    iget-object v3, v0, Lcom/google/android/a/o;->b:Ljava/lang/String;

    .line 320
    const/4 v0, 0x0

    .line 321
    iget-object v1, p0, Lcom/google/android/a/l;->m:Lcom/google/android/a/b/a;

    if-eqz v1, :cond_8

    .line 322
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    if-nez v0, :cond_2

    .line 323
    new-instance v0, Lcom/google/android/a/e;

    const-string v1, "Media requires a DrmSessionManager"

    invoke-direct {v0, v1}, Lcom/google/android/a/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 325
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/a/l;->z:Z

    if-nez v0, :cond_3

    .line 326
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    iget-object v1, p0, Lcom/google/android/a/l;->m:Lcom/google/android/a/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/a/b/b;->a(Lcom/google/android/a/b/a;)V

    .line 327
    iput-boolean v9, p0, Lcom/google/android/a/l;->z:Z

    .line 329
    :cond_3
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v0}, Lcom/google/android/a/b/b;->b()I

    move-result v0

    .line 330
    if-nez v0, :cond_4

    .line 331
    new-instance v0, Lcom/google/android/a/e;

    iget-object v1, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v1}, Lcom/google/android/a/b/b;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 332
    :cond_4
    if-eq v0, v10, :cond_5

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 334
    :cond_5
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v0}, Lcom/google/android/a/b/b;->c()Landroid/media/MediaCrypto;

    move-result-object v1

    .line 335
    iget-object v0, p0, Lcom/google/android/a/l;->d:Lcom/google/android/a/b/b;

    invoke-interface {v0, v3}, Lcom/google/android/a/b/b;->a(Ljava/lang/String;)Z

    move-result v0

    move v6, v0

    move-object v0, v1

    .line 344
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/a/l;->c:Lcom/google/android/a/k;

    invoke-virtual {p0, v1, v3, v6}, Lcom/google/android/a/l;->a(Lcom/google/android/a/k;Ljava/lang/String;Z)Lcom/google/android/a/d;
    :try_end_0
    .catch Lcom/google/android/a/m$b; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v3, v1

    .line 350
    :goto_2
    if-nez v3, :cond_6

    .line 351
    new-instance v1, Lcom/google/android/a/l$a;

    iget-object v4, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    const v5, -0xc34f

    invoke-direct {v1, v4, v2, v6, v5}, Lcom/google/android/a/l$a;-><init>(Lcom/google/android/a/o;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v1}, Lcom/google/android/a/l;->a(Lcom/google/android/a/l$a;)V

    .line 355
    :cond_6
    iget-object v1, v3, Lcom/google/android/a/d;->a:Ljava/lang/String;

    .line 356
    iget-boolean v2, v3, Lcom/google/android/a/d;->b:Z

    iput-boolean v2, p0, Lcom/google/android/a/l;->o:Z

    .line 357
    iget-object v2, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    invoke-static {v1, v2}, Lcom/google/android/a/l;->a(Ljava/lang/String;Lcom/google/android/a/o;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/l;->p:Z

    .line 358
    invoke-static {v1}, Lcom/google/android/a/l;->a(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/l;->q:Z

    .line 359
    invoke-static {v1}, Lcom/google/android/a/l;->b(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/l;->r:Z

    .line 360
    invoke-static {v1}, Lcom/google/android/a/l;->c(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/l;->s:Z

    .line 361
    iget-object v2, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    invoke-static {v1, v2}, Lcom/google/android/a/l;->b(Ljava/lang/String;Lcom/google/android/a/o;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/a/l;->t:Z

    .line 363
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 364
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createByCodecName("

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ")"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 365
    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    .line 366
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 367
    const-string v2, "configureCodec"

    invoke-static {v2}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 368
    iget-object v2, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    iget-boolean v3, v3, Lcom/google/android/a/d;->b:Z

    iget-object v7, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    invoke-direct {p0, v7}, Lcom/google/android/a/l;->b(Lcom/google/android/a/o;)Landroid/media/MediaFormat;

    move-result-object v7

    invoke-virtual {p0, v2, v3, v7, v0}, Lcom/google/android/a/l;->a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V

    .line 369
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 370
    const-string v0, "codec.start()"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 372
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 373
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 374
    sub-long v4, v2, v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/l;->a(Ljava/lang/String;JJ)V

    .line 376
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/l;->u:[Ljava/nio/ByteBuffer;

    .line 377
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/l;->v:[Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 382
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/a/l;->u()I

    move-result v0

    if-ne v0, v10, :cond_7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_4
    iput-wide v0, p0, Lcom/google/android/a/l;->w:J

    .line 384
    iput v8, p0, Lcom/google/android/a/l;->x:I

    .line 385
    iput v8, p0, Lcom/google/android/a/l;->y:I

    .line 386
    iput-boolean v9, p0, Lcom/google/android/a/l;->J:Z

    .line 387
    iget-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->a:I

    goto/16 :goto_0

    .line 345
    :catch_0
    move-exception v1

    .line 346
    new-instance v3, Lcom/google/android/a/l$a;

    iget-object v4, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    const v5, -0xc34e

    invoke-direct {v3, v4, v1, v6, v5}, Lcom/google/android/a/l$a;-><init>(Lcom/google/android/a/o;Ljava/lang/Throwable;ZI)V

    invoke-direct {p0, v3}, Lcom/google/android/a/l;->a(Lcom/google/android/a/l$a;)V

    move-object v3, v2

    goto/16 :goto_2

    .line 378
    :catch_1
    move-exception v0

    .line 379
    new-instance v2, Lcom/google/android/a/l$a;

    iget-object v3, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    invoke-direct {v2, v3, v0, v6, v1}, Lcom/google/android/a/l$a;-><init>(Lcom/google/android/a/o;Ljava/lang/Throwable;ZLjava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/a/l;->a(Lcom/google/android/a/l$a;)V

    goto :goto_3

    .line 382
    :cond_7
    const-wide/16 v0, -0x1

    goto :goto_4

    :cond_8
    move v6, v0

    move-object v0, v2

    goto/16 :goto_1
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/l;->l:Lcom/google/android/a/o;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final l()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected m()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 427
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 428
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/l;->w:J

    .line 429
    iput v4, p0, Lcom/google/android/a/l;->x:I

    .line 430
    iput v4, p0, Lcom/google/android/a/l;->y:I

    .line 431
    iput-boolean v2, p0, Lcom/google/android/a/l;->I:Z

    .line 432
    iget-object v0, p0, Lcom/google/android/a/l;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 433
    iput-object v3, p0, Lcom/google/android/a/l;->u:[Ljava/nio/ByteBuffer;

    .line 434
    iput-object v3, p0, Lcom/google/android/a/l;->v:[Ljava/nio/ByteBuffer;

    .line 435
    iput-boolean v2, p0, Lcom/google/android/a/l;->A:Z

    .line 436
    iput-boolean v2, p0, Lcom/google/android/a/l;->D:Z

    .line 437
    iput-boolean v2, p0, Lcom/google/android/a/l;->o:Z

    .line 438
    iput-boolean v2, p0, Lcom/google/android/a/l;->p:Z

    .line 439
    iput-boolean v2, p0, Lcom/google/android/a/l;->q:Z

    .line 440
    iput-boolean v2, p0, Lcom/google/android/a/l;->r:Z

    .line 441
    iput-boolean v2, p0, Lcom/google/android/a/l;->s:Z

    .line 442
    iput-boolean v2, p0, Lcom/google/android/a/l;->t:Z

    .line 443
    iput-boolean v2, p0, Lcom/google/android/a/l;->E:Z

    .line 444
    iput v2, p0, Lcom/google/android/a/l;->B:I

    .line 445
    iput v2, p0, Lcom/google/android/a/l;->C:I

    .line 446
    iget-object v0, p0, Lcom/google/android/a/l;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->b:I

    .line 448
    :try_start_0
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 451
    :try_start_1
    iget-object v0, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 453
    iput-object v3, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    .line 457
    :cond_0
    return-void

    .line 453
    :catchall_0
    move-exception v0

    iput-object v3, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    throw v0

    .line 450
    :catchall_1
    move-exception v0

    .line 451
    :try_start_2
    iget-object v1, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 453
    iput-object v3, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    throw v0

    :catchall_2
    move-exception v0

    iput-object v3, p0, Lcom/google/android/a/l;->n:Landroid/media/MediaCodec;

    throw v0
.end method

.method protected final n()I
    .locals 1

    .prologue
    .line 833
    iget v0, p0, Lcom/google/android/a/l;->F:I

    return v0
.end method

.method protected o()J
    .locals 2

    .prologue
    .line 846
    const-wide/16 v0, 0x0

    return-wide v0
.end method
