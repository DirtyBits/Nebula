.class public final Lcom/google/android/a/f/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/f/c$a;
    }
.end annotation


# static fields
.field private static final a:[B

.field private static final b:[I

.field private static final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/a/f/c;->a:[B

    .line 50
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/a/f/c;->b:[I

    .line 68
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/a/f/c;->c:[I

    return-void

    .line 46
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 50
    :array_1
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data

    .line 68
    :array_2
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x8
        -0x1
        -0x1
        -0x1
        0x7
        0x8
        -0x1
        0x8
        -0x1
    .end array-data
.end method

.method public static a([B)Landroid/util/Pair;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0xd

    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x4

    .line 108
    new-instance v4, Lcom/google/android/a/f/i;

    invoke-direct {v4, p0}, Lcom/google/android/a/f/i;-><init>([B)V

    .line 109
    invoke-virtual {v4, v8}, Lcom/google/android/a/f/i;->c(I)I

    move-result v5

    .line 110
    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v3

    .line 112
    const/16 v0, 0xf

    if-ne v3, v0, :cond_1

    .line 113
    const/16 v0, 0x18

    invoke-virtual {v4, v0}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 118
    :goto_0
    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v3

    .line 119
    if-eq v5, v8, :cond_0

    const/16 v6, 0x1d

    if-ne v5, v6, :cond_6

    .line 125
    :cond_0
    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v5

    .line 126
    const/16 v0, 0xf

    if-ne v5, v0, :cond_3

    .line 127
    const/16 v0, 0x18

    invoke-virtual {v4, v0}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 132
    :goto_1
    invoke-virtual {v4, v8}, Lcom/google/android/a/f/i;->c(I)I

    move-result v5

    .line 133
    const/16 v6, 0x16

    if-ne v5, v6, :cond_6

    .line 135
    invoke-virtual {v4, v7}, Lcom/google/android/a/f/i;->c(I)I

    move-result v3

    move v10, v3

    move v3, v0

    move v0, v10

    .line 138
    :goto_2
    sget-object v4, Lcom/google/android/a/f/c;->c:[I

    aget v0, v4, v0

    .line 139
    const/4 v4, -0x1

    if-eq v0, v4, :cond_5

    :goto_3
    invoke-static {v1}, Lcom/google/android/a/f/b;->a(Z)V

    .line 140
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 115
    :cond_1
    if-ge v3, v9, :cond_2

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/android/a/f/b;->a(Z)V

    .line 116
    sget-object v0, Lcom/google/android/a/f/c;->b:[I

    aget v0, v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    .line 115
    goto :goto_4

    .line 129
    :cond_3
    if-ge v5, v9, :cond_4

    move v0, v1

    :goto_5
    invoke-static {v0}, Lcom/google/android/a/f/b;->a(Z)V

    .line 130
    sget-object v0, Lcom/google/android/a/f/c;->b:[I

    aget v0, v0, v5

    goto :goto_1

    :cond_4
    move v0, v2

    .line 129
    goto :goto_5

    :cond_5
    move v1, v2

    .line 139
    goto :goto_3

    :cond_6
    move v10, v3

    move v3, v0

    move v0, v10

    goto :goto_2
.end method

.method public static a(Lcom/google/android/a/f/i;)Lcom/google/android/a/f/c$a;
    .locals 14

    .prologue
    const/16 v1, 0x8

    const/4 v13, 0x3

    const/16 v3, 0x10

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 278
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/i;->c(I)I

    move-result v0

    .line 279
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/i;->b(I)V

    .line 280
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 283
    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    const/16 v2, 0x6e

    if-eq v0, v2, :cond_0

    const/16 v2, 0x7a

    if-eq v0, v2, :cond_0

    const/16 v2, 0xf4

    if-eq v0, v2, :cond_0

    const/16 v2, 0x2c

    if-eq v0, v2, :cond_0

    const/16 v2, 0x53

    if-eq v0, v2, :cond_0

    const/16 v2, 0x56

    if-eq v0, v2, :cond_0

    const/16 v2, 0x76

    if-eq v0, v2, :cond_0

    const/16 v2, 0x80

    if-eq v0, v2, :cond_0

    const/16 v2, 0x8a

    if-ne v0, v2, :cond_14

    .line 286
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v5

    .line 287
    if-ne v5, v13, :cond_1

    .line 288
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 290
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 291
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 292
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v0

    .line 294
    if-eqz v0, :cond_5

    .line 295
    if-eq v5, v13, :cond_3

    move v0, v1

    :goto_0
    move v7, v6

    .line 296
    :goto_1
    if-ge v7, v0, :cond_5

    .line 297
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v2

    .line 298
    if-eqz v2, :cond_2

    .line 299
    const/4 v2, 0x6

    if-ge v7, v2, :cond_4

    move v2, v3

    :goto_2
    invoke-static {p0, v2}, Lcom/google/android/a/f/c;->a(Lcom/google/android/a/f/i;I)V

    .line 296
    :cond_2
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_1

    .line 295
    :cond_3
    const/16 v0, 0xc

    goto :goto_0

    .line 299
    :cond_4
    const/16 v2, 0x40

    goto :goto_2

    :cond_5
    move v0, v5

    .line 305
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 306
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v2

    int-to-long v8, v2

    .line 307
    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_9

    .line 308
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 318
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 319
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 321
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v2

    add-int/lit8 v5, v2, 0x1

    .line 322
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v2

    add-int/lit8 v7, v2, 0x1

    .line 323
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v8

    .line 324
    if-eqz v8, :cond_a

    move v2, v4

    :goto_4
    rsub-int/lit8 v2, v2, 0x2

    mul-int/2addr v2, v7

    .line 325
    if-nez v8, :cond_7

    .line 326
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 329
    :cond_7
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 330
    mul-int/lit8 v7, v5, 0x10

    .line 331
    mul-int/lit8 v5, v2, 0x10

    .line 332
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v2

    .line 333
    if-eqz v2, :cond_13

    .line 334
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v9

    .line 335
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v10

    .line 336
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v11

    .line 337
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v12

    .line 339
    if-nez v0, :cond_c

    .line 341
    if-eqz v8, :cond_b

    move v0, v4

    :goto_5
    rsub-int/lit8 v0, v0, 0x2

    .line 348
    :goto_6
    add-int v2, v9, v10

    mul-int/2addr v2, v4

    sub-int v2, v7, v2

    .line 349
    add-int v4, v11, v12

    mul-int/2addr v0, v4

    sub-int v0, v5, v0

    .line 352
    :goto_7
    const/high16 v4, 0x3f800000    # 1.0f

    .line 353
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v5

    .line 354
    if-eqz v5, :cond_8

    .line 355
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->b()Z

    move-result v5

    .line 356
    if-eqz v5, :cond_8

    .line 357
    invoke-virtual {p0, v1}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    .line 358
    const/16 v5, 0xff

    if-ne v1, v5, :cond_10

    .line 359
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/i;->c(I)I

    move-result v1

    .line 360
    invoke-virtual {p0, v3}, Lcom/google/android/a/f/i;->c(I)I

    move-result v3

    .line 361
    if-eqz v1, :cond_12

    if-eqz v3, :cond_12

    .line 362
    int-to-float v1, v1

    int-to-float v3, v3

    div-float/2addr v1, v3

    :goto_8
    move v4, v1

    .line 372
    :cond_8
    :goto_9
    new-instance v1, Lcom/google/android/a/f/c$a;

    invoke-direct {v1, v2, v0, v4}, Lcom/google/android/a/f/c$a;-><init>(IIF)V

    return-object v1

    .line 309
    :cond_9
    const-wide/16 v10, 0x1

    cmp-long v2, v8, v10

    if-nez v2, :cond_6

    .line 310
    invoke-virtual {p0, v4}, Lcom/google/android/a/f/i;->b(I)V

    .line 311
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->e()I

    .line 312
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->e()I

    .line 313
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    move-result v2

    int-to-long v8, v2

    move v2, v6

    .line 314
    :goto_a
    int-to-long v10, v2

    cmp-long v5, v10, v8

    if-gez v5, :cond_6

    .line 315
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->d()I

    .line 314
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_a
    move v2, v6

    .line 324
    goto/16 :goto_4

    :cond_b
    move v0, v6

    .line 341
    goto :goto_5

    .line 343
    :cond_c
    if-ne v0, v13, :cond_d

    move v2, v4

    .line 344
    :goto_b
    if-ne v0, v4, :cond_e

    const/4 v0, 0x2

    .line 346
    :goto_c
    if-eqz v8, :cond_f

    :goto_d
    rsub-int/lit8 v4, v4, 0x2

    mul-int/2addr v0, v4

    move v4, v2

    goto :goto_6

    .line 343
    :cond_d
    const/4 v2, 0x2

    goto :goto_b

    :cond_e
    move v0, v4

    .line 344
    goto :goto_c

    :cond_f
    move v4, v6

    .line 346
    goto :goto_d

    .line 364
    :cond_10
    sget-object v3, Lcom/google/android/a/f/h;->b:[F

    array-length v3, v3

    if-ge v1, v3, :cond_11

    .line 365
    sget-object v3, Lcom/google/android/a/f/h;->b:[F

    aget v4, v3, v1

    goto :goto_9

    .line 367
    :cond_11
    const-string v3, "CodecSpecificDataUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected aspect_ratio_idc value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :cond_12
    move v1, v4

    goto :goto_8

    :cond_13
    move v0, v5

    move v2, v7

    goto/16 :goto_7

    :cond_14
    move v0, v4

    goto/16 :goto_3
.end method

.method private static a(Lcom/google/android/a/f/i;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 376
    .line 378
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, p1, :cond_2

    .line 379
    if-eqz v1, :cond_0

    .line 380
    invoke-virtual {p0}, Lcom/google/android/a/f/i;->e()I

    move-result v1

    .line 381
    add-int/2addr v1, v0

    add-int/lit16 v1, v1, 0x100

    rem-int/lit16 v1, v1, 0x100

    .line 383
    :cond_0
    if-nez v1, :cond_1

    .line 378
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 383
    goto :goto_1

    .line 385
    :cond_2
    return-void
.end method

.method public static a(III)[B
    .locals 4

    .prologue
    .line 153
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 154
    const/4 v1, 0x0

    shl-int/lit8 v2, p0, 0x3

    and-int/lit16 v2, v2, 0xf8

    shr-int/lit8 v3, p1, 0x1

    and-int/lit8 v3, v3, 0x7

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 155
    const/4 v1, 0x1

    shl-int/lit8 v2, p1, 0x7

    and-int/lit16 v2, v2, 0x80

    shl-int/lit8 v3, p2, 0x3

    and-int/lit8 v3, v3, 0x78

    or-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 156
    return-object v0
.end method

.method public static a([BII)[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    sget-object v0, Lcom/google/android/a/f/c;->a:[B

    array-length v0, v0

    add-int/2addr v0, p2

    new-array v0, v0, [B

    .line 196
    sget-object v1, Lcom/google/android/a/f/c;->a:[B

    sget-object v2, Lcom/google/android/a/f/c;->a:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 197
    sget-object v1, Lcom/google/android/a/f/c;->a:[B

    array-length v1, v1

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    return-object v0
.end method
