.class public Lcom/google/android/a/n;
.super Lcom/google/android/a/l;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/n$a;
    }
.end annotation


# instance fields
.field private final c:Lcom/google/android/a/w;

.field private final d:Lcom/google/android/a/n$a;

.field private final e:J

.field private final f:I

.field private final g:I

.field private h:Landroid/view/Surface;

.field private i:Z

.field private j:Z

.field private k:J

.field private l:J

.field private m:I

.field private n:I

.field private o:I

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:F

.field private u:I

.field private v:I

.field private w:I

.field private x:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/a/s;Lcom/google/android/a/k;IJLandroid/os/Handler;Lcom/google/android/a/n$a;I)V
    .locals 13

    .prologue
    .line 173
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v1 .. v12}, Lcom/google/android/a/n;-><init>(Landroid/content/Context;Lcom/google/android/a/s;Lcom/google/android/a/k;IJLcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/n$a;I)V

    .line 175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/a/s;Lcom/google/android/a/k;IJLcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/n$a;I)V
    .locals 9

    .prologue
    .line 202
    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v2 .. v8}, Lcom/google/android/a/l;-><init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/l$b;)V

    .line 204
    new-instance v2, Lcom/google/android/a/w;

    invoke-direct {v2, p1}, Lcom/google/android/a/w;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/a/n;->c:Lcom/google/android/a/w;

    .line 205
    iput p4, p0, Lcom/google/android/a/n;->f:I

    .line 206
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p5

    iput-wide v2, p0, Lcom/google/android/a/n;->e:J

    .line 207
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/a/n;->d:Lcom/google/android/a/n$a;

    .line 208
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/a/n;->g:I

    .line 209
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/a/n;->k:J

    .line 210
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/a/n;->q:I

    .line 211
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/a/n;->r:I

    .line 212
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/android/a/n;->t:F

    .line 213
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/android/a/n;->p:F

    .line 214
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/a/n;->u:I

    .line 215
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/a/n;->v:I

    .line 216
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/android/a/n;->x:F

    .line 217
    return-void
.end method

.method static synthetic a(Lcom/google/android/a/n;)Lcom/google/android/a/n$a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/a/n;->d:Lcom/google/android/a/n$a;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/n;->d:Lcom/google/android/a/n$a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/a/n;->u:I

    iget v1, p0, Lcom/google/android/a/n;->q:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/a/n;->v:I

    iget v1, p0, Lcom/google/android/a/n;->r:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/a/n;->w:I

    iget v1, p0, Lcom/google/android/a/n;->s:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/a/n;->x:F

    iget v1, p0, Lcom/google/android/a/n;->t:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 568
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    iget v2, p0, Lcom/google/android/a/n;->q:I

    .line 553
    iget v3, p0, Lcom/google/android/a/n;->r:I

    .line 554
    iget v4, p0, Lcom/google/android/a/n;->s:I

    .line 555
    iget v5, p0, Lcom/google/android/a/n;->t:F

    .line 556
    iget-object v6, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/a/n$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/n$1;-><init>(Lcom/google/android/a/n;IIIF)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 564
    iput v2, p0, Lcom/google/android/a/n;->u:I

    .line 565
    iput v3, p0, Lcom/google/android/a/n;->v:I

    .line 566
    iput v4, p0, Lcom/google/android/a/n;->w:I

    .line 567
    iput v5, p0, Lcom/google/android/a/n;->x:F

    goto :goto_0
.end method

.method private a(Landroid/media/MediaFormat;Z)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 500
    const-string v0, "max-input-size"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 542
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 505
    if-eqz p2, :cond_2

    const-string v1, "max-height"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 506
    const-string v1, "max-height"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 508
    :cond_2
    const-string v1, "width"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 509
    if-eqz p2, :cond_3

    const-string v3, "max-width"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 510
    const-string v1, "max-width"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 514
    :cond_3
    const-string v3, "mime"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_4
    :goto_1
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 516
    :pswitch_0
    const-string v3, "BRAVIA 4K 2015"

    sget-object v4, Lcom/google/android/a/f/n;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 522
    add-int/lit8 v1, v1, 0xf

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v0, v0, 0xf

    div-int/lit8 v0, v0, 0x10

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x10

    move v1, v0

    move v0, v2

    .line 540
    :goto_2
    mul-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v0, 0x2

    div-int v0, v1, v0

    .line 541
    const-string v1, "max-input-size"

    invoke-virtual {p1, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    goto :goto_0

    .line 514
    :sswitch_0
    const-string v5, "video/avc"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x0

    goto :goto_1

    :sswitch_1
    const-string v5, "video/x-vnd.on2.vp8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x1

    goto :goto_1

    :sswitch_2
    const-string v5, "video/hevc"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v3, v2

    goto :goto_1

    :sswitch_3
    const-string v5, "video/x-vnd.on2.vp9"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v3, 0x3

    goto :goto_1

    .line 527
    :pswitch_1
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v2

    .line 529
    goto :goto_2

    .line 532
    :pswitch_2
    mul-int/2addr v1, v0

    .line 533
    const/4 v0, 0x4

    .line 534
    goto :goto_2

    .line 514
    :sswitch_data_0
    .sparse-switch
        -0x63185e82 -> :sswitch_2
        0x4f62373a -> :sswitch_0
        0x5f50bed8 -> :sswitch_1
        0x5f50bed9 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/view/Surface;)V
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    if-ne v0, p1, :cond_1

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iput-object p1, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/n;->i:Z

    .line 311
    invoke-virtual {p0}, Lcom/google/android/a/n;->u()I

    move-result v0

    .line 312
    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 313
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/a/n;->m()V

    .line 314
    invoke-virtual {p0}, Lcom/google/android/a/n;->j()V

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/n;->d:Lcom/google/android/a/n$a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/a/n;->i:Z

    if-eqz v0, :cond_1

    .line 584
    :cond_0
    :goto_0
    return-void

    .line 575
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    .line 576
    iget-object v1, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/a/n$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/a/n$2;-><init>(Lcom/google/android/a/n;Landroid/view/Surface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/n;->i:Z

    goto :goto_0
.end method

.method private z()V
    .locals 7

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/n;->d:Lcom/google/android/a/n$a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/a/n;->m:I

    if-nez v0, :cond_1

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 592
    iget v2, p0, Lcom/google/android/a/n;->m:I

    .line 593
    iget-wide v4, p0, Lcom/google/android/a/n;->l:J

    sub-long v4, v0, v4

    .line 594
    iget-object v3, p0, Lcom/google/android/a/n;->b:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/a/n$3;

    invoke-direct {v6, p0, v2, v4, v5}, Lcom/google/android/a/n$3;-><init>(Lcom/google/android/a/n;IJ)V

    invoke-virtual {v3, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 601
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/n;->m:I

    .line 602
    iput-wide v0, p0, Lcom/google/android/a/n;->l:J

    goto :goto_0
.end method


# virtual methods
.method protected a(IJZ)V
    .locals 4

    .prologue
    .line 230
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/a/l;->a(IJZ)V

    .line 231
    if-eqz p4, :cond_0

    iget-wide v0, p0, Lcom/google/android/a/n;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 232
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/a/n;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/a/n;->k:J

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/n;->c:Lcom/google/android/a/w;

    invoke-virtual {v0}, Lcom/google/android/a/w;->a()V

    .line 235
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 294
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 295
    check-cast p2, Landroid/view/Surface;

    invoke-direct {p0, p2}, Lcom/google/android/a/n;->a(Landroid/view/Surface;)V

    .line 299
    :goto_0
    return-void

    .line 297
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/a/l;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(J)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 239
    invoke-super {p0, p1, p2}, Lcom/google/android/a/l;->a(J)V

    .line 240
    iput-boolean v0, p0, Lcom/google/android/a/n;->j:Z

    .line 241
    iput v0, p0, Lcom/google/android/a/n;->n:I

    .line 242
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/n;->k:J

    .line 243
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;I)V
    .locals 2

    .prologue
    .line 457
    const-string v0, "skipVideoBuffer"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 458
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 459
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 460
    iget-object v0, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->f:I

    .line 461
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;IJ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 489
    invoke-direct {p0}, Lcom/google/android/a/n;->a()V

    .line 490
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 491
    invoke-virtual {p1, p2, p3, p4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IJ)V

    .line 492
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 493
    iget-object v0, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->e:I

    .line 494
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/n;->j:Z

    .line 495
    invoke-direct {p0}, Lcom/google/android/a/n;->i()V

    .line 496
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    .prologue
    .line 327
    invoke-direct {p0, p3, p2}, Lcom/google/android/a/n;->a(Landroid/media/MediaFormat;Z)V

    .line 328
    iget-object v0, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {p1, p3, v0, p4, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 329
    iget v0, p0, Lcom/google/android/a/n;->f:I

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    .line 330
    return-void
.end method

.method protected a(Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 350
    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-left"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-top"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 353
    :goto_0
    if-eqz v1, :cond_3

    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v2, "crop-left"

    invoke-virtual {p1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iput v0, p0, Lcom/google/android/a/n;->q:I

    .line 356
    if-eqz v1, :cond_4

    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "crop-top"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    :goto_2
    iput v0, p0, Lcom/google/android/a/n;->r:I

    .line 359
    iget v0, p0, Lcom/google/android/a/n;->p:F

    iput v0, p0, Lcom/google/android/a/n;->t:F

    .line 360
    sget v0, Lcom/google/android/a/f/n;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_5

    .line 364
    iget v0, p0, Lcom/google/android/a/n;->o:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/a/n;->o:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    .line 365
    :cond_0
    iget v0, p0, Lcom/google/android/a/n;->q:I

    .line 366
    iget v1, p0, Lcom/google/android/a/n;->r:I

    iput v1, p0, Lcom/google/android/a/n;->q:I

    .line 367
    iput v0, p0, Lcom/google/android/a/n;->r:I

    .line 368
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/a/n;->t:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/a/n;->t:F

    .line 374
    :cond_1
    :goto_3
    return-void

    .line 350
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 353
    :cond_3
    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 356
    :cond_4
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 372
    :cond_5
    iget v0, p0, Lcom/google/android/a/n;->o:I

    iput v0, p0, Lcom/google/android/a/n;->s:I

    goto :goto_3
.end method

.method protected a(Lcom/google/android/a/p;)V
    .locals 2

    .prologue
    .line 334
    invoke-super {p0, p1}, Lcom/google/android/a/l;->a(Lcom/google/android/a/p;)V

    .line 335
    iget-object v0, p1, Lcom/google/android/a/p;->a:Lcom/google/android/a/o;

    iget v0, v0, Lcom/google/android/a/o;->m:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lcom/google/android/a/n;->p:F

    .line 337
    iget-object v0, p1, Lcom/google/android/a/p;->a:Lcom/google/android/a/o;

    iget v0, v0, Lcom/google/android/a/o;->l:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lcom/google/android/a/n;->o:I

    .line 339
    return-void

    .line 335
    :cond_0
    iget-object v0, p1, Lcom/google/android/a/p;->a:Lcom/google/android/a/o;

    iget v0, v0, Lcom/google/android/a/o;->m:F

    goto :goto_0

    .line 337
    :cond_1
    iget-object v0, p1, Lcom/google/android/a/p;->a:Lcom/google/android/a/o;

    iget v0, v0, Lcom/google/android/a/o;->l:I

    goto :goto_1
.end method

.method protected a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 11

    .prologue
    .line 387
    if-eqz p9, :cond_0

    .line 388
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/n;->a(Landroid/media/MediaCodec;I)V

    .line 389
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/n;->n:I

    .line 390
    const/4 v2, 0x1

    .line 453
    :goto_0
    return v2

    .line 393
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/a/n;->j:Z

    if-nez v2, :cond_2

    .line 394
    sget v2, Lcom/google/android/a/f/n;->a:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 395
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/a/n;->a(Landroid/media/MediaCodec;IJ)V

    .line 399
    :goto_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/n;->n:I

    .line 400
    const/4 v2, 0x1

    goto :goto_0

    .line 397
    :cond_1
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/n;->c(Landroid/media/MediaCodec;I)V

    goto :goto_1

    .line 403
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/a/n;->u()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 404
    const/4 v2, 0x0

    goto :goto_0

    .line 408
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long/2addr v2, p3

    .line 409
    move-object/from16 v0, p7

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    sub-long/2addr v4, p1

    sub-long v2, v4, v2

    .line 412
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 413
    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    add-long/2addr v2, v4

    .line 416
    iget-object v6, p0, Lcom/google/android/a/n;->c:Lcom/google/android/a/w;

    move-object/from16 v0, p7

    iget-wide v8, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {v6, v8, v9, v2, v3}, Lcom/google/android/a/w;->a(JJ)J

    move-result-wide v2

    .line 418
    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 420
    const-wide/16 v6, -0x7530

    cmp-long v6, v4, v6

    if-gez v6, :cond_4

    .line 422
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/n;->b(Landroid/media/MediaCodec;I)V

    .line 423
    const/4 v2, 0x1

    goto :goto_0

    .line 426
    :cond_4
    sget v6, Lcom/google/android/a/f/n;->a:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_5

    .line 428
    const-wide/32 v6, 0xc350

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 429
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/a/n;->a(Landroid/media/MediaCodec;IJ)V

    .line 430
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/n;->n:I

    .line 431
    const/4 v2, 0x1

    goto :goto_0

    .line 435
    :cond_5
    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-gez v2, :cond_7

    .line 436
    const-wide/16 v2, 0x2af8

    cmp-long v2, v4, v2

    if-lez v2, :cond_6

    .line 441
    const-wide/16 v2, 0x2710

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    :try_start_0
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :cond_6
    :goto_2
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/n;->c(Landroid/media/MediaCodec;I)V

    .line 447
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/a/n;->n:I

    .line 448
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 442
    :catch_0
    move-exception v2

    .line 443
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 453
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method protected a(Landroid/media/MediaCodec;ZLcom/google/android/a/o;Lcom/google/android/a/o;)Z
    .locals 2

    .prologue
    .line 379
    iget-object v0, p4, Lcom/google/android/a/o;->b:Ljava/lang/String;

    iget-object v1, p3, Lcom/google/android/a/o;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    iget v0, p3, Lcom/google/android/a/o;->h:I

    iget v1, p4, Lcom/google/android/a/o;->h:I

    if-ne v0, v1, :cond_1

    iget v0, p3, Lcom/google/android/a/o;->i:I

    iget v1, p4, Lcom/google/android/a/o;->i:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/android/a/k;Lcom/google/android/a/o;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 222
    iget-object v1, p2, Lcom/google/android/a/o;->b:Ljava/lang/String;

    .line 223
    invoke-static {v1}, Lcom/google/android/a/f/f;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "video/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p1, v1, v0}, Lcom/google/android/a/k;->a(Ljava/lang/String;Z)Lcom/google/android/a/d;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected b(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    .line 464
    const-string v0, "dropVideoBuffer"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 465
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 466
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 467
    iget-object v0, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->g:I

    .line 468
    iget v0, p0, Lcom/google/android/a/n;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/n;->m:I

    .line 469
    iget v0, p0, Lcom/google/android/a/n;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/a/n;->n:I

    .line 470
    iget-object v0, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v1, p0, Lcom/google/android/a/n;->n:I

    iget-object v2, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v2, v2, Lcom/google/android/a/b;->h:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/a/b;->h:I

    .line 472
    iget v0, p0, Lcom/google/android/a/n;->m:I

    iget v1, p0, Lcom/google/android/a/n;->g:I

    if-ne v0, v1, :cond_0

    .line 473
    invoke-direct {p0}, Lcom/google/android/a/n;->z()V

    .line 475
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 267
    invoke-super {p0}, Lcom/google/android/a/l;->c()V

    .line 268
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/n;->m:I

    .line 269
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/a/n;->l:J

    .line 270
    return-void
.end method

.method protected c(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 478
    invoke-direct {p0}, Lcom/google/android/a/n;->a()V

    .line 479
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/android/a/f/m;->a(Ljava/lang/String;)V

    .line 480
    invoke-virtual {p1, p2, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 481
    invoke-static {}, Lcom/google/android/a/f/m;->a()V

    .line 482
    iget-object v0, p0, Lcom/google/android/a/n;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->e:I

    .line 483
    iput-boolean v2, p0, Lcom/google/android/a/n;->j:Z

    .line 484
    invoke-direct {p0}, Lcom/google/android/a/n;->i()V

    .line 485
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 274
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/a/n;->k:J

    .line 275
    invoke-direct {p0}, Lcom/google/android/a/n;->z()V

    .line 276
    invoke-super {p0}, Lcom/google/android/a/l;->d()V

    .line 277
    return-void
.end method

.method protected f()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v6, -0x1

    .line 247
    invoke-super {p0}, Lcom/google/android/a/l;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/a/n;->j:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/a/n;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/a/n;->n()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 250
    :cond_0
    iput-wide v6, p0, Lcom/google/android/a/n;->k:J

    .line 261
    :cond_1
    :goto_0
    return v0

    .line 252
    :cond_2
    iget-wide v2, p0, Lcom/google/android/a/n;->k:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    move v0, v1

    .line 254
    goto :goto_0

    .line 255
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/a/n;->k:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 260
    iput-wide v6, p0, Lcom/google/android/a/n;->k:J

    move v0, v1

    .line 261
    goto :goto_0
.end method

.method protected g()V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, -0x1

    .line 281
    iput v0, p0, Lcom/google/android/a/n;->q:I

    .line 282
    iput v0, p0, Lcom/google/android/a/n;->r:I

    .line 283
    iput v1, p0, Lcom/google/android/a/n;->t:F

    .line 284
    iput v1, p0, Lcom/google/android/a/n;->p:F

    .line 285
    iput v0, p0, Lcom/google/android/a/n;->u:I

    .line 286
    iput v0, p0, Lcom/google/android/a/n;->v:I

    .line 287
    iput v1, p0, Lcom/google/android/a/n;->x:F

    .line 288
    iget-object v0, p0, Lcom/google/android/a/n;->c:Lcom/google/android/a/w;

    invoke-virtual {v0}, Lcom/google/android/a/w;->b()V

    .line 289
    invoke-super {p0}, Lcom/google/android/a/l;->g()V

    .line 290
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Lcom/google/android/a/l;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/n;->h:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
