.class public Lcom/google/android/a/j;
.super Lcom/google/android/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/i;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/a/j$a;
    }
.end annotation


# instance fields
.field private final c:Lcom/google/android/a/j$a;

.field private final d:Lcom/google/android/a/a/b;

.field private e:Z

.field private f:Landroid/media/MediaFormat;

.field private g:I

.field private h:J

.field private i:Z

.field private j:Z

.field private k:J


# direct methods
.method public constructor <init>(Lcom/google/android/a/s;Lcom/google/android/a/k;)V
    .locals 2

    .prologue
    .line 106
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/a/j;-><init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;Z)V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 122
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/a/j;-><init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/j$a;)V

    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/j$a;)V
    .locals 9

    .prologue
    .line 154
    const/4 v7, 0x0

    const/4 v8, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/a/j;-><init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/j$a;Lcom/google/android/a/a/a;I)V

    .line 156
    return-void
.end method

.method public constructor <init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/j$a;Lcom/google/android/a/a/a;I)V
    .locals 1

    .prologue
    .line 179
    invoke-direct/range {p0 .. p6}, Lcom/google/android/a/l;-><init>(Lcom/google/android/a/s;Lcom/google/android/a/k;Lcom/google/android/a/b/b;ZLandroid/os/Handler;Lcom/google/android/a/l$b;)V

    .line 181
    iput-object p6, p0, Lcom/google/android/a/j;->c:Lcom/google/android/a/j$a;

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/j;->g:I

    .line 183
    new-instance v0, Lcom/google/android/a/a/b;

    invoke-direct {v0, p7, p8}, Lcom/google/android/a/a/b;-><init>(Lcom/google/android/a/a/a;I)V

    iput-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    .line 184
    return-void
.end method

.method static synthetic a(Lcom/google/android/a/j;)Lcom/google/android/a/j$a;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/a/j;->c:Lcom/google/android/a/j$a;

    return-object v0
.end method

.method private a(IJJ)V
    .locals 8

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/j;->c:Lcom/google/android/a/j$a;

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/j$3;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/a/j$3;-><init>(Lcom/google/android/a/j;IJJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 443
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/a/a/b$d;)V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/j;->c:Lcom/google/android/a/j$a;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/j$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/a/j$1;-><init>(Lcom/google/android/a/j;Lcom/google/android/a/a/b$d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 420
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/a/a/b$f;)V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/j;->c:Lcom/google/android/a/j$a;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/google/android/a/j;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/a/j$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/a/j$2;-><init>(Lcom/google/android/a/j;Lcom/google/android/a/a/b$f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 431
    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {p0}, Lcom/google/android/a/j;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/a/a/b;->a(Z)J

    move-result-wide v0

    .line 289
    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 290
    iget-boolean v2, p0, Lcom/google/android/a/j;->i:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-wide v0, p0, Lcom/google/android/a/j;->h:J

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/j;->i:Z

    .line 294
    :cond_0
    iget-wide v0, p0, Lcom/google/android/a/j;->h:J

    return-wide v0

    .line 290
    :cond_1
    iget-wide v2, p0, Lcom/google/android/a/j;->h:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected a(Lcom/google/android/a/k;Ljava/lang/String;Z)Lcom/google/android/a/d;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 198
    invoke-virtual {p0, p2}, Lcom/google/android/a/j;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {p1}, Lcom/google/android/a/k;->a()Ljava/lang/String;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/j;->e:Z

    .line 202
    new-instance v0, Lcom/google/android/a/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/a/d;-><init>(Ljava/lang/String;Z)V

    .line 206
    :goto_0
    return-object v0

    .line 205
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/a/j;->e:Z

    .line 206
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/a/l;->a(Lcom/google/android/a/k;Ljava/lang/String;Z)Lcom/google/android/a/d;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(I)V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 398
    packed-switch p1, :pswitch_data_0

    .line 406
    invoke-super {p0, p1, p2}, Lcom/google/android/a/l;->a(ILjava/lang/Object;)V

    .line 409
    :goto_0
    return-void

    .line 400
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/a/a/b;->a(F)V

    goto :goto_0

    .line 403
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    check-cast p2, Landroid/media/PlaybackParams;

    invoke-virtual {v0, p2}, Lcom/google/android/a/a/b;->a(Landroid/media/PlaybackParams;)V

    goto :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(J)V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0, p1, p2}, Lcom/google/android/a/l;->a(J)V

    .line 310
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->j()V

    .line 311
    iput-wide p1, p0, Lcom/google/android/a/j;->h:J

    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/a/j;->i:Z

    .line 313
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 224
    const-string v0, "mime"

    invoke-virtual {p3, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 225
    iget-boolean v1, p0, Lcom/google/android/a/j;->e:Z

    if-eqz v1, :cond_0

    .line 227
    const-string v1, "mime"

    const-string v2, "audio/raw"

    invoke-virtual {p3, v1, v2}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 229
    const-string v1, "mime"

    invoke-virtual {p3, v1, v0}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iput-object p3, p0, Lcom/google/android/a/j;->f:Landroid/media/MediaFormat;

    .line 235
    :goto_0
    return-void

    .line 232
    :cond_0
    invoke-virtual {p1, p3, v3, p4, v4}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 233
    iput-object v3, p0, Lcom/google/android/a/j;->f:Landroid/media/MediaFormat;

    goto :goto_0
.end method

.method protected a(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/a/j;->f:Landroid/media/MediaFormat;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 245
    :goto_0
    iget-object v1, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/google/android/a/j;->f:Landroid/media/MediaFormat;

    :cond_0
    invoke-virtual {v1, p1, v0}, Lcom/google/android/a/a/b;->a(Landroid/media/MediaFormat;Z)V

    .line 246
    return-void

    .line 244
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 6

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/a/j;->e:Z

    if-eqz v0, :cond_0

    iget v0, p7, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 322
    const/4 v0, 0x1

    .line 384
    :goto_0
    return v0

    .line 325
    :cond_0
    if-eqz p9, :cond_1

    .line 326
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 327
    iget-object v0, p0, Lcom/google/android/a/j;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->f:I

    .line 328
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->f()V

    .line 329
    const/4 v0, 0x1

    goto :goto_0

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 335
    :try_start_0
    iget v0, p0, Lcom/google/android/a/j;->g:I

    if-eqz v0, :cond_4

    .line 336
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    iget v1, p0, Lcom/google/android/a/j;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/a/a/b;->a(I)I

    .line 341
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/a/j;->j:Z
    :try_end_0
    .catch Lcom/google/android/a/a/b$d; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    invoke-virtual {p0}, Lcom/google/android/a/j;->u()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 347
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->e()V

    .line 363
    :cond_2
    :goto_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    iget v2, p7, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v3, p7, Landroid/media/MediaCodec$BufferInfo;->size:I

    iget-wide v4, p7, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object v1, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/a/a/b;->a(Ljava/nio/ByteBuffer;IIJ)I

    move-result v0

    .line 365
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/a/j;->k:J
    :try_end_1
    .catch Lcom/google/android/a/a/b$f; {:try_start_1 .. :try_end_1} :catch_1

    .line 372
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_3

    .line 373
    invoke-virtual {p0}, Lcom/google/android/a/j;->i()V

    .line 374
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/a/j;->i:Z

    .line 378
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 379
    const/4 v0, 0x0

    invoke-virtual {p5, p8, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 380
    iget-object v0, p0, Lcom/google/android/a/j;->a:Lcom/google/android/a/b;

    iget v1, v0, Lcom/google/android/a/b;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/a/b;->e:I

    .line 381
    const/4 v0, 0x1

    goto :goto_0

    .line 338
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/a/j;->g:I

    .line 339
    iget v0, p0, Lcom/google/android/a/j;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/a/j;->a(I)V
    :try_end_2
    .catch Lcom/google/android/a/a/b$d; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 342
    :catch_0
    move-exception v0

    .line 343
    invoke-direct {p0, v0}, Lcom/google/android/a/j;->a(Lcom/google/android/a/a/b$d;)V

    .line 344
    new-instance v1, Lcom/google/android/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 351
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/a/j;->j:Z

    .line 352
    iget-object v1, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v1}, Lcom/google/android/a/a/b;->h()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/a/j;->j:Z

    .line 353
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/a/j;->j:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/a/j;->u()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 354
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/a/j;->k:J

    sub-long v4, v0, v2

    .line 355
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->d()J

    move-result-wide v0

    .line 356
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_6

    const-wide/16 v2, -0x1

    .line 357
    :goto_3
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->c()I

    move-result v1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/a/j;->a(IJJ)V

    goto :goto_2

    .line 356
    :cond_6
    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    goto :goto_3

    .line 366
    :catch_1
    move-exception v0

    .line 367
    invoke-direct {p0, v0}, Lcom/google/android/a/j;->a(Lcom/google/android/a/a/b$f;)V

    .line 368
    new-instance v1, Lcom/google/android/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/a/e;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 384
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected a(Lcom/google/android/a/k;Lcom/google/android/a/o;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 189
    iget-object v1, p2, Lcom/google/android/a/o;->b:Ljava/lang/String;

    .line 190
    invoke-static {v1}, Lcom/google/android/a/f/f;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "audio/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/a/j;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/a/k;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    invoke-interface {p1, v1, v0}, Lcom/google/android/a/k;->a(Ljava/lang/String;Z)Lcom/google/android/a/d;

    move-result-object v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/a/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected b()Lcom/google/android/a/i;
    .locals 0

    .prologue
    .line 239
    return-object p0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/google/android/a/l;->c()V

    .line 267
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->e()V

    .line 268
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->i()V

    .line 273
    invoke-super {p0}, Lcom/google/android/a/l;->d()V

    .line 274
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/android/a/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/a/l;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/a/j;->g:I

    .line 301
    :try_start_0
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-super {p0}, Lcom/google/android/a/l;->g()V

    .line 305
    return-void

    .line 303
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/a/l;->g()V

    throw v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/a/j;->d:Lcom/google/android/a/a/b;

    invoke-virtual {v0}, Lcom/google/android/a/a/b;->g()V

    .line 390
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 394
    return-void
.end method
