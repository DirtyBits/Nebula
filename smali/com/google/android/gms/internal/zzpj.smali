.class public interface abstract Lcom/google/android/gms/internal/zzpj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getAvailableAssetNames()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCustomTemplateId()Ljava/lang/String;
.end method

.method public abstract getVideoController()Lcom/google/android/gms/internal/zzks;
.end method

.method public abstract performClick(Ljava/lang/String;)V
.end method

.method public abstract recordImpression()V
.end method

.method public abstract zzP(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract zzQ(Ljava/lang/String;)Lcom/google/android/gms/internal/zzos;
.end method

.method public abstract zzei()Lcom/google/android/gms/dynamic/IObjectWrapper;
.end method

.method public abstract zzen()Lcom/google/android/gms/dynamic/IObjectWrapper;
.end method

.method public abstract zzj(Lcom/google/android/gms/dynamic/IObjectWrapper;)Z
.end method
