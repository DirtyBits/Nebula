.class public interface abstract Lcom/google/android/gms/internal/zzkn;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract initialize()V
.end method

.method public abstract setAppMuted(Z)V
.end method

.method public abstract setAppVolume(F)V
.end method

.method public abstract zzb(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;)V
.end method

.method public abstract zzc(Ljava/lang/String;Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method

.method public abstract zzu(Ljava/lang/String;)V
.end method
