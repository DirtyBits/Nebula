.class public interface abstract Lcom/google/android/gms/internal/zzjz;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getAdUnitId()Ljava/lang/String;
.end method

.method public abstract getMediationAdapterClassName()Ljava/lang/String;
.end method

.method public abstract getVideoController()Lcom/google/android/gms/internal/zzks;
.end method

.method public abstract isLoading()Z
.end method

.method public abstract isReady()Z
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method

.method public abstract setImmersiveMode(Z)V
.end method

.method public abstract setManualImpressionsEnabled(Z)V
.end method

.method public abstract setUserId(Ljava/lang/String;)V
.end method

.method public abstract showInterstitial()V
.end method

.method public abstract stopLoading()V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzadd;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zziv;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzjl;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzjo;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzke;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzkk;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzky;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzlx;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zznh;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzxg;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzxo;Ljava/lang/String;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzir;)Z
.end method

.method public abstract zzaI()Ljava/lang/String;
.end method

.method public abstract zzal()Lcom/google/android/gms/dynamic/IObjectWrapper;
.end method

.method public abstract zzam()Lcom/google/android/gms/internal/zziv;
.end method

.method public abstract zzao()V
.end method

.method public abstract zzax()Lcom/google/android/gms/internal/zzke;
.end method

.method public abstract zzay()Lcom/google/android/gms/internal/zzjo;
.end method
