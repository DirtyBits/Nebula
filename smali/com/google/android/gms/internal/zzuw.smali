.class public interface abstract Lcom/google/android/gms/internal/zzuw;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract onAdClicked()V
.end method

.method public abstract onAdClosed()V
.end method

.method public abstract onAdFailedToLoad(I)V
.end method

.method public abstract onAdImpression()V
.end method

.method public abstract onAdLeftApplication()V
.end method

.method public abstract onAdLoaded()V
.end method

.method public abstract onAdOpened()V
.end method

.method public abstract onAppEvent(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzuz;)V
.end method

.method public abstract zzb(Lcom/google/android/gms/internal/zzpj;Ljava/lang/String;)V
.end method
