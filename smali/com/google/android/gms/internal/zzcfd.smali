.class public interface abstract Lcom/google/android/gms/internal/zzcfd;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract zza(Lcom/google/android/gms/internal/zzceh;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/zzceh;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzcji;",
            ">;"
        }
    .end annotation
.end method

.method public abstract zza(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzceh;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/internal/zzceh;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzcek;",
            ">;"
        }
    .end annotation
.end method

.method public abstract zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzcji;",
            ">;"
        }
    .end annotation
.end method

.method public abstract zza(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/internal/zzceh;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/gms/internal/zzceh;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzcji;",
            ">;"
        }
    .end annotation
.end method

.method public abstract zza(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzceh;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzcek;Lcom/google/android/gms/internal/zzceh;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzcez;Lcom/google/android/gms/internal/zzceh;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzcez;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzcji;Lcom/google/android/gms/internal/zzceh;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzcez;Ljava/lang/String;)[B
.end method

.method public abstract zzb(Lcom/google/android/gms/internal/zzceh;)V
.end method

.method public abstract zzb(Lcom/google/android/gms/internal/zzcek;)V
.end method

.method public abstract zzc(Lcom/google/android/gms/internal/zzceh;)Ljava/lang/String;
.end method

.method public abstract zzk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/zzcek;",
            ">;"
        }
    .end annotation
.end method
