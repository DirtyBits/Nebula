.class public interface abstract Lcom/google/android/gms/internal/zzks;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract getAspectRatio()F
.end method

.method public abstract getPlaybackState()I
.end method

.method public abstract isCustomControlsEnabled()Z
.end method

.method public abstract isMuted()Z
.end method

.method public abstract mute(Z)V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzkv;)V
.end method

.method public abstract zzdv()F
.end method

.method public abstract zzdw()F
.end method
