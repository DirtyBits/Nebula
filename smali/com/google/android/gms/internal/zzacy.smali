.class public interface abstract Lcom/google/android/gms/internal/zzacy;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract destroy()V
.end method

.method public abstract getMediationAdapterClassName()Ljava/lang/String;
.end method

.method public abstract isLoaded()Z
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method

.method public abstract setImmersiveMode(Z)V
.end method

.method public abstract setUserId(Ljava/lang/String;)V
.end method

.method public abstract show()V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzadd;)V
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzadj;)V
.end method

.method public abstract zzf(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method

.method public abstract zzg(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method

.method public abstract zzh(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
.end method
