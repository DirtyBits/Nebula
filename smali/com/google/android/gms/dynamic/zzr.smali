.class public final Lcom/google/android/gms/dynamic/zzr;
.super Lcom/google/android/gms/dynamic/zzl;


# instance fields
.field private zzaSE:Landroid/support/v4/a/o;


# direct methods
.method private constructor <init>(Landroid/support/v4/a/o;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/zzl;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    return-void
.end method

.method public static zza(Landroid/support/v4/a/o;)Lcom/google/android/gms/dynamic/zzr;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Lcom/google/android/gms/dynamic/zzr;

    invoke-direct {v0, p0}, Lcom/google/android/gms/dynamic/zzr;-><init>(Landroid/support/v4/a/o;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getArguments()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final getId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getId()I

    move-result v0

    return v0
.end method

.method public final getRetainInstance()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getRetainInstance()Z

    move-result v0

    return v0
.end method

.method public final getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTargetRequestCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getTargetRequestCode()I

    move-result v0

    return v0
.end method

.method public final getUserVisibleHint()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getUserVisibleHint()Z

    move-result v0

    return v0
.end method

.method public final getView()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzw(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final isAdded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isAdded()Z

    move-result v0

    return v0
.end method

.method public final isDetached()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isDetached()Z

    move-result v0

    return v0
.end method

.method public final isHidden()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isHidden()Z

    move-result v0

    return v0
.end method

.method public final isInLayout()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isInLayout()Z

    move-result v0

    return v0
.end method

.method public final isRemoving()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isRemoving()Z

    move-result v0

    return v0
.end method

.method public final isResumed()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isResumed()Z

    move-result v0

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final setHasOptionsMenu(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public final setMenuVisibility(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->setMenuVisibility(Z)V

    return-void
.end method

.method public final setRetainInstance(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->setRetainInstance(Z)V

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->setUserVisibleHint(Z)V

    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/o;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/a/o;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final zzC(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/gms/dynamic/zzn;->zzE(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v1, v0}, Landroid/support/v4/a/o;->registerForContextMenu(Landroid/view/View;)V

    return-void
.end method

.method public final zzD(Lcom/google/android/gms/dynamic/IObjectWrapper;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/gms/dynamic/zzn;->zzE(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v1, v0}, Landroid/support/v4/a/o;->unregisterForContextMenu(Landroid/view/View;)V

    return-void
.end method

.method public final zztA()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzw(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final zztB()Lcom/google/android/gms/dynamic/zzk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getTargetFragment()Landroid/support/v4/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzr;->zza(Landroid/support/v4/a/o;)Lcom/google/android/gms/dynamic/zzr;

    move-result-object v0

    return-object v0
.end method

.method public final zzty()Lcom/google/android/gms/dynamic/IObjectWrapper;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getActivity()Landroid/support/v4/a/p;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzn;->zzw(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;

    move-result-object v0

    return-object v0
.end method

.method public final zztz()Lcom/google/android/gms/dynamic/zzk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzr;->zzaSE:Landroid/support/v4/a/o;

    invoke-virtual {v0}, Landroid/support/v4/a/o;->getParentFragment()Landroid/support/v4/a/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/zzr;->zza(Landroid/support/v4/a/o;)Lcom/google/android/gms/dynamic/zzr;

    move-result-object v0

    return-object v0
.end method
