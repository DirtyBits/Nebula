.class public Lcom/millennialmedia/InterstitialAd;
.super Lcom/millennialmedia/internal/AdPlacement;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/InterstitialAd$ExpirationRunnable;,
        Lcom/millennialmedia/InterstitialAd$DisplayOptions;,
        Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;,
        Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;,
        Lcom/millennialmedia/InterstitialAd$InterstitialListener;
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

.field private h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private volatile k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

.field private volatile l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/millennialmedia/InterstitialAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/AdPlacement;-><init>(Ljava/lang/String;)V

    .line 273
    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/PlayList;)Lcom/millennialmedia/internal/PlayList;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->b:Lcom/millennialmedia/internal/PlayList;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;)V
    .locals 2

    .prologue
    .line 896
    monitor-enter p0

    .line 897
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 898
    monitor-exit p0

    .line 920
    :cond_0
    :goto_0
    return-void

    .line 902
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "showing"

    if-ne v0, v1, :cond_2

    .line 903
    const-string v0, "show_failed"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 905
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 907
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad show failed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 911
    if-eqz v0, :cond_0

    .line 912
    new-instance v1, Lcom/millennialmedia/InterstitialAd$8;

    invoke-direct {v1, p0, v0, p1}, Lcom/millennialmedia/InterstitialAd$8;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 905
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->j(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;)V

    return-void
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 8

    .prologue
    .line 387
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->copy()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v1

    .line 389
    monitor-enter p0

    .line 391
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    monitor-exit p0

    .line 570
    :goto_0
    return-void

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v2, "play_list_loaded"

    .line 396
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v2, "ad_adapter_load_failed"

    .line 397
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 399
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 402
    :cond_2
    :try_start_1
    const-string v0, "loading_ad_adapter"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 409
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getItemHash()I

    .line 410
    iput-object v1, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 411
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 413
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/PlayList;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 414
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v2, "Unable to find ad adapter in play list"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_3
    invoke-direct {p0, v1}, Lcom/millennialmedia/InterstitialAd;->e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0

    .line 423
    :cond_4
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->getPlayListItemReporter(Lcom/millennialmedia/internal/AdPlacementReporter;)Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    move-result-object v2

    .line 427
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0, p0, v2}, Lcom/millennialmedia/internal/PlayList;->getNextAdAdapter(Lcom/millennialmedia/internal/AdPlacement;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)Lcom/millennialmedia/internal/adadapters/AdAdapter;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    .line 429
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 432
    iget-object v3, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    .line 434
    iget-object v3, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    iget v3, v3, Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;->requestTimeout:I

    .line 435
    if-lez v3, :cond_6

    .line 436
    iget-object v4, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v4, :cond_5

    .line 437
    iget-object v4, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v4}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 442
    :cond_5
    new-instance v4, Lcom/millennialmedia/InterstitialAd$3;

    invoke-direct {v4, p0, v1, v2}, Lcom/millennialmedia/InterstitialAd$3;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    int-to-long v6, v3

    invoke-static {v4, v6, v7}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v3

    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 460
    :cond_6
    iget-object v3, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    new-instance v4, Lcom/millennialmedia/InterstitialAd$4;

    invoke-direct {v4, p0, v1, v2}, Lcom/millennialmedia/InterstitialAd$4;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    invoke-virtual {v3, v0, v4}, Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;->init(Landroid/content/Context;Lcom/millennialmedia/internal/adadapters/InterstitialAdapter$InterstitialAdapterListener;)V

    goto/16 :goto_0

    .line 567
    :cond_7
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    .line 568
    invoke-direct {p0, v1}, Lcom/millennialmedia/InterstitialAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;)V
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 688
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    .line 689
    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InterstitialAd;)Z
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/millennialmedia/InterstitialAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic b(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;)Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    return-object p1
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 694
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 696
    iput-object v1, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 699
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 701
    iput-object v1, p0, Lcom/millennialmedia/InterstitialAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 703
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 4

    .prologue
    .line 708
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->e()V

    .line 710
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getInterstitialExpirationDuration()I

    move-result v0

    .line 711
    if-lez v0, :cond_0

    .line 712
    new-instance v1, Lcom/millennialmedia/InterstitialAd$ExpirationRunnable;

    invoke-direct {v1, p0, p1}, Lcom/millennialmedia/InterstitialAd$ExpirationRunnable;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    int-to-long v2, v0

    .line 713
    invoke-static {v1, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 716
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/millennialmedia/InterstitialAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic c(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object p1
.end method

.method private c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 730
    monitor-enter p0

    .line 731
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 732
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onAdAdapterLoadFailed called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_0
    monitor-exit p0

    .line 756
    :goto_0
    return-void

    .line 739
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 740
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 741
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAdAdapterLoadFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 753
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 748
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 749
    monitor-exit p0

    goto :goto_0

    .line 752
    :cond_4
    const-string v0, "ad_adapter_load_failed"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 753
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 755
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0
.end method

.method public static createInstance(Ljava/lang/String;)Lcom/millennialmedia/InterstitialAd;
    .locals 2

    .prologue
    .line 262
    invoke-static {}, Lcom/millennialmedia/MMSDK;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/millennialmedia/MMInitializationException;

    const-string v1, "Unable to create instance, SDK must be initialized first"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMInitializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_0
    new-instance v0, Lcom/millennialmedia/InterstitialAd;

    invoke-direct {v0, p0}, Lcom/millennialmedia/InterstitialAd;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic d(Lcom/millennialmedia/InterstitialAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 761
    monitor-enter p0

    .line 763
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 764
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 765
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onLoadSucceeded called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :cond_0
    monitor-exit p0

    .line 807
    :cond_1
    :goto_0
    return-void

    .line 771
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 772
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 773
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadSucceeded called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    :cond_3
    monitor-exit p0

    goto :goto_0

    .line 785
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 780
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 781
    monitor-exit p0

    goto :goto_0

    .line 784
    :cond_5
    const-string v0, "loaded"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 785
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 787
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Load succeeded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->b()V

    .line 789
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 794
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 797
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 798
    if-eqz v0, :cond_1

    .line 799
    new-instance v1, Lcom/millennialmedia/InterstitialAd$5;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$5;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/millennialmedia/InterstitialAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 722
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 723
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 725
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 812
    monitor-enter p0

    .line 813
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814
    monitor-exit p0

    .line 854
    :cond_0
    :goto_0
    return-void

    .line 817
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 818
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 819
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onLoadFailed called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 837
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 825
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loading_play_list"

    .line 826
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 827
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 828
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 834
    :cond_5
    const-string v0, "load_failed"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 835
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->b()V

    .line 836
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 837
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 839
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load failed for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". If this warning persists please check your placement configuration."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 844
    if-eqz v0, :cond_0

    .line 845
    new-instance v1, Lcom/millennialmedia/InterstitialAd$6;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$6;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method static synthetic f(Lcom/millennialmedia/InterstitialAd;)Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    return-object v0
.end method

.method static synthetic f(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 859
    monitor-enter p0

    .line 860
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 861
    monitor-exit p0

    .line 891
    :cond_0
    :goto_0
    return-void

    .line 864
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 865
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 866
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onShown called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 876
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 872
    :cond_3
    :try_start_1
    const-string v0, "shown"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 874
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/AdPlacementReporter;->setDisplayed(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    .line 876
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 878
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad shown"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 882
    if-eqz v0, :cond_0

    .line 883
    new-instance v1, Lcom/millennialmedia/InterstitialAd$7;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$7;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/millennialmedia/InterstitialAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic g(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private g(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 925
    monitor-enter p0

    .line 926
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 927
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onClosed called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    :cond_0
    monitor-exit p0

    .line 953
    :goto_0
    return-void

    .line 934
    :cond_1
    const-string v0, "idle"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 935
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 937
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad closed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 941
    if-eqz v0, :cond_2

    .line 942
    new-instance v1, Lcom/millennialmedia/InterstitialAd$9;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$9;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    .line 952
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;)V

    goto :goto_0

    .line 935
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "idle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "load_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    .line 1046
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "destroyed"

    .line 1047
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "show_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "shown"

    .line 1048
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1050
    :cond_0
    const/4 v0, 0x0

    .line 1053
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic h(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->g(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private h(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 958
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad clicked"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->setClicked(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 962
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 963
    if-eqz v0, :cond_0

    .line 964
    new-instance v1, Lcom/millennialmedia/InterstitialAd$10;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$10;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    .line 972
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->h(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private i(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 977
    monitor-enter p0

    .line 978
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 979
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 980
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onAdLeftApplication called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    :cond_0
    monitor-exit p0

    .line 1000
    :cond_1
    :goto_0
    return-void

    .line 985
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 987
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad left application"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 991
    if-eqz v0, :cond_1

    .line 992
    new-instance v1, Lcom/millennialmedia/InterstitialAd$11;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$11;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 985
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic j(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/millennialmedia/InterstitialAd;->i(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private j(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 1005
    monitor-enter p0

    .line 1006
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1007
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1008
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "onExpired called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    :cond_0
    monitor-exit p0

    .line 1039
    :cond_1
    :goto_0
    return-void

    .line 1015
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "show_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1016
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1017
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onExpired called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    :cond_3
    monitor-exit p0

    goto :goto_0

    .line 1024
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1023
    :cond_4
    :try_start_1
    const-string v0, "expired"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 1024
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1026
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    const-string v1, "Ad expired"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 1030
    if-eqz v0, :cond_1

    .line 1031
    new-instance v1, Lcom/millennialmedia/InterstitialAd$12;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InterstitialAd$12;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method protected c()Z
    .locals 2

    .prologue
    .line 1060
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "showing"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1066
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1067
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroying ad "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    :cond_0
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->f:Ljava/lang/ref/WeakReference;

    .line 1071
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    .line 1072
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    .line 1074
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->b()V

    .line 1076
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 1077
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1078
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1081
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eqz v0, :cond_3

    .line 1082
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_2

    .line 1083
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 1084
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 1086
    :cond_2
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    .line 1088
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eqz v0, :cond_5

    .line 1089
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_4

    .line 1090
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 1091
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 1093
    :cond_4
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->k:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    .line 1096
    :cond_5
    iput-object v3, p0, Lcom/millennialmedia/InterstitialAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 1097
    return-void
.end method

.method public getCreativeInfo()Lcom/millennialmedia/CreativeInfo;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;->getCreativeInfo()Lcom/millennialmedia/CreativeInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExpired()Z
    .locals 2

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    const/4 v0, 0x0

    .line 664
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 2

    .prologue
    .line 645
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    const/4 v0, 0x0

    .line 649
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public load(Landroid/content/Context;Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;)V
    .locals 6

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 292
    :cond_0
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading playlist for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->f:Ljava/lang/ref/WeakReference;

    .line 297
    monitor-enter p0

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "idle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "load_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    .line 299
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v1, "show_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 301
    sget-object v0, Lcom/millennialmedia/InterstitialAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load interstitial ad, state is invalid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    monitor-exit p0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 306
    :cond_1
    :try_start_1
    const-string v0, "loading_play_list"

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    .line 307
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/InterstitialAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 313
    if-nez p2, :cond_2

    .line 314
    new-instance p2, Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;

    invoke-direct {p2}, Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;-><init>()V

    .line 317
    :cond_2
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->getRequestState()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v0

    .line 320
    iget-object v1, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v1, :cond_3

    .line 321
    iget-object v1, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 324
    :cond_3
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getInterstitialTimeout()I

    move-result v1

    .line 328
    new-instance v2, Lcom/millennialmedia/InterstitialAd$1;

    invoke-direct {v2, p0, v0}, Lcom/millennialmedia/InterstitialAd$1;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    int-to-long v4, v1

    invoke-static {v2, v4, v5}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/InterstitialAd;->h:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 340
    invoke-virtual {p2}, Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;->getImpressionGroup()Ljava/lang/String;

    move-result-object v2

    .line 341
    invoke-virtual {p2, p0}, Lcom/millennialmedia/InterstitialAd$InterstitialAdMetadata;->toMap(Lcom/millennialmedia/internal/AdPlacement;)Ljava/util/Map;

    move-result-object v3

    new-instance v4, Lcom/millennialmedia/InterstitialAd$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/millennialmedia/InterstitialAd$2;-><init>(Lcom/millennialmedia/InterstitialAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Ljava/lang/String;)V

    invoke-static {v3, v4, v1}, Lcom/millennialmedia/internal/playlistserver/PlayListServer;->loadPlayList(Ljava/util/Map;Lcom/millennialmedia/internal/playlistserver/PlayListServer$PlayListLoadListener;I)V

    goto/16 :goto_0
.end method

.method public setListener(Lcom/millennialmedia/InterstitialAd$InterstitialListener;)V
    .locals 1

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    :goto_0
    return-void

    .line 634
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/InterstitialAd;->g:Lcom/millennialmedia/InterstitialAd$InterstitialListener;

    goto :goto_0
.end method

.method public show(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 581
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/millennialmedia/InterstitialAd;->show(Landroid/content/Context;Lcom/millennialmedia/InterstitialAd$DisplayOptions;)V

    .line 582
    return-void
.end method

.method public show(Landroid/content/Context;Lcom/millennialmedia/InterstitialAd$DisplayOptions;)V
    .locals 3

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/millennialmedia/InterstitialAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 620
    :goto_0
    return-void

    .line 598
    :cond_0
    const/4 v0, 0x0

    .line 600
    if-nez p1, :cond_1

    .line 601
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "Unable to show interstitial, specified context cannot be null"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 604
    :cond_1
    monitor-enter p0

    .line 605
    :try_start_0
    iget-object v1, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    const-string v2, "loaded"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 606
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to show interstitial ad, state is not valid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 610
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    if-eqz v0, :cond_3

    .line 613
    new-instance v1, Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;

    const/4 v2, 0x4

    invoke-direct {v1, v2, v0}, Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/millennialmedia/InterstitialAd;->a(Lcom/millennialmedia/InterstitialAd$InterstitialErrorStatus;)V

    goto :goto_0

    .line 608
    :cond_2
    :try_start_1
    const-string v1, "showing"

    iput-object v1, p0, Lcom/millennialmedia/InterstitialAd;->a:Ljava/lang/String;

    goto :goto_1

    .line 610
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 618
    :cond_3
    invoke-direct {p0}, Lcom/millennialmedia/InterstitialAd;->e()V

    .line 619
    iget-object v0, p0, Lcom/millennialmedia/InterstitialAd;->l:Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/millennialmedia/internal/adadapters/InterstitialAdapter;->show(Landroid/content/Context;Lcom/millennialmedia/InterstitialAd$DisplayOptions;)V

    goto :goto_0
.end method
