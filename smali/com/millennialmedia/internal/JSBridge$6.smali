.class Lcom/millennialmedia/internal/JSBridge$6;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/JSBridge;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/JSBridge;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/JSBridge;)V
    .locals 0

    .prologue
    .line 2383
    iput-object p1, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2387
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2388
    if-nez v0, :cond_1

    .line 2404
    :cond_0
    :goto_0
    return-void

    .line 2392
    :cond_1
    iget-object v2, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    const-string v3, "MmJsBridge.mraid.setPlacementType"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    iget-boolean v1, v1, Lcom/millennialmedia/internal/JSBridge;->d:Z

    if-eqz v1, :cond_2

    const-string v1, "interstitial"

    :goto_1
    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2393
    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    const-string v2, "MmJsBridge.mraid.setSupports"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->a()Lorg/json/JSONObject;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2394
    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v1, v0}, Lcom/millennialmedia/internal/JSBridge;->d(Lcom/millennialmedia/internal/MMWebView;)V

    .line 2395
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    const-string v1, "MmJsBridge.mraid.setViewable"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    iget-boolean v3, v3, Lcom/millennialmedia/internal/JSBridge;->j:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2398
    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/JSBridge;->f:Z

    if-eqz v0, :cond_3

    const-string v0, "expanded"

    :goto_2
    invoke-virtual {v1, v0}, Lcom/millennialmedia/internal/JSBridge;->b(Ljava/lang/String;)V

    .line 2401
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->c(Lcom/millennialmedia/internal/JSBridge;)Lcom/millennialmedia/internal/JSBridge$JSBridgeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2402
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$6;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->c(Lcom/millennialmedia/internal/JSBridge;)Lcom/millennialmedia/internal/JSBridge$JSBridgeListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/JSBridge$JSBridgeListener;->onJSBridgeReady()V

    goto :goto_0

    .line 2392
    :cond_2
    const-string v1, "inline"

    goto :goto_1

    .line 2398
    :cond_3
    const-string v0, "default"

    goto :goto_2
.end method
