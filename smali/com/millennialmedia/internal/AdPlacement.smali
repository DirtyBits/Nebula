.class public abstract Lcom/millennialmedia/internal/AdPlacement;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/AdPlacement$RequestState;
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field protected volatile a:Ljava/lang/String;

.field protected volatile b:Lcom/millennialmedia/internal/PlayList;

.field protected volatile c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

.field protected d:Lcom/millennialmedia/XIncentivizedEventListener;

.field private volatile f:Z

.field public placementId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/millennialmedia/internal/AdPlacement;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/internal/AdPlacement;->f:Z

    .line 33
    const-string v0, "idle"

    iput-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->a:Ljava/lang/String;

    .line 120
    if-nez p1, :cond_0

    .line 121
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "PlacementId must be a non null."

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->placementId:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->placementId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "PlacementId cannot be an empty string."

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_1
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 234
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    sget-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroying ad "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    const-string v0, "destroyed"

    iput-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->a:Ljava/lang/String;

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/internal/AdPlacement;->f:Z

    .line 241
    invoke-virtual {p0}, Lcom/millennialmedia/internal/AdPlacement;->d()V

    .line 243
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    sget-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    const-string v1, "Ad destroyed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 3

    .prologue
    .line 160
    if-nez p1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    sget-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incentive earned <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;->eventId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    .line 170
    if-eqz v0, :cond_0

    .line 171
    new-instance v1, Lcom/millennialmedia/internal/AdPlacement$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/millennialmedia/internal/AdPlacement$1;-><init>(Lcom/millennialmedia/internal/AdPlacement;Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;Lcom/millennialmedia/XIncentivizedEventListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected abstract c()Z
.end method

.method protected abstract d()V
.end method

.method public declared-synchronized destroy()V
    .locals 3

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/AdPlacement;->isDestroyed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    :goto_0
    monitor-exit p0

    return-void

    .line 200
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/internal/AdPlacement;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-direct {p0}, Lcom/millennialmedia/internal/AdPlacement;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 203
    :cond_1
    :try_start_2
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    sget-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destroy is pending "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/internal/AdPlacement;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected declared-synchronized f()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 214
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/millennialmedia/internal/AdPlacement;->a:Ljava/lang/String;

    const-string v2, "destroyed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    :goto_0
    monitor-exit p0

    return v0

    .line 218
    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lcom/millennialmedia/internal/AdPlacement;->f:Z

    if-eqz v1, :cond_2

    .line 219
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 220
    sget-object v1, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing pending destroy "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_1
    invoke-direct {p0}, Lcom/millennialmedia/internal/AdPlacement;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 228
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getCreativeInfo()Lcom/millennialmedia/CreativeInfo;
.end method

.method public getRequestState()Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-direct {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;-><init>()V

    iput-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 114
    iget-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method public isDestroyed()Z
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->a:Ljava/lang/String;

    const-string v1, "destroyed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/millennialmedia/internal/AdPlacement;->f:Z

    if-eqz v0, :cond_1

    .line 258
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/AdPlacement;->e:Ljava/lang/String;

    const-string v1, "Placement has been destroyed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const/4 v0, 0x1

    .line 263
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public xGetIncentivizedListener()Lcom/millennialmedia/XIncentivizedEventListener;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/millennialmedia/internal/AdPlacement;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    return-object v0
.end method

.method public xSetIncentivizedListener(Lcom/millennialmedia/XIncentivizedEventListener;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/millennialmedia/internal/AdPlacement;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    .line 143
    return-void
.end method
