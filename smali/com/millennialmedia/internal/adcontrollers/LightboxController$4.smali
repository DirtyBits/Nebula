.class Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/adcontrollers/LightboxController;->a(Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;)Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

.field final synthetic b:Lcom/millennialmedia/internal/adcontrollers/LightboxController;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/adcontrollers/LightboxController;Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->b:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    iput-object p2, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public expand(Lcom/millennialmedia/internal/SizableStateManager$ExpandParams;)Z
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    return v0
.end method

.method public onAdLeftApplication()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;->onAdLeftApplication()V

    .line 369
    return-void
.end method

.method public onClicked()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;->onClicked()V

    .line 362
    return-void
.end method

.method public onFailed()V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;->initFailed()V

    .line 349
    return-void
.end method

.method public onLoaded()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/LightboxController$4;->a:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;->initSucceeded()V

    .line 342
    return-void
.end method

.method public onReady()V
    .locals 0

    .prologue
    .line 355
    return-void
.end method

.method public resize(Lcom/millennialmedia/internal/SizableStateManager$ResizeParams;)Z
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    return v0
.end method

.method public setOrientation(I)V
    .locals 0

    .prologue
    .line 401
    return-void
.end method

.method public showCloseIndicator(Z)V
    .locals 0

    .prologue
    .line 395
    return-void
.end method
