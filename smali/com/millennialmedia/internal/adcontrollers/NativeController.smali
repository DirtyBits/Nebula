.class public Lcom/millennialmedia/internal/adcontrollers/NativeController;
.super Lcom/millennialmedia/internal/adcontrollers/AdController;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;,
        Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;,
        Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public assets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;

.field public impTrackers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public jsTracker:Ljava/lang/String;

.field public link:Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;

.field public version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/millennialmedia/internal/adcontrollers/NativeController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/millennialmedia/internal/adcontrollers/AdController;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->version:I

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/millennialmedia/internal/adcontrollers/AdController;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->version:I

    .line 116
    iput-object p1, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->b:Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;

    .line 117
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;
    .locals 3

    .prologue
    .line 306
    new-instance v0, Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;

    invoke-direct {v0}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;-><init>()V

    .line 307
    const-string v1, "url"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;->url:Ljava/lang/String;

    .line 309
    const-string v1, "clicktrackers"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 311
    :try_start_0
    const-string v1, "clicktrackers"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 314
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/JSONUtils;->convertToStringArray(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>([Ljava/lang/Object;)V

    iput-object v2, v0, Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;->clickTrackerUrls:Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :cond_0
    :goto_0
    const-string v1, "fallback"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;->fallback:Ljava/lang/String;

    .line 323
    return-object v0

    .line 316
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private a(Lorg/json/JSONArray;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 203
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 205
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 207
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 210
    const-string v2, "id"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 213
    const-string v2, "required"

    invoke-virtual {v6, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_6

    .line 214
    const/4 v2, 0x1

    .line 219
    :goto_1
    const-string v3, "title"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    :try_start_0
    const-string v3, "title"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 224
    new-instance v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;

    sget-object v9, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;->TITLE:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;

    invoke-direct {v3, v9, v7, v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;-><init>(Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;IZ)V

    .line 225
    new-instance v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Title;

    invoke-direct {v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Title;-><init>()V

    iput-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->title:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Title;

    .line 226
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->title:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Title;

    const-string v7, "text"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Title;->value:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 286
    :goto_2
    if-eqz v2, :cond_0

    .line 289
    :try_start_1
    const-string v3, "link"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a(Lorg/json/JSONObject;)Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;

    move-result-object v3

    iput-object v3, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->link:Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4

    .line 295
    :goto_3
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 228
    :catch_0
    move-exception v2

    move-object v2, v4

    .line 230
    goto :goto_2

    .line 232
    :cond_1
    const-string v3, "img"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 235
    :try_start_2
    const-string v3, "img"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 237
    new-instance v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;

    sget-object v9, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;->IMAGE:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;

    invoke-direct {v3, v9, v7, v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;-><init>(Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;IZ)V

    .line 238
    new-instance v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;

    invoke-direct {v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;-><init>()V

    iput-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->image:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;

    .line 239
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->image:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;

    const-string v7, "url"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;->url:Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 242
    :try_start_3
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->image:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;

    const-string v7, "w"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;->width:Ljava/lang/Integer;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_6

    .line 248
    :goto_4
    :try_start_4
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->image:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;

    const-string v7, "h"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Image;->height:Ljava/lang/Integer;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    move-object v2, v3

    .line 255
    goto :goto_2

    .line 253
    :catch_1
    move-exception v2

    move-object v2, v4

    .line 255
    goto :goto_2

    .line 257
    :cond_2
    const-string v3, "video"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 260
    :try_start_5
    const-string v3, "video"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 262
    new-instance v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;

    sget-object v9, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;->VIDEO:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;

    invoke-direct {v3, v9, v7, v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;-><init>(Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;IZ)V

    .line 263
    new-instance v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Video;

    invoke-direct {v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Video;-><init>()V

    iput-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->video:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Video;

    .line 264
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->video:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Video;

    const-string v7, "vasttag"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Video;->vastTag:Ljava/lang/String;
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_2

    move-object v2, v3

    .line 268
    goto/16 :goto_2

    .line 266
    :catch_2
    move-exception v2

    move-object v2, v4

    .line 268
    goto/16 :goto_2

    .line 270
    :cond_3
    const-string v3, "data"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 273
    :try_start_6
    const-string v3, "data"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 275
    new-instance v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;

    sget-object v9, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;->DATA:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;

    invoke-direct {v3, v9, v7, v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;-><init>(Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Type;IZ)V

    .line 276
    new-instance v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;

    invoke-direct {v2}, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;-><init>()V

    iput-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->data:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;

    .line 277
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->data:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;

    const-string v7, "value"

    invoke-virtual {v8, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;->value:Ljava/lang/String;

    .line 278
    iget-object v2, v3, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset;->data:Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;

    const-string v7, "label"

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/millennialmedia/internal/adcontrollers/NativeController$Asset$Data;->label:Ljava/lang/String;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_3

    move-object v2, v3

    .line 282
    goto/16 :goto_2

    .line 280
    :catch_3
    move-exception v2

    move-object v2, v4

    .line 281
    goto/16 :goto_2

    .line 300
    :cond_4
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0, v5}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->assets:Ljava/util/List;

    .line 301
    return-void

    .line 291
    :catch_4
    move-exception v3

    goto/16 :goto_3

    .line 249
    :catch_5
    move-exception v2

    goto :goto_5

    .line 243
    :catch_6
    move-exception v2

    goto/16 :goto_4

    :cond_5
    move-object v2, v4

    goto/16 :goto_2

    :cond_6
    move v2, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public canHandleContent(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 192
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "native"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method public init(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 122
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading native assets "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 128
    const-string v1, "native"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 131
    const-string v1, "ver"

    iget v2, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->version:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->version:I

    .line 134
    const-string v1, "assets"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 135
    invoke-direct {p0, v1}, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a(Lorg/json/JSONArray;)V

    .line 138
    const-string v1, "link"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a(Lorg/json/JSONObject;)Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;

    move-result-object v1

    iput-object v1, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->link:Lcom/millennialmedia/internal/adcontrollers/NativeController$Link;

    .line 141
    const-string v1, "imptrackers"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 142
    if-eqz v1, :cond_1

    .line 144
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/JSONUtils;->convertToStringArray(Lorg/json/JSONArray;)[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>([Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->impTrackers:Ljava/util/List;

    .line 148
    :cond_1
    const-string v1, "jstracker"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->jsTracker:Ljava/lang/String;

    .line 150
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->b:Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;->initSucceeded()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    sget-object v1, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a:Ljava/lang/String;

    const-string v2, "Initialization of the native controller instance failed"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 154
    iget-object v1, p0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->b:Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;

    invoke-interface {v1, v0}, Lcom/millennialmedia/internal/adcontrollers/NativeController$NativeControllerListener;->initFailed(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 162
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/millennialmedia/internal/adcontrollers/NativeController;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Releasing native assets "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    new-instance v0, Lcom/millennialmedia/internal/adcontrollers/NativeController$1;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/adcontrollers/NativeController$1;-><init>(Lcom/millennialmedia/internal/adcontrollers/NativeController;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 179
    return-void
.end method
