.class Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->close()V

    .line 269
    return-void
.end method

.method public onClicked()V
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->c(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;)Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;->onClick()V

    .line 255
    return-void
.end method

.method public onFailed()V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->b(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;)V

    .line 247
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->c(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;)Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;->initFailed()V

    .line 248
    return-void
.end method

.method public onIncentiveEarned(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->c(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;)Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;->onIncentiveEarned(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V

    .line 262
    return-void
.end method

.method public onLoaded()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3$1;->a:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;

    iget-object v0, v0, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$3;->b:Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;

    invoke-static {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;->c(Lcom/millennialmedia/internal/adcontrollers/VASTVideoController;)Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/millennialmedia/internal/adcontrollers/VASTVideoController$VASTVideoControllerListener;->initSucceeded()V

    .line 240
    return-void
.end method
