.class public Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/utils/ViewUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewabilityWatcher"
.end annotation


# instance fields
.field a:I

.field b:Landroid/graphics/Rect;

.field volatile c:Z

.field volatile d:Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;

.field volatile e:Z

.field volatile f:Z

.field volatile g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field volatile h:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;

.field volatile i:Lcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;

.field public volatile viewable:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a:I

    .line 52
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b:Landroid/graphics/Rect;

    .line 54
    iput-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    .line 56
    iput-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->e:Z

    .line 57
    iput-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->f:Z

    .line 62
    iput-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->viewable:Z

    .line 67
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating viewability watcher <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> for view <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->g:Ljava/lang/ref/WeakReference;

    .line 72
    iput-object p2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->h:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;

    .line 76
    new-instance v0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$1;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$1;-><init>(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->i:Lcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;

    .line 93
    return-void
.end method

.method private a()V
    .locals 0

    .prologue
    .line 313
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->postOnUiThread(Ljava/lang/Runnable;)V

    .line 314
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->f:Z

    if-eqz v0, :cond_1

    .line 215
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Trying to set view tree observer when already set"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 227
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding ViewTreeObserver.\n\tViewability watcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tViewTreeObserver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->f:Z

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 269
    invoke-static {p1}, Lcom/millennialmedia/internal/utils/ViewUtils;->getActivityForView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 270
    if-nez v0, :cond_0

    .line 283
    :goto_0
    return-void

    .line 274
    :cond_0
    if-eqz p2, :cond_2

    iget-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->e:Z

    if-nez v1, :cond_2

    .line 275
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget-object v2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->i:Lcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;

    invoke-static {v1, v2}, Lcom/millennialmedia/internal/ActivityListenerManager;->registerListener(ILcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;)V

    .line 276
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Lcom/millennialmedia/internal/ActivityListenerManager;->getLifecycleState(I)Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->d:Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;

    .line 282
    :cond_1
    :goto_1
    iput-boolean p2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->e:Z

    goto :goto_0

    .line 278
    :cond_2
    if-nez p2, :cond_1

    iget-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->e:Z

    if-eqz v1, :cond_1

    .line 279
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->i:Lcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/ActivityListenerManager;->unregisterListener(ILcom/millennialmedia/internal/ActivityListenerManager$ActivityListener;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a()V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a(Landroid/view/View;Z)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->f:Z

    if-nez v0, :cond_1

    .line 242
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Trying to remove view tree observer when not set"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 253
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 254
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removing ViewTreeObserver.\n\tViewability watcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tViewTreeObserver: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 263
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->f:Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    if-eqz v0, :cond_0

    .line 302
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a()V

    .line 304
    :cond_0
    return-void
.end method

.method public onPreDraw()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    if-eqz v0, :cond_0

    .line 290
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a()V

    .line 293
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 181
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewAttachedToWindow called.\n\tViewability watcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    if-eqz v0, :cond_1

    .line 188
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a(Landroid/view/View;)V

    .line 189
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a(Landroid/view/View;Z)V

    .line 190
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a()V

    .line 192
    :cond_1
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 198
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow called.\n\tViewability watcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    iget-boolean v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    if-eqz v0, :cond_1

    .line 205
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b(Landroid/view/View;)V

    .line 206
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a(Landroid/view/View;Z)V

    .line 207
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a()V

    .line 209
    :cond_1
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 325
    .line 329
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 330
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->d:Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;

    sget-object v4, Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;->RESUMED:Lcom/millennialmedia/internal/ActivityListenerManager$LifecycleState;

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 332
    iget v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a:I

    if-nez v3, :cond_3

    move v2, v1

    .line 354
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->viewable:Z

    if-eq v1, v2, :cond_2

    .line 355
    iput-boolean v2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->viewable:Z

    .line 357
    iget-boolean v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->c:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->h:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;

    if-eqz v1, :cond_2

    .line 358
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 359
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Notifying listener of viewability change.\n\tViewability watcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\tViewable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->viewable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_1
    iget-object v1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->h:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;

    iget-boolean v2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->viewable:Z

    invoke-interface {v1, v0, v2}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;->onViewableChanged(Landroid/view/View;Z)V

    .line 368
    :cond_2
    return-void

    .line 335
    :cond_3
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 336
    iget-object v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-long v4, v3

    .line 337
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v6

    mul-int/2addr v3, v6

    int-to-long v6, v3

    .line 340
    cmp-long v3, v4, v10

    if-lez v3, :cond_0

    .line 343
    iget v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a:I

    const/4 v8, -0x1

    if-ne v3, v8, :cond_4

    move v2, v1

    .line 344
    goto :goto_0

    .line 346
    :cond_4
    cmp-long v3, v6, v10

    if-lez v3, :cond_0

    .line 347
    const-wide/16 v8, 0x64

    mul-long/2addr v4, v8

    div-long/2addr v4, v6

    .line 348
    iget v3, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-ltz v3, :cond_5

    :goto_1
    move v2, v1

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public setMinViewabilityPercent(I)V
    .locals 3

    .prologue
    .line 98
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting the viewability percentage.\n\tViewability watcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tPercentage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_0
    iput p1, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->a:I

    .line 105
    return-void
.end method

.method public startWatching()V
    .locals 3

    .prologue
    .line 110
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting watcher.\n\tViewability watcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->g:Ljava/lang/ref/WeakReference;

    .line 113
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    new-instance v0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$2;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$2;-><init>(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 146
    return-void
.end method

.method public stopWatching()V
    .locals 3

    .prologue
    .line 151
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Lcom/millennialmedia/internal/utils/ViewUtils;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stopping watcher.\n\tViewability watcher: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->g:Ljava/lang/ref/WeakReference;

    .line 154
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    new-instance v0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$3;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher$3;-><init>(Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 175
    return-void
.end method
