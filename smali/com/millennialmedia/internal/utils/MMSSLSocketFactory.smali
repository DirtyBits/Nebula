.class public Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;,
        Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$MMSSLContextFactory;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static volatile h:Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;


# instance fields
.field private e:Ljavax/net/ssl/SSLSocketFactory;

.field private f:[Ljava/lang/String;

.field private g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    const-class v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    .line 45
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "TLSv1.1"

    aput-object v1, v0, v2

    const-string v1, "TLSv1.2"

    aput-object v1, v0, v3

    sput-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->b:[Ljava/lang/String;

    .line 46
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "TLS_RSA_WITH_AES_128_CBC_SHA"

    aput-object v1, v0, v2

    const-string v1, "TLS_RSA_WITH_AES_128_GCM_SHA256"

    aput-object v1, v0, v3

    const-string v1, "TLS_RSA_WITH_AES_256_CBC_SHA"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, "TLS_RSA_WITH_AES_256_GCM_SHA384"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->c:[Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->d:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;-><init>()V

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a()Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->h:Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->g:[Ljava/lang/String;

    .line 95
    return-void
.end method

.method private a()Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    const-string v2, "Initializing MMSSLSocketFactory"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->b()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    .line 111
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    .line 113
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSupportedProtocols()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->g:[Ljava/lang/String;

    .line 114
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    .line 116
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->close()V

    .line 118
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Protocols enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->g:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cipher suites enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_1
    :goto_0
    return-object p0

    .line 123
    :cond_2
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    const-string v2, "Failed to instantiate a valid SSLContext."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object p0, v1

    .line 125
    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    sget-object v2, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    const-string v3, "Failed to initialize MMSSLSocketFactory"

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p0, v1

    .line 130
    goto :goto_0
.end method

.method private a(Ljava/net/Socket;)Ljava/net/Socket;
    .locals 5

    .prologue
    .line 225
    instance-of v1, p1, Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_1

    .line 226
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    sget-object v1, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    const-string v2, "Setting enabled protocols and cipher suites on secure socket"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_0
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    move-object v1, v0

    iget-object v2, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->g:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 235
    move-object v0, p1

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    move-object v1, v0

    iget-object v2, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :cond_1
    :goto_0
    return-object p1

    .line 237
    :catch_0
    move-exception v1

    .line 238
    sget-object v2, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to set secure socket properties - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 248
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 249
    new-instance v1, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 252
    invoke-interface {v1, v0}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 254
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljavax/net/ssl/SSLContext;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 141
    .line 143
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 145
    :try_start_0
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$MMSSLContextFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    if-nez v0, :cond_0

    .line 155
    new-instance v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;

    invoke-direct {v0, p0, v1}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;-><init>(Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$1;)V

    .line 158
    :cond_0
    const-string v1, "TLS"

    invoke-interface {v0, v1}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$MMSSLContextFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    return-object v0

    .line 146
    :catch_0
    move-exception v0

    .line 147
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not instantiate custom MMSSLContextFactory using class = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reverting to default."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static getInstance()Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->h:Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    return-object v0
.end method


# virtual methods
.method public createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    .line 194
    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 202
    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 210
    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    .line 219
    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->e:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    .line 186
    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;->f:[Ljava/lang/String;

    return-object v0
.end method
