.class public Lcom/millennialmedia/internal/utils/ViewUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;,
        Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;
    }
.end annotation


# static fields
.field public static final AT_LEAST_ONE_PIXEL_VIEWABLE:I = -0x1

.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/millennialmedia/internal/utils/ViewUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static attachView(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/millennialmedia/internal/utils/ViewUtils;->attachView(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 375
    return-void
.end method

.method public static attachView(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    .line 380
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 381
    invoke-static {p1}, Lcom/millennialmedia/internal/utils/ViewUtils;->removeFromParent(Landroid/view/View;)V

    .line 386
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 387
    instance-of v1, v0, Landroid/content/MutableContextWrapper;

    if-eqz v1, :cond_2

    .line 388
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 389
    if-eq v0, v1, :cond_2

    .line 390
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 391
    sget-object v2, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v3, "Changing view context to match parent context"

    invoke-static {v2, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_1
    check-cast v0, Landroid/content/MutableContextWrapper;

    invoke-virtual {v0, v1}, Landroid/content/MutableContextWrapper;->setBaseContext(Landroid/content/Context;)V

    .line 397
    :cond_2
    if-eqz p2, :cond_3

    .line 398
    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 402
    :goto_0
    return-void

    .line 400
    :cond_3
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static convertPixelsToDips(I)I
    .locals 2

    .prologue
    .line 600
    int-to-float v0, p0

    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getDisplayDensity()F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static convertPixelsToDips(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 606
    if-nez p0, :cond_1

    .line 607
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to convert for null dimensions"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getDisplayDensity()F

    move-result v0

    .line 615
    iget v1, p0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-int v1, v1

    .line 616
    iget v2, p0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    .line 618
    iget v3, p0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    div-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, p0, Landroid/graphics/Rect;->left:I

    .line 619
    iget v3, p0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    div-float v0, v3, v0

    float-to-int v0, v0

    iput v0, p0, Landroid/graphics/Rect;->top:I

    .line 620
    iget v0, p0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    iput v0, p0, Landroid/graphics/Rect;->right:I

    .line 621
    iget v0, p0, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    .line 623
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 624
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Converted dimensions from pixels to dips: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/graphics/Rect;->flattenToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getActivityForView(Landroid/view/View;)Landroid/app/Activity;
    .locals 4

    .prologue
    .line 631
    const/4 v1, 0x0

    .line 633
    if-eqz p0, :cond_2

    .line 637
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 638
    :goto_0
    instance-of v2, v0, Landroid/content/MutableContextWrapper;

    if-eqz v2, :cond_0

    .line 639
    check-cast v0, Landroid/content/MutableContextWrapper;

    invoke-virtual {v0}, Landroid/content/MutableContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 642
    :cond_0
    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_2

    .line 643
    check-cast v0, Landroid/app/Activity;

    .line 647
    :goto_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 648
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Found activity <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "> for view <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public static getActivityHashForView(Landroid/view/View;)I
    .locals 7

    .prologue
    .line 444
    const/4 v0, -0x1

    .line 446
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getActivityForView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v1

    .line 447
    if-nez v1, :cond_1

    .line 448
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to get activity hash"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :goto_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "Found activity hash code <%d> for view <%s>"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 455
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 454
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    :cond_0
    return v0

    .line 450
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static getContentDimensions(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 538
    if-nez p0, :cond_1

    .line 539
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to calculate content dimensions for null view"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 561
    :cond_0
    :goto_0
    return-object p1

    .line 544
    :cond_1
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getDecorView(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 545
    if-nez v1, :cond_2

    .line 546
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v2, "Unable to calculate content for null root view"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 548
    goto :goto_0

    .line 551
    :cond_2
    if-nez p1, :cond_3

    .line 552
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 555
    :cond_3
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 557
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "Content dimensions for View <%s>: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/graphics/Rect;->flattenToString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getDecorView(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 464
    .line 467
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getActivityForView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_1

    .line 469
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 477
    :goto_0
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 478
    check-cast v0, Landroid/view/ViewGroup;

    .line 481
    :goto_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    sget-object v1, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v2, "Found decor view <%s> for view <%s>"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_0
    return-object v0

    .line 470
    :cond_1
    if-eqz p0, :cond_3

    .line 474
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static getParentContainer(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 433
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 434
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 435
    const/4 v0, 0x0

    .line 438
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_0
.end method

.method public static getViewDimensionsOnScreen(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 503
    if-nez p0, :cond_1

    .line 504
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to calculate view dimensions for null view"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    const/4 p1, 0x0

    .line 527
    :cond_0
    :goto_0
    return-object p1

    .line 509
    :cond_1
    if-nez p1, :cond_2

    .line 510
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 513
    :cond_2
    new-array v0, v2, [I

    .line 517
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 518
    aget v1, v0, v3

    iput v1, p1, Landroid/graphics/Rect;->left:I

    .line 519
    aget v0, v0, v4

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 520
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 521
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 523
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "On screen dimensions for View <%s>: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->flattenToString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getViewDimensionsRelativeToContent(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 572
    invoke-static {p0, p1}, Lcom/millennialmedia/internal/utils/ViewUtils;->getViewDimensionsOnScreen(Landroid/view/View;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    .line 573
    if-eqz v1, :cond_1

    .line 574
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getDecorView(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 575
    if-nez v0, :cond_0

    .line 576
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to calculate dimensions for null root view"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const/4 v0, 0x0

    .line 594
    :goto_0
    return-object v0

    .line 583
    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 584
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 585
    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 586
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 589
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 590
    sget-object v2, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v3, "Dimensions relative to content for View <%s>: %s"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p0, v4, v0

    const/4 v5, 0x1

    if-eqz v1, :cond_3

    .line 591
    invoke-virtual {v1}, Landroid/graphics/Rect;->flattenToString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v4, v5

    .line 590
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    .line 594
    goto :goto_0

    .line 591
    :cond_3
    const-string v0, "null"

    goto :goto_1
.end method

.method public static getViewPositionOnScreen(Landroid/view/View;)Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 491
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 495
    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 497
    new-instance v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method

.method public static isChild(Landroid/view/ViewGroup;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 422
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 423
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 424
    check-cast v0, Landroid/view/ViewGroup;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    .line 427
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 424
    goto :goto_0

    :cond_1
    move v0, v1

    .line 427
    goto :goto_0
.end method

.method public static removeFromParent(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 407
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 408
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 409
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    sget-object v0, Lcom/millennialmedia/internal/utils/ViewUtils;->a:Ljava/lang/String;

    const-string v1, "Unable to remove view from parent, no valid parent view found"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
