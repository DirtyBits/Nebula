.class Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$MMSSLContextFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultMMSSLContextFactory"
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;


# direct methods
.method private constructor <init>(Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;->a:Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$1;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/utils/MMSSLSocketFactory$DefaultMMSSLContextFactory;-><init>(Lcom/millennialmedia/internal/utils/MMSSLSocketFactory;)V

    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-static {p1}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 37
    invoke-virtual {v0, v1, v1, v1}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 39
    return-object v0
.end method
