.class public Lcom/millennialmedia/internal/utils/HttpUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;,
        Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;,
        Lcom/millennialmedia/internal/utils/HttpUtils$Response;,
        Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/millennialmedia/internal/utils/HttpUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->a:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static getBitmapFromGetRequest(Ljava/lang/String;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 391
    new-instance v0, Lcom/millennialmedia/internal/utils/IOUtils$BitmapStreamer;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/IOUtils$BitmapStreamer;-><init>()V

    invoke-static {p0, v1, v1, v1, v0}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static getContentFromGetRequest(Ljava/lang/String;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 367
    new-instance v0, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;

    invoke-direct {v0}, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;-><init>()V

    invoke-static {p0, v1, v1, v1, v0}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static getContentFromGetRequest(Ljava/lang/String;I)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 373
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;

    invoke-direct {v1}, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;-><init>()V

    invoke-static {p0, v2, v2, v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static getContentFromPostRequest(Ljava/lang/String;I)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 397
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;

    invoke-direct {v1}, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;-><init>()V

    invoke-static {p0, v2, v2, v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static getContentFromPostRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 2

    .prologue
    .line 379
    const/4 v0, 0x0

    new-instance v1, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;

    invoke-direct {v1}, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;-><init>()V

    invoke-static {p0, p1, p2, v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static getContentFromPostRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 2

    .prologue
    .line 385
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;

    invoke-direct {v1}, Lcom/millennialmedia/internal/utils/IOUtils$StringStreamer;-><init>()V

    invoke-static {p0, p1, p2, v0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils;->sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    return-object v0
.end method

.method public static sendHttpRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)Lcom/millennialmedia/internal/utils/HttpUtils$Response;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 328
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 329
    if-nez p3, :cond_4

    const/16 v7, 0x3a98

    .line 336
    :goto_0
    new-instance v1, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/millennialmedia/internal/utils/HttpUtils$ResponseStreamer;)V

    .line 339
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    sget-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->a:Ljava/lang/String;

    .line 341
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "Sending Http request.\n\t%s"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 340
    invoke-static {v0, v4}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    if-eqz v0, :cond_1

    .line 345
    sget-object v0, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    invoke-interface {v0, p0, v1}, Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;->onRequest(Ljava/lang/String;Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;)V

    .line 348
    :cond_1
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 350
    int-to-long v4, v7

    invoke-virtual {v1, v4, v5}, Lcom/millennialmedia/internal/utils/HttpUtils$HttpRequestRunner;->a(J)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    .line 352
    sget-object v1, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    if-eqz v1, :cond_2

    .line 353
    sget-object v1, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    invoke-interface {v1, p0, v0}, Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;->onResponse(Ljava/lang/String;Lcom/millennialmedia/internal/utils/HttpUtils$Response;)V

    .line 356
    :cond_2
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 357
    sget-object v1, Lcom/millennialmedia/internal/utils/HttpUtils;->a:Ljava/lang/String;

    .line 358
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "Http response.\n\trequestId: %d\n\t%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v9

    invoke-virtual {v0}, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_3
    return-object v0

    .line 329
    :cond_4
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_0
.end method

.method public static setInterceptor(Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;)V
    .locals 0

    .prologue
    .line 404
    sput-object p0, Lcom/millennialmedia/internal/utils/HttpUtils;->b:Lcom/millennialmedia/internal/utils/HttpUtils$HttpInterceptor;

    .line 405
    return-void
.end method
