.class public interface abstract Lcom/millennialmedia/internal/adadapters/MMAdAdapter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT_MIN_IMPRESSION_DELAY:I = 0x3e8

.field public static final DEFAULT_MIN_IMPRESSION_VIEWABILITY_PERCENTAGE:I = 0x32

.field public static final NO_IMPRESSION_DELAY:I = 0x0

.field public static final NO_MIN_IMPRESSION_VIEWABILITY_PERCENTAGE:I = -0x1


# virtual methods
.method public abstract close()V
.end method

.method public abstract getImpressionDelay()J
.end method

.method public abstract getMinImpressionViewabilityPercentage()I
.end method

.method public abstract release()V
.end method
