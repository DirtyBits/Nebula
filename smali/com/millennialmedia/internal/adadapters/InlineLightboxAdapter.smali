.class public Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;
.super Lcom/millennialmedia/internal/adadapters/InlineAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/adadapters/MMAdAdapter;


# instance fields
.field private c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

.field private d:Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

.field private e:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter;-><init>()V

    .line 20
    new-instance v0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter$1;-><init>(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->e:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->d:Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController;->close()V

    .line 118
    :cond_0
    return-void
.end method

.method public display(Landroid/widget/RelativeLayout;II)V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 125
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 127
    iget-object v1, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    invoke-virtual {v1, p1, v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController;->attach(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    :cond_0
    return-void
.end method

.method public getImpressionDelay()J
    .locals 2

    .prologue
    .line 91
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinImpressionDuration()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getMinImpressionViewabilityPercentage()I
    .locals 1

    .prologue
    .line 98
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinImpressionViewabilityPercent()I

    move-result v0

    return v0
.end method

.method public init(Landroid/content/Context;Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;)V
    .locals 2

    .prologue
    .line 82
    iput-object p2, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->d:Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;

    .line 83
    new-instance v0, Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    iget-object v1, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->e:Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;

    invoke-direct {v0, v1}, Lcom/millennialmedia/internal/adcontrollers/LightboxController;-><init>(Lcom/millennialmedia/internal/adcontrollers/LightboxController$LightboxControllerListener;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    .line 84
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    iget-object v1, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/millennialmedia/internal/adcontrollers/LightboxController;->init(Landroid/content/Context;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adcontrollers/LightboxController;->release()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/internal/adadapters/InlineLightboxAdapter;->c:Lcom/millennialmedia/internal/adcontrollers/LightboxController;

    .line 109
    :cond_0
    return-void
.end method
