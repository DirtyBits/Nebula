.class public Lcom/millennialmedia/internal/Handshake;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;,
        Lcom/millennialmedia/internal/Handshake$HandshakeInfo;
    }
.end annotation


# static fields
.field public static final HANDSHAKE_JSON:Ljava/lang/String; = "handshake.json"

.field public static final HANDSHAKE_PATH:Ljava/lang/String; = "/admax/sdk/handshake/1"

.field public static final HANDSHAKE_VERSION:I = 0x1

.field private static final a:Ljava/lang/String;

.field private static b:Z

.field private static c:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

.field private static d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

.field private static e:I

.field private static f:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private static g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/millennialmedia/internal/playlistserver/PlayListServerAdapter;",
            ">;>;"
        }
    .end annotation
.end field

.field private static h:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    const-class v0, Lcom/millennialmedia/internal/Handshake;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    .line 78
    sput-boolean v1, Lcom/millennialmedia/internal/Handshake;->b:Z

    .line 81
    sput v1, Lcom/millennialmedia/internal/Handshake;->e:I

    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->f:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 85
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$HandshakeInfo;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x1388

    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 369
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parsing handshake:\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    if-eqz p0, :cond_5

    .line 376
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 379
    new-instance v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    invoke-direct {v1}, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;-><init>()V

    .line 381
    const-string v4, "ver"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->version:Ljava/lang/String;

    .line 385
    :try_start_0
    iget-object v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->version:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 392
    if-le v4, v5, :cond_1

    .line 393
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake response did not contain a compatible version. Received version, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " expected max version of "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :goto_0
    return-object v2

    .line 386
    :catch_0
    move-exception v0

    .line 387
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handshake version is not a valid integer, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->version:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 400
    :cond_1
    const-string v2, "config"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->config:Ljava/lang/String;

    .line 402
    const-string v2, "playlistServer"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 403
    const-string v4, "name"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->activePlaylistServerName:Ljava/lang/String;

    .line 404
    const-string v4, "baseUrl"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->activePlaylistServerBaseUrl:Ljava/lang/String;

    .line 406
    const-string v2, "handshakeBaseUrl"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->handshakeBaseUrl:Ljava/lang/String;

    .line 407
    const-string v2, "rptBaseUrl"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBaseUrl:Ljava/lang/String;

    .line 408
    const-string v2, "ttl"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->handshakeTtl:I

    .line 409
    const-string v2, "sdkEnabled"

    invoke-virtual {v3, v2, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->sdkEnabled:Z

    .line 410
    const-string v2, "moatEnabled"

    invoke-virtual {v3, v2, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->moatEnabled:Z

    .line 411
    const-string v2, "rptBatchSize"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBatchSize:I

    .line 412
    const-string v2, "rptFreq"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBatchFrequency:I

    .line 413
    const-string v2, "inlineTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->inlineTimeout:I

    .line 414
    const-string v2, "instlTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->interstitialTimeout:I

    .line 415
    const-string v2, "nativeTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTimeout:I

    .line 416
    const-string v2, "clientAdTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->clientMediationTimeout:I

    .line 417
    const-string v2, "serverAdTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->serverToServerTimeout:I

    .line 418
    const-string v2, "exTmax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->exchangeTimeout:I

    .line 419
    const-string v2, "minInlineRefresh"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minInlineRefreshRate:I

    .line 420
    const-string v2, "instlExpDur"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->interstitialExpirationDuration:I

    .line 421
    const-string v2, "nativeExpDur"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeExpirationDuration:I

    .line 422
    const-string v2, "vastSkipOffsetMax"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vastVideoSkipOffsetMax:I

    .line 423
    const-string v2, "vastSkipOffsetMin"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vastVideoSkipOffsetMin:I

    .line 424
    invoke-static {v3}, Lcom/millennialmedia/internal/Handshake;->a(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTypeDefinitions:Ljava/util/Map;

    .line 426
    const-string v2, "vpaid"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 427
    const-string v4, "startAdTimeout"

    invoke-static {v2, v4, v6}, Lcom/millennialmedia/internal/utils/JSONUtils;->optInt(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidStartAdTimeout:I

    .line 428
    const-string v4, "skipAdTimeout"

    const/16 v5, 0x1f4

    invoke-static {v2, v4, v5}, Lcom/millennialmedia/internal/utils/JSONUtils;->optInt(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidSkipAdTimeout:I

    .line 429
    const-string v4, "adUnitTimeout"

    invoke-static {v2, v4, v6}, Lcom/millennialmedia/internal/utils/JSONUtils;->optInt(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidAdUnitTimeout:I

    .line 430
    const-string v4, "htmlEndCardTimeout"

    invoke-static {v2, v4, v6}, Lcom/millennialmedia/internal/utils/JSONUtils;->optInt(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidHtmlEndCardTimeout:I

    .line 431
    const-string v4, "maxBackButtonDelay"

    const/16 v5, 0x7d0

    invoke-static {v2, v4, v5}, Lcom/millennialmedia/internal/utils/JSONUtils;->optInt(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidMaxBackButtonDelay:I

    .line 434
    const-string v2, "minImpressionDuration"

    .line 435
    invoke-virtual {v3, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minImpressionDuration:I

    .line 437
    const-string v2, "minImpressionViewabilityPercent"

    .line 438
    invoke-virtual {v3, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minImpressionViewabilityPercent:I

    .line 440
    const-string v2, "exists"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 441
    if-eqz v2, :cond_3

    .line 442
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 443
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 444
    if-nez v3, :cond_2

    .line 442
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 449
    :cond_2
    :try_start_1
    iget-object v4, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->existingPackages:Ljava/util/Map;

    const-string v5, "id"

    .line 450
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "pkg"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 452
    :catch_1
    move-exception v3

    goto :goto_2

    .line 459
    :cond_3
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 460
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Handshake successfully parsed"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    :goto_3
    move-object v2, v0

    .line 464
    goto/16 :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_3
.end method

.method private static a(Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    const/4 v0, 0x0

    .line 472
    const-string v1, "nativeConfig"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 474
    const-string v2, "typeDefs"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 475
    if-eqz v3, :cond_2

    .line 476
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 478
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 479
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 481
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 483
    new-instance v5, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    const-string v6, "name"

    .line 484
    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;-><init>(Ljava/lang/String;)V

    .line 486
    const-string v6, "components"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 488
    invoke-virtual {v6}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .line 489
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 491
    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 493
    iget-object v9, v5, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->componentDefinitions:Ljava/util/List;

    new-instance v10, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    const-string v11, "publisherRequired"

    .line 495
    invoke-virtual {v8, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    const-string v12, "advertiserRequired"

    .line 496
    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-direct {v10, v1, v11, v8}, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;-><init>(Ljava/lang/String;II)V

    .line 493
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 499
    :cond_0
    invoke-interface {v2, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 503
    :cond_2
    return-object v0
.end method

.method static synthetic a()V
    .locals 0

    .prologue
    .line 39
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->b()V

    return-void
.end method

.method private static b()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 257
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    .line 258
    if-nez v0, :cond_1

    .line 259
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v1, "Handshake request already in progress"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    const v1, 0xea60

    .line 271
    :try_start_0
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->isDeviceIdle()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 272
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Skipping handshake request while device is dozing"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :goto_1
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->f:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_3

    .line 344
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 345
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Canceling existing handshake refresh"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_2
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->f:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 350
    :cond_3
    new-instance v0, Lcom/millennialmedia/internal/Handshake$2;

    invoke-direct {v0}, Lcom/millennialmedia/internal/Handshake$2;-><init>()V

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->f:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 363
    :goto_2
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 274
    :cond_4
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 275
    const-string v0, "ver"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 276
    const-string v0, "sdkVer"

    const-string v3, "6.4.0-0220c20"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 277
    const-string v0, "os"

    const-string v3, "android"

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 278
    const-string v0, "osv"

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 279
    const-string v0, "appId"

    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 281
    const-string v0, "https://ads.nexage.com"

    .line 282
    sget-object v3, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    if-eqz v3, :cond_5

    sget v3, Lcom/millennialmedia/internal/Handshake;->e:I

    const/16 v4, 0xa

    if-ge v3, v4, :cond_5

    .line 283
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->handshakeBaseUrl:Ljava/lang/String;

    .line 285
    :cond_5
    const-string v3, "/admax/sdk/handshake/1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 286
    sget v3, Lcom/millennialmedia/internal/Handshake;->e:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/millennialmedia/internal/Handshake;->e:I

    .line 288
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 290
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 291
    sget-object v3, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Executing handshake request.\n\tattempt: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/millennialmedia/internal/Handshake;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\turl: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\tpost data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_6
    const-string v3, "application/json"

    const/16 v4, 0x3a98

    .line 298
    invoke-static {v0, v2, v3, v4}, Lcom/millennialmedia/internal/utils/HttpUtils;->getContentFromPostRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/millennialmedia/internal/utils/HttpUtils$Response;

    move-result-object v0

    .line 301
    iget v2, v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->code:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_8

    iget-object v2, v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->content:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_8

    .line 303
    :try_start_2
    iget-object v2, v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->content:Ljava/lang/String;

    invoke-static {v2}, Lcom/millennialmedia/internal/Handshake;->a(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v2

    .line 304
    if-nez v2, :cond_7

    .line 305
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Unable to create handshake info object"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 326
    :catch_0
    move-exception v0

    .line 327
    :try_start_3
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "An error occurred parsing the handshake response.  Reverting to last known good copy."

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 359
    :catch_1
    move-exception v0

    .line 360
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Cannot build the handshake request data"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 307
    :cond_7
    :try_start_4
    sput-object v2, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    .line 309
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getMillennialDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "handshake.json"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 310
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 313
    :try_start_5
    iget-object v0, v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->content:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/millennialmedia/internal/utils/IOUtils;->write(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 317
    :try_start_6
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    .line 322
    :goto_3
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getHandshakeTtl()I

    move-result v1

    .line 324
    const/4 v0, 0x0

    sput v0, Lcom/millennialmedia/internal/Handshake;->e:I
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_1

    .line 331
    :catch_2
    move-exception v0

    .line 332
    :try_start_7
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Unable to open a file to store the handshake response."

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1

    goto/16 :goto_1

    .line 314
    :catch_3
    move-exception v0

    .line 315
    :try_start_8
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v4, "Error storing handshake response"

    invoke-static {v2, v4, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 317
    :try_start_9
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_3

    .line 334
    :catch_4
    move-exception v0

    .line 335
    :try_start_a
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Exception occurred when trying to load handshake."

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_1

    .line 317
    :catchall_0
    move-exception v0

    :try_start_b
    invoke-static {v3}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    throw v0
    :try_end_b
    .catch Lorg/json/JSONException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    .line 339
    :cond_8
    :try_start_c
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handshake request failed with HTTP response code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/millennialmedia/internal/utils/HttpUtils$Response;->code:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_1
.end method

.method public static getActivePlayListServerAdapterClass()Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/millennialmedia/internal/playlistserver/PlayListServerAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 895
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->g:Ljava/util/Map;

    .line 896
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getActivePlaylistServerName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 898
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 899
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake active playlist server adapter class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    :cond_0
    return-object v0
.end method

.method public static getActivePlaylistServerBaseUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 563
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->activePlaylistServerBaseUrl:Ljava/lang/String;

    .line 564
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 565
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake active playlist server base url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    :cond_0
    return-object v0
.end method

.method public static getActivePlaylistServerName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 552
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->activePlaylistServerName:Ljava/lang/String;

    .line 553
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 554
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake playlist server name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_0
    return-object v0
.end method

.method public static getClientMediationTimeout()I
    .locals 4

    .prologue
    .line 675
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->clientMediationTimeout:I

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 676
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 677
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake client mediation timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    :cond_0
    return v0
.end method

.method public static getConfig()Ljava/lang/String;
    .locals 4

    .prologue
    .line 541
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->config:Ljava/lang/String;

    .line 542
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 543
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake config: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_0
    return-object v0
.end method

.method public static getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;
    .locals 2

    .prologue
    .line 509
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    if-eqz v0, :cond_1

    .line 510
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v1, "Returning current handshake info"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    .line 524
    :goto_0
    return-object v0

    .line 516
    :cond_1
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->c:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    if-eqz v0, :cond_3

    .line 517
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 518
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v1, "Returning default handshake info"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_2
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->c:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    goto :goto_0

    .line 524
    :cond_3
    new-instance v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    invoke-direct {v0}, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;-><init>()V

    goto :goto_0
.end method

.method public static getExchangeTimeout()I
    .locals 4

    .prologue
    .line 698
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->exchangeTimeout:I

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 699
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 700
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake exchange timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    :cond_0
    return v0
.end method

.method public static getExistingIds()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 908
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 909
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v3, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->existingPackages:Ljava/util/Map;

    .line 911
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 912
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 913
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/Utils;->isPackageAvailable(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 914
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 918
    :cond_1
    return-object v2
.end method

.method public static getExistingPackages()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 924
    new-instance v0, Ljava/util/HashMap;

    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->existingPackages:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 926
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 927
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake existing packages: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_0
    return-object v0
.end method

.method public static getHandshakeTtl()I
    .locals 4

    .prologue
    .line 585
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->handshakeTtl:I

    const v1, 0xea60

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 586
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 587
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake handshake ttl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    :cond_0
    return v0
.end method

.method public static getInlineTimeout()I
    .locals 4

    .prologue
    .line 641
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->inlineTimeout:I

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 642
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 643
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake inline timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :cond_0
    return v0
.end method

.method public static getInterstitialExpirationDuration()I
    .locals 4

    .prologue
    .line 722
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->interstitialExpirationDuration:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 723
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 724
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake interstitial expiration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_0
    return v0
.end method

.method public static getInterstitialTimeout()I
    .locals 4

    .prologue
    .line 652
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->interstitialTimeout:I

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 653
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake interstitial timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_0
    return v0
.end method

.method public static getMinImpressionDuration()I
    .locals 4

    .prologue
    .line 762
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minImpressionDuration:I

    .line 764
    if-ltz v0, :cond_0

    const v1, 0xea60

    if-le v0, v1, :cond_1

    .line 767
    :cond_0
    const/4 v0, 0x0

    .line 770
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 771
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake minimum impression duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    :cond_2
    return v0
.end method

.method public static getMinImpressionViewabilityPercent()I
    .locals 4

    .prologue
    .line 744
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minImpressionViewabilityPercent:I

    .line 746
    if-ltz v0, :cond_0

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 749
    :cond_0
    const/4 v0, 0x0

    .line 752
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 753
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake minimum impression viewability percentage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_2
    return v0
.end method

.method public static getMinInlineRefreshRate()I
    .locals 4

    .prologue
    .line 710
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->minInlineRefreshRate:I

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 712
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 713
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake min inline refresh rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :cond_0
    return v0
.end method

.method public static getNativeExpirationDuration()I
    .locals 4

    .prologue
    .line 733
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeExpirationDuration:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 734
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 735
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake native expiration duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    :cond_0
    return v0
.end method

.method public static getNativeTimeout()I
    .locals 4

    .prologue
    .line 663
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTimeout:I

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 664
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 665
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake native timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    :cond_0
    return v0
.end method

.method public static getNativeTypeDefinition(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;
    .locals 4

    .prologue
    .line 867
    const/4 v0, 0x0

    .line 869
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTypeDefinitions:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 870
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTypeDefinitions:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    .line 873
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 874
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake native type definition: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    :cond_1
    return-object v0
.end method

.method public static getNativeTypeDefinitions()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 883
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->nativeTypeDefinitions:Ljava/util/Map;

    .line 885
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 886
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake native type definitions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    :cond_0
    return-object v0
.end method

.method public static getReportingBaseUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 574
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBaseUrl:Ljava/lang/String;

    .line 575
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 576
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake reporting base url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_0
    return-object v0
.end method

.method public static getReportingBatchFrequency()I
    .locals 4

    .prologue
    .line 630
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBatchFrequency:I

    const v1, 0x1d4c0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 631
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 632
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake reporting batch frequency: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    return v0
.end method

.method public static getReportingBatchSize()I
    .locals 4

    .prologue
    .line 618
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->reportingBatchSize:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 619
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 620
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake reportingBatchSize: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    :cond_0
    return v0
.end method

.method public static getServerToServerTimeout()I
    .locals 4

    .prologue
    .line 687
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->serverToServerTimeout:I

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 688
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 689
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake server to server timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    :cond_0
    return v0
.end method

.method public static getVASTVideoSkipOffsetMax()I
    .locals 4

    .prologue
    .line 780
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vastVideoSkipOffsetMax:I

    .line 781
    if-gez v0, :cond_0

    .line 782
    const/4 v0, 0x0

    .line 785
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 786
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VAST video max skip offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    :cond_1
    return v0
.end method

.method public static getVASTVideoSkipOffsetMin()I
    .locals 4

    .prologue
    .line 795
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vastVideoSkipOffsetMin:I

    .line 796
    if-gez v0, :cond_0

    .line 797
    const/4 v0, 0x0

    .line 800
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 801
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VAST video min skip offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    :cond_1
    return v0
.end method

.method public static getVPAIDAdUnitTimeout()I
    .locals 4

    .prologue
    .line 832
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidAdUnitTimeout:I

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 833
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 834
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VPAID ad unit timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    :cond_0
    return v0
.end method

.method public static getVPAIDHTMLEndCardTimeout()I
    .locals 4

    .prologue
    .line 844
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidHtmlEndCardTimeout:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 845
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 846
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VPAID html end card timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    :cond_0
    return v0
.end method

.method public static getVPAIDMaxBackButtonDelay()I
    .locals 4

    .prologue
    .line 856
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidMaxBackButtonDelay:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 857
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VPAID max back button delay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_0
    return v0
.end method

.method public static getVPAIDSkipAdTimeout()I
    .locals 4

    .prologue
    .line 821
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidSkipAdTimeout:I

    const/16 v1, 0x1f4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 822
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 823
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VPAID skip ad timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_0
    return v0
.end method

.method public static getVPAIDStartAdTimeout()I
    .locals 4

    .prologue
    .line 810
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->vpaidStartAdTimeout:I

    const/16 v1, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 811
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 812
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake VPAID start ad timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :cond_0
    return v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 4

    .prologue
    .line 530
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->version:Ljava/lang/String;

    .line 531
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 532
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    :cond_0
    return-object v0
.end method

.method public static initialize()V
    .locals 5

    .prologue
    .line 159
    sget-boolean v0, Lcom/millennialmedia/internal/Handshake;->b:Z

    if-eqz v0, :cond_1

    .line 160
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v1, "Handshake already initialized"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/millennialmedia/internal/Handshake;->b:Z

    .line 171
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->g:Ljava/util/Map;

    .line 172
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->g:Ljava/util/Map;

    const-string v1, "green"

    const-class v2, Lcom/millennialmedia/internal/playlistserver/GreenServerAdapter;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->g:Ljava/util/Map;

    const-string v1, "orange"

    const-class v2, Lcom/millennialmedia/internal/playlistserver/OrangeServerAdapter;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const/4 v1, 0x0

    .line 178
    :try_start_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 179
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Loading packaged default handshake"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_2
    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "mmadsdk/default_handshake.json"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 183
    const-string v0, "UTF-8"

    invoke-static {v1, v0}, Lcom/millennialmedia/internal/utils/IOUtils;->read(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    invoke-static {v0}, Lcom/millennialmedia/internal/Handshake;->a(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->c:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    move-object v0, v1

    .line 196
    :goto_1
    :try_start_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 197
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Loading previously stored handshake"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_3
    new-instance v2, Ljava/io/File;

    invoke-static {}, Lcom/millennialmedia/internal/utils/EnvironmentUtils;->getMillennialDir()Ljava/io/File;

    move-result-object v1

    const-string v3, "handshake.json"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 201
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 203
    :try_start_2
    const-string v0, "UTF-8"

    invoke-static {v1, v0}, Lcom/millennialmedia/internal/utils/IOUtils;->read(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-static {v0}, Lcom/millennialmedia/internal/Handshake;->a(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    .line 205
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->d:Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    if-nez v0, :cond_4

    .line 206
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "Unable to create handshake info object"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 216
    :cond_4
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    :try_start_3
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Could not read default handshake."

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    move-object v0, v1

    .line 192
    goto :goto_1

    .line 188
    :catch_1
    move-exception v0

    .line 189
    :try_start_4
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Could not parse the default handshake."

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 191
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    move-object v0, v1

    .line 192
    goto :goto_1

    .line 191
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    throw v0

    .line 209
    :catch_2
    move-exception v1

    .line 210
    :goto_2
    :try_start_5
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v2, "No handshake.json exists."

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 216
    invoke-static {v0}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    goto/16 :goto_0

    .line 211
    :catch_3
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 212
    :goto_3
    :try_start_6
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Could not read handshake.json"

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 216
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    goto/16 :goto_0

    .line 213
    :catch_4
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 214
    :goto_4
    :try_start_7
    sget-object v2, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    const-string v3, "Could not parse handshake.json"

    invoke-static {v2, v3, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 216
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_5
    invoke-static {v1}, Lcom/millennialmedia/internal/utils/IOUtils;->closeStream(Ljava/io/Closeable;)Z

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_5

    .line 213
    :catch_5
    move-exception v0

    goto :goto_4

    .line 211
    :catch_6
    move-exception v0

    goto :goto_3

    .line 209
    :catch_7
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method

.method public static isMoatEnabled()Z
    .locals 4

    .prologue
    .line 607
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->moatEnabled:Z

    .line 608
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 609
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake moat enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    :cond_0
    return v0
.end method

.method public static isRequestInProgress()Z
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public static isSdkEnabled()Z
    .locals 4

    .prologue
    .line 596
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getCurrentHandshakeInfo()Lcom/millennialmedia/internal/Handshake$HandshakeInfo;

    move-result-object v0

    iget-boolean v0, v0, Lcom/millennialmedia/internal/Handshake$HandshakeInfo;->sdkEnabled:Z

    .line 597
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598
    sget-object v1, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handshake sdk enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_0
    return v0
.end method

.method public static request(Z)V
    .locals 3

    .prologue
    .line 234
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    sget-object v0, Lcom/millennialmedia/internal/Handshake;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting handshake, async mode <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    if-eqz p0, :cond_1

    .line 239
    new-instance v0, Lcom/millennialmedia/internal/Handshake$1;

    invoke-direct {v0}, Lcom/millennialmedia/internal/Handshake$1;-><init>()V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_1
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->b()V

    goto :goto_0
.end method
