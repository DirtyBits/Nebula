.class public Lcom/millennialmedia/internal/MMWebView;
.super Landroid/webkit/WebView;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/MMWebView$MMWebViewGestureListener;,
        Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;,
        Lcom/millennialmedia/internal/MMWebView$MMWebViewViewabilityListener;,
        Lcom/millennialmedia/internal/MMWebView$MMWebChromeClient;,
        Lcom/millennialmedia/internal/MMWebView$MMWebViewClient;,
        Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;,
        Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;

.field public static googleSecurityPatchEnabled:Z


# instance fields
.field protected final a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

.field b:Lcom/millennialmedia/internal/JSBridge;

.field c:Ljava/lang/String;

.field private final e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

.field private final f:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

.field private g:Landroid/view/GestureDetector;

.field private h:Z

.field private i:[I

.field private j:[I

.field private volatile k:Z

.field private l:Lcom/b/a/a/a/k;

.field private m:Lcom/b/a/a/a/x;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/millennialmedia/internal/MMWebView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    .line 65
    const/4 v0, 0x1

    sput-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 369
    new-instance v0, Landroid/content/MutableContextWrapper;

    invoke-direct {v0, p1}, Landroid/content/MutableContextWrapper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 56
    iput-boolean v2, p0, Lcom/millennialmedia/internal/MMWebView;->h:Z

    .line 57
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    .line 58
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    .line 59
    iput-boolean v2, p0, Lcom/millennialmedia/internal/MMWebView;->k:Z

    .line 371
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Creating webview "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    if-eqz p2, :cond_1

    :goto_0
    iput-object p2, p0, Lcom/millennialmedia/internal/MMWebView;->e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    .line 376
    const-string v0, "MMWebView"

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/MMWebView;->setTag(Ljava/lang/Object;)V

    .line 378
    if-nez p3, :cond_2

    .line 379
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to create MMWebView instance, specified listener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_1
    invoke-static {}, Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;->getDefault()Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    move-result-object p2

    goto :goto_0

    .line 381
    :cond_2
    iput-object p3, p0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    .line 383
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;->transparent:Z

    if-eqz v0, :cond_3

    .line 384
    invoke-virtual {p0, v2}, Lcom/millennialmedia/internal/MMWebView;->setBackgroundColor(I)V

    .line 387
    :cond_3
    invoke-virtual {p0, v2}, Lcom/millennialmedia/internal/MMWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 388
    invoke-virtual {p0, v2}, Lcom/millennialmedia/internal/MMWebView;->setVerticalScrollBarEnabled(Z)V

    .line 390
    new-instance v0, Landroid/view/GestureDetector;

    .line 391
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/millennialmedia/internal/MMWebView$MMWebViewGestureListener;

    invoke-direct {v4, p3}, Lcom/millennialmedia/internal/MMWebView$MMWebViewGestureListener;-><init>(Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;)V

    invoke-direct {v0, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->g:Landroid/view/GestureDetector;

    .line 393
    new-instance v0, Lcom/millennialmedia/internal/MMWebView$MMWebViewClient;

    invoke-direct {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewClient;-><init>()V

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/MMWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 394
    new-instance v0, Lcom/millennialmedia/internal/MMWebView$MMWebChromeClient;

    invoke-direct {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebChromeClient;-><init>()V

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/MMWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 397
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    .line 398
    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 399
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 400
    const-string v0, "UTF-8"

    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 401
    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 402
    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setGeolocationEnabled(Z)V

    .line 403
    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 405
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v0, v4, :cond_5

    .line 406
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 407
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v4, "Disabling user gesture requirement for media playback"

    invoke-static {v0, v4}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_4
    invoke-virtual {v3, v2}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 413
    :cond_5
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 414
    sget-object v4, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Google security patch is "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    if-eqz v0, :cond_8

    const-string v0, "enabled"

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_6
    sget-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 418
    sget-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 420
    sget-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    invoke-virtual {v3, v0}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 421
    sget-boolean v0, Lcom/millennialmedia/internal/MMWebView;->googleSecurityPatchEnabled:Z

    if-nez v0, :cond_c

    :goto_5
    invoke-virtual {v3, v1}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 424
    new-instance v0, Lcom/millennialmedia/internal/JSBridge;

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    iget-boolean v1, v1, Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;->interstitial:Z

    new-instance v2, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;

    invoke-direct {v2, p0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewJSBridgeListener;-><init>(Lcom/millennialmedia/internal/MMWebView;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/millennialmedia/internal/JSBridge;-><init>(Lcom/millennialmedia/internal/MMWebView;ZLcom/millennialmedia/internal/JSBridge$JSBridgeListener;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    .line 426
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;->enableEnhancedAdControl:Z

    if-eqz v0, :cond_7

    .line 427
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->enableApiCalls()V

    .line 430
    :cond_7
    new-instance v0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    new-instance v1, Lcom/millennialmedia/internal/MMWebView$MMWebViewViewabilityListener;

    invoke-direct {v1}, Lcom/millennialmedia/internal/MMWebView$MMWebViewViewabilityListener;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;-><init>(Landroid/view/View;Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->f:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    .line 431
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->f:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->startWatching()V

    .line 433
    return-void

    .line 414
    :cond_8
    const-string v0, "disabled"

    goto :goto_1

    :cond_9
    move v0, v2

    .line 417
    goto :goto_2

    :cond_a
    move v0, v2

    .line 418
    goto :goto_3

    :cond_b
    move v0, v2

    .line 420
    goto :goto_4

    :cond_c
    move v1, v2

    .line 421
    goto :goto_5
.end method

.method static synthetic a(Lcom/millennialmedia/internal/MMWebView;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/millennialmedia/internal/MMWebView;->d()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 625
    :try_start_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    :goto_0
    return-void

    .line 626
    :catch_0
    move-exception v0

    .line 628
    sget-object v1, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v2, "Error loading url"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/millennialmedia/internal/MMWebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/MMWebView;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/millennialmedia/internal/MMWebView;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/millennialmedia/internal/MMWebView;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/millennialmedia/internal/MMWebView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/MMWebView;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 699
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 700
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 699
    :goto_0
    return v0

    .line 700
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 585
    iget-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->h:Z

    if-eqz v0, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->a()V

    .line 588
    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->onLoaded()V

    .line 594
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 722
    return-void
.end method

.method public varargs callJavascript(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 714
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0, p1, p2}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 717
    :cond_0
    return-void
.end method

.method protected getExtraScriptsToInject()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 728
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 535
    iget-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->k:Z

    if-eqz v0, :cond_0

    .line 536
    const/4 v0, 0x0

    .line 539
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public varargs invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 707
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0, p1, p2}, Lcom/millennialmedia/internal/JSBridge;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 709
    :cond_0
    return-void
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->k:Z

    return v0
.end method

.method public isEnhancedAdControlEnabled()Z
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->areApiCallsEnabled()Z

    move-result v0

    return v0
.end method

.method public isJSBridgeReady()Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->isReady()Z

    move-result v0

    return v0
.end method

.method public loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 612
    iput-object p1, p0, Lcom/millennialmedia/internal/MMWebView;->c:Ljava/lang/String;

    .line 615
    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 619
    :goto_0
    return-void

    .line 616
    :catch_0
    move-exception v0

    .line 617
    sget-object v1, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v2, "Error hit when calling through to loadDataWithBaseUrl"

    invoke-static {v1, v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 636
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 637
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "Url is null or empty"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :cond_0
    :goto_0
    return-void

    .line 644
    :cond_1
    iget-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->k:Z

    if-eqz v0, :cond_2

    .line 645
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "Attempt to load url after webview has been destroyed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 652
    :cond_2
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 653
    iput-object p1, p0, Lcom/millennialmedia/internal/MMWebView;->c:Ljava/lang/String;

    .line 656
    :cond_3
    new-instance v0, Lcom/millennialmedia/internal/MMWebView$2;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/internal/MMWebView$2;-><init>(Lcom/millennialmedia/internal/MMWebView;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 439
    invoke-super {p0}, Landroid/webkit/WebView;->onAttachedToWindow()V

    .line 443
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->e:Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;

    iget-boolean v0, v0, Lcom/millennialmedia/internal/MMWebView$MMWebViewOptions;->enableMoat:Z

    if-eqz v0, :cond_4

    .line 444
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->getActivityForView(Landroid/view/View;)Landroid/app/Activity;

    move-result-object v0

    .line 445
    if-eqz v0, :cond_3

    .line 446
    invoke-static {v0}, Lcom/b/a/a/a/k;->a(Landroid/app/Activity;)Lcom/b/a/a/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->l:Lcom/b/a/a/a/k;

    .line 448
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->m:Lcom/b/a/a/a/x;

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->l:Lcom/b/a/a/a/k;

    invoke-virtual {v0, p0}, Lcom/b/a/a/a/k;->a(Landroid/webkit/WebView;)Lcom/b/a/a/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->m:Lcom/b/a/a/a/x;

    .line 451
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->m:Lcom/b/a/a/a/x;

    invoke-interface {v0}, Lcom/b/a/a/a/x;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 452
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "MOAT tracking initialization failed."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/MMWebView;->getLocationOnScreen([I)V

    .line 472
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 473
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 474
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 476
    :cond_1
    return-void

    .line 456
    :cond_2
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "MOAT tracking enabled for MMWebView."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 461
    :cond_3
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "MOAT disabled. Cannot determine host Activity for Ad."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 464
    :cond_4
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "Moat is not enabled for this MMWebView."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 484
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 488
    :cond_0
    invoke-super {p0}, Landroid/webkit/WebView;->onDetachedFromWindow()V

    .line 489
    return-void
.end method

.method public onNotifyClicked()V
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->onClicked()V

    .line 783
    return-void
.end method

.method public onScrollChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 496
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    invoke-virtual {p0, v0}, Lcom/millennialmedia/internal/MMWebView;->getLocationOnScreen([I)V

    .line 497
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    aget v0, v0, v2

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    aget v1, v1, v2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    aget v0, v0, v3

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    aget v1, v1, v3

    if-ne v0, v1, :cond_1

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    aget v1, v1, v2

    aput v1, v0, v2

    .line 502
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->i:[I

    iget-object v1, p0, Lcom/millennialmedia/internal/MMWebView;->j:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 504
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0, p0}, Lcom/millennialmedia/internal/JSBridge;->b(Lcom/millennialmedia/internal/MMWebView;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 669
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onSizeChanged(IIII)V

    .line 671
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0, p0}, Lcom/millennialmedia/internal/JSBridge;->c(Lcom/millennialmedia/internal/MMWebView;)V

    .line 674
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 685
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->enableApiCalls()V

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->g:Landroid/view/GestureDetector;

    if-eqz v0, :cond_1

    .line 690
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->g:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 693
    :cond_1
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 550
    invoke-static {}, Lcom/millennialmedia/internal/utils/ThreadUtils;->isUiThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    const-string v1, "release must be called on the UI thread"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    :goto_0
    return-void

    .line 556
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    sget-object v0, Lcom/millennialmedia/internal/MMWebView;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Releasing webview "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_1
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 562
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->removeFromParent(Landroid/view/View;)V

    .line 565
    :cond_2
    const-string v0, "about:blank"

    invoke-super {p0, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 566
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->stopLoading()V

    .line 567
    invoke-virtual {p0, v3}, Lcom/millennialmedia/internal/MMWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 568
    invoke-virtual {p0, v3}, Lcom/millennialmedia/internal/MMWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 569
    invoke-virtual {p0}, Lcom/millennialmedia/internal/MMWebView;->destroy()V

    .line 571
    iput-object v3, p0, Lcom/millennialmedia/internal/MMWebView;->g:Landroid/view/GestureDetector;

    .line 573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->k:Z

    goto :goto_0
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 512
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->a:Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;->onFailed()V

    .line 529
    :goto_0
    return-void

    .line 519
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/internal/MMWebView;->h:Z

    .line 520
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 522
    new-instance v1, Lcom/millennialmedia/internal/MMWebView$1;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/internal/MMWebView$1;-><init>(Lcom/millennialmedia/internal/MMWebView;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setStateCollapsed()V
    .locals 1

    .prologue
    .line 758
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setStateCollapsed()V

    .line 761
    :cond_0
    return-void
.end method

.method public setStateExpanded()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setStateExpanded()V

    .line 753
    :cond_0
    return-void
.end method

.method public setStateResized()V
    .locals 1

    .prologue
    .line 734
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 735
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setStateResized()V

    .line 737
    :cond_0
    return-void
.end method

.method public setStateResizing()V
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setStateResizing()V

    .line 777
    :cond_0
    return-void
.end method

.method public setStateUnresized()V
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setStateUnresized()V

    .line 745
    :cond_0
    return-void
.end method

.method public setTwoPartExpand()V
    .locals 1

    .prologue
    .line 766
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/millennialmedia/internal/MMWebView;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->setTwoPartExpand()V

    .line 769
    :cond_0
    return-void
.end method
