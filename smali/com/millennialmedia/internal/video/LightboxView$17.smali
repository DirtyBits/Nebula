.class Lcom/millennialmedia/internal/video/LightboxView$17;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/video/LightboxView;->a(Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;)Lcom/millennialmedia/internal/MMWebView$MMWebViewListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;

.field final synthetic b:Lcom/millennialmedia/internal/video/LightboxView;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/video/LightboxView;Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;)V
    .locals 0

    .prologue
    .line 1427
    iput-object p1, p0, Lcom/millennialmedia/internal/video/LightboxView$17;->b:Lcom/millennialmedia/internal/video/LightboxView;

    iput-object p2, p0, Lcom/millennialmedia/internal/video/LightboxView$17;->a:Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 1478
    return-void
.end method

.method public expand(Lcom/millennialmedia/internal/SizableStateManager$ExpandParams;)Z
    .locals 1

    .prologue
    .line 1464
    const/4 v0, 0x0

    return v0
.end method

.method public onAdLeftApplication()V
    .locals 1

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/millennialmedia/internal/video/LightboxView$17;->a:Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;->onAdLeftApplication()V

    .line 1458
    return-void
.end method

.method public onClicked()V
    .locals 1

    .prologue
    .line 1450
    iget-object v0, p0, Lcom/millennialmedia/internal/video/LightboxView$17;->a:Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;

    invoke-interface {v0}, Lcom/millennialmedia/internal/video/LightboxView$LightboxViewListener;->onClicked()V

    .line 1451
    return-void
.end method

.method public onFailed()V
    .locals 0

    .prologue
    .line 1438
    return-void
.end method

.method public onLoaded()V
    .locals 0

    .prologue
    .line 1432
    return-void
.end method

.method public onReady()V
    .locals 0

    .prologue
    .line 1444
    return-void
.end method

.method public resize(Lcom/millennialmedia/internal/SizableStateManager$ResizeParams;)Z
    .locals 1

    .prologue
    .line 1471
    const/4 v0, 0x0

    return v0
.end method

.method public setOrientation(I)V
    .locals 0

    .prologue
    .line 1490
    return-void
.end method

.method public showCloseIndicator(Z)V
    .locals 0

    .prologue
    .line 1484
    return-void
.end method
