.class Lcom/millennialmedia/internal/video/InlineWebVideoView$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/video/InlineWebVideoView;-><init>(Landroid/content/Context;ZZZZILjava/lang/String;Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lcom/millennialmedia/internal/video/InlineWebVideoView;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;ZZ)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    iput-boolean p2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->a:Z

    iput-boolean p3, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 437
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    .line 439
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->a:Z

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->setAlpha(F)V

    .line 441
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->setVisibility(I)V

    .line 444
    :cond_0
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->b:Z

    if-eqz v0, :cond_1

    .line 445
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setAlpha(F)V

    .line 446
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 449
    :cond_1
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->a:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->b:Z

    if-eqz v0, :cond_3

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;->c:Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-static {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    .line 452
    :cond_3
    return-void
.end method
