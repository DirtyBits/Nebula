.class public Lcom/millennialmedia/internal/video/InlineWebVideoView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;,
        Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebViewViewabilityListener;,
        Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;,
        Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;
    }
.end annotation


# static fields
.field public static final PROGRESS_UPDATES_DISABLED:I = -0x1

.field private static final a:Ljava/lang/String;

.field private static volatile b:I


# instance fields
.field private A:Z

.field private B:Z

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/millennialmedia/internal/MMWebView;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/widget/FrameLayout;

.field private e:Lcom/millennialmedia/internal/video/MMVideoView;

.field private f:Landroid/widget/ImageView;

.field private g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

.field private h:Landroid/widget/ToggleButton;

.field private i:Landroid/net/Uri;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:J

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;

.field private u:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private v:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

.field private w:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    .line 48
    const/16 v0, 0x64

    sput v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZZZILjava/lang/String;Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;)V
    .locals 10

    .prologue
    .line 378
    new-instance v2, Landroid/content/MutableContextWrapper;

    invoke-direct {v2, p1}, Landroid/content/MutableContextWrapper;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    const/4 v2, -0x1

    iput v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->n:I

    .line 64
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->q:J

    .line 66
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    .line 73
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->x:Z

    .line 74
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->y:Z

    .line 75
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->z:Z

    .line 76
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->A:Z

    .line 77
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->B:Z

    .line 380
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->w:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;

    .line 383
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/content/MutableContextWrapper;

    .line 385
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    .line 386
    move/from16 v0, p6

    iput v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->n:I

    .line 387
    iput-boolean p4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->o:Z

    .line 388
    iput-boolean p5, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->p:Z

    .line 390
    new-instance v2, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    new-instance v4, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebViewViewabilityListener;

    invoke-direct {v4}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebViewViewabilityListener;-><init>()V

    invoke-direct {v2, p0, v4}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;-><init>(Landroid/view/View;Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;)V

    iput-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->v:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    .line 391
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->v:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    invoke-virtual {v2}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->startWatching()V

    .line 393
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d:Landroid/widget/FrameLayout;

    .line 395
    const/high16 v2, -0x1000000

    invoke-virtual {p0, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->setBackgroundColor(I)V

    .line 397
    new-instance v2, Lcom/millennialmedia/internal/video/MMVideoView;

    const/4 v6, 0x0

    move v4, p2

    move v5, p3

    move-object v7, p0

    invoke-direct/range {v2 .. v7}, Lcom/millennialmedia/internal/video/MMVideoView;-><init>(Landroid/content/Context;ZZLjava/util/Map;Lcom/millennialmedia/internal/video/MMVideoView$MMVideoViewListener;)V

    iput-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    .line 401
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 403
    const/16 v4, 0x11

    iput v4, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 405
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 406
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MMInlineWebVideoView_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b:I

    add-int/lit8 v5, v4, 0x1

    sput v5, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->setTag(Ljava/lang/Object;)V

    .line 408
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 411
    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->f:Landroid/widget/ImageView;

    .line 412
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->f:Landroid/widget/ImageView;

    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 413
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 414
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 416
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 419
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v4, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 421
    new-instance v4, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    iget-object v7, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    move-object v5, p0

    move-object v6, v3

    move v8, p2

    move v9, p3

    invoke-direct/range {v4 .. v9}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;Landroid/content/Context;Lcom/millennialmedia/internal/video/MMVideoView;ZZ)V

    iput-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    .line 422
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 425
    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 427
    if-nez p4, :cond_0

    .line 428
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->setVisibility(I)V

    .line 430
    :cond_0
    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {p0, v4, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 432
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v2, v4}, Lcom/millennialmedia/internal/video/MMVideoView;->setMediaController(Lcom/millennialmedia/internal/video/MMVideoView$MediaController;)V

    .line 433
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    new-instance v4, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;

    invoke-direct {v4, p0, p4, p5}, Lcom/millennialmedia/internal/video/InlineWebVideoView$1;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;ZZ)V

    invoke-virtual {v2, v4}, Lcom/millennialmedia/internal/video/MMVideoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    new-instance v4, Lcom/millennialmedia/internal/video/InlineWebVideoView$2;

    invoke-direct {v4, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$2;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-virtual {v2, v4}, Lcom/millennialmedia/internal/video/MMVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 472
    new-instance v2, Landroid/widget/ToggleButton;

    invoke-direct {v2, v3}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    .line 473
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 474
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 476
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    .line 477
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/millennialmedia/R$drawable;->mmadsdk_expand_collapse:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 479
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    new-instance v3, Lcom/millennialmedia/internal/video/InlineWebVideoView$3;

    invoke-direct {v3, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$3;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 491
    if-nez p5, :cond_1

    .line 492
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 495
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b(Z)Landroid/graphics/Rect;

    move-result-object v2

    .line 496
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 497
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-direct {v3, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 499
    const/16 v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 500
    const/16 v2, 0xb

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 501
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    invoke-virtual {p0, v2, v3}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 502
    return-void
.end method

.method private a(Landroid/util/DisplayMetrics;I)I
    .locals 2

    .prologue
    .line 1272
    int-to-float v0, p2

    iget v1, p1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/InlineWebVideoView;Z)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b(Z)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/millennialmedia/internal/MMWebView;)V
    .locals 5

    .prologue
    .line 903
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 904
    new-instance v0, Landroid/widget/AbsoluteLayout$LayoutParams;

    iget v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->l:I

    iget v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->m:I

    iget v3, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->j:I

    iget v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->k:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 905
    invoke-static {p1, p0, v0}, Lcom/millennialmedia/internal/utils/ViewUtils;->attachView(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 906
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->t:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;

    invoke-interface {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;->attachSucceeded(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    .line 908
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d()V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/internal/video/InlineWebVideoView;Lcom/millennialmedia/internal/MMWebView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Lcom/millennialmedia/internal/MMWebView;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->a(Z)V

    .line 1239
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b(Z)Landroid/graphics/Rect;

    move-result-object v1

    .line 1241
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    .line 1242
    invoke-virtual {v0}, Landroid/widget/ToggleButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1244
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1245
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1246
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1247
    return-void
.end method

.method private b(Z)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1253
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/millennialmedia/R$dimen;->mmadsdk_control_button_max_width_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1255
    if-eqz p1, :cond_0

    .line 1256
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1265
    :goto_0
    return-object v0

    .line 1258
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/millennialmedia/R$dimen;->mmadsdk_control_button_min_width_height:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1262
    iget v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->m:I

    div-int/lit8 v2, v2, 0x5

    .line 1263
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1265
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 662
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 663
    new-instance v0, Lcom/millennialmedia/internal/MMActivity$MMActivityConfig;

    invoke-direct {v0}, Lcom/millennialmedia/internal/MMActivity$MMActivityConfig;-><init>()V

    .line 665
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/millennialmedia/internal/video/InlineWebVideoView$5;

    invoke-direct {v2, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$5;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-static {v1, v0, v2}, Lcom/millennialmedia/internal/MMActivity;->launch(Landroid/content/Context;Lcom/millennialmedia/internal/MMActivity$MMActivityConfig;Lcom/millennialmedia/internal/MMActivity$MMActivityListener;)V

    .line 755
    :cond_0
    :goto_0
    return-void

    .line 751
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 752
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.expandToFullScreen could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/millennialmedia/internal/video/InlineWebVideoView;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Lcom/millennialmedia/internal/video/MMVideoView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 1150
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->o:Z

    if-eqz v0, :cond_2

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->u:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 1152
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->u:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1155
    :cond_1
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$10;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$10;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    const-wide/16 v2, 0x9c4

    invoke-static {v0, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->u:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1219
    :cond_2
    return-void
.end method

.method static synthetic d(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/widget/ToggleButton;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1278
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$12;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$12;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 1287
    return-void
.end method

.method static synthetic e(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c()V

    return-void
.end method

.method static synthetic f(Lcom/millennialmedia/internal/video/InlineWebVideoView;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->j:I

    return v0
.end method

.method static synthetic g(Lcom/millennialmedia/internal/video/InlineWebVideoView;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->k:I

    return v0
.end method

.method static synthetic h(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic i(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->b()V

    return-void
.end method

.method static synthetic k(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic l(Lcom/millennialmedia/internal/video/InlineWebVideoView;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->l:I

    return v0
.end method

.method static synthetic m(Lcom/millennialmedia/internal/video/InlineWebVideoView;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->m:I

    return v0
.end method

.method static synthetic n(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->i:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic o(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->d:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic p(Lcom/millennialmedia/internal/video/InlineWebVideoView;)Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->w:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewListener;

    return-object v0
.end method

.method private setKeepScreenOnUiThread(Z)V
    .locals 1

    .prologue
    .line 1224
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$11;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView$11;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;Z)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1232
    return-void
.end method


# virtual methods
.method public expandToFullScreen()V
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->h:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 657
    return-void
.end method

.method public mute()V
    .locals 2

    .prologue
    .line 778
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 779
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->mute()V

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 781
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.mute could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBufferingUpdate(Lcom/millennialmedia/internal/video/MMVideoView;I)V
    .locals 0

    .prologue
    .line 1144
    return-void
.end method

.method public onComplete(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1003
    invoke-virtual {p1, v6}, Lcom/millennialmedia/internal/video/MMVideoView;->seekTo(I)V

    .line 1005
    invoke-direct {p0, v6}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->setKeepScreenOnUiThread(Z)V

    .line 1007
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1008
    if-eqz v0, :cond_3

    .line 1009
    monitor-enter p0

    .line 1010
    :try_start_0
    iget-boolean v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->B:Z

    if-nez v1, :cond_1

    .line 1011
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->B:Z

    .line 1012
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1013
    sget-object v1, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v2, "InlineVideoView[%s]: firing end event"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1016
    :cond_0
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "timeUpdate"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/millennialmedia/internal/video/MMVideoView;->getDuration()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1017
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "tracking"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "end"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1019
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1021
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, "stateChange"

    aput-object v3, v2, v7

    const-string v3, "complete"

    aput-object v3, v2, v8

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1029
    :cond_2
    :goto_0
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$9;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$9;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1038
    return-void

    .line 1019
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1024
    :cond_3
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1025
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineVideoView anchor WebView is gone.  Tracking events disabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onError(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1125
    iput-boolean v5, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    .line 1127
    invoke-direct {p0, v4}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->setKeepScreenOnUiThread(Z)V

    .line 1129
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1130
    if-eqz v0, :cond_0

    .line 1131
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "error"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "Inline video play back failed."

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1135
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1136
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->t:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;

    invoke-interface {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;->attachFailed(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    .line 1138
    :cond_1
    return-void
.end method

.method public onMuted(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1105
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1106
    if-eqz v0, :cond_0

    .line 1107
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "mute"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1109
    :cond_0
    return-void
.end method

.method public onPause(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 989
    invoke-direct {p0, v4}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->setKeepScreenOnUiThread(Z)V

    .line 990
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 991
    if-eqz v0, :cond_0

    .line 992
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "stateChange"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "paused"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 994
    :cond_0
    return-void
.end method

.method public onPrepared(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 1

    .prologue
    .line 872
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_0

    .line 873
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$6;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView$6;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;Lcom/millennialmedia/internal/video/MMVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 898
    :cond_0
    return-void
.end method

.method public declared-synchronized onProgress(Lcom/millennialmedia/internal/video/MMVideoView;I)V
    .locals 8

    .prologue
    .line 1044
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1045
    if-eqz v0, :cond_7

    .line 1046
    invoke-virtual {p1}, Lcom/millennialmedia/internal/video/MMVideoView;->getDuration()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    .line 1048
    iget-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->y:Z

    if-nez v2, :cond_1

    if-lt p2, v1, :cond_1

    .line 1049
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1050
    sget-object v2, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v3, "InlineVideoView[%s]: firing q1 event"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->y:Z

    .line 1053
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "tracking"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "q1"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1056
    :cond_1
    iget-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->z:Z

    if-nez v2, :cond_3

    mul-int/lit8 v2, v1, 0x2

    if-lt p2, v2, :cond_3

    .line 1057
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1058
    sget-object v2, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v3, "InlineVideoView[%s]: firing midpoint event"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->z:Z

    .line 1061
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "tracking"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "q2"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1064
    :cond_3
    iget-boolean v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->A:Z

    if-nez v2, :cond_5

    mul-int/lit8 v1, v1, 0x3

    if-lt p2, v1, :cond_5

    .line 1065
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1066
    sget-object v1, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v2, "InlineVideoView[%s]: firing q3 event"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->A:Z

    .line 1069
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "tracking"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "q3"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1072
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1073
    iget v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->n:I

    const/4 v4, -0x1

    if-eq v1, v4, :cond_6

    iget-wide v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->q:J

    sub-long v4, v2, v4

    iget v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->n:I

    int-to-long v6, v1

    cmp-long v1, v4, v6

    if-ltz v1, :cond_6

    .line 1076
    iput-wide v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->q:J

    .line 1078
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "timeUpdate"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1086
    :cond_6
    :goto_0
    monitor-exit p0

    return-void

    .line 1082
    :cond_7
    :try_start_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1083
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineVideoView anchor WebView is gone.  Tracking events disabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1044
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onReadyToStart(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 914
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 915
    if-eqz v0, :cond_0

    .line 916
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "stateChange"

    aput-object v3, v2, v5

    const-string v3, "readyToStart"

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 919
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "updateVideoURL"

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->i:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 922
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "durationChange"

    aput-object v3, v2, v5

    invoke-virtual {p1}, Lcom/millennialmedia/internal/video/MMVideoView;->getDuration()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 924
    :cond_0
    return-void
.end method

.method public onSeek(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 5

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1093
    if-eqz v0, :cond_0

    .line 1097
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "seek"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/millennialmedia/internal/video/MMVideoView;->getCurrentPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1099
    :cond_0
    return-void
.end method

.method public onStart(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 930
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$7;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$7;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 939
    invoke-direct {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c()V

    .line 941
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 942
    if-eqz v0, :cond_3

    .line 943
    monitor-enter p0

    .line 944
    :try_start_0
    iget-boolean v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->x:Z

    if-nez v1, :cond_1

    .line 945
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->x:Z

    .line 946
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 947
    sget-object v1, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "InlineWebVideoView["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: firing start event"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    :cond_0
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "tracking"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "start"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 953
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 955
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    new-array v2, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v6

    const-string v3, "stateChange"

    aput-object v3, v2, v5

    const-string v3, "playing"

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 962
    :cond_2
    :goto_0
    return-void

    .line 953
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 958
    :cond_3
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 959
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView anchor WebView is gone.  Tracking events disabled."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 5

    .prologue
    .line 968
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$8;

    invoke-direct {v0, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$8;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 979
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 980
    if-eqz v0, :cond_0

    .line 981
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "stateChange"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "stopped"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 983
    :cond_0
    return-void
.end method

.method public onUnmuted(Lcom/millennialmedia/internal/video/MMVideoView;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1115
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 1116
    if-eqz v0, :cond_0

    .line 1117
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "mute"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1119
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 631
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->pause()V

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.pause could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/MMVideoView;->release()V

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    .line 511
    :cond_0
    return-void
.end method

.method public remove()V
    .locals 5

    .prologue
    .line 804
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/MMVideoView;->stop()V

    .line 808
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 809
    if-eqz v0, :cond_1

    .line 810
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "stateChange"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "removed"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 813
    :cond_1
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->removeFromParent(Landroid/view/View;)V

    .line 814
    return-void
.end method

.method public reposition(IIII)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 820
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_6

    .line 821
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    if-gez p4, :cond_2

    .line 822
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "All position parameters must be greater than or equal to zero."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_1
    :goto_0
    return-void

    .line 827
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_5

    .line 828
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 829
    if-eqz v0, :cond_4

    .line 830
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->getWidth()I

    move-result v1

    sub-int/2addr v1, p1

    if-lt v1, p3, :cond_3

    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->getHeight()I

    move-result v1

    sub-int/2addr v1, p2

    if-lt v1, p4, :cond_3

    .line 833
    iput p3, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->l:I

    .line 834
    iput p4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->m:I

    .line 835
    iput p1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->j:I

    .line 836
    iput p2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->k:I

    .line 838
    invoke-direct {p0, v5}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Z)V

    .line 840
    new-instance v1, Landroid/widget/AbsoluteLayout$LayoutParams;

    invoke-direct {v1, p3, p4, p1, p2}, Landroid/widget/AbsoluteLayout$LayoutParams;-><init>(IIII)V

    .line 842
    invoke-static {p0}, Lcom/millennialmedia/internal/utils/ViewUtils;->removeFromParent(Landroid/view/View;)V

    .line 843
    invoke-static {v0, p0, v1}, Lcom/millennialmedia/internal/utils/ViewUtils;->attachView(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 845
    invoke-virtual {v0}, Lcom/millennialmedia/internal/MMWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 846
    iget-object v2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const-string v5, "reposition"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-direct {p0, v1, p3}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 847
    invoke-direct {p0, v1, p4}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-direct {p0, v1, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-direct {p0, v1, p2}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 846
    invoke-virtual {v0, v2, v3}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 850
    :cond_3
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "Cannot reposition the inline video as it will not fit within the anchor view."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 854
    :cond_4
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "Cannot position the InlineVideoView because the anchor view is gone."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 858
    :cond_5
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "Cannot position the InlineVideoView because the anchor view has not been set."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 862
    :cond_6
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.reposition could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public seekTo(I)V
    .locals 2

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/video/MMVideoView;->seekTo(I)V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 646
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 647
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.seekTo could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAnchorView(Lcom/millennialmedia/internal/MMWebView;IIIILcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;)V
    .locals 2

    .prologue
    .line 517
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    if-ltz p4, :cond_0

    if-gez p5, :cond_2

    .line 518
    :cond_0
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "All position parameters must be greater than or equal to zero."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-interface {p6, p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;->attachFailed(Lcom/millennialmedia/internal/video/InlineWebVideoView;)V

    .line 537
    :cond_1
    :goto_0
    return-void

    .line 524
    :cond_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    .line 525
    iput-object p6, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->t:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineWebVideoViewAttachListener;

    .line 526
    iput p2, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->j:I

    .line 527
    iput p3, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->k:I

    .line 528
    iput p4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->l:I

    .line 529
    iput p5, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->m:I

    .line 532
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a(Z)V

    .line 534
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->i:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_1

    .line 535
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/video/MMVideoView;->setVideoURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public setPlaceholder(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 544
    new-instance v0, Lcom/millennialmedia/internal/video/InlineWebVideoView$4;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/internal/video/InlineWebVideoView$4;-><init>(Lcom/millennialmedia/internal/video/InlineWebVideoView;Landroid/net/Uri;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 573
    :cond_0
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 579
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    .line 580
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->x:Z

    .line 581
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->y:Z

    .line 582
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->z:Z

    .line 583
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->A:Z

    .line 584
    iput-boolean v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->B:Z

    .line 586
    iput-object p1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->i:Landroid/net/Uri;

    .line 588
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/video/MMVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 591
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 592
    if-eqz v0, :cond_0

    .line 595
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "stateChange"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "loading"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 598
    :cond_0
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->start()V

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.start could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 617
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v0, :cond_1

    .line 618
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/MMVideoView;->stop()V

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.stop could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public triggerTimeUpdate()V
    .locals 5

    .prologue
    .line 761
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 762
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 763
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    if-eqz v1, :cond_0

    .line 764
    iget-object v1, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->r:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/millennialmedia/internal/video/InlineWebVideoView;->getTag()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "timeUpdate"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->e:Lcom/millennialmedia/internal/video/MMVideoView;

    invoke-virtual {v4}, Lcom/millennialmedia/internal/video/MMVideoView;->getCurrentPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/millennialmedia/internal/MMWebView;->invokeCallback(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.triggerTimeUpdate could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unmute()V
    .locals 2

    .prologue
    .line 791
    iget-boolean v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->s:Z

    if-nez v0, :cond_1

    .line 792
    iget-object v0, p0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->g:Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/InlineWebVideoView$InlineVideoControls;->unmute()V

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 795
    sget-object v0, Lcom/millennialmedia/internal/video/InlineWebVideoView;->a:Ljava/lang/String;

    const-string v1, "InlineWebVideoView.unmute could not complete because of a previous error."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
