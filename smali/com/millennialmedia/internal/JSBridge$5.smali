.class Lcom/millennialmedia/internal/JSBridge$5;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/internal/JSBridge;->c(Lcom/millennialmedia/internal/MMWebView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/MMWebView;

.field final synthetic b:Lcom/millennialmedia/internal/JSBridge;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/JSBridge;Lcom/millennialmedia/internal/MMWebView;)V
    .locals 0

    .prologue
    .line 2279
    iput-object p1, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    iput-object p2, p0, Lcom/millennialmedia/internal/JSBridge$5;->a:Lcom/millennialmedia/internal/MMWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2283
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$5;->a:Lcom/millennialmedia/internal/MMWebView;

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/MMWebView;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2284
    if-nez v0, :cond_1

    .line 2309
    :cond_0
    :goto_0
    return-void

    .line 2288
    :cond_1
    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    iget-boolean v1, v1, Lcom/millennialmedia/internal/JSBridge;->h:Z

    if-eqz v1, :cond_2

    .line 2289
    iget-object v1, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    iget-boolean v1, v1, Lcom/millennialmedia/internal/JSBridge;->g:Z

    if-nez v1, :cond_0

    .line 2291
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2292
    const-string v2, "currentPosition"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2293
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    const-string v2, "MmJsBridge.mraid.setPositions"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/millennialmedia/internal/JSBridge;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2295
    :catch_0
    move-exception v0

    .line 2296
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error creating json object in setCurrentPosition"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2302
    :cond_2
    const-string v1, "width"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 2303
    const-string v2, "height"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 2304
    if-lez v1, :cond_0

    if-lez v0, :cond_0

    .line 2305
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    iput-boolean v4, v0, Lcom/millennialmedia/internal/JSBridge;->k:Z

    .line 2306
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$5;->b:Lcom/millennialmedia/internal/JSBridge;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/JSBridge;->b()V

    goto :goto_0
.end method
