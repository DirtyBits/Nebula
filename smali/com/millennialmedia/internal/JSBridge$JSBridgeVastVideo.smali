.class Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/internal/JSBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "JSBridgeVastVideo"
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/JSBridge;


# direct methods
.method constructor <init>(Lcom/millennialmedia/internal/JSBridge;)V
    .locals 0

    .prologue
    .line 1997
    iput-object p1, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public close(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2028
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2029
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2030
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2031
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->close()V

    .line 2035
    :goto_0
    return-void

    .line 2033
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Close cannot be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pause(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2016
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2017
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2018
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->pause()V

    .line 2022
    :goto_0
    return-void

    .line 2020
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Pause cannot be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public play(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2002
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2003
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2004
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2005
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->play()V

    .line 2009
    :goto_0
    return-void

    .line 2007
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Play cannot be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public restart(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2052
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2053
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2054
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2055
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->restart()V

    .line 2059
    :goto_0
    return-void

    .line 2057
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Restart cannot be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public seek(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2066
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2067
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "seekTime"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2068
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2069
    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->seek(I)V

    .line 2073
    :goto_0
    return-void

    .line 2071
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Seek cannot be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTimeInterval(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2093
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2094
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2095
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "timeInterval"

    const/4 v3, -0x1

    .line 2096
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 2098
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2099
    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->setTimeInterval(I)V

    .line 2104
    :goto_0
    return-void

    .line 2101
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SetTimeInterval can\'t be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public skip(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2041
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2042
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2043
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2044
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->skip()V

    .line 2046
    :cond_0
    return-void
.end method

.method public triggerTimeUpdate(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 2079
    iget-object v0, p0, Lcom/millennialmedia/internal/JSBridge$JSBridgeVastVideo;->a:Lcom/millennialmedia/internal/JSBridge;

    invoke-static {v0}, Lcom/millennialmedia/internal/JSBridge;->a(Lcom/millennialmedia/internal/JSBridge;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/MMWebView;

    .line 2080
    instance-of v1, v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    if-eqz v1, :cond_0

    .line 2081
    check-cast v0, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;

    .line 2082
    invoke-virtual {v0}, Lcom/millennialmedia/internal/video/VASTVideoView$VASTVideoWebView;->triggerTimeUpdate()V

    .line 2087
    :goto_0
    return-void

    .line 2084
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/JSBridge;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TriggerTimeUpdate can\'t be called on a WebView that is not part of a VAST Video creative."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
