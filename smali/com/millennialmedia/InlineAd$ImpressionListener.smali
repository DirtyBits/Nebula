.class Lcom/millennialmedia/InlineAd$ImpressionListener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/millennialmedia/InlineAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ImpressionListener"
.end annotation


# instance fields
.field a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

.field b:I

.field c:J

.field volatile d:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field volatile e:Z

.field f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/millennialmedia/InlineAd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/millennialmedia/InlineAd;Landroid/view/View;JI)V
    .locals 3

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->e:Z

    .line 320
    iput p5, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->b:I

    .line 321
    if-nez p5, :cond_0

    const-wide/16 p3, 0x0

    :cond_0
    iput-wide p3, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->c:J

    .line 323
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->f:Ljava/lang/ref/WeakReference;

    .line 325
    new-instance v0, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    new-instance v1, Lcom/millennialmedia/InlineAd$ImpressionListener$1;

    invoke-direct {v1, p0}, Lcom/millennialmedia/InlineAd$ImpressionListener$1;-><init>(Lcom/millennialmedia/InlineAd$ImpressionListener;)V

    invoke-direct {v0, p2, v1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;-><init>(Landroid/view/View;Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityListener;)V

    iput-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    .line 367
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    iget v1, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->b:I

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->setMinViewabilityPercent(I)V

    .line 377
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->startWatching()V

    goto :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 383
    monitor-enter p0

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->a:Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/utils/ViewUtils$ViewabilityWatcher;->stopWatching()V

    .line 385
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->d:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->d:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/InlineAd$ImpressionListener;->d:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 389
    :cond_0
    monitor-exit p0

    .line 390
    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
