.class public Lcom/millennialmedia/NativeAd;
.super Lcom/millennialmedia/internal/AdPlacement;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/NativeAd$ExpirationRunnable;,
        Lcom/millennialmedia/NativeAd$ImpressionReporter;,
        Lcom/millennialmedia/NativeAd$NativeAdMetadata;,
        Lcom/millennialmedia/NativeAd$NativeErrorStatus;,
        Lcom/millennialmedia/NativeAd$NativeListener;,
        Lcom/millennialmedia/NativeAd$ComponentName;
    }
.end annotation


# static fields
.field public static final COMPONENT_ID_BODY:Ljava/lang/String; = "body"

.field public static final COMPONENT_ID_CALL_TO_ACTION:Ljava/lang/String; = "callToAction"

.field public static final COMPONENT_ID_DISCLAIMER:Ljava/lang/String; = "disclaimer"

.field public static final COMPONENT_ID_ICON_IMAGE:Ljava/lang/String; = "iconImage"

.field public static final COMPONENT_ID_MAIN_IMAGE:Ljava/lang/String; = "mainImage"

.field public static final COMPONENT_ID_RATING:Ljava/lang/String; = "rating"

.field public static final COMPONENT_ID_TITLE:Ljava/lang/String; = "title"

.field public static final NATIVE_TYPE_INLINE:Ljava/lang/String; = "inline"

.field private static final e:Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/millennialmedia/NativeAd$NativeListener;

.field private h:Z

.field private i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

.field public loadedComponents:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Z

.field private volatile n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

.field private volatile o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

.field private p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/millennialmedia/NativeAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 402
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/AdPlacement;-><init>(Ljava/lang/String;)V

    .line 76
    iput-boolean v0, p0, Lcom/millennialmedia/NativeAd;->h:Z

    .line 81
    iput-boolean v0, p0, Lcom/millennialmedia/NativeAd;->m:Z

    .line 97
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    .line 122
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    .line 404
    if-eqz p2, :cond_0

    array-length v1, p2

    if-eqz v1, :cond_0

    aget-object v1, p2, v0

    if-eqz v1, :cond_0

    aget-object v1, p2, v0

    .line 405
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    :cond_0
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "Unable to create native ad, nativeTypes is required"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 413
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getNativeTypeDefinitions()Ljava/util/Map;

    move-result-object v5

    .line 415
    array-length v6, p2

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_4

    aget-object v7, p2, v3

    .line 416
    const/4 v2, 0x0

    .line 418
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 419
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 420
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->typeName:Ljava/lang/String;

    .line 422
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 429
    :goto_1
    if-eqz v1, :cond_3

    .line 430
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 432
    :cond_3
    new-instance v0, Lcom/millennialmedia/MMException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to load native ad, specified native type <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> is not recognized"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_4
    iput-object v4, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    .line 438
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method private static a(Lcom/millennialmedia/internal/adadapters/AdAdapter;)J
    .locals 2

    .prologue
    .line 2183
    instance-of v0, p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 2184
    check-cast p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {p0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->getImpressionDelay()J

    move-result-wide v0

    .line 2189
    :goto_0
    return-wide v0

    .line 2186
    :cond_0
    const-wide/16 v0, 0x3e8

    goto :goto_0
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/PlayList;)Lcom/millennialmedia/internal/PlayList;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->b:Lcom/millennialmedia/internal/PlayList;

    return-object p1
.end method

.method private a(Lcom/millennialmedia/NativeAd$ComponentName;I)Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1436
    .line 1438
    sget-object v0, Lcom/millennialmedia/NativeAd$ComponentName;->CALL_TO_ACTION:Lcom/millennialmedia/NativeAd$ComponentName;

    if-ne p1, v0, :cond_1

    .line 1439
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    .line 1446
    :goto_0
    if-nez v0, :cond_3

    .line 1447
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "Unable to get component info for component name <%s> and instance id <%d>, did not find component info list"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    .line 1449
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    .line 1447
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1476
    :cond_0
    :goto_1
    return-object v0

    .line 1440
    :cond_1
    sget-object v0, Lcom/millennialmedia/NativeAd$ComponentName;->ICON_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    if-ne p1, v0, :cond_2

    .line 1441
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    goto :goto_0

    .line 1442
    :cond_2
    sget-object v0, Lcom/millennialmedia/NativeAd$ComponentName;->MAIN_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    if-ne p1, v0, :cond_6

    .line 1443
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    goto :goto_0

    .line 1454
    :cond_3
    if-ge p2, v6, :cond_4

    .line 1455
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get component info for component name <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "> and instance id <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">, instance id must be greater than 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1458
    goto :goto_1

    .line 1461
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, p2, :cond_5

    .line 1462
    sget-object v2, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to get component info for component name <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "> and instance id <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">, only <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1463
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "> instances found"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1462
    invoke-static {v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1465
    goto :goto_1

    .line 1468
    :cond_5
    add-int/lit8 v2, p2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    .line 1469
    if-nez v0, :cond_0

    .line 1470
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to get component info for component name <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "> and instance id <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">, found value is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1473
    goto/16 :goto_1

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    return-object p1
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1482
    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    .line 1483
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to retrieve the requested <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "> instance, instance value must be 1 or greater"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1500
    :goto_0
    return-object v0

    .line 1489
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1490
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, p1, :cond_1

    .line 1491
    sget-object v2, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to retrieve the requested <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "> instance <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">, only <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1493
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "> instances available"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1491
    invoke-static {v2, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1495
    goto :goto_0

    .line 1498
    :cond_1
    invoke-direct {p0, p2, p1}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;I)V

    .line 1500
    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    return-object p1
.end method

.method private a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1900
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1901
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const/16 v0, 0x384

    if-gt v1, v0, :cond_1

    .line 1902
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 1904
    if-eqz v0, :cond_1

    .line 1905
    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 1906
    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1901
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1908
    :cond_0
    new-instance v1, Lcom/millennialmedia/MMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected View with tag = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to be a TextView."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1916
    :cond_1
    return-object v2
.end method

.method private a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V
    .locals 6

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v2

    .line 1512
    new-instance v0, Lcom/millennialmedia/NativeAd$6;

    move-object v1, p0

    move-object v3, p4

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd$6;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;Lcom/millennialmedia/NativeAd$ComponentName;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1547
    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 0

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacementReporter;I)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/adadapters/NativeAdapter;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->b(Lcom/millennialmedia/internal/adadapters/NativeAdapter;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 6

    .prologue
    .line 579
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->copy()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v1

    .line 581
    monitor-enter p0

    .line 583
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    monitor-exit p0

    .line 732
    :goto_0
    return-void

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v2, "play_list_loaded"

    .line 588
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v2, "ad_adapter_load_failed"

    .line 589
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 591
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 603
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 594
    :cond_2
    :try_start_1
    const-string v0, "loading_ad_adapter"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 601
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getItemHash()I

    .line 602
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 603
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/PlayList;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 606
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 607
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to find ad adapter in play list"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_3
    invoke-direct {p0, v1}, Lcom/millennialmedia/NativeAd;->e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0

    .line 615
    :cond_4
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->getPlayListItemReporter(Lcom/millennialmedia/internal/AdPlacementReporter;)Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    move-result-object v2

    .line 619
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0, p0, v2}, Lcom/millennialmedia/internal/PlayList;->getNextAdAdapter(Lcom/millennialmedia/internal/AdPlacement;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)Lcom/millennialmedia/internal/adadapters/AdAdapter;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    .line 621
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 624
    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    .line 626
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    iget v0, v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->requestTimeout:I

    .line 627
    if-lez v0, :cond_6

    .line 628
    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v3, :cond_5

    .line 629
    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v3}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 634
    :cond_5
    new-instance v3, Lcom/millennialmedia/NativeAd$3;

    invoke-direct {v3, p0, v1, v2}, Lcom/millennialmedia/NativeAd$3;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    int-to-long v4, v0

    invoke-static {v3, v4, v5}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 652
    :cond_6
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    new-instance v3, Lcom/millennialmedia/NativeAd$4;

    invoke-direct {v3, p0, v1, v2}, Lcom/millennialmedia/NativeAd$4;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    invoke-virtual {v0, v3}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->init(Lcom/millennialmedia/internal/adadapters/NativeAdapter$NativeAdapterListener;)V

    goto/16 :goto_0

    .line 729
    :cond_7
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    .line 730
    invoke-direct {p0, v1}, Lcom/millennialmedia/NativeAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacementReporter;I)V
    .locals 1

    .prologue
    .line 1047
    iget-boolean v0, p0, Lcom/millennialmedia/NativeAd;->m:Z

    if-nez v0, :cond_0

    .line 1048
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/NativeAd;->m:Z

    .line 1050
    invoke-static {p1, p2}, Lcom/millennialmedia/internal/AdPlacementReporter;->setDisplayed(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    .line 1051
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getImpressionTrackers()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/millennialmedia/NativeAd;->a(Ljava/util/List;)V

    .line 1054
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->g()V

    .line 1056
    :cond_0
    return-void
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V
    .locals 1

    .prologue
    .line 1063
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    .line 1067
    invoke-static {p1}, Lcom/millennialmedia/internal/AdPlacementReporter;->setClicked(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 1072
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;->clickTrackerUrls:Ljava/util/List;

    if-nez v0, :cond_1

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getClickTrackers()Ljava/util/List;

    move-result-object v0

    .line 1078
    :goto_0
    invoke-direct {p0, v0}, Lcom/millennialmedia/NativeAd;->a(Ljava/util/List;)V

    .line 1079
    return-void

    .line 1075
    :cond_1
    iget-object v0, p2, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;->clickTrackerUrls:Ljava/util/List;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1554
    if-nez v0, :cond_0

    .line 1555
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1556
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1559
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1560
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/NativeAd$ComponentName;",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 838
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 839
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 840
    if-eqz v0, :cond_1

    .line 841
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 842
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;

    .line 843
    if-nez v1, :cond_0

    .line 841
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 847
    :cond_0
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 848
    iget-object v5, v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 849
    invoke-direct {p0, v4, p2, v2, v1}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 851
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 855
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 994
    if-nez p2, :cond_1

    .line 995
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to invoke "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " action, url is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1001
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1003
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 1004
    if-eqz v0, :cond_0

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/utils/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1005
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->h()V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1084
    if-eqz p1, :cond_0

    .line 1085
    new-instance v0, Lcom/millennialmedia/NativeAd$5;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/NativeAd$5;-><init>(Lcom/millennialmedia/NativeAd;Ljava/util/List;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThread(Ljava/lang/Runnable;)V

    .line 1101
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/NativeAd$ComponentName;",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1824
    if-nez p1, :cond_1

    .line 1850
    :cond_0
    return-void

    .line 1828
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1831
    if-eqz p5, :cond_2

    .line 1832
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    move v3, v1

    .line 1837
    :goto_0
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v3, :cond_0

    .line 1838
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1840
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_3

    .line 1841
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1842
    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1843
    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    invoke-direct {p0, v1, p3, v4, v2}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 1837
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1834
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v3, v1

    goto :goto_0

    .line 1846
    :cond_3
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1847
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private a(Landroid/view/View;ZZ)Z
    .locals 12

    .prologue
    .line 1695
    const/4 v3, 0x0

    .line 1696
    const/4 v2, 0x1

    .line 1700
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1702
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->componentDefinitions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1705
    iget-object v5, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->componentId:Ljava/lang/String;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1708
    :cond_0
    const-string v0, "body"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1709
    const-string v0, "body"

    .line 1710
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1712
    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_1

    .line 1713
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the body component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1714
    const/4 v0, 0x0

    move v2, v0

    .line 1717
    :cond_1
    const-string v0, "callToAction"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 1718
    const-string v0, "callToAction"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1719
    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_2

    .line 1720
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the \'Call To Action\' component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1721
    const/4 v2, 0x0

    .line 1724
    :cond_2
    const-string v0, "disclaimer"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 1725
    const-string v0, "disclaimer"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1726
    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_3

    .line 1727
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the Disclaimer component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    const/4 v2, 0x0

    .line 1731
    :cond_3
    const-string v0, "iconImage"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->b(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 1732
    const-string v0, "iconImage"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1733
    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_4

    .line 1734
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the \'Icon Image\' component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1735
    const/4 v2, 0x0

    .line 1738
    :cond_4
    const-string v0, "mainImage"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->b(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 1739
    const-string v0, "mainImage"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1740
    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_5

    .line 1741
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the \'Main Image\' component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1742
    const/4 v2, 0x0

    .line 1745
    :cond_5
    const-string v0, "rating"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 1746
    const-string v0, "rating"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1747
    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v5

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v5, v0, :cond_6

    .line 1748
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the Rating component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1749
    const/4 v2, 0x0

    .line 1752
    :cond_6
    const-string v0, "title"

    invoke-direct {p0, p1, v0}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    .line 1753
    const-string v0, "title"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1754
    if-eqz v0, :cond_7

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v4

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v4, v0, :cond_7

    .line 1755
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Layout does not contain the required number of Views for the Title component."

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1756
    const/4 v2, 0x0

    .line 1759
    :cond_7
    if-eqz v2, :cond_b

    .line 1761
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1762
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1763
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1764
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->v:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1765
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1766
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    .line 1767
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_a

    const/4 v0, 0x1

    .line 1769
    :goto_1
    if-nez v0, :cond_8

    if-nez p2, :cond_c

    .line 1771
    :cond_8
    const-string v2, "body"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->BODY:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->s:Ljava/util/List;

    move-object v0, p0

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1773
    const-string v2, "disclaimer"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->DISCLAIMER:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    move-object v0, p0

    move-object v1, v7

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1776
    const-string v2, "rating"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->RATING:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->u:Ljava/util/List;

    move-object v0, p0

    move-object v1, v10

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1777
    const-string v2, "title"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->TITLE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->v:Ljava/util/List;

    move-object v0, p0

    move-object v1, v11

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1779
    const-string v2, "callToAction"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->CALL_TO_ACTION:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    move-object v0, p0

    move-object v1, v6

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1782
    const-string v2, "iconImage"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->ICON_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    move-object v0, p0

    move-object v1, v8

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->a(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1785
    const-string v2, "mainImage"

    sget-object v3, Lcom/millennialmedia/NativeAd$ComponentName;->MAIN_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    move-object v0, p0

    move-object v1, v9

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/NativeAd;->a(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V

    .line 1788
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/NativeAd;->h:Z

    .line 1789
    const/4 v0, 0x1

    .line 1793
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    if-eqz v1, :cond_9

    .line 1794
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    invoke-virtual {v1}, Lcom/millennialmedia/NativeAd$ImpressionReporter;->cancel()V

    .line 1796
    :cond_9
    new-instance v1, Lcom/millennialmedia/NativeAd$ImpressionReporter;

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    invoke-static {v2}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/adadapters/AdAdapter;)J

    move-result-wide v2

    invoke-direct {v1, p0, p1, v2, v3}, Lcom/millennialmedia/NativeAd$ImpressionReporter;-><init>(Lcom/millennialmedia/NativeAd;Landroid/view/View;J)V

    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    .line 1797
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    invoke-virtual {v1}, Lcom/millennialmedia/NativeAd$ImpressionReporter;->a()V

    .line 1804
    :goto_2
    return v0

    .line 1767
    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    .line 1801
    :cond_b
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Layout was not updated because it did not contain the required Views."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move v0, v3

    goto :goto_2
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/millennialmedia/NativeAd;->m:Z

    return v0
.end method

.method static synthetic a(Lcom/millennialmedia/NativeAd;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/millennialmedia/NativeAd;->m:Z

    return p1
.end method

.method private a(Lcom/millennialmedia/internal/adadapters/NativeAdapter;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 737
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getType()Ljava/lang/String;

    move-result-object v1

    .line 738
    if-nez v1, :cond_0

    .line 739
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to load components, native type is not set"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    :goto_0
    return v0

    .line 745
    :cond_0
    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 746
    sget-object v2, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to load components, native type <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "> is not a requested native type"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 752
    :cond_1
    invoke-static {v1}, Lcom/millennialmedia/internal/Handshake;->getNativeTypeDefinition(Ljava/lang/String;)Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    .line 753
    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    if-nez v2, :cond_2

    .line 754
    sget-object v2, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to load components, unable to find list of required components for native type <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ">"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 762
    :cond_2
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getTitleList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->v:Ljava/util/List;

    .line 763
    const-string v0, "title"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->TITLE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->v:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 766
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getBodyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->s:Ljava/util/List;

    .line 767
    const-string v0, "body"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->BODY:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->s:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 770
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getIconImageList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    .line 771
    const-string v0, "iconImage"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->ICON_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->b(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 774
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getMainImageList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    .line 775
    const-string v0, "mainImage"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->MAIN_IMAGE:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->b(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 778
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getCallToActionList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    .line 779
    const-string v0, "callToAction"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->CALL_TO_ACTION:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->c(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 782
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getRatingList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->u:Ljava/util/List;

    .line 783
    const-string v0, "rating"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->RATING:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->u:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 786
    invoke-virtual {p1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getDisclaimerList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    .line 789
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 790
    new-instance v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;

    invoke-direct {v0}, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;-><init>()V

    .line 791
    const-string v2, "Sponsored"

    iput-object v2, v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;->value:Ljava/lang/String;

    .line 793
    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 795
    :cond_3
    const-string v0, "disclaimer"

    sget-object v2, Lcom/millennialmedia/NativeAd$ComponentName;->DISCLAIMER:Lcom/millennialmedia/NativeAd$ComponentName;

    iget-object v3, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    invoke-direct {p0, v0, v2, v3}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V

    .line 798
    invoke-direct {p0, v1}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 804
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 806
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->componentDefinitions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 809
    if-eqz v0, :cond_2

    .line 810
    iget v6, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->adverstiserRequired:I

    .line 812
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    iget-object v7, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->componentId:Ljava/lang/String;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 813
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v6, :cond_0

    .line 814
    :cond_1
    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->componentId:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 818
    :cond_2
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Missing configuration data for native type: %s."

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 831
    :goto_1
    return v0

    .line 824
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 825
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load required components <"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "> for native type <"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ">"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 828
    goto :goto_1

    :cond_4
    move v0, v3

    .line 831
    goto :goto_1
.end method

.method static synthetic b(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic b(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/adadapters/NativeAdapter;)Lcom/millennialmedia/internal/adadapters/NativeAdapter;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    return-object p1
.end method

.method private b(Landroid/view/View;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1922
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1923
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const/16 v0, 0x384

    if-gt v1, v0, :cond_1

    .line 1924
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 1926
    if-eqz v0, :cond_1

    .line 1927
    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 1928
    check-cast v0, Landroid/widget/ImageView;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1923
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1930
    :cond_0
    new-instance v1, Lcom/millennialmedia/MMException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected View with tag = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to be a ImageView."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1939
    :cond_1
    return-object v2
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1565
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1567
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->componentDefinitions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;

    .line 1572
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    iget-object v5, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->componentId:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 1573
    if-eqz v1, :cond_3

    .line 1574
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    .line 1577
    :goto_1
    iget v5, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    if-ge v1, v5, :cond_0

    .line 1578
    const-string v5, "Component: %s, required: %d, accessed: %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->componentId:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v7, 0x1

    iget v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition$ComponentDefinition;->publisherRequired:I

    .line 1580
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    .line 1579
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1578
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1584
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1585
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to validate that all required native components have been accessed:\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1586
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1588
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    new-instance v1, Lcom/millennialmedia/MMException;

    invoke-direct {v1, v0}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1591
    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method static synthetic b(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 4

    .prologue
    .line 1959
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->g()V

    .line 1961
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getNativeExpirationDuration()I

    move-result v0

    .line 1962
    if-lez v0, :cond_0

    .line 1963
    new-instance v1, Lcom/millennialmedia/NativeAd$ExpirationRunnable;

    invoke-direct {v1, p0, p1}, Lcom/millennialmedia/NativeAd$ExpirationRunnable;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    int-to-long v2, v0

    .line 1964
    invoke-static {v1, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1966
    :cond_0
    return-void
.end method

.method private b(Lcom/millennialmedia/internal/adadapters/NativeAdapter;)V
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 945
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 948
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    .line 949
    return-void
.end method

.method private b(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/NativeAd$ComponentName;",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 863
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 864
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 865
    if-eqz v0, :cond_1

    .line 866
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 867
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;

    .line 868
    if-nez v1, :cond_0

    .line 866
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 872
    :cond_0
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 874
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 875
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 876
    invoke-direct {p0, v5, p2, v2, v1}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 878
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 882
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 883
    return-void
.end method

.method private b(Ljava/util/List;Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/NativeAd$ComponentName;",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1869
    if-nez p1, :cond_1

    .line 1895
    :cond_0
    return-void

    .line 1873
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1876
    if-eqz p5, :cond_2

    .line 1877
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    move v3, v1

    .line 1882
    :goto_0
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v3, :cond_0

    .line 1883
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1885
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_3

    .line 1886
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1887
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1888
    invoke-interface {p4, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    invoke-direct {p0, v1, p3, v4, v2}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 1882
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1879
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v3, v1

    goto :goto_0

    .line 1891
    :cond_3
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1892
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method static synthetic c(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object p1
.end method

.method private c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 1980
    monitor-enter p0

    .line 1982
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1983
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1984
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "onAdAdapterLoadFailed called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986
    :cond_0
    monitor-exit p0

    .line 2005
    :goto_0
    return-void

    .line 1989
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1990
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1991
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAdAdapterLoadFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 2002
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1997
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1998
    monitor-exit p0

    goto :goto_0

    .line 2001
    :cond_4
    const-string v0, "ad_adapter_load_failed"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 2002
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2004
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Lcom/millennialmedia/NativeAd$ComponentName;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/millennialmedia/NativeAd$ComponentName;",
            "Ljava/util/List",
            "<",
            "Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 889
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 890
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 891
    if-eqz v0, :cond_1

    .line 892
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 893
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;

    .line 894
    if-nez v1, :cond_0

    .line 892
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 898
    :cond_0
    new-instance v4, Landroid/widget/Button;

    invoke-direct {v4, v0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 899
    iget-object v5, v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$TextComponentInfo;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 900
    invoke-direct {p0, v4, p2, v2, v1}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;Lcom/millennialmedia/NativeAd$ComponentName;ILcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 902
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 906
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 907
    return-void
.end method

.method static synthetic c(Lcom/millennialmedia/NativeAd;)Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/adadapters/NativeAdapter;)Z
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/adadapters/NativeAdapter;)Z

    move-result v0

    return v0
.end method

.method public static createInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/millennialmedia/NativeAd;
    .locals 2

    .prologue
    .line 378
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lcom/millennialmedia/NativeAd;->createInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/millennialmedia/NativeAd;

    move-result-object v0

    return-object v0
.end method

.method public static createInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/millennialmedia/NativeAd;
    .locals 2

    .prologue
    .line 392
    invoke-static {}, Lcom/millennialmedia/MMSDK;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    new-instance v0, Lcom/millennialmedia/MMInitializationException;

    const-string v1, "Unable to create instance, SDK must be initialized first"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMInitializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :cond_0
    new-instance v0, Lcom/millennialmedia/NativeAd;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/NativeAd;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic d(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic d(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 5

    .prologue
    .line 2010
    monitor-enter p0

    .line 2012
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2013
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2014
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "onLoadSucceeded called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2016
    :cond_0
    monitor-exit p0

    .line 2062
    :cond_1
    :goto_0
    return-void

    .line 2019
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2020
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2021
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadSucceeded called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2023
    :cond_3
    monitor-exit p0

    goto :goto_0

    .line 2037
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2027
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2028
    monitor-exit p0

    goto :goto_0

    .line 2031
    :cond_5
    const-string v0, "loaded"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 2033
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Load succeeded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->e()V

    .line 2035
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 2036
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 2037
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2041
    :try_start_2
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    .line 2042
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPostLoaded"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Lcom/millennialmedia/NativeAd;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2044
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2052
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    .line 2053
    if-eqz v0, :cond_1

    .line 2054
    new-instance v1, Lcom/millennialmedia/NativeAd$7;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/NativeAd$7;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/NativeAd$NativeListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2045
    :catch_0
    move-exception v0

    .line 2046
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2047
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Could not find method <onPostLoaded> in adAdapter"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1945
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 1946
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1947
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1950
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 1951
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1952
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->j:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1954
    :cond_1
    return-void
.end method

.method static synthetic e(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 2067
    monitor-enter p0

    .line 2069
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2070
    monitor-exit p0

    .line 2109
    :cond_0
    :goto_0
    return-void

    .line 2073
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2074
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2075
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "onLoadFailed called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2077
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 2096
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2080
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loading_play_list"

    .line 2081
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2082
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2083
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2085
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 2088
    :cond_5
    const-string v0, "load_failed"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 2090
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Load failed for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". If this warning persists please check your placement configuration."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->e()V

    .line 2095
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 2096
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2099
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    .line 2100
    if-eqz v0, :cond_0

    .line 2101
    new-instance v1, Lcom/millennialmedia/NativeAd$8;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/NativeAd$8;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/NativeAd$NativeListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/millennialmedia/NativeAd;)Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method static synthetic f(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/millennialmedia/NativeAd;->d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 2132
    monitor-enter p0

    .line 2133
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2134
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2135
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "onExpired called but load state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2137
    :cond_0
    monitor-exit p0

    .line 2163
    :cond_1
    :goto_0
    return-void

    .line 2140
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2141
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2142
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onExpired called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2144
    :cond_3
    monitor-exit p0

    goto :goto_0

    .line 2148
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2147
    :cond_4
    :try_start_1
    const-string v0, "expired"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 2148
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2150
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Ad expired"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2153
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    .line 2154
    if-eqz v0, :cond_1

    .line 2155
    new-instance v1, Lcom/millennialmedia/NativeAd$10;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/NativeAd$10;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/NativeAd$NativeListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/millennialmedia/NativeAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1971
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 1972
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1973
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->k:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1975
    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/millennialmedia/NativeAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2114
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Ad left application"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2117
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    .line 2118
    if-eqz v0, :cond_0

    .line 2119
    new-instance v1, Lcom/millennialmedia/NativeAd$9;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/NativeAd$9;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/NativeAd$NativeListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    .line 2127
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/internal/adadapters/NativeAdapter;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    return-object v0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 2169
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "idle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "load_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    .line 2170
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "destroyed"

    .line 2171
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2173
    :cond_0
    const/4 v0, 0x0

    .line 2176
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic j(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/internal/adadapters/NativeAdapter;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    return-object v0
.end method

.method static synthetic k(Lcom/millennialmedia/NativeAd;)Lcom/millennialmedia/NativeAd$NativeListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    return-object v0
.end method


# virtual methods
.method protected c()Z
    .locals 1

    .prologue
    .line 2196
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2202
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    .line 2203
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    .line 2204
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    .line 2206
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->e()V

    .line 2207
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->g()V

    .line 2209
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    if-eqz v0, :cond_0

    .line 2210
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    invoke-virtual {v0}, Lcom/millennialmedia/NativeAd$ImpressionReporter;->cancel()V

    .line 2211
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->l:Lcom/millennialmedia/NativeAd$ImpressionReporter;

    .line 2214
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v0, :cond_2

    .line 2215
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_1

    .line 2216
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 2217
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 2219
    :cond_1
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    .line 2221
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v0, :cond_4

    .line 2222
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_3

    .line 2223
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 2224
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 2226
    :cond_3
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->n:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    .line 2229
    :cond_4
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    .line 2231
    new-instance v0, Lcom/millennialmedia/NativeAd$11;

    invoke-direct {v0, p0}, Lcom/millennialmedia/NativeAd$11;-><init>(Lcom/millennialmedia/NativeAd;)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2249
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2251
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 2252
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2253
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    .line 2256
    :cond_5
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->s:Ljava/util/List;

    .line 2257
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->t:Ljava/util/List;

    .line 2258
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->u:Ljava/util/List;

    .line 2259
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->v:Ljava/util/List;

    .line 2260
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->w:Ljava/util/List;

    .line 2261
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->x:Ljava/util/List;

    .line 2262
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->y:Ljava/util/List;

    .line 2264
    iput-object v1, p0, Lcom/millennialmedia/NativeAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 2265
    return-void
.end method

.method public fireCallToActionClicked()V
    .locals 2

    .prologue
    .line 1366
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1378
    :goto_0
    return-void

    .line 1370
    :cond_0
    sget-object v0, Lcom/millennialmedia/NativeAd$ComponentName;->CALL_TO_ACTION:Lcom/millennialmedia/NativeAd$ComponentName;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/NativeAd$ComponentName;I)Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    move-result-object v0

    .line 1371
    if-nez v0, :cond_1

    .line 1372
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Unable to fire clicked, found component info is null"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1377
    :cond_1
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    goto :goto_0
.end method

.method public fireImpression()V
    .locals 2

    .prologue
    .line 1020
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1041
    :goto_0
    return-void

    .line 1024
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1025
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Native ad is not in a loaded state, you must load before showing"

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/utils/Utils;->logAndFireMMException(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1030
    :cond_1
    iget-boolean v0, p0, Lcom/millennialmedia/NativeAd;->h:Z

    if-eqz v0, :cond_2

    .line 1031
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Impression firing is disabled when using a managed layout."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1036
    :cond_2
    invoke-direct {p0}, Lcom/millennialmedia/NativeAd;->b()V

    .line 1038
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "All required components have been accessed, firing impression"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    goto :goto_0
.end method

.method public getBody()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getBody(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getBody(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1181
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1191
    :goto_0
    return-object v0

    .line 1185
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1186
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get body, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1191
    :cond_1
    const-string v0, "body"

    const-string v1, "body"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public getCallToActionButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 1268
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getCallToActionButton(I)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method public getCallToActionButton(I)Landroid/widget/Button;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1280
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1290
    :goto_0
    return-object v0

    .line 1284
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1285
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get call to action button, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1290
    :cond_1
    const-string v0, "callToAction"

    const-string v1, "call to action"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    goto :goto_0
.end method

.method public getCallToActionUrl()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1389
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1401
    :goto_0
    return-object v0

    .line 1393
    :cond_0
    sget-object v1, Lcom/millennialmedia/NativeAd$ComponentName;->CALL_TO_ACTION:Lcom/millennialmedia/NativeAd$ComponentName;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/NativeAd$ComponentName;I)Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    move-result-object v1

    .line 1394
    if-nez v1, :cond_1

    .line 1395
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get call to action url, found component info is not for a call to action component"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1401
    :cond_1
    iget-object v0, v1, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;->clickUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getCreativeInfo()Lcom/millennialmedia/CreativeInfo;
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getCreativeInfo()Lcom/millennialmedia/CreativeInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisclaimer()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1334
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getDisclaimer(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getDisclaimer(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1346
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1356
    :goto_0
    return-object v0

    .line 1350
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1351
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get disclaimer, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1356
    :cond_1
    const-string v0, "disclaimer"

    const-string v1, "disclaimer"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public getIconImage()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getIconImage(I)Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getIconImage(I)Landroid/widget/ImageView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1214
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1224
    :goto_0
    return-object v0

    .line 1218
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1219
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get icon image, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1224
    :cond_1
    const-string v0, "iconImage"

    const-string v1, "icon image"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public getImageUrl(Lcom/millennialmedia/NativeAd$ComponentName;I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1413
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1424
    :goto_0
    return-object v0

    .line 1417
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/NativeAd$ComponentName;I)Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;

    move-result-object v0

    .line 1418
    instance-of v2, v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;

    if-nez v2, :cond_1

    .line 1419
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get image url, found component info is not for a image component"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 1421
    goto :goto_0

    .line 1424
    :cond_1
    check-cast v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;

    iget-object v0, v0, Lcom/millennialmedia/internal/adadapters/NativeAdapter$ImageComponentInfo;->bitmapUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMainImage()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1235
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getMainImage(I)Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getMainImage(I)Landroid/widget/ImageView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1247
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1257
    :goto_0
    return-object v0

    .line 1251
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1252
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get main image, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1257
    :cond_1
    const-string v0, "mainImage"

    const-string v1, "main image"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public getNativeType()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1111
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1125
    :cond_0
    :goto_0
    return-object v0

    .line 1115
    :cond_1
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1116
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get native type, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1121
    :cond_2
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v1, :cond_0

    .line 1125
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->p:Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;

    iget-object v0, v0, Lcom/millennialmedia/internal/Handshake$NativeTypeDefinition;->typeName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRating()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1301
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getRating(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getRating(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1313
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1323
    :goto_0
    return-object v0

    .line 1317
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1318
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get rating, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1323
    :cond_1
    const-string v0, "rating"

    const-string v1, "rating"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public getTitle()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1136
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/millennialmedia/NativeAd;->getTitle(I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1148
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1158
    :goto_0
    return-object v0

    .line 1152
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1153
    sget-object v1, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v2, "Unable to get title, ad not loaded"

    invoke-static {v1, v2}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1158
    :cond_1
    const-string v0, "title"

    const-string v1, "title"

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public hasExpired()Z
    .locals 2

    .prologue
    .line 959
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 960
    const/4 v0, 0x0

    .line 963
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public inflateLayout(Landroid/content/Context;[I)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1646
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688
    :goto_0
    return-object v4

    .line 1650
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/utils/ThreadUtils;->isUiThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1651
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "NativeAd.inflateLayout must be called on the UI thread."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1656
    :cond_1
    if-nez p1, :cond_2

    .line 1657
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Unable to inflate a layout because the provided Context is null."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1662
    :cond_2
    if-eqz p2, :cond_3

    array-length v0, p2

    if-nez v0, :cond_4

    .line 1663
    :cond_3
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Unable to inflate a layout because the layoutIds are null or empty."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1668
    :cond_4
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1669
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Cannot inflate a layout. The NativeAd is not loaded."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1674
    :cond_5
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    move v0, v1

    .line 1677
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_8

    .line 1678
    aget v2, p2, v0

    invoke-virtual {v5, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1679
    array-length v2, p2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    const/4 v2, 0x1

    .line 1681
    :goto_2
    invoke-direct {p0, v3, v2, v1}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;ZZ)Z

    move-result v2

    if-eqz v2, :cond_7

    move-object v0, v3

    :goto_3
    move-object v4, v0

    .line 1688
    goto :goto_0

    :cond_6
    move v2, v1

    .line 1679
    goto :goto_2

    .line 1677
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    move-object v0, v4

    goto :goto_3
.end method

.method public invokeDefaultAction()V
    .locals 2

    .prologue
    .line 972
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 989
    :cond_0
    :goto_0
    return-void

    .line 976
    :cond_1
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v0

    if-nez v0, :cond_2

    .line 977
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Unable to invoke default action, ad not loaded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 982
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    if-eqz v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/millennialmedia/NativeAd;->a(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/adadapters/NativeAdapter$ComponentInfo;)V

    .line 988
    const-string v0, "default"

    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->o:Lcom/millennialmedia/internal/adadapters/NativeAdapter;

    invoke-virtual {v1}, Lcom/millennialmedia/internal/adadapters/NativeAdapter;->getDefaultAction()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/millennialmedia/NativeAd;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isReady()Z
    .locals 2

    .prologue
    .line 932
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    const/4 v0, 0x0

    .line 936
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public load(Landroid/content/Context;Lcom/millennialmedia/NativeAd$NativeAdMetadata;)V
    .locals 6

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    :goto_0
    return-void

    .line 457
    :cond_0
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading playlist for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/NativeAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    if-nez p1, :cond_1

    .line 460
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "Unable to load native, specified context cannot be null"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->f:Ljava/lang/ref/WeakReference;

    .line 464
    monitor-enter p0

    .line 465
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "idle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "load_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    .line 466
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    const-string v1, "expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 468
    monitor-exit p0

    goto :goto_0

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 471
    :cond_2
    :try_start_1
    const-string v0, "loading_play_list"

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->a:Ljava/lang/String;

    .line 472
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/NativeAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 476
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 477
    iget-object v0, p0, Lcom/millennialmedia/NativeAd;->loadedComponents:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/NativeAd;->h:Z

    .line 481
    if-nez p2, :cond_3

    .line 482
    new-instance p2, Lcom/millennialmedia/NativeAd$NativeAdMetadata;

    invoke-direct {p2}, Lcom/millennialmedia/NativeAd$NativeAdMetadata;-><init>()V

    .line 485
    :cond_3
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->getRequestState()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v0

    .line 488
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v1, :cond_4

    .line 489
    iget-object v1, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 492
    :cond_4
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getNativeTimeout()I

    move-result v1

    .line 496
    new-instance v2, Lcom/millennialmedia/NativeAd$1;

    invoke-direct {v2, p0, v0}, Lcom/millennialmedia/NativeAd$1;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    int-to-long v4, v1

    invoke-static {v2, v4, v5}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/NativeAd;->i:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 509
    invoke-virtual {p2, p0}, Lcom/millennialmedia/NativeAd$NativeAdMetadata;->toMap(Lcom/millennialmedia/internal/AdPlacement;)Ljava/util/Map;

    move-result-object v2

    .line 510
    const-string v3, "nativeTypes"

    iget-object v4, p0, Lcom/millennialmedia/NativeAd;->q:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 512
    invoke-virtual {p2}, Lcom/millennialmedia/NativeAd$NativeAdMetadata;->getImpressionGroup()Ljava/lang/String;

    move-result-object v3

    .line 513
    new-instance v4, Lcom/millennialmedia/NativeAd$2;

    invoke-direct {v4, p0, v0, v3}, Lcom/millennialmedia/NativeAd$2;-><init>(Lcom/millennialmedia/NativeAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Ljava/lang/String;)V

    invoke-static {v2, v4, v1}, Lcom/millennialmedia/internal/playlistserver/PlayListServer;->loadPlayList(Ljava/util/Map;Lcom/millennialmedia/internal/playlistserver/PlayListServer$PlayListLoadListener;I)V

    goto/16 :goto_0
.end method

.method public setListener(Lcom/millennialmedia/NativeAd$NativeListener;)V
    .locals 1

    .prologue
    .line 917
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    :goto_0
    return-void

    .line 921
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/NativeAd;->g:Lcom/millennialmedia/NativeAd$NativeListener;

    goto :goto_0
.end method

.method public updateLayout(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1604
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1627
    :goto_0
    return-void

    .line 1608
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/utils/ThreadUtils;->isUiThread()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1609
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "NativeAd.updateLayout must be called on the UI thread."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1614
    :cond_1
    if-nez p1, :cond_2

    .line 1615
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Unable to updated; the provided layout was null."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1620
    :cond_2
    invoke-virtual {p0}, Lcom/millennialmedia/NativeAd;->isReady()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1621
    sget-object v0, Lcom/millennialmedia/NativeAd;->e:Ljava/lang/String;

    const-string v1, "Cannot update the layout. The NativeAd is not loaded."

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1626
    :cond_3
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/millennialmedia/NativeAd;->a(Landroid/view/View;ZZ)Z

    goto :goto_0
.end method
