.class Lcom/millennialmedia/InlineAd$4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

.field final synthetic b:Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

.field final synthetic c:Lcom/millennialmedia/InlineAd;


# direct methods
.method constructor <init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iput-object p2, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    iput-object p3, p0, Lcom/millennialmedia/InlineAd$4;->b:Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public displayFailed()V
    .locals 3

    .prologue
    .line 957
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 958
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ad adapter display failed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 962
    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->b:Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    const/4 v2, -0x3

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;I)V

    .line 965
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->d(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 966
    return-void
.end method

.method public displaySucceeded()V
    .locals 3

    .prologue
    .line 943
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 944
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Display succeeded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 948
    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->b:Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    invoke-static {v0, v1}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    .line 950
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    iget-object v2, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v2}, Lcom/millennialmedia/InlineAd;->o(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V

    .line 951
    return-void
.end method

.method public initFailed()V
    .locals 3

    .prologue
    .line 928
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 929
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ad adapter init failed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 933
    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->b:Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    const/4 v2, -0x3

    invoke-static {v0, v1, v2}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;I)V

    .line 936
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->d(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 937
    return-void
.end method

.method public initSucceeded()V
    .locals 4

    .prologue
    .line 866
    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    monitor-enter v1

    .line 867
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v0}, Lcom/millennialmedia/InlineAd;->i(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v0

    iget-object v2, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, v2}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 868
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 869
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    const-string v2, "initSucceeded called but request state is not valid"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    :cond_0
    monitor-exit v1

    .line 922
    :goto_0
    return-void

    .line 875
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v0}, Lcom/millennialmedia/InlineAd;->j(Lcom/millennialmedia/InlineAd;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "loading_ad_adapter"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 876
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 877
    invoke-static {}, Lcom/millennialmedia/InlineAd;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSucceeded called but placement state is not valid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    .line 878
    invoke-static {v3}, Lcom/millennialmedia/InlineAd;->k(Lcom/millennialmedia/InlineAd;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 877
    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 883
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 885
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v0}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 886
    if-nez v0, :cond_4

    .line 887
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd$4;->displayFailed()V

    goto :goto_0

    .line 893
    :cond_4
    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v2, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v2}, Lcom/millennialmedia/InlineAd;->l(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V

    .line 894
    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/millennialmedia/InlineAd;->b(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    .line 896
    new-instance v1, Lcom/millennialmedia/InlineAd$4$1;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$4$1;-><init>(Lcom/millennialmedia/InlineAd$4;Landroid/view/ViewGroup;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onAdLeftApplication()V
    .locals 2

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->h(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 1008
    return-void
.end method

.method public onClicked()V
    .locals 2

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->g(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 1001
    return-void
.end method

.method public onCollapsed()V
    .locals 2

    .prologue
    .line 993
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->f(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 994
    return-void
.end method

.method public onExpanded()V
    .locals 2

    .prologue
    .line 986
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1}, Lcom/millennialmedia/InlineAd;->e(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    .line 987
    return-void
.end method

.method public onIncentiveEarned(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    invoke-static {v0, p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V

    .line 1015
    return-void
.end method

.method public onResize(II)V
    .locals 2

    .prologue
    .line 972
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1, p1, p2}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;II)V

    .line 973
    return-void
.end method

.method public onResized(IIZ)V
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/millennialmedia/InlineAd$4;->c:Lcom/millennialmedia/InlineAd;

    iget-object v1, p0, Lcom/millennialmedia/InlineAd$4;->a:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;IIZ)V

    .line 980
    return-void
.end method
