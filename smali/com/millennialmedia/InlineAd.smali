.class public Lcom/millennialmedia/InlineAd;
.super Lcom/millennialmedia/internal/AdPlacement;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/millennialmedia/InlineAd$RefreshRunnable;,
        Lcom/millennialmedia/InlineAd$AdSize;,
        Lcom/millennialmedia/InlineAd$ImpressionListener;,
        Lcom/millennialmedia/InlineAd$InlineAdMetadata;,
        Lcom/millennialmedia/InlineAd$InlineErrorStatus;,
        Lcom/millennialmedia/InlineAd$InlineAbortListener;,
        Lcom/millennialmedia/InlineAd$InlineListener;
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/millennialmedia/InlineAd$InlineListener;

.field private h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

.field private i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

.field private j:Landroid/widget/RelativeLayout;

.field private k:J

.field private l:Ljava/lang/Integer;

.field private m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

.field private volatile p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

.field private volatile q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

.field private volatile r:Lcom/millennialmedia/InlineAd$ImpressionListener;

.field private volatile s:Z

.field private volatile t:Z

.field private volatile u:Z

.field private volatile v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/millennialmedia/InlineAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 589
    invoke-direct {p0, p1}, Lcom/millennialmedia/internal/AdPlacement;-><init>(Ljava/lang/String;)V

    .line 66
    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->s:Z

    .line 67
    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    .line 68
    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->u:Z

    .line 69
    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->v:Z

    .line 591
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->f:Ljava/lang/ref/WeakReference;

    .line 592
    return-void
.end method

.method private static a(Lcom/millennialmedia/internal/adadapters/AdAdapter;)J
    .locals 2

    .prologue
    .line 1535
    instance-of v0, p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 1536
    check-cast p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {p0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->getImpressionDelay()J

    move-result-wide v0

    .line 1541
    :goto_0
    return-wide v0

    .line 1538
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinImpressionDuration()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->j:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/PlayList;)Lcom/millennialmedia/internal/PlayList;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->b:Lcom/millennialmedia/internal/PlayList;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->f:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 1132
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/millennialmedia/internal/AdPlacementReporter;->setDisplayed(Lcom/millennialmedia/internal/AdPlacementReporter;I)V

    .line 1134
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    if-eqz v0, :cond_0

    .line 1135
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    invoke-virtual {v0}, Lcom/millennialmedia/InlineAd$ImpressionListener;->cancel()V

    .line 1136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    .line 1138
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/XIncentivizedEventListener$XIncentiveEvent;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;II)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;II)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;IIZ)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;IIZ)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V

    return-void
.end method

.method static synthetic a(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V

    return-void
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 8

    .prologue
    .line 781
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->copy()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v1

    .line 783
    monitor-enter p0

    .line 785
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 786
    monitor-exit p0

    .line 1022
    :goto_0
    return-void

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v2, "play_list_loaded"

    .line 790
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v2, "ad_adapter_load_failed"

    .line 791
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 793
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 805
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 796
    :cond_2
    :try_start_1
    const-string v0, "loading_ad_adapter"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 803
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getItemHash()I

    .line 804
    iput-object v1, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    .line 805
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 807
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/PlayList;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 808
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 809
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v2, "Unable to find ad adapter in play list"

    invoke-static {v0, v2}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :cond_3
    invoke-direct {p0, v1}, Lcom/millennialmedia/InlineAd;->c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0

    .line 816
    :cond_4
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->v:Z

    if-eqz v0, :cond_5

    .line 817
    invoke-direct {p0, v1}, Lcom/millennialmedia/InlineAd;->h(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0

    .line 823
    :cond_5
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->getPlayListItemReporter(Lcom/millennialmedia/internal/AdPlacementReporter;)Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;

    move-result-object v2

    .line 827
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->b:Lcom/millennialmedia/internal/PlayList;

    invoke-virtual {v0, p0, v2}, Lcom/millennialmedia/internal/PlayList;->getNextAdAdapter(Lcom/millennialmedia/internal/AdPlacement;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)Lcom/millennialmedia/internal/adadapters/AdAdapter;

    move-result-object v0

    check-cast v0, Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    .line 829
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 832
    iget-object v3, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eqz v3, :cond_8

    if-eqz v0, :cond_8

    .line 835
    iget-object v3, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    iget v3, v3, Lcom/millennialmedia/internal/adadapters/InlineAdapter;->requestTimeout:I

    .line 836
    if-lez v3, :cond_7

    .line 837
    iget-object v4, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v4, :cond_6

    .line 838
    iget-object v4, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v4}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 843
    :cond_6
    new-instance v4, Lcom/millennialmedia/InlineAd$3;

    invoke-direct {v4, p0, v1, v2}, Lcom/millennialmedia/InlineAd$3;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    int-to-long v6, v3

    invoke-static {v4, v6, v7}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v3

    iput-object v3, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 861
    :cond_7
    iget-object v3, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v4, Lcom/millennialmedia/InlineAd$4;

    invoke-direct {v4, p0, v1, v2}, Lcom/millennialmedia/InlineAd$4;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    invoke-virtual {v3, v0, v4}, Lcom/millennialmedia/internal/adadapters/InlineAdapter;->init(Landroid/content/Context;Lcom/millennialmedia/internal/adadapters/InlineAdapter$InlineAdapterListener;)V

    goto/16 :goto_0

    .line 1019
    :cond_8
    invoke-virtual {v1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayListItem(Lcom/millennialmedia/internal/AdPlacementReporter;Lcom/millennialmedia/internal/AdPlacementReporter$PlayListItemReporter;)V

    .line 1020
    invoke-direct {p0, v1}, Lcom/millennialmedia/InlineAd;->b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;II)V
    .locals 2

    .prologue
    .line 1306
    monitor-enter p0

    .line 1307
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1308
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onResize called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    :cond_0
    monitor-exit p0

    .line 1330
    :cond_1
    :goto_0
    return-void

    .line 1314
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1316
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad resizing"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    .line 1320
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1321
    if-eqz v0, :cond_1

    .line 1322
    new-instance v1, Lcom/millennialmedia/InlineAd$8;

    invoke-direct {v1, p0, v0, p2, p3}, Lcom/millennialmedia/InlineAd$8;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;II)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1314
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;IIZ)V
    .locals 6

    .prologue
    .line 1336
    monitor-enter p0

    .line 1337
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1338
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1339
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onResized called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    :cond_0
    monitor-exit p0

    .line 1362
    :cond_1
    :goto_0
    return-void

    .line 1344
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1346
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ad resized, is closed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1347
    if-eqz p4, :cond_3

    .line 1348
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    .line 1352
    :cond_3
    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1353
    if-eqz v2, :cond_1

    .line 1354
    new-instance v0, Lcom/millennialmedia/InlineAd$9;

    move-object v1, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/millennialmedia/InlineAd$9;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;IIZ)V

    invoke-static {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1344
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lcom/millennialmedia/internal/AdPlacement$RequestState;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V
    .locals 7

    .prologue
    .line 1175
    monitor-enter p0

    .line 1177
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1178
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1179
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onRequestSucceeded called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    :cond_0
    monitor-exit p0

    .line 1226
    :cond_1
    :goto_0
    return-void

    .line 1185
    :cond_2
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1186
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1187
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestSucceeded called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    :cond_3
    monitor-exit p0

    goto :goto_0

    .line 1209
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1194
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1195
    monitor-exit p0

    goto :goto_0

    .line 1198
    :cond_5
    const-string v0, "loaded"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 1200
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Request succeeded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->i()V

    .line 1203
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 1205
    new-instance v1, Lcom/millennialmedia/InlineAd$ImpressionListener;

    iget-object v3, p0, Lcom/millennialmedia/InlineAd;->j:Landroid/widget/RelativeLayout;

    invoke-static {p2}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/adadapters/AdAdapter;)J

    move-result-wide v4

    .line 1206
    invoke-static {p2}, Lcom/millennialmedia/InlineAd;->b(Lcom/millennialmedia/internal/adadapters/AdAdapter;)I

    move-result v6

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/millennialmedia/InlineAd$ImpressionListener;-><init>(Lcom/millennialmedia/InlineAd;Landroid/view/View;JI)V

    iput-object v1, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    .line 1208
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    invoke-virtual {v0}, Lcom/millennialmedia/InlineAd$ImpressionListener;->a()V

    .line 1209
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1212
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1213
    if-eqz v0, :cond_1

    .line 1214
    new-instance v1, Lcom/millennialmedia/InlineAd$5;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$5;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private a(Lcom/millennialmedia/internal/adadapters/InlineAdapter;)V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 624
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    .line 625
    return-void
.end method

.method private static b(Lcom/millennialmedia/internal/adadapters/AdAdapter;)I
    .locals 1

    .prologue
    .line 1549
    instance-of v0, p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_0

    .line 1550
    check-cast p0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {p0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->getMinImpressionViewabilityPercentage()I

    move-result v0

    .line 1555
    :goto_0
    return v0

    .line 1552
    :cond_0
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinImpressionViewabilityPercent()I

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/InlineAd$ImpressionListener;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    return-object v0
.end method

.method static synthetic b(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object p1
.end method

.method static synthetic b(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/adadapters/InlineAdapter;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    return-object p1
.end method

.method private b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 1143
    monitor-enter p0

    .line 1145
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1146
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onAdAdapterLoadFailed called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    :cond_0
    monitor-exit p0

    .line 1170
    :goto_0
    return-void

    .line 1153
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1154
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1155
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAdAdapterLoadFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 1167
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1162
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1163
    monitor-exit p0

    goto :goto_0

    .line 1166
    :cond_4
    const-string v0, "ad_adapter_load_failed"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 1167
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1169
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->a(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private c(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 1231
    monitor-enter p0

    .line 1232
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1233
    monitor-exit p0

    .line 1278
    :cond_0
    :goto_0
    return-void

    .line 1236
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compareRequest(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1237
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1238
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onRequestFailed called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 1261
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1244
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loading_play_list"

    .line 1245
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1246
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1247
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestFailed called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 1253
    :cond_5
    const-string v0, "load_failed"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 1255
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request failed for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". If this warning persists please check your placement configuration."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->i()V

    .line 1260
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 1261
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1264
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1265
    if-eqz v0, :cond_0

    .line 1266
    new-instance v1, Lcom/millennialmedia/InlineAd$6;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$6;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/millennialmedia/InlineAd;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    return v0
.end method

.method public static createInstance(Ljava/lang/String;Landroid/view/ViewGroup;)Lcom/millennialmedia/InlineAd;
    .locals 2

    .prologue
    .line 571
    invoke-static {}, Lcom/millennialmedia/MMSDK;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 572
    new-instance v0, Lcom/millennialmedia/MMInitializationException;

    const-string v1, "Unable to create instance, SDK must be initialized first"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMInitializationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 575
    :cond_0
    if-nez p1, :cond_1

    .line 576
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "Unable to create instance, ad container cannot be null"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 579
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    .line 580
    new-instance v0, Lcom/millennialmedia/MMException;

    const-string v1, "Unable to create instance, ad container must have an associated context"

    invoke-direct {v0, v1}, Lcom/millennialmedia/MMException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_2
    new-instance v0, Lcom/millennialmedia/InlineAd;

    invoke-direct {v0, p0, p1}, Lcom/millennialmedia/InlineAd;-><init>(Ljava/lang/String;Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method static synthetic d(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->b(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 1283
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad clicked"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/millennialmedia/InlineAd;->a(I)V

    .line 1288
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->setClicked(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 1291
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1292
    if-eqz v0, :cond_0

    .line 1293
    new-instance v1, Lcom/millennialmedia/InlineAd$7;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$7;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    .line 1301
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/millennialmedia/InlineAd;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->u:Z

    return v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/millennialmedia/InlineAd;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->g()V

    return-void
.end method

.method static synthetic e(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private e(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 1367
    monitor-enter p0

    .line 1368
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1369
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1370
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onExpanded called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    :cond_0
    monitor-exit p0

    .line 1392
    :cond_1
    :goto_0
    return-void

    .line 1375
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1377
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad expanded"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->u:Z

    .line 1379
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    .line 1382
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1383
    if-eqz v0, :cond_1

    .line 1384
    new-instance v1, Lcom/millennialmedia/InlineAd$10;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$10;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1375
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic f(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private f(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 1397
    monitor-enter p0

    .line 1398
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1399
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onCollapsed called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    :cond_0
    monitor-exit p0

    .line 1421
    :cond_1
    :goto_0
    return-void

    .line 1405
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1407
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad collapsed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->u:Z

    .line 1411
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1412
    if-eqz v0, :cond_1

    .line 1413
    new-instance v1, Lcom/millennialmedia/InlineAd$11;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$11;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1405
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic f(Lcom/millennialmedia/InlineAd;)Z
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 666
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->u:Z

    if-eqz v0, :cond_1

    .line 667
    :cond_0
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Inline ad is resized or expanded, unable to request new ad"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :goto_0
    return-void

    .line 674
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 675
    iget-wide v2, p0, Lcom/millennialmedia/InlineAd;->k:J

    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinInlineRefreshRate()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 676
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Too soon since last inline ad request, unable to request ad"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 681
    :cond_2
    monitor-enter p0

    .line 683
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 684
    monitor-exit p0

    goto :goto_0

    .line 696
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 687
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 688
    monitor-exit p0

    goto :goto_0

    .line 692
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->v:Z

    .line 693
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

    .line 695
    const-string v0, "loading_play_list"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 696
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 699
    iput-object v6, p0, Lcom/millennialmedia/InlineAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 702
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/millennialmedia/InlineAd;->k:J

    .line 705
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    if-nez v0, :cond_5

    .line 706
    new-instance v0, Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    invoke-direct {v0}, Lcom/millennialmedia/InlineAd$InlineAdMetadata;-><init>()V

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    .line 709
    :cond_5
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->getRequestState()Lcom/millennialmedia/internal/AdPlacement$RequestState;

    move-result-object v0

    .line 712
    iget-object v1, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v1, :cond_6

    .line 713
    iget-object v1, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 716
    :cond_6
    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getInlineTimeout()I

    move-result v1

    .line 720
    new-instance v2, Lcom/millennialmedia/InlineAd$1;

    invoke-direct {v2, p0, v0}, Lcom/millennialmedia/InlineAd$1;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    int-to-long v4, v1

    invoke-static {v2, v4, v5}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v2

    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 732
    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    invoke-virtual {v2}, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->getImpressionGroup()Ljava/lang/String;

    move-result-object v2

    .line 733
    iget-object v3, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    invoke-virtual {v3, p0}, Lcom/millennialmedia/InlineAd$InlineAdMetadata;->e(Lcom/millennialmedia/InlineAd;)Ljava/util/Map;

    move-result-object v3

    new-instance v4, Lcom/millennialmedia/InlineAd$2;

    invoke-direct {v4, p0, v0, v2}, Lcom/millennialmedia/InlineAd$2;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;Ljava/lang/String;)V

    invoke-static {v3, v4, v1}, Lcom/millennialmedia/internal/playlistserver/PlayListServer;->loadPlayList(Ljava/util/Map;Lcom/millennialmedia/internal/playlistserver/PlayListServer$PlayListLoadListener;I)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->d(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private g(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 2

    .prologue
    .line 1426
    monitor-enter p0

    .line 1427
    :try_start_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1428
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onAdLeftApplication called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    :cond_0
    monitor-exit p0

    .line 1449
    :cond_1
    :goto_0
    return-void

    .line 1434
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad left application"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1440
    if-eqz v0, :cond_1

    .line 1441
    new-instance v1, Lcom/millennialmedia/InlineAd$12;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$12;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1434
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic h(Lcom/millennialmedia/InlineAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 1078
    monitor-enter p0

    .line 1081
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_2

    .line 1082
    :cond_0
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1083
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Refresh disabled or already started, returning"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    :cond_1
    monitor-exit p0

    .line 1091
    :goto_0
    return-void

    .line 1089
    :cond_2
    new-instance v0, Lcom/millennialmedia/InlineAd$RefreshRunnable;

    invoke-direct {v0, p0}, Lcom/millennialmedia/InlineAd$RefreshRunnable;-><init>(Lcom/millennialmedia/InlineAd;)V

    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnWorkerThreadDelayed(Ljava/lang/Runnable;J)Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1090
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic h(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/millennialmedia/InlineAd;->g(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V

    return-void
.end method

.method private h(Lcom/millennialmedia/internal/AdPlacement$RequestState;)V
    .locals 3

    .prologue
    .line 1454
    monitor-enter p0

    .line 1456
    :try_start_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1457
    monitor-exit p0

    .line 1496
    :cond_0
    :goto_0
    return-void

    .line 1460
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    invoke-virtual {v0, p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->compare(Lcom/millennialmedia/internal/AdPlacement$RequestState;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1461
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1462
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "onAborted called but request state is not valid"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1465
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 1477
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1468
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loading_ad_adapter"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1469
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1470
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAborted called but placement state is not valid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    :cond_4
    monitor-exit p0

    goto :goto_0

    .line 1476
    :cond_5
    const-string v0, "aborted"

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    .line 1477
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1479
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad aborted"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1483
    invoke-virtual {p1}, Lcom/millennialmedia/internal/AdPlacement$RequestState;->getAdPlacementReporter()Lcom/millennialmedia/internal/AdPlacementReporter;

    move-result-object v0

    invoke-static {v0}, Lcom/millennialmedia/internal/AdPlacementReporter;->reportPlayList(Lcom/millennialmedia/internal/AdPlacementReporter;)V

    .line 1486
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

    .line 1487
    if-eqz v0, :cond_0

    .line 1488
    new-instance v1, Lcom/millennialmedia/InlineAd$13;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$13;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineAbortListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic i(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/AdPlacement$RequestState;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->c:Lcom/millennialmedia/internal/AdPlacement$RequestState;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1118
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1120
    iput-object v1, p0, Lcom/millennialmedia/InlineAd;->m:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1123
    :cond_0
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 1124
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1125
    iput-object v1, p0, Lcom/millennialmedia/InlineAd;->n:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1127
    :cond_1
    return-void
.end method

.method static synthetic j(Lcom/millennialmedia/InlineAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1501
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    const-string v1, "Ad abort failed"

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1504
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

    .line 1505
    if-eqz v0, :cond_0

    .line 1506
    new-instance v1, Lcom/millennialmedia/InlineAd$14;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$14;-><init>(Lcom/millennialmedia/InlineAd;Lcom/millennialmedia/InlineAd$InlineAbortListener;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOffUiThread(Ljava/lang/Runnable;)V

    .line 1514
    :cond_0
    return-void
.end method

.method static synthetic k(Lcom/millennialmedia/InlineAd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "idle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "load_failed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "loaded"

    .line 1521
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "aborted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->a:Ljava/lang/String;

    const-string v1, "destroyed"

    .line 1522
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1524
    :cond_0
    const/4 v0, 0x0

    .line 1527
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic l(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    return-object v0
.end method

.method static synthetic m(Lcom/millennialmedia/InlineAd;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->j:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic n(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/InlineAd$InlineAdMetadata;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    return-object v0
.end method

.method static synthetic o(Lcom/millennialmedia/InlineAd;)Lcom/millennialmedia/internal/adadapters/InlineAdapter;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    return-object v0
.end method

.method static synthetic p(Lcom/millennialmedia/InlineAd;)Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->v:Z

    return v0
.end method

.method static synthetic q(Lcom/millennialmedia/InlineAd;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->j()V

    return-void
.end method


# virtual methods
.method a()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1064
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1065
    const/4 v0, 0x0

    .line 1071
    :goto_0
    return-object v0

    .line 1068
    :cond_0
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1069
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {}, Lcom/millennialmedia/internal/Handshake;->getMinInlineRefreshRate()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1071
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->l:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public abort(Lcom/millennialmedia/InlineAd$InlineAbortListener;)V
    .locals 3

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    :goto_0
    return-void

    .line 643
    :cond_0
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to abort playlist request for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

    .line 647
    monitor-enter p0

    .line 648
    :try_start_0
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 649
    invoke-static {}, Lcom/millennialmedia/MMLog;->isDebugEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Aborting playlist request for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->v:Z

    .line 655
    monitor-exit p0

    goto :goto_0

    .line 657
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 659
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->j()V

    goto :goto_0
.end method

.method b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1096
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1100
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/millennialmedia/InlineAd;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/millennialmedia/InlineAd;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 1562
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1568
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    .line 1569
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->h:Lcom/millennialmedia/InlineAd$InlineAbortListener;

    .line 1570
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->d:Lcom/millennialmedia/XIncentivizedEventListener;

    .line 1572
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    if-eqz v0, :cond_0

    .line 1573
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    invoke-virtual {v0}, Lcom/millennialmedia/InlineAd$ImpressionListener;->cancel()V

    .line 1574
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->r:Lcom/millennialmedia/InlineAd$ImpressionListener;

    .line 1577
    :cond_0
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->i()V

    .line 1579
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    if-eqz v0, :cond_1

    .line 1580
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    invoke-interface {v0}, Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;->cancel()V

    .line 1581
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->o:Lcom/millennialmedia/internal/utils/ThreadUtils$ScheduledRunnable;

    .line 1584
    :cond_1
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eqz v0, :cond_3

    .line 1585
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_2

    .line 1586
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 1587
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 1589
    :cond_2
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    .line 1591
    :cond_3
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eqz v0, :cond_5

    .line 1592
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    instance-of v0, v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    if-eqz v0, :cond_4

    .line 1593
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->close()V

    .line 1594
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    check-cast v0, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;

    invoke-interface {v0}, Lcom/millennialmedia/internal/adadapters/MMAdAdapter;->release()V

    .line 1596
    :cond_4
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->p:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    .line 1599
    :cond_5
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->f:Ljava/lang/ref/WeakReference;

    .line 1600
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1601
    new-instance v1, Lcom/millennialmedia/InlineAd$15;

    invoke-direct {v1, p0, v0}, Lcom/millennialmedia/InlineAd$15;-><init>(Lcom/millennialmedia/InlineAd;Ljava/lang/ref/WeakReference;)V

    invoke-static {v1}, Lcom/millennialmedia/internal/utils/ThreadUtils;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1610
    :cond_6
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->j:Landroid/widget/RelativeLayout;

    .line 1611
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    .line 1612
    iput-object v2, p0, Lcom/millennialmedia/InlineAd;->b:Lcom/millennialmedia/internal/PlayList;

    .line 1613
    return-void
.end method

.method public getCreativeInfo()Lcom/millennialmedia/CreativeInfo;
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/millennialmedia/InlineAd;->q:Lcom/millennialmedia/internal/adadapters/InlineAdapter;

    invoke-virtual {v0}, Lcom/millennialmedia/internal/adadapters/InlineAdapter;->getCreativeInfo()Lcom/millennialmedia/CreativeInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public request(Lcom/millennialmedia/InlineAd$InlineAdMetadata;)V
    .locals 3

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    :goto_0
    return-void

    .line 606
    :cond_0
    sget-object v0, Lcom/millennialmedia/InlineAd;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requesting playlist for placement ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/millennialmedia/InlineAd;->placementId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/millennialmedia/MMLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->i:Lcom/millennialmedia/InlineAd$InlineAdMetadata;

    .line 609
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/millennialmedia/InlineAd;->s:Z

    .line 611
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->g()V

    .line 612
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->h()V

    goto :goto_0
.end method

.method public setListener(Lcom/millennialmedia/InlineAd$InlineListener;)V
    .locals 1

    .prologue
    .line 1032
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1037
    :goto_0
    return-void

    .line 1036
    :cond_0
    iput-object p1, p0, Lcom/millennialmedia/InlineAd;->g:Lcom/millennialmedia/InlineAd$InlineListener;

    goto :goto_0
.end method

.method public setRefreshInterval(I)V
    .locals 1

    .prologue
    .line 1050
    invoke-virtual {p0}, Lcom/millennialmedia/InlineAd;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1059
    :cond_0
    :goto_0
    return-void

    .line 1054
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/millennialmedia/InlineAd;->l:Ljava/lang/Integer;

    .line 1056
    iget-boolean v0, p0, Lcom/millennialmedia/InlineAd;->s:Z

    if-eqz v0, :cond_0

    .line 1057
    invoke-direct {p0}, Lcom/millennialmedia/InlineAd;->h()V

    goto :goto_0
.end method
