.class final Lcom/facebook/a/a/a$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/a/a/a;->b(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:J

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(JLandroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 157
    iput-wide p1, p0, Lcom/facebook/a/a/a$3;->a:J

    iput-object p3, p0, Lcom/facebook/a/a/a$3;->b:Landroid/content/Context;

    iput-object p4, p0, Lcom/facebook/a/a/a$3;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 160
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v0

    if-nez v0, :cond_1

    .line 161
    new-instance v0, Lcom/facebook/a/a/f;

    iget-wide v2, p0, Lcom/facebook/a/a/a$3;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/facebook/a/a/f;-><init>(Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-static {v0}, Lcom/facebook/a/a/a;->a(Lcom/facebook/a/a/f;)Lcom/facebook/a/a/f;

    .line 162
    iget-object v0, p0, Lcom/facebook/a/a/a$3;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/a/a/a$3;->c:Ljava/lang/String;

    .line 166
    invoke-static {}, Lcom/facebook/a/a/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-static {v0, v1, v4, v2}, Lcom/facebook/a/a/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a/a/h;Ljava/lang/String;)V

    .line 189
    :cond_0
    :goto_0
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/a/a/a$3;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/a/a/f;->a(Ljava/lang/Long;)V

    .line 190
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/a/a/f;->j()V

    .line 191
    return-void

    .line 167
    :cond_1
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/a/a/f;->c()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-wide v0, p0, Lcom/facebook/a/a/a$3;->a:J

    .line 169
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/a/a/f;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 170
    invoke-static {}, Lcom/facebook/a/a/a;->d()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 173
    iget-object v0, p0, Lcom/facebook/a/a/a$3;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/a/a/a$3;->c:Ljava/lang/String;

    .line 176
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v2

    .line 177
    invoke-static {}, Lcom/facebook/a/a/a;->c()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/a/a/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a/a/f;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/facebook/a/a/a$3;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/a/a/a$3;->c:Ljava/lang/String;

    .line 182
    invoke-static {}, Lcom/facebook/a/a/a;->c()Ljava/lang/String;

    move-result-object v2

    .line 178
    invoke-static {v0, v1, v4, v2}, Lcom/facebook/a/a/g;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/a/a/h;Ljava/lang/String;)V

    .line 183
    new-instance v0, Lcom/facebook/a/a/f;

    iget-wide v2, p0, Lcom/facebook/a/a/a$3;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/facebook/a/a/f;-><init>(Ljava/lang/Long;Ljava/lang/Long;)V

    invoke-static {v0}, Lcom/facebook/a/a/a;->a(Lcom/facebook/a/a/f;)Lcom/facebook/a/a/f;

    goto :goto_0

    .line 184
    :cond_2
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 185
    invoke-static {}, Lcom/facebook/a/a/a;->b()Lcom/facebook/a/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/a/a/f;->e()V

    goto :goto_0
.end method
