.class public Lcom/facebook/internal/i;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/internal/i$a;
    }
.end annotation


# direct methods
.method public static a(Lcom/facebook/internal/a;)V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/facebook/l;

    const-string v1, "Unable to show the provided content via the web or the installed version of the Facebook app. Some dialogs are only supported starting API 14."

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-static {p0, v0}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/a;Lcom/facebook/l;)V

    .line 46
    return-void
.end method

.method public static a(Lcom/facebook/internal/a;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/facebook/internal/a;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/internal/a;->d()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 61
    invoke-virtual {p0}, Lcom/facebook/internal/a;->e()Z

    .line 62
    return-void
.end method

.method public static a(Lcom/facebook/internal/a;Landroid/os/Bundle;Lcom/facebook/internal/h;)V
    .locals 5

    .prologue
    .line 130
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/ae;->b(Landroid/content/Context;)V

    .line 131
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/ae;->a(Landroid/content/Context;)V

    .line 133
    invoke-interface {p2}, Lcom/facebook/internal/h;->name()Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-static {p2}, Lcom/facebook/internal/i;->d(Lcom/facebook/internal/h;)Landroid/net/Uri;

    move-result-object v1

    .line 135
    if-nez v1, :cond_0

    .line 136
    new-instance v1, Lcom/facebook/l;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to fetch the Url for the DialogFeature : \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_0
    invoke-static {}, Lcom/facebook/internal/y;->a()I

    move-result v0

    .line 144
    invoke-virtual {p0}, Lcom/facebook/internal/a;->c()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-static {v2, v0, p1}, Lcom/facebook/internal/ab;->a(Ljava/lang/String;ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 147
    if-nez v0, :cond_1

    .line 148
    new-instance v0, Lcom/facebook/l;

    const-string v1, "Unable to fetch the app\'s key-hash"

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->isRelative()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 154
    invoke-static {}, Lcom/facebook/internal/ab;->a()Ljava/lang/String;

    move-result-object v2

    .line 155
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-static {v2, v1, v0}, Lcom/facebook/internal/ad;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    .line 164
    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 165
    const-string v2, "url"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "is_fallback"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 171
    invoke-virtual {p0}, Lcom/facebook/internal/a;->c()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    invoke-interface {p2}, Lcom/facebook/internal/h;->a()Ljava/lang/String;

    move-result-object v3

    .line 173
    invoke-static {}, Lcom/facebook/internal/y;->a()I

    move-result v4

    .line 169
    invoke-static {v0, v2, v3, v4, v1}, Lcom/facebook/internal/y;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 175
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/FacebookActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 176
    const-string v1, "FacebookDialogFragment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    invoke-virtual {p0, v0}, Lcom/facebook/internal/a;->a(Landroid/content/Intent;)V

    .line 179
    return-void

    .line 159
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 158
    invoke-static {v2, v1, v0}, Lcom/facebook/internal/ad;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/internal/a;Lcom/facebook/internal/i$a;Lcom/facebook/internal/h;)V
    .locals 5

    .prologue
    .line 185
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v1

    .line 186
    invoke-interface {p2}, Lcom/facebook/internal/h;->a()Ljava/lang/String;

    move-result-object v2

    .line 188
    invoke-static {p2}, Lcom/facebook/internal/i;->c(Lcom/facebook/internal/h;)Lcom/facebook/internal/y$e;

    move-result-object v3

    .line 189
    invoke-virtual {v3}, Lcom/facebook/internal/y$e;->b()I

    move-result v0

    .line 190
    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 191
    new-instance v0, Lcom/facebook/l;

    const-string v1, "Cannot present this dialog. This likely means that the Facebook app is not installed."

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    invoke-static {v0}, Lcom/facebook/internal/y;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 199
    invoke-interface {p1}, Lcom/facebook/internal/i$a;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 204
    :goto_0
    if-nez v0, :cond_1

    .line 205
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 210
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/internal/a;->c()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 208
    invoke-static {v1, v4, v2, v3, v0}, Lcom/facebook/internal/y;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/internal/y$e;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 214
    if-nez v0, :cond_3

    .line 215
    new-instance v0, Lcom/facebook/l;

    const-string v1, "Unable to create Intent; this likely means theFacebook app is not installed."

    invoke-direct {v0, v1}, Lcom/facebook/l;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_2
    invoke-interface {p1}, Lcom/facebook/internal/i$a;->b()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_3
    invoke-virtual {p0, v0}, Lcom/facebook/internal/a;->a(Landroid/content/Intent;)V

    .line 221
    return-void
.end method

.method public static a(Lcom/facebook/internal/a;Lcom/facebook/internal/r;)V
    .locals 2

    .prologue
    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/facebook/internal/a;->b()Landroid/content/Intent;

    move-result-object v0

    .line 67
    invoke-virtual {p0}, Lcom/facebook/internal/a;->d()I

    move-result v1

    .line 65
    invoke-virtual {p1, v0, v1}, Lcom/facebook/internal/r;->a(Landroid/content/Intent;I)V

    .line 69
    invoke-virtual {p0}, Lcom/facebook/internal/a;->e()Z

    .line 70
    return-void
.end method

.method public static a(Lcom/facebook/internal/a;Lcom/facebook/l;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/facebook/internal/i;->b(Lcom/facebook/internal/a;Lcom/facebook/l;)V

    .line 51
    return-void
.end method

.method public static a(Lcom/facebook/internal/h;)Z
    .locals 2

    .prologue
    .line 74
    invoke-static {p0}, Lcom/facebook/internal/i;->c(Lcom/facebook/internal/h;)Lcom/facebook/internal/y$e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/internal/y$e;->b()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/internal/h;)[I
    .locals 3

    .prologue
    .line 255
    .line 256
    invoke-interface {p2}, Lcom/facebook/internal/h;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/internal/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/internal/o$a;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {v0}, Lcom/facebook/internal/o$a;->d()[I

    move-result-object v0

    .line 260
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-interface {p2}, Lcom/facebook/internal/h;->b()I

    move-result v2

    aput v2, v0, v1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/internal/a;Lcom/facebook/l;)V
    .locals 5

    .prologue
    .line 83
    if-nez p1, :cond_0

    .line 100
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/ae;->b(Landroid/content/Context;)V

    .line 88
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 89
    invoke-static {}, Lcom/facebook/p;->f()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/FacebookActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 90
    sget-object v1, Lcom/facebook/FacebookActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    invoke-virtual {p0}, Lcom/facebook/internal/a;->c()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 96
    invoke-static {}, Lcom/facebook/internal/y;->a()I

    move-result v3

    .line 97
    invoke-static {p1}, Lcom/facebook/internal/y;->a(Lcom/facebook/l;)Landroid/os/Bundle;

    move-result-object v4

    .line 92
    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/internal/y;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0, v0}, Lcom/facebook/internal/a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/internal/h;)Z
    .locals 1

    .prologue
    .line 79
    invoke-static {p0}, Lcom/facebook/internal/i;->d(Lcom/facebook/internal/h;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/internal/h;)Lcom/facebook/internal/y$e;
    .locals 2

    .prologue
    .line 240
    invoke-static {}, Lcom/facebook/p;->j()Ljava/lang/String;

    move-result-object v0

    .line 241
    invoke-interface {p0}, Lcom/facebook/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    .line 242
    invoke-static {v0, v1, p0}, Lcom/facebook/internal/i;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/internal/h;)[I

    move-result-object v0

    .line 244
    invoke-static {v1, v0}, Lcom/facebook/internal/y;->a(Ljava/lang/String;[I)Lcom/facebook/internal/y$e;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/facebook/internal/h;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 224
    invoke-interface {p0}, Lcom/facebook/internal/h;->name()Ljava/lang/String;

    move-result-object v0

    .line 225
    invoke-interface {p0}, Lcom/facebook/internal/h;->a()Ljava/lang/String;

    move-result-object v1

    .line 226
    invoke-static {}, Lcom/facebook/p;->j()Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-static {v2, v1, v0}, Lcom/facebook/internal/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/internal/o$a;

    move-result-object v1

    .line 230
    const/4 v0, 0x0

    .line 231
    if-eqz v1, :cond_0

    .line 232
    invoke-virtual {v1}, Lcom/facebook/internal/o$a;->c()Landroid/net/Uri;

    move-result-object v0

    .line 235
    :cond_0
    return-object v0
.end method
