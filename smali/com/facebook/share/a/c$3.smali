.class Lcom/facebook/share/a/c$3;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/facebook/u$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/share/a/c;->a(Lcom/facebook/share/a/c$m;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/facebook/share/a/c$e;

.field final synthetic b:Lcom/facebook/share/a/c$g;

.field final synthetic c:Lcom/facebook/share/a/c$m;

.field final synthetic d:Lcom/facebook/share/a/c;


# direct methods
.method constructor <init>(Lcom/facebook/share/a/c;Lcom/facebook/share/a/c$e;Lcom/facebook/share/a/c$g;Lcom/facebook/share/a/c$m;)V
    .locals 0

    .prologue
    .line 1216
    iput-object p1, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    iput-object p2, p0, Lcom/facebook/share/a/c$3;->a:Lcom/facebook/share/a/c$e;

    iput-object p3, p0, Lcom/facebook/share/a/c$3;->b:Lcom/facebook/share/a/c$g;

    iput-object p4, p0, Lcom/facebook/share/a/c$3;->c:Lcom/facebook/share/a/c$m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/u;)V
    .locals 6

    .prologue
    .line 1219
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    iget-object v1, p0, Lcom/facebook/share/a/c$3;->a:Lcom/facebook/share/a/c$e;

    iget-object v1, v1, Lcom/facebook/share/a/c$e;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/share/a/c;->c(Lcom/facebook/share/a/c;Ljava/lang/String;)Ljava/lang/String;

    .line 1220
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    invoke-static {v0}, Lcom/facebook/share/a/c;->h(Lcom/facebook/share/a/c;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/ad;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1221
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    iget-object v1, p0, Lcom/facebook/share/a/c$3;->b:Lcom/facebook/share/a/c$g;

    iget-object v1, v1, Lcom/facebook/share/a/c$g;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/share/a/c;->c(Lcom/facebook/share/a/c;Ljava/lang/String;)Ljava/lang/String;

    .line 1222
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    iget-object v1, p0, Lcom/facebook/share/a/c$3;->b:Lcom/facebook/share/a/c$g;

    iget-boolean v1, v1, Lcom/facebook/share/a/c$g;->f:Z

    invoke-static {v0, v1}, Lcom/facebook/share/a/c;->d(Lcom/facebook/share/a/c;Z)Z

    .line 1225
    :cond_0
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    invoke-static {v0}, Lcom/facebook/share/a/c;->h(Lcom/facebook/share/a/c;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/ad;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1226
    sget-object v0, Lcom/facebook/y;->f:Lcom/facebook/y;

    .line 1227
    invoke-static {}, Lcom/facebook/share/a/c;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to verify the FB id for \'%s\'. Verify that it is a valid FB object or page"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    .line 1230
    invoke-static {v5}, Lcom/facebook/share/a/c;->j(Lcom/facebook/share/a/c;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1226
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1231
    iget-object v1, p0, Lcom/facebook/share/a/c$3;->d:Lcom/facebook/share/a/c;

    const-string v2, "get_verified_id"

    iget-object v0, p0, Lcom/facebook/share/a/c$3;->b:Lcom/facebook/share/a/c$g;

    .line 1232
    invoke-virtual {v0}, Lcom/facebook/share/a/c$g;->a()Lcom/facebook/o;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/share/a/c$3;->b:Lcom/facebook/share/a/c$g;

    .line 1233
    invoke-virtual {v0}, Lcom/facebook/share/a/c$g;->a()Lcom/facebook/o;

    move-result-object v0

    .line 1231
    :goto_0
    invoke-static {v1, v2, v0}, Lcom/facebook/share/a/c;->a(Lcom/facebook/share/a/c;Ljava/lang/String;Lcom/facebook/o;)V

    .line 1237
    :cond_1
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->c:Lcom/facebook/share/a/c$m;

    if-eqz v0, :cond_2

    .line 1238
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->c:Lcom/facebook/share/a/c$m;

    invoke-interface {v0}, Lcom/facebook/share/a/c$m;->a()V

    .line 1240
    :cond_2
    return-void

    .line 1233
    :cond_3
    iget-object v0, p0, Lcom/facebook/share/a/c$3;->a:Lcom/facebook/share/a/c$e;

    .line 1234
    invoke-virtual {v0}, Lcom/facebook/share/a/c$e;->a()Lcom/facebook/o;

    move-result-object v0

    goto :goto_0
.end method
