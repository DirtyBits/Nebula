.class Lcom/facebook/share/a/c$k;
.super Lcom/facebook/share/a/c$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/share/a/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "k"
.end annotation


# instance fields
.field e:Ljava/lang/String;

.field final synthetic f:Lcom/facebook/share/a/c;


# direct methods
.method constructor <init>(Lcom/facebook/share/a/c;Ljava/lang/String;Lcom/facebook/share/widget/LikeView$e;)V
    .locals 5

    .prologue
    .line 1383
    iput-object p1, p0, Lcom/facebook/share/a/c$k;->f:Lcom/facebook/share/a/c;

    .line 1384
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/share/a/c$a;-><init>(Lcom/facebook/share/a/c;Ljava/lang/String;Lcom/facebook/share/widget/LikeView$e;)V

    .line 1386
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1387
    const-string v1, "object"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    new-instance v1, Lcom/facebook/s;

    .line 1390
    invoke-static {}, Lcom/facebook/a;->a()Lcom/facebook/a;

    move-result-object v2

    const-string v3, "me/og.likes"

    sget-object v4, Lcom/facebook/w;->b:Lcom/facebook/w;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/facebook/s;-><init>(Lcom/facebook/a;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/w;)V

    .line 1389
    invoke-virtual {p0, v1}, Lcom/facebook/share/a/c$k;->a(Lcom/facebook/s;)V

    .line 1394
    return-void
.end method


# virtual methods
.method protected a(Lcom/facebook/o;)V
    .locals 6

    .prologue
    .line 1403
    invoke-virtual {p1}, Lcom/facebook/o;->b()I

    move-result v0

    .line 1404
    const/16 v1, 0xdad

    if-ne v0, v1, :cond_0

    .line 1409
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/share/a/c$k;->c:Lcom/facebook/o;

    .line 1419
    :goto_0
    return-void

    .line 1411
    :cond_0
    sget-object v0, Lcom/facebook/y;->a:Lcom/facebook/y;

    .line 1412
    invoke-static {}, Lcom/facebook/share/a/c;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error liking object \'%s\' with type \'%s\' : %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/share/a/c$k;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/share/a/c$k;->b:Lcom/facebook/share/widget/LikeView$e;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    .line 1411
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/internal/v;->a(Lcom/facebook/y;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1417
    iget-object v0, p0, Lcom/facebook/share/a/c$k;->f:Lcom/facebook/share/a/c;

    const-string v1, "publish_like"

    invoke-static {v0, v1, p1}, Lcom/facebook/share/a/c;->a(Lcom/facebook/share/a/c;Ljava/lang/String;Lcom/facebook/o;)V

    goto :goto_0
.end method

.method protected a(Lcom/facebook/v;)V
    .locals 2

    .prologue
    .line 1398
    invoke-virtual {p1}, Lcom/facebook/v;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "id"

    invoke-static {v0, v1}, Lcom/facebook/internal/ad;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/a/c$k;->e:Ljava/lang/String;

    .line 1399
    return-void
.end method
