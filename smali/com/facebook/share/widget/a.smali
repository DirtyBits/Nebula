.class public Lcom/facebook/share/widget/a;
.super Lcom/facebook/internal/j;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/share/widget/a$c;,
        Lcom/facebook/share/widget/a$a;,
        Lcom/facebook/share/widget/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/internal/j",
        "<",
        "Lcom/facebook/share/b/a;",
        "Lcom/facebook/share/widget/a$b;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/facebook/internal/f$b;->h:Lcom/facebook/internal/f$b;

    .line 75
    invoke-virtual {v0}, Lcom/facebook/internal/f$b;->a()I

    move-result v0

    sput v0, Lcom/facebook/share/widget/a;->b:I

    .line 74
    return-void
.end method

.method private constructor <init>(Lcom/facebook/internal/r;)V
    .locals 1

    .prologue
    .line 169
    sget v0, Lcom/facebook/share/widget/a;->b:I

    invoke-direct {p0, p1, v0}, Lcom/facebook/internal/j;-><init>(Lcom/facebook/internal/r;I)V

    .line 170
    return-void
.end method

.method static synthetic a(Lcom/facebook/share/b/a;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lcom/facebook/share/widget/a;->b(Lcom/facebook/share/b/a;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Fragment;Lcom/facebook/share/b/a;)V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/facebook/internal/r;

    invoke-direct {v0, p0}, Lcom/facebook/internal/r;-><init>(Landroid/app/Fragment;)V

    invoke-static {v0, p1}, Lcom/facebook/share/widget/a;->a(Lcom/facebook/internal/r;Lcom/facebook/share/b/a;)V

    .line 124
    return-void
.end method

.method private static a(Lcom/facebook/internal/r;Lcom/facebook/share/b/a;)V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/facebook/share/widget/a;

    invoke-direct {v0, p0}, Lcom/facebook/share/widget/a;-><init>(Lcom/facebook/internal/r;)V

    .line 130
    invoke-virtual {v0, p1}, Lcom/facebook/share/widget/a;->a(Ljava/lang/Object;)V

    .line 131
    return-void
.end method

.method private static b(Lcom/facebook/share/b/a;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 276
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 277
    const-string v0, "app_link_url"

    invoke-virtual {p0}, Lcom/facebook/share/b/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v0, "preview_image_url"

    invoke-virtual {p0}, Lcom/facebook/share/b/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v0, "destination"

    .line 281
    invoke-virtual {p0}, Lcom/facebook/share/b/a;->e()Lcom/facebook/share/b/a$a$a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/share/b/a$a$a;->toString()Ljava/lang/String;

    move-result-object v2

    .line 279
    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0}, Lcom/facebook/share/b/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_1

    .line 286
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/share/b/a;->d()Ljava/lang/String;

    move-result-object v2

    .line 288
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 291
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 292
    const-string v4, "promo_code"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 293
    const-string v4, "promo_text"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 295
    const-string v4, "deeplink_context"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-string v3, "promo_code"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v0, "promo_text"

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :cond_0
    :goto_1
    return-object v1

    .line 285
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    .line 299
    const-string v0, "AppInviteDialog"

    const-string v2, "Json Exception in creating deeplink context"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcom/facebook/share/widget/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/share/widget/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/facebook/share/widget/a;->h()Z

    move-result v0

    return v0
.end method

.method static synthetic f()Lcom/facebook/internal/h;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/facebook/share/widget/a;->j()Lcom/facebook/internal/h;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g()Z
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/facebook/share/widget/a;->i()Z

    move-result v0

    return v0
.end method

.method private static h()Z
    .locals 1

    .prologue
    .line 134
    invoke-static {}, Lcom/facebook/share/widget/a;->j()Lcom/facebook/internal/h;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/i;->a(Lcom/facebook/internal/h;)Z

    move-result v0

    return v0
.end method

.method private static i()Z
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/facebook/share/widget/a;->j()Lcom/facebook/internal/h;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/internal/i;->b(Lcom/facebook/internal/h;)Z

    move-result v0

    return v0
.end method

.method private static j()Lcom/facebook/internal/h;
    .locals 1

    .prologue
    .line 272
    sget-object v0, Lcom/facebook/share/a/a;->a:Lcom/facebook/share/a/a;

    return-object v0
.end method


# virtual methods
.method protected b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/internal/j",
            "<",
            "Lcom/facebook/share/b/a;",
            "Lcom/facebook/share/widget/a$b;",
            ">.a;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 213
    new-instance v1, Lcom/facebook/share/widget/a$a;

    invoke-direct {v1, p0, v2}, Lcom/facebook/share/widget/a$a;-><init>(Lcom/facebook/share/widget/a;Lcom/facebook/share/widget/a$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    new-instance v1, Lcom/facebook/share/widget/a$c;

    invoke-direct {v1, p0, v2}, Lcom/facebook/share/widget/a$c;-><init>(Lcom/facebook/share/widget/a;Lcom/facebook/share/widget/a$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    return-object v0
.end method

.method protected c()Lcom/facebook/internal/a;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Lcom/facebook/internal/a;

    invoke-virtual {p0}, Lcom/facebook/share/widget/a;->a()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/internal/a;-><init>(I)V

    return-object v0
.end method
