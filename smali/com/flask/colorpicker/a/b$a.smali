.class public Lcom/flask/colorpicker/a/b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flask/colorpicker/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Lcom/flask/colorpicker/a/b$1;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/flask/colorpicker/a/b$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    return-object v0
.end method

.method public a(F)Lcom/flask/colorpicker/a/b$a;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 45
    return-object p0
.end method

.method public a(I)Lcom/flask/colorpicker/a/b$a;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    return-object p0
.end method

.method public a(Landroid/graphics/Paint$Style;)Lcom/flask/colorpicker/a/b$a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    return-object p0
.end method

.method public a(Landroid/graphics/PorterDuff$Mode;)Lcom/flask/colorpicker/a/b$a;
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    invoke-direct {v1, p1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 50
    return-object p0
.end method

.method public a(Landroid/graphics/Shader;)Lcom/flask/colorpicker/a/b$a;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/flask/colorpicker/a/b$a;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 55
    return-object p0
.end method
