.class public Lcom/flask/colorpicker/f;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)F
    .locals 2

    .prologue
    .line 10
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(F)I
    .locals 1

    .prologue
    .line 14
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static a(FI)I
    .locals 2

    .prologue
    .line 18
    invoke-static {p0}, Lcom/flask/colorpicker/f;->a(F)I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    const v1, 0xffffff

    and-int/2addr v1, p1

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(IF)I
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 23
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 24
    const/4 v1, 0x2

    aput p1, v0, v1

    .line 25
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v0

    return v0
.end method

.method public static a(IZ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    move v1, v0

    .line 36
    :goto_0
    if-eqz p1, :cond_1

    const-string v0, "#%08X"

    .line 37
    :goto_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    and-int/2addr v1, p0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 35
    :cond_0
    const v0, 0xffffff

    move v1, v0

    goto :goto_0

    .line 36
    :cond_1
    const-string v0, "#%06X"

    goto :goto_1
.end method

.method public static b(I)F
    .locals 2

    .prologue
    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [F

    .line 30
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 31
    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method
