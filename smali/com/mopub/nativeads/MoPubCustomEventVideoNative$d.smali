.class Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation build Lcom/mopub/common/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mopub/nativeads/MoPubCustomEventVideoNative;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "d"
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 899
    :try_start_0
    const-string v0, "Play-Visible-Percent"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->b:I

    .line 900
    const-string v0, "Pause-Visible-Percent"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->c:I

    .line 901
    const-string v0, "Impression-Min-Visible-Percent"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->d:I

    .line 903
    const-string v0, "Impression-Visible-Ms"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->e:I

    .line 904
    const-string v0, "Max-Buffer-Ms"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->f:I

    .line 905
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->a:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 909
    :goto_0
    return-void

    .line 906
    :catch_0
    move-exception v0

    .line 907
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->a:Z

    goto :goto_0
.end method


# virtual methods
.method a()Z
    .locals 1

    .prologue
    .line 912
    iget-boolean v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->a:Z

    return v0
.end method

.method b()I
    .locals 1

    .prologue
    .line 916
    iget v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->b:I

    return v0
.end method

.method c()I
    .locals 1

    .prologue
    .line 920
    iget v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->c:I

    return v0
.end method

.method d()I
    .locals 1

    .prologue
    .line 924
    iget v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->d:I

    return v0
.end method

.method e()I
    .locals 1

    .prologue
    .line 928
    iget v0, p0, Lcom/mopub/nativeads/MoPubCustomEventVideoNative$d;->e:I

    return v0
.end method
