.class public Lb/a/a/d/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(DDD)D
    .locals 4

    .prologue
    .line 7
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, p0

    mul-double/2addr v0, p2

    mul-double v2, p0, p4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(FFF)F
    .locals 2

    .prologue
    .line 12
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p0

    mul-float/2addr v0, p1

    mul-float v1, p0, p2

    add-float/2addr v0, v1

    return v0
.end method

.method public static b(FFF)F
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 18
    cmpg-float v2, p0, v0

    if-gez v2, :cond_1

    .line 20
    :goto_0
    cmpl-float v2, v0, v1

    if-lez v2, :cond_0

    move v0, v1

    .line 22
    :cond_0
    sub-float/2addr v1, v0

    mul-float/2addr v1, p1

    mul-float/2addr v0, p2

    add-float/2addr v0, v1

    return v0

    :cond_1
    move v0, p0

    goto :goto_0
.end method
