.class public Landroid/support/v4/a/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/a/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/a/s",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/support/v4/a/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/s",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    .line 53
    return-void
.end method

.method public static final a(Landroid/support/v4/a/s;)Landroid/support/v4/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/a/s",
            "<*>;)",
            "Landroid/support/v4/a/r;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, Landroid/support/v4/a/r;

    invoke-direct {v0, p0}, Landroid/support/v4/a/r;-><init>(Landroid/support/v4/a/s;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/support/v4/a/o;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->b(Ljava/lang/String;)Landroid/support/v4/a/o;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/support/v4/a/t;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->k()Landroid/support/v4/a/u;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/a/u;->a(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->a(Landroid/content/res/Configuration;)V

    .line 305
    return-void
.end method

.method public a(Landroid/os/Parcelable;Landroid/support/v4/a/v;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/a/u;->a(Landroid/os/Parcelable;Landroid/support/v4/a/v;)V

    .line 159
    return-void
.end method

.method public a(Landroid/support/v4/a/o;)V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    iget-object v1, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v2, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0, v1, v2, p1}, Landroid/support/v4/a/u;->a(Landroid/support/v4/a/s;Landroid/support/v4/a/q;Landroid/support/v4/a/o;)V

    .line 106
    return-void
.end method

.method public a(Landroid/support/v4/g/k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/g/k",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/a/ab;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 445
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/s;->a(Landroid/support/v4/g/k;)V

    .line 446
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/support/v4/a/s;->b(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 453
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->a(Z)V

    .line 283
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->a(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/a/u;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b()Landroid/support/v4/a/ab;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->l()Landroid/support/v4/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->b(Landroid/view/Menu;)V

    .line 378
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->b(Z)V

    .line 294
    return-void
.end method

.method public b(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/u;->b(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->i()V

    .line 128
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0, p1}, Landroid/support/v4/a/s;->a(Z)V

    .line 407
    return-void
.end method

.method public d()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->h()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public e()Landroid/support/v4/a/v;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->g()Landroid/support/v4/a/v;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->j()V

    .line 191
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->k()V

    .line 202
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->l()V

    .line 213
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->m()V

    .line 224
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->n()V

    .line 235
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->o()V

    .line 246
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->p()V

    .line 250
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->r()V

    .line 272
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->s()V

    .line 317
    return-void
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    iget-object v0, v0, Landroid/support/v4/a/s;->d:Landroid/support/v4/a/u;

    invoke-virtual {v0}, Landroid/support/v4/a/u;->d()Z

    move-result v0

    return v0
.end method

.method public p()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->n()V

    .line 396
    return-void
.end method

.method public q()V
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->o()V

    .line 421
    return-void
.end method

.method public r()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->p()V

    .line 428
    return-void
.end method

.method public s()Landroid/support/v4/g/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/support/v4/g/k",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    iget-object v0, p0, Landroid/support/v4/a/r;->a:Landroid/support/v4/a/s;

    invoke-virtual {v0}, Landroid/support/v4/a/s;->q()Landroid/support/v4/g/k;

    move-result-object v0

    return-object v0
.end method
