.class public Landroid/support/v7/widget/ac;
.super Landroid/support/v7/widget/aj$g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v7/widget/ac$b;,
        Landroid/support/v7/widget/ac$a;,
        Landroid/support/v7/widget/ac$d;,
        Landroid/support/v7/widget/ac$c;
    }
.end annotation


# instance fields
.field a:I

.field b:Landroid/support/v7/widget/ai;

.field c:Z

.field d:I

.field e:I

.field f:Landroid/support/v7/widget/ac$d;

.field final g:Landroid/support/v7/widget/ac$a;

.field private k:Landroid/support/v7/widget/ac$c;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method private A()Landroid/view/View;
    .locals 1

    .prologue
    .line 1496
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private a(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I
    .locals 3

    .prologue
    .line 848
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v0

    sub-int/2addr v0, p1

    .line 850
    if-lez v0, :cond_1

    .line 851
    neg-int v0, v0

    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v7/widget/ac;->c(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I

    move-result v0

    neg-int v0, v0

    .line 856
    add-int v1, p1, v0

    .line 857
    if-eqz p4, :cond_0

    .line 859
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->d()I

    move-result v2

    sub-int v1, v2, v1

    .line 860
    if-lez v1, :cond_0

    .line 861
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ai;->a(I)V

    .line 862
    add-int/2addr v0, v1

    .line 865
    :cond_0
    :goto_0
    return v0

    .line 853
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ZZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 1508
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    .line 1509
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 1512
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private a(IIZLandroid/support/v7/widget/aj$r;)V
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 1118
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p4}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$r;)I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/ac$c;->h:I

    .line 1119
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p1, v2, Landroid/support/v7/widget/ac$c;->f:I

    .line 1121
    if-ne p1, v1, :cond_2

    .line 1122
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v2, Landroid/support/v7/widget/ac$c;->h:I

    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->g()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/support/v7/widget/ac$c;->h:I

    .line 1124
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->A()Landroid/view/View;

    move-result-object v2

    .line 1126
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-boolean v4, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, Landroid/support/v7/widget/ac$c;->e:I

    .line 1128
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v3, Landroid/support/v7/widget/ac$c;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/ac$c;->d:I

    .line 1129
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 1131
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v1}, Landroid/support/v7/widget/ai;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1144
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p2, v1, Landroid/support/v7/widget/ac$c;->c:I

    .line 1145
    if-eqz p3, :cond_0

    .line 1146
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v2, v1, Landroid/support/v7/widget/ac$c;->c:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/support/v7/widget/ac$c;->c:I

    .line 1148
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v0, v1, Landroid/support/v7/widget/ac$c;->g:I

    .line 1149
    return-void

    :cond_1
    move v0, v1

    .line 1126
    goto :goto_0

    .line 1135
    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->z()Landroid/view/View;

    move-result-object v2

    .line 1136
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v3, Landroid/support/v7/widget/ac$c;->h:I

    iget-object v5, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v5}, Landroid/support/v7/widget/ai;->c()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Landroid/support/v7/widget/ac$c;->h:I

    .line 1137
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-boolean v4, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v4, :cond_3

    :goto_2
    iput v1, v3, Landroid/support/v7/widget/ac$c;->e:I

    .line 1139
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v3, Landroid/support/v7/widget/ac$c;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/ac$c;->d:I

    .line 1140
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 1141
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v1}, Landroid/support/v7/widget/ai;->c()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    .line 1137
    goto :goto_2
.end method

.method private a(Landroid/support/v7/widget/ac$a;)V
    .locals 2

    .prologue
    .line 894
    iget v0, p1, Landroid/support/v7/widget/ac$a;->a:I

    iget v1, p1, Landroid/support/v7/widget/ac$a;->b:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/ac;->c(II)V

    .line 895
    return-void
.end method

.method private a(Landroid/support/v7/widget/aj$m;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1217
    if-gez p2, :cond_1

    .line 1244
    :cond_0
    :goto_0
    return-void

    .line 1226
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v2

    .line 1227
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_3

    .line 1228
    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-ltz v0, :cond_0

    .line 1229
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v1

    .line 1230
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v1

    if-le v1, p2, :cond_2

    .line 1231
    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;II)V

    goto :goto_0

    .line 1228
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1236
    :goto_2
    if-ge v0, v2, :cond_0

    .line 1237
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v3

    .line 1238
    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v3

    if-le v3, p2, :cond_4

    .line 1239
    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;II)V

    goto :goto_0

    .line 1236
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private a(Landroid/support/v7/widget/aj$m;II)V
    .locals 1

    .prologue
    .line 1191
    if-ne p2, p3, :cond_1

    .line 1206
    :cond_0
    return-void

    .line 1197
    :cond_1
    if-le p3, p2, :cond_2

    .line 1198
    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_0

    .line 1199
    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/ac;->a(ILandroid/support/v7/widget/aj$m;)V

    .line 1198
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1202
    :cond_2
    :goto_1
    if-le p2, p3, :cond_0

    .line 1203
    invoke-virtual {p0, p2, p1}, Landroid/support/v7/widget/ac;->a(ILandroid/support/v7/widget/aj$m;)V

    .line 1202
    add-int/lit8 p2, p2, -0x1

    goto :goto_1
.end method

.method private a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;)V
    .locals 2

    .prologue
    .line 1297
    iget-boolean v0, p2, Landroid/support/v7/widget/ac$c;->a:Z

    if-nez v0, :cond_0

    .line 1305
    :goto_0
    return-void

    .line 1300
    :cond_0
    iget v0, p2, Landroid/support/v7/widget/ac$c;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1301
    iget v0, p2, Landroid/support/v7/widget/ac$c;->g:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ac;->b(Landroid/support/v7/widget/aj$m;I)V

    goto :goto_0

    .line 1303
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/ac$c;->g:I

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;I)V

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/high16 v4, -0x80000000

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 762
    invoke-virtual {p1}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    if-ne v0, v5, :cond_1

    :cond_0
    move v1, v2

    .line 840
    :goto_0
    return v1

    .line 766
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    if-ltz v0, :cond_2

    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    invoke-virtual {p1}, Landroid/support/v7/widget/aj$r;->d()I

    move-result v3

    if-lt v0, v3, :cond_3

    .line 767
    :cond_2
    iput v5, p0, Landroid/support/v7/widget/ac;->d:I

    .line 768
    iput v4, p0, Landroid/support/v7/widget/ac;->e:I

    move v1, v2

    .line 772
    goto :goto_0

    .line 777
    :cond_3
    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    iput v0, p2, Landroid/support/v7/widget/ac$a;->a:I

    .line 778
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$d;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 781
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    iget-boolean v0, v0, Landroid/support/v7/widget/ac$d;->c:Z

    iput-boolean v0, p2, Landroid/support/v7/widget/ac$a;->c:Z

    .line 782
    iget-boolean v0, p2, Landroid/support/v7/widget/ac$a;->c:Z

    if-eqz v0, :cond_4

    .line 783
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    iget v2, v2, Landroid/support/v7/widget/ac$d;->b:I

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    goto :goto_0

    .line 786
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    iget v2, v2, Landroid/support/v7/widget/ac$d;->b:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    goto :goto_0

    .line 792
    :cond_5
    iget v0, p0, Landroid/support/v7/widget/ac;->e:I

    if-ne v0, v4, :cond_e

    .line 793
    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->a(I)Landroid/view/View;

    move-result-object v0

    .line 794
    if-eqz v0, :cond_a

    .line 795
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ai;->c(Landroid/view/View;)I

    move-result v3

    .line 796
    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->f()I

    move-result v4

    if-le v3, v4, :cond_6

    .line 798
    invoke-virtual {p2}, Landroid/support/v7/widget/ac$a;->b()V

    goto :goto_0

    .line 801
    :cond_6
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 803
    if-gez v3, :cond_7

    .line 804
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v0

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    .line 805
    iput-boolean v2, p2, Landroid/support/v7/widget/ac$a;->c:Z

    goto :goto_0

    .line 808
    :cond_7
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->d()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    .line 810
    if-gez v2, :cond_8

    .line 811
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v0

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    .line 812
    iput-boolean v1, p2, Landroid/support/v7/widget/ac$a;->c:Z

    goto/16 :goto_0

    .line 815
    :cond_8
    iget-boolean v2, p2, Landroid/support/v7/widget/ac$a;->c:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->b()I

    move-result v2

    add-int/2addr v0, v2

    :goto_1
    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    goto/16 :goto_0

    :cond_9
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_1

    .line 820
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-lez v0, :cond_c

    .line 822
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    .line 823
    iget v3, p0, Landroid/support/v7/widget/ac;->d:I

    if-ge v3, v0, :cond_d

    move v0, v1

    :goto_2
    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->c:Z

    if-ne v0, v3, :cond_b

    move v2, v1

    :cond_b
    iput-boolean v2, p2, Landroid/support/v7/widget/ac$a;->c:Z

    .line 826
    :cond_c
    invoke-virtual {p2}, Landroid/support/v7/widget/ac$a;->b()V

    goto/16 :goto_0

    :cond_d
    move v0, v2

    .line 823
    goto :goto_2

    .line 831
    :cond_e
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    iput-boolean v0, p2, Landroid/support/v7/widget/ac$a;->c:Z

    .line 833
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_f

    .line 834
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/ac;->e:I

    sub-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    goto/16 :goto_0

    .line 837
    :cond_f
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/ac;->e:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/ac$a;->b:I

    goto/16 :goto_0
.end method

.method private b(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I
    .locals 4

    .prologue
    .line 873
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v0

    sub-int v0, p1, v0

    .line 875
    if-lez v0, :cond_1

    .line 877
    invoke-virtual {p0, v0, p2, p3}, Landroid/support/v7/widget/ac;->c(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I

    move-result v0

    neg-int v0, v0

    .line 881
    add-int v1, p1, v0

    .line 882
    if-eqz p4, :cond_0

    .line 884
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 885
    if-lez v1, :cond_0

    .line 886
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ai;->a(I)V

    .line 887
    sub-int/2addr v0, v1

    .line 890
    :cond_0
    :goto_0
    return v0

    .line 879
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(ZZ)Landroid/view/View;
    .locals 2

    .prologue
    .line 1526
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    .line 1527
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 1530
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/widget/ac$a;)V
    .locals 2

    .prologue
    .line 908
    iget v0, p1, Landroid/support/v7/widget/ac$a;->a:I

    iget v1, p1, Landroid/support/v7/widget/ac$a;->b:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/ac;->d(II)V

    .line 909
    return-void
.end method

.method private b(Landroid/support/v7/widget/aj$m;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1256
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v2

    .line 1257
    if-gez p2, :cond_1

    .line 1283
    :cond_0
    :goto_0
    return-void

    .line 1264
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->e()I

    move-result v0

    sub-int v3, v0, p2

    .line 1265
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 1266
    :goto_1
    if-ge v0, v2, :cond_0

    .line 1267
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v4

    .line 1268
    iget-object v5, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v4

    if-ge v4, v3, :cond_2

    .line 1269
    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;II)V

    goto :goto_0

    .line 1266
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1274
    :cond_3
    add-int/lit8 v0, v2, -0x1

    :goto_2
    if-ltz v0, :cond_0

    .line 1275
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v1

    .line 1276
    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v1

    if-ge v1, v3, :cond_4

    .line 1277
    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;II)V

    goto :goto_0

    .line 1274
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2
.end method

.method private b(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;II)V
    .locals 9

    .prologue
    .line 642
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 648
    invoke-virtual {p1}, Landroid/support/v7/widget/aj$m;->b()Ljava/util/List;

    move-result-object v5

    .line 649
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    .line 650
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v7

    .line 651
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_6

    .line 652
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$u;

    .line 653
    invoke-virtual {v0}, Landroid/support/v7/widget/aj$u;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v2

    move v1, v3

    .line 651
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    move v2, v0

    goto :goto_1

    .line 656
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/aj$u;->getLayoutPosition()I

    move-result v1

    .line 657
    if-ge v1, v7, :cond_3

    const/4 v1, 0x1

    :goto_3
    iget-boolean v8, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eq v1, v8, :cond_4

    const/4 v1, -0x1

    .line 659
    :goto_4
    const/4 v8, -0x1

    if-ne v1, v8, :cond_5

    .line 660
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    iget-object v0, v0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ai;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v3

    move v1, v0

    move v0, v2

    goto :goto_2

    .line 657
    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x1

    goto :goto_4

    .line 662
    :cond_5
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    iget-object v0, v0, Landroid/support/v7/widget/aj$u;->itemView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ai;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v2

    move v1, v3

    goto :goto_2

    .line 670
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput-object v5, v0, Landroid/support/v7/widget/ac$c;->k:Ljava/util/List;

    .line 671
    if-lez v3, :cond_7

    .line 672
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->z()Landroid/view/View;

    move-result-object v0

    .line 673
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, p3}, Landroid/support/v7/widget/ac;->d(II)V

    .line 674
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v3, v0, Landroid/support/v7/widget/ac$c;->h:I

    .line 675
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/ac$c;->c:I

    .line 676
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$c;->a()V

    .line 677
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 680
    :cond_7
    if-lez v2, :cond_8

    .line 681
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->A()Landroid/view/View;

    move-result-object v0

    .line 682
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, p4}, Landroid/support/v7/widget/ac;->c(II)V

    .line 683
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v2, v0, Landroid/support/v7/widget/ac$c;->h:I

    .line 684
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/ac$c;->c:I

    .line 685
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$c;->a()V

    .line 686
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 688
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/ac$c;->k:Ljava/util/List;

    goto/16 :goto_0
.end method

.method private b(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)V
    .locals 1

    .prologue
    .line 693
    invoke-direct {p0, p2, p3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/ac;->c(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 709
    invoke-virtual {p3}, Landroid/support/v7/widget/ac$a;->b()V

    .line 710
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->n:Z

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    iput v0, p3, Landroid/support/v7/widget/ac$a;->a:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 898
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->d()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/ac$c;->c:I

    .line 899
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/ac$c;->e:I

    .line 901
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p1, v0, Landroid/support/v7/widget/ac$c;->d:I

    .line 902
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v1, v0, Landroid/support/v7/widget/ac$c;->f:I

    .line 903
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p2, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 904
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/ac$c;->g:I

    .line 905
    return-void

    :cond_0
    move v0, v1

    .line 899
    goto :goto_0
.end method

.method private c(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 721
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v2

    if-nez v2, :cond_1

    .line 754
    :cond_0
    :goto_0
    return v0

    .line 724
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->w()Landroid/view/View;

    move-result-object v2

    .line 725
    if-eqz v2, :cond_2

    invoke-static {p3, v2, p2}, Landroid/support/v7/widget/ac$a;->a(Landroid/support/v7/widget/ac$a;Landroid/view/View;Landroid/support/v7/widget/aj$r;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 726
    invoke-virtual {p3, v2}, Landroid/support/v7/widget/ac$a;->a(Landroid/view/View;)V

    move v0, v1

    .line 727
    goto :goto_0

    .line 729
    :cond_2
    iget-boolean v2, p0, Landroid/support/v7/widget/ac;->l:Z

    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->n:Z

    if-ne v2, v3, :cond_0

    .line 732
    iget-boolean v2, p3, Landroid/support/v7/widget/ac$a;->c:Z

    if-eqz v2, :cond_6

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->f(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v2

    .line 735
    :goto_1
    if-eqz v2, :cond_0

    .line 736
    invoke-virtual {p3, v2}, Landroid/support/v7/widget/ac$a;->b(Landroid/view/View;)V

    .line 739
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->k()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 741
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->d()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3}, Landroid/support/v7/widget/ai;->c()I

    move-result v3

    if-ge v2, v3, :cond_4

    :cond_3
    move v0, v1

    .line 746
    :cond_4
    if-eqz v0, :cond_5

    .line 747
    iget-boolean v0, p3, Landroid/support/v7/widget/ac$a;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v0

    :goto_2
    iput v0, p3, Landroid/support/v7/widget/ac$a;->b:I

    :cond_5
    move v0, v1

    .line 752
    goto :goto_0

    .line 732
    :cond_6
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->g(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v2

    goto :goto_1

    .line 747
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v0

    goto :goto_2
.end method

.method private d(II)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 912
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->c()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Landroid/support/v7/widget/ac$c;->c:I

    .line 913
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p1, v0, Landroid/support/v7/widget/ac$c;->d:I

    .line 914
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/ac$c;->e:I

    .line 916
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v1, v0, Landroid/support/v7/widget/ac$c;->f:I

    .line 917
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p2, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 918
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/ac$c;->g:I

    .line 920
    return-void

    :cond_0
    move v0, v1

    .line 914
    goto :goto_0
.end method

.method private f(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1548
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->h(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->i(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private g(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1565
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->i(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->h(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private h(Landroid/support/v7/widget/aj$r;)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1052
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-nez v0, :cond_0

    .line 1056
    :goto_0
    return v4

    .line 1055
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1056
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/ac;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, Landroid/support/v7/widget/ac;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/ac;->o:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/ac;->c:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/ap;->a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ai;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/aj$g;ZZ)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private h(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1570
    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v4

    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->d()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private i(I)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x1

    const/high16 v1, -0x80000000

    .line 1453
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 1474
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v2

    .line 1457
    goto :goto_0

    .line 1459
    :sswitch_2
    iget v3, p0, Landroid/support/v7/widget/ac;->a:I

    if-eq v3, v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1462
    :sswitch_3
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    move v0, v1

    goto :goto_0

    .line 1465
    :sswitch_4
    iget v2, p0, Landroid/support/v7/widget/ac;->a:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 1468
    :sswitch_5
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    if-nez v0, :cond_2

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 1453
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method private i(Landroid/support/v7/widget/aj$r;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1063
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-nez v0, :cond_0

    .line 1067
    :goto_0
    return v4

    .line 1066
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1067
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/ac;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, Landroid/support/v7/widget/ac;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/ac;->o:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ap;->a(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ai;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/aj$g;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private i(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1574
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    const/4 v4, -0x1

    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->d()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private j(Landroid/support/v7/widget/aj$r;)I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1074
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-nez v0, :cond_0

    .line 1078
    :goto_0
    return v4

    .line 1077
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1078
    iget-object v1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_2

    move v0, v3

    :goto_1
    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/ac;->a(ZZ)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->o:Z

    if-nez v0, :cond_1

    move v4, v3

    :cond_1
    invoke-direct {p0, v4, v3}, Landroid/support/v7/widget/ac;->b(ZZ)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/ac;->o:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/ap;->b(Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ai;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/aj$g;Z)I

    move-result v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1
.end method

.method private y()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 340
    iget v1, p0, Landroid/support/v7/widget/ac;->a:I

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 341
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->m:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    .line 345
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/widget/ac;->m:Z

    if-nez v1, :cond_2

    :goto_1
    iput-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private z()Landroid/view/View;
    .locals 1

    .prologue
    .line 1486
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I
    .locals 2

    .prologue
    .line 1003
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1004
    const/4 v0, 0x0

    .line 1006
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/ac;->c(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I

    move-result v0

    goto :goto_0
.end method

.method a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    .line 1321
    iget v1, p2, Landroid/support/v7/widget/ac$c;->c:I

    .line 1322
    iget v0, p2, Landroid/support/v7/widget/ac$c;->g:I

    if-eq v0, v6, :cond_1

    .line 1324
    iget v0, p2, Landroid/support/v7/widget/ac$c;->c:I

    if-gez v0, :cond_0

    .line 1325
    iget v0, p2, Landroid/support/v7/widget/ac$c;->g:I

    iget v2, p2, Landroid/support/v7/widget/ac$c;->c:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/ac$c;->g:I

    .line 1327
    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;)V

    .line 1329
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/ac$c;->c:I

    iget v2, p2, Landroid/support/v7/widget/ac$c;->h:I

    add-int/2addr v0, v2

    .line 1330
    new-instance v2, Landroid/support/v7/widget/ac$b;

    invoke-direct {v2}, Landroid/support/v7/widget/ac$b;-><init>()V

    .line 1331
    :cond_2
    if-lez v0, :cond_3

    invoke-virtual {p2, p3}, Landroid/support/v7/widget/ac$c;->a(Landroid/support/v7/widget/aj$r;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1332
    invoke-virtual {v2}, Landroid/support/v7/widget/ac$b;->a()V

    .line 1333
    invoke-virtual {p0, p1, p3, p2, v2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/ac$b;)V

    .line 1334
    iget-boolean v3, v2, Landroid/support/v7/widget/ac$b;->b:Z

    if-eqz v3, :cond_4

    .line 1365
    :cond_3
    :goto_0
    iget v0, p2, Landroid/support/v7/widget/ac$c;->c:I

    sub-int v0, v1, v0

    return v0

    .line 1337
    :cond_4
    iget v3, p2, Landroid/support/v7/widget/ac$c;->b:I

    iget v4, v2, Landroid/support/v7/widget/ac$b;->a:I

    iget v5, p2, Landroid/support/v7/widget/ac$c;->f:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/ac$c;->b:I

    .line 1344
    iget-boolean v3, v2, Landroid/support/v7/widget/ac$b;->c:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget-object v3, v3, Landroid/support/v7/widget/ac$c;->k:Ljava/util/List;

    if-nez v3, :cond_5

    invoke-virtual {p3}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v3

    if-nez v3, :cond_6

    .line 1346
    :cond_5
    iget v3, p2, Landroid/support/v7/widget/ac$c;->c:I

    iget v4, v2, Landroid/support/v7/widget/ac$b;->a:I

    sub-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/ac$c;->c:I

    .line 1348
    iget v3, v2, Landroid/support/v7/widget/ac$b;->a:I

    sub-int/2addr v0, v3

    .line 1351
    :cond_6
    iget v3, p2, Landroid/support/v7/widget/ac$c;->g:I

    if-eq v3, v6, :cond_8

    .line 1352
    iget v3, p2, Landroid/support/v7/widget/ac$c;->g:I

    iget v4, v2, Landroid/support/v7/widget/ac$b;->a:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/ac$c;->g:I

    .line 1353
    iget v3, p2, Landroid/support/v7/widget/ac$c;->c:I

    if-gez v3, :cond_7

    .line 1354
    iget v3, p2, Landroid/support/v7/widget/ac$c;->g:I

    iget v4, p2, Landroid/support/v7/widget/ac$c;->c:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/ac$c;->g:I

    .line 1356
    :cond_7
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;)V

    .line 1358
    :cond_8
    if-eqz p4, :cond_2

    iget-boolean v3, v2, Landroid/support/v7/widget/ac$b;->d:Z

    if-eqz v3, :cond_2

    goto :goto_0
.end method

.method protected a(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p1}, Landroid/support/v7/widget/aj$r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->f()I

    move-result v0

    .line 417
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Landroid/support/v7/widget/aj$h;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 180
    new-instance v0, Landroid/support/v7/widget/aj$h;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/aj$h;-><init>(II)V

    return-object v0
.end method

.method public a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 385
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    .line 386
    if-nez v0, :cond_1

    .line 387
    const/4 v0, 0x0

    .line 398
    :cond_0
    :goto_0
    return-object v0

    .line 389
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v1

    .line 390
    sub-int v1, p1, v1

    .line 391
    if-ltz v1, :cond_2

    if-ge v1, v0, :cond_2

    .line 392
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v0

    .line 393
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v1

    if-eq v1, p1, :cond_0

    .line 398
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v7/widget/aj$g;->a(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method a(IIZZ)Landroid/view/View;
    .locals 7

    .prologue
    .line 1689
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1690
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v3

    .line 1691
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v4

    .line 1692
    if-le p2, p1, :cond_1

    const/4 v0, 0x1

    .line 1693
    :goto_0
    const/4 v2, 0x0

    .line 1694
    :goto_1
    if-eq p1, p2, :cond_3

    .line 1695
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v1

    .line 1696
    iget-object v5, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v5, v1}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v5

    .line 1697
    iget-object v6, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v6, v1}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v6

    .line 1698
    if-ge v5, v4, :cond_4

    if-le v6, v3, :cond_4

    .line 1699
    if-eqz p3, :cond_0

    .line 1700
    if-lt v5, v3, :cond_2

    if-gt v6, v4, :cond_2

    .line 1710
    :cond_0
    :goto_2
    return-object v1

    .line 1692
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 1702
    :cond_2
    if-eqz p4, :cond_4

    if-nez v2, :cond_4

    .line 1694
    :goto_3
    add-int/2addr p1, v0

    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 1710
    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_3
.end method

.method a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1580
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1583
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->c()I

    move-result v5

    .line 1584
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->d()I

    move-result v6

    .line 1585
    if-le p4, p3, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 1586
    :goto_1
    if-eq p3, p4, :cond_3

    .line 1587
    invoke-virtual {p0, p3}, Landroid/support/v7/widget/ac;->e(I)Landroid/view/View;

    move-result-object v3

    .line 1588
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    .line 1589
    if-ltz v0, :cond_6

    if-ge v0, p5, :cond_6

    .line 1590
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1591
    if-nez v4, :cond_6

    move-object v0, v2

    .line 1586
    :goto_2
    add-int/2addr p3, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 1585
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 1594
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 1596
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 1597
    goto :goto_2

    .line 1604
    :cond_3
    if-eqz v2, :cond_5

    :goto_3
    move-object v3, v2

    :cond_4
    return-object v3

    :cond_5
    move-object v2, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method public a(Landroid/view/View;ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/high16 v5, -0x80000000

    const/4 v0, 0x0

    .line 1716
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->y()V

    .line 1717
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v1

    if-nez v1, :cond_1

    .line 1754
    :cond_0
    :goto_0
    return-object v0

    .line 1721
    :cond_1
    invoke-direct {p0, p2}, Landroid/support/v7/widget/ac;->i(I)I

    move-result v3

    .line 1722
    if-eq v3, v5, :cond_0

    .line 1725
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1727
    if-ne v3, v6, :cond_2

    .line 1728
    invoke-direct {p0, p3, p4}, Landroid/support/v7/widget/ac;->g(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1732
    :goto_1
    if-eqz v2, :cond_0

    .line 1739
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1740
    const v1, 0x3ea8f5c3    # 0.33f

    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->f()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 1741
    invoke-direct {p0, v3, v1, v7, p4}, Landroid/support/v7/widget/ac;->a(IIZLandroid/support/v7/widget/aj$r;)V

    .line 1742
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v5, v1, Landroid/support/v7/widget/ac$c;->g:I

    .line 1743
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput-boolean v7, v1, Landroid/support/v7/widget/ac$c;->a:Z

    .line 1744
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    const/4 v4, 0x1

    invoke-virtual {p0, p3, v1, p4, v4}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 1746
    if-ne v3, v6, :cond_3

    .line 1747
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->z()Landroid/view/View;

    move-result-object v1

    .line 1751
    :goto_2
    if-eq v1, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1754
    goto :goto_0

    .line 1730
    :cond_2
    invoke-direct {p0, p3, p4}, Landroid/support/v7/widget/ac;->f(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 1749
    :cond_3
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->A()Landroid/view/View;

    move-result-object v1

    goto :goto_2
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 988
    iput p1, p0, Landroid/support/v7/widget/ac;->d:I

    .line 989
    iput p2, p0, Landroid/support/v7/widget/ac;->e:I

    .line 990
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$d;->b()V

    .line 993
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->l()V

    .line 994
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 261
    instance-of v0, p1, Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_0

    .line 262
    check-cast p1, Landroid/support/v7/widget/ac$d;

    iput-object p1, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    .line 263
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->l()V

    .line 270
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, -0x80000000

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 464
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/ac;->d:I

    if-eq v0, v6, :cond_1

    .line 465
    :cond_0
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 466
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ac;->c(Landroid/support/v7/widget/aj$m;)V

    .line 619
    :goto_0
    return-void

    .line 470
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$d;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 471
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    iget v0, v0, Landroid/support/v7/widget/ac$d;->a:I

    iput v0, p0, Landroid/support/v7/widget/ac;->d:I

    .line 474
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 475
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput-boolean v1, v0, Landroid/support/v7/widget/ac$c;->a:Z

    .line 477
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->y()V

    .line 479
    iget-object v0, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$a;->a()V

    .line 480
    iget-object v0, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    iget-boolean v2, p0, Landroid/support/v7/widget/ac;->c:Z

    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->n:Z

    xor-int/2addr v2, v3

    iput-boolean v2, v0, Landroid/support/v7/widget/ac$a;->c:Z

    .line 482
    iget-object v0, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/ac;->b(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)V

    .line 491
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    .line 494
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v2, v2, Landroid/support/v7/widget/ac$c;->j:I

    if-ltz v2, :cond_8

    move v2, v1

    .line 501
    :goto_1
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3}, Landroid/support/v7/widget/ai;->c()I

    move-result v3

    add-int/2addr v2, v3

    .line 502
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3}, Landroid/support/v7/widget/ai;->g()I

    move-result v3

    add-int/2addr v0, v3

    .line 503
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Landroid/support/v7/widget/ac;->d:I

    if-eq v3, v6, :cond_3

    iget v3, p0, Landroid/support/v7/widget/ac;->e:I

    if-eq v3, v7, :cond_3

    .line 508
    iget v3, p0, Landroid/support/v7/widget/ac;->d:I

    invoke-virtual {p0, v3}, Landroid/support/v7/widget/ac;->a(I)Landroid/view/View;

    move-result-object v3

    .line 509
    if-eqz v3, :cond_3

    .line 512
    iget-boolean v4, p0, Landroid/support/v7/widget/ac;->c:Z

    if-eqz v4, :cond_9

    .line 513
    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->d()I

    move-result v4

    iget-object v5, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v5, v3}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v3

    sub-int v3, v4, v3

    .line 515
    iget v4, p0, Landroid/support/v7/widget/ac;->e:I

    sub-int/2addr v3, v4

    .line 521
    :goto_2
    if-lez v3, :cond_a

    .line 522
    add-int/2addr v2, v3

    .line 530
    :cond_3
    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-virtual {p0, p1, p2, v3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)V

    .line 531
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;)V

    .line 532
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v4

    iput-boolean v4, v3, Landroid/support/v7/widget/ac$c;->i:Z

    .line 533
    iget-object v3, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    iget-boolean v3, v3, Landroid/support/v7/widget/ac$a;->c:Z

    if-eqz v3, :cond_b

    .line 535
    iget-object v3, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/ac;->b(Landroid/support/v7/widget/ac$a;)V

    .line 536
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v2, v3, Landroid/support/v7/widget/ac$c;->h:I

    .line 537
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v2, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 538
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v2, v2, Landroid/support/v7/widget/ac$c;->b:I

    .line 539
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v3, Landroid/support/v7/widget/ac$c;->d:I

    .line 540
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v3, Landroid/support/v7/widget/ac$c;->c:I

    if-lez v3, :cond_4

    .line 541
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v3, Landroid/support/v7/widget/ac$c;->c:I

    add-int/2addr v0, v3

    .line 544
    :cond_4
    iget-object v3, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/ac$a;)V

    .line 545
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v0, v3, Landroid/support/v7/widget/ac$c;->h:I

    .line 546
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v0, Landroid/support/v7/widget/ac$c;->d:I

    iget-object v5, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v5, v5, Landroid/support/v7/widget/ac$c;->e:I

    add-int/2addr v3, v5

    iput v3, v0, Landroid/support/v7/widget/ac$c;->d:I

    .line 547
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 548
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 550
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v0, v0, Landroid/support/v7/widget/ac$c;->c:I

    if-lez v0, :cond_e

    .line 552
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v0, v0, Landroid/support/v7/widget/ac$c;->c:I

    .line 553
    invoke-direct {p0, v4, v2}, Landroid/support/v7/widget/ac;->d(II)V

    .line 554
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v0, v2, Landroid/support/v7/widget/ac$c;->h:I

    .line 555
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 556
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v0, v0, Landroid/support/v7/widget/ac$c;->b:I

    :goto_4
    move v2, v0

    move v0, v3

    .line 588
    :cond_5
    :goto_5
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v3

    if-lez v3, :cond_6

    .line 592
    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->c:Z

    iget-boolean v4, p0, Landroid/support/v7/widget/ac;->n:Z

    xor-int/2addr v3, v4

    if-eqz v3, :cond_d

    .line 593
    invoke-direct {p0, v0, p1, p2, v8}, Landroid/support/v7/widget/ac;->a(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I

    move-result v3

    .line 594
    add-int/2addr v2, v3

    .line 595
    add-int/2addr v0, v3

    .line 596
    invoke-direct {p0, v2, p1, p2, v1}, Landroid/support/v7/widget/ac;->b(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I

    move-result v1

    .line 597
    add-int/2addr v2, v1

    .line 598
    add-int/2addr v0, v1

    .line 608
    :cond_6
    :goto_6
    invoke-direct {p0, p1, p2, v2, v0}, Landroid/support/v7/widget/ac;->b(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;II)V

    .line 609
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$r;->a()Z

    move-result v0

    if-nez v0, :cond_7

    .line 610
    iput v6, p0, Landroid/support/v7/widget/ac;->d:I

    .line 611
    iput v7, p0, Landroid/support/v7/widget/ac;->e:I

    .line 612
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->a()V

    .line 614
    :cond_7
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->n:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/ac;->l:Z

    .line 615
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    goto/16 :goto_0

    :cond_8
    move v2, v0

    move v0, v1

    .line 499
    goto/16 :goto_1

    .line 517
    :cond_9
    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v4}, Landroid/support/v7/widget/ai;->c()I

    move-result v4

    sub-int/2addr v3, v4

    .line 519
    iget v4, p0, Landroid/support/v7/widget/ac;->e:I

    sub-int v3, v4, v3

    goto/16 :goto_2

    .line 524
    :cond_a
    sub-int/2addr v0, v3

    goto/16 :goto_3

    .line 560
    :cond_b
    iget-object v3, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-direct {p0, v3}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/ac$a;)V

    .line 561
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v0, v3, Landroid/support/v7/widget/ac$c;->h:I

    .line 562
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 563
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v0, v0, Landroid/support/v7/widget/ac$c;->b:I

    .line 564
    iget-object v3, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v3, v3, Landroid/support/v7/widget/ac$c;->d:I

    .line 565
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v4, Landroid/support/v7/widget/ac$c;->c:I

    if-lez v4, :cond_c

    .line 566
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v4, Landroid/support/v7/widget/ac$c;->c:I

    add-int/2addr v2, v4

    .line 569
    :cond_c
    iget-object v4, p0, Landroid/support/v7/widget/ac;->g:Landroid/support/v7/widget/ac$a;

    invoke-direct {p0, v4}, Landroid/support/v7/widget/ac;->b(Landroid/support/v7/widget/ac$a;)V

    .line 570
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v2, v4, Landroid/support/v7/widget/ac$c;->h:I

    .line 571
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v2, Landroid/support/v7/widget/ac$c;->d:I

    iget-object v5, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v5, v5, Landroid/support/v7/widget/ac$c;->e:I

    add-int/2addr v4, v5

    iput v4, v2, Landroid/support/v7/widget/ac$c;->d:I

    .line 572
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v2, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 573
    iget-object v2, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v2, v2, Landroid/support/v7/widget/ac$c;->b:I

    .line 575
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v4, Landroid/support/v7/widget/ac$c;->c:I

    if-lez v4, :cond_5

    .line 576
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v4, v4, Landroid/support/v7/widget/ac$c;->c:I

    .line 578
    invoke-direct {p0, v3, v0}, Landroid/support/v7/widget/ac;->c(II)V

    .line 579
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput v4, v0, Landroid/support/v7/widget/ac$c;->h:I

    .line 580
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    .line 581
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v0, v0, Landroid/support/v7/widget/ac$c;->b:I

    goto/16 :goto_5

    .line 600
    :cond_d
    invoke-direct {p0, v2, p1, p2, v8}, Landroid/support/v7/widget/ac;->b(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I

    move-result v3

    .line 601
    add-int/2addr v2, v3

    .line 602
    add-int/2addr v0, v3

    .line 603
    invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v7/widget/ac;->a(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Z)I

    move-result v1

    .line 604
    add-int/2addr v2, v1

    .line 605
    add-int/2addr v0, v1

    goto/16 :goto_6

    :cond_e
    move v0, v2

    goto/16 :goto_4
.end method

.method a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$a;)V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/ac$b;)V
    .locals 9

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1370
    invoke-virtual {p3, p1}, Landroid/support/v7/widget/ac$c;->a(Landroid/support/v7/widget/aj$m;)Landroid/view/View;

    move-result-object v1

    .line 1371
    if-nez v1, :cond_0

    .line 1377
    iput-boolean v7, p4, Landroid/support/v7/widget/ac$b;->b:Z

    .line 1440
    :goto_0
    return-void

    .line 1380
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/widget/aj$h;

    .line 1381
    iget-object v0, p3, Landroid/support/v7/widget/ac$c;->k:Ljava/util/List;

    if-nez v0, :cond_5

    .line 1382
    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->c:Z

    iget v0, p3, Landroid/support/v7/widget/ac$c;->f:I

    if-ne v0, v4, :cond_3

    move v0, v7

    :goto_1
    if-ne v3, v0, :cond_4

    .line 1384
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->b(Landroid/view/View;)V

    .line 1396
    :goto_2
    invoke-virtual {p0, v1, v2, v2}, Landroid/support/v7/widget/ac;->a(Landroid/view/View;II)V

    .line 1397
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ai;->c(Landroid/view/View;)I

    move-result v0

    iput v0, p4, Landroid/support/v7/widget/ac$b;->a:I

    .line 1399
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    if-ne v0, v7, :cond_a

    .line 1400
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->f()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1401
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->q()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->u()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1402
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ai;->d(Landroid/view/View;)I

    move-result v2

    sub-int v2, v0, v2

    .line 1407
    :goto_3
    iget v3, p3, Landroid/support/v7/widget/ac$c;->f:I

    if-ne v3, v4, :cond_9

    .line 1408
    iget v3, p3, Landroid/support/v7/widget/ac$c;->b:I

    .line 1409
    iget v4, p3, Landroid/support/v7/widget/ac$c;->b:I

    iget v5, p4, Landroid/support/v7/widget/ac$b;->a:I

    sub-int/2addr v4, v5

    move v8, v3

    move v3, v4

    move v4, v0

    move v0, v8

    .line 1428
    :goto_4
    iget v5, v6, Landroid/support/v7/widget/aj$h;->leftMargin:I

    add-int/2addr v2, v5

    iget v5, v6, Landroid/support/v7/widget/aj$h;->topMargin:I

    add-int/2addr v3, v5

    iget v5, v6, Landroid/support/v7/widget/aj$h;->rightMargin:I

    sub-int/2addr v4, v5

    iget v5, v6, Landroid/support/v7/widget/aj$h;->bottomMargin:I

    sub-int v5, v0, v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/ac;->a(Landroid/view/View;IIII)V

    .line 1436
    invoke-virtual {v6}, Landroid/support/v7/widget/aj$h;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v6}, Landroid/support/v7/widget/aj$h;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1437
    :cond_1
    iput-boolean v7, p4, Landroid/support/v7/widget/ac$b;->c:Z

    .line 1439
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v0

    iput-boolean v0, p4, Landroid/support/v7/widget/ac$b;->d:Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1382
    goto :goto_1

    .line 1386
    :cond_4
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/ac;->b(Landroid/view/View;I)V

    goto :goto_2

    .line 1389
    :cond_5
    iget-boolean v3, p0, Landroid/support/v7/widget/ac;->c:Z

    iget v0, p3, Landroid/support/v7/widget/ac$c;->f:I

    if-ne v0, v4, :cond_6

    move v0, v7

    :goto_5
    if-ne v3, v0, :cond_7

    .line 1391
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->a(Landroid/view/View;)V

    goto :goto_2

    :cond_6
    move v0, v2

    .line 1389
    goto :goto_5

    .line 1393
    :cond_7
    invoke-virtual {p0, v1, v2}, Landroid/support/v7/widget/ac;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 1404
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->s()I

    move-result v2

    .line 1405
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ai;->d(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_3

    .line 1411
    :cond_9
    iget v4, p3, Landroid/support/v7/widget/ac$c;->b:I

    .line 1412
    iget v3, p3, Landroid/support/v7/widget/ac$c;->b:I

    iget v5, p4, Landroid/support/v7/widget/ac$b;->a:I

    add-int/2addr v3, v5

    move v8, v3

    move v3, v4

    move v4, v0

    move v0, v8

    goto :goto_4

    .line 1415
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->t()I

    move-result v3

    .line 1416
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ai;->d(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v3

    .line 1418
    iget v2, p3, Landroid/support/v7/widget/ac$c;->f:I

    if-ne v2, v4, :cond_b

    .line 1419
    iget v2, p3, Landroid/support/v7/widget/ac$c;->b:I

    .line 1420
    iget v4, p3, Landroid/support/v7/widget/ac$c;->b:I

    iget v5, p4, Landroid/support/v7/widget/ac$b;->a:I

    sub-int/2addr v4, v5

    move v8, v2

    move v2, v4

    move v4, v8

    goto :goto_4

    .line 1422
    :cond_b
    iget v4, p3, Landroid/support/v7/widget/ac$c;->b:I

    .line 1423
    iget v2, p3, Landroid/support/v7/widget/ac$c;->b:I

    iget v5, p4, Landroid/support/v7/widget/ac$b;->a:I

    add-int/2addr v2, v5

    move v8, v2

    move v2, v4

    move v4, v8

    goto :goto_4
.end method

.method public a(Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$m;)V
    .locals 1

    .prologue
    .line 214
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/aj$g;->a(Landroid/support/v7/widget/aj;Landroid/support/v7/widget/aj$m;)V

    .line 215
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->p:Z

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/ac;->c(Landroid/support/v7/widget/aj$m;)V

    .line 217
    invoke-virtual {p2}, Landroid/support/v7/widget/aj$m;->a()V

    .line 219
    :cond_0
    return-void
.end method

.method public a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0, p1}, Landroid/support/v7/widget/aj$g;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 224
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-lez v0, :cond_0

    .line 225
    invoke-static {p1}, Landroid/support/v4/h/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/h/a/k;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/h/a/k;->b(I)V

    .line 228
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/h/a/k;->c(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public b(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1015
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    if-nez v0, :cond_0

    .line 1016
    const/4 v0, 0x0

    .line 1018
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/ac;->c(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1023
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->h(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method public b()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 234
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_0

    .line 235
    new-instance v0, Landroid/support/v7/widget/ac$d;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/ac$d;-><init>(Landroid/support/v7/widget/ac$d;)V

    .line 256
    :goto_0
    return-object v0

    .line 237
    :cond_0
    new-instance v0, Landroid/support/v7/widget/ac$d;

    invoke-direct {v0}, Landroid/support/v7/widget/ac$d;-><init>()V

    .line 238
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v1

    if-lez v1, :cond_2

    .line 239
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 240
    iget-boolean v1, p0, Landroid/support/v7/widget/ac;->l:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/ac;->c:Z

    xor-int/2addr v1, v2

    .line 241
    iput-boolean v1, v0, Landroid/support/v7/widget/ac$d;->c:Z

    .line 242
    if-eqz v1, :cond_1

    .line 243
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->A()Landroid/view/View;

    move-result-object v1

    .line 244
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->d()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/ai;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/ac$d;->b:I

    .line 246
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/ac$d;->a:I

    goto :goto_0

    .line 248
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/ac;->z()Landroid/view/View;

    move-result-object v1

    .line 249
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/ac$d;->a:I

    .line 250
    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ai;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    invoke-virtual {v2}, Landroid/support/v7/widget/ai;->c()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/ac$d;->b:I

    goto :goto_0

    .line 254
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/ac$d;->b()V

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 960
    iput p1, p0, Landroid/support/v7/widget/ac;->d:I

    .line 961
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/ac;->e:I

    .line 962
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    invoke-virtual {v0}, Landroid/support/v7/widget/ac$d;->b()V

    .line 965
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->l()V

    .line 966
    return-void
.end method

.method c(ILandroid/support/v7/widget/aj$m;Landroid/support/v7/widget/aj$r;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1152
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move p1, v2

    .line 1174
    :goto_0
    return p1

    .line 1155
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput-boolean v1, v0, Landroid/support/v7/widget/ac$c;->a:Z

    .line 1156
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->g()V

    .line 1157
    if-lez p1, :cond_2

    move v0, v1

    .line 1158
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1159
    invoke-direct {p0, v0, v3, v1, p3}, Landroid/support/v7/widget/ac;->a(IIZLandroid/support/v7/widget/aj$r;)V

    .line 1160
    iget-object v1, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iget v1, v1, Landroid/support/v7/widget/ac$c;->g:I

    .line 1161
    iget-object v4, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    invoke-virtual {p0, p2, v4, p3, v2}, Landroid/support/v7/widget/ac;->a(Landroid/support/v7/widget/aj$m;Landroid/support/v7/widget/ac$c;Landroid/support/v7/widget/aj$r;Z)I

    move-result v4

    add-int/2addr v1, v4

    .line 1162
    if-gez v1, :cond_3

    move p1, v2

    .line 1166
    goto :goto_0

    .line 1157
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 1168
    :cond_3
    if-le v3, v1, :cond_4

    mul-int p1, v0, v1

    .line 1169
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ai;->a(I)V

    .line 1173
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    iput p1, v0, Landroid/support/v7/widget/ac$c;->j:I

    goto :goto_0
.end method

.method public c(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1028
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->h(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1033
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->i(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 285
    iget v1, p0, Landroid/support/v7/widget/ac;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1038
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->i(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->n:Z

    return v0
.end method

.method public f(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1043
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->j(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method protected f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 923
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->n()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Landroid/support/v7/widget/aj$r;)I
    .locals 1

    .prologue
    .line 1048
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ac;->j(Landroid/support/v7/widget/aj$r;)I

    move-result v0

    return v0
.end method

.method g()V
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    if-nez v0, :cond_0

    .line 928
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->h()Landroid/support/v7/widget/ac$c;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ac;->k:Landroid/support/v7/widget/ac$c;

    .line 930
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    if-nez v0, :cond_1

    .line 931
    iget v0, p0, Landroid/support/v7/widget/ac;->a:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/aj$g;I)Landroid/support/v7/widget/ai;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/ai;

    .line 933
    :cond_1
    return-void
.end method

.method h()Landroid/support/v7/widget/ac$c;
    .locals 1

    .prologue
    .line 941
    new-instance v0, Landroid/support/v7/widget/ac$c;

    invoke-direct {v0}, Landroid/support/v7/widget/ac$c;-><init>()V

    return-object v0
.end method

.method public i()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1626
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v2, v0, v2, v1}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v0

    .line 1627
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public j()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 1666
    invoke-virtual {p0}, Landroid/support/v7/widget/ac;->p()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/support/v7/widget/ac;->a(IIZZ)Landroid/view/View;

    move-result-object v1

    .line 1667
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ac;->c(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 1823
    iget-object v0, p0, Landroid/support/v7/widget/ac;->f:Landroid/support/v7/widget/ac$d;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/ac;->l:Z

    iget-boolean v1, p0, Landroid/support/v7/widget/ac;->n:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
