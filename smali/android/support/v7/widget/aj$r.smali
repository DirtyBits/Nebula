.class public Landroid/support/v7/widget/aj$r;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/widget/aj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "r"
.end annotation


# instance fields
.field a:I

.field private b:I

.field private c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method static synthetic a(Landroid/support/v7/widget/aj$r;I)I
    .locals 0

    .prologue
    .line 9343
    iput p1, p0, Landroid/support/v7/widget/aj$r;->e:I

    return p1
.end method

.method static synthetic a(Landroid/support/v7/widget/aj$r;)Z
    .locals 1

    .prologue
    .line 9343
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->i:Z

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/aj$r;Z)Z
    .locals 0

    .prologue
    .line 9343
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$r;->f:Z

    return p1
.end method

.method static synthetic b(Landroid/support/v7/widget/aj$r;I)I
    .locals 0

    .prologue
    .line 9343
    iput p1, p0, Landroid/support/v7/widget/aj$r;->d:I

    return p1
.end method

.method static synthetic b(Landroid/support/v7/widget/aj$r;)Z
    .locals 1

    .prologue
    .line 9343
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->h:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/widget/aj$r;Z)Z
    .locals 0

    .prologue
    .line 9343
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$r;->g:Z

    return p1
.end method

.method static synthetic c(Landroid/support/v7/widget/aj$r;I)I
    .locals 0

    .prologue
    .line 9343
    iput p1, p0, Landroid/support/v7/widget/aj$r;->b:I

    return p1
.end method

.method static synthetic c(Landroid/support/v7/widget/aj$r;)Z
    .locals 1

    .prologue
    .line 9343
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->j:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v7/widget/aj$r;Z)Z
    .locals 0

    .prologue
    .line 9343
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$r;->h:Z

    return p1
.end method

.method static synthetic d(Landroid/support/v7/widget/aj$r;)Z
    .locals 1

    .prologue
    .line 9343
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->f:Z

    return v0
.end method

.method static synthetic d(Landroid/support/v7/widget/aj$r;Z)Z
    .locals 0

    .prologue
    .line 9343
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$r;->i:Z

    return p1
.end method

.method static synthetic e(Landroid/support/v7/widget/aj$r;)Z
    .locals 1

    .prologue
    .line 9343
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->g:Z

    return v0
.end method

.method static synthetic e(Landroid/support/v7/widget/aj$r;Z)Z
    .locals 0

    .prologue
    .line 9343
    iput-boolean p1, p0, Landroid/support/v7/widget/aj$r;->j:Z

    return p1
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 9386
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->g:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 9397
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->i:Z

    return v0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 9470
    iget v0, p0, Landroid/support/v7/widget/aj$r;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 9506
    iget-boolean v0, p0, Landroid/support/v7/widget/aj$r;->g:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/aj$r;->d:I

    iget v1, p0, Landroid/support/v7/widget/aj$r;->e:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/aj$r;->a:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 9513
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "State{mTargetPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/aj$r;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/aj$r;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mItemCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/aj$r;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPreviousLayoutItemCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/aj$r;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDeletedInvisibleItemCountSincePreviousLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/widget/aj$r;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mStructureChanged="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/aj$r;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mInPreLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/aj$r;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRunSimpleAnimations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/aj$r;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRunPredictiveAnimations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/widget/aj$r;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
