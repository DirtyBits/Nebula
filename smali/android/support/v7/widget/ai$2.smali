.class final Landroid/support/v7/widget/ai$2;
.super Landroid/support/v7/widget/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v7/widget/ai;->b(Landroid/support/v7/widget/aj$g;)Landroid/support/v7/widget/ai;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/support/v7/widget/aj$g;)V
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/ai;-><init>(Landroid/support/v7/widget/aj$g;Landroid/support/v7/widget/ai$1;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 316
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    .line 318
    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/aj$g;->g(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/aj$h;->topMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/aj$g;->g(I)V

    .line 284
    return-void
.end method

.method public b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    .line 311
    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/aj$g;->i(Landroid/view/View;)I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/aj$h;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$g;->t()I

    move-result v0

    return v0
.end method

.method public c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 293
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    .line 295
    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/aj$g;->e(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/aj$h;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/aj$h;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$g;->r()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1}, Landroid/support/v7/widget/aj$g;->v()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 301
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj$h;

    .line 303
    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/aj$g;->d(Landroid/view/View;)I

    move-result v1

    iget v2, v0, Landroid/support/v7/widget/aj$h;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/support/v7/widget/aj$h;->rightMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$g;->r()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$g;->r()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1}, Landroid/support/v7/widget/aj$g;->t()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v1}, Landroid/support/v7/widget/aj$g;->v()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Landroid/support/v7/widget/ai$2;->a:Landroid/support/v7/widget/aj$g;

    invoke-virtual {v0}, Landroid/support/v7/widget/aj$g;->v()I

    move-result v0

    return v0
.end method
